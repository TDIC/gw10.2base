<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.guidewire.pl.productmodel</groupId>
  <artifactId>productmodel-codegen-parent</artifactId>
  <version>3.3.2</version>
  <packaging>pom</packaging>

  <name>Product Model Codegen</name>
  <description>Product Model Codegen projects</description>

  <organization>
    <name>Guidewire, Inc.</name>
    <url>http://guidewire.com</url>
  </organization>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <!-- plugins -->
    <maven.enforcer.plugin.version>3.0.0-M2</maven.enforcer.plugin.version>
    <versions.maven.plugin.version>2.7</versions.maven.plugin.version>
    <maven.compiler.plugin.version>3.8.0</maven.compiler.plugin.version>
    <maven.release.plugin.version>2.5.3</maven.release.plugin.version>
    <maven.surefire.plugin.version>3.0.0-M3</maven.surefire.plugin.version>

    <!-- maven/java -->
    <required.maven.version>3.6.0</required.maven.version>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>

    <!-- libraries -->
    <commons.cli.version>1.4</commons.cli.version>
    <guava.version>22.0</guava.version>
    <jaxb2.basics.runtime.version>0.6.4</jaxb2.basics.runtime.version>
    <junit.version>4.12</junit.version>
    <org.jetbrains.annotations.version>17.0.0</org.jetbrains.annotations.version> 

    <codegen.api.version>0.20.1</codegen.api.version>
    <codemodel.version>1.0.1</codemodel.version>
    <entity.codegen.version>10.10.2</entity.codegen.version>
    <pli18n.version>0.7.1</pli18n.version>
    <simplexml.version>1.13.1</simplexml.version>
  </properties>

  <scm>
    <developerConnection>scm:git:ssh://git@stash.guidewire.com/pc/productmodel-codegen.git</developerConnection>
    <connection>scm:git:https://git@stash.guidewire.com/scm/pc/productmodel-codegen.git</connection>
    <tag>productmodel-codegen-parent-3.3.2</tag>
  </scm>

  <distributionManagement>
    <repository>
      <id>com.guidewire.pl.releases</id> <!-- id must match id in the ~/.m2/settings.xml! -->
      <url>https://nexus.guidewire.com/content/repositories/releases/</url>
      <name>Guidewire internal releases repository</name>
    </repository>
    <snapshotRepository>
      <id>snapshots-dev</id>
      <url>https://nexus.guidewire.com/content/repositories/snapshots-dev/</url>
      <name>Guidewire internal dev snapshots repository</name>
    </snapshotRepository>
  </distributionManagement>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-enforcer-plugin</artifactId>
          <version>${maven.enforcer.plugin.version}</version>
          <executions>
            <execution>
              <id>enforce-maven</id>
              <goals>
                <goal>enforce</goal>
              </goals>
              <configuration>
                <rules>
                  <requireMavenVersion>
                    <version>${required.maven.version}</version>
                  </requireMavenVersion>
                </rules>
              </configuration>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>versions-maven-plugin</artifactId>
          <version>${versions.maven.plugin.version}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${maven.compiler.plugin.version}</version>
          <configuration>
            <compilerArguments>
              <Xlint />
            </compilerArguments>
            <showWarnings>true</showWarnings>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>${maven.surefire.plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-release-plugin</artifactId>
          <version>${maven.release.plugin.version}</version>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>versions-maven-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <forkCount>0</forkCount>
          <!-- useSystemClassLoader true is the default, and that setting gives a warning if you don't fork -->
          <useSystemClassLoader>false</useSystemClassLoader>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <modules>
    <module>productmodel-codegen</module>
    <module>productmodel-codegen-gradle-executor</module>
  </modules>

  <pluginRepositories>
    <pluginRepository>
      <id>gw.releases.group</id>
      <url>https://nexus.guidewire.com/content/repositories/releases-group/</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </pluginRepository>
    <pluginRepository>
      <id>gw.snapshots.group</id>
      <url>https://nexus.guidewire.com/content/repositories/snapshots-group/</url>
      <releases>
        <enabled>false</enabled>
      </releases>
    </pluginRepository>
  </pluginRepositories>

  <repositories>
    <repository>
      <id>gw.thirdparty</id>
      <url>https://nexus.guidewire.com/content/repositories/thirdparty/</url>
      <name>Guidewire Nexus repository for third party libs not in Central</name>
    </repository>
    <repository>
      <id>gw.releases.group</id>
      <url>https://nexus.guidewire.com/content/groups/releases-group/</url>
      <name>Guidewire internal releases repository</name>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
    <repository>
      <id>gw.snapshots.group</id>
      <url>https://nexus.guidewire.com/content/groups/snapshots-group/</url>
      <name>Guidewire internal snapshots repository</name>
      <releases>
        <enabled>false</enabled>
      </releases>
    </repository>
  </repositories>
</project>
