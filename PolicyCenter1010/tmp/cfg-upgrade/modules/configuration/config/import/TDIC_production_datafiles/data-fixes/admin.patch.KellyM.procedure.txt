	TESTING
	Create a backup of PC PROD, on machine 70 (or wherever you like).
	Login to 70 as Kelly
	Observe Login error on 70, so that we know we are applying a patch to a system that needs it
	Logout of 70
	Login to 70 as Mary (or any system administrator who can use the Import Data Wizard)
	Navigate to the Administration > Utilities > Import Data Wizard on 70
	Start to import the patch file onto 70, but do NOT complete it
	Verify these expected counts:
	_2_ records to be added
	_0_ records to be deleted
	_0_ records that require conflict resolution
	Complete the import on 70
	Logout of 70
	Login to 70 as Kelly
	Observe no login errors on 70
	Navigate to Desktop > My Activities, and drill into an Activity owned by Kelly.  Observe no errors
	Navigate to an Account.  Observe no errors
	Navigate to a Policy.  Observe no errors
	Navigate back to the Account using the MRU list.  Observe no errors
	Navigate back to the Policy using the MRU list.  Observe no errors
	

PROD DEPLOYMENT
	Login to PROD as Mary (or any system administrator who can use the Import Data Wizard)
	Navigate to the Administration > Utilities > Import Data Wizard on PROD
	Start to import the patch file onto PROD, but do NOT complete it
	Verify these expected counts match the counts observed from 70 
	_2_ records to be added
	_0_ records to be deleted
	_0_ records that require conflict resolution
	Complete the import on PROD
	Logout of PROD
	Login to PROD as Kelly
	Observe no login errors on PROD
	Navigate to Desktop > My Activities, and drill into an Activity owned by Kelly.  Observe no errors
	Navigate to an Account.  Observe no errors
	Navigate to a Policy.  Observe no errors
	Navigate back to the Account using the MRU list.  Observe no errors
	Navigate back to the Policy using the MRU list.  Observe no errors
