<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <!-- The BOP Personal Property Cov requires its own CoverageInputSet because the valuation method impacts the
    available choices for co-insurance. -->
  <InputSet
    id="CoverageInputSet"
    mode="BOPPersonalPropCov">
    <Require
      name="coveragePattern"
      type="gw.api.productmodel.ClausePattern"/>
    <Require
      name="coverable"
      type="Coverable"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Variable
      initialValue="coverable as BOPBuilding"
      name="building"
      type="entity.BOPBuilding"/>
    <Variable
      initialValue="getBOPPersonalPropCoinsuranceValue(building.BOPPersonalPropCov.BOPBPPValuationTerm.Value)"
      name="coinsuranceTerm"
      recalculateOnRefresh="true"
      type="productmodel.OptionBOPPersonalPropCoinType"/>
    <HiddenInput
      id="CovPatternDisplayName"
      value="coveragePattern.DisplayName"
      valueType="java.lang.String"/>
    <InputGroup
      allowToggle="coveragePattern.allowToggle(coverable)"
      alwaysShowCheckbox="true"
      childrenVisible="coverable.getCoverageConditionOrExclusion(coveragePattern) != null"
      id="CovPatternInputGroup"
      label="coveragePattern.DisplayName"
      onToggle="building.setCoverageConditionOrExclusionExists(coveragePattern, VALUE)">
      <InputSetRef
        def="CovTermDirectInputSet(building.BOPPersonalPropCov.BOPBPPBldgLimTerm,openForEdit)"
        id="BOPPersonalPropCovLimit"
        visible="building.BOPPersonalPropCov.hasCovTerm(&quot;BOPBPPBldgLim&quot;)"/>
      <RangeInput
        id="BOPPersonalPropCovValuationMethod"
        label="building.BOPPersonalPropCov.BOPBPPValuationTerm.DisplayName"
        required="building.BOPPersonalPropCov.BOPBPPValuationTerm.Pattern.Required"
        value="building.BOPPersonalPropCov.BOPBPPValuationTerm.Value"
        valueRange="building.BOPPersonalPropCov.BOPBPPValuationTerm?.Pattern.OrderedAvailableValues"
        valueType="typekey.ValuationMethod"
        visible="building.BOPPersonalPropCov.hasCovTerm(&quot;BOPBPPValuation&quot;)">
        <PostOnChange/>
      </RangeInput>
      <RangeInput
        editable="openForEdit and gw.web.productmodel.ChoiceCovTermUtil.isEditable(coinsuranceTerm)"
        id="BOPPersonalPropCovCoinsurance"
        label="building.BOPPersonalPropCov.BOPPersonalPropCoinTerm.DisplayName"
        required="building.BOPPersonalPropCov.BOPPersonalPropCoinTerm.Pattern.Required"
        sortValueRange="false"
        value="coinsuranceTerm.OptionValue"
        valueRange="gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(coinsuranceTerm, openForEdit)"
        valueType="gw.api.productmodel.CovTermOpt&lt;productmodel.OptionBOPPersonalPropCoinType&gt;"
        visible="building.BOPPersonalPropCov.hasCovTerm(&quot;BOPPersonalPropCoin&quot;) and building.BOPPersonalPropCov.BOPBPPValuationTerm.Value != ValuationMethod.TC_FUNCVALUE"/>
      <PostOnChange/>
      <RangeInput
        id="BOPBldgInflationGuard"
        label="building.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.DisplayName"
        required="building.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Pattern.Required"
        sortValueRange="false"
        value="building.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.OptionValue"
        valueRange="gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(building.BOPPersonalPropCov.BOPInflationGuard_TDICTerm, openForEdit)"
        valueType="gw.api.productmodel.CovTermOpt&lt;productmodel.OptionBOPInflationGuard_TDICType&gt;"/>
    </InputGroup>
    <Code><![CDATA[uses gw.api.productmodel.ClausePatternLookup
uses gw.api.productmodel.CovTermOpt

function getBOPBuildingCoinsuranceValue(valMethodValue : gw.entity.TypeKey) : OptionBOPBuildingCoinType {
  if (valMethodValue == ValuationMethod.TC_FUNCVALUE) { 
    var optionValue = building.BOPBuildingCov.BOPBuildingCoinTerm.Pattern.getCovTermOpt("0")
    building.BOPBuildingCov.BOPBuildingCoinTerm.setOptionValue(optionValue)
  }
  return building.BOPBuildingCov.BOPBuildingCoinTerm
}

function getBOPPersonalPropCoinsuranceValue(valMethodValue : gw.entity.TypeKey) : OptionBOPPersonalPropCoinType {
  if (valMethodValue == ValuationMethod.TC_FUNCVALUE) { 
    var optionValue = building.BOPPersonalPropCov.BOPPersonalPropCoinTerm.Pattern.getCovTermOpt("0")
    building.BOPPersonalPropCov.BOPPersonalPropCoinTerm.setOptionValue(optionValue)
  }
  return building.BOPPersonalPropCov.BOPPersonalPropCoinTerm
}

function setCoverageExistence(build : BOPBuilding){
  var pattern1 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("BOPNewBPP_TDIC")
  var pattern2 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("BOPPollCleanRemoval_TDIC")
  var pattern3 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("BOPFireExtRecharge_TDIC")
  var pattern4 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("BOPFireDepSerCharge_TDIC")
  var pattern5 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("BOPBPPDebrisRemoval_TDIC")
  if(build.BOPPersonalPropCovExists){
    build.setCoverageExists(pattern1,true)
    build.setCoverageExists(pattern2,true)
    build.setCoverageExists(pattern3,true)
    build.setCoverageExists(pattern4,true)
  }
}]]></Code>
  </InputSet>
</PCF>