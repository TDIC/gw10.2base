<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../app-px/pcf.xsd">
  <Screen
    id="UploadDocumentScreen">
    <Require
      name="DocumentApplicationContext"
      type="gw.document.DocumentApplicationContext"/>
    <Variable
      initialValue="null"
      name="UploadedWebFiles"
      type="java.util.Collection&lt;gw.api.web.WebFile&gt;"/>
    <Variable
      initialValue="new java.util.ArrayList&lt;gw.document.DocumentCreationInfo&gt;()"
      name="DocumentCreationInfos"
      type="java.util.Collection&lt;gw.document.DocumentCreationInfo&gt;"/>
    <!-- Force the evaluation of the dropzone flag to be evaluated when rendering
         by using a recalculateOnRefresh variable. This is necessary to pick up
          any iterator updates on the request. -->
    <Variable
      initialValue="enableDefaultDropzone()"
      name="enableDefaultDropzoneFlag"
      recalculateOnRefresh="true"
      type="boolean"/>
    <Variable
      initialValue="new acc.onbase.util.MultiUploadGuard(DocumentCreationInfos)"
      name="MultiUploadGuard"
      type="acc.onbase.util.MultiUploadGuard"/>
    <Toolbar>
      <ToolbarButton
        action="MultiUploadGuard.commit(CurrentLocation)"
        available="DocumentCreationInfos.Count &gt; 0"
        id="CustomUpdate"
        label="DisplayKey.get(&quot;Web.DocumentsLV.Button.Update&quot;)"/>
      <EditButtons
        editVisible="false"
        updateVisible="false"/>
      <ToolbarDivider/>
      <FileInput
        buttonText="DisplayKey.get(&quot;Web.DocumentDetails.DocumentContents.AddFiles&quot;)"
        dropzoneWidgets="UploadDocumentScreen"
        editable="true"
        id="FileInput"
        multiple="true"
        value="UploadedWebFiles"
        valueDisplayed="false"
        valueWidgetType="FileValue">
        <PostOnChange
          onChange="addDocumentCreationInfos()"/>
      </FileInput>
    </Toolbar>
    <AlertBar
      id="warningMessage"
      label="DisplayKey.get(&quot;Web.DocumentUpload.Screen.Warning&quot;)"
      visible="DocumentCreationInfos.hasMatch(\elt -&gt; elt.File.Size &gt; 10000000)"/>
    <PanelSet>
      <Verbatim
        id="DefaultDropzone"
        label="DisplayKey.get(&quot;Web.DocumentDetails.DocumentContents.DropzoneLabel&quot;)"
        labelStyleClass="g-dropzone"
        visible="enableDefaultDropzoneFlag"/>
      <PanelRef
        def="DocumentDetailsEditLVPanelSet(DocumentApplicationContext,DocumentCreationInfos)"
        visible="DocumentCreationInfos.Count &gt; 0">
        <Toolbar
          id="DocumentDetailsEditLVPanelToolbar">
          <RemoveButton
            id="RemoveDocumentCreationInfo"
            iterator="DocumentDetailsEditLVPanelSet.DocumentCreationInfoIterator"
            label="DisplayKey.get(&quot;Web.DocumentsLV.Button.Remove&quot;)"/>
          <CheckedValuesToolbarButton
            allCheckedRowsAction="BulkEditDocumentDetailsPopup.push(DocumentApplicationContext.createDocumentDetailsHelper(CheckedValues*.Document))"
            id="EditDocumentCreationInfo"
            iterator="DocumentDetailsEditLVPanelSet.DocumentCreationInfoIterator"
            label="DisplayKey.get(&quot;Web.DocumentDetails.DocumentContents.EditDetails&quot;)"
            showConfirmMessage="false"/>
        </Toolbar>
      </PanelRef>
    </PanelSet>
    <Code><![CDATA[function addDocumentCreationInfos() : gw.document.DocumentCreationInfo[] {
  var newDocumentCreationInfos = new java.util.ArrayList<gw.document.DocumentCreationInfo>()
  if (UploadedWebFiles != null) {
    var iter = UploadedWebFiles.iterator();
    while (iter.hasNext()) {
      var newDocumentCreationInfo = DocumentApplicationContext.createDocumentCreationInfo();
      newDocumentCreationInfo.File = iter.next()
      //GINTEG-156 : Defaulting 'Section' to 'Claims Documents'
      newDocumentCreationInfo.Document.Section = DocumentSection.TC_CLAIMS_TDIC
      newDocumentCreationInfo.Document.Section = DocumentSection.TC_POLICYDOCUMENTS_TDIC
      //GINTEG-445 : Defaulting Security type to 'Internal Only'
      newDocumentCreationInfo.Document.SecurityType = DocumentSecurityType.TC_INTERNALONLY
      newDocumentCreationInfo.Document.Type = DocumentType.TC_POL_RECEIVED_EXTERNAL_SOURCE
          iter.remove()
      newDocumentCreationInfos.add(newDocumentCreationInfo)
    }
  }

  DocumentCreationInfos.addAll(newDocumentCreationInfos)
  return newDocumentCreationInfos.toTypedArray()
}

function enableDefaultDropzone() : boolean {
  if (DocumentCreationInfos.Count == 0)
    return true
  gw.api.util.LocationUtil.addRequestScopedInfoMessage(DisplayKey.get("Web.DocumentDetails.DocumentContents.DropzoneAlert"))
  return false
}]]></Code>
  </Screen>
</PCF>