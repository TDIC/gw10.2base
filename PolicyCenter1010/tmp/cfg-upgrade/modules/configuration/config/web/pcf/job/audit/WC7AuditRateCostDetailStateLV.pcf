<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <ListViewPanel
    id="WC7AuditRateCostDetailStateLV">
    <Require
      name="isPremiumReport"
      type="boolean"/>
    <Require
      name="stateCosts"
      type="java.util.Set&lt;WC7Cost&gt;"/>
    <Require
      name="basedOnStateCosts"
      type="java.util.Set&lt;WC7Cost&gt;"/>
    <Require
      name="jurisdiction"
      type="WC7Jurisdiction"/>
    <Variable
      initialValue="stateCosts.byCalcOrder_TDIC(401, 1000000, WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM)//stateCosts.byCalcOrder(401, 1000000)"
      name="stateAggCosts"
      recalculateOnRefresh="true"
      type="java.util.Set&lt;entity.WC7Cost&gt;"/>
    <Variable
      initialValue="basedOnStateCosts.byCalcOrder_TDIC(401, 1000000, WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM)//basedOnStateCosts.byCalcOrder(401, 1000000)"
      name="basedOnAggCosts"
      recalculateOnRefresh="true"
      type="java.util.Set&lt;entity.WC7Cost&gt;"/>
    <Variable
      initialValue="getAllCosts( stateAggCosts, basedOnAggCosts )"
      name="allCosts"
      recalculateOnRefresh="true"
      type="java.util.Set&lt;entity.WC7Cost&gt;"/>
    <Variable
      initialValue="stateAggCosts.partitionUniquely( \ c -&gt; c.CostKey )"
      name="stateCostMap"
      recalculateOnRefresh="true"
      type="java.util.Map&lt;gw.api.domain.financials.CostKey,entity.WC7Cost&gt;"/>
    <Variable
      initialValue="basedOnAggCosts.partitionUniquely( \ c -&gt; c.CostKey )"
      name="basedOnCostMap"
      recalculateOnRefresh="true"
      type="java.util.Map&lt;gw.api.domain.financials.CostKey,entity.WC7Cost&gt;"/>
    <Variable
      initialValue="jurisdiction.Branch.PreferredSettlementCurrency"
      name="currency"
      type="typekey.Currency"/>
    <Variable
      name="wc7RatingUtil"
      type="tdic.pc.config.rating.wc7.WC7RatingUtil"/>
    <Row
      renderAsSmartHeader="true">
      <TextCell
        id="LocationNum"
        value="DisplayKey.get(&quot;Web.SubmissionWizard.Quote.WC.Loc&quot;)"
        valueType="java.lang.String"/>
      <TextCell
        id="ClassCode"
        value="DisplayKey.get(&quot;Web.Quote.CumulDetail.Default.ClassCode&quot;)"
        valueType="java.lang.String"/>
      <TextCell
        id="Description"
        value="DisplayKey.get(&quot;Web.SubmissionWizard.Quote.WC.Desc&quot;)"
        valueType="java.lang.String"/>
      <TextCell
        id="EstBasis"
        value="basisLabel()"
        valueType="java.lang.String"
        visible="not isPremiumReport"/>
      <TextCell
        id="Basis"
        value="isPremiumReport ? DisplayKey.get(&quot;Web.AuditWizard.Basis&quot;) : DisplayKey.get(&quot;Web.AuditWizard.AuditedBasis&quot;)"
        valueType="java.lang.String"/>
      <TextCell
        id="Rate"
        value="DisplayKey.get(&quot;Web.SubmissionWizard.Quote.WC.Rate&quot;)"
        valueType="java.lang.String"
        visible="not isPremiumReport"/>
      <TextCell
        id="EstPremium"
        value="premiumLabel()//amountLabel()"
        valueType="java.lang.String"
        visible="not isPremiumReport"/>
      <TextCell
        __disabled="true"
        id="AuditedRate"
        value="isPremiumReport ? DisplayKey.get(&quot;Web.AuditWizard.Premiums.Details.Rate&quot;) : DisplayKey.get(&quot;Web.AuditWizard.Premiums.Details.AuditedRate&quot;)"
        valueType="java.lang.String"/>
      <TextCell
        id="Amount"
        value="isPremiumReport ? DisplayKey.get(&quot;Web.AuditWizard.Premiums.Details.Premium.Premium&quot;) : DisplayKey.get(&quot;Web.AuditWizard.Premiums.Details.Premium.Audited&quot;)"
        valueType="java.lang.String"/>
      <TextCell
        id="Difference"
        value="DisplayKey.get(&quot;Web.AuditWizard.Premiums.Details.Change&quot;)"
        valueType="java.lang.String"
        visible="not isPremiumReport"/>
      <TextCell
        __disabled="true"
        id="Step_TDIC"
        value="&quot;Step&quot;"
        visible="(not isPremiumReport) and perm.System.viewratingstep_TDIC"/>
    </Row>
    <RowIterator
      editable="false"
      elementName="cost"
      id="f400t500"
      pageSize="0"
      type="WC7JurisdictionCost"
      value="allCosts.byCalcOrder_TDIC( 401, 500, WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).toTypedArray()"
      valueType="entity.WC7Cost[]">
      <Variable
        initialValue="stateCostMap.get( cost.CostKey )"
        name="aggCost"
        recalculateOnRefresh="true"
        type="entity.WC7Cost"/>
      <Variable
        initialValue="basedOnAggCosts.toList().firstWhere( \ b -&gt; wc7RatingUtil.getCalcOrder_TDIC(b) == aggCost.CalcOrder)//basedOnCostMap.get( cost.CostKey )"
        name="basedOnAggCost"
        recalculateOnRefresh="true"
        type="entity.WC7Cost"/>
      <Variable
        initialValue="aggCost == null ? java.math.BigDecimal.ZERO.ofCurrency(cost.Branch.PreferredSettlementCurrency) : aggCost.ActualTermAmountBilling"
        name="aggCostTermAmount"
        recalculateOnRefresh="true"
        type="gw.pl.currency.MonetaryAmount"/>
      <Variable
        initialValue="basedOnAggCost == null ? java.math.BigDecimal.ZERO.ofCurrency(cost.Branch.PreferredSettlementCurrency) : basedOnAggCost.ActualTermAmountBilling"
        name="basedOnTermAmt"
        type="gw.pl.currency.MonetaryAmount"/>
      <IteratorSort
        sortBy="cost.CalcOrder"
        sortOrder="1"/>
      <Row
        id="AggCostRow">
        <TextCell
          id="empty"
          valueType="null"/>
        <TextCell
          id="ClassCode"
          value="cost.ClassCode"
          valueType="java.lang.String"/>
        <TextCell
          id="Description"
          value="cost.Description"
          valueType="java.lang.String"/>
        <TextCell
          align="right"
          id="EstBasis"
          value="basedOnAggCost == null or basedOnAggCost.Basis == 0 ? &quot;&quot; : basedOnAggCost.Basis.DisplayValue"
          valueType="java.lang.String"
          visible="not isPremiumReport"/>
        <TextCell
          align="right"
          id="Basis"
          value="aggCost == null or aggCost.Basis == 0 ? &quot;&quot; : aggCost.Basis.DisplayValue"
          valueType="java.lang.String"/>
        <TextCell
          align="right"
          id="Rate"
          value="basedOnAggCost == null or basedOnAggCost.ActualAdjRate == 0 ? &quot;&quot; : basedOnAggCost.ActualAdjRate as String"
          valueType="java.lang.String"
          visible="not isPremiumReport"/>
        <MonetaryAmountCell
          align="right"
          formatType="currency"
          id="EstPremium"
          value="basedOnTermAmt"
          visible="not isPremiumReport"/>
        <TextCell
          __disabled="true"
          align="right"
          id="AuditedRateValue"
          value="aggCost == null or aggCost.ActualAdjRate == 0 ? &quot;&quot; : aggCost.ActualAdjRate as String"
          valueType="java.lang.String"/>
        <MonetaryAmountCell
          align="right"
          formatType="currency"
          id="TermAmount"
          value="aggCostTermAmount"/>
        <MonetaryAmountCell
          align="right"
          formatType="currency"
          id="Difference"
          value="aggCostTermAmount - basedOnTermAmt"
          visible="not isPremiumReport"/>
        <TextCell
          __disabled="true"
          align="right"
          id="Step_TDIC"
          value="aggCost.CalcOrder"
          valueType="Integer"
          visible="(not isPremiumReport) and perm.System.viewratingstep_TDIC"/>
      </Row>
    </RowIterator>
    <Row>
      <TextCell
        id="Empty1"
        valueType="null"/>
      <TextCell
        id="Empty2"
        valueType="null"/>
      <TextCell
        bold="true"
        id="DescriptionFoot400"
        value="DisplayKey.get(&quot;TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.EstimatedAnnualPremium&quot;)"
        valueType="java.lang.String"/>
      <TextCell
        id="Empty6"
        valueType="null"
        visible="not isPremiumReport"/>
      <TextCell
        id="Empty9"
        valueType="null"/>
      <TextCell
        id="Empty3"
        valueType="null"
        visible="not isPremiumReport"/>
      <MonetaryAmountCell
        align="right"
        formatType="currency"
        id="EstTotalPremium"
        value="basedOnStateCosts.byCalcOrder_TDIC(0,501,WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).AmountSum(currency)//basedOnStateCosts.where( \ w -&gt; w.CalcOrder &lt; 501 ).AmountSum(currency)"
        valueType="gw.pl.currency.MonetaryAmount"
        visible="not isPremiumReport"/>
      <TextCell
        __disabled="true"
        id="Empty8"
        valueType="null"/>
      <MonetaryAmountCell
        align="right"
        bold="true"
        formatType="currency"
        id="AmountSubtotal400"
        value="stateCosts.byCalcOrder_TDIC(0,501,WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).AmountSum(currency)//stateCosts.where( \ w -&gt; w.CalcOrder &lt; 501 ).AmountSum(currency)"/>
      <MonetaryAmountCell
        align="right"
        formatType="currency"
        id="PremiumDifference"
        value="stateCosts.byCalcOrder_TDIC(0,501,WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).AmountSum(currency) - basedOnStateCosts.byCalcOrder_TDIC(0,501,WC7PremiumLevelType.TC_ESTIMATEDANNUALPREMIUM).AmountSum(currency)//stateCosts.where( \ w -&gt; w.CalcOrder &lt; 501 ).AmountSum(currency) - basedOnStateCosts.where( \ w -&gt; w.CalcOrder &lt; 501 ).AmountSum(currency)"
        visible="not isPremiumReport"/>
      <TextCell
        __disabled="true"
        align="right"
        id="Step2_TDIC"
        visible="(not isPremiumReport) and perm.System.viewratingstep_TDIC"/>
    </Row>
    <RowIterator
      __disabled="true"
      editable="false"
      elementName="cost"
      id="gt500"
      pageSize="0"
      type="WC7JurisdictionCost"
      value="allCosts.byCalcOrder( 501, 1000000 ).toTypedArray()"
      valueType="entity.WC7Cost[]">
      <Variable
        initialValue="stateCostMap.get( cost.CostKey )"
        name="aggCost"
        recalculateOnRefresh="true"
        type="entity.WC7Cost"/>
      <Variable
        initialValue="basedOnCostMap.get( cost.CostKey )"
        name="basedOnAggCost"
        recalculateOnRefresh="true"
        type="entity.WC7Cost"/>
      <Variable
        initialValue="aggCost == null ? java.math.BigDecimal.ZERO.ofCurrency(cost.Branch.PreferredSettlementCurrency) : aggCost.ActualTermAmountBilling"
        name="aggCostTermAmount"
        recalculateOnRefresh="true"
        type="gw.pl.currency.MonetaryAmount"/>
      <Variable
        initialValue="basedOnAggCost == null ? java.math.BigDecimal.ZERO.ofCurrency(cost.Branch.PreferredSettlementCurrency)  : basedOnAggCost.ActualTermAmountBilling"
        name="basedOnTermAmt"
        type="gw.pl.currency.MonetaryAmount"/>
      <IteratorSort
        sortBy="cost.CalcOrder"
        sortOrder="1"/>
      <Row
        id="AggCostRow">
        <TextCell
          id="empty"
          valueType="null"/>
        <TextCell
          id="ClassCode"
          value="cost.ClassCode"
          valueType="java.lang.String"/>
        <TextCell
          id="Description"
          value="cost.Description"
          valueType="java.lang.String"/>
        <TextCell
          align="right"
          id="EstBasis"
          value="basedOnAggCost == null or basedOnAggCost.Basis == 0 ? &quot;&quot; : basedOnAggCost.Basis.DisplayValue"
          valueType="java.lang.String"
          visible="not isPremiumReport"/>
        <TextCell
          align="right"
          id="Basis"
          value="aggCost == null or aggCost.Basis == 0 ? &quot;&quot; : aggCost.Basis.DisplayValue"
          valueType="java.lang.String"/>
        <TextCell
          align="right"
          id="Rate"
          value="basedOnAggCost == null or basedOnAggCost.ActualAdjRate == 0 ? &quot;&quot; : basedOnAggCost.ActualAdjRate as String"
          valueType="java.lang.String"
          visible="not isPremiumReport"/>
        <MonetaryAmountCell
          align="right"
          formatType="currency"
          id="EstPremium"
          value="basedOnTermAmt"
          visible="not isPremiumReport"/>
        <TextCell
          __disabled="true"
          align="right"
          id="AuditedRateValue1"
          value="aggCost == null or aggCost.ActualAdjRate == 0 ? &quot;&quot; : aggCost.ActualAdjRate as String"
          valueType="java.lang.String"/>
        <MonetaryAmountCell
          align="right"
          formatType="currency"
          id="TermAmount"
          value="aggCostTermAmount"/>
        <MonetaryAmountCell
          align="right"
          formatType="currency"
          id="Difference"
          value="aggCostTermAmount - basedOnTermAmt"
          visible="not isPremiumReport"/>
        <TextCell
          __disabled="true"
          align="right"
          id="Step3_TDIC"
          value="aggCost.CalcOrder"
          valueType="Integer"
          visible="(not isPremiumReport) and perm.System.viewratingstep_TDIC"/>
      </Row>
    </RowIterator>
    <Row>
      <TextCell
        id="Empty13"
        valueType="null"/>
      <TextCell
        id="Empty14"
        valueType="null"/>
      <TextCell
        bold="true"
        id="DescriptionFoota500"
        value="DisplayKey.get(&quot;TDIC.Web.SubmissionWizard.Quote.WC7.Subtotal.TotalAmountDue&quot;)"
        valueType="java.lang.String"/>
      <TextCell
        id="Empty20"
        valueType="null"
        visible="not isPremiumReport"/>
      <TextCell
        id="Empty23"
        valueType="null"/>
      <TextCell
        id="Empty17"
        valueType="null"
        visible="not isPremiumReport"/>
      <MonetaryAmountCell
        align="right"
        formatType="currency"
        id="EstSubtotal"
        value="basedOnStateCosts.AmountSum(currency)"
        visible="not isPremiumReport"/>
      <TextCell
        __disabled="true"
        id="Empty22"
        valueType="null"/>
      <MonetaryAmountCell
        align="right"
        bold="true"
        formatType="currency"
        id="CumulAmountSubtotal500"
        value="stateCosts.AmountSum(currency)"/>
      <MonetaryAmountCell
        align="right"
        formatType="currency"
        id="SubtotalDifference"
        value="stateCosts.AmountSum(currency) - basedOnStateCosts.AmountSum(currency)"
        visible="not isPremiumReport"/>
      <TextCell
        __disabled="true"
        align="right"
        id="Step4_TDIC"
        visible="(not isPremiumReport) and perm.System.viewratingstep_TDIC"/>
    </Row>
    <Code><![CDATA[function basisLabel() : String {
  return (isRevisedAudit() ?
    DisplayKey.get("Web.AuditWizard.PriorAuditedBasis") :
    DisplayKey.get("Web.AuditWizard.EstBasis"))
}

function amountLabel() : String {
  return (isRevisedAudit() ?
    DisplayKey.get("Web.AuditWizard.PriorAuditedAmount") :
    DisplayKey.get("Web.AuditWizard.EstAmount"))
}
function premiumLabel() : String {
  return (isRevisedAudit() ?
      DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.PriorAudited") :
      DisplayKey.get("Web.AuditWizard.Premiums.Details.Premium.Estimated"))
}
private function isRevisedAudit() : boolean {
  return jurisdiction.Branch.Audit.AuditInformation.IsRevision
}

private function getAllCosts( curCosts : java.util.Set<WC7Cost>, priorCosts : java.util.Set<WC7Cost> ) : java.util.Set<WC7Cost>
{
  var curCostKeys = curCosts.map( \ curCost -> curCost.CostKey )
  var missingPriorCosts = priorCosts.where( \ priorCost -> not curCostKeys.contains( priorCost.CostKey ) ).toSet()
  return curCosts.union( missingPriorCosts )
}]]></Code>
  </ListViewPanel>
</PCF>