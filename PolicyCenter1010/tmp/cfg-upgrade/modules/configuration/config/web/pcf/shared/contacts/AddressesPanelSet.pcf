<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <PanelSet
    available="period.profileChange_TDIC"
    desc="Provides a reusable panel displaying a Contact&apos;s Addresses in a ListDetailPanel."
    id="AddressesPanelSet">
    <Require
      name="contact"
      type="Contact"/>
    <Require
      name="showAddressTools"
      type="boolean"/>
    <Require
      name="account"
      type="entity.Account"/>
    <Require
      name="period"
      type="entity.PolicyPeriod"/>
    <Variable
      initialValue="gw.util.concurrent.LocklessLazyVar.make(\ -&gt; period.OpenForEdit)"
      name="openForEditInit"
      recalculateOnRefresh="true"
      type="gw.util.concurrent.LocklessLazyVar&lt;java.lang.Boolean&gt;"/>
    <Variable
      initialValue="period != null ? openForEditInit.get() : CurrentLocation.InEditMode"
      name="openForEdit"
      recalculateOnRefresh="true"
      type="java.lang.Boolean"/>
    <Variable
      initialValue="true"
      name="standardizeVisible"
      type="Boolean"/>
    <ListDetailPanel
      selectionName="selectedAddress"
      selectionType="Address">
      <Variable
        initialValue="new tdic.pc.config.addressverification.AddressVerificationHelper()"
        name="helper"
        type="tdic.pc.config.addressverification.AddressVerificationHelper"/>
      <Variable
        name="addressList"
        type="java.util.ArrayList&lt;Address&gt;"/>
      <Variable
        name="standardizedSelectedAddress"
        type="Address"/>
      <PanelRef
        available="period?.profileChange_TDIC">
        <TitleBar
          title="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail&quot;)"/>
        <Toolbar>
          <ToolbarButton
            action="addressList = helper.validateAddress(selectedAddress, false); standardizedSelectedAddress = selectedAddress; helper.displayExceptions()"
            available="standardizeVisible and openForEdit"
            id="Standardize"
            label="DisplayKey.get(&quot;Web.AccountFile.Locations.Standardize&quot;)"
            visible="standardizeVisible and openForEdit"/>
          <IteratorButtons
            iterator="AddressesLV"/>
        </Toolbar>
        <ListViewPanel
          desc="List of addresses"
          id="AddressesLV">
          <RowIterator
            checkBoxVisible="address != contact.PrimaryAddress &amp;&amp; address != period?.PolicyAddress.Address"
            editable="true"
            elementName="address"
            hasCheckBoxes="true"
            hideCheckBoxesIfReadOnly="true"
            pageSize="4"
            toAdd="contact.addAddress(address)"
            toRemove="if (address != contact.PrimaryAddress &amp;&amp; address != period?.PolicyAddress.Address) contact.safeRemoveAddress(address) "
            value="contact.AllAddresses"
            valueType="entity.Address[]">
            <Row>
              <RadioButtonCell
                action="contact.makePrimaryAddress(address)"
                align="center"
                editable="true"
                id="Primary"
                label="DisplayKey.get(&quot;Web.Addresses.Primary&quot;)"
                radioButtonGroup="PrimaryAddress"
                value="address == contact.PrimaryAddress">
                <PostOnChange/>
              </RadioButtonCell>
              <TypeKeyCell
                id="AddressType"
                label="DisplayKey.get(&quot;Web.Addresses.AddressType&quot;)"
                value="address.AddressType"
                valueType="typekey.AddressType"/>
              <TextCell
                id="DisplayedName"
                label="DisplayKey.get(&quot;Web.Addresses.Address&quot;)"
                value="address.addressString(&quot;,&quot;, false, false)"
                wrap="true"/>
              <TextCell
                desc="20150515 TJ Talluto: US1296 Discretionary Synchronizing"
                id="Description_TDIC"
                label="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail.Description&quot;)"
                value="address.Description"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </PanelRef>
      <CardViewPanel>
        <Card
          id="AddressDetailCard"
          title="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail&quot;)">
          <DetailViewPanel
            id="AddressDetailDV">
            <InputColumn>
              <Label
                label="DisplayKey.get(&quot;Web.Policy.Address.SelectionIsPolicyAddress&quot;)"
                visible="selectedAddress == period?.PolicyAddress.Address"/>
              <InputSetRef
                def="LinkedAddressInputSet(selectedAddress, contact, period, account, openForEdit)"/>
              <InputSetRef
                def="AddressInputSet(new gw.pcf.contacts.AddressInputSetAddressOwner(selectedAddress, false, true))"
                editable="selectedAddress.LinkedAddress == null"/>
              <TypeKeyInput
                __disabled="true"
                editable="selectedAddress.LinkedAddress == null"
                id="AddressType"
                label="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail.AddressType&quot;)"
                required="true"
                value="selectedAddress.AddressType"
                valueType="typekey.AddressType"/>
              <InputSetRef
                def="TDIC_AccountAddressTypeInputSet(selectedAddress)"
                id="AddressTypeRef_TDIC"/>
              <TextInput
                editable="selectedAddress.LinkedAddress == null"
                id="Description"
                label="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail.Description&quot;)"
                value="selectedAddress.Description"/>
              <DateInput
                __disabled="true"
                editable="selectedAddress.LinkedAddress == null"
                id="ValidUntil"
                label="DisplayKey.get(&quot;Web.ContactDetail.AddressDetail.ValidUntil&quot;)"
                value="selectedAddress.ValidUntil"/>
            </InputColumn>
          </DetailViewPanel>
          <PanelRef
            hideIfReadOnly="true"
            visible="(addressList ?.Count &gt; 0 ? true : false) and openForEdit">
            <TitleBar
              id="SelectTitle"
              title="DisplayKey.get(&quot;Web.AccountFile.Locations.Suggested&quot;)"/>
            <Toolbar/>
            <ListViewPanel>
              <RowIterator
                editable="false"
                elementName="suggestedAddress"
                id="melissaReturn"
                value="addressList.toTypedArray()"
                valueType="entity.Address[]">
                <Row>
                  <LinkCell
                    label="DisplayKey.get(&quot;Web.AccountFile.Locations.Select&quot;)">
                    <Link
                      action="helper.replaceAddressWithSuggested(suggestedAddress, standardizedSelectedAddress, false); addressList = null"
                      id="SelectAddress"
                      label="DisplayKey.get(&quot;Web.AccountFile.Locations.Select&quot;)"
                      styleClass="miniButton"/>
                  </LinkCell>
                  <TextCell
                    id="AddressLine1"
                    label="DisplayKey.get(&quot;Web.AccountFile.Locations.AddressLine1&quot;)"
                    value="suggestedAddress.AddressLine1"/>
                  <TextCell
                    id="AddressLine2"
                    label="DisplayKey.get(&quot;Web.AccountFile.Locations.AddressLine2&quot;)"
                    value="suggestedAddress.AddressLine2"/>
                  <TextCell
                    id="city"
                    label="DisplayKey.get(&quot;Web.AccountFile.Locations.City&quot;)"
                    value="suggestedAddress.City"/>
                  <TextCell
                    id="State"
                    label="DisplayKey.get(&quot;Web.AccountFile.Locations.State&quot;)"
                    value="suggestedAddress.State"
                    valueType="typekey.State"/>
                  <TextCell
                    id="zip"
                    label="DisplayKey.get(&quot;Web.AccountFile.Locations.PostalCode&quot;)"
                    value="suggestedAddress.PostalCode"/>
                </Row>
              </RowIterator>
            </ListViewPanel>
          </PanelRef>
        </Card>
        <Card
          id="AddressToolsCard"
          title="DisplayKey.get(&quot;Web.ContactDetail.AddressTools&quot;)"
          visible="showAddressTools and contact.PrimaryAddress != selectedAddress and not selectedAddress.New">
          <PanelRef
            id="AddressTools">
            <TitleBar
              title="DisplayKey.get(&quot;Web.ContactDetail.AddressTools.MergeAddress&quot;)"/>
            <Toolbar>
              <ToolbarButton
                available="showAddressTools and contact.PrimaryAddress != selectedAddress and not selectedAddress.New"
                id="MergeAddresses"
                label="DisplayKey.get(&quot;Web.ContactDetail.AddressTools.MergeAddressInto&quot;)">
                <MenuItemIterator
                  elementName="survivorAddress"
                  value="contact.AllAddresses.subtract({selectedAddress}).where(\ a -&gt; not a.New).toTypedArray()"
                  valueType="entity.Address[]">
                  <MenuItem
                    action="contact.mergeAddresses(survivorAddress, selectedAddress);gw.api.web.PebblesUtil.invalidateIterators(CurrentLocation, entity.Address)"
                    confirmMessage="DisplayKey.get(&quot;Web.ContactDetail.AddressTools.MergeAddress.Confirm&quot;, survivorAddress, selectedAddress)"
                    id="survivorAddress"
                    label="survivorAddress.DisplayName"/>
                </MenuItemIterator>
              </ToolbarButton>
            </Toolbar>
            <PanelRow/>
          </PanelRef>
        </Card>
      </CardViewPanel>
    </ListDetailPanel>
  </PanelSet>
</PCF>