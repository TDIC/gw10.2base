<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <InputSet
    id="CovTermInputSet"
    mode="datetime">
    <Require
      name="term"
      type="gw.api.domain.covterm.CovTerm"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Require
      name="coverable"
      type="Coverable"/>
    <Variable
      initialValue="coverable.PolicyLine.Branch"
      name="Period"
      type="PolicyPeriod"/>
    <DateInput
      editable="isRetroDatetimeFieldsEditable() //term.Pattern.CodeIdentifier == &quot;GLNTCExpDate_TDIC&quot; ? false : true"
      id="DateTimeTermInput"
      label="term.Pattern.DisplayName"
      requestValidationExpression="Period.isDiscountEffDateValid(VALUE)"
      required="!(term.Pattern.CodeIdentifier == &quot;GLPriorActsRetroDate_TDIC&quot; and (Period.Job.Subtype == typekey.Job.TC_SUBMISSION and Period.Status == PolicyPeriodStatus.TC_DRAFT)) and !(term.Pattern.CodeIdentifier == &quot;GLDEPLRetroDate_TDIC&quot;)"
      value="(term as gw.api.domain.covterm.GenericCovTerm&lt;java.util.Date&gt;).Value"
      visible="dateVisibility()"/>
    <DateInput
      id="EREDPLExpDate"
      label="term.Pattern.DisplayName"
      value="(term as gw.api.domain.covterm.GenericCovTerm&lt;java.util.Date&gt;).Value"
      visible="term.Pattern.CodeIdentifier == &quot;GLERPDLExpDate_TDIC&quot; or term.Pattern.CodeIdentifier == &quot;GLCybEREExpirationDate_TDIC&quot;"/>
    <TextInput
      id="EREDPLExpirationDate"
      label="term.Pattern.DisplayName"
      value="DisplayKey.get(&quot;TDIC.Web.Policy.EREDPLExpDate&quot;)"
      visible="term.Pattern.CodeIdentifier == &quot;GLERPEExpirationDate_TDIC&quot;"/>
    <DateInput
      available="(term.Pattern.CodeIdentifier == &quot;GLPriorActsRetroDate_TDIC&quot; and (Period.Job.Subtype == typekey.Job.TC_SUBMISSION and Period.Status == PolicyPeriodStatus.TC_DRAFT))"
      editable="isRetroDatetimeFieldsEditable()"
      id="DateTimeTermInput_RetroActive"
      label="term.Pattern.DisplayName"
      requestValidationExpression="validateRetroDateforPriorActs(VALUE)"
      required="true"
      value="(term as gw.api.domain.covterm.GenericCovTerm&lt;java.util.Date&gt;).Value"
      visible="(term.Pattern.CodeIdentifier == &quot;GLPriorActsRetroDate_TDIC&quot; and (Period.Job.Subtype == typekey.Job.TC_SUBMISSION and Period.Status == PolicyPeriodStatus.TC_DRAFT))">
      <PostOnChange/>
    </DateInput>
    <DateInput
      available="term.Pattern.CodeIdentifier == &quot;GLDEPLRetroDate_TDIC&quot;"
      editable="isRetroDatetimeFieldsEditable()"
      id="DateTimeTermInput_RetroActive1"
      label="term.Pattern.DisplayName"
      required="true"
      value="(term as gw.api.domain.covterm.GenericCovTerm&lt;java.util.Date&gt;).Value"
      visible="(term.Pattern.CodeIdentifier == &quot;GLDEPLRetroDate_TDIC&quot;)">
      <PostOnChange/>
    </DateInput>
    <Code><![CDATA[uses gw.api.util.DateUtil
uses typekey.Job

function isRetroDatetimeFieldsEditable(): boolean {
  var ereAdditionalCoveragesEffExpDates = {"GLCybEREEffectiveDate_TDIC", "GLCybEREExpirationDate_TDIC", "GLERPDLEffDate_TDIC",
      "GLERPDLExpDate_TDIC", "GLERPEEffectiveDate_TDIC", "GLERPEExpirationDate_TDIC"}
  if(ereAdditionalCoveragesEffExpDates.contains(term.Pattern.CodeIdentifier)) {
    return false
  }
  if (term.Pattern.CodeIdentifier == "GLNTCExpDate_TDIC") { // Original condition in the field of Editible property
    return false
  }

  var period = coverable.PolicyLine.Branch

  if (term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC") {
    if (period.GLLineExists) {
      if ((period.Job.Subtype == typekey.Job.TC_SUBMISSION and period.Status == PolicyPeriodStatus.TC_DRAFT) or period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        return true
      } else if (period.Job.Subtype == typekey.Job.TC_RENEWAL and period.Status == PolicyPeriodStatus.TC_DRAFT) {
        if (term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC" and period.GLLine.BasedOn?.GLDentalEmpPracLiabCov_TDICExists) {
          return false
        }
      }
    }
  }
  // Defect, GPC-3486: BR-034 - Added Requiremen: Prior Acts Retroactive date shall be read-only for all transactions other than New Submission
  if (period.GLLineExists and period.GLLine.GLPriorActsCov_TDICExists) {
    if (term.Pattern.CodeIdentifier == "GLPriorActsRetroDate_TDIC") {
      if ((period.Job.Subtype == typekey.Job.TC_SUBMISSION and period.Status == PolicyPeriodStatus.TC_DRAFT)) {
        return true
      } else { //if (period.Job.Subtype == typekey.Job.TC_RENEWAL and period.Status == PolicyPeriodStatus.TC_DRAFT) {
        return false
      }
    }
  }

  return true
}
/*
function validateRetroDateforDEPL(retroDate : Date) : String {
  if (Period.GLLineExists) {
    var TransEffDate = Period.EditEffectiveDate
    if (Period.GLLine.GLDentalEmpPracLiabCov_TDICExists and retroDate != null) {
      var _01_01_1998 = DateUtil.createDateInstance(1, 1, 1998)
      var _07_01_1998 = DateUtil.createDateInstance(7, 1, 1998)
      var _07_01_1998_States = {Jurisdiction.TC_CA, Jurisdiction.TC_AZ, Jurisdiction.TC_NV}
      
      if (_07_01_1998_States.contains(Period.BaseState) and retroDate.before(_07_01_1998)) {
        return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.BeforeDate", _07_01_1998, Period.BaseState)
      } else if (retroDate.before(_01_01_1998)) {
        return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.BeforeDate", _01_01_1998, Period.BaseState)
      }
      if (retroDate.after(TransEffDate)) {
        return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLDentalPracLiabCovTDIC.AfterDate")
      }
    }
  }
  return null
}
*/
  function validateRetroDateforPriorActs(retroDate : Date) : String{
    if(Period.GLLine.GLPriorActsCov_TDICExists and retroDate != null){
      var PolicyEffDate = Period.PeriodStart
      var _07_01_1984 = DateUtil.createDateInstance(7, 1, 1984)
      if(retroDate.before(_07_01_1984)){
        return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLPriorActsCovTDIC.BeforeDate")
      }
      if(retroDate.after(PolicyEffDate)){
        return DisplayKey.get("TDIC.Web.Policy.GLLine.CovTerm.Validation.GLPriorActsCovTDIC.AfterDate")
      }
    }
  return null
}

function dateVisibility(): boolean{
 return !(term.Pattern.CodeIdentifier == "GLERPEExpirationDate_TDIC" or term.Pattern.CodeIdentifier == "GLCybEREExpirationDate_TDIC" or 
     term.Pattern.CodeIdentifier == "GLERPDLExpDate_TDIC") and !(term.Pattern.CodeIdentifier == "GLPriorActsRetroDate_TDIC" and 
     (Period.Job.Subtype == typekey.Job.TC_SUBMISSION and Period.Status == PolicyPeriodStatus.TC_DRAFT)) and !(term.Pattern.CodeIdentifier == "GLDEPLRetroDate_TDIC")
}
]]></Code>
  </InputSet>
</PCF>