<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../../pcf.xsd">
  <InputSet
    id="CoverageInputSet"
    mode="WC7WorkersCompEmpLiabInsurancePolicyACov">
    <Require
      name="coveragePattern"
      type="gw.api.productmodel.ClausePattern"/>
    <Require
      name="coverable"
      type="Coverable"/>
    <Require
      name="openForEdit"
      type="boolean"/>
    <Variable
      initialValue="coverable as WC7WorkersCompLine"
      name="wc7line"
      type="entity.WC7WorkersCompLine"/>
    <Variable
      initialValue="wc7line.WC7WorkersCompEmpLiabInsurancePolicyACov"
      name="wc7EmpLiabCov"
      recalculateOnRefresh="true"
      type="productmodel.WC7WorkersCompEmpLiabInsurancePolicyACov"/>
    <HiddenInput
      id="CovPatternDisplayName"
      value="coveragePattern.DisplayName"
      valueType="java.lang.String"/>
    <InputGroup
      allowToggle="coveragePattern.allowToggle(coverable)"
      childrenVisible="coverable.getCoverageConditionOrExclusion(coveragePattern) != null"
      id="CovPatternInputGroup"
      label="coveragePattern.DisplayName"
      onToggle="coverable.setCoverageConditionOrExclusionExists(coveragePattern, VALUE)">
      <RangeInput
        editable="gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7EmpLiabLimitTerm)"
        id="WC7EmpLiabLimitTermInput"
        label="wc7EmpLiabCov.WC7EmpLiabLimitTerm.DisplayName"
        required="wc7EmpLiabCov.WC7EmpLiabLimitTerm.Pattern.Required"
        value="wc7EmpLiabCov.WC7EmpLiabLimitTerm.PackageValue"
        valueRange="gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitTerm))"
        valueType="gw.api.productmodel.CovTermPack&lt;productmodel.PackageWC7EmpLiabLimitType&gt;"/>
      <RangeInput
        editable="gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm)"
        id="WC7EmpLiabLimitEachAcc_TDIC"
        label="wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm.DisplayName"
        required="wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm.Pattern.Required"
        value="wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm.OptionValue"
        valueRange="gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitEachAccTDICTerm, openForEdit)"
        valueType="gw.api.productmodel.CovTermOpt&lt;productmodel.OptionWC7EmpLiabLimitEachAccTDICType&gt;">
        <PostOnChange
          deferUpdate="false"
          onChange="var issues = coverable.syncCoverages({gw.api.upgrade.PCCoercions.makeProductModel&lt;gw.api.productmodel.CoveragePattern&gt;(wc7EmpLiabCov.PatternCode)}); if(issues.HasElements) {throw new gw.api.util.DisplayableException(issues.map( \ i -&gt; i.BaseMessage).join(&quot;\n&quot;))};"/>
      </RangeInput>
      <RangeInput
        editable="gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm)"
        id="WC7EmpLiabPolicyLimitTermInput"
        label="wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm.DisplayName"
        required="wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm.Pattern.Required"
        value="wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm.OptionValue"
        valueRange="gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm, gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabPolicyLimitTerm))"
        valueType="gw.api.productmodel.CovTermOpt&lt;productmodel.OptionWC7EmpLiabPolicyLimitType&gt;">
        <PostOnChange
          deferUpdate="false"
          onChange="var issues = coverable.syncCoverages({gw.api.upgrade.PCCoercions.makeProductModel&lt;gw.api.productmodel.CoveragePattern&gt;(wc7EmpLiabCov.PatternCode)}); if(issues.HasElements) {throw new gw.api.util.DisplayableException(issues.map( \ i -&gt; i.BaseMessage).join(&quot;\n&quot;))};"/>
      </RangeInput>
      <RangeInput
        editable="gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm)"
        id="WC7EmpLiabLimitEachEmp_TDIC"
        label="wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm.DisplayName"
        required="wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm.Pattern.Required"
        value="wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm.OptionValue"
        valueRange="gw.web.productmodel.ChoiceCovTermUtil.getModelValueRange(wc7EmpLiabCov.WC7EmpLiabLimitEachEmpTDICTerm, openForEdit)"
        valueType="gw.api.productmodel.CovTermOpt&lt;productmodel.OptionWC7EmpLiabLimitEachEmpTDICType&gt;">
        <PostOnChange
          deferUpdate="false"
          onChange="var issues = coverable.syncCoverages({gw.api.upgrade.PCCoercions.makeProductModel&lt;gw.api.productmodel.CoveragePattern&gt;(wc7EmpLiabCov.PatternCode)}); if(issues.HasElements) {throw new gw.api.util.DisplayableException(issues.map( \ i -&gt; i.BaseMessage).join(&quot;\n&quot;))};"/>
      </RangeInput>
      <RangeInput
        editable="gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7StopGapTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7StopGapTerm)"
        id="WC7StopGapTermInput"
        label="wc7EmpLiabCov.WC7StopGapTerm.DisplayName"
        required="wc7EmpLiabCov.WC7StopGapTerm.Pattern.Required"
        value="wc7EmpLiabCov.WC7StopGapTerm.Value"
        valueRange="wc7EmpLiabCov.WC7StopGapTerm?.Pattern.OrderedAvailableValues"
        valueType="typekey.StopGap"
        visible="wc7EmpLiabCov.hasCovTermByCodeIdentifier(&quot;WC7StopGap&quot;)">
        <PostOnChange/>
      </RangeInput>
      <TextInput
        editable="gw.web.productmodel.ChoiceCovTermUtil.isEditable(wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm) and wc7line.isLineCoverageTermEditable(wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm)"
        id="WC7IncludedMonopolisticStatesTermInput"
        label="wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.DisplayName"
        required="wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.Pattern.Required"
        validationExpression="wc7line.validateIncludedMonopolisticStates(wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.Value)"
        value="wc7EmpLiabCov.WC7IncludedMonopolisticStatesTerm.Value"
        valueType="java.lang.String"
        visible="wc7EmpLiabCov.HasWC7IncludedMonopolisticStatesTerm and wc7EmpLiabCov.WC7StopGapTerm.Value == typekey.StopGap.TC_LISTEDSTATESONLY"/>
    </InputGroup>
  </InputSet>
</PCF>