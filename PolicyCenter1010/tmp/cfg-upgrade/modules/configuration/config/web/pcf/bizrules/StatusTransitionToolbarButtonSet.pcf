<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <ToolbarButtonSet
    id="StatusTransitionToolbarButtonSet">
    <Require
      name="stateHolder"
      type="gw.bizrules.pcf.RuleDetailsStateHolder"/>
    <ToolbarButton
      action="CurrentLocation.startEditing(); stateHolder.makeLatestVersionEditable()"
      available="!stateHolder.ImportInProgress"
      hideIfEditable="true"
      id="Edit"
      label="DisplayKey.get(&apos;Button.Edit&apos;)"
      shortcut="E"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.EditTooltip&apos;)"
      visible="stateHolder.LatestVersionSelected and gw.bizrules.pcf.RulePermissionUIHelper.canEditRule(stateHolder.getSelectedVersion())"/>
    <ToolbarButton
      action="CurrentLocation.commit()"
      hideIfReadOnly="true"
      id="Update"
      label="DisplayKey.get(&apos;Button.Update&apos;)"
      shortcut="U"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.UpdateTooltip&apos;)"/>
    <ToolbarButton
      action="CurrentLocation.cancel()"
      hideIfReadOnly="true"
      id="Cancel"
      label="DisplayKey.get(&apos;Button.Cancel&apos;)"
      shortcut="C"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.CancelTooltip&apos;)"/>
    <ToolbarDivider/>
    <ToolbarButton
      action="stateHolder.toggleEnableOrDisableSelectedVersion()"
      confirmMessage="stateHolder.getEnableDisableToggleConfirmationText()"
      hideIfEditable="true"
      id="EnableDisableToggle"
      label="stateHolder.getEnableDisableToggleLabel()"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.EnableDisableToggleTooltip&apos;)"
      visible="stateHolder.EnableDisableToggleVisible"/>
    <ToolbarDivider/>
    <ToolbarButton
      action="CurrentLocation.startEditing(); stateHolder.createDraftBasedOnSelectedVersion()"
      available="!stateHolder.ImportInProgress"
      hideIfEditable="true"
      id="Revert"
      label="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.Revert&apos;)"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.RevertTooltip&apos;)"
      visible="stateHolder.NotLatestVersionSelected and !stateHolder.Head.HeadVersion.Editable and gw.bizrules.pcf.RulePermissionUIHelper.canEditRule(stateHolder.getSelectedVersion())"/>
    <ToolbarButton
      action="stateHolder.deleteDraftInNewBundle()"
      available="!stateHolder.ImportInProgress"
      confirmMessage="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.DeleteVersionConfirmation&apos;)"
      hideIfEditable="true"
      id="DeleteDraft"
      label="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.DeleteDraft&apos;)"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.DeleteDraftTooltip&apos;)"
      visible="stateHolder.DeleteDraftVisible and gw.bizrules.pcf.RulePermissionUIHelper.canEditRule(stateHolder.getSelectedVersion())"/>
    <ToolbarButton
      action="stateHolder.deleteWipInNewBundle()"
      available="!stateHolder.ImportInProgress"
      confirmMessage="stateHolder.History.Count == 1 ? DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.DeleteRuleConfirmation&apos;) : DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.DeleteVersionConfirmation&apos;)"
      hideIfEditable="true"
      id="DeleteWip"
      label="DisplayKey.get(&apos;Button.Delete&apos;)"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.DeleteTooltip&apos;)"
      visible="stateHolder.DeleteWipVisible and gw.bizrules.pcf.RulePermissionUIHelper.canEditRule(stateHolder.getSelectedVersion())"/>
    <ToolbarDivider/>
    <ToolbarButton
      action="stateHolder.changeHeadVersionStatusInNewBundle(TC_STAGED)"
      available="!stateHolder.ImportInProgress"
      hideIfEditable="true"
      id="PromoteToStaged"
      label="DisplayKey.get(&quot;BizRules.StatusTransitionToolbarButtonSet.PromoteToStaged&quot;)"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.PromoteToStagedTooltip&apos;)"
      visible="stateHolder.LatestVersionSelected and stateHolder.SelectedVersion.Status == RuleStatus.TC_DRAFT and gw.bizrules.pcf.RulePermissionUIHelper.canEditRule(stateHolder.getSelectedVersion())"/>
    <ToolbarButton
      action="stateHolder.changeHeadVersionStatusInNewBundle(RuleStatus.TC_APPROVED)"
      available="!stateHolder.ImportInProgress"
      hideIfEditable="true"
      id="PromoteToApproved"
      label="DisplayKey.get(&quot;BizRules.StatusTransitionToolbarButtonSet.PromoteToApproved&quot;)"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.PromoteToApprovedTooltip&apos;)"
      visible="stateHolder.LatestVersionSelected and stateHolder.SelectedVersion.Status == RuleStatus.TC_STAGED and gw.bizrules.pcf.RulePermissionUIHelper.canApproveRule(stateHolder.getSelectedVersion())"/>
    <ToolbarButton
      action="stateHolder.deploySelectedVersion()"
      available="not gw.bizrules.management.BizRulesRollingUpgradeManager.Instance.RollingUpgradeProcessStartedInDB"
      confirmMessage="stateHolder.getDeployConfirmationText()"
      hideIfEditable="true"
      id="Deploy"
      label="DisplayKey.get(&quot;BizRules.StatusTransitionToolbarButtonSet.Deploy&quot;)"
      tooltip="DisplayKey.get(&apos;BizRules.StatusTransitionToolbarButtonSet.DeployTooltip&apos;)"
      visible="stateHolder.DeploymentEnabled and stateHolder.SelectedVersionDeployable and gw.bizrules.pcf.RulePermissionUIHelper.canDeployRule(stateHolder.getSelectedVersion())"/>
    <ToolbarDivider/>
  </ToolbarButtonSet>
</PCF>