<?xml version="1.0"?>
<typelistextension
  xmlns="http://guidewire.com/typelists"
  desc="Types of batch processes"
  name="BatchProcessType">
  <!-- batch processes -->
  <typecode
    code="ActivityEsc"
    desc="Activity escalation monitor"
    name="Activity Escalation">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="DataDistribution"
    desc="Data distribution for the database"
    name="Data Distribution">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="DBStats"
    desc="Database statistics"
    name="Database statistics">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="GroupException"
    desc="Group exception Monitor"
    name="Group Exception">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UserException"
    desc="User exception Monitor"
    name="User Exception">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Workflow"
    desc="Will execute the workflow writer."
    name="Workflow">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Geocode"
    desc="Geocoding Addresses queue writer."
    name="Geocode Writer">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PhoneNumberNormalizer"
    desc="Performs a normalization of phone numbers contact"
    name="Phone number normalizer">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="OpenPolicyException"
    desc="Policy Exception Monitor for open policies"
    name="Open Policy Exception">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="BoundPolicyException"
    desc="Policy Exception Monitor for bound policies"
    name="Bound Policy Exception">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ClosedPolicyException"
    desc="Policy Exception Monitor for closed policies"
    name="Closed Policy Exception">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyRenewalStart"
    desc="Policy Renewal Start monitor"
    name="Policy Renewal Start">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="FormTextDataDelete"
    desc="Deletes orphaned, purged, or archived FormTextData"
    name="Form Text Data Delete">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AuditTask"
    desc="Audit task monitor"
    name="AuditTask">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="OverduePremiumReport"
    desc="Monitor for overdue premium reports"
    name="Overdue Premium Report">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="TeamScreens"
    desc="Collect summary counts for team screens"
    name="Team Screens">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="JobExpire"
    desc="Expire a job if no action has been taken upon it for a configured period of time."
    name="Job Expiration">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PremiumCeding"
    desc="Reinsurance ceding of premium"
    name="PremiumCeding">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyHoldJobEval"
    desc="Evaluates jobs against the policy holds blocking it"
    name="Policy Hold Job Evaluation">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AccountWithdraw"
    desc="Evaluates accounts and closes them."
    name="Account Withdraw Evaluation">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ActivityRetire"
    desc="Retires canceled activities after configured time"
    name="Retire Activities">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ArchivePolicyTerm"
    desc="Policy term archiving monitor"
    name="Archive policy terms">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="RestorePolicyTerm"
    desc="Policy term retrieve from archive monitor"
    name="Retrieve policy terms">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ImpactTestingTestPrep"
    desc="Prepares policy periods for impact testing"
    name="Impact Testing Test Case Preparation">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ImpactTestingTestRun"
    desc="Runs the test periods through the rating algorithm"
    name="Impact Testing Test Case Run">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ImpactTestingExport"
    desc="Exports the test periods to Excel"
    name="Impact Testing Export">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Purge"
    desc="Purges Entities which are no longer needed"
    name="Purge">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PurgeOrphanedPolicyPeriod"
    desc="Purges policy periods orphaned as a result of preemption"
    name="Purge Orphaned Policy Periods">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ResetPurgeStatusAndCheckDates"
    desc="Reset purge status and purge/prune dates on Job"
    name="Reset Purge Status and Check Dates">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PurgeMessageHistory"
    desc="Purges old messages from the message history table"
    name="Purge Message History">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PurgeWorksheets"
    desc="Purge WorksheetContainer objects"
    name="Purge Rating Worksheets">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ExtractWorksheets"
    desc="Extract rating worksheet data from WorksheetContainer objects and flag these objects for purging"
    name="Extract Rating Worksheets">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyRenewalClearCheckDate"
    desc="Clears existing Policy Renewal Check Dates"
    name="Clear Policy Renewal Check Dates">
    <category
      code="MaintenanceOnly"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ApplyPendingAccountDataUpdates"
    desc="Apply any of the pending updates to account data."
    name="Apply Pending Account Data Updates">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="SolrDataImport"
    desc="Performs a full data import of the app database into the Solr/Lucene index"
    name="Solr Data Import">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PurgeQuoteClones"
    desc="Purge Quote Clones: Purge temporary cloned policy periods"
    name="Purge Quote Clones">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AccountHolderCount"
    desc="Adjust AccountHolderCount value on Contact"
    name="AccountHolderCount">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyLocationsRiskAssessment"
    desc="Retrieves risk assessments for all the locations on a policy period"
    name="PolicyLocationsRiskAssessment">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PurgeRiskAssessmentTempStore"
    desc="Purges all temporary risk assessment entities"
    name="PurgeRiskAssessmentTempStore">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="HandleUnresolvedContingency"
    desc="Handles all the contingencies which are unresolved"
    name="Handle Unresolved Contingency">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="RecalculateContingencyActionStartDate"
    desc="Recalculate action start date for all contingencies where action has not started"
    name="Recalculate Contingency Action Start Date">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AsyncQuoting"
    desc="Quote PolicyPeriods asynchronously"
    name="Asynchronous Quoting"/>
  <typecode
    code="AsyncRating"
    desc="Rate PolicyPeriods asynchronously"
    name="Asynchronous Rating"/>
  <typecode
    code="BulkSubmission"
    desc="This job creates documents asynchronously using Bulk API"
    name="Bulk Submission Job">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ArchiveReferenceTrackingSync"
    desc="Ensures that, as long archive reference tracking is enabled, that the archive document references table is in sync with the archive store."
    name="Archive Reference Tracking Sync">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="DocumentCreationBatch"
    desc="Batch Process to Create Document Asynchronously using Command Center"
    name="Document Creation Batch">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="CacheManager"
    desc="Rebuild the cache from GWINT database"
    name="Cache Manager"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="FeedPivotalAS400Batch_TDIC"
    desc="PolicyCenter batch process that read all in force and cancelled policies and write them to a “comma-delimited”."
    name="Feed Pivotal/AS400 Batch"
    retired="true">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ProcessFinalAuditEstimation_TDIC"
    desc="Process estimated Final Audits for policys that are at least &quot;ScriptParameters.TDIC_WC7DaysAfterPolicyExpiredToStartFinalAuditBatch&quot;  days expired."
    name="Process Final Audit Estimation TDIC">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="GLInterface"
    desc="General Ledger Monthly Reporting batch for Written and Earned Premiums"
    name="Finance Interface General Ledger Monthly"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="GLInterface_TDIC"
    desc="General Ledger Monthly Reporting batch for Written and Earned Premiums"
    name="Finance Interface General Ledger Monthly"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ReviewMultilineDiscount_TDIC"
    desc="Review the multiline discount indicator on all WC policy periods using the multiline discount specification"
    name="Review the multiline discount indicator on all WC policy periods">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="WCpolsReportBatch"
    desc="WCPOLS Report Batch"
    name="WCpolsReportBatch">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AuditDocumentCreationBatch"
    desc="Triggers documents creation for Audit Documents"
    name="Audit Document Creation Batch">
    <category
      xmlns=""
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      xmlns=""
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="WCstatInitialReportBatch"
    desc="WCSTAT Initial Report Batch"
    name="WCstatInitialReportBatch">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="WCstatSubsequentReportBatch"
    desc="WCstat Subsequent Report Batch"
    name="WCstatSubsequentReportBatch">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ADAMembershipSync_TDIC"
    desc="ADA Membership sync with Aptify"
    name="ADA Membership sync with Aptify">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="EarnedPremiumReport_TDIC"
    desc="Earned premium batch process for reports purpose"
    name="EarnedPremiumReport"
    retired="true">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="DataMigrationPA_Ext"
    desc="Run personal auto data migration"
    name="Data Migration for Personal Auto"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="DataMigrationWC7_Ext"
    desc="Run workers&apos; compensation (v7) data migration"
    name="Data Migration for Workers&apos; Compensation (v7)"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="MigratedADAMembershipSync_TDIC"
    desc="ADA Membership Sync with Aptify for migrated records to be run at the time of initial load"
    name="Migrated ADA Membership Status Sync"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="WCstatMigratedDataOneTimeBatch"
    desc="One time batch for WCSTAT migrate data"
    name="WCstatMigratedDataOneTimeBatch">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Create_CM_Contacts_TDIC"
    desc="One time process to create contacts in the Contact Manager System "
    name="Create CM Contacts">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UnencryptOfficialIDs_TDIC"
    desc="Unencrypt Official IDs"
    name="Unencrypt Official IDs">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="CompleteActivities_TDIC"
    desc="Complete Activities"
    name="Complete Activities">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UpdateChangeReasons_TDIC"
    desc="Update Change Reasons"
    name="Update Change Reasons">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AS400Batch_TDIC"
    desc="PolicyCenter batch process that read all policies and write them to a CSV File."
    name="AS400 Batch">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="FeedPivotalBatch_TDIC"
    desc="PolicyCenter batch process that read all in force and cancelled policies and write them to a CSV File."
    name="Feed Pivotal Batch">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="LossRatioRecalc_TDIC"
    desc="Process retrieves all Policy Terms available and recalculate Loss Ratio for each term and persist values"
    name="Loss Ratio Recalculation">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AutomaticRateBookLoader_TDIC"
    desc="Automatic RateBook Loader"
    name="Automatic RateBook Loader">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AS400InboundBatch_TDIC"
    desc="AS400InboundFileLoader"
    name="AS400 Inbound FileLoader">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="exportRatebooks_TDIC"
    desc="Job to export ratebooks"
    name="Export Ratebooks">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyPayload_Ext"
    desc="Policy Payload Creation Batch"
    name="Policy Payload Creation Batch">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyConversion_Ext"
    desc="Policy Conversion Creation Batch"
    name="Policy Conversion Creation Batch">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="EnroleOutboundBatch_TDIC"
    desc="PolicyCenter batch process that read all policies and write them to a TXT File."
    name="Enrole Outbound Batch">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="EnroleInboundBatch_TDIC"
    desc="Enrole inbound FileLoader."
    name="Enrole Inbound Batch">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="MultiLinePolicyBatch_Ext"
    desc="MultiLine Policy Batch "
    name="MultiLine Policy Batch">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="FinancePremiumsProcessBatch_TDIC"
    desc="Daily batch to process premiums for bound and audit completed transactions"
    name="Finance Premiums Process Daily">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="FinancePremiumsReportBatch_TDIC"
    desc="Monthly batch process to generate flat file of aggregated premiums for Lawson report"
    name="Finance Premiums Report Monthly">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
</typelistextension>