<?xml version="1.0"?>
<typelistextension
  xmlns="http://guidewire.com/typelists"
  desc="Reason code"
  name="ReasonCode">
  <typecode
    code="foundOtherInsurer_TDIC"
    desc="Found Other Insurer"
    name="Found Other Insurer"/>
  <typecode
    code="nonpayment"
    desc="Non-payment"
    name="Non-payment"
    priority="7">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="fraud"
    desc="Fraud"
    name="Fraud"/>
  <typecode
    code="cancel"
    desc="Cancellation of underlying insurance"
    name="Cancellation of underlying insurance"/>
  <typecode
    code="flatrewrite"
    desc="Policy rewritten or replaced (flat cancel)"
    name="Policy rewritten or replaced (flat cancel)">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="midtermrewrite"
    desc="Policy rewritten (mid-term)"
    name="Policy rewritten (mid-term)">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="nottaken"
    desc="Not Taken - Non-Payment of Premium"
    name="Not Taken - Non Payment of Premium">
    <category
      code="insured"
      typelist="CancellationSource"/>
    <category
      code="NotTaken"
      typelist="PolicyPeriodStatus"/>
    <category
      code="NotTaken"
      typelist="PolicyPeriodStatus"/>
    <category
      code="NotTaking"
      typelist="PolicyPeriodStatus"/>
  </typecode>
  <typecode
    code="sold"
    desc="Sold Practice"
    name="Sold Practice"
    priority="1">
    <category
      code="insured"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="noemployee"
    desc="No employees"
    name="No employees"
    priority="5">
    <category
      code="insured"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="noc"
    desc="Insured&apos;s request - N.O.C"
    name="Insured&apos;s request - N.O.C"/>
  <typecode
    code="fincononpay"
    desc="Insured&apos;s request - (Finance co. nonpay)"
    name="Insured&apos;s request - (finance co. nonpay)"/>
  <typecode
    code="violation"
    desc="Violation of health, safety, fire, or codes"
    name="Violation of health, safety, fire, or codes"/>
  <typecode
    code="vacant"
    desc="Vacant; below occupancy limit"
    name="Vacant; below occupancy limit"/>
  <typecode
    code="uwreasons"
    desc="Underwriting reasons"
    name="Underwriting reasons"
    priority="12">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="suspension"
    desc="Suspension or revocation of license or permits"
    name="Suspension or revocation of license or permits"/>
  <typecode
    code="riskchange"
    desc="Substantial change in risk or increase in hazard"
    name="Substantial change in risk or increase in hazard"/>
  <typecode
    code="wrapup"
    desc="Participation in wrap-up complete"
    name="Participation in wrap-up complete"/>
  <typecode
    code="nonreport"
    desc="Non-report of payroll"
    name="Non-report of payroll"
    priority="9">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="nondisclose"
    desc="Non disclosure of losses or underwriting information"
    name="Non disclosure of losses or underwriting information"/>
  <typecode
    code="eligibility"
    desc="Non membership"
    name="Non membership"
    priority="4">
    <category
      code="NotTakenAck"
      typelist="LetterType"/>
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="reinsurance"
    desc="Loss of reinsurance"
    name="Loss of reinsurance"/>
  <typecode
    code="failcoop"
    desc="Failure to cooperate"
    name="Failure to cooperate"
    priority="3">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="failterm"
    desc="Failure to comply with terms and conditions of audit"
    name="Failure to comply with terms and conditions of audit"
    priority="4">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="failsafe"
    desc="Failure to comply with safety recommendations"
    name="Failure to comply with safety recommendations"/>
  <typecode
    code="criminal"
    desc="Criminal conduct by the insured"
    name="Criminal conduct by the insured"/>
  <typecode
    code="condemn"
    desc="Condemned/unsafe"
    name="Condemned/unsafe"/>
  <typecode
    code="LossHistory"
    desc="Loss history"
    name="Loss history"/>
  <typecode
    code="OpsChars"
    desc="Operations characteristics"
    name="Operations characteristics"/>
  <typecode
    code="ProductsChars"
    desc="Products characteristics"
    name="Products characteristics"/>
  <typecode
    code="PaymentHistory"
    desc="Payment history"
    name="Payment history"/>
  <typecode
    code="ProdRequirements"
    desc="Does not meet program/product requirements"
    name="Does not meet program/product requirements"/>
  <typecode
    code="CovsNotAvailable"
    desc="Requested coverages/limits not available"
    name="Requested coverages/limits not available"/>
  <typecode
    code="InfoNotProvided"
    desc="Required information not provided"
    name="Required information not provided"/>
  <typecode
    code="errorEntered_TDIC"
    desc="Entered in Error "
    name="Entered in Error  "/>
  <typecode
    code="featureBenefit_TDIC"
    desc="Features/Benefits "
    name="Features/Benefits ">
    <category
      code="NotTakenAck"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="finReason_TDIC"
    desc="Financial Reasons "
    name="Financial Reasons ">
    <category
      code="NotTakenAck"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="interstateOper_TDIC"
    desc="Interstate Operations "
    name="Interstate Operations "
    priority="3">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="noResponse_TDIC"
    desc="No Response"
    name="No Response">
    <category
      code="NotTakenAck"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="OCUnder16Over65_TDIC"
    desc="Operating Characteristics - Employees under age of 16 or over age 65 "
    name="Operating Characteristics - Employees under age of 16 or over age 65 ">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="OCExp8839or8810_TDIC"
    desc="Operating Characteristics - Exposures outside 8839 or 8810 class code "
    name="Operating Characteristics - Exposures outside 8839 or 8810 class code ">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="OCUsState_TDIC"
    desc="Operating Characteristics - US States Longshoremen&apos;s and Harbor Worker&apos;s exposure "
    name="Operating Characteristics - US States Longshoremen&apos;s and Harbor Worker&apos;s exposure ">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="perAppRequest_TDIC"
    desc="Per Applicant Request "
    name="Per Applicant Request "/>
  <typecode
    code="previousNonRenew_TDIC"
    desc="Previous Non-Renew "
    name="Previous Non-Renew ">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="quoteExpired_TDIC"
    desc="Quote Expired "
    name="Quote Expired ">
    <category
      code="NotTakenAck"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="earlyLate_TDIC"
    desc="Too Early/Late "
    name="Too Early/Late ">
    <category
      code="NotTakenAck"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="tookCoverElse_TDIC"
    desc="Placed coverage elsewhere "
    name="Placed coverage elsewhere "
    priority="2">
    <category
      code="insured"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="udKnowIncidOpen_TDIC"
    desc="Uninsured Dentist - Known incident or open claim "
    name="Uninsured Dentist - Known incident or open claim ">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="udPrevApplied_TDIC"
    desc="Uninsured Dentist - Previously applied as uninsured "
    name="Uninsured Dentist - Previously applied as uninsured ">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="other_TDIC"
    desc="Other"
    name="Other"
    priority="13">
    <category
      code="Declination"
      typelist="LetterType"/>
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="failReportPriorPay_TDIC"
    desc="Failure To Report Prior Payroll"
    name="Failure To Report Prior Payroll">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="failPayPriorPrem_TDIC"
    desc="Failure To Pay Prior Premium"
    name="Failure To Pay Prior Premium">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="incompleteApp_TDIC"
    desc="Incomplete Application"
    name="Incomplete Application">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="reqByApplicant_TDIC"
    desc="Requested By Applicant"
    name="Requested By Applicant">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="noLongerInBusinessRet_TDIC"
    desc="No longer in business - retired"
    name="No longer in business - retired"
    priority="3">
    <category
      code="insured"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="noLongerInBusinessDec_TDIC"
    desc="No longer in business - deceased"
    name="No longer in business - deceased"
    priority="4">
    <category
      code="insured"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="ineligilbleClassCode_TDIC"
    desc="Ineligible class code"
    name="Ineligible class code"
    priority="6">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="nonpaymentaudit_TDIC"
    desc="Non-payment of final audit"
    name="Non-payment of final audit"
    priority="8"/>
  <typecode
    code="movedOutOfState_TDIC"
    desc="Moved out of state"
    name="Moved out of state"
    priority="10">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typecode
    code="rewritten_TDIC"
    desc="Re-written by TDIC"
    name="Re-written by TDIC"
    priority="11">
    <category
      code="carrier"
      typelist="CancellationSource"/>
  </typecode>
  <typefilter
    desc="Cancellation reasons used for Delinquency"
    name="DelinquencyReasons_TDIC">
    <include
      code="nonpayment"/>
    <include
      code="nottaken"/>
  </typefilter>
  <typefilter
    desc="Submission Withdrawn"
    name="submissionWithdrawnCP_TDIC">
    <include
      code="appWithdraw_TDIC"/>
    <include
      code="claimFreq_TDIC"/>
    <include
      code="claimSev_TDIC"/>
    <include
      code="covElsewhere_TDIC"/>
    <include
      code="dupSubmission_TDIC"/>
    <include
      code="incompleteApp_TDIC"/>
    <include
      code="incompleteSub_TDIC"/>
    <include
      code="processEndor_TDIC"/>
    <include
      code="reinstated_TDIC"/>
    <include
      code="doesNotMeetUWCriteria_TDIC"/>
  </typefilter>
  <typefilter
    desc="Submission Declined"
    name="submissionDeclinedCP_TDIC">
    <include
      code="claimFreq_TDIC"/>
    <include
      code="claimSev_TDIC"/>
    <include
      code="doesNotMeetUWCriteria_TDIC"/>
    <include
      code="uninsurable_TDIC"/>
  </typefilter>
  <typefilter
    desc="PL Declined Reasons"
    name="PLDeclinedReasons">
    <include
      code="claimFreq_TDIC"/>
    <include
      code="claimSev_TDIC"/>
    <include
      code="uninsurable_TDIC"/>
    <include
      code="uwCriteria_TDIC"/>
  </typefilter>
  <typecode
    code="claimFreq_TDIC"
    desc="Claim Frequency"
    name="Claim Frequency">
    <category
      code="Declination"
      typelist="LetterType"/>
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="claimSev_TDIC"
    desc="Claim Severity"
    name="Claim Severity">
    <category
      code="Declination"
      typelist="LetterType"/>
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="licRevoked_TDIC"
    desc="License Revoked"
    name="License Revoked">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="miscUnderstand_TDIC"
    desc="Miscellaneous Underwriting"
    name="Miscellaneous Underwriting">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="pop_TDIC"
    desc="Pattern of Practice"
    name="Pattern of Practice">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="uninsurable_TDIC"
    desc="Uninsurable"
    name="Uninsurable">
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="incompleteSub_TDIC"
    desc="Incomplete Submission"
    name="Incomplete Submission">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="covElsewhere_TDIC"
    desc="Coverage Placed Elsewhere"
    name="Coverage Placed Elsewhere">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="uwCriteria_TDIC"
    desc="Does Not Meet UW Criteria"
    name="Does Not Meet UW Criteria">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
    <category
      code="Declination"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="dupSubmission_TDIC"
    desc="Duplicate Submission"
    name="Duplicate Submission">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="processEndor_TDIC"
    desc="Processed as an Endorsement"
    name="Processed as an Endorsement">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typecode
    code="reinstated_TDIC"
    desc="Reinstated"
    name="Reinstated">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typefilter
    desc="PL Submission Withdrawn"
    name="PLWithdrawn">
    <include
      code="appWithdraw_TDIC"/>
    <include
      code="claimFreq_TDIC"/>
    <include
      code="claimSev_TDIC"/>
    <include
      code="covElsewhere_TDIC"/>
    <include
      code="uwCriteria_TDIC"/>
    <include
      code="dupSubmission_TDIC"/>
    <include
      code="incompleteApp_TDIC"/>
    <include
      code="incompleteSub_TDIC"/>
    <include
      code="processEndor_TDIC"/>
    <include
      code="reinstated_TDIC"/>
  </typefilter>
  <typecode
    code="appWithdraw_TDIC"
    desc="Application Withdrawn"
    name="Application Withdrawn">
    <category
      code="Withdraw_TDIC"
      typelist="LetterType"/>
  </typecode>
  <typefilter
    desc="PL Decline for Cyber"
    name="PLDeclineCyb">
    <include
      code="claimSev_TDIC"/>
    <include
      code="claimFreq_TDIC"/>
    <include
      code="incompleteApp_TDIC"/>
    <include
      code="miscUnderstand_TDIC"/>
  </typefilter>
  <typefilter
    desc="PL Decline for other Offerings"
    name="PLDeclineOthers">
    <include
      code="claimFreq_TDIC"/>
    <include
      code="claimSev_TDIC"/>
    <include
      code="incompleteApp_TDIC"/>
    <include
      code="licRevoked_TDIC"/>
    <include
      code="miscUnderstand_TDIC"/>
    <include
      code="pop_TDIC"/>
    <include
      code="uninsurable_TDIC"/>
  </typefilter>
  <typecode
    code="claimsHistFreq_TDIC"
    desc="ClaimsFreq"
    name="Claims Freq"
    retired="true"/>
  <typecode
    code="ClaimsHistSeverity_TDIC"
    desc="ClaimsSeverity"
    name="Claims Severity"
    retired="true"/>
  <typecode
    code="AppWithdrawn_TDIC"
    desc="App Withdrawn"
    name="App Withdrawn"
    retired="true"/>
  <typecode
    code="covPlacedElsewhere_TDIC"
    desc="Cov Placed"
    name="Cov placed"
    retired="true"/>
  <typecode
    code="doesNotMeetUWCriteria_TDIC"
    name="Does Not Meet UW Criteria"/>
  <typecode
    code="escrowFellApart_TDIC"
    desc="Escrow Fell Apart"
    name="Escrow Fell Apart"/>
  <typecode
    code="falsifiedDocuments_TDIC"
    desc="Falsified Documents"
    name="Falsified Documents"/>
  <typecode
    code="practiceClosed_TDIC"
    desc="Practice Closed"
    name="Practice Closed"/>
  <typecode
    code="companionPropPolicyCanceled_TDIC"
    desc="Companion property policy canceled"
    name="Companion property policy canceled"/>
  <typecode
    code="StDentalAssocNonMemberApplicant_TDIC"
    desc="State Dental Association Non-Member/Applicant"
    name="State Dental Association Non-Member/Applicant"/>
  <typecode
    code="employedbyGovernment_TDIC"
    desc="Employed by Government"
    name="Employed by Government"/>
  <typecode
    code="employerProvidingInsurance_TDIC"
    desc="Employer Providing Insurance"
    name="Employer Providing Insurance"/>
  <typecode
    code="foundOtherInsured_TDIC"
    desc="Found Other Insured"
    name="Found Other Insured"/>
  <typecode
    code="insuredRequestDuetoMembershipRequirement_TDIC"
    desc="Insured Request Due to Membership Requirement"
    name="Insured Request Due to Membership Requirement"/>
  <typecode
    code="movedOutofCountry_TDIC"
    desc="Moved Out of Country"
    name="Moved Out of Country"/>
  <typecode
    code="permanentlyDisabled_TDIC"
    desc="Permanently Disabled"
    name="Permanently Disabled"/>
  <typecode
    code="policyholderRequestMiscellaneous_TDIC"
    desc="Policyholder Request Miscellaneous"
    name="Policyholder Request Miscellaneous"/>
  <typecode
    code="tdicLeavingMarket_TDIC"
    desc="TDIC Leaving Market"
    name="TDIC Leaving Market"/>
  <typecode
    code="wentBare_TDIC"
    desc="Went Bare"
    name="Went Bare"/>
  <typefilter
    desc="PL/CM-OC Insured Cancellation Reasons"
    name="PL_CM_OCInsuredCancelReasons">
    <include
      code="employedbyGovernment_TDIC"/>
    <include
      code="employerProvidingInsurance_TDIC"/>
    <include
      code="insuredRequestDuetoMembershipRequirement_TDIC"/>
    <include
      code="movedOutOfState_TDIC"/>
    <include
      code="movedOutofCountry_TDIC"/>
    <include
      code="noLongerInBusinessDec_TDIC"/>
    <include
      code="noLongerInBusinessRet_TDIC"/>
    <include
      code="nottaken"/>
    <include
      code="permanentlyDisabled_TDIC"/>
    <include
      code="policyholderRequestMiscellaneous_TDIC"/>
    <include
      code="rewritten_TDIC"/>
    <include
      code="wentBare_TDIC"/>
    <include
      code="foundOtherInsurer_TDIC"/>
  </typefilter>
  <typefilter
    desc="PL/CM-OC Carrier Cancellation Reasons"
    name="PL_CM_OCCarrierCancelReasons">
    <include
      code="StDentalAssocNonMemberApplicant_TDIC"/>
    <include
      code="rewritten_TDIC"/>
    <include
      code="falsifiedDocuments_TDIC"/>
    <include
      code="nonpayment"/>
    <include
      code="other_TDIC"/>
  </typefilter>
  <typefilter
    desc="PL/CYB Insured Cancellation Reasons"
    name="PL_CYBInsuredCancelReasons">
    <include
      code="foundOtherInsurer_TDIC"/>
    <include
      code="movedOutOfState_TDIC"/>
    <include
      code="nottaken"/>
    <include
      code="policyholderRequestMiscellaneous_TDIC"/>
    <include
      code="practiceClosed_TDIC"/>
    <include
      code="rewritten_TDIC"/>
    <include
      code="sold"/>
    <include
      code="NonRenewPolicyholderRequest_TDIC"/>
  </typefilter>
  <typefilter
    desc="PL/CYB Carrier Cancellation Reasons"
    name="PL_CYBCarrierCancelReasons">
    <include
      code="StDentalAssocNonMemberApplicant_TDIC"/>
    <include
      code="companionPropPolicyCanceled_TDIC"/>
    <include
      code="policyholderRequestMiscellaneous_TDIC"/>
    <include
      code="rewritten_TDIC"/>
    <include
      code="falsifiedDocuments_TDIC"/>
    <include
      code="nonpayment"/>
    <include
      code="other_TDIC"/>
  </typefilter>
  <typecode
    code="NonRenewPolicyholderRequest_TDIC"
    name="Non-Renew Policyholder Request"/>
  <typefilter
    desc="BOP Carrier Cancellation Reasons"
    name="BOP_CarrierCancelReasons">
    <include
      code="rewritten_TDIC"/>
    <include
      code="StDentalAssocNonMemberApplicant_TDIC"/>
    <include
      code="falsifiedDocuments_TDIC"/>
    <include
      code="nonpayment"/>
    <include
      code="other_TDIC"/>
  </typefilter>
  <typefilter
    desc="BOP Insured Cancellation Reasons"
    name="BOP_InsuredCancelReasons">
    <include
      code="NonRenewPolicyholderRequest_TDIC"/>
    <include
      code="escrowFellApart_TDIC"/>
    <include
      code="foundOtherInsurer_TDIC"/>
    <include
      code="nottaken"/>
    <include
      code="policyholderRequestMiscellaneous_TDIC"/>
    <include
      code="practiceClosed_TDIC"/>
    <include
      code="rewritten_TDIC"/>
    <include
      code="sold"/>
    <include
      code="movedPracticeLocation_TDIC"/>
  </typefilter>
  <typecode
    code="movedPracticeLocation_TDIC"
    desc="Moved Practice Location"
    name="Moved Practice Location"/>
  <typefilter
    desc="Work Comp Carrier Cancellation Reasons"
    name="WC_CarrierCancelReasons">
    <include
      code="failcoop"/>
    <include
      code="failterm"/>
    <include
      code="ineligilbleClassCode_TDIC"/>
    <include
      code="movedOutOfState_TDIC"/>
    <include
      code="nonreport"/>
    <include
      code="other_TDIC"/>
    <include
      code="rewritten_TDIC"/>
    <include
      code="unresolvedcontingency"/>
    <include
      code="uwreasons"/>
    <include
      code="claimSev_TDIC"/>
    <include
      code="claimFreq_TDIC"/>
  </typefilter>
  <typefilter
    desc="Work Comp Insured Cancellation Reasons"
    name="WC_InsuredCancelReasons">
    <include
      code="noLongerInBusinessDec_TDIC"/>
    <include
      code="noLongerInBusinessRet_TDIC"/>
    <include
      code="noemployee"/>
    <include
      code="sold"/>
    <include
      code="tookCoverElse_TDIC"/>
  </typefilter>
</typelistextension>