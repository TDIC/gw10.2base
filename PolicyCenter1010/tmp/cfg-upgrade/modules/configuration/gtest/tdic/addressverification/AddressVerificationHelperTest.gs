package tdic.addressverification

uses tdic.TDICTestBase
uses gw.api.builder.AddressBuilder
uses tdic.pc.config.addressverification.AddressVerificationHelper

/**
 * ShaneS 05/25/2015
 */
class AddressVerificationHelperTest extends TDICTestBase {

  /**
   * ShaneS 05/05/2015
   */
  function testUpdateAddressStatus_standardized(){
    var address = getAddress()
    address.StandardizationStatus_TDIC = typekey.AddressStatusType_TDIC.TC_STANDARDIZED
    new AddressVerificationHelper().updateAddressStatus(address)

    var expectedStatus = typekey.AddressStatusType_TDIC.TC_STANDARDIZED
    var actualStatus = address.StandardizationStatus_TDIC
    assertEquals(expectedStatus, actualStatus)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testUpdateAddressStatus_overridden(){
    var address = getAddress()
    address.StandardizationStatus_TDIC = typekey.AddressStatusType_TDIC.TC_OVERRIDDEN
    new AddressVerificationHelper().updateAddressStatus(address)

    var expectedStatus = typekey.AddressStatusType_TDIC.TC_OVERRIDDEN
    var actualStatus = address.StandardizationStatus_TDIC
    assertEquals(expectedStatus, actualStatus)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testUpdateAddressStatus_invalid(){
    var address = getAddress()
    address.StandardizationStatus_TDIC = typekey.AddressStatusType_TDIC.TC_INVALID
    new AddressVerificationHelper().updateAddressStatus(address)

    var expectedStatus = typekey.AddressStatusType_TDIC.TC_OVERRIDDEN
    var actualStatus = address.StandardizationStatus_TDIC
    assertEquals(expectedStatus, actualStatus)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testUpdateAddressStatus_new(){
    var address = getAddress()
    address.StandardizationStatus_TDIC = typekey.AddressStatusType_TDIC.TC_NEW
    new AddressVerificationHelper().updateAddressStatus(address)

    var expectedStatus = typekey.AddressStatusType_TDIC.TC_OVERRIDDEN
    var actualStatus = address.StandardizationStatus_TDIC
    assertEquals(expectedStatus, actualStatus)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testReplaceAddressWithSuggested_addressLine1(){
    var address = getAddress()
    var suggestedAddress = getSuggestedAddress()
    new AddressVerificationHelper().replaceAddressWithSuggested(address, suggestedAddress, false)

    var expectedAddressLine1 = suggestedAddress.AddressLine1
    var actualAddressLine1 = address.AddressLine1
    assertEquals(expectedAddressLine1, actualAddressLine1)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testReplaceAddressWithSuggested_addressLine2(){
    var address = getAddress()
    var suggestedAddress = getSuggestedAddress()
    new AddressVerificationHelper().replaceAddressWithSuggested(address, suggestedAddress, false)

    var expectedAddressLine2 = suggestedAddress.AddressLine2
    var actualAddressLine2 = address.AddressLine2
    assertEquals(expectedAddressLine2, actualAddressLine2)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testReplaceAddressWithSuggested_addressLine3(){
    var address = getAddress()
    var suggestedAddress = getSuggestedAddress()
    new AddressVerificationHelper().replaceAddressWithSuggested(address, suggestedAddress, false)

    var expectedAddressLine3 = suggestedAddress.AddressLine3
    var actualAddressLine3 = address.AddressLine3
    assertEquals(expectedAddressLine3, actualAddressLine3)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testReplaceAddressWithSuggested_city(){
    var address = getAddress()
    var suggestedAddress = getSuggestedAddress()
    new AddressVerificationHelper().replaceAddressWithSuggested(address, suggestedAddress, false)

    var expectedCity = suggestedAddress.City
    var actualCity = address.City
    assertEquals(expectedCity, actualCity)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testReplaceAddressWithSuggested_state(){
    var address = getAddress()
    var suggestedAddress = getSuggestedAddress()
    new AddressVerificationHelper().replaceAddressWithSuggested(address, suggestedAddress, false)

    var expectedState = suggestedAddress.State
    var actualState = address.State
    assertEquals(expectedState, actualState)
  }

  /**
   * ShaneS 05/05/2015
   */
  function testReplaceAddressWithSuggested_zip(){
    var address = getAddress()
    var suggestedAddress = getSuggestedAddress()
    new AddressVerificationHelper().replaceAddressWithSuggested(address, suggestedAddress, false)

    var expectedZip = suggestedAddress.PostalCode
    var actualZip = address.PostalCode
    assertEquals(expectedZip, actualZip)
  }

  /**
   * ShaneS 05/05/2015
   */
  private function getAddress() : Address{
    return createAddress("1202 Kel Street", "Floor 5, Ste 6", "", "Scaramento", typekey.State.TC_CA, "95818", typekey.AddressStatusType_TDIC.TC_STANDARDIZED)
  }

  /**
   * ShaneS 05/05/2015
   */
  private function getSuggestedAddress(): Address{
    return createAddress("1202 K St", "Floor 5", "Ste 6", "Scaramento", typekey.State.TC_CA, "95818-8765", typekey.AddressStatusType_TDIC.TC_STANDARDIZED)
  }

  /**
   * ShaneS 05/05/2015
   */
  private function createAddress(addressLine1 : String, addressLine2 : String, addressLine3 : String, city : String,
                                 state : typekey.State, zip : String, status : typekey.AddressStatusType_TDIC) : Address {
    return new AddressBuilder()
      .withAddressLine1(addressLine1)
      .withAddressLine2(addressLine2)
      .withAddressLine3(addressLine3)
      .withCity(city)
      .withState(state)
      .withPostalCode(zip)
      .create()
  }
}