package tdic.pc.common.batch.generalledger.prmlines

uses tdic.pc.common.batch.generalledger.util.TDIC_GLReportUtil
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_PremiumObject

uses java.util.Map
uses java.lang.Exception
uses java.util.Date
uses java.text.SimpleDateFormat
uses java.util.List

/**
 * US627
 * 12/30/2014 Kesava Tavva
 *
 * Tests the functionality of TDIC_WrittenPremiumLines.
 */
@gw.testharness.ServerTest
class TDIC_WrittenPremiumLinesTest  extends gw.testharness.TestBase {

  var prmLines : TDIC_PremiumLinesInterface
  var premiums : Map<Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>>
  var prmLinesList : List<String>
  var startDate : Date
  var endDate : Date
  /**
   * This method is to initialize instances that are required for each test case execution
   */
  override function beforeMethod() {
    startDate = TDIC_GLReportUtil.getBatchRunPeriodStartDate(12,2014)
    endDate = TDIC_GLReportUtil.getBatchRunPeriodEndDate(startDate)
    prmLines = new TDIC_WrittenPremiumLines(endDate)
    premiums = TDIC_PremiumLinesTestUtil.buildWrittenPremiumTestData()
  }

  /**
   * This method is to test total number of premium lines generated for test premiums data
   */
  function testGeneratePremiumLines_TotalLines() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      assertEquals(TDIC_PremiumLinesTestUtil.getPremiumsCount(premiums)*2,prmLinesList.Count)
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test total number of columns reported in each premium line
   */
  function testTotalColumnsInEachLine() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      prmLinesList.each( \ line -> {
        assertEquals(36,line.split(",").Count)
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test RUNGROUP reported in each premium line
   */
  function testRunGroup() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      prmLinesList.each( \ line -> {
        assertEquals(36,line.split(",").Count)
        assertEquals("\"GW-LGL-WPM\"",line.split(",")[0])
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test Sequence number reported in each premium line
   */
  function testSequenceNumber() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var seqNumPrefix = "00000"
      var seqNum = 0
      prmLinesList.each( \ line -> {
        seqNum = seqNum+1
        assertEquals(36,line.split(",").Count)
        assertEquals("${seqNumPrefix}${seqNum}",line.split(",")[1])
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test Company reported in each premium line
   */
  function testCompany() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      prmLinesList.each( \ line -> {
        assertEquals(36,line.split(",").Count)
        assertEquals("5",line.split(",")[2])
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test old Company reported in each premium line
   */
  function testOldCompany() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_EXP_CONSTANT)
            || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_PREMIUM)
            || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_TERRORISM)
          ){
          assertEquals("\"0005800.06.40\"",columns[3])
        }else {
          assertEquals("\"0005000.06.40\"",columns[3])
        }
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test old account number reported in each premium line
   */
  function testOldAccountNumber() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_EXP_CONSTANT)
            || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_PREMIUM)
            || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_TERRORISM)
            || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_STATE_ASSMNTS)
            || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_PRM_RCV_ACCOUNT)
        ){
          assertTrue(Boolean.TRUE)
        }else{
          assertTrue(Boolean.FALSE)
        }
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test SourceCode reported in each premium line
   */
  function testSourceCode() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      prmLinesList.each( \ line -> {
        assertEquals(36,line.split(",").Count)
        assertEquals("\"IG\"",line.split(",")[5])
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test SystemDate reported in each premium line
   */
  function testSystemDate() {
    try {
      var sysDate = TDIC_PremiumLinesTestUtil.getSystemDate()
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      prmLinesList.each( \ line -> {
        assertEquals(36,line.split(",").Count)
        assertEquals("\"${sysDate}\"",line.split(",")[6])
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test Reference number reported in each premium line
   */
  function testReferenceNumber() {
    try {
      var periodEndDate = TDIC_PremiumLinesTestUtil.getFormattedDate(new SimpleDateFormat("MMddyy"), endDate)
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        assertTrue(columns[7].contains("CA${periodEndDate}"))
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test description reported in each premium line
   */
  function testDescription() {
    try {
      var periodEndDate = TDIC_PremiumLinesTestUtil.getFormattedDate(new SimpleDateFormat("MM/dd/yyyy"), endDate)
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_EXP_CONSTANT)
            || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_PREMIUM)
            || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_TERRORISM)
        ){
          assertEquals("\"WC PREMIUM FOR CA ${periodEndDate}\"",columns[8])
        }else if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_STATE_ASSMNTS)){
          assertEquals("\"SURCH WC FOR CA ${periodEndDate}\"",columns[8])
        }else{
          assertEquals("\"PREM RCVD FOR CA ${periodEndDate}\"",columns[8])
        }
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test Units Count reported in each premium line
   */
  function testUnitsCount() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_PREMIUM))
          assertEquals("5",columns[10])
        else if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_EXP_CONSTANT)
          || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_TERRORISM)
          || columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_STATE_ASSMNTS)
        ) {
          assertEquals("0",columns[10])
        }
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test Trans amount reported in each premium line
   */
  function testTransAmount() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_PREMIUM))
        {
          assertEquals("-14900.00",columns[11])
        }else if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_EXP_CONSTANT)){
          assertEquals("-433.00",columns[11])
        }else if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_TERRORISM)){
          assertEquals("-540.00",columns[11])
        }else if(columns[4].contains(TDIC_PremiumLinesTestUtil.ACC_NBR_STATE_ASSMNTS)){
          assertEquals("-1490.00",columns[11])
        }
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test posting date reported in each premium line
   */
  function testPostingDate() {
    try {
      var periodEndDate = TDIC_PremiumLinesTestUtil.getFormattedDate(new SimpleDateFormat("MM/dd/yyyy"), endDate)
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        assertEquals("\"${periodEndDate}\"",columns[17])
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test SubLine1 reported in each premium line
   */
  function testSubLine1() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        assertEquals(TDIC_PremiumLinesTestUtil.SUB_LINE1,"${columns[12]},${columns[13]},${columns[14]},${columns[15]},${columns[16]}")
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test SubLine2 reported in each premium line
   */
  function testSubLine2() {
    try {
      var periodEndDate = TDIC_PremiumLinesTestUtil.getFormattedDate(new SimpleDateFormat("MM/dd/yyyy"), endDate)
      var expectedSubLine2 = TDIC_PremiumLinesTestUtil.SUB_LINE2_1+"\"${periodEndDate}\""+TDIC_PremiumLinesTestUtil.SUB_LINE2_2
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        assertEquals(expectedSubLine2,"${columns[18]},${columns[19]},${columns[20]},${columns[21]},${columns[22]},${columns[23]},${columns[24]},${columns[25]},${columns[26]},${columns[27]}")
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test SubLine3 reported in each premium line
   */
  function testSubLine3() {
    try {
      prmLinesList = prmLines.generatePremiumLines(premiums)
      assertNotNull(prmLinesList)
      var columns : String[]
      prmLinesList.each( \ line -> {
        columns = line.split(",")
        assertEquals(36,columns.Count)
        assertEquals(TDIC_PremiumLinesTestUtil.SUB_LINE3,"${columns[29]},${columns[30]},${columns[31]},${columns[32]},${columns[33]},${columns[34]},${columns[35]}")
      })
    }catch(e : Exception) {
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test account code mapping for WC product line
   */
  function testGetMappedLOBAccountCode() {
    try{
      var prodLines = TDIC_PremiumLinesTestUtil.getLOBs()
      prodLines.each( \ lob -> {
        switch(lob){
          case typekey.PolicyLine.TC_WC7WORKERSCOMPLINE.Code :
            assertEquals("40",new TDIC_WrittenPremiumLines(endDate).getMappedLOBAccountCode(lob))
            break
          default :
            assertTrue(Boolean.FALSE)
        }
      })
    }catch(e : Exception){
      fail(e.StackTraceAsString)
    }
  }

  /**
   * This method is to test account code mapping for Jurisdiction state CA
   */
  function testGetMappedJurisdictionAccountCode() {
    try{
      var prodLines = TDIC_PremiumLinesTestUtil.getJurisdictions()
      prodLines.each( \ jur -> {
        switch(jur){
          case typekey.Jurisdiction.TC_CA.Code :
              assertEquals("06",new TDIC_WrittenPremiumLines(endDate).getMappedJurisdictionAccountCode(jur))
              break
          default :
              assertTrue(Boolean.FALSE)
        }
      })
    }catch(e : Exception){
      fail(e.StackTraceAsString)
    }
  }
}