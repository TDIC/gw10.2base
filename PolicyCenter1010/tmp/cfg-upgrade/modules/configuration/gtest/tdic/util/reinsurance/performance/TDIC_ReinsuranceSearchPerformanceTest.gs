package tdic.util.reinsurance.performance

uses tdic.TDICTestBase
uses tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchCriteria
uses org.apache.commons.lang3.time.StopWatch
uses gw.api.database.Query
uses org.slf4j.LoggerFactory

/**
 * US1377
 * ShaneS 04/24/2015
 */
class TDIC_ReinsuranceSearchPerformanceTest extends TDICTestBase {

  var _rilogger = LoggerFactory.getLogger("TDIC_REINSURANCE_SEARCH")

  /**
   * US1377
   * ShaneS 05/18/2015
   */
  function testReinsuranceSearchPerformance(){
    for(0..20){
      doTest(500)
    }
  }

  /**
   * US1377
   * ShaneS 04/24/2015
   */
  private function doTest(numberOfSubmissions : int){
    new tdic.util.reinsurance.performance.TDIC_ReinsuranceSearchPerformanceTestSampleData().create(numberOfSubmissions)

    var addressLine1 = "627 Wisconsin St"
    var productCode = "WC7WorkersComp"
    var product = gw.api.system.PCDependenciesGateway.getProductModel().getAllInstances(gw.api.productmodel.Product).firstWhere( \ elt -> elt.CodeIdentifier == productCode)

    var ri = new TDIC_ReinsuranceSearchCriteria()
    ri.AddressLine1 = addressLine1
    ri.Product = product

    var watch = new StopWatch()
    watch.start()
    var results = ri.overridePerformReinsuranceSearch()
    watch.stop()

    var numberOfPolicies = Query.make(Policy).select().Count

    _rilogger.info("TDIC_ReinsuranceSearchPerformanceTest#testReinsuranceSearchPerformance() - Time to complete: "+ watch.toString() +" with "+ numberOfPolicies +" policies in the databse.")
  }
}