package tdic.plugin.numgen

uses com.guidewire.pl.web.controller.UserDisplayableException
uses gw.plugin.Plugins
uses gw.plugin.policynumgen.IPolicyNumGenPlugin
uses java.lang.Exception
uses gw.api.locale.DisplayKey
uses tdic.pc.integ.plugins.numgen.TDICPolicyNumGenPlugin

/**
 * US679
 * 11/06/2014 Rob Kelly
 *
 * Tests the functionality of <code>TDICPolicyNumGenPlugin</code>.
 */
//@gw.testharness.ServerTest
class TDICPolicyNumGenPluginTest extends tdic.TDICTestBase {

  private static final var POLICY_NUM_SEQUENCE_NAME = "policy_num"

  construct() {

  }

  public function testGenNewPeriodPolicyNumberWithNullSequence() {

    try {

      var policyNumPlugin = new TDICPolicyNumGenPlugin()
      policyNumPlugin.genNewPeriodPolicyNumber(null)
      fail("failure - exception should be thrown for null sequence")
    }
    catch (ude : UserDisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, DisplayKey.get("TDIC.NumberGenerator.Policy.NoSequence"), ude.Message)
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for null sequence: ${e.getMessage()}")
    }
  }

  public function testGenNewPeriodPolicyNumberWithNullPolicyPeriod() {

    try {

      var policyNumPlugin = Plugins.get(IPolicyNumGenPlugin)
      policyNumPlugin.genNewPeriodPolicyNumber(null)
      fail("failure - exception should be thrown for null policy period")
    }
    catch (ude : UserDisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, DisplayKey.get("TDIC.NumberGenerator.Policy.NullPolicyPeriod"), ude.Message)
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for null policy period: ${e.getMessage()}")
    }
  }

  public function testGenNewPeriodPolicyNumberWithFirstNewSubmission() {

    var policyNumPlugin = Plugins.get(IPolicyNumGenPlugin)
    setSequenceNumber(POLICY_NUM_SEQUENCE_NAME, 8000)
    var expectedPolicyNumber = "0000008001"
    var policyNum = policyNumPlugin.genNewPeriodPolicyNumber(createNewSubmission())
    assertEquals("failure - Policy number incorrect", expectedPolicyNumber, policyNum)
  }

  public function testGenNewPeriodPolicyNumberWithRandomNewSubmission() {

    var policyNumPlugin = Plugins.get(IPolicyNumGenPlugin)
    setSequenceNumber(POLICY_NUM_SEQUENCE_NAME, 0177802)
    var expectedPolicyNumber = "0000177803"
    var policyNum = policyNumPlugin.genNewPeriodPolicyNumber(createNewSubmission())
    assertEquals("failure - Policy number incorrect", expectedPolicyNumber, policyNum)
  }

  public function testGenNewPeriodPolicyNumberWithPolicyChange() {

    var expectedPolicyNumber = "00881188"
    var policyNumPlugin = Plugins.get(IPolicyNumGenPlugin)
    var policyNum = policyNumPlugin.genNewPeriodPolicyNumber(createPolicyChange(expectedPolicyNumber))
    assertEquals("failure - Policy number incorrect", expectedPolicyNumber, policyNum)
  }

  public function testGenNewPeriodPolicyNumberWithRenewal() {

    var expectedPolicyNumber = "00991199"
    var policyNumPlugin = Plugins.get(IPolicyNumGenPlugin)
    var policyNum = policyNumPlugin.genNewPeriodPolicyNumber(createPolicyRenewal(expectedPolicyNumber))
    assertEquals("failure - Policy number incorrect", expectedPolicyNumber, policyNum)
  }
}