package tdic.reinsurance.search

uses tdic.pc.config.reinsurancesearch.TDIC_ReinsuranceSearchHelper

/**
 * US1378
 * ShaneS 05/18/2015
 */
class TDIC_ReinsuranceSearchHelperTest extends tdic.TDICTestBase {

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_withNoConversionNeededExceptWhiteSpace(){
    var addressLine1 = "26 M St"
    var expectedResult = "26MST"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, false)
    assertEquals("Address was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_withWordNeedingStandardization(){
    var addressLine1 = "1201 K Str"
    var expectedResult = "1201KST"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, false)
    assertEquals("Address was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_withPunctuation(){
    var addressLine1 = "12(01), K-St"
    var expectedResult = "1201KST"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, false)
    assertEquals("Address was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_withAdditionalAddressInfoWithWhiteSpace(){
    var addressLine1 = "1201 K St # 14"
    var expectedResult = "1201KST"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, false)
    assertEquals("Address was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_withAdditionalAddressInfoWithoutWhiteSpace(){
    var addressLine1 = "1201 K St Ste3"
    var expectedResult = "1201KST"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, false)
    assertEquals("Address was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_withAdditionalAddressInfoAndPunctuation(){
    var addressLine1 = "1201 K St, Room 5"
    var expectedResult = "1201KST"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, false)
    assertEquals("Address was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_withAdditionalAddressInfoAndPunctuationAndStandardization(){
    var addressLine1 = "25 3rd Avenue Parkway, Room 5"
    var expectedResult = "253RDAVE"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, false)
    assertEquals("Address was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_userDisplayableKeepsWhiteSpace(){
    var addressLine1 = "26 M St"
    var expectedResult = addressLine1

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, true)
    assertEquals("Address was not converted as expected (user displayable).", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_userDisplayableWithPunctuation(){
    var addressLine1 = "26, M St"
    var expectedResult = "26 M St"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, true)
    assertEquals("Address was not converted as expected (user displayable).", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_userDisplayableWithStandardizationNeeded(){
    var addressLine1 = "1201 K Str"
    var expectedResult = "1201 K St"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, true)
    assertEquals("Address was not converted as expected (user displayable).", expectedResult, actualResult)
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  function testConvertAddress_userDisplayableWithAdditionalAddressInfoAndPunctuationAndStandardization(){
    var addressLine1 = "25 3rd Avenue Parkway, Room 5"
    var expectedResult = "25 3rd Ave"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertAddress(addressLine1, true)
    assertEquals("Address was not converted as expected (user displayable).", expectedResult, actualResult)
  }

  /**
   * DE264
   * ShaneS 05/21/2015
   */
  function testConvertZipCode_withNoHyphen(){
    var zip = "95648"
    var expectedResult = "95648"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertZipCode(zip)
    assertEquals("Zip code was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * DE264
   * ShaneS 05/21/2015
   */
  function testConvertZipCode_withNoHyphenAndNoTrailingDigits(){
    var zip = "95648-"
    var expectedResult = "95648"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertZipCode(zip)
    assertEquals("Zip code was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * DE264
   * ShaneS 05/21/2015
   */
  function testConvertZipCode_withHyphenAndTrailingDigits(){
    var zip = "95648-8807"
    var expectedResult = "95648"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertZipCode(zip)
    assertEquals("Zip code was not converted as expected.", expectedResult, actualResult)
  }

  /**
   * DE264
   * ShaneS 05/21/2015
   */
  function testConvertZipCode_withHyphenAndTrailingDigitsAndWhiteSpace(){
    var zip = "95648 - 8807"
    var expectedResult = "95648"

    var actualResult = TDIC_ReinsuranceSearchHelper.convertZipCode(zip)
    assertEquals("Zip code was not converted as expected.", expectedResult, actualResult)
  }
}