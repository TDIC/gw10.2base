package com.tdic.plugins.hpexstream.core.util

uses gw.pl.persistence.core.Bundle

interface TDIC_Helper {

  abstract function createStub(aTemplateId: String, aPrintOrder: int, anEntity: Object, eventName: String, user: String)

  abstract function skipDocCreation(aTemplateId: String, anEntity: Object, aBundle: Bundle)

}