package com.tdic.pc.common.base.dao

uses com.tdic.pc.common.base.dbconnection.DBConnection
uses com.tdic.util.properties.PropertyUtil
uses org.slf4j.Logger

uses java.sql.Connection

/**
 * JDBC DAO Framework class. This class provides function to get DB Connection.
 */
abstract class AbstractJdbcDAO {
  protected function getConnection(aLogger: Logger): Connection {
    return DBConnection.getConnection(PropertyUtil.getInstance(aLogger).getProperty(getConnectionProperty()), aLogger)
  }

  /**
   * Gets the connection property.
   */
  @Returns("The connection property")
  protected abstract function getConnectionProperty(): String
}
