/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package com.tdic.plugins.hpexstream.core.bo.commandcenter

/**
 * Data transfer object that represents a response retrieved from Command Center's Data Channel.
 *
 * @author Miro Kubicek
 */
class TDIC_CommandCenterDCResponse {
  var _documentList: java.util.ArrayList<TDIC_DCResponseDocument> as DocumentList
  construct() {
    _documentList = new java.util.ArrayList<TDIC_DCResponseDocument> ()
  }
}