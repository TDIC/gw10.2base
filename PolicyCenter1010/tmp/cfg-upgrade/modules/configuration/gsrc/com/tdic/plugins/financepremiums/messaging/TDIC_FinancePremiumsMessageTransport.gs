package com.tdic.plugins.financepremiums.messaging

uses gw.api.util.DateUtil
uses gw.plugin.InitializablePlugin
uses org.slf4j.LoggerFactory
uses tdic.pc.integ.plugins.message.TDIC_MessagePlugin

uses java.text.DecimalFormat

class TDIC_FinancePremiumsMessageTransport extends TDIC_MessagePlugin implements InitializablePlugin {
  public static final var DEST_ID : int = 20

  private static final var CLASS_NAME = TDIC_FinancePremiumsMessageTransport.Type.RelativeName

  private static final var logger = LoggerFactory.getLogger("TDIC_FINANCE_PREMIUMS")

  override function send(message : Message, transformedPayload : String) {
    var policyPeriod = message.PolicyPeriod
    logger.info("${CLASS_NAME}#send(): ${message.EventName}:${policyPeriod.PolicyNumber}-${policyPeriod.TermNumber}"+ " ${message.MessageRoot} - start")
    try{
     // generatePremiums(policyPeriod)
      message.reportAck()
      //message.reportError()//TODO temporarty
    }catch(e : Exception) {
      logger.error("Finance Premiums message transport error: ", e)
      message.ErrorDescription = e.Message
      message.reportError()
    }
    logger.info("${CLASS_NAME}#send(): ${message.EventName}:${policyPeriod.PolicyNumber}-${policyPeriod.TermNumber}"+ " ${message.MessageRoot} - end")
  }



  override function shutdown() {
    logger.info("${CLASS_NAME}#shutdown() - start")
    // Do Nothing
    logger.info("${CLASS_NAME}#shutdown() - end")
  }

  override function resume() {
    logger.info("${CLASS_NAME}#resume() - start")
    // Do Nothing
    logger.info("${CLASS_NAME}#resume() - end")
  }

  override property set Parameters(map : Map) {

  }

}