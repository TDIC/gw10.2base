package com.tdic.plugins.hpexstream.core.bo

class TDIC_TemplateidToDocPubIdMap {

  private var _templateId: String as TemplateId
  private var _pendingDocUID: String as PendingDocUID
  private var _documentPublicId: String as DocUID
  private var _documentType: String as DocType
  private var _mimeType: String as MimeType
  private var _author: String as Author
  private var _publicId: String as PublicId

  construct(aTemplateId: String, aDocument: Document) {
    _templateId = aTemplateId
    if(aDocument != null){
      _pendingDocUID = aDocument.PendingDocUID
      _documentPublicId = aDocument.DocUID
      _documentType = aDocument.Type.DisplayName
      _mimeType = aDocument.MimeType
      _author = aDocument.Author
      _publicId = aDocument.PublicID
    }
  }

}