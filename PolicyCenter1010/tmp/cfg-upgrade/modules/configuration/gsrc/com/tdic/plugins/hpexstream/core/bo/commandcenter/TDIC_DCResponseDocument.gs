/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package com.tdic.plugins.hpexstream.core.bo.commandcenter

uses java.io.InputStream

class TDIC_DCResponseDocument {
  private var _documentName: String as DocumentName
  private var _documentContent: byte[] as DocumentContent
  private var _inputStream: InputStream as DocumentInputStream
  construct() {
  }

  construct(aDocumentName: String, aDocumentContent: byte[]) {
    _documentName = aDocumentName
    _documentContent = aDocumentContent
  }

  construct(name: String, content: InputStream) {
    _documentName = name
    _inputStream = content
  }
}
