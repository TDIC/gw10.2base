/**
This is batch process that clears out completed workflow logs.  It expects upto
two parameters the days for successful processes, the batch size.

Things to note:
This will throw an exception out of the plugin if the args are not correct
That doWork does not have a bundle so it uses Transaction.runWithBundle to obtain a bundle
It uses the default check initial conditions
*/
package gw.processes

uses gw.api.system.PLConfigParameters
uses gw.api.admin.WorkflowUtil
uses gw.api.upgrade.Coercions
uses gw.api.database.Query
uses typekey.Workflow

@Export
class PurgeWorkflows extends BatchProcessBase
{
  var _succDays = PLConfigParameters.WorkflowPurgeDaysOld.Value

  construct() {
    this(null)
  }
  
  construct(arguments : Object[]) {
    super(TC_PURGEWORKFLOWS)
    if (arguments != null) {
      _succDays = arguments[1] != null ? (Coercions.makeIntFrom(arguments[1])) : _succDays
    }
  }

  override property get Description() : String {
    return "purge(daysOld=${_succDays})"
  }

  override function doWork() : void {
    deleteAssociatedDocuments_TDIC()
    OperationsCompleted = WorkflowUtil.deleteOldWorkflowsFromDatabase( _succDays )
  }

  /**
   * This method will delete Related AssctdDocument Entries for all the Workflows the Batch job is going to Purge
   */
  function deleteAssociatedDocuments_TDIC() {
    var workflowQuery = Query.make(entity.Workflow)
    workflowQuery.compare("State", Equals, WorkflowState.TC_COMPLETED)
    workflowQuery.compare("Subtype", Equals, Workflow.TC_DOCUMENTAPPROVALWF_TDIC)
    workflowQuery.compare("UpdateTime", LessThan, Date.CurrentDate.addDays(-_succDays))
    var workflows = workflowQuery.select().toList()
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      for (wrkFlow in workflows) {
        var assctdDocumentQueryuery = Query.make(AssctdDocument)
        assctdDocumentQueryuery.compare("Owner", Equals, wrkFlow.ID)
        var result = assctdDocumentQueryuery.select().first()
        if (result != null) {
          bundle.delete(result)
        }
      }
    })
  }
}
