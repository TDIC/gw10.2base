/**
This is batch process that clears out completed workflow logs.  It expects upto
two parameters the days for successful processes, the batch size.

Things to note:
This will throw an exception out of the plugin if the args are not correct
That doWork does not have a bundle so it uses Transaction.runWithBundle to obtain a bundle
It uses the default check initial conditions
*/
package gw.processes

uses gw.api.database.IQueryBeanResult
uses gw.api.database.InOperation
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.system.PLConfigParameters
uses gw.api.util.DateUtil

@Export
class PurgeWorkflowLogs extends PurgeProcessBase
{
  construct() {
    this({PLConfigParameters.WorkflowLogPurgeDaysOld.Value})
  }

  construct(daysOld : int, batchSize : int) {
    this({daysOld, batchSize})
  }
  
  private construct(arguments : Object[]) {
    super(TC_PURGEWORKFLOWLOGS, arguments)
  }

  override function getQueryToRetrieveOldEntries( daysOld : int ) : IQueryBeanResult<KeyableBean> {
    deleteDocumentApprovalWF_TDIC(daysOld)
    return Query.make(WorkflowLogEntry).and(\r -> {
      r.compare("LogDate", Relop.LessThan, DateUtil.currentDate().addDays(- daysOld))
      r.subselect("Workflow", InOperation.CompareIn, Query.make(Workflow), "ID")
          .compare("State", Relop.Equals, WorkflowState.TC_COMPLETED)
          .compare("Subtype", Relop.NotEquals, typekey.Workflow.TC_DOCUMENTAPPROVALWF_TDIC)
    }).select()
  }

  /**
   * This method will delete Related DOCUMENTAPPROVALWF_TDIC Entries for all the Workflows the Batch job is going to Purge
   */
  function deleteDocumentApprovalWF_TDIC(daysOld : int) : void {
    var resultList = Query.make(WorkflowLogEntry).and(\r -> {
      r.compare("LogDate", Relop.LessThan, DateUtil.currentDate().addDays(- daysOld))
      r.subselect("Workflow", InOperation.CompareIn, Query.make(Workflow), "ID")
          .compare("State", Relop.Equals, WorkflowState.TC_COMPLETED)
          .compare("Subtype", Relop.Equals, typekey.Workflow.TC_DOCUMENTAPPROVALWF_TDIC)
    }).select()
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      for (result in resultList) {
        if (result != null) {
          bundle.delete(result)
        }
      }
    })
  }

}
