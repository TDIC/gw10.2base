package gw.pcf

uses gw.entity.TypeKey
uses gw.util.TypekeyUtil
uses java.util.List

class WC7ModifiersInputSetUIHelper extends WC7AbstractModifiersInputSetUIHelper {

  function typekeyModifierAvailable(modifier : Modifier, period : PolicyPeriod) : boolean {
    // has more specific functionality in standards-based
    return true
  }

  function typekeyModifierDisablePostOnChange(modifier : Modifier) : boolean {
    // has more specific functionality in standards-based
    return true
  }

  function rateModifierAvailable(modifier : Modifier, period : PolicyPeriod) : boolean{
    // has more specific functionality in standards-based
    return true
  }

  function rateModifierDisablePostOnChange(modifier : Modifier) : boolean {
    // has more specific functionality in standards-based
    return true
  }

  override function filterTypekeyModifier(modifier : Modifier) : List<TypeKey>{
    return TypekeyUtil.getTypeKeys(modifier.TypeList).toList().cast(TypeKey)
  }

  override function updateModifierDependencies(modifier : Modifier) {
    if (modifier.PatternCode == "WC7ExpMod") {
      updateExperienceModStatus(modifier)
    }
  }

  override function filterTypeKeyExperienceModifierStatus(modifier : WC7Modifier) : List<TypeKey> {
    return WC7ExpModStatus.TF_NONCONTINGENTSTATES.TypeKeys
  }
}
