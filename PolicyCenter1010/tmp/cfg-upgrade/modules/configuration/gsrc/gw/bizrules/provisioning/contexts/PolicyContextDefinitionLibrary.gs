package gw.bizrules.provisioning.contexts

uses com.fasterxml.jackson.databind.node.BooleanNode
uses gw.api.database.InOperation
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.financials.IMoney
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
uses gw.api.util.DateUtil
uses gw.api.util.FXRateUtil
uses gw.api.web.dashboard.ui.claims.ClaimHelper
uses gw.pl.currency.MonetaryAmount
uses tdic.pc.config.transientpolicy.TransientPriorPolicy_TDIC
uses tdic.pc.integ.plugins.policyperiod.TDIC_PolicyPeriodIsMultiline
uses typekey.Currency
uses typekey.Job

uses java.math.BigDecimal

@Export
class PolicyContextDefinitionLibrary {

  final var IL_MINE_SUBSIDENCE_COUNTIES_TDIC = {
      "Bond", "Bureau", "Christian", "Clinton", "Douglas", "Franklin", "Fulton", "Gallatin", "Grundy", "Jackson", "Jefferson",
      "Knox", "LaSalle", "Logan", "McDonough", "Macoupin", "Madison", "Marion", "Marshall", "Menard", "Mercer", "Montgomery",
      "Peoria", "Perry", "Putnam", "Randolph", "Rock Island", "St. Clair", "Saline", "Sangamon", "Tazewell", "Vermilion",
      "Washington", "Williamson"}
  final var EPL_CLAIM_TYPE = "Employment Practices"
  final var CYB_CLAIM_TYPE = "Cyber Liability"

  function localize(key : String) : String {
    return DisplayKey.get(key)
  }

  function localize(key : String, p : Object) : String {
    return DisplayKey.get(key, p)
  }

  function localize(key : String, p1 : Object, p2 : Object) : String {
    return DisplayKey.get(key, p1, p2)
  }

  function localize(key : String, p1 : Object, p2 : Object, p3 : Object) : String {
    return DisplayKey.get(key, p1, p2, p3)
  }

  function renderAsCurrency(value : IMoney) : String {
    return CurrencyUtil.renderAsCurrency(value)
  }

  function renderAsCurrency(amount : BigDecimal, currency : Currency) : String {
    return CurrencyUtil.renderAsCurrency(amount, currency)
  }

  function monetaryAmount(amount : BigDecimal, currency : Currency) : MonetaryAmount {
    return new MonetaryAmount(amount, currency)
  }

  function hasGoodDriverDiscount(driver : PolicyDriver) : boolean {
    return (driver.AccountContactRole.AccountContact.getRole(TC_DRIVER) as Driver).GoodDriverDiscount
  }

  function convertAmount(amount : MonetaryAmount, toCurrency : Currency) : MonetaryAmount {
    return FXRateUtil.convertAmount(amount, toCurrency)
  }

  function currentDate() : Date {
    return Date.Today
  }

  /*
   * create by: SureshB
   * @description: method to get the answer for BOPBuilding UW question
   * @create time: 5:17 PM 9/6/2019
   */
  function getAnswerForBOPBuildingQuestion_TDIC(policyPeriod : PolicyPeriod, question : String) : boolean {
    if (policyPeriod.BOPLineExists) {
      var answers = policyPeriod.getAnswerFromBOPBuildingUWQuestionIfQuestionVisible_TDIC(question)
      if (answers.Count > 0) {
        return answers?.hasMatch(\answer -> answer?.BooleanAnswer)
      }
    }
    return false
  }
    /*
   * create by: SureshB
   * @description: method to get the answer for BOPBuilding UW question(Integer Type)
   * @create time: 5:17 PM 9/6/2019
   */

  function getAnswerForBOPBuildingIntegerQuestion_TDIC(policyPeriod : PolicyPeriod, question : String) : Integer[] {
    if (policyPeriod.BOPLineExists) {
      var answers = policyPeriod.getAnswerFromBOPBuildingUWQuestionIfQuestionVisible_TDIC(question)
      if (answers.Count > 0) {
        return answers*.IntegerAnswer
      }
    }
    return {}
  }
      /*
   * create by: SureshB
   * @description: method to get the answer for BOPBuilding UW question(Dropdown Type)
   * @create time: 5:17 PM 9/6/2019
   */

  function getAnswerForBOPBuildingDropdownQuestion_TDIC(policyPeriod : PolicyPeriod, question : String) : String[] {
    if (policyPeriod.BOPLineExists) {
      var answers = policyPeriod.getAnswerFromBOPBuildingUWQuestionIfQuestionVisible_TDIC(question)
      if (answers.Count > 0) {
        return answers*.ChoiceAnswer*.ChoiceCode
      }
    }
    return {}
  }
    /*
   * create by: SureshB
   * @description: method to get the visibility of the BopBuilding UW question
   * @create time: 5:17 PM 9/6/2019
   */
  function isBOPBuildingUWQuestionVisible_TDIC(policyPeriod : PolicyPeriod, question : String) : boolean {
    if (policyPeriod.BOPLineExists) {
      return policyPeriod.isBOPBuildingUWQuestionVisible_TDIC(question)
    }
    return false
  }

  /*
   * create by: Jeff Lin
   * @description: CP/BOP: method to determine whether the building CP/BOP building of question, "List types of occupancies if other than medical/dental offices" has value.
   * @create time: 2:50 PM 3/3/2020
   */
  function hasListTypesOccupanciesNonMedicalDental(policyPeriod : PolicyPeriod) : boolean {
    if (policyPeriod.Offering.CodeIdentifier != "BOPBusinessOwnersPolicy_TDIC") {
      return false
    }
    if (isBOPBuildingUWQuestionVisible_TDIC(policyPeriod, "OccupanciesOtherThanMedicalDental")) {
      var qSet = policyPeriod.PrimaryLocation.QuestionSets.firstWhere(\elt -> elt.CodeIdentifier == "BOPBuildingSupp")
      var question = qSet.getQuestionByCodeIdentifier("OccupanciesOtherThanMedicalDental")
      return policyPeriod.PrimaryLocation.getAnswer(question)?.TextAnswer.HasContent
    }
    return false
  }

  /*
   * create by: Jeff Lin
   * @description: CP/LRP: method to determine whether the building CP/LRP building of question, both questions, Ocupencies By Others and All Occupancies Medical/Dental Offices? are answered Yes
   * @create time: 2:50 PM 3/3/2020
   */
  function isAllOccupanciesOtherThanMedicalDental(policyPeriod : PolicyPeriod) : boolean {
    if (policyPeriod.Offering.CodeIdentifier != "BOPLessorsRisk_TDIC") {
      return false
    }
    var primaryLoc = policyPeriod.PrimaryLocation
    var qSet = primaryLoc.QuestionSets.firstWhere(\elt -> elt.CodeIdentifier == "BOPBuildingSupp")
    var question = qSet.getQuestionByCodeIdentifier("OcupenciesByOthers")
    if (primaryLoc.getAnswer(question)?.BooleanAnswer) {
      var question2 = qSet.getQuestionByCodeIdentifier("AllOccupanciesMedicalDentalOffices")
      return primaryLoc.getAnswer(question2)?.BooleanAnswer
    }
    return false
  }

  /*
   * create by: SureshB
   * @description: method to determine whether the building is 30 years old
   * @create time: 8:57 PM 9/6/2019
   */

  function isBOPBuildingOver30YearsOld_TDIC(policyPeriod : PolicyPeriod) : boolean {
    if (policyPeriod.BOPLineExists) {
      return policyPeriod.BOPLine.BOPLocations*.Buildings?.hasMatch(\building -> building.isBuildingOver30YearsOld_TDIC())
    }
    return false
  }
  /*
   * create by: SureshB
   * @description: method to get the Year Built of BOPBuilding if its 30 years old
   * @create time: 9:35 PM 9/6/2019
   * update jeera ticket 2419
   */

  function getBopBuildingYearOfBuiltIfOver30YearsOld_TDIC(policyPeriod : PolicyPeriod) : Integer {
    var building = (policyPeriod.BOPLine?.BOPLocations*.Buildings?.firstWhere(\building -> building.isBuildingOver30YearsOld_TDIC()))?.Building
    var currentYear = DateUtil.getYear(DateUtil.currentDate())
    if (policyPeriod.BOPLineExists) {
      if (policyPeriod.Offering?.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
        if (building != null and building.BuildingImprovement_TDIC == false) {
          if (building.Roofing?.YearAdded != null and building.Plumbing?.YearAdded != null and building.Wiring?.YearAdded != null and
              (building.Roofing?.YearAdded >= currentYear - 10) and (building.Plumbing?.YearAdded >= currentYear - 10) and (building.Wiring?.YearAdded >= currentYear - 10)) {
            return null
          } else {
            return building?.YearBuilt
          }
        }
      }
      if (policyPeriod.Offering?.CodeIdentifier == "BOPLessorsRisk_TDIC") {
        if (building != null and (building.Roofing?.YearAdded != null and building.Plumbing?.YearAdded != null and building.Wiring?.YearAdded != null and
            (building.Roofing?.YearAdded >= currentYear - 10) and (building.Plumbing?.YearAdded >= currentYear - 10) and (building.Wiring?.YearAdded >= currentYear - 10))) {
          return null
        } else {
          return building?.YearBuilt
        }

      }
    }

    return null
  }
  /*
   * create by: SureshB
   * @description: method to get the occupancy status if the building occupancy status is {'Condominium' or 'Triple Net Lease' or 'not currently occupying building'}
   * @create time: 4:52 PM 9/18/2019
   */

  function getBOPBuildingOccupancyStatusIfCondTripleNetNot_TDIC(policyPeriod : PolicyPeriod) : String {
    var statusList = {BOPOccupancyStatus_TDIC.TC_CONDO, BOPOccupancyStatus_TDIC.TC_TRIPLE}
    if (policyPeriod.BOPLineExists) {
      return policyPeriod.BOPLine.BOPLocations*.Buildings.firstWhere(\building -> statusList.contains(building.Building.BOPOccupancyStatus_TDIC))
          .Building.BOPOccupancyStatus_TDIC.DisplayName
    }
    return null
  }
  /*
   * create by: SureshB
   * @description: method to check if PC can trigger Detained Claim UW issue(3 or more claims in five years or one claim greater than $500,000 (TDIC claims history))
   * @create time: 4:57 PM 9/18/2019
   */

  function canDetainedClaimsUWIssueRasied_TDIC(policyPeriod : PolicyPeriod) : boolean {
    var claimHelper = new ClaimHelper(policyPeriod.Policy)
    var hasClaim500000 = claimHelper.OpenClaims?.hasMatch(\elt1 -> elt1.TotalIncurred?.toNumber() > 500000)
    var hasMoreThan3ClaimsIn5Years = claimHelper.OpenClaims?.where(\elt1 -> Date.CurrentDate.YearOfDate - elt1?.LossDate.YearOfDate <= 5).Count > 3
    return hasClaim500000 and hasMoreThan3ClaimsIn5Years
  }

  /*
   * create by: Jeff Lin
   * @description: Check to determine the Anesthetic Modality Selection with Specialty Code and Class Code combination are correct or not
   * @create time: 4:57 PM 10/25/2019
   */
  function isAnestheticModalitySelectionCorrect(period : PolicyPeriod) : boolean {
    var exposures = period.GLLine.Exposures

      if (period.GLLine.getAnswerForGLUWQuestion_TDIC("Conscioussedation") or
          period.GLLine.getAnswerForGLUWQuestion_TDIC("Conscioussedationoffice") or
          period.GLLine.getAnswerForGLUWQuestion_TDIC("Generalanesthesiaoffice")) {
        if (exposures.hasMatch(\elt -> elt.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_00GENERALDENTIST_TDIC and elt.ClassCode_TDIC == ClassCode_TDIC.TC_11)
            or exposures.hasMatch(\exp -> exp.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_10ORALSURGERY_TDIC and exp.ClassCode_TDIC == ClassCode_TDIC.TC_01)
            or exposures.hasMatch(\exp -> exp.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_15ENDODONTICS_TDIC and exp.ClassCode_TDIC == ClassCode_TDIC.TC_01)
            or exposures.hasMatch(\exp -> exp.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_20ORTHODONTICS_TDIC and exp.ClassCode_TDIC == ClassCode_TDIC.TC_01)
            or exposures.hasMatch(\exp -> exp.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_30PEDIATRICDENTISTRY_TDIC and exp.ClassCode_TDIC == ClassCode_TDIC.TC_01)
            or exposures.hasMatch(\exp -> exp.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_40PERIODONTICS_TDIC and exp.ClassCode_TDIC == ClassCode_TDIC.TC_01)
            or exposures.hasMatch(\exp -> exp.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_50PROSTHODONTICS_TDIC and exp.ClassCode_TDIC == ClassCode_TDIC.TC_01)
            or exposures.hasMatch(\exp -> exp.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_60ORALPATHOLOGY_TDIC and exp.ClassCode_TDIC == ClassCode_TDIC.TC_01)
            or exposures.hasMatch(\exp -> exp.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_90DENTALANESTHESIOLOGY_TDIC and exp.ClassCode_TDIC == ClassCode_TDIC.TC_01))
        {
          return false
        }
      }
      if (period.GLLine.getAnswerForGLUWQuestion_TDIC("Conscioussedationoffice") or
          period.GLLine.getAnswerForGLUWQuestion_TDIC("Generalanesthesiaoffice")) {
        if (exposures.hasMatch(\elt -> elt.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_00GENERALDENTIST_TDIC and
            elt.ClassCode_TDIC == ClassCode_TDIC.TC_20))
        {
          return false
        }
      }
      if (period.GLLine.getAnswerForGLUWQuestion_TDIC("Generalanesthesiaoffice")) {
        if (exposures.hasMatch(\elt -> elt.SpecialityCode_TDIC == SpecialityCode_TDIC.TC_00GENERALDENTIST_TDIC and
            elt.ClassCode_TDIC == ClassCode_TDIC.TC_40))
        {
          return false
        }
      }
    return true
  }

  /*
   * create by: Jeff Lin
   * @description: Check if the first dentist license year within the lat 12 months and its year is not equal to the policy year return true to trigger the UW Issue.
   * @create time: 3:12 PM 3/9/2020
   */
  function isFirstLicensedYearNotEqualToPolicyEffYear(policyPeriod : PolicyPeriod) : boolean {
    if (not (policyPeriod.Offering?.CodeIdentifier == "PLClaimsMade_TDIC" or policyPeriod.Offering?.CodeIdentifier == "PLOccurence_TDIC")) {
      return false
    }
    var qSet = policyPeriod.GLLine.QuestionSets.firstWhere(\elt -> elt.CodeIdentifier == "GLUnderwriting")
    var question = qSet.getQuestionByCodeIdentifier("LicensedFirstTimeInLast12Months_TDIC")
    if (policyPeriod.GLLine.getAnswer(question)?.BooleanAnswer) {
      var question3 = qSet.getQuestionByCodeIdentifier("YearGraduated_TDIC")
      var yearGranted : Integer = null
      if ((policyPeriod.GLLine.getAnswer(question3)?.AnswerValue as Integer) != null) {
        yearGranted = policyPeriod.GLLine.getAnswer(question3)?.AnswerValue as Integer
      }
      return (yearGranted != policyPeriod.EditEffectiveDate.YearOfDate)
    }
    return false
  }

  /*
   * create by: Jeff Lin
   * @description: Check if it can deatin the 2 or more EPL Claims in the five years
   * @create time: 4:57 PM 10/25/2019
   */
  function canDetainedTwoOrMoreEPLClaimsIn5Years_TDIC(policyPeriod : PolicyPeriod) : boolean {
    if (policyPeriod.Offering?.CodeIdentifier != "PLClaimsMade_TDIC" and policyPeriod.Offering?.CodeIdentifier != "PLOccurence_TDIC") {
      return false
    }
    var claimHelper = new ClaimHelper(policyPeriod.Policy)
    var claims = claimHelper.Claims.where(\elt -> elt.ClaimType_TDIC == EPL_CLAIM_TYPE)
    return claims.HasElements and claims.countWhere(\elt -> elt.LossDate != null and elt.LossDate.afterOrEqual(policyPeriod.PeriodStart.addYears(-5).trimToMidnight())) >= 2
  }

  /*
   * create by: Jeff Lin
   * @description: Check if it can deatin the 2 or more CYB Claims in the five years
   * @create time: 4:57 PM 10/25/2019
   */
  function canDetainedTwoOrMoreCYBClaimsIn5Years_TDIC(policyPeriod : PolicyPeriod) : boolean {
    if (policyPeriod.Offering?.CodeIdentifier != "PLCyberLiab_TDIC") {
      return false
    }
    var claimHelper = new ClaimHelper(policyPeriod.Policy)
    var claims = claimHelper.Claims.where(\elt -> elt.ClaimType_TDIC == CYB_CLAIM_TYPE)
    return claims.HasElements and claims.countWhere(\elt -> elt.LossDate != null and elt.LossDate.afterOrEqual(policyPeriod.PeriodStart.addYears(-5).trimToMidnight())) >= 2
  }

  /*
   * create by: Jeff Lin
   * @description: Check if there exists a BOP policy with the same primary name insured and expiration date for this CYB submission
   * @create time: 2:15 PM 11/07/2019
   */
  function hasSameBOPPolicyExpDateForCYB(policyPeriod : PolicyPeriod) : boolean {
    if (policyPeriod.Offering?.CodeIdentifier == "PLCyberLiab_TDIC") {
      var qryResults = Query.make(PolicyPeriod)
          .compare(PolicyPeriod#PrimaryInsuredName, Equals, policyPeriod.PrimaryInsuredName)
          .compare(PolicyPeriod#Status, Relop.Equals, PolicyPeriodStatus.TC_BOUND)
          .compare(PolicyPeriod#PeriodEnd, Relop.Equals, policyPeriod.PeriodEnd)
          .join(PolicyPeriod#Policy).compare(Policy#ProductCode, Relop.Equals, "BusinessOwners")
          .select()
      if(qryResults.HasElements){
        return true
      }
    }
    return false
  }

  /*
   * create by: Jeff Lin
   * @description: Check if the Base state is IL and its county is the following ones
   * @create time: 10:07 AM 01/02/2021
   */
  function hasILMineSubsidenceCounty_TDIC(period : PolicyPeriod) : boolean {
    if(period.BOPLineExists and period.BaseState == Jurisdiction.TC_IL  and (period.Job.Subtype == Job.TC_POLICYCHANGE or period.Job.Subtype == Job.TC_RENEWAL)){
      var policyLocations = period.BOPLine.BOPLocations.where(\elt -> elt.PolicyLocation.LocationNum
          > period.BasedOn.BOPLine.BOPLocations.sortByDescending(\location ->location.PolicyLocation.LocationNum).first().PolicyLocation.LocationNum)
      var loc = policyLocations.firstWhere(\elt -> elt.PolicyLocation.County != null)
      if(policyLocations.HasElements and loc.PolicyLocation.County != null){
        return IL_MINE_SUBSIDENCE_COUNTIES_TDIC.contains(loc.PolicyLocation.County.trim())
      }
    }
    else if ((period.Job.Subtype == Job.TC_SUBMISSION or period.Job.Subtype == Job.TC_REWRITE) and period.BOPLineExists and period.BaseState == Jurisdiction.TC_IL && period.PolicyAddress.Address.County!=null) {
      return IL_MINE_SUBSIDENCE_COUNTIES_TDIC.contains(period.PolicyAddress.Address.County.trim())
    }
    return false
  }

  private function hasILMineSubsidenceCov(bopLine : BOPLine) : boolean {
    return bopLine.BOPLocations.hasMatch(\loc -> loc.Buildings.hasMatch(\bldg -> bldg.BOPILMineSubCov_TDICExists))
  }

  function findLocationNumHavingMoreThan7Bldg(period : PolicyPeriod) : String {
    if((period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) or (period.Job.Subtype == typekey.Job.TC_RENEWAL)){

      var applicableBuildings = period.BOPLine?.BOPLocations*.Buildings?.where(\building -> {
        return
            building?.BOPBuildingCovExists && ((building?.BasedOn?.Building?.NumStories <= 7 && building?.Building?.NumStories > 7)
                || (building?.BasedOn == null && building?.Building?.NumStories > 7) ||
                (building?.BasedOn?.Building?.NumStories > 7 && !building?.BasedOn?.BOPBuildingCovExists && building?.Building?.NumStories > 7))
      })
      if(applicableBuildings?.HasElements){
        var sb = new StringBuilder()
        for(building in applicableBuildings){
          if(sb.isEmpty()){
            sb.append(building?.BOPLocation?.Location?.LocationNum +" ")
          }else{
            sb.append(", ")
            sb.append(building?.BOPLocation?.Location?.LocationNum + " ")
          }
          sb.toString()
        }
        return sb.toString()
      }
    }
    else{
      var applicableBuildings = period?.BOPLine?.BOPLocations*.Buildings?.where(\building -> {
        return building?.BOPBuildingCovExists && building?.Building?.NumStories > 7
      })
      if(applicableBuildings?.HasElements){
        var sb = new StringBuilder()
        for(building in applicableBuildings){
          if(sb.isEmpty()){
            sb.append(building?.BOPLocation?.Location?.LocationNum + " ")
          }else{
            sb.append(", ")
            sb.append(building?.BOPLocation?.Location?.LocationNum + " ")
          }
          sb.toString()
        }
        return sb.toString()
      }
    }
    return null
  }

  function bldgGrtrThan7Flr(period : PolicyPeriod) : boolean {
    if ((period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) or (period.Job.Subtype == typekey.Job.TC_RENEWAL)) {
      return
          period.BOPLine?.BOPLocations*.Buildings?.hasMatch(\building -> {
            return
                //Existing buildings with changed number of floors to 7+
                building?.BOPBuildingCovExists && ((building?.BasedOn?.Building?.NumStories <= 7 && building?.Building?.NumStories > 7)
                    //New building with 7+ floors
                    || (building?.BasedOn == null && building?.Building?.NumStories > 7) ||
                    (building?.BasedOn?.Building?.NumStories >7 && !building?.BasedOn?.BOPBuildingCovExists && building?.Building?.NumStories > 7))
          })
    }
    else{
      return period.BOPLine?.BOPLocations*.Buildings?.hasMatch(\building -> building?.BOPBuildingCovExists && building?.Building?.NumStories > 7)
    }
  }

  function bldgMoreThan30kSQFT(period : PolicyPeriod) : boolean {
    if ((period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) or (period.Job.Subtype == typekey.Job.TC_RENEWAL)) {
      return
          period.BOPLine?.BOPLocations*.Buildings?.hasMatch(\building -> {
            return
                //Existing buildings with changed number of floors to 7+
                building?.BOPBuildingCovExists && ((building?.BasedOn?.Building?.TotalArea <= 30000 && building?.Building?.TotalArea > 30000)
                    //New building with 7+ floors
                    || (building?.BasedOn == null && building?.Building?.TotalArea > 30000) ||
                    (building?.BasedOn?.Building?.TotalArea > 30000 && !building?.BasedOn?.BOPBuildingCovExists && building?.Building?.TotalArea > 30000))
          })
    }
    else{
      return period.BOPLine?.BOPLocations*.Buildings?.hasMatch(\building -> building?.BOPBuildingCovExists && building?.Building?.TotalArea > 30000)
    }
  }

  function findLocationNumHavingMoreSQFT(period : PolicyPeriod) : String {
    if((period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) or (period.Job.Subtype == typekey.Job.TC_RENEWAL)){

      var applicableBuildings = period.BOPLine?.BOPLocations*.Buildings?.where(\building -> {
        return
            building?.BOPBuildingCovExists && ((building?.BasedOn?.Building?.TotalArea <= 30000 && building?.Building?.TotalArea > 30000)
                || (building?.BasedOn == null && building?.Building?.TotalArea > 30000) ||
                (building?.BasedOn?.Building?.TotalArea > 30000 && !building?.BasedOn?.BOPBuildingCovExists && building?.Building?.TotalArea > 30000))
      })
      if(applicableBuildings?.HasElements){
        var sb = new StringBuilder()
        for(building in applicableBuildings){
          if(sb.isEmpty()){
            sb.append(building?.BOPLocation?.Location?.LocationNum +" ")
          }else{
            sb.append(", ")
            sb.append(building?.BOPLocation?.Location?.LocationNum + " ")
          }
          sb.toString()
        }
        return sb.toString()
      }
    }
    else{
      var applicableBuildings = period?.BOPLine?.BOPLocations*.Buildings?.where(\building -> {
        return building?.BOPBuildingCovExists && building?.Building?.TotalArea > 30000
      })
      if(applicableBuildings?.HasElements){
        var sb = new StringBuilder()
        for(building in applicableBuildings){
          if(sb.isEmpty()){
            sb.append(building?.BOPLocation?.Location?.LocationNum + " ")
          }else{
            sb.append(", ")
            sb.append(building?.BOPLocation?.Location?.LocationNum + " ")
          }
          sb.toString()
        }
        return sb.toString()
      }
    }
    return null
  }

  /*
   * create by: Jeff Lin
   * @description: Check the current UI's Multi Line Discount differ from th one from Integration
   * @create time: 4:57 PM 10/25/2019
   */
  function isIntegrationMultinLineDiscountMatched_TDIC(period : PolicyPeriod) : boolean {
    var multiLineDiscount = new TDIC_PolicyPeriodIsMultiline(period);

    /*var applied = multiLineDiscount.isSatisfied();
    return (applied and period.MultiLineDiscount_TDIC == MultiLineDiscount_TDIC.TC_N) or
        (not applied and period.MultiLineDiscount_TDIC != MultiLineDiscount_TDIC.TC_N)*/
    if(period.GLLineExists
        and period.Offering.CodeIdentifier != "PLCyberLiab_TDIC"
        and period.MultiLineDiscount_TDIC != multiLineDiscount.isSatisfiedForPL()) {
      return false
    } else if(period.BOPLineExists and period.MultiLineDiscount_TDIC != multiLineDiscount.isSatisfiedForCP()) {
      return false
    }
    return true
  }

  /*
   * create by: Jeff Lin
   * @description: Check if the part time hours is less than 20 hours or not
   * @create time: 9:53 PM 12/09/2019
   */
  function isPartTimeHoursLessThan20_TDIC(period : PolicyPeriod) : boolean {
    var qSet = period.GLLine.QuestionSets.firstWhere(\elt -> elt.CodeIdentifier == "GLUnderwriting")
    var question = qSet.getQuestionByCodeIdentifier("AverageHoursPerWeekToPractiveDentistryOverNextYear_TDIC")
    return period.GLLine.getAnswer(question)?.IntegerAnswer <= 20
  }

  /*
   * create by: Jeff Lin
   * @description: Check if In what capacity do you provide services? Other or mire than one options
   * @create time: 9:53 PM 12/09/2019
   */
  function isBusinessCapacityIsOther_TDIC(period : PolicyPeriod) : boolean {
    var qSet = period.GLLine.QuestionSets.firstWhere(\elt -> elt.CodeIdentifier == "GLUnderwriting")
    var question = qSet?.getQuestionByCodeIdentifier("WhatCaptaincyYouProvideServicesPLCMOC_TDIC")
        if(period.GLLine.getAnswer(question)?.AnswerValue != null) {
          return period.GLLine.getAnswer(question)?.AnswerValue.toString() == "Other"   // more than 1
        }
    return false
  }

  /*
   * create by: Jeff Lin
   * @description: Check if it is Dental Association by ADA or Local Dental Society is blank
   * @create time: 4:57 PM 10/25/2019
   */
  function isDentalAssociation_TDIC(period : PolicyPeriod) : boolean {
    var qSet = period.GLLine.QuestionSets.firstWhere(\elt -> elt.CodeIdentifier == "GLUnderwriting")
    var q1 = qSet?.Questions?.firstWhere(\elt -> elt.CodeIdentifier == "ADANumber_TDIC")
    return (period.GLLine.getAnswerForGLUWQuestion_TDIC("AreYouMemberOrApplicantOfYourStateDentalAssociation_TDIC"))
        and not period.GLLine.getAnswer(q1)?.TextAnswer.HasContent
  }

  /*
   * create by: Jeff Lin
   * @description: Check if it is Multi Owner Dental Practice Entity
   * @create time: 4:57 PM 10/25/2019
   */
  function isMultiOwnerDentalPracticeEntity_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("PracticeAsPartnerInDentalPartnership_TDIC") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("PracticeAsOfficerDirectorShareholder_TDIC")
  }

  /*
   * create by: Jeff Lin
   * @description: Check if there is Known Incidents exists
   * @create time: 4:57 PM 10/25/2019
   */
  function hasKnownIncidents_TDIC(period : PolicyPeriod) : boolean {
    var glLine = period.GLLine
    var hasClaimOrAllegatio = glLine.getAnswerForGLUWQuestion_TDIC("ClaimOfMalpractice_TDIC")
    var hasBelieveGiveRiseToClaim = glLine.getAnswerForGLUWQuestion_TDIC("BelieveCouldGiveRiseToClaimPLCMOC_TDIC")
    return (hasClaimOrAllegatio or hasBelieveGiveRiseToClaim)
  }

  /*
   * create by: Jeff Lin
   * @description: Check if it's the full time member of a dental school faculty
   * @create time: 10:14 PM 12/09/2019
   */
  function isFulltimeMemberDentalSchoolFaculty_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("AreYouFullTimeMemberOfDentalSchool_TDIC")
  }

  /*
   * create by: Jeff Lin
   * @description: Check if it completed a special program? Yes
   * @create time: 10:44 PM 12/09/2019
   */
  function hasCompletedASpecialtyProgram_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("HaveYouCompletedASpecialityProgram_TDIC")
  }

  /*
   * create by: Jeff Lin
   * @description: Check if Do you have any personal health problems that could reasonably be expected to affect the care you provide patients or your ability to manage your practice? Yes
   * @create time: 11:05 PM 12/09/2019
   */
  function hasHealthProblems_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("DoYouHaveAnyHealthProblems_TDIC")
  }

  /*
   * create by: Jeff Lin
   * @description: Check if 'Have you ever been convicted of a crime other than minor traffic violations or are you currently charged with a crime?' Yes
   * @create time: 11:15 PM 12/09/2019
   */
  function hasCrimeActivities_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("ConvictedOfCrimeOtherThanMinorTrafficViolationsPLCMOC_TDC")
  }

  /*
   * create by: Jeff Lin
   * @description: Check if it holds the dental licence in other states? Yes
   * @create time: 10:55 PM 12/09/2019
    * @param PolicyPeriod
   * @return: boolean
   */
  function hasHoldDentalLicenceOtherStates_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("HoldDentalLicenseInOtherStates_TDIC")
  }

  /*
   * create by: Jeff Lin
   * @description: Check if 'Do you perform sleep apnea/snoring therapy?' Yes and 'Do you treat only after a physician's referral?' No
   * @create time: 10:14 PM 12/09/2019
   */
  function isPerformSleepApnea_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("PerformSleepApneaSnoringTherapy_TDIC")and
        not (period.GLLine.getAnswerForGLUWQuestion_TDIC("IfYesTreatOnlyAfterPhysiciansReferralPLCMOC_TDIC"))
  }

  /*
   * create by: Jeff Lin
   * @description: Check if 'Are you a full-time student enrolled in an accredited dental postgraduate program?' Yes
   * @create time: 10:14 PM 12/09/2019
   */
  function isFulltimeStudentInDentalPostgraduateProgram_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("AreYouFullTimeStudentEnrolledInDentalPostGrad_TDIC")
  }


  /*
   * create by: Jeff Lin
   * @description: Check the Risk Management Discount has been modified.
   * @create time: 4:57 PM 10/25/2019
   */
  function isRiskManagementDiscountModified_TDIC(period : PolicyPeriod) : boolean {
    if (not period.GLLine.GLRiskMgmtDiscount_TDICExists) {
      if (period.GLLine.BasedOn?.GLRiskMgmtDiscount_TDICExists) {
        return true
      }
    } else {
      if (period.GLLine.BasedOn?.GLRiskMgmtDiscount_TDICExists) {
        if (period.GLLine.GLRiskMgmtDiscount_TDIC?.EffectiveDate != period.GLLine.BasedOn?.GLRiskMgmtDiscount_TDIC?.EffectiveDate or
            period.GLLine.GLRiskMgmtDiscount_TDIC?.ExpirationDate != period.GLLine.BasedOn?.GLRiskMgmtDiscount_TDIC?.ExpirationDate) {
          return true
        }
      } else {
        return true
      }
    }
    return false
  }

  /*
   * create by: Jeff Lin
   * @description: Check if Have you ever practiced without professional liability insurance? ' or 'Are you now or have you ever practiced without professional liability insurance?
   * @create time: 9:38 PM 12/09/2019
   */
  function hasPracticedWithoutPLliability_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("YouEverPracticedWithoutPLInsurancePLCMOC_TDIC")
  }

  /*
   * create by: Sanjana S
   * @description: Check if effective date and month of below discounts match effective date and month of Policy Term.
   * @create time: 12:55 PM 12/16/2019
   */
  function validateDateAndMonth_TDIC(period : PolicyPeriod) : boolean {
    var policytermDate = period.PeriodStart.DayOfMonth
    var policyTermMonth = period.PeriodStart.MonthName
    if (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (period.GLLine.GLFullTimeFacultyDiscount_TDICExists and
          period.GLLine.GLFullTimeFacultyDiscount_TDIC.GLFTFEffDate_TDICTerm != null) {
        var effDate = period.GLLine.GLFullTimeFacultyDiscount_TDIC.GLFTFEffDate_TDICTerm.Value.DayOfMonth
        var effMonth = period.GLLine.GLFullTimeFacultyDiscount_TDIC.GLFTFEffDate_TDICTerm.Value.MonthName
        if (effDate != policytermDate or effMonth != policyTermMonth) {
          return true
        }
      }
      if (period.GLLine.GLPartTimeDiscount_TDICExists and
          period.GLLine.GLPartTimeDiscount_TDIC.GLPTEffDate_TDICTerm != null) {
        var effDate = period.GLLine.GLPartTimeDiscount_TDIC.GLPTEffDate_TDICTerm.Value.DayOfMonth
        var effMonth = period.GLLine.GLPartTimeDiscount_TDIC.GLPTEffDate_TDICTerm.Value.MonthName
        if (effDate != policytermDate or effMonth != policyTermMonth) {
          return true
        }
      }
    }
    return false
  }

  /*
   * create by: Sanjana S
   * @description: Check if renewal AS400 policy has New Dentist Program in the previous term and brought
   * to Policy Center with New Graduate discount applied.s
   * @create time: 12:55 PM 12/16/2019
   *
   * Modified By: Jeff Lin, 4/14/2020, 12:02 pm,
   * check if it migrated Renewal on period.BasedOn.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION
   * PL BR-0497:	Detained New Graduate AS400 policy type 7	Detained New Graduate AS400 policy type 7
   * "When renewal AS400 policy has New Dentist Program in the previous term and brought to Policy Center with New Graduate discount applied
   */
  function validateDetainedNewGraduate_TDIC(period : PolicyPeriod) : boolean {
    if (period.BasedOn.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION and period.GLLineExists and
        period.BasedOn.GLLine.GLNewDentistDiscount_TDICExists and period.GLLine.GLNewGradDiscount_TDICExists) {
      return true
    }
    return false
  }

  /*
   * create by: Sanjana S
   * @description: Check If Primary Name Insured state and policy base state is different
   * @create time: 12:55 PM 12/16/2019
   */
  function reviewPNIMailingAddress_TDIC(period : PolicyPeriod) : boolean {
    var PNIState = period.PolicyContactRoles.firstWhere(\elt -> elt typeis PolicyPriNamedInsured)
        .ContactDenorm.PrimaryAddress.State.Code
    if (PNIState != period.BaseState.Code) {
      return true
    }
    return false
  }

  /*
   * create by: Jeff Lin
   * @description: Check if there exists the 'Gap in Prior Coverage' UW Issue to block the Issuance
   * @create time: 5:27 PM 03/27/2020 4:30 PM
   */
  function hasGapInPriorPolicies_TDIC(period : PolicyPeriod) : boolean {

    if (period.BasedOn != null and period.BasedOn?.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION) {
      return false
    }
    var priorPolicies = getPriorPolicies_TDIC(period)
    if(priorPolicies.HasElements) {
      //  Compare the expiration date of each Prior Policy term with the effective date of the next Prior Policy term (terms are sorted ascending)
      for (priorPolicy in priorPolicies.orderBy(\elt -> elt.PolicyStart) index i) {
        if (i >= 1) {
          var tmpDaysDifference = priorPolicies[i - 1].PolicyEnd.differenceInDays(priorPolicy.PolicyStart)// this function ignores the time component
          if (tmpDaysDifference > 0) {
            return true   //Gap in coverage
          } else { /*dates are identical - no gap*/ }
        }
        // Compare the expiration date of the most recent Prior Policy term against the start of this Policy with TDIC (not to be confused with the term on the workorder)
        if (i == priorPolicies.Count - 1) {// test the last record only
          var tmpStartDateOfPolicy = period.Policy.findEarliestPeriodStart()
          var tmpDaysDifference = priorPolicies[i].PolicyEnd.differenceInDays(tmpStartDateOfPolicy)// this function ignores the time component
          if (tmpDaysDifference > 0) {
            return true   //Gap in coverage
          } else { /*dates are identical - no gap*/ }
        }
      }
    }
    return false
  }

  /*
   * create by: Jeff Lin
   * @description: Check if there exists the 'Overlap in prior coverage' UW Issue to block the Issuance
   * @create time: 03/30/2020 7:50 PM
   */
  function hasOverlapInPriorPolicies_TDIC(period : PolicyPeriod) : boolean {

    if (period.BasedOn != null and period.BasedOn?.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION) {
      return false
    }
    var priorPolicies = getPriorPolicies_TDIC(period)
    if(priorPolicies.HasElements) {
      //  Compare the expiration date of each Prior Policy term with the effective date of the next Prior Policy term (terms are sorted ascending)
      for (priorPolicy in priorPolicies.orderBy(\elt -> elt.PolicyStart) index i) {
        if (i >= 1) {
          var tmpDaysDifference = priorPolicies[i - 1].PolicyEnd.differenceInDays(priorPolicy.PolicyStart)// this function ignores the time component
          if (tmpDaysDifference < 0) {
            return true   // Coverage Overlap
          } else { /*dates are identical - no overlap*/ }
        }
        // Compare the expiration date of the most recent Prior Policy term against the start of this Policy with TDIC (not to be confused with the term on the workorder)
        if (i == priorPolicies.Count - 1) {// test the last record only
          var tmpStartDateOfPolicy = period.Policy.findEarliestPeriodStart()
          var tmpDaysDifference = priorPolicies[i].PolicyEnd.differenceInDays(tmpStartDateOfPolicy)// this function ignores the time component
          if (tmpDaysDifference < 0) {
            return true   // Coverage Overlap
          } else { /*dates are identical - no overlap*/ }
        }
      }
    }

    return false
  }
  /**
   * create by: ChitraK
   * @description: Check for Anesthetic Modalities to create UW Issue
   * @create time: 3:43 PM 4/6/2020
   * @return:
   */

  function isAnestheticModalitySelected_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("Localanesthesia")or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("N2OO2analgesia") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("Oralconscioussedation") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("Conscioussedation") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("Conscioussedationoffice") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("Generalanesthesiaoffice")
  }
  /**
   * create by: ChitraK
   * @description: Check for Problematic Procedures to create UW Issue
   * @create time: 3:43 PM 4/6/2020
   * @return:
   */

  function isProblematicProceduresSelected_TDIC(period : PolicyPeriod) : boolean {
    return period.GLLine.getAnswerForGLUWQuestion_TDIC("bloodCompatibilityTests")or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("ChelationTherapy") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("CosmeticSurgery") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("DermalFillers") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("HomeopathicTherapies") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("Liposuction") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("Nico") or
        period.GLLine.getAnswerForGLUWQuestion_TDIC("Teledentistry")
  }


  function isContactNameLengExceedsLmt_TDIC(period : PolicyPeriod) : boolean{
    var bldg = period.BOPLine?.BOPLocations*.Buildings
    var ismortgageLengthExceedsLimit : boolean = false
    var isAdditionalInsuredLengthExceedsLimit : boolean = false
    if(bldg.Count > 0){
      ismortgageLengthExceedsLimit =  bldg*.BOPBldgMortgagees?.hasMatch(\elt -> elt?.DisplayName?.length() > 64)
      isAdditionalInsuredLengthExceedsLimit = bldg*.AdditionalInsureds?.hasMatch(\elt1 -> elt1?.DisplayName?.length() > 64)
    }
    if(period.PrimaryNamedInsured.DisplayName.length > 64 or
    isAdditionalInsuredLengthExceedsLimit or ismortgageLengthExceedsLimit){
      return true
    }
    return false
  }


  /****************************************************Private functions*****************************************************/

  private function getPriorPolicies_TDIC(period : PolicyPeriod) : List<TransientPriorPolicy_TDIC> {
    var priorPolicies : List<TransientPriorPolicy_TDIC> = {}

    if (period != null and period.Policy.PriorPolicies.HasElements or period.Policy.Account.Policies.hasMatch( \ elt -> elt != period.Policy)) {
      if ( not (period.Policy.PriorPolicies*.EffectiveDate.contains(null)
          or period.Policy.PriorPolicies*.ExpirationDate.contains(null)
          or period.Policy.PriorPolicies*.Carrier.contains(null)
          or period.Policy.PriorPolicies*.PolicyNumber.contains(null))) {

        //PC Policies
        var tdicPolicies = period.Policy.Account.Policies.where( \ elt -> elt != period.Policy
            and elt.LatestBoundPeriod != null
            and elt.LatestBoundPeriod.getSlice(elt.LatestBoundPeriod.EditEffectiveDate)?.Offering?.CodeIdentifier == period.Offering.CodeIdentifier)
        for (eachPriorTDICPolicy in tdicPolicies){
          priorPolicies.add(new TransientPriorPolicy_TDIC(eachPriorTDICPolicy))
        }

        //Risk Analysis Policies
        for (eachPriorCarrierPolicy in period.Policy.PriorPolicies){
          priorPolicies.add(new TransientPriorPolicy_TDIC(eachPriorCarrierPolicy))
        }
      }
    }
    return priorPolicies
  }
}