package gw.api.databuilder.wc7

uses gw.api.databuilder.DataBuilder
uses java.math.BigDecimal
uses gw.api.databuilder.BuilderContext
uses java.lang.IllegalStateException
uses gw.api.locale.DisplayKey

class WC7RateFactorBuilder extends DataBuilder<WC7RateFactor, WC7RateFactorBuilder> {
  var _rateFactorType : RateFactorType

  construct() {
    super(WC7RateFactor)
    withJustification("Something")
    withRateFactorType(RateFactorType.TC_WORKPLACEMAINT)
  }

  override function createBean(context: BuilderContext): WC7RateFactor {
    var mod = context.ParentBean as WC7Modifier
    if(_rateFactorType == null) {
      throw new IllegalStateException(DisplayKey.get("Builder.WC7RateFactor.Error.NullRateFactor"))
    }
    var rateFactor = mod.getRateFactor(_rateFactorType) as WC7RateFactor
    if (rateFactor == null) {
      throw new IllegalStateException(DisplayKey.get("Builder.WC7RateFactor.Error.InvalidRateFactorType", _rateFactorType))
    }
    return rateFactor
  }

  function withAssessment(value: double): WC7RateFactorBuilder {
    set(WC7RateFactor#Assessment.PropertyInfo, BigDecimal.valueOf(value))
    return this
  }
  
  final function withJustification(justification : String) : WC7RateFactorBuilder {
    set(WC7RateFactor#Justification.PropertyInfo, justification)
    return this
  }
  
  final function withRateFactorType(rateFactorType: RateFactorType) : WC7RateFactorBuilder {
    _rateFactorType = rateFactorType
    return this
  }
  
}
