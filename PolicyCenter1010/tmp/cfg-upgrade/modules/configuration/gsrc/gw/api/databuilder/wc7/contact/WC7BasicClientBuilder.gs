package gw.api.databuilder.wc7.contact

@Export
class WC7BasicClientBuilder extends WC7PolicyContactRoleBuilder<WC7PolicyContactRole, WC7BasicClientBuilder> {

  construct() {
    super(WC7PolicyContactRole)
  }
}
