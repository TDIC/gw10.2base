package gw.api.databuilder.wc7.contact

uses gw.api.productmodel.ClausePattern
uses java.util.Date
uses gw.api.upgrade.PCCoercions

@Export
class WC7LaborContactDetailBuilder extends WC7ContactDetailBuilder<WC7LaborContactDetail, WC7LaborContactDetailBuilder> {

  function withWorkLocation(workLocation : String) : WC7LaborContactDetailBuilder {
    set(WC7LaborContactDetail#WorkLocation.getPropertyInfo(), workLocation)
    return this
  }
  
  function withDescriptionOfDuties(descriptionOfDuties : String) : WC7LaborContactDetailBuilder {
    set(WC7LaborContactDetail#DescriptionOfDuties.getPropertyInfo(), descriptionOfDuties)
    return this
  }

  function withContractOrProject(contractOrProject: String) : WC7LaborContactDetailBuilder {
    set(WC7LaborContactDetail#ContractProject.getPropertyInfo(), contractOrProject)
    return this
  }
  
  function withNumberOfEmployees(numberOfEmployees : int) : WC7LaborContactDetailBuilder {
    set(WC7LaborContactDetail#NumberOfEmployees.getPropertyInfo(), numberOfEmployees)
    return this
  }
  
  function withContractEffectiveDate(contractEffectiveDate : Date) : WC7LaborContactDetailBuilder {
    set(WC7LaborContactDetail#ContractEffectiveDate.getPropertyInfo(), contractEffectiveDate)
    return this
  }
  
  function withContractExpirationDate(contractExpirationDate : Date) : WC7LaborContactDetailBuilder {
    set(WC7LaborContactDetail#ContractExpirationDate.getPropertyInfo(), contractExpirationDate)
    return this
  }

  function withJurisdiction(aJurisdiction : Jurisdiction) : WC7LaborContactDetailBuilder {
    set(WC7LaborContactDetail#Jurisdiction.getPropertyInfo(), aJurisdiction)
    return this
  }

  override protected function newContactDetailFor(line : WC7Line, contactRole : WC7PolicyContactRole) : WC7LaborContactDetail {
    var clausePattern = super.ClausePattern ?: defaultClausePatternFor(contactRole)
    line.setCoverageConditionOrExclusionExists(clausePattern, true)
    var clause = line.getCoverageConditionOrExclusion(clausePattern)
    return (contactRole as WC7LaborContact).addNewLaborContactDetail(clause)
  }

  private function defaultClausePatternFor(contactRole : WC7PolicyContactRole) : ClausePattern {
    if (contactRole typeis WC7PolicyLaborClient)
      return PCCoercions.makeProductModel<ClausePattern>(_isIncluded ? "WC7EmployeeLeasingClientEndorsementCond" : "WC7EmployeeLeasingClientExclEndorsementExcl")
    else if (contactRole typeis WC7PolicyLaborContractor)
      return PCCoercions.makeProductModel<ClausePattern>(_isIncluded ? "WC7LaborContractorEndorsementACond" : "WC7LaborContractorExclEndorsementExcl")
    else
      throw invalidContactRoleType(contactRole)
  }
}
