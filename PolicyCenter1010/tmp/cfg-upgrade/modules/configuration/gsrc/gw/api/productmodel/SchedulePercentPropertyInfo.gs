package gw.api.productmodel

uses gw.lang.reflect.IType

uses java.lang.Integer

class SchedulePercentPropertyInfo extends AbstractSchedulePropertyInfo<Integer> {
  construct(scheduledItemType: IType, columnName: String, columnLabel: String, isRequired: boolean, isIdentityColumn : boolean, priority : int) {
    super(scheduledItemType, columnName, columnLabel, isRequired, isIdentityColumn, priority)
  }

  construct(columnName: String, columnLabel: String, isRequired: boolean, isIdentityColumn : boolean, priority : int) {
    super(columnName, columnLabel, isRequired, isIdentityColumn, priority)
  }

  override property get ValueType(): String {
    return "Percent"
  }
}