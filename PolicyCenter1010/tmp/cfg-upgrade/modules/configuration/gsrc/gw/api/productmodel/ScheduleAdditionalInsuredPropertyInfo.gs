package gw.api.productmodel

uses gw.lang.reflect.IType

class ScheduleAdditionalInsuredPropertyInfo extends gw.api.productmodel.AbstractSchedulePropertyInfo<PolicyAddlInsuredDetail> {

  construct(columnName : String, colLabel : String, isRequired : boolean, isIdentityColumn : boolean, priority : int) {
    super(columnName, colLabel, isRequired, isIdentityColumn, priority)
  }

  construct(scheduledItemType : IType, columnName : String, colLabel : String, isRequired : boolean, isIdentityColumn : boolean, priority : int) {
    super(scheduledItemType, columnName, colLabel, isRequired, isIdentityColumn, priority)
  }

  override property get ValueType() : String {
    return "AdditionalInsured"
  }
}