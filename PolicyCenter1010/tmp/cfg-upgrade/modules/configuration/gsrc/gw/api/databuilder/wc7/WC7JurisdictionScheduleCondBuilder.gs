package gw.api.databuilder.wc7

uses gw.api.builder.PolicyConditionBuilder

class WC7JurisdictionScheduleCondBuilder extends PolicyConditionBuilder {

  construct() {
    super(WC7JurisdictionScheduleCond)
  }
  
  function withScheduledItem(item: WC7JurisdictionScheduleCondItemBuilder) : WC7JurisdictionScheduleCondBuilder {
    addAdditiveArrayElement(WC7JurisdictionScheduleCond#WC7JurisdictionScheduleCondItems.getPropertyInfo(), item)
    return this
  }

}
