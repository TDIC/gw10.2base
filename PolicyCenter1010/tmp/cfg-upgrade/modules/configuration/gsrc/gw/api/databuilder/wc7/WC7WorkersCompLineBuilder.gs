package gw.api.databuilder.wc7

uses gw.api.builder.PolicyLineBuilder
uses gw.api.builder.CoverageBuilder
uses gw.api.databuilder.wc7.contact.WC7PolicyOwnerOfficerBuilder
uses gw.entity.IArrayPropertyInfo
uses gw.api.builder.BuilderArrayPopulator
uses gw.api.databuilder.wc7.contact.WC7PolicyLaborClientBuilder
uses gw.api.databuilder.wc7.contact.WC7PolicyLaborContractorBuilder
uses gw.api.builder.ExclusionBuilder
uses gw.api.builder.PolicyConditionBuilder
uses java.util.ArrayList
uses java.lang.Integer
uses gw.api.databuilder.populator.BeanPopulator
uses gw.api.util.JurisdictionMappingUtil
uses gw.api.databuilder.wc7.contact.WC7BasicClientBuilder
uses gw.api.databuilder.DataBuilder
uses gw.lang.reflect.features.PropertyReference

@Export
class WC7WorkersCompLineBuilder extends PolicyLineBuilder<entity.WC7WorkersCompLine, WC7WorkersCompLineBuilder> {

  construct() {
    super(entity.WC7WorkersCompLine)
    this.withJurisdiction(new WC7JurisdictionBuilder( new WC7SubmissionBuilderHelper().Jurisdiction))
    this.withWC7EmpLiabLimitWithStopGap("100,000/100,000", "500,000", StopGap.TC_ALLMONOPOLISTICSTATES)
    this.withWC7TerrorismRiskInsProgReauthEndt()
    addPopulator(Integer.MAX_VALUE, new BeanPopulator<WC7WorkersCompLine>() {
       override function execute(line : WC7WorkersCompLine) {
         var jurisdictions = line.PolicyLocations.map(\ p -> JurisdictionMappingUtil.getJurisdiction(p))
         line.WC7Jurisdictions.each(\ j -> { 
           // Unused WC7Jurisdictions are removed. 
           if (not jurisdictions.contains(j.Jurisdiction)) {
             line.removeFromWC7Jurisdictions(j)  
           }
         })
       }
    })
  }

  construct(_withBenefitsDeductible : boolean) {
    super(entity.WC7WorkersCompLine)
    this.withJurisdiction(new WC7JurisdictionBuilder( new WC7SubmissionBuilderHelper().Jurisdiction, _withBenefitsDeductible))
    this.withWC7EmpLiabLimitWithStopGap("100,000/100,000", "500,000", StopGap.TC_ALLMONOPOLISTICSTATES)
    this.withWC7TerrorismRiskInsProgReauthEndt()
    addPopulator(Integer.MAX_VALUE, new BeanPopulator<WC7WorkersCompLine>() {
      override function execute(line : WC7WorkersCompLine) {
        var jurisdictions = line.PolicyLocations.map(\ p -> JurisdictionMappingUtil.getJurisdiction(p))
        line.WC7Jurisdictions.each(\ j -> {
          // Unused WC7Jurisdictions are removed.
          if (not jurisdictions.contains(j.Jurisdiction)) {
            line.removeFromWC7Jurisdictions(j)
          }
        })
      }
    })
  }
  function withCoverage(coverageBuilder : CoverageBuilder) : WC7WorkersCompLineBuilder {
    return withArrayElement(WC7WorkersCompLine#WC7LineCoverages, coverageBuilder)
  }

  function withExclusion(exclusionBuilder : ExclusionBuilder) : WC7WorkersCompLineBuilder {
    return withArrayElement(WC7WorkersCompLine#WC7LineExclusions, exclusionBuilder)
  }

  function withCondition(conditionBuilder : PolicyConditionBuilder) : WC7WorkersCompLineBuilder {
    return withArrayElement(WC7WorkersCompLine#WC7LineConditions, conditionBuilder)
  }

  final function withWC7CoveredEmployee(eu : WC7CoveredEmployeeBuilder) : WC7WorkersCompLineBuilder {
    return withAdditiveArrayElement(WC7WorkersCompLine#WC7CoveredEmployees, eu)
  }

  final function withWC7FedCoveredEmployee(eu : WC7FedCoveredEmployeeBuilder) : WC7WorkersCompLineBuilder {
    return withAdditiveArrayElement(WC7WorkersCompLine#WC7FedCoveredEmployees, eu)
  }

  final function withWC7MaritimeCoveredEmployee(eu : WC7MaritimeCoveredEmployeeBuilder) : WC7WorkersCompLineBuilder {
    return withAdditiveArrayElement(WC7WorkersCompLine#WC7MaritimeCoveredEmployees, eu)
  }
  
  final function withJurisdiction(jurisdiction : WC7JurisdictionBuilder) : WC7WorkersCompLineBuilder {
    return withAdditiveArrayElement(WC7WorkersCompLine#WC7Jurisdictions, jurisdiction)
  }

  function withRRP(rrpBuilder : WC7RetrospectiveRatingPlanBuilder) : WC7WorkersCompLineBuilder {
    set( WC7WorkersCompLine#RetrospectiveRatingPlan.getPropertyInfo(), rrpBuilder )
    return this
  }    
  
  function withParticipatingPlan(planBuilder : WC7ParticipatingPlanBuilder) : WC7WorkersCompLineBuilder {
    set( WC7WorkersCompLine#ParticipatingPlan.getPropertyInfo(), planBuilder )
    return this
  }

  function withEmployeeLeasingPlan(plan : WC7EmployeeLeasingPlanBuilder) : WC7WorkersCompLineBuilder {
    set(WC7WorkersCompLine#EmployeeLeasingPlan.getPropertyInfo(), plan)
    return this
  }

  function withWC7PolicyOwnerOfficer(ownerOfficer : WC7PolicyOwnerOfficerBuilder) : WC7WorkersCompLineBuilder {
    return withArrayPopulator(WC7WorkersCompLine#WC7PolicyOwnerOfficers, ownerOfficer)
  }

  function withWC7BasicClient(client : WC7BasicClientBuilder) : WC7WorkersCompLineBuilder {
    return withArrayPopulator(WC7WorkersCompLine#WC7BasicClients, client)
  }

  function withWC7PolicyLaborClient(policyLaborClient : WC7PolicyLaborClientBuilder) : WC7WorkersCompLineBuilder {
    return withArrayPopulator(WC7WorkersCompLine#WC7PolicyLaborClients, policyLaborClient)
  }

  function withWC7CoordinatedPolicy(coordinatedPolicyBuilder : WC7CoordinatedPolicyBuilder) : WC7WorkersCompLineBuilder {
    addArrayElement(WC7WorkersCompLine#MultipleCoordinatedPolicies.getPropertyInfo(), coordinatedPolicyBuilder, 400)
    return this
  }


  function withWC7PolicyLaborContractor(policyLaborContractor : WC7PolicyLaborContractorBuilder) : WC7WorkersCompLineBuilder {
    return withArrayPopulator(WC7WorkersCompLine#WC7PolicyLaborContractors, policyLaborContractor)
  }
  
  function withExcludedWorkplace(excludedWorkplace : WC7ExcludedWorkplaceBuilder) : WC7WorkersCompLineBuilder {
    return withArrayElement(WC7WorkersCompLine#WC7ExcludedWorkplaces, excludedWorkplace)
  }
  
  function withWaiverOfSubro(waiver : WC7WaiverOfSubroBuilder) : WC7WorkersCompLineBuilder {
    return withArrayElement(WC7WorkersCompLine#WC7WaiverOfSubros, waiver)
  }
  
  function withAircraftSeat(seat : WC7AircraftSeatBuilder) : WC7WorkersCompLineBuilder {
    return withArrayElement(WC7WorkersCompLine#WC7AircraftSeats, seat)
  }
  
  function withAircraftSeats(seats : ArrayList<WC7AircraftSeatBuilder>) : WC7WorkersCompLineBuilder {
    if (seats != null) {
      for (seat in seats) {
        addArrayElement(WC7WorkersCompLine#WC7AircraftSeats, seat)
      }
    }
    
    return this
  }

  function withWC7ManuscriptOption(option : WC7ManuscriptOptionBuilder) : WC7WorkersCompLineBuilder {
    return withArrayElement(WC7WorkersCompLine#WC7ManuscriptOptions, option)
  }

  public static function createAllJurisdictions(policyPeriod: PolicyPeriod) {
    var line = policyPeriod.WC7Line
    policyPeriod.LocationStates.each( \state -> line.addJurisdiction(state ))
  }

  final function withWC7SupplDiseaseExposure(supplDiseaseExposureBuilder : WC7SupplDiseaseExposureBuilder) : WC7WorkersCompLineBuilder {
    return withAdditiveArrayElement(WC7WorkersCompLine#WC7SupplDiseaseExposures, supplDiseaseExposureBuilder)
  }

  final function withWC7AtomicEnergyExposure(atomicEnergyExposureBuilder : WC7AtomicEnergyExposureBuilder) : WC7WorkersCompLineBuilder {
    return withAdditiveArrayElement(WC7WorkersCompLine#WC7AtomicEnergyExposures, atomicEnergyExposureBuilder)
  }
  
  final function withWC7EmpLiabLimitWithStopGap(packageDescription : String, optionValue : String, stopGapValue : StopGap) : WC7WorkersCompLineBuilder {
    withCoverage(new CoverageBuilder(WC7WorkersCompCov)
                       .withPatternCode("WC7WorkersCompEmpLiabInsurancePolicyACov")
                       .withPackageCovTerm("WC7EmpLiabLimit", packageDescription)
                       .withOptionCovTerm("WC7EmpLiabPolicyLimit", optionValue)
                       .withTypekeyCovTerm("WC7StopGap", stopGapValue))
    return this
  }
  
  final function withWC7TerrorismRiskInsProgReauthEndt() : WC7WorkersCompLineBuilder {
    withCoverage(new CoverageBuilder(WC7WorkersCompCov)
                       .withPatternCode("WC7TerrsmRiskInsProgReauthActDisclsrCov1"))
    return this
  }

  private function withArrayElement<T extends Object>(
      propertyReference: PropertyReference <WC7WorkersCompLine, T>,
      builder: DataBuilder) : WC7WorkersCompLineBuilder {
    addArrayElement(propertyReference, builder)
    return this
  }

  private function withAdditiveArrayElement<T extends Object>(
      propertyReference: PropertyReference <WC7WorkersCompLine, T>,
      builder: DataBuilder) : WC7WorkersCompLineBuilder {
    addAdditiveArrayElement(propertyReference, builder)
    return this
  }

  private function withArrayPopulator<T extends Object>(
      propertyReference: PropertyReference <WC7WorkersCompLine, T>,
      builder: DataBuilder) : WC7WorkersCompLineBuilder {
    addPopulator(new BuilderArrayPopulator(propertyReference.PropertyInfo as IArrayPropertyInfo, builder))
    return this
  }
}