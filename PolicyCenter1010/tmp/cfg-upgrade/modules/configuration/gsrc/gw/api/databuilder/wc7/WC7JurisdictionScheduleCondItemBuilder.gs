package gw.api.databuilder.wc7

uses gw.api.databuilder.DataBuilder
uses gw.api.databuilder.BuilderContext
uses java.util.Date

class WC7JurisdictionScheduleCondItemBuilder extends DataBuilder<WC7JurisdictSchedCondItem, WC7JurisdictionScheduleCondItemBuilder > {

  construct() {
    super(WC7JurisdictSchedCondItem)
  }
  
  protected override function createBean(context : BuilderContext) : WC7JurisdictSchedCondItem{
    var cov = context.ParentBean as WC7JurisdictionScheduleCond
    var scheduledItem = new WC7JurisdictSchedCondItem(cov.Branch)
    cov.addToWC7JurisdictionScheduleCondItems(scheduledItem)
    return scheduledItem
  }

  function withString1(string1 : String) : WC7JurisdictionScheduleCondItemBuilder {
    set(WC7JurisdictSchedCondItem#StringCol1, string1)
    return this
  }

  function withString2(string2 : String) : WC7JurisdictionScheduleCondItemBuilder {
    set(WC7JurisdictSchedCondItem#StringCol2, string2)
    return this
  }

  function withTypeKey1(typeKey : String) : WC7JurisdictionScheduleCondItemBuilder {
    set(WC7JurisdictSchedCondItem#TypeKeyCol1, typeKey)
    return this
  }
  
  function withTypeKey2(typeKey : String) : WC7JurisdictionScheduleCondItemBuilder {
    set(WC7JurisdictSchedCondItem#TypeKeyCol2, typeKey)
    return this
  }

  function withDate1(date1 : Date) : WC7JurisdictionScheduleCondItemBuilder {
    set(WC7JurisdictSchedCondItem#DateCol1, date1)
    return this
  }
  
  function withInt1(int1 : int) : WC7JurisdictionScheduleCondItemBuilder {
    set(WC7JurisdictSchedCondItem#IntCol1, int1)
    return this
  }
  
  function withInt2(int2 : int) : WC7JurisdictionScheduleCondItemBuilder {
    set(WC7JurisdictSchedCondItem#IntCol2, int2)
    return this
  }

  function withClassCode(classCode : WC7ClassCode) : WC7JurisdictionScheduleCondItemBuilder {
    set(WC7JurisdictSchedCondItem#ClassCode, classCode)
    return this
  }

}
