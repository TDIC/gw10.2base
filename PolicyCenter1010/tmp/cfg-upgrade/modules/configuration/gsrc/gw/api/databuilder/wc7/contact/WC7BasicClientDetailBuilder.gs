package gw.api.databuilder.wc7.contact

class WC7BasicClientDetailBuilder extends WC7ContactDetailBuilder<WC7ContactDetail, WC7BasicClientDetailBuilder> {

  override protected function newContactDetailFor(line : WC7Line, contactRole : WC7PolicyContactRole) : WC7ContactDetail {
    return contactRole.addNewBasicContactDetail(line.getCoverageConditionOrExclusion(ClausePattern))
  }
}