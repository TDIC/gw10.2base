package gw.contact

enhancement GWPCContactOfficialIDEnhancement: entity.Contact {

  property get NCCIintrastateOfficialID(): String {
    return this.getOfficialID(TC_NCCIINTRASTATE)
  }

  property set NCCIintrastateOfficialID(id: String) {
    this.setOfficialID(TC_NCCIINTRASTATE, id)
  }

  property get ADANumber_TDICOfficialID(): String {
    return this.getOfficialID(TC_ADANUMBER_TDIC)
  }

  property set ADANumber_TDICOfficialID(id: String) {
    this.setOfficialID(TC_ADANUMBER_TDIC, id)
  }
}
