  package gw.contactmapper.ab800

  uses gw.internal.xml.xsd.typeprovider.XmlSchemaTypeToGosuTypeMappings
  uses gw.lang.reflect.IType
  uses gw.webservice.contactapi.abcontactapihelpers.core.BeanPopulator
  uses gw.webservice.contactapi.abcontactapihelpers.core.XmlPopulator
  uses gw.webservice.contactapi.beanmodel.anonymous.elements.XmlBackedInstance_Field
  uses gw.webservice.contactapi.beanmodel.XmlBackedInstance
  uses gw.webservice.contactapi.mapping.FieldMappingImpl

  /**
   * Created with IntelliJ IDEA.
   * User: SunnihithB
   * Date: 12/2/15
   * Time: 5:30 PM
   * Class for handling mapping of Contacts.
   */
  internal class OfficialIDValueMapper extends FieldMappingImpl<OfficialID> {

  protected static final var LINK_ID            : String = "LinkID"
  protected static final var EXTERNAL_PUBLIC_ID : String = "External_PublicID"


  construct() {
  super(OfficialID#OfficialIDValue, BOTH, OfficialID#OfficialIDValue.PropertyInfo.Name)
  }

  override function populateXml(xp : XmlPopulator<OfficialID>) {

  var theOffID = xp.Bean
  var abOffIDXML = xp.XmlBackedInstance

  if (theOffID typeis OfficialID) {
  //var oId = theOffID
  var colType = OfficialID.Type.TypeInfo.getProperty("OfficialIDValue").FeatureType
  var oIdOriginalValue = theOffID.getOriginalValue("OfficialIDValue")
    populateFieldXMLWithValue(abOffIDXML, "OfficialIDValue", theOffID.OfficialIDValue, oIdOriginalValue, colType, theOffID.New)
  }
  }

  override function populateBean(bp : BeanPopulator<OfficialID>) {
  var theOID = bp.Bean
  var abOIDXML = bp.XmlBackedInstance
  theOID.OfficialIDType = typekey.OfficialIDType.get(abOIDXML.fieldValue("OfficialIDValue"))
  }


  private function populateFieldXMLWithValue(instanceXML     : XmlBackedInstance,
  ab_fieldName    : String,
  value           : Object,
  originalValue   : Object,
  columnType      : IType,
  beanIsNew       : boolean) {
  var fieldXML = new XmlBackedInstance_Field()
  instanceXML.Field.add(fieldXML)
  fieldXML.Name = ab_fieldName
  var pair = XmlSchemaTypeToGosuTypeMappings.gosuToSchema(columnType)
  fieldXML.Type = pair.First
  fieldXML.setAttributeSimpleValue(XmlBackedInstance_Field.$ATTRIBUTE_QNAME_Value,
  pair.Second.gosuValueToStorageValue(value))
  if (beanIsNew or ab_fieldName == LINK_ID or ab_fieldName == EXTERNAL_PUBLIC_ID) return
  fieldXML.setAttributeSimpleValue(XmlBackedInstance_Field.$ATTRIBUTE_QNAME_OrigValue,
  pair.Second.gosuValueToStorageValue(originalValue))
  }


}
