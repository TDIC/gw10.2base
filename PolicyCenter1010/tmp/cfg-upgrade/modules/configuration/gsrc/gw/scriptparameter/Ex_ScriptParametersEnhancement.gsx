package gw.scriptparameter

uses java.math.BigDecimal

/**
 * File created by Guidewire Upgrade Tools.
 */
@Export
enhancement Ex_ScriptParametersEnhancement : ScriptParameters {

  public static property get TDIC_WC7DaysAfterPolicyExpiredToStartFinalAuditBatch() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterPolicyExpiredToStartFinalAuditBatch") as Integer
  }

  public static property get TDIC_WC7DaysAfterAuditDueDateToStartFinalAuditBatch() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterAuditDueDateToStartFinalAuditBatch") as Integer
  }

  public static property get TDIC_WC7DaysAfterRenewalToSendFirstVoluntaryAuditDocuments() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterRenewalToSendFirstVoluntaryAuditDocuments") as Integer
  }

  public static property get TDIC_WC7AuditedBasisChangePercentageThreshold() : BigDecimal {
    return ScriptParameters.getParameterValue("TDIC_WC7AuditedBasisChangePercentageThreshold") as BigDecimal
  }

  public static property get ExternalIssuance() : Boolean {
    return ScriptParameters.getParameterValue("ExternalIssuance") as Boolean
  }

  public static property get EnableAddressStandardizationStub() : Boolean {
    return ScriptParameters.getParameterValue("EnableAddressStandardizationStub") as Boolean
  }

  public static property get TDIC_ReinsuranceAdminSearchResultLimit() : Integer {
    return ScriptParameters.getParameterValue("TDIC_ReinsuranceAdminSearchResultLimit") as Integer
  }

  public static property get TDIC_DAYSOFBATCHRECORDS() : Integer {
    return ScriptParameters.getParameterValue("TDIC_DAYSOFBATCHRECORDS") as Integer
  }

  public static property get TDIC_WC7DaysAfterExpirationToSendPhysicalAuditLetter() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterExpirationToSendPhysicalAuditLetter") as Integer
  }

  public static property get TDIC_WC7DaysAfterStartDateToSetDueDate() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterStartDateToSetDueDate") as Integer
  }

  public static property get TDIC_WC7DaysAfterRenewalToSendPolicyHolderNotice() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterRenewalToSendPolicyHolderNotice") as Integer
  }

  public static property get TDIC_WC7AdditionalToCalculateFinalAuditStartDateIfCancelled() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7AdditionalToCalculateFinalAuditStartDateIfCancelled") as Integer
  }

  public static property get PercentageAuditedBasisIncrease() : BigDecimal {
    return ScriptParameters.getParameterValue("PercentageAuditedBasisIncrease") as BigDecimal
  }

  public static property get TDIC_WC7AuditReportingInitializationThresholdValue() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7AuditReportingInitializationThresholdValue") as Integer
  }

  public static property get TDIC_UseEarnedPremiumReportPeriodStartDate() : Boolean {
    return ScriptParameters.getParameterValue("TDIC_UseEarnedPremiumReportPeriodStartDate") as Boolean
  }

  public static property get TDIC_WC7DaysAfterExpirationToSendAuditFirstRequestDocuments() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterExpirationToSendAuditFirstRequestDocuments") as Integer
  }

  public static property get TDIC_DataSource() : String {
    return ScriptParameters.getParameterValue("TDIC_DataSource") as String
  }

  public static property get TDIC_WC7AdditionalToCalculateFinalAuditStartDate() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7AdditionalToCalculateFinalAuditStartDate") as Integer
  }

  public static property get CommandCenterIssuance() : Boolean {
    return ScriptParameters.getParameterValue("CommandCenterIssuance") as Boolean
  }

  public static property get TDIC_WC7DaysAfterRenewalToSendPhysicalAuditDocuments() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterRenewalToSendPhysicalAuditDocuments") as Integer
  }

  public static property get TDIC_WC7DaysAfterRenewalToSendAuditLetterEstimationDocuments() : Integer {
    return ScriptParameters.getParameterValue("TDIC_WC7DaysAfterRenewalToSendAuditLetterEstimationDocuments") as Integer
  }

  public static property get TDIC_AutomatedRateBookLoader() : Boolean {
    return ScriptParameters.getParameterValue("TDIC_AutomatedRateBookLoader") as Boolean
  }
  
  public static property get TDIC_BasicTemplateRatingStartDate(): Date {
      return ScriptParameters.getParameterValue("TDIC_BasicTemplateRatingStartDate") as Date
  }
    
  public static property get TDIC_RateBooksToBeExported() : String {
    return ScriptParameters.getParameterValue("TDIC_RateBooksToBeExported") as String
  }

  public static property get TDIC_ExportRateBooksWithStatus() : String {
    return ScriptParameters.getParameterValue("TDIC_ExportRateBooksWithStatus") as String
  }

  public static property get TDIC_RateBookEnvs() : String {
    return ScriptParameters.getParameterValue("TDIC_RateBookEnvs") as String
  }

  public static property get TDIC_RatebookExportImportArchivePath() : String {
    return ScriptParameters.getParameterValue("TDIC_RatebookExportImportArchivePath") as String
  }

}