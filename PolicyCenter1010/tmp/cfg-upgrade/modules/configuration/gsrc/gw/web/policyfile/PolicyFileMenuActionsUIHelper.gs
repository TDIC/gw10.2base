package gw.web.policyfile

uses gw.api.job.EffectiveDateCalculator
uses gw.api.locale.DisplayKey
uses gw.assignment.AssignmentUtil
uses gw.plugin.Plugins
uses gw.plugin.jobnumbergen.IJobNumberGenPlugin
uses gw.plugin.policyperiod.IPolicyTermPlugin
uses pcf.JobForward
uses pcf.PolicyFileForward
uses pcf.ReinstatementWizard
uses pcf.IssuanceWizard
uses pcf.RenewalWizard
uses pcf.api.Location
uses typekey.UserRole

@Export
class PolicyFileMenuActionsUIHelper {

  // Create a new Submission and Policy from this Policy's initial submission
  public static function copySubmission(submission : entity.Submission) {
    var currentUser: User = User.util.CurrentUser
    currentUser.assertCanView(submission)
    var copy = submission.copyPolicyPeriod(submission.LatestPeriod)
    JobForward.go(copy)
  }

  public static function canCopySubmission(submission : entity.Submission) : boolean {
    return submission != null and perm.Submission.create and perm.System.jobcopy
  }


  public static function startReinstatement(policyPeriod : entity.PolicyPeriod, CurrentLocation : Location) {
    var job = new Reinstatement(policyPeriod.Bundle)
    if (job.startJobAndCommit(policyPeriod, CurrentLocation)) {
      ReinstatementWizard.go(job, job.LatestPeriod)
    }
  }

  public static function getCancellationLabel(cancellation : Cancellation) : String {
    var reasonCode = cancellation.CancelReasonCode
    var reasonStr = (reasonCode == null) ? DisplayKey.get("Java.RescindCancellationMenuActionWidget.NoReason") :
        reasonCode.DisplayName
    var effDateStr = gw.api.util.StringUtil.formatDate(cancellation.PolicyPeriod.EditEffectiveDate, "medium")
    return DisplayKey.get("Web.PolicyFile.CancellationLabel", reasonStr, effDateStr)
  }

  public static function getRescindableCancellations(period : PolicyPeriod) : Cancellation[] {
    var openCancellations = period.Policy.OpenJobs.whereTypeIs(Cancellation)
    var withBasedOn = openCancellations.where(\ j -> j.PolicyPeriod.BasedOn == period)
    return withBasedOn.where(\ job : Cancellation -> job.PolicyPeriod.Status == PolicyPeriodStatus.TC_CANCELING)
  }

  public static function startIssuance(policyPeriod : entity.PolicyPeriod, CurrentLocation : Location) {
    var job = new Issuance(policyPeriod.Bundle)
    if (job.startJobAndCommit(policyPeriod.Policy, CurrentLocation)) {
      IssuanceWizard.go(job, job.LatestPeriod)
    }
  }

  public static function startRenewal(policyPeriod : entity.PolicyPeriod, CurrentLocation : Location) {
    var renewal = new Renewal(policyPeriod.Bundle)
    if (renewal.startJobAndCommit(policyPeriod.Policy, CurrentLocation)) {
      RenewalWizard.go(renewal, renewal.LatestPeriod)
    }
  }

  /**
   * BrittoS 05/01//2020
   * Starting new renewal job for migrated policy if the original renewal was withdrawn
   */
  public static function startNewMigratedRenewal_TDIC(period : entity.PolicyPeriod) {
    var policyPeriod = period.BasedOn
    var policy =  policyPeriod.Policy
    var renewal : Renewal
    try {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        renewal = new Renewal(bundle)
        com.guidewire.pc.system.internal.InternalMethods.internal(policy).addToJobs(renewal)
        var renewalInternal =  com.guidewire.pc.system.internal.InternalMethods.asRenewalInternal(renewal)
        var effDate = policyPeriod.getPeriodEnd()

        var effDateCalc = EffectiveDateCalculator.instance()
        var effDateTime = effDateCalc.getRenewalEffectiveDate(effDate, policyPeriod, renewal)
        var defaultTermType = policy.Product.DefaultTermType
        var periodEnd = Plugins.get(IPolicyTermPlugin).calculatePeriodEndFromBasedOn(effDateTime, defaultTermType, policyPeriod, true)
        var expDateTime = effDateCalc.getRenewalExpirationDate(effDateTime, periodEnd, policyPeriod, renewal)

        policyPeriod = bundle.add(policyPeriod).getSlice(policyPeriod.EditEffectiveDate)
        var renewalPeriod = policyPeriod.createDraftBranchInNewPeriod(effDateTime, expDateTime)
        renewalPeriod.TermType = defaultTermType
        renewalInternal.addToPeriods(renewalPeriod)
        renewalPeriod.updateTermNumber()
        renewalPeriod.Renewal.CloseDate = null
        renewalPeriod.Renewal.RoleAssignments.each(\elt -> {
          elt.CloseDate = null
        })
        AssignmentUtil.assignAndLogUserRole(renewal, User.util.CurrentUser, User.util.CurrentUser.DefaultAssignmentGroup, UserRole.TC_CREATOR, "NewRenewal.assignCreator")
        renewalInternal.JobNumber = Plugins.get(IJobNumberGenPlugin).genNewJobNumber(renewal)
        renewalPeriod.edit()
      }, User.util.UnrestrictedUser)

      if(renewal != null and renewal.LatestPeriod != null) {
        RenewalWizard.go(renewal, renewal.LatestPeriod)
      }
    } catch(e) {
      throw e
    }
  }

  /**
   * BrittoS 05/01/2020
   * "Renew Policy" button visibility
   */
  public static function canCreateNewMigratedRenewal_TDIC(policyPeriod : entity.PolicyPeriod) : Boolean {
    if(policyPeriod.isLegacyConversion and policyPeriod.Status == PolicyPeriodStatus.TC_WITHDRAWN) {
      var period = policyPeriod.Policy.retrieveBoundOrLegacyRenewalPeriod(policyPeriod.PolicyNumber, policyPeriod.EditEffectiveDate)
      return (period == null or period.Status == PolicyPeriodStatus.TC_WITHDRAWN)
    }
    return false
  }

  public static function redirectRenewalWithdrawn_TDIC(policyPeriod : entity.PolicyPeriod) {
    if(policyPeriod.isLegacyConversion) {
      gw.api.web.policy.PolicyForwardUtil.getPolicyDestination().go()
    } else {
      PolicyFileForward.go(policyPeriod.BasedOn, policyPeriod.BasedOn.PeriodStart)
    }
  }
}