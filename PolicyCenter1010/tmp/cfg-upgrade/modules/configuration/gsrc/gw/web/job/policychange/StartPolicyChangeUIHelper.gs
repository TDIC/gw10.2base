package gw.web.job.policychange

uses gw.api.locale.DisplayKey

uses java.util.Date

uses gw.api.productmodel.ClausePatternLookup
uses gw.api.util.DisplayableException
uses pcf.api.Location
uses typekey.Job

@Export
class StartPolicyChangeUIHelper {

  public static function getConfirmMessage(inForcePeriod : entity.PolicyPeriod, effectiveDate : Date) : String {
    var result = ""
    if (inForcePeriod != null) {
      if(inForcePeriod.Policy.isOOSChange(effectiveDate)){
        result = result + DisplayKey.get("Web.Job.OOS.VerifyOOSChange")
      }
      if(inForcePeriod.hasFinalAuditFinished()){
        result = result + DisplayKey.get("Web.Job.FinalAuditCompleted")
      }
    }
    return result
  }

  /**
   * Apply the EffectiveTimePlugin to initalize the time part of the EffectiveDate.
   * The EffectiveDate will be validated by PolicyPlugin.canStartPolicyChange()
   */
  public static function applyEffectiveTimePluginForPolicyChange( policyPeriod : entity.PolicyPeriod, job : entity.PolicyChange, effDate : Date ) : Date {
    var effDateTime = gw.api.job.EffectiveDateCalculator.instance().getPolicyChangeEffectiveDate(effDate, policyPeriod, job)
    if (effDateTime < policyPeriod.PeriodStart) {
      effDateTime = policyPeriod.PeriodStart
    } else if (effDateTime >= policyPeriod.PeriodEnd) {
      effDateTime = policyPeriod.PeriodEnd.addMinutes(-1)
    }
    return effDateTime
  }


  /**
   * Returns a string with applicable warnings when starting a policy change. These warnings will not
   * prevent the policy change from starting. The string may be empty.
   */
  public static function getPolicyChangeWarningMessage(pInForcePeriod: PolicyPeriod, pEffectiveDate: Date) : String {
    var messages = new java.util.ArrayList<String>()
    if (pInForcePeriod != null) {
      if (!pInForcePeriod.Policy.Issued) {
        messages.add(DisplayKey.get("Web.Job.ChangeNonIssuedPolicy"))
      }
      if ( pInForcePeriod.Canceled ) {
        var cancellationDate = pInForcePeriod.CancellationDate.format("short")
        messages.add(DisplayKey.get("Web.Job.PolicyIsCanceledAsOf", cancellationDate))
      }
      if(pInForcePeriod.Policy.hasOpenPolicyChangeJob()) {
        messages.add(DisplayKey.get("Web.PolicyChange.MayResultInPreemption"))
      }
      if ( pInForcePeriod.Policy.RewrittenToNewAccountDestination != null){
        messages.add(DisplayKey.get("Web.Job.ChangeRewriteNewAccountPolicy", pInForcePeriod.Policy.RewrittenToNewAccountDestination.LatestBoundPeriod.PolicyNumber))
      }
    }

    return messages.Empty ? null : messages.join(", ")
  }

  /**
   * Gets the in-force period of {@code policyPeriod} that is effective as of {@code effectiveDate}.
   */
  public static function getInForcePeriodWithBasedOn(policyPeriod : PolicyPeriod, effectiveDate : Date) : PolicyPeriod {
    return (effectiveDate != null) ? Policy.finder.findPolicyPeriodByPolicyAndAsOfDate(policyPeriod.Policy, effectiveDate) : null
  }

  /**
   * Refreshes the in-force period so that the policy change does not rely on a cached version. Then, attempts to start
   * the policy change job and commit. Returns {@code true} if the job was successfully started.
   */
  public static function refreshAndStartJobAndCommit(job : PolicyChange, policyPeriod : PolicyPeriod, effectiveDate : Date, location : Location) : boolean {
    // see PC-26271 for why it is necessary to get the latest policy period from the database
    var period = getUpdatedPolicyPeriod(policyPeriod)
    var inForcePeriod = getInForcePeriodWithBasedOn(period, effectiveDate) ?: period
    //var inForcePeriodChange = getInForcePeriodWithBasedOn(period, effDateChange) ?: period
    var inForceEffectiveDate = applyEffectiveTimePluginForPolicyChange(inForcePeriod, job, effectiveDate)

    return job.startJobAndCommit(inForcePeriod.Policy, inForceEffectiveDate, location)
  }
 /**
  * create by: ChitraK
  * @description: validate policy change reason
  * @create time: 3:21 PM 12/12/2019
  * @return:
  */

  public static function verifyPolicyChangeReason_TDIC(job:PolicyChange,pp:PolicyPeriod){
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageEREPL"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageERECyber"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageEREPLOffer"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageERECyberOffer"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EREPLRESCINDED).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageEREPLResc"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERRESCINDED).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageERECybResc"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFERNA).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageEREPLNAOffer"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERNA).Count==1)
    throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageERECybNAOffer"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFERNR).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageEREPLNROffer"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERNR).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageERECybNROffer"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.hasMatch(\changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_REGENERATEDECLARATION))
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageRegeneratinDeclaration"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_PROFILECHANGE).Count==1)
        throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessage"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageEREPLDocGen"))
    if(job.ChangeReasons.Count > 1 && job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBDOCGEN).Count==1)
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessageERECybDocGen"))
  }
/*
 * create by: SureshB
 * @description: method to display an error message if user is trying to create a policy change on flat cancelled policy.
 * @create time: 3:12 PM 1/29/2020
  * @param PolicyPeriod, java.util.Date
 * @return:
 */

  public static function validatePolicyChangeOnFlatCancelledPolicy_TDIC (period : PolicyPeriod, effectiveDate : java.util.Date, job: PolicyChange) {
    var ereReasons = {"ereCyber", "ereCyberNR", "ereCyberRescinded", "erePLOffer", "erePLOfferNR", "ereCyberNA", "ereCyberOffer", "erePLOfferNA", "erePLRescinded", "extendedreportingperiodendorsementplcm"}
    if ((period.GLLineExists || period.BOPLineExists ) and (effectiveDate >= period?.CancellationDate)
        and !(job.ChangeReasons.hasMatch(\reason -> ereReasons.contains(reason.ChangeReason.Code)))) {
      throw new DisplayableException(DisplayKey.get("TDIC.Validation.Display.ErrorMessagePolicyChangeOnFlatCancel",effectiveDate))
    }
  }



  public static function setEREReasons_TDIC(policyPeriod : PolicyPeriod,  effectiveDate : Date) {
    var period = getUpdatedPolicyPeriod(policyPeriod)
    var cancelJob = period.Policy.Jobs.where(\elt -> elt.Subtype == Job.TC_CANCELLATION && elt.DisplayStatus == "Bound").first()
    var renewalJob = period.Policy.Jobs.where(\elt -> elt.Subtype == Job.TC_RENEWAL).first()
    // Added by Jeff, 11/13/2019, 10:14 PM, GPC-213/584
    if (cancelJob != null and
        (cancelJob.LatestPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or cancelJob.LatestPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC")) {
      tdic.web.pcf.helper.PolicyCancellationScreenHelper.setCancelReasonAndRatedForExtendedCovEndorsement(policyPeriod, cancelJob.LatestPeriod)
    }
    // Added by Jeff, 11/14/2019, 10:10 AM, GPC-213/584
    if (renewalJob!=null and renewalJob.LatestPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED and
        (renewalJob.LatestPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or renewalJob.LatestPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC")) {
      //tdic.web.pcf.helper.PolicyRenewalScreenHelper.setNonRenewedReasonAndRatedForExtendedCovEndorsement(period, renewalJob.LatestPeriod)
      //GWPS-599
      tdic.web.pcf.helper.PolicyRenewalScreenHelper.setNonRenewedReasonAndRatedForExtendedCovEndorsement(policyPeriod, renewalJob.LatestPeriod)
    }
  }



  /**
   * Get the most up-to-date version of {@code policyPeriod} from the database.
   */
  static function getUpdatedPolicyPeriod(policyPeriod : PolicyPeriod) : PolicyPeriod {
    var q = gw.api.database.Query.make(entity.PolicyPeriod)
    q.compare("Policy", Equals, policyPeriod.Policy.ID)
    var result = q.select()
    return result.FirstResult
  }
  /**
   * create by: SureshB
   * @description: method to fileter the List<ChangeReasonType_TDIC> based on the state
   * @create time: 4:35 PM 8/27/2019
   * @param PolicyPeriod and List<ChangeReasonType_TDIC>
   * @return: List<ChangeReasonType_TDIC>
   */
  static function  filterChangeReasons_TDIC(policyPeriod:PolicyPeriod, changeReasons:List<ChangeReasonType_TDIC>) : List<ChangeReasonType_TDIC> {
    var completedTransactions = policyPeriod.Policy.Jobs.where(\elt -> elt.DisplayStatus == "Bound")
    var filteredChangeReasons = new ArrayList<ChangeReasonType_TDIC>()
    for (changeReason in changeReasons) {
      if (!changeReason.Categories.whereTypeIs(Jurisdiction).IsEmpty) {
        if(changeReason.hasCategory(policyPeriod.BaseState) && !changeReason.Retired){
          filteredChangeReasons.add(changeReason)
        }
      }
      else{
        filteredChangeReasons.add(changeReason)
      }
   }

    //BR-022
    var cancelJob = completedTransactions.where(\elt -> elt.Subtype == Job.TC_CANCELLATION && elt.DisplayStatus == "Bound").first()
    var midTermCancel = policyPeriod.CancellationDate != null && policyPeriod.RefundCalcMethod != CalculationMethod.TC_FLAT
    var expPolicy = policyPeriod.Job.LatestPeriod.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.DisplayName
    var nonRenewedPolicy = policyPeriod.Policy.Jobs.where(\elt -> elt.Subtype == Job.TC_RENEWAL && elt.DisplayStatus == "Non-renewed").Count > 0

    var priorPolicy = policyPeriod.isCanceled() && policyPeriod.RefundCalcMethod == CalculationMethod.TC_FLAT && policyPeriod.BasedOn.Renewal!=null
    if(policyPeriod.GLLineExists && (!midTermCancel && !expPolicy && !priorPolicy && !nonRenewedPolicy)){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM)
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBER)
    }
    if(policyPeriod.CancellationDate == null && !nonRenewedPolicy && !expPolicy){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLOFFER)
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBEROFFER)
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLOFFERNA)
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBERNA)
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLRESCINDED)
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBERRESCINDED)
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLOFFERNR)
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBERNR)
    }
    //BR-025 Pl Policy Change
    if(policyPeriod.GLLineExists && policyPeriod.GLLine.GLDentalEmpPracLiabCov_TDICExists) {
      filteredChangeReasons.add(ChangeReasonType_TDIC.TC_CANCELEPLI)
    }

    //BR-036
    if(policyPeriod.GLLineExists && (policyPeriod.CancellationDate != null || policyPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED) &&
        completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER || elt1.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM).Count == 1).HasElements){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLOFFER)
    }

    //GWPS-2057
    if(policyPeriod.GLLineExists && (policyPeriod?.Offering?.CodeIdentifier == "PLClaimsMade_TDIC") && (policyPeriod.GLLine.GLExtendedReportPeriodCov_TDICExists) && (policyPeriod.Policy.EndorsementDetails_TDIC?.hasMatch(\elt -> elt.EREStatus == EndorsementStatus_TDIC.TC_PAID))){
      filteredChangeReasons.add(ChangeReasonType_TDIC.TC_EREPLDOCGEN)
    }
    if(policyPeriod.GLLineExists && (policyPeriod?.Offering?.CodeIdentifier == "PLCyberLiab_TDIC") && (policyPeriod.GLLine.GLCyvSupplementalERECov_TDICExists) && (policyPeriod.Policy.EndorsementDetails_TDIC?.hasMatch(\elt -> elt.EREStatus == EndorsementStatus_TDIC.TC_PAID))){
      filteredChangeReasons.add(ChangeReasonType_TDIC.TC_ERECYBDOCGEN)
    }

    //BR-040
    if(policyPeriod.GLLineExists && (policyPeriod.CancellationDate != null || policyPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED) &&
        completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER || elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER).Count == 1).HasElements){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBEROFFER)
    }

    //BR-052
    if(policyPeriod.GLLineExists && (policyPeriod.CancellationDate != null || policyPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED) &&
        completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFERNA || elt1.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM).HasElements).HasElements){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLOFFERNA)
    }

    //BR-056
    if(policyPeriod.GLLineExists && (policyPeriod.CancellationDate != null || policyPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED) &&
        completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER || elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERNA).Count == 1).HasElements){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBERNA)
    }

    //BR-060
    if(policyPeriod.GLLineExists && (policyPeriod.CancellationDate != null || policyPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED) &&
        completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFERNR || elt1.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM).Count == 1).HasElements){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLOFFERNR)
    }

    //BR-064
    if(policyPeriod.GLLineExists && (policyPeriod.CancellationDate != null || policyPeriod.Status == PolicyPeriodStatus.TC_NONRENEWED) &&
        completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER || elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERNR).Count == 1).HasElements){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBERNR)
    }

    //BR-048 Initiate PL Policy Change//Need to be tested yet.
    var ereCyberRes = completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERRESCINDED).Count == 1).HasElements
    var ereCyb = completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER).Count >= 1).HasElements
    if(!ereCyb){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBERRESCINDED)
    }else {
      if (ereCyb) {
        if (ereCyberRes) {
          filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_ERECYBERRESCINDED)
        }
      }
    }

    //BR-044 Initiate PL Policy Change//Need to be tested yet.
    var erePLRes = completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_EREPLRESCINDED).Count == 1).HasElements
    var erePL = completedTransactions.where(\elt -> elt.Subtype == Job.TC_POLICYCHANGE && elt.ChangeReasons.where(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM).Count >= 1).HasElements
    if(!erePL){
      filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLRESCINDED)
    }else {
      if (erePL) {
        if (erePLRes) {
          filteredChangeReasons.remove(ChangeReasonType_TDIC.TC_EREPLRESCINDED)
        }
      }
    }

    return filteredChangeReasons.sort()
  }


  static function checkforEREReasons_TDIC(policyChange : PolicyChange,policyPeriod : PolicyPeriod): Boolean{
    if(policyPeriod.GLLineExists) {
      var patternERE = ClausePatternLookup.getCoveragePatternByCodeIdentifier("GLExtendedReportPeriodCov_TDIC")
      var patternCyberERE = ClausePatternLookup.getCoveragePatternByCodeIdentifier("GLCyvSupplementalERECov_TDIC")
      //BR-047
      if (policyChange.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EREPLRESCINDED).Count == 1) {
        if (policyPeriod.GLLineExists && policyPeriod.GLLine.GLExtendedReportPeriodCov_TDICExists)
          policyPeriod.GLLine.setCoverageExists(patternERE, false)
      }
      //BR-051
      if (policyChange.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERRESCINDED).Count == 1) {
        if (policyPeriod.GLLineExists && policyPeriod.GLLine.GLCyvSupplementalERECov_TDICExists)
          policyPeriod.GLLine.setCoverageExists(patternCyberERE, false)
      }

      //BR-026
      if (policyChange.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI).HasElements) {
        var pattern1 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("GLDentalEmpPracLiabCov_TDIC")
        policyPeriod.GLLine.setCoverageExists(pattern1, false)
      }
      //BR-027
      if (policyChange.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM).HasElements) {
        policyPeriod.GLLine.setCoverageExists(patternERE, true)
        tdic.web.pcf.helper.PolicyCancellationScreenHelper.setExtendedReportingEffDates(patternERE, policyPeriod.GLLine)
      } else {
        policyPeriod.GLLine.setCoverageExists(patternERE, false)
      }
      //BR-027
      if (policyChange.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER).HasElements) {
        policyPeriod.GLLine.setCoverageExists(patternCyberERE, true)
        tdic.web.pcf.helper.PolicyCancellationScreenHelper.setExtendedReportingEffDates(patternCyberERE, policyPeriod.GLLine)
      } else {
        policyPeriod.GLLine.setCoverageExists(patternCyberERE, false)
      }
      //BR-028
      var patternEREDEPL = ClausePatternLookup.getCoveragePatternByCodeIdentifier("GLExtendedReportPeriodDPLCov_TDIC")
      if (policyChange.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI).HasElements) {
        policyPeriod.GLLine.setCoverageExists(patternEREDEPL, true)
        tdic.web.pcf.helper.PolicyCancellationScreenHelper.setExtendedReportingEffDates(patternEREDEPL, policyPeriod.GLLine)
      } else {
        policyPeriod.GLLine.setCoverageExists(patternEREDEPL, false)
      }
    }
  return true
  }

  static function setPolicyChangeEffDate_TDIC(period : PolicyPeriod) : Date{
    var cancelJob = period.Policy.Jobs.where(\elt -> elt.Subtype == Job.TC_CANCELLATION && elt.DisplayStatus == "Bound").sortByDescending(\cancelledjob -> cancelledjob.SelectedVersion.EditEffectiveDate).first() //GWPS-2075: fixing cancellation condition

    if(cancelJob!=null && cancelJob.LatestPeriod.RefundCalcMethod != CalculationMethod.TC_FLAT) {
      return cancelJob.LatestPeriod.EditEffectiveDate.addDays(-1)
    }
    var renewalJob = period.Policy.Jobs.where(\elt -> elt.Subtype == Job.TC_RENEWAL && elt.LatestPeriod.PeriodDisplayStatus == "Non-renewed").first()
    if(renewalJob != null){
      return period.PeriodEnd.addDays(-1)
    }
    if(cancelJob!=null && cancelJob.LatestPeriod.RefundCalcMethod == CalculationMethod.TC_FLAT && cancelJob.LatestPeriod.BasedOn.Renewal!=null){
      return cancelJob.LatestPeriod.BasedOn.Renewal.LatestPeriod.BasedOn.PeriodEnd.addDays(-1)
    }
    return period.EditEffectiveDate
  }

  /**
   * create by: SanjanaS
   * @description: method to implement BR-024, BR-042, BR-046, BR-50 in
   * PC-PL Policy Change - Initiate Policy Change
   * @create time: 3:35 PM 03/04/2020
   * @param changeReason ChangeReason_TDIC
   * @return:
   */
  static var isERESelected : boolean = false
  static function checkChangeReason_TDIC(changeReason: ChangeReason_TDIC) {
    var ereReasons = {"ereCyber", "ereCyberNR", "ereCyberRescinded", "erePLOffer", "erePLOfferNR", "ereCyberNA", "ereCyberOffer", "erePLOfferNA", "erePLRescinded", "extendedreportingperiodendorsementplcm"}
    if (ereReasons.contains(changeReason.ChangeReason.Code)){
      isERESelected =  true
    }
    else {
      isERESelected = false
    }
  }



  /**
   * create by: SanjanaS
   * @description: method to implement BR-024, BR-042, BR-046, BR-50 in
   * PC-PL Policy Change - Initiate Policy Change
   * @create time: 3:35 PM 03/04/2020
   * @param policyPeriod PolicyPeriod
   * @return: java.util.Date
   */
  static function getEffDate_TDIC(policyPeriod: PolicyPeriod, changedEffDate: Date, changeReason: ChangeReason_TDIC): Date{
    var ereReasonsA = {"ereCyber", "ereCyberRescinded", "ereCyberOffer", "erePLOffer", "erePLRescinded", "extendedreportingperiodendorsementplcm"}
    var ereReasonsB = {"erePLOfferNR", "ereCyberNA", "ereCyberNR", "erePLOfferNA"}
    var effDate = setPolicyChangeEffDate_TDIC(policyPeriod)
    if(isERESelected and ereReasonsA.contains(changeReason.ChangeReason.Code)){
      return effDate
    }
    if(isERESelected and ereReasonsB.contains(changeReason.ChangeReason.Code)){
      return effDate.addDays(1)
    }
    if(changeReason.ChangeReason.Code== ChangeReasonType_TDIC.TC_REGENERATEDECLARATION.Code){
      return policyPeriod.EditEffectiveDate
    }
    if((changeReason.ChangeReason.Code == ChangeReasonType_TDIC.TC_EREPLDOCGEN.Code) or (changeReason.ChangeReason.Code == ChangeReasonType_TDIC.TC_ERECYBDOCGEN.Code)){
      return effDate
    }
    return changedEffDate != null? changedEffDate: policyPeriod.EditEffectiveDate
  }




  /**
   * create by: SanjanaS
   * @description: method to implement BR-024, BR-042, BR-046, BR-50 in
   * PC-PL Policy Change - Initiate Policy Change
   * @create time: 3:35 PM 03/04/2020
   * @param changeReason ChangeReason_TDIC
   * @return: 
   */
  static function checkRemovedReason_TDIC(checkedValue : ChangeReason_TDIC) {
    var ereReasons = {"ereCyber", "ereCyberNR", "ereCyberRescinded", "erePLOffer", "erePLOfferNR", "ereCyberNA", "ereCyberOffer", "erePLOfferNA", "erePLRescinded", "extendedreportingperiodendorsementplcm"}
    var availableChangeReasons = checkedValue.Job.ChangeReasons
    if (ereReasons.contains(checkedValue.ChangeReason.Code) and !availableChangeReasons.hasMatch(\cr -> ereReasons.contains(cr.ChangeReason.Code))) {
      isERESelected = false
    }
  }

  public static function getChangeReasons_TDIC(policyPeriod : PolicyPeriod): List<ChangeReasonType_TDIC> {
    if (policyPeriod.BOPLineExists) {
      if (policyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
        return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_BOPCHANGEREASONS.TypeKeys)
      }
      if (policyPeriod.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
        return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_LRPCHANGEREASONS.TypeKeys)
      }
    }
    else if (policyPeriod.GLLineExists) {
      if (policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and (policyPeriod.isCanceled() or policyPeriod.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code or policyPeriod.RenewalStatus_TDIC.DisplayName == PolicyPeriodStatus.TC_NONRENEWED.DisplayName)) {
        return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_PLCMCHANGEREASONS.TypeKeys)
      }
      if (policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and !((policyPeriod.isCanceled() and policyPeriod.RefundCalcMethod != CalculationMethod.TC_FLAT) or policyPeriod.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code)) {
        return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_PLCMCHANGEREASONSWITHOUTCANCEL_TDIC.TypeKeys)
      }
      if (policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC" and ((policyPeriod.isCanceled() and policyPeriod.RefundCalcMethod != CalculationMethod.TC_FLAT) or policyPeriod.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code)) {
        return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_PLOCCHANGEREASONS.TypeKeys)
      }
      if (policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC" and !((policyPeriod.isCanceled() and policyPeriod.RefundCalcMethod != CalculationMethod.TC_FLAT) or policyPeriod.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code)) {
        return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_PLOCCHANGEREASONSWITHOUTCANCEL_TDIC.TypeKeys)
      }
      if (policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC" and (policyPeriod.isCanceled() or policyPeriod.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code or policyPeriod.RenewalStatus_TDIC.DisplayName == PolicyPeriodStatus.TC_NONRENEWED.DisplayName)) {
        return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_CYBCHANGEREASONS.TypeKeys)
      }
      if (policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC" and !((policyPeriod.isCanceled() and policyPeriod.RefundCalcMethod != CalculationMethod.TC_FLAT) or policyPeriod.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code)) {
        return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_CYBCHANGEREASONSWITHOUTCANCEL_TDIC.TypeKeys)
      }
    }
    else if (policyPeriod.WC7LineExists) {
      return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.TF_WCCHANGEREASONS.TypeKeys)
    }
    // this will retrun all the typekey values. based on the future LOB changes(other than WC and BOP) we need to create a filter and add the chagnes as above.
    return gw.web.job.policychange.StartPolicyChangeUIHelper.filterChangeReasons_TDIC(policyPeriod,ChangeReasonType_TDIC.getTypeKeys(false))
  }
}