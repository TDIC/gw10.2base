package gw.question

uses gw.api.productmodel.QuestionSet
uses gw.web.productmodel.ProductModelSyncIssueWrapper

enhancement AnswerContainerEnhancement : entity.AnswerContainer {

  /**
   * Syncs questions against the product model, fixes all issues marked as ShouldFixDuringNormalSync,
   * and returns all the issues found regardless of whether or not they were fixed.
   */
  function syncQuestions() : List<ProductModelSyncIssueWrapper> {
    this.clearQuestionDependencies()
    var originalIssues = this.checkAnswersAgainstProductModel()
    var issueWrappers = ProductModelSyncIssueWrapper.wrapIssues(originalIssues)
    issueWrappers.fixDuringNormalSync(this)
    return issueWrappers
  }

  /**
   * Syncs questions in questionSetsToSync against the product model, fixes all issues marked as ShouldFixDuringNormalSync,
   * and returns all the issues found regardless of whether or not they were fixed.
   */
  function syncQuestions(questionSetsToSync : QuestionSet[]) : List<ProductModelSyncIssueWrapper> {
    this.clearQuestionDependencies()
    var originalIssues = this.checkAnswersAgainstProductModel(questionSetsToSync)
    var issueWrappers = ProductModelSyncIssueWrapper.wrapIssues(originalIssues)
    issueWrappers.fixDuringNormalSync(this)
    return issueWrappers
  }

  /**
   * Returns true if this answer container has an answer for any question in "questionSet"
   */
  function hasAnswerForQuestionSet(questionSet : QuestionSet) : boolean {
    return questionSet.OrderedQuestions.hasMatch(\question -> this.getAnswer(question) != null)
  }

  /*
   * create by: SanjanaS
   * @description: method to get the NewGradDiscountEffectiveDate
   * @create time: 05:00 PM 02/07/2020
    * @param null
   * @return: java.util.Date
   */
  function setNewGradDiscountEffDates_TDIC() {
    if(not isLegacyPolicy()) {
      setNewGradDiscountEffectiveDate_TDIC()
      setNewGradDiscountExpirationDate_TDIC()
    }
  }

  /**
   *
   */
  function setNewGradDiscountEffectiveDate_TDIC() {
    var newGradDiscEffDateAnswer = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "NewGradDiscEffDate_TDIC")
    var branch = this.AssociatedPolicyPeriod
    if (branch.Job typeis Submission) {
      if(branch.GLLine.GLNewDentistDiscount_TDICExists) {
        newGradDiscEffDateAnswer.setDateAnswer(branch.PeriodStart)
      } else if(branch.GLLine.GLNewGradDiscount_TDICExists) {
        if(newGradDiscEffDateAnswer.DateAnswer == null) {
          newGradDiscEffDateAnswer.setDateAnswer(branch.PeriodStart)
        }
      }
    }
  }

  /**
   *
   */
  function setNewGradDiscountExpirationDate_TDIC() {
    var period = this.AssociatedPolicyPeriod
    if(period.GLLineExists and period.Offering.CodeIdentifier != "PLCyberLiab_TDIC") {
      var newGradDiscEffDate = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "NewGradDiscEffDate_TDIC")
      if (newGradDiscEffDate != null and newGradDiscEffDate.DateAnswer != null) {
        var expDateQuestion = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "NewGradDiscExpDate_TDIC")
        expDateQuestion.setDateAnswer(newGradDiscEffDate.DateAnswer.trimToMidnight().addYears(3))
      }
    }
  }

  /**
   * GPC-3581 - New Grad Discount effective date and expiration date should be optional for Migrated Policies.
   * GWPS-1694 -New Grad Discount effective date and expiration date should be optional when New Dentist Program or New Graduate discount is not selected.
   */
  function isNewGradEffectiveDateMandatory_TDIC() : Boolean {
    var period = this.AssociatedPolicyPeriod
    var validJobTypes = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_RENEWAL, typekey.Job.TC_POLICYCHANGE}

    if (validJobTypes.contains(period.GLLine.JobType)) {
      if (not period.GLLine.GLNewDentistDiscount_TDICExists and not period.GLLine.GLNewGradDiscount_TDICExists) {
        return false
      } else if (isLegacyPolicy()) {
        return false
      }
    }
    return true
  }

  /**
   *
   */
  function isNewGradEffectiveDateEditable_TDIC() : Boolean {
    var period = this.AssociatedPolicyPeriod
    var validJobTypes = {typekey.Job.TC_SUBMISSION, typekey.Job.TC_RENEWAL, typekey.Job.TC_POLICYCHANGE}

    //New grad on or prior to 1992 are non-editable
    var newGradDiscEffDate = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "NewGradDiscEffDate_TDIC")
    if(isLegacyPolicy() and newGradDiscEffDate.DateAnswer != null and newGradDiscEffDate.DateAnswer.YearOfDate <= 1992) {
      return false
    } else if(period.GLLineExists
        and validJobTypes.contains(period.GLLine.JobType)                 //Submission, Renewal & PolicyChange
        and period.Offering.CodeIdentifier != "PLCyberLiab_TDIC"          //Not Cyber
        and not period.GLLine.GLNewDentistDiscount_TDICExists             //Not editable for New Dentists
        and perm.System.editnewgradeffdate_tdic) {                        //must have permission to edit
      return true
    }
    return false
  }

  /**
   *
   * @return
   */
  function isLegacyPolicy() : Boolean {
    if (this.AssociatedPolicyPeriod.BasedOn != null
        and ((this.AssociatedPolicyPeriod.LegacyPolicyNumber_TDIC != null
          and this.AssociatedPolicyPeriod.LegacyPolicyNumber_TDIC.trim() != "")
        or this.AssociatedPolicyPeriod.BasedOn.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION)) {
      return true
    }
    return false
  }

  function setDoYouPerformProvideBelowServices_TDIC(){
    if(this.AssociatedPolicyPeriod.GLLineExists){
      var doYouPerformProvide = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "DoYouPerformProvideBelowServices_TDIC").BooleanAnswer
      if(!doYouPerformProvide){
        //bloodCompatibilityTests, ChelationTherapy, CosmeticSurgery, DermalFillers, HomeopathicTherapies, Liposuction, Nico, Teledentistry
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "bloodCompatibilityTests").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "ChelationTherapy").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "CosmeticSurgery").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "DermalFillers").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "HomeopathicTherapies").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "Liposuction").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "Nico").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "Teledentistry").setBooleanAnswer(Boolean.FALSE)
      }
    }
  }


  function setGLLineQuestionsWithDefaultValues(){
    var accountHolderContact = this.AssociatedPolicyPeriod.Policy.Account.AccountHolderContact
    //Set the "DentalLicenseNumber_TDIC" Question
    var dentalLicenseNumber = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "DentalLicenseNumber_TDIC")
    if((dentalLicenseNumber != null) and (dentalLicenseNumber.AnswerValue == null) and (accountHolderContact typeis Person) and (accountHolderContact.LicenseNumber_TDIC != null)){
      dentalLicenseNumber.setAnswerValue(accountHolderContact.LicenseNumber_TDIC)
    }
    //Set the "ADANumber_TDIC" Question
    var adaNumber = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "ADANumber_TDIC")
    if((adaNumber != null) and (adaNumber.AnswerValue == null) and (accountHolderContact typeis Person) and (accountHolderContact.ADANumberOfficialID_TDIC != null)){
      adaNumber.setAnswerValue(accountHolderContact.ADANumberOfficialID_TDIC)
    }
    //Set the "DentalLicenseState_TDIC" Question
    var dentalLicenseState = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "DentalLicenseState_TDIC")
    if((dentalLicenseState != null) and (dentalLicenseState.ChoiceAnswer == null)){
      dentalLicenseState.setChoiceAnswer(gw.api.upgrade.PCCoercions.makeProductModel<gw.api.productmodel.Question>("DentalLicenseState_TDIC").
          Choices.firstWhere(\elt -> elt.ChoiceCode == this.AssociatedPolicyPeriod.BaseState.DisplayName))
    }
  }


  function setTreatPatientsUnderBelowAnestheticModalities_TDIC(){
    if(this.AssociatedPolicyPeriod.GLLineExists){
      var treatPatients = this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "TreatPatientsUnderBelowAnestheticModalities_TDIC").BooleanAnswer
      if(!treatPatients){
        //Localanesthesia, N2OO2analgesia, Oralconscioussedation, Conscioussedation, Conscioussedationoffice, Generalanesthesiaoffice,
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "Localanesthesia").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "N2OO2analgesia").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "Oralconscioussedation").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "Conscioussedation").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "Conscioussedationoffice").setBooleanAnswer(Boolean.FALSE)
        this.Answers.firstWhere(\ans -> ans.Question.CodeIdentifier == "Generalanesthesiaoffice").setBooleanAnswer(Boolean.FALSE)
      }
    }
  }
}
