package gw.account

uses java.util.HashSet
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException

enhancement AccountLocationEnhancement : entity.AccountLocation {
  
  /**
   * Return true if this account location is the primary location on the account.
   */
  property get Primary() : boolean {
    return this.Account.PrimaryLocation == this
  }
  
  /**
   * Return true if this account location is primary or is currently in use as a policy location
   */
  property get InUse() : boolean {
    return Primary or not this.canRemove()
  }

  /**
   * Validates that the State and Country have not been changed on the AccountLocation.
   */
  function validateStateAndCountryHaveNotChanged() {
    var oldState = (this.OriginalVersion as AccountLocation).State
    var oldCountry = (this.OriginalVersion as AccountLocation).Country
    var newState = this.State
    var newCountry = this.Country

    if (oldState != newState) {
      throw new DisplayableException(DisplayKey.get("Web.AccountLocation.StateChanged", oldState, newState))
    } else if (oldCountry != newCountry) {
      throw new DisplayableException(DisplayKey.get("Web.AccountLocation.CountryChanged", oldCountry, newCountry))
    }
  }

  //20150512 TJ Talluto: US1296 Discretionary Synchronizing

  public function getImpactedPoliciesForAccountLocationChange_TDIC(): HashSet<PolicyPeriod> {
    //print ("AccountLocationEnhancement : getImpactedPoliciesForLocationChange_TDIC()")
    var tmpSet: HashSet<PolicyPeriod> = new HashSet<PolicyPeriod>()
    for (each in this.Account.Policies) {
      //.where( \ elt -> elt.Branch.Status == typekey.PolicyPeriodStatus.TC_BOUND)){
      var tmpLatestBoundPeriod = each.LatestBoundPeriod
      //Defect GW-407- Filter the List of impacted policy, which is In Force Policies.
      if (!tmpLatestBoundPeriod.Canceled and !(gw.api.util.DateUtil.currentDate() >= tmpLatestBoundPeriod.PeriodEnd)){
        for (eachLocation in tmpLatestBoundPeriod.CurrentAndFutureLocations) {
          if (eachLocation.isUpToDate()){
            tmpSet.add(each.LatestBoundPeriod)
          }
        }
      }
    }
    return tmpSet
  }
}
