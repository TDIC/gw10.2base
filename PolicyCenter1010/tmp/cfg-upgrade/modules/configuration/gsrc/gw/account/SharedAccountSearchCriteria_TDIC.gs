package gw.account
uses gw.api.database.Query
uses gw.api.database.IQueryBeanResult
uses gw.search.EntitySearchCriteria
uses gw.api.database.ISelectQueryBuilder

@Export
class SharedAccountSearchCriteria_TDIC extends SharedContactAccountSearchCriteria {

  //BrittoS  - PNI search critiera
  var _contactRoles : typekey.AccountContactRole[]

  // contact roles to include in shared contact search
  property get SearchableSharedContactRoles() : typekey.AccountContactRole[] {
    if(_contactRoles.HasElements) {
      return _contactRoles
    }
    return {typekey.AccountContactRole.TC_ACCOUNTHOLDER, typekey.AccountContactRole.TC_NAMEDINSURED}
  }

  property set SearchableSharedContactRoles(roles : typekey.AccountContactRole[]) {
    _contactRoles = roles
  }

}
