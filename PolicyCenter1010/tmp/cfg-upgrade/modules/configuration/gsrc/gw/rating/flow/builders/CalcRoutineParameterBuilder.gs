package gw.rating.flow.builders

uses gw.api.databuilder.DataBuilder
uses gw.api.productmodel.CoveragePattern
uses gw.lang.reflect.IType

class CalcRoutineParameterBuilder extends DataBuilder<CalcRoutineParameter, CalcRoutineParameterBuilder> {

  construct() {
    super(CalcRoutineParameter)
  }

  function withCode(code : CalcRoutineParamName) : CalcRoutineParameterBuilder {
    set(CalcRoutineParameter#Code.PropertyInfo, code)
    return this
  }

  function withCoverageCode(coveragePattern : CoveragePattern) : CalcRoutineParameterBuilder {
    set(CalcRoutineParameter#CoveragePattern.PropertyInfo, coveragePattern.CodeIdentifier)
    return this
  }
  
  function withCoverageWrapper(wrapperClass : IType) : CalcRoutineParameterBuilder {
    set(CalcRoutineParameter#UseWrapper.PropertyInfo, true)
    set(CalcRoutineParameter#WrapperClass.PropertyInfo, wrapperClass.Name)
    return this
  }

  function withParamType(paramType : IType) : CalcRoutineParameterBuilder {
    set(CalcRoutineParameter#ParamType.PropertyInfo, paramType.Name)
    return this
  }

  @Deprecated("Use withParamType(paramType : IType) instead")
  function withParamType(paramtype : String) : CalcRoutineParameterBuilder {
    set(CalcRoutineParameter#ParamType.PropertyInfo, paramtype)
    return this
  }
  
  function withWritable(paramType : Boolean) : CalcRoutineParameterBuilder {
   set(CalcRoutineParameter#Writable.PropertyInfo,paramType)
   return this
  }

  function isWritable() : CalcRoutineParameterBuilder {
    set(CalcRoutineParameter#Writable.PropertyInfo, true)
    return this
  }

}
