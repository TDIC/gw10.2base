package gw.lob.wc7

uses gw.validation.PCValidationContext
uses gw.validation.ValidationUtil
uses gw.lob.common.AbstractPolicyInfoValidation
uses gw.api.locale.DisplayKey

@Export
class WC7PolicyInfoValidation extends AbstractPolicyInfoValidation<WC7WorkersCompLine> {

  construct(valContext : PCValidationContext, polLine : WC7WorkersCompLine) {
    super(valContext, polLine);
    _line = polLine
  }

  override protected function validateImpl() {
    super.validateImpl();
    checkOwnerOfficersPresent_TDIC()
    checkSoleProprietorshipHasNoOwnerOfficers_TDIC()
    checkTotalOwnershipOfOwnerOfficersIs100_TDIC()
  }
  
  /**
   * US890, robk
   */
  private function checkOwnerOfficersPresent_TDIC() {
    Context.addToVisited(this, "checkOwnerOfficersPresent_TDIC")
    if (shouldHaveAtLeastOneOwnerOfficer_TDIC(_line.Branch.PolicyOrgType_TDIC) and PolicyOwnerOfficers.length < 1) {
      Result.addError(_line, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.WC7.Validation.MissingOwnerOfficer", 1, _line.Branch.PolicyOrgType_TDIC))
    }
    else if (shouldHaveAtLeastTwoOwnerOfficers_TDIC(_line.Branch.PolicyOrgType_TDIC) and PolicyOwnerOfficers.length < 2) {
      Result.addError(_line, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.WC7.Validation.MissingOwnerOfficer", 2, _line.Branch.PolicyOrgType_TDIC))
    }
  }

  private function shouldHaveAtLeastOneOwnerOfficer_TDIC(orgType : typekey.PolicyOrgType_TDIC) : boolean {
    return orgType == typekey.PolicyOrgType_TDIC.TC_PRIVATECORP or
           orgType == typekey.PolicyOrgType_TDIC.TC_LLC
           // GW-3096 TJT 20180216 remove this constraint
           // or orgType == typekey.PolicyOrgType_TDIC.TC_NONPROFIT
  }

  private function shouldHaveAtLeastTwoOwnerOfficers_TDIC(orgType : typekey.PolicyOrgType_TDIC) : boolean {
    return orgType == typekey.PolicyOrgType_TDIC.TC_JOINTVENTURE or
        orgType == typekey.PolicyOrgType_TDIC.TC_PARTNERSHIP
  }

  private property get PolicyOwnerOfficers() : WC7PolicyOwnerOfficer[] {
    //BrittoS - fix for GPC-2895
    //var policyOwnerOfficers = _line.WC7PolicyOwnerOfficers.where( \ oo -> oo.IntrinsicType != PolicyEntityOwner_TDIC)
    var policyOwnerOfficers = _line.Branch.WC7Line.WC7PolicyOwnerOfficers.where( \ oo -> oo.IntrinsicType != PolicyEntityOwner_TDIC)
    return policyOwnerOfficers == null ? new WC7PolicyOwnerOfficer[] {} : policyOwnerOfficers
  }

  /**
   * US890, robk
   */
  private function checkSoleProprietorshipHasNoOwnerOfficers_TDIC() {
    Context.addToVisited(this, "checkSoleProprietorshipHasNoOwnerOfficers_TDIC")
    if (_line.Branch.PolicyOrgType_TDIC == typekey.PolicyOrgType_TDIC.TC_SOLEPROPSHIP and PolicyOwnerOfficers.length > 0) {
      Result.addError(_line, TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.WC7.Validation.OwnerOfficerNotAllowed", _line.Branch.PolicyOrgType_TDIC))
    }
  }

  /**
   * US890, robk
   */
  private function checkTotalOwnershipOfOwnerOfficersIs100_TDIC() {
    Context.addToVisited(this, "checkTotalOwnershipOfOwnerOfficersIs100_TDIC")
    var ownerOfficers = PolicyOwnerOfficers
    if (ownerOfficers == null or ownerOfficers.length == 0) {
      return
    }

    var totalOwnership = 0
    for (anOwnerOfficer in ownerOfficers) {
      // BrianS - Adding nulls will get a null pointer exception.  Treat as 0%
      if (anOwnerOfficer.WC7OwnershipPct != null) {
        totalOwnership += anOwnerOfficer.WC7OwnershipPct.intValue()
      }
    }

    //20171023 TJT GW-2990: change from ownership != 100 to ownership > 100, and add a UWI for ownership < 100
    if (totalOwnership > 100) {  //if (totalOwnership != 100) {
      //Result.addError(_line, "default", displaykey.TDIC.Web.Policy.WC7.Validation.OwnershipMustSumTo100)
      Result.addError(_line, TC_DEFAULT, DisplayKey.get("TDIC.OwnershipExceeds100"))
    }
  }
}
