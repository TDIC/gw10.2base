package gw.lob.gl.rating

uses entity.windowed.GLCostVersionList
uses gw.financials.PolicyPeriodFXRateCache

class GLMinPremCostData_TDIC extends GLCostData<GLMinPremCost_TDIC> {
  var _locationID : String
  var _costType : GLCostType_TDIC
  construct(effDate : Date, expDate : Date, stateArg : Jurisdiction) {
    super(effDate, expDate, stateArg, null, null)
  }

  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction) {
    super(effDate, expDate, c, rateCache, stateArg, null, null)
  }

  construct(c : GLMinPremCost_TDIC, rateCache : PolicyPeriodFXRateCache) {
    super(c, rateCache)
    _costType = c.GLCostType
  }

  construct (effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction, costType : GLCostType_TDIC){
    super(effDate, expDate, c, rateCache, stateArg, null, null)
    _costType = costType
  }

  override function getVersionedCosts(line : GeneralLiabilityLine) : List<gw.pl.persistence.core.effdate.EffDatedVersionList> {
    return line.VersionList.GLCosts.where( \ vl -> (vl typeis GLCostVersionList) and versionListMatches(vl.AsOf(EffectiveDate))).toList()
  }

  private function versionListMatches(cost : GLCost) : boolean {
    return cost typeis GLMinPremCost_TDIC and cost.State == State
  }

  override property get GLKeyValues() : List<Object> {
    return {_costType.Code}
  }

  override function setSpecificFieldsOnCost( line: GeneralLiabilityLine, cost: GLMinPremCost_TDIC ) {
    super.setSpecificFieldsOnCost( line, cost )
    cost.GLCostType = _costType
  }


  private function matchesStep(cost : GLMinPremCost_TDIC) : boolean {
    return cost.GLCostType == _costType
  }
}