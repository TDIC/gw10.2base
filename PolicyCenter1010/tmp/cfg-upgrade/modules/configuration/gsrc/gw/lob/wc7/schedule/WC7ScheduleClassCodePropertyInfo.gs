package gw.lob.wc7.schedule

uses gw.api.productmodel.AbstractSchedulePropertyInfo
uses gw.api.productmodel.IValueRangeGetter
uses gw.api.productmodel.SchedulePropertyValueProvider
uses gw.api.util.StateJurisdictionMappingUtil
uses java.util.List

/**
* A {@link ScheduleTypeKeyPropertyInfo} specific to {@link WC7ClassCode.eti}
*/
@Export
class WC7ScheduleClassCodePropertyInfo extends AbstractSchedulePropertyInfo<WC7ClassCode> {
  var _wc7Line : WC7WorkersCompLine
  var _jurisdiction: Jurisdiction

  construct(
      columnName : String,
      colLabel : String,
      jurisdictionScheduleClause : WC7JurisdictionScheduleCond,
      isRequired : boolean,
      isIdentityColumn: boolean,
      priority: int
  ) {
    this(jurisdictionScheduleClause.Branch.WC7Line,
      columnName,
      colLabel,
      isRequired,
      isIdentityColumn,
      priority,
      jurisdictionScheduleClause.WC7Jurisdiction.Jurisdiction
    )
  }

  /**
   * Constructs a new column info with the passed arguments.
   * @param wc7Line      {@link WC7WorkersCompLine}
   * @param columnName       {@link #Column}'s name
   * @param colLabel         Text to display for the column label in LVs
   * @param isRequired       Whether or not the input cell should be required
   * @param juris            Jurisdiction to attach to
   *
   */
  construct(
      wc7Line : WC7WorkersCompLine,
      columnName : String,
      colLabel : String,
      isRequired : boolean,
      isIdentityColumn : boolean,
      priority : int,
      juris : Jurisdiction) {

    super(WC7ScheduledItem, columnName, colLabel, isRequired, isIdentityColumn, priority)
     _wc7Line = wc7Line
     _jurisdiction = juris
  }

  override property get ValueType(): String {
    return "ForeignKey"
  }

  override public function createValueProvider( item : ScheduledItem ) : SchedulePropertyValueProvider {
    return new WC7ScheduleClassCodePropertyValueProvider<WC7ClassCode>(item, this.getPropertyInfo());
  }

  /**
   * Range of values; useful for RangeCells in the UI
   */
  override public property get ValueRange() : List<WC7ClassCode> {
    var valueRangeGetter = new WC7ScheduleClassCodePropertyInfo.WC7ClassCodeValueRangeGetter(_wc7Line, _jurisdiction)
    return valueRangeGetter.ValueRange.cast(WC7ClassCode).toList()
  }

  class WC7ClassCodeValueRangeGetter implements IValueRangeGetter {
    var _wc7LineForClassCodeValueRangeGetter : WC7WorkersCompLine
    var _state : State

    construct(wc7Line : WC7WorkersCompLine, juris : Jurisdiction) {
      _wc7LineForClassCodeValueRangeGetter = wc7Line
      _state = StateJurisdictionMappingUtil.getStateMappingForJurisdiction(juris)
    }

    override property get ValueRange() : KeyableBean[] {
      return _wc7LineForClassCodeValueRangeGetter.getClassCodesForWC7CoveredEmployees(_state)
    }
  }

}
