package gw.lob.wc7.availability

uses gw.api.domain.covterm.CovTerm
uses gw.api.productmodel.ClausePattern
uses gw.entity.ITypeFilter

uses java.util.Set

class WC7StateFilterFactory {
    var _availability : WC7ClauseAvailability
    var _policyJurisdictions : Set<Jurisdiction>
    
    construct(wc7Line : entity.WC7WorkersCompLine) {
      _availability = WC7ClauseAvailability.Instance
      _policyJurisdictions = wc7Line.LocationJurisdictions?.toSet()
    }
    
    function createFilterForClause(clausePattern : ClausePattern, wc7Line : entity.WC7WorkersCompLine) : ITypeFilter<Jurisdiction> {
      return \ -> _policyJurisdictions.where(
          \ state -> _availability.includesClause(clausePattern, state, wc7Line))
    }

    function createFilterForCovTerm(covTerm : CovTerm) : ITypeFilter<Jurisdiction> {
      return \ -> _policyJurisdictions.where(
          \ state -> _availability.includesCovTerm(covTerm, state))
    }
  }