package gw.lob.bop
uses gw.account.AccountContactRoleToPolicyContactRoleSyncedField

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 2/15/2020
 * Create Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
enhancement TDIC_BOPPolicyOwnerOfficerEnhancement : BOPPolicyOwnerOfficer_TDIC {

   /**
   * Shared and revisioned relationship/title.

  property get RelationshipTitle() : Relationship {
    return AccountContactRoleToPolicyContactRoleSyncedField.BOPRelationshipTitle.getValue(this)
  }

  /**
   * Shared and revisioned relationship/title.
   */
  property set RelationshipTitle(arg : Relationship) {
    AccountContactRoleToPolicyContactRoleSyncedField.BOPRelationshipTitle.setValue(this, arg)
  }*/

  property get Role_TDIC() : String {
    return this.IntrinsicType.DisplayName
  }

}
