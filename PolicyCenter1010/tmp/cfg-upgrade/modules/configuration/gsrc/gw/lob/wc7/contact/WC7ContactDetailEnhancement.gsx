package gw.lob.wc7.contact

uses gw.api.domain.Clause
uses java.lang.IllegalArgumentException

@Export
enhancement WC7ContactDetailEnhancement : entity.WC7ContactDetail {

  function isIncluded() : boolean {
    return this.ParentClause typeis PolicyCondition
  }

  property get Inclusion() : typekey.Inclusion {
    return isIncluded() ? TC_INCL : TC_EXCL
  }

  property get Address() : Address {
    return this.WC7Contact.AccountContactRole.AccountContact.Contact.PrimaryAddress
  }

  // Can't define this as a property setter, because a getter is defined on the entity and
  // "Enhancements cannot override properties."
  function setParentClause(clause : Clause) {
    if (clause typeis PolicyCondition)
      this.setFieldValue("LaborContactCondition", clause)
    else if (clause typeis Exclusion)
      this.setFieldValue("LaborContactExclusion", clause)
    else
      throw new IllegalArgumentException("'${clause}' is an invalid parent clause " +
          " because it is neither a condition nor an exclusion.")
  }

  property get ContactName() : String {
    return this.WC7Contact.DisplayName
  }
}
