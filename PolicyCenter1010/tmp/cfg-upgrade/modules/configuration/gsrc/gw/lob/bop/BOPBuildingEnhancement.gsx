package gw.lob.bop

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.api.productmodel.ClausePatternLookup
uses gw.api.util.DisplayableException
uses gw.entity.IEntityPropertyInfo
uses gw.entity.IEntityType
uses gw.plugin.Plugins
uses gw.plugin.contact.IContactConfigPlugin

uses java.math.BigDecimal
uses java.math.RoundingMode

uses entity.AccountContact
uses entity.Contact

/**
 * Enhancement methods for {@link entity.BOPBuilding BOPBuilding}
 */
enhancement BOPBuildingEnhancement : entity.BOPBuilding {

  /**
   * @return an array of all {@link entity.BOPBuildingCov Coverages} for this {@link entity.BOPBuilding BOPBuilding}
   * @see entity.BOPBuilding#Coverages
   */
  property get SortedCoverages() : entity.BOPBuildingCov[] {
    return this.Coverages
  }

  /**
   * @return an array of {@link entity.BOPClassCode[] BOPClassCode} class codes associated with the primary named insured's industry code, or an empty list if the industry code has not been selected.
   */
  property get ClassCodes() : BOPClassCode[] {
    var ic = this.PolicyLine.Branch.PrimaryNamedInsured.IndustryCode
    return (ic == null ? new BOPClassCode[0] : ic.BOPClassCodes.toTypedArray())
  }

  /**
   * A display name with location information.
   * <br/><br/>
   * e.g.<br/><br/><i>100 West Tower (1200 Main St. San Carlos, CA 95123)</i><br/><br/>
   * Note: this will depend on the actual {@link entity.BOPBuilding} and {@link entity.BOPLocation} display name implementation.
   *
   * @return the display name of this building with the location's display name appended in parenthesis
   */
  function getBuildingLocationDisplay() : String {
    return this.DisplayName + " (" + this.BOPLocation.DisplayName + ")"
  }

  /**
   * create by: ChitraK
   *
   * @description: Method to set Ordinance Limits
   * @create time: 11:56 AM 7/29/2019
   * @return:
   */
  function setOrdinanceTermLimits_TDIC() {
    if (this.BOPOrdLaw_TDICExists) {
      if (this.BOPBuildingCovExists && this.BOPBuildingCov.BOPBldgLimTerm.Value != null) {
        this.BOPOrdLaw_TDIC.BOPOrdLawUndamageLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value.multiply(0.25).setScale(0, RoundingMode.HALF_UP))
        this.BOPOrdLaw_TDIC.BOPOrdLawDemolitionLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value.multiply(0.10).setScale(0, RoundingMode.HALF_UP))
        this.BOPOrdLaw_TDIC.BOPOrdLawIncCostLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value.multiply(0.25).setScale(0, RoundingMode.HALF_UP))
      }
    }
  }

  // Jeff, Added for GPC-823
  // BR-003	Limit for Building Debris Removal	Default value will be 30% of limit entered for Building Coverage
  function setBuildingDebrisLimit_TDIC() {
    if (this.BOPBuilDebrisRemoval_TDICExists && this.BOPBuildingCov.BOPBldgLimTerm.Value != null)
      this.BOPBuilDebrisRemoval_TDIC.BOPDebrisRemovInclLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value.multiply(0.3).setScale(0, RoundingMode.HALF_UP))
  }

  // Jeff, Added for GPC-823
  // BR-005	Limit for BPP Debris Removal	Default value will be 30% of limit entered for BPP Coverage
  function setBPPDebrisLimit_TDIC() {
    if (this.BOPBPPDebrisRemoval_TDICExists && this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value != null) {
      this.BOPBPPDebrisRemoval_TDIC.BOPBPPDebrisRemoInclLimit_TDICTerm.setValue(this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value.multiply(0.3).setScale(0, RoundingMode.HALF_UP))
    }
  }

  // Jeff, Added for GPC-823 changes
  function setZipcodeTerritoryCodeForEQEQSLCov_TDIC() {
    var zipcode = this.BOPLocation.getLocation().PostalCode.substring(0, 5)
    if (this.BOPEqBldgCovExists) {
      if (this.BOPEqBldgCov.HasBOPEQZipCode_TDICTerm and this.BOPEqBldgCov.BOPEQZipCode_TDICTerm.ValueAsString == null) {
        this.BOPEqBldgCov.BOPEQZipCode_TDICTerm.setValueFromString(zipcode)
      }
      if (this.BOPEqBldgCov.HasBOPEQTerritory_TDICTerm and this.BOPEqBldgCov.BOPEQTerritory_TDICTerm.ValueAsString == null) {
        this.BOPEqBldgCov.BOPEQTerritory_TDICTerm.setValueFromString(getTerritoryCodeByZipCode(zipcode))
      }
    }
  }

  function setZipcodeTerritoryCodeForEQSLCov_TDIC() {
    var zipcode = this.BOPLocation.getLocation().PostalCode.substring(0, 5)
    if (this.BOPEqSpBldgCovExists) {
      if (this.BOPEqBldgCov.HasBOPEQZipCode_TDICTerm and this.BOPEqBldgCov.BOPEQZipCode_TDICTerm.ValueAsString != null) {
        this.BOPEqSpBldgCov.BOPEQSLZipCode_TDICTerm.setValueFromString(zipcode)
      }
      if (this.BOPEqBldgCov.HasBOPEQTerritory_TDICTerm and this.BOPEqBldgCov.BOPEQTerritory_TDICTerm.ValueAsString != null) {
        this.BOPEqSpBldgCov.BOPEQSLTerritory_TDICTerm.setValueFromString(getTerritoryCodeByZipCode(zipcode))
      }
    }
  }

  // Jeff, Added for GPC-823 to get the Territory Code By Zip Code from System Table
  private function getTerritoryCodeByZipCode(postalCode : String) : String {
    var query = Query.make(BOPZipTerritoryCode_TDIC)
    if (postalCode != null) {
      query.compare("ZipCode", Relop.Equals, postalCode)
    }
    var zipTerritoryCodesPair = query.select().FirstResult
    if (zipTerritoryCodesPair != null) {
      return zipTerritoryCodesPair.TerritoryCode
    }
    return null
  }

  /**
   * create by: ChitraK
   *
   * @description: Method to set EarthQuake Limits
   * @create time: 11:55 AM 7/29/2019
   * @return:
   */
  function setEarthquakeLimits_TDIC() {
    var pattern1 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("BOPEqSpBldgCov")
    if (!this.BOPEqBldgCovExists) {
      this.setCoverageExists(pattern1, false)
    }
    if (this.BOPBuildingCovExists && this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value != null) {
      if (this.BOPEqBldgCovExists)
        this.BOPEqBldgCov.BOPEQInclLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value + this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value)
    } else if (this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value != null) {
      if (this.BOPEqBldgCovExists)
        this.BOPEqBldgCov.BOPEQInclLimit_TDICTerm.setValue(this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value)
    }
    else if (this.BOPBuildingCov.BOPBldgLimTerm.Value != null) {
      if (this.BOPEqBldgCovExists)
        this.BOPEqBldgCov.BOPEQInclLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value)
    }
    //Set Territory Codes
    var terrCode = this.setEarthquakeTerritoryCode_TDIC()
    if (this.BOPEqBldgCovExists) {
      if (terrCode != null)
        this.BOPEqBldgCov.BOPEQTerritory_TDICTerm.setValueFromString(terrCode.toString())
    }
  }

  function setEnhancedCoverageLimits(){
    if (this.BOPEncCovEndtCond_TDICExists) {
      this.BOPEncCovFineCond_TDIC.BOPEncCovFineCondIncLimit_TDICTerm.setValue(25000)
      this.BOPEncCovMoneySCond_TDIC.BOPEncCovMoneySCondIncLimit_TDICTerm.setValue(25000)
      this.BOPNewBuildingCov_TDIC.BOPNewBuildIncLimit_TDICTerm.setValue(500000)
      this.BOPNewBPP_TDIC.BOPNewBPPIncLimit_TDICTerm.setValue(250000)
      this.BOPPollCleanRemoval_TDIC.BOPPollCleanRemovInclLimit_TDICTerm.setValue(25000)
      this.BOPFireExtRecharge_TDIC.BOPFireExtRechargeInclLimit_TDICTerm.setValue(5000)
      this.BOPFireDepSerCharge_TDIC.BOPFireDeptSerChargeInclLimit_TDICTerm.setValue(25000)
      this.BOPBPPOffPremises_TDIC.BOPBPPOffPremInclLimit_TDICTerm.setValue(50000)
    }


  }

  function remEnhancedCoverageLimits() {
    if(!(this.BOPEncCovEndtCond_TDICExists) and !(this.Branch.AssociatedPolicyPeriod.Offering.Description == 'Lessor\'s Risk Policy')) {
      this.BOPNewBuildingCov_TDIC.BOPNewBuildIncLimit_TDICTerm.setValue(250000)
      this.BOPNewBPP_TDIC.BOPNewBPPIncLimit_TDICTerm.setValue(100000)
      this.BOPPollCleanRemoval_TDIC.BOPPollCleanRemovInclLimit_TDICTerm.setValue(15000)
      this.BOPFireExtRecharge_TDIC.BOPFireExtRechargeInclLimit_TDICTerm.setValue(1000)
      this.BOPFireDepSerCharge_TDIC.BOPFireDeptSerChargeInclLimit_TDICTerm.setValue(10000)
      this.BOPBPPOffPremises_TDIC.BOPBPPOffPremInclLimit_TDICTerm.setValue(25000)
    }
  }

  /**
   * create by: ChitraK
   *
   * @description: Set EQ SP Coverage
   * @create time: 9:10 PM 12/3/2019
   * @return:
   */

  function setEQSPExistence_TDIC() {
    var pattern1 = ClausePatternLookup.getCoveragePatternByCodeIdentifier("BOPEqSpBldgCov")
    if (!this.BOPEqBldgCovExists) {
      this.setCoverageExists(pattern1, false)
    }
    if (this.BOPBuildingCovExists && this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value != null) {
      if (this.BOPEqBldgCovExists && this.BOPEqSpBldgCovExists)
        this.BOPEqSpBldgCov.BOPEQSLInclLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value + this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value)
    } else if (this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value != null) {
      if (this.BOPEqBldgCovExists && this.BOPEqSpBldgCovExists)
        this.BOPEqSpBldgCov.BOPEQSLInclLimit_TDICTerm.setValue(this.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value)
    }
    else if (this.BOPBuildingCov.BOPBldgLimTerm.Value != null) {
      if (this.BOPEqBldgCovExists && this.BOPEqSpBldgCovExists)
        this.BOPEqSpBldgCov.BOPEQSLInclLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value)
    }
    //Set Territory Codes
    var terrCode = this.setEarthquakeTerritoryCode_TDIC()
    if (this.BOPEqSpBldgCovExists) {
      if (terrCode != null)
        this.BOPEqSpBldgCov.BOPEQSLTerritory_TDICTerm.setValueFromString(terrCode.toString())
    }
  }

  /**
   * create by: ChitraK
   *
   * @description: Method to set Earthquake Deductibles
   * @create time: 8:37 PM 7/26/2019
   * @return:
   */

  function setEarthquakeDeductibles_TDIC() {
    if (this.BOPEqBldgCovExists) {
      if ({"1", "2", "3", "4", "5", "6", "8", "9", "12", "13"}.contains(this.BOPEqBldgCov.BOPEQBldgClass_TDICTerm.getValueAsString())) {
        var opt = this.BOPEqBldgCov.BOPEQDeductible_TDICTerm.Pattern.getCovTermOpt("5")
        this.BOPEqBldgCov.BOPEQDeductible_TDICTerm.setOptionValue(opt)
      } else if ({"7", "10", "11", "14", "15"}.contains(this.BOPEqBldgCov.BOPEQBldgClass_TDICTerm.getValueAsString())) {
        var opt = this.BOPEqBldgCov.BOPEQDeductible_TDICTerm.Pattern.getCovTermOpt("10")
        this.BOPEqBldgCov.BOPEQDeductible_TDICTerm.setOptionValue(opt)
      }
    }
  }

  function setEarthquakeSLDeductibles_TDIC() {
    if (this.BOPEqSpBldgCovExists) {
      this.BOPEqSpBldgCov.BOPEQSLBldgClass_TDICTerm.setValueFromString(this.BOPEqBldgCov.BOPEQBldgClass_TDICTerm.getValueAsString())
      this.BOPEqSpBldgCov.BOPEQSLDeductible_TDICTerm.setValueFromString(this.BOPEqBldgCov.BOPEQDeductible_TDICTerm.ValueAsString)

    }
  }


  function setEQSLZone_TDIC() {
    if (this.BOPEqBldgCovExists && this.BOPEqSpBldgCovExists && this.BOPEqBldgCov.BOPEQZone_TDICTerm.Value != null) {
      this.BOPEqSpBldgCov.BOPEQSLZone_TDICTerm.setValueFromString(this.BOPEqBldgCov.BOPEQZone_TDICTerm.ValueAsString)
    }
  }
  /*
   * create by: ChitraK
   * @description: Method to set Territory Code
   * @create time: 11:32 AM 7/29/2019
   * @return: 
   */

  function setEarthquakeTerritoryCode_TDIC() : Integer {
    var zipCode : Integer
    var locZipCode = this.BOPLocation.PolicyLocation.PostalCode
    if (locZipCode.contains("-"))
      zipCode = locZipCode.split("-").first().toInt()
    else
      zipCode = locZipCode.toInt()

    var q = Query.make(RDZipTerritoryData_TDIC).compare(RDZipTerritoryData_TDIC#ZipCode, Equals, zipCode)
    var terrCode = q.select()
    if (terrCode.Count > 0)
      return terrCode.FirstResult.TerritoryCode
    return null
  }

  /**
   * create by: SureshB
   *
   * @param : ContactType
   * @description: method to add new contact of type PolicyMortgagee_TDIC
   * @create time: 12:38 PM 8/26/2019
   * @return: PolicyMortgagee_TDIC
   */
  function addNewPolicyMortgageeOfContactType_TDIC(contactType : ContactType) : PolicyMortgagee_TDIC {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return addPolicyMortgagee_TDIC(acctContact.Contact)
  }

  /**
   * create by: SureshB
   *
   * @param : ContactType
   * @description: method to add new contact of type PolicyMortgagee_TDIC
   * @create time: 12:40 PM 8/26/2019
   * @return: PolicyMortgagee_TDIC
   */
  function addPolicyMortgagee_TDIC(contact : Contact) : PolicyMortgagee_TDIC {
    if (this.BOPBldgMortgagees.firstWhere(\p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyMortgagee.Error.AlreadyExists_TDIC", contact))
    }
    var mortgagee = this.Branch.addNewPolicyContactRoleForContact(contact, TC_POLICYMORTGAGEE_TDIC) as PolicyMortgagee_TDIC
    this.addToBOPBldgMortgagees(mortgagee)
    return mortgagee
  }

  /**
   * create by: SureshB
   *
   * @param : ContactType
   * @description: method to add contact of type PolicyLossPayee_TDIC
   * @create time: 12:41 PM 8/26/2019
   * @return: PolicyLossPayee_TDIC
   */
  function addNewPolicyLossOfPayeeOfContactType_TDIC(contactType : ContactType) : PolicyLossPayee_TDIC {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return addPolicyLossPayee_TDIC(acctContact.Contact)
  }

  /**
   * create by: SureshB
   *
   * @param : ContactType
   * @description: method to add contact of type PolicyLossPayee_TDIC
   * @create time: 12:42 PM 8/26/2019
   * @return: PolicyLossPayee_TDIC
   */
  function addPolicyLossPayee_TDIC(contact : Contact) : PolicyLossPayee_TDIC {
    if (this.BOPBldgLossPayees.firstWhere(\p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyLossPayee.Error.AlreadyExists_TDIC", contact))
    }
    var lossPayee = this.Branch.addNewPolicyContactRoleForContact(contact, TC_POLICYLOSSPAYEE_TDIC) as PolicyLossPayee_TDIC
    this.addToBOPBldgLossPayees(lossPayee)
    return lossPayee
  }

  /**
   * create by: SureshB
   *
   * @param null
   * @description: property to get the unassigned PolicyMortgagee_TDIC contacts
   * @create time: 12:43 PM 8/26/2019
   * @return: AccountContact[]
   */
  property get UnassignedPolicyMortgagee_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYMORTGAGEE_TDIC)
    var assignedMortgagees = this.BOPBldgMortgagees.map(\mortgagee -> mortgagee.AccountContactRole.AccountContact)
    return this.Branch.Policy.Account
        .getAccountContactsWithRole(accountContactRoleType)
        .subtract(assignedMortgagees)
        .toTypedArray()
  }
  property get ExistingMortgagee_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYMORTGAGEE_TDIC)
    return this.Branch.Policy.Account.getAccountContactsWithRole(accountContactRoleType)
  }

  property get ExistingLossPayees_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYLOSSPAYEE_TDIC)
    return this.Branch.Policy.Account.getAccountContactsWithRole(accountContactRoleType)
  }

  /**
   * create by: SureshB
   *
   * @param null
   * @description: property to get unassigned PolicyLossPayee_TDIC contacts
   * @create time: 12:44 PM 8/26/2019
   * @return: AccountContact[]
   */
  property get UnassignedPolicyLossPayee_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYLOSSPAYEE_TDIC)
    var assignedLossPayees = this.BOPBldgLossPayees.map(\losspayee -> losspayee.AccountContactRole.AccountContact)
    return this.Branch.Policy.Account
        .getAccountContactsWithRole(accountContactRoleType)
        .subtract(assignedLossPayees)
        .toTypedArray()
  }

  /**
   * create by: SureshB
   *
   * @param null
   * @description: property to get contacts other than type PolicyMortgagee_TDIC
   * @create time: 12:45 PM 8/26/2019
   * @return: AccountContact[]
   */
  property get MortgageeOtherCandidates_TDIC() : AccountContact[] {
    return otherContactsFor_TDIC(TC_POLICYMORTGAGEE_TDIC)
  }

  /**
   * create by: SureshB
   *
   * @param null
   * @description: property to get contacts other than type PolicyLossPayee_TDIC
   * @create time: 12:47 PM 8/26/2019
   * @return: AccountContact[]
   */
  property get LossPayeeOtherCandidates_TDIC() : AccountContact[] {
    return otherContactsFor_TDIC(TC_POLICYLOSSPAYEE_TDIC)
  }

  /**
   * create by: SureshB
   *
   * @param : typekey.PolicyContactRole
   * @description: method to get the active contacts other than type of parsed contact role
   * @create time: 12:48 PM 8/26/2019
   * @return: AccountContact[]
   */
  private function otherContactsFor_TDIC(policyContactRole : typekey.PolicyContactRole) : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(policyContactRole)
    return this.Branch.Policy.Account.ActiveAccountContacts
        .where(\ac -> plugin.canBeRole(ac.ContactType, accountContactRoleType) and not ac.hasRole(accountContactRoleType))
  }

  /**
   * create by: ChitraK
   *
   * @description: To Set IL Limits
   * @create time: 5:50 PM 9/3/2019
   * @return:
   */

  function setILSubLimit_TDIC() {
    var limitValue : BigDecimal = 750000
    if (this.BOPILMineSubCov_TDICExists) {
      if (this.BOPBuildingCovExists && this.BOPBuildingCov.BOPBldgLimTerm.Value != null && this.BOPBuildingCov.BOPBldgLimTerm.Value < limitValue) {
        this.BOPILMineSubCov_TDIC.BOPILMineSubInclLimit_TDICTerm.setValue(this.BOPBuildingCov.BOPBldgLimTerm.Value)
      } else {
        this.BOPILMineSubCov_TDIC.BOPILMineSubInclLimit_TDICTerm.setValue(limitValue)
      }
    }
  }
  /*
   * create by: SureshB
   * @description: method to check if the building is 30 years old
   * @create time: 8:46 PM 9/6/2019
    * @param
   * @return: boolean
   */

  function isBuildingOver30YearsOld_TDIC() : boolean {
    if (this.Building?.YearBuilt != null) {
      return Date.CurrentDate.YearOfDate - this.Building.YearBuilt > 30
    }
    return false
  }

  /**
   * create by: ChitraK
   *
   * @description: set Existence for Building
   * @create time: 4:34 PM 11/4/2019
   * @return:
   */

  function setBuildingExistence_TDIC() : ExistenceType {
    if (this.PolicyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
      return ExistenceType.TC_ELECTABLE
    }
    return ExistenceType.TC_REQUIRED
  }

  /**
   * All the existing Active additional insureds on the Account
   * Includes all of them as multiple detail rows are allowed, so it is perfectly valid to include Contacts that are
   * already AdditionalInsureds on this PolicyLine
   */
  property get ExistingAdditionalInsureds_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYADDLINSURED)
    return this.Branch.Policy.Account.getAccountContactsWithRole(accountContactRoleType)
  }

  /**
   * Any Active account contact that is not an additional insured and can be an additional insured.
   */
  property get AdditionalInsuredOtherCandidates_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYADDLINSURED)
    return this.Branch.Policy.Account.ActiveAccountContacts
        .where(\acr -> plugin.canBeRole(acr.ContactType, accountContactRoleType) and not acr.hasRole(accountContactRoleType))
  }


  function addNewAdditionalInsuredDetailForContact_TDIC(contact : Contact) : PolicyAddlInsuredDetail {
    var policyAdditionalInsured = this.AdditionalInsureds.firstWhere(\ai -> ai.AccountContactRole.AccountContact.Contact == contact)
    if (policyAdditionalInsured == null) {
      policyAdditionalInsured = this.addNewAdditionalInsured_TDIC(contact)
    }
    var policyadditionalInsuredDetail = policyAdditionalInsured.addNewAdditionalInsuredDetail()
    return policyadditionalInsuredDetail
  }

  function addNewAdditionalInsuredDetailOfContactType_TDIC(contactType : ContactType) : PolicyAddlInsuredDetail {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    acctContact.addNewRole(TC_ADDITIONALINSURED)
    var policyAdditionalInsured = this.addNewAdditionalInsured_TDIC(acctContact.Contact)
    var policyAdditionalInsuredDetail = policyAdditionalInsured.addNewAdditionalInsuredDetail()
    return policyAdditionalInsuredDetail
  }

  /**
   * Add the supplied <code>acctContact</code> as a PolicyAddlInsured.  This method
   * has the side-effect if adding an AdditionalInsured role to the acctContact if it
   * doesn't already have one; or of adding the AccountContact if one does not exist
   * for the Contact on this Account already.
   */
  function addNewAdditionalInsured_TDIC(contact : Contact) : PolicyAddlInsured {
    if (this.AdditionalInsureds.firstWhere(\p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyAddlInsured.Error.AlreadyExists", contact))
    }
    var identityPropertyMap = new HashMap<IEntityPropertyInfo, Object>()
    identityPropertyMap.put((PolicyAddlInsured.Type as IEntityType).EntityProperties.toList().firstWhere(\i -> i.Name == "PolicyLine"), this.FixedId)
    var policyAdditionalInsured = this.Branch.addNewPolicyContactRoleForContactWithCheckProperties(contact, TC_POLICYADDLINSURED, identityPropertyMap) as PolicyAddlInsured
    this.addToAdditionalInsureds(policyAdditionalInsured)
    return policyAdditionalInsured
  }

  /**
   * create by: SanjanaS
   * @description: Create and add BOP Manuscript.
   * @create time: 9:00 AM 5/11/2020
   * @return: BOPManuscript_TDIC
   */

  function createAndAddBOPManuscript_TDIC() : BOPManuscript_TDIC {
    var item = new BOPManuscript_TDIC(this.Branch)
  //  item.BusinessOwnersLine = this
    item.ManuscriptEffectiveDate = this.Branch.EditEffectiveDate
    this.addToBOPManuscript(item)
    return item
  }

  /**
   * create by: SanjanaS
   * @description: Remove BOP Manuscript.
   * @create time: 11:30 PM 5/22/2020
   * @return:
   */
  function toRemoveFromBOPManuscript_TDIC(scheduledItem:BOPManuscript_TDIC ){
    if(scheduledItem.BasedOn == null){
      this.removeFromBOPManuscript(scheduledItem)
    }
    else{
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.GLAdditionalCoverages.Error"))
    }
  }

  /**
   * create by: SanjanaS
   * @description: Remove Loss Payee.
   * @create time: 11:30 PM 5/22/2020
   * @return:
   */
  function toRemoveFromBOPBldgLossPayees(lossPayeeDetail:PolicyLossPayee_TDIC ){
    if(lossPayeeDetail.BasedOn == null || lossPayeeDetail.Branch.Job typeis Renewal){
      this.removeFromBOPBldgLossPayees(lossPayeeDetail)
    }
    else{
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.BOPCoverages.Error", lossPayeeDetail))
    }
  }

  /**
   * create by: SanjanaS
   * @description: Remove Mortgagee.
   * @create time: 11:30 PM 5/22/2020
   * @return:
   */
  function toRemoveFromBOPBldgMortgagees(mortgageeDetail:PolicyMortgagee_TDIC ){
    if(mortgageeDetail.BasedOn == null || mortgageeDetail.Branch.Job typeis Renewal){
      this.removeFromBOPBldgMortgagees(mortgageeDetail)
    }
    else{
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.BOPCoverages.Error", mortgageeDetail))
    }
  }
}
