package gw.lob.bop.rating
uses java.util.Date

uses entity.windowed.BusinessOwnersLineVersionList
uses gw.api.effdate.EffDatedUtil
uses gw.financials.PolicyPeriodFXRateCache
uses java.util.List

@Export
class BOPMinPremiumCostData extends BOPCostData<BOPMinPremiumCost> {
  var _locationID : String
  var _costType : BOPCostType_TDIC
  construct(effDate : Date, expDate : Date, stateArg : Jurisdiction) {
    super(effDate, expDate, stateArg)
  }

  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction) {
    super(effDate, expDate, c, rateCache, stateArg)
  }

  construct(c : BOPMinPremiumCost, rateCache : PolicyPeriodFXRateCache) {
    super(c, rateCache)
  }

  construct (effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, stateArg : Jurisdiction, costType : BOPCostType_TDIC){
    super(effDate, expDate, c, rateCache, stateArg)
   _costType = costType
   }

  override function getVersionedCosts(line : BOPLine) : List<gw.pl.persistence.core.effdate.EffDatedVersionList> {
    var covVL = EffDatedUtil.createVersionList( line.Branch, line.FixedId ) as BusinessOwnersLineVersionList
    var costs = covVL.BOPCosts.where(\costVL -> costVL typeis BOPMinPremiumCost and matchesStep(costVL))
    return costs.toList()
  }
  
  protected override property get KeyValues() : List<Object> {
    return {_costType.Code}
  }

  override function setSpecificFieldsOnCost( line: BOPLine, cost: BOPMinPremiumCost ) {
    super.setSpecificFieldsOnCost( line, cost )
    cost.BOPCostType = _costType
  }


  private function matchesStep(cost : BOPMinPremiumCost) : boolean {
    return cost.BOPCostType == _costType
  }
}
