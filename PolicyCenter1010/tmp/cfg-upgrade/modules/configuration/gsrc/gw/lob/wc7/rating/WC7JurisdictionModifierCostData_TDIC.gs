package gw.lob.wc7.rating

uses java.util.Date
uses gw.api.effdate.EffDatedUtil
uses entity.windowed.WC7ModifierVersionList
uses java.util.List
uses gw.pl.persistence.core.Key

class WC7JurisdictionModifierCostData_TDIC extends WC7CostData<WC7ModifierCost> {

  private var _jurisdictionID : Key
  private var _costType : WC7JurisdictionCostType as readonly CostType
  private var _modifier : WC7Modifier

  construct(effDate : Date, expDate : Date, modifier : WC7Modifier, costTypeArg : WC7JurisdictionCostType, order : int) {
    super(effDate, expDate)
    _jurisdictionID = modifier.WC7Jurisdiction.FixedId
    _costType = costTypeArg
    _modifier = modifier
    _costType = costTypeArg
    CalcOrder = order
  }

  construct(c : WC7ModifierCost) {
    super(c)
    _costType = c.JurisdictionCostType
    _modifier = c.WC7Modifier

  }

  override function setSpecificFieldsOnCost(line : WC7WorkersCompLine, cost : WC7ModifierCost) {
    super.setSpecificFieldsOnCost( line, cost )
    cost.setFieldValue("WC7Jurisdiction", _jurisdictionID)
    cost.JurisdictionCostType = CostType
    cost.setFieldValue("WC7Modifier", _modifier.FixedId)
    cost.StatCode = StatCode
  }

  override property get KeyValues() : List<Object> {
    return {_costType, _jurisdictionID, _modifier.FixedId}
  }

  override function getVersionedCosts(line : WC7WorkersCompLine) : List<gw.pl.persistence.core.effdate.EffDatedVersionList> {
    var modifierVL = EffDatedUtil.createVersionList( line.Branch, _modifier.FixedId ) as WC7ModifierVersionList
    return modifierVL.Costs.where( \ costVL -> matchesStep(costVL.AllVersions.first())).toList()
  }

  private function matchesStep(cost : WC7ModifierCost) : boolean {
    return cost.JurisdictionCostType == _costType
  }

  override property get Jurisdiction() : Jurisdiction {
    return _modifier.WC7Jurisdiction.Jurisdiction
  }
}
