package gw.lob.wc7

uses gw.api.logicalmatch.AbstractEffDatedPropertiesMatcher
uses gw.entity.IEntityPropertyInfo

class WC7GenericEffDatedPropertiesMatcher_TDIC<T extends EffDated>  extends AbstractEffDatedPropertiesMatcher<T>  {
  construct(owner : T) {
    super(owner)
  }
  protected override property get IdentityColumns() : Iterable<IEntityPropertyInfo> {
    return null
  }
}