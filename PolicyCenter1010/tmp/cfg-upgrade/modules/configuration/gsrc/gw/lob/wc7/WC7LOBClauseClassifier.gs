package gw.lob.wc7

uses gw.api.productmodel.ClausePattern
uses java.util.Set

class WC7LOBClauseClassifier implements WC7ClauseClassifier {

  override function isSimpleContactSchedule(pattern : ClausePattern) : boolean {
    return SimpleContactScheduleClauses.contains(pattern)
  }

  override property get SimpleContactScheduleClauses() : Set<ClausePattern> {
    return {}
  }
}