package gw.lob.gl

uses gw.contact.AbstractPolicyContactRoleAccountSyncableImpl
uses gw.account.AccountContactRoleToPolicyContactRoleSyncedField
uses com.google.common.collect.ImmutableSet
uses java.util.Set

class TDIC_GLPolicyOwnerOfficeAccountSyncableImpl extends AbstractPolicyContactRoleAccountSyncableImpl<GLPolicyOwnerOfficer_TDIC> {

  static final var ACCOUNT_SYNCED_FIELDS = ImmutableSet.copyOf  (
      AbstractPolicyContactRoleAccountSyncableImpl.AccountSyncedFieldsInternal.union(
          {
              AccountContactRoleToPolicyContactRoleSyncedField.WC7RelationshipTitle
          }
      )
  )

  protected static property get AccountSyncedFieldsInternal() : Set<gw.api.domain.account.AccountSyncedField<gw.api.domain.account.AccountSyncable, java.lang.Object>> {
    // provided so subclasses can extend this list
    return ACCOUNT_SYNCED_FIELDS
  }

  construct(accountSyncable : GLPolicyOwnerOfficer_TDIC) {
    super(accountSyncable)
  }

  override property get AccountSyncedFields() : Set<gw.api.domain.account.AccountSyncedField<gw.api.domain.account.AccountSyncable, java.lang.Object>> {
    // must override to ensure that we call the correct static AccountSyncedFieldsInternal property
    return AccountSyncedFieldsInternal
  }
}