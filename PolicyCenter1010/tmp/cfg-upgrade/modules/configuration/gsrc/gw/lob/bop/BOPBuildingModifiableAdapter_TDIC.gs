package gw.lob.bop

uses gw.api.domain.ModifiableAdapter

class BOPBuildingModifiableAdapter_TDIC implements ModifiableAdapter {
  var _owner : BOPBuilding

  construct(owner : BOPBuilding) {
    _owner = owner
  }

  override property get PolicyLine() : PolicyLine {
    return _owner.PolicyLine
  }

  override property get PolicyPeriod() : PolicyPeriod {
    return PolicyLine.Branch
  }

  override property get State() : Jurisdiction {
    return gw.api.util.JurisdictionMappingUtil.getJurisdiction(_owner.BOPLocation.Location)
  }

  override property get AllModifiers() : Modifier[] {
    return _owner.BOPBuildingModifiers_TDIC
  }

  override function addToModifiers(element : Modifier) {
    _owner.addToBOPBuildingModifiers_TDIC(element as BOPBuildingModifier_TDIC)
  }

  override function removeFromModifiers(element : Modifier) {
    _owner.removeFromBOPBuildingModifiers_TDIC(element as BOPBuildingModifier_TDIC)
  }

  override function createRawModifier() : Modifier {
    return new BOPBuildingModifier_TDIC(_owner.Branch)
  }

  override function postUpdateModifiers() {
  }

  override property get ReferenceDateInternal() : Date {
    return _owner.ReferenceDateInternal
  }

  override property set ReferenceDateInternal(date : Date) {
    _owner.ReferenceDateInternal = date
  }
}
