package gw.lob.bop

uses entity.AddlInterestDetail
uses gw.api.locale.DisplayKey
uses gw.api.util.JurisdictionMappingUtil
uses gw.lob.AbstractBuildingValidation
uses gw.util.GosuStringUtil
uses gw.validation.PCValidationContext
uses gw.validation.ValidationUtil

/**
 * Defines the Business Owners Policy line building validation utility class.
 * <p>
 * Used to validate a BOP line building.
 */
@Export
class BOPBuildingValidation extends AbstractBuildingValidation<BOPClassCodeSearchCriteria> {

  var _building : BOPBuilding

  construct(valContext : PCValidationContext, bldg : BOPBuilding, validatedClassCodeSearchCriteria : Set<BOPClassCodeSearchCriteria>) {
    super(valContext, validatedClassCodeSearchCriteria)
    _building = bldg
  }

  /**
   * Validate the given building.
   *
   * @see #validate()
   */
  static function validateBuilding(bldg : BOPBuilding) {
    var context = ValidationUtil.createContext(TC_DEFAULT)
    new BOPBuildingValidation(context, bldg, {}).validate()
    context.raiseExceptionIfProblemsFound()
  }

  /**
   * create by: ChitraK
   *
   * @description: please add description
   * @create time: 9:43 PM 7/24/2019
   * @return:
   */

  static function validateBuildingRules_TDIC(bld : BOPBuilding, quoteType : QuoteType) {

    PCValidationContext.doPageLevelValidation(\context -> new BOPBuildingValidation(context, bld, {}).validateRateModifiers_TDIC())
    if ({"HI", "NJ", "PA"}.contains(bld.BOPLocation.PolicyLocation.State))
      PCValidationContext.doPageLevelValidation(\context -> new BOPBuildingValidation(context, bld, {}).validateEQEQSLCovTerms_TDIC())
    PCValidationContext.doPageLevelValidation(\context -> new BOPBuildingValidation(context, bld, {}).validateAdditionalInsuredTypeAndDesc_TDIC())
    PCValidationContext.doPageLevelValidation(\context -> new BOPBuildingValidation(context, bld, {}).validateBuildingsqft_TDIC(quoteType))
  }

  /**
   * Validate the current building.
   * <p>
   * Checks the following:
   * <ul>
   * <li>At least base coverage exists for this building (BOPBuildingCov or BOPPersonalPropCov)</li>
   * <li>For BOPOrdinanceCov coverage.
   * <ul>
   * <li>Ensure the limits are set properly</li>
   * <li>BOPOrdinanceCov ensure that all limits are empty if BOPOrdLawIncomeExpenseTerm is set</li>
   * </ul>
   * </li>
   * <li>BOPOrdinanceCov and BOPBuildingCov are not both selected</li>
   * <li>The Building was built between teh minimum and maxiumim policy creation year</li>
   * <li>Building improvement dates are between the building's built date and today</li>
   * <li>Check that the building class code is valid.</li>
   * <li>Additional interest details are unique</li>
   * </ul>
   */
  override function validateImpl() {
    Context.addToVisited(this, "validateImpl")
    //checkAtLeastOneBaseCov()
    checkOrdLawCoverageLimits()
    checkIncomeExpenseTerm()
    checkOrdLawCovRequiresBuildingCov()
    checkYearBuiltMakesSense()
    checkBuildingImprovementDatesMakeSense()
    checkClassCodeIsValid()
    addlInterestDetailUnique()
    //validateDEBLDMWLDCovExistance_TDIC()
  }

  /**
   *
   */
  private function checkAtLeastOneBaseCov() {
    Context.addToVisited(this, "checkAtLeastOneBaseCov")
    if (not(_building.BOPBuildingCovExists or _building.BOPPersonalPropCovExists)) {
      Result.addError(_building, TC_DEFAULT,
          DisplayKey.get("Web.Policy.BOP.Validation.BuildingOrBusinessPersonalPropertyCovRequired", _building.DisplayName, _building.BOPLocation.DisplayName))
    }
  }

  /**
   * Ensure the limits are set properly for BOPOrdinanceCov
   */
  private function checkOrdLawCoverageLimits() {
    Context.addToVisited(this, "checkOrdLawCoverageLimits")
    if (_building.BOPOrdinanceCovExists) {
      var errortext = ""
      var cov = _building.BOPOrdinanceCov
      if (cov.BOPOrdLawCov23LimTerm.Value > 0) {
        if (cov.BOPOrdLawCov2LimTerm.Value > 0) {
          errortext = DisplayKey.get("Web.Policy.BOP.Validation.Coverage2Limit")
        }
        if (cov.BOPOrdLawCov3LimTerm.Value > 0) {
          if (errortext != "") {
            errortext = errortext + DisplayKey.get("Web.Policy.BOP.Validation.AndCoverage3Limit")
          } else {
            errortext = DisplayKey.get("Web.Policy.BOP.Validation.Coverage3Limit")
          }
        }
      }
      if (errortext != "") {
        Result.addError(cov, TC_DEFAULT,
            DisplayKey.get("Web.Policy.BOP.Validation.Combined23Limit", _building.DisplayName, _building.BOPLocation.DisplayName, errortext))
      }
    }
  }

  /**
   * BOPOrdinanceCov ensure that all limits are empty if BOPOrdLawIncomeExpenseTerm is set
   */
  private function checkIncomeExpenseTerm() {
    Context.addToVisited(this, "checkIncomeExpenseTerm")
    if (_building.BOPOrdinanceCovExists) {
      var cov = _building.BOPOrdinanceCov
      if (cov.BOPOrdLawIncomeExpenseTerm.Value and allCoveragesEmpty(cov)) {
        Result.addError(cov, TC_DEFAULT,
            DisplayKey.get("Web.Policy.BOP.Validation.IncomeExpenseTerm", _building.DisplayName, _building.BOPLocation.DisplayName))
      }
    }
  }

  /**
   * BOPOrdinanceCov and BOPBuildingCov are not both selected
   */
  private function checkOrdLawCovRequiresBuildingCov() {
    Context.addToVisited(this, "checkOrdLawCovRequiresBuildingCov")
    if (_building.BOPOrdinanceCovExists and !_building.BOPBuildingCovExists) {
      Result.addError(_building, TC_DEFAULT,
          DisplayKey.get("Web.Policy.BOP.Validation.OrdLawRequiresBuilding", _building.DisplayName, _building.BOPLocation.DisplayName))
    }
  }

  /**
   * The Building was built between teh minimum and maxiumim policy creation year
   */
  private function checkYearBuiltMakesSense() {
    Context.addToVisited(this, "checkYearBuiltMakesSense")
    var yb = _building.Building.YearBuilt
    var min = ValidationUtil.getMinPolicyCreationYear()
    var max = ValidationUtil.getMaxPolicyCreationYear()
    if (yb < min or yb > max) {
      Result.addError(_building, TC_DEFAULT,
          DisplayKey.get("Web.Policy.BOP.Validation.YearBuildingBuilt", DisplayKey.get("Web.Policy.BOP.Building.YearBuilt"), min, max))
    }
  }

  /**
   * Building improvement dates are between the building's built date and today
   */
  private function checkBuildingImprovementDatesMakeSense() {
    Context.addToVisited(this, "checkBuildingImprovementDatesMakeSense")
    checkBuildingImprovementDate(_building.Building.Heating.YearAdded,
        DisplayKey.get("Web.Policy.LocationContainer.Location.Building.LastUpdateHeating"))
    checkBuildingImprovementDate(_building.Building.Plumbing.YearAdded,
        DisplayKey.get("Web.Policy.LocationContainer.Location.Building.LastUpdatePlumbing"))
    checkBuildingImprovementDate(_building.Building.Roofing.YearAdded,
        DisplayKey.get("Web.Policy.LocationContainer.Location.Building.LastUpdateRoofing"))
    checkBuildingImprovementDate(_building.Building.Wiring.YearAdded,
        DisplayKey.get("Web.Policy.LocationContainer.Location.Building.LastUpdateWiring"))
  }

  private function checkBuildingImprovementDate(elementYear : Integer, elementType : String) {
    if (elementYear < _building.Building.YearBuilt) {
      Result.addError(_building, TC_DEFAULT,
          DisplayKey.get("Web.Policy.BOP.Validation.BuildingImprovementYearPredatesYearBuilt", elementType,
              DisplayKey.get("Web.Policy.BOP.Building.YearBuilt")))
    } else if (elementYear > Date.Today.YearOfDate) {
      Result.addError(_building, TC_DEFAULT,
          DisplayKey.get("Web.Policy.BOP.Validation.BuildingDateInFuture", elementType))
    }
  }

  private function allCoveragesEmpty(cov : BOPOrdinanceCov) : boolean {
    return cov.BOPOrdLawCov23LimTerm.Value == null and cov.BOPOrdLawCov2LimTerm.Value == null and cov.BOPOrdLawCov3LimTerm.Value == null
  }

  /**
   * Add an error if Additional Interest details are not unique for current building.
   * {@link AddlInterestDetail} details must be unique for the following properties:
   * <ul>
   * <li>{@link entity.AddlInterestDetail#PolicyAddlInterest PolicyAddlInterest}</li>
   * <li>{@link entity.AddlInterestDetail#AdditionalInterestType AdditionalInterestType}</li>
   * <li>{@link entity.AddlInterestDetail#ContractNumber ContractNumber}</li>
   * </ul>
   */
  private function addlInterestDetailUnique() {
    Context.addToVisited(this, "addlInterestDetailUnique")
    var thisSet = new HashSet<AddlInterestDetail>(_building.AdditionalInterestDetails.toList())
    for (detail in _building.AdditionalInterestDetails) {
      var oldCount = thisSet.Count
      thisSet.removeWhere(\o -> o.PolicyAddlInterest == detail.PolicyAddlInterest and
          o.AdditionalInterestType == detail.AdditionalInterestType and
          o.ContractNumber == detail.ContractNumber)
      if (thisSet.Count < oldCount - 1) {
        Result.addError(_building, TC_DEFAULT, DisplayKey.get("EntityName.PolicyLine.Validation.AddlInterestDetailUnique", detail.DisplayName), "PersonalVehicles")
        if (!thisSet.HasElements) {
          return
        }
      }
    }
  }

  /*****************************************************************************
   *
   * Properties and methods for building ClassCode validation.
   *
   ****************************************************************************/
  override protected property get ClassCodeCode() : String {
    return _building.ClassCode.Code
  }

  override protected property get ReferenceDate() : Date {
    var bopLocation = _building.BOPLocation

    var bopLine = bopLocation.BOPLine
    return bopLine.getReferenceDateForCurrentJob(JurisdictionMappingUtil.getJurisdiction(bopLocation.Location))
  }

  override protected property get PreviousSelectedClassCode() : BOPClassCode {
    var bopLine = _building.BOPLocation.BOPLine
    return (bopLine.Branch.Job.NewTerm)
        ? null : _building.BasedOn.ClassCode
  }

  override protected function createClassCodeSearchCritieria() : BOPClassCodeSearchCriteria {
    return new BOPClassCodeSearchCriteria()
  }

  override protected function addClassCodeError() : void {
    Result.addError(_building, TC_DEFAULT,
        DisplayKey.get("Web.Policy.BOP.Validation.UnavailableClassCode",
            _building.BOPLocation.Location.LocationNum,
            _building.Building.BuildingNum, ClassCodeCode))
  }

  /**
   * create by: ChitraK
   *
   * @description: validate EQ Limits
   * @create time: 8:29 PM 7/25/2019
   * @return:
   */

  private function validateEQEQSLCovTerms_TDIC() {
    var chkValues : Boolean = null
    if (_building.BOPEqBldgCovExists && _building.BOPEqSpBldgCovExists)
      chkValues = (_building.BOPEqSpBldgCov.BOPEQSLBldgClass_TDICTerm.Value == _building.BOPEqBldgCov.BOPEQBldgClass_TDICTerm.Value) &&
          (_building.BOPEqSpBldgCov.BOPEQSLTerritory_TDICTerm.Value == _building.BOPEqBldgCov.BOPEQTerritory_TDICTerm.Value) &&
          (_building.BOPEqSpBldgCov.BOPEQSLZone_TDICTerm.Value == _building.BOPEqBldgCov.BOPEQZone_TDICTerm.Value)
    if (chkValues == false)
      Context.Result.addError(_building, ValidationLevel.TC_DEFAULT, DisplayKey.get("BOP.Building.Validation.Error3_TDIC"))

  }

  /**
   * create by: ChitraK
   *
   * @description: Method to validate Modifiers Sum
   * @create time: 5:42 PM 8/20/2019
   * @return:
   */

  private function validateRateModifiers_TDIC() {
    var mod = _building.BOPBuildingModifiers_TDIC
    var SRRates = mod.where(\modi -> modi.ScheduleRate) as Modifier[]

    if (SRRates.where(\elt -> elt.RateFactors.sum(\elt1 -> elt1.AssessmentWithinLimits) > elt.Maximum or
        elt.RateFactors.sum(\elt1 -> elt1.AssessmentWithinLimits) < elt.Minimum).Count > 0)
      Context.Result.addError(_building, ValidationLevel.TC_DEFAULT, DisplayKey.get("BOP.Building.Validation.Error4_TDIC"))
  }
  /*
   * create by: SureshB
   * @description: method to validate Additional Insured Type and Description
   * @create time: 6:33 PM 10/10/2019
    * @param null
   * @return:
   */

  private function validateAdditionalInsuredTypeAndDesc_TDIC() {
    var warningRequired = false
    warningRequired = _building.PolicyLine.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.hasMatch(\addlInsuredDetail ->
        addlInsuredDetail.AdditionalInsuredType == AdditionalInsuredType.TC_OTHER and GosuStringUtil.isBlank(addlInsuredDetail.Desciption))
    if (warningRequired) {
      Context.Result.addWarning(_building, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.LocationContainer.Location.Building.AdditionalInsured.HasOtherTypeAndBlankDesc"))
    }
  }

  /*
   * create by: SureshB
   * @description: method to add/remove Dental Employee Benefits Liability/Dental Medical Waste Legal Defense coverages based on existance
   * of Dental General Liability and bound PL Policy on the same account
   * @create time: 4:05 PM 11/4/2019
    * @param null
   * @return:
   */

  /*GWPS-1865 Add/remove Dental Employee Benefits Liability/Dental Medical Waste Legal Defense coverages based on existance
   * of Dental General Liability*/
  private function validateDEBLDMWLDCovExistance_TDIC() {
    if (Level == ValidationLevel.TC_QUOTABLE and _building.BOPDentalGenLiabilityCov_TDICExists and _building.PolicyPeriod.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
    //  if (!_building.PolicyPeriod.Policy.Account.IssuedPolicies.hasMatch(\issuedPolicy -> issuedPolicy.Product.CodeIdentifier == "GeneralLiability")) {
        if (!_building.BOPDEBLCov_TDICExists and !_building.BOPDMWLDCov_TDICExists) {
          _building.setCoverageExists(_building.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("BOPDEBLCov_TDIC"), true)
          _building.setCoverageExists(_building.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("BOPDMWLDCov_TDIC"), true)
          Result.addWarning(_building, Level, DisplayKey.get("TDIC.BOP.Building.Validation.DEBLDMWLDCovsAdded"))
     //   }
      } else {
        if (_building.BOPDEBLCov_TDICExists and _building.BasedOn?.BOPDEBLCov_TDICExists and _building.BOPDMWLDCov_TDICExists and
            _building.BasedOn?.BOPDMWLDCov_TDICExists) {
          _building.setCoverageExists(_building.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("BOPDEBLCov_TDIC"), false)
          _building.setCoverageExists(_building.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("BOPDMWLDCov_TDIC"), false)
          Result.addWarning(_building, Level, DisplayKey.get("TDIC.BOP.Building.Validation.DEBLDMWLDCovsRemoved"))
        }
      }
    }
  }

  /**
   * create by: ChitraK
   *
   * @description: validate building sqft for quick quote
   * @create time: 8:12 PM 11/6/2019
   * @return:
   */

  private function validateBuildingsqft_TDIC(quoteType : QuoteType) {
    if (quoteType == QuoteType.TC_QUICK && _building.BOPBuildingCovExists and _building.Building.TotalArea == null) {
      Context.Result.addError(_building, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.BOPBuilding.Validation.MandatryError"))
    }
  }
}
