package gw.lob.bop

uses entity.BOPCost
uses entity.BOPLocationCov
uses entity.Building
uses entity.BusinessOwnersCov
uses entity.PolicyLine
uses entity.windowed.BOPBuildingVersionList
uses entity.windowed.BOPCostVersionList
uses gw.api.domain.LineSpecificBuilding
uses gw.api.locale.DisplayKey
uses gw.api.policy.AbstractPolicyLineMethodsImpl
uses gw.api.tree.RowTreeRootNode
uses gw.api.util.JurisdictionMappingUtil
uses gw.lob.bop.rating.BOPSysTableRatingEngine
uses gw.lob.common.AbstractPolicyInfoValidation
uses gw.lob.common.AsyncQuoteCoverableThresholdByLine
uses gw.pl.persistence.core.Key
uses gw.plugin.diff.impl.BOPDiffHelper
uses gw.policy.PolicyLineValidation
uses gw.rating.AbstractRatingEngine
uses gw.rating.worksheet.treenode.WorksheetTreeNodeContainer
uses gw.rating.worksheet.treenode.WorksheetTreeNodeUtil
uses gw.validation.PCValidationContext
uses tdic.pc.config.rating.bop.BOPRatingEngine
uses java.math.BigDecimal
uses java.math.RoundingMode

/**
 * An implementation of PolicyLineMethods for {@link entity.BusinessOwnersLine BusinessOwnersLine}
 */
@Export
class BOPPolicyLineMethods extends AbstractPolicyLineMethodsImpl {

  var _line : entity.BusinessOwnersLine

  construct(line : entity.BusinessOwnersLine) {
    super(line)
    _line = line
  }

  override property get CoveredStates() : Jurisdiction[] {
    var covStates = new HashSet<Jurisdiction>()
    if (_line.BaseState != null) {
      covStates.add(_line.BaseState)
    }
    for (loc in _line.BOPLocations) {
      covStates.add(JurisdictionMappingUtil.getJurisdiction(loc.Location))
    }
    return covStates?.toTypedArray()
  }

  override property get AllCoverages() : Coverage[] {
    var coverages = new ArrayList<Coverage>()
    coverages.addAll(_line.BOPLineCoverages.toList())
    coverages.addAll(_line.BOPLocations*.Coverages.toList())
    //noinspection MultipleAsteriskDotOperators
    coverages.addAll(_line.BOPLocations*.Buildings*.Coverages.toList())
    return coverages?.toTypedArray()
  }

  override property get AllExclusions() : entity.Exclusion[] {
    return _line.BOPLineExclusions
  }

  override property get AllConditions() : entity.PolicyCondition[] {
    return _line.BOPLineConditions
  }

  override property get AllCoverables() : entity.Coverable[] {
    var list : ArrayList<Coverable> = {_line}
    _line.BOPLocations.each(\location -> list.add(location))
    _line.BOPLocations.flatMap(\location -> location.Buildings.toList()).each(\building -> list.add(building))
    return list.toTypedArray()
  }

  override property get AllModifiables() : Modifiable[] {
    var list : ArrayList<Modifiable> = {_line.BOPLocations*.Buildings.first()}
    return list.toTypedArray()
  }

  override function initialize() {
    _line.createCoveragesConditionsAndExclusions()
    _line.syncModifiers()
    _line.initializeEquipmentAutoNumberSequence(_line.Bundle)
  }

  override function cloneAutoNumberSequences() {
    _line.cloneEquipmentAutoNumberSequence()
    for (var location in _line.BOPLocations) {
      location.Location.cloneBuildingAutoNumberSequence()
    }
  }

  override function resetAutoNumberSequences() {
    _line.resetEquipmentAutoNumberSequence()
    for (var location in _line.BOPLocations) {
      location.Location.resetBuildingAutoNumberSequence()
    }
  }

  override function bindAutoNumberSequences() {
    _line.bindEquipmentAutoNumberSequence()
    for (var location in _line.BOPLocations) {
      location.Location.bindBuildingAutoNumberSequence()
    }
  }

  override function renumberAutoNumberSequences() {
    _line.renumberNewScheduledEquipments()
    for (var location in _line.BOPLocations) {
      if (not location.New) { // if this is a new location, don't need to renumber
        location.Location.renumberBuildingAutoNumberSequence()
      }
    }
  }

  /**
   * All BOPCosts across the term, in window mode.
   */
  override property get CostVLs() : Iterable<BOPCostVersionList> {
    return _line.VersionList.BOPCosts
  }

  override property get Transactions() : Set<Transaction> {
    return _line.BOPTransactions.toSet()
  }

  override function canSafelyDeleteLocation(location : PolicyLocation) : String {
    var allBOPLocationsEverOnLocation = getAllBOPLocationsEverForLocation(location)
    var currentOrFutureBOPLocationsMap = allBOPLocationsEverOnLocation
        .where(\bopLoc -> bopLoc.ExpirationDate > location.SliceDate)
        .partition(\bopLoc -> bopLoc.EffectiveDate <= location.SliceDate ? "current" : "future")
    if (not(currentOrFutureBOPLocationsMap["current"] == null or currentOrFutureBOPLocationsMap["current"].Empty)) {
      return DisplayKey.get("BusinessOwners.Location.CannotDelete.HasBOPLocation", location)
    } else if (not(currentOrFutureBOPLocationsMap["future"] == null or currentOrFutureBOPLocationsMap["future"].Empty)) {
      var futureDatesStr = currentOrFutureBOPLocationsMap["future"].map(\bVeh -> bVeh.EffectiveDate).order().join(", ")
      return DisplayKey.get("BusinessOwners.Location.CannotDelete.HasFutureBOPLocation", location, futureDatesStr)
    }
    return super.canSafelyDeleteLocation(location)
  }

  /**
   * Jeff Lin, 12/13/2019 5:35 PM PST, for CP/BOP line to add to increase Bldg Limits (applicable) and BPP Limits by the Inflation Guard percentage during the Auto/Manual renewal
   * GPC-1217/613: On a renewal transaction, automatically increase the Building limit (if applicable) and BPP limits (BOP only) by the Inflation Guard percentage
   */
  override function onRenewalInitialize() {
    super.onRenewalInitialize()
    increaseBOPBuildingAndBPPLimitsByInflationGuard_TDIC()
    removeExpiredBOPBuildingItems(_line.Branch)
  }

  private function increaseBOPBuildingAndBPPLimitsByInflationGuard_TDIC() {
    if (not(_line typeis BOPLine and _line.Branch.Job.Subtype == typekey.Job.TC_RENEWAL)) {
      return
    }

    (_line as BOPLine).BOPLocations.each(\loc -> {
      loc.Buildings.each(\bldg -> {
        // if building Cov exists, increase the building Coverage limits by Inflation Guard percentage
        if (bldg.BOPBuildingCovExists and bldg.BOPBuildingCov.HasBOPBldgLimTerm and bldg.BOPBuildingCov.BOPBldgLimTerm.Value > 0 and
            bldg.BOPBuildingCov.HasBOPBldgAnnualIncreaseTerm and bldg.BOPBuildingCov.BOPBldgAnnualIncreaseTerm.Value > 0) {
          bldg.BOPBuildingCov.BOPBldgLimTerm.Value = (bldg.BasedOn?.BOPBuildingCov.BOPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPBuildingCov.BOPBldgAnnualIncreaseTerm.Value / 100))).setScale(0, RoundingMode.HALF_UP)
          bldg.setBuildingDebrisLimit_TDIC()
        }
        // if Business Personal Property Coverage (BPP) exists, increase the BPP Coverage limits by Inflation Guard percentage
        if (bldg.BOPPersonalPropCovExists and bldg.BOPPersonalPropCov.HasBOPBPPBldgLimTerm and bldg.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value > 0 and
            bldg.BOPPersonalPropCov.HasBOPInflationGuard_TDICTerm and bldg.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Value > 0) {
          bldg.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value = (bldg.BasedOn?.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Value / 100))).setScale(0, RoundingMode.HALF_UP)
          bldg.setBPPDebrisLimit_TDIC()
        }
        //EQ Incrmemtal coverage limits
        if (bldg.BOPEqBldgCovExists and bldg.BOPBuildingCovExists and bldg.BOPPersonalPropCovExists  )
        {
          var increamentedBOPPersonalPropCovLimit = (bldg.BasedOn?.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Value / 100))).setScale(0,RoundingMode.HALF_UP)
          var increamentedBOPBuildingCovLimit = (bldg.BasedOn?.BOPBuildingCov.BOPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPBuildingCov.BOPBldgAnnualIncreaseTerm.Value / 100))).setScale(0, RoundingMode.HALF_UP)

          bldg.BOPEqBldgCov.BOPEQInclLimit_TDICTerm.Value = (increamentedBOPPersonalPropCovLimit + increamentedBOPBuildingCovLimit)
        }
        else if (bldg.BOPEqBldgCovExists and bldg.BOPPersonalPropCovExists and bldg.BOPPersonalPropCov.HasBOPBPPBldgLimTerm and bldg.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value > 0 and

            bldg.BOPPersonalPropCov.HasBOPInflationGuard_TDICTerm and bldg.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Value > 0)
        {
          var EQLIMIT=(bldg.BasedOn?.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Value / 100))).setScale(0,RoundingMode.HALF_UP)
          bldg.BOPEqBldgCov.BOPEQInclLimit_TDICTerm.Value = EQLIMIT
        }
        else if (bldg.BOPEqBldgCovExists and bldg.BOPBuildingCovExists and bldg.BOPBuildingCov.HasBOPBldgLimTerm and bldg.BOPBuildingCov.BOPBldgLimTerm.Value > 0 and
            bldg.BOPBuildingCov.HasBOPBldgAnnualIncreaseTerm and bldg.BOPBuildingCov.BOPBldgAnnualIncreaseTerm.Value > 0) {
          var EQLIMIT_BU=(bldg.BasedOn?.BOPBuildingCov.BOPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPBuildingCov.BOPBldgAnnualIncreaseTerm.Value / 100))).setScale(0, RoundingMode.HALF_UP)
          bldg.BOPEqBldgCov.BOPEQInclLimit_TDICTerm.Value = EQLIMIT_BU
        }
       //EQSL Incrmemtal coverage limits
        if (bldg.BOPEqBldgCovExists and bldg.BOPEqSpBldgCovExists and bldg.BOPBuildingCovExists and bldg.BOPPersonalPropCovExists  )
        {
          var increamentedBPPCovLimit = (bldg.BasedOn?.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Value / 100))).setScale(0,RoundingMode.HALF_UP)
          var increamentedBOPBuCovLimit = (bldg.BasedOn?.BOPBuildingCov.BOPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPBuildingCov.BOPBldgAnnualIncreaseTerm.Value / 100))).setScale(0, RoundingMode.HALF_UP)

          bldg.BOPEqSpBldgCov.BOPEQSLInclLimit_TDICTerm.Value = (increamentedBPPCovLimit + increamentedBOPBuCovLimit)
        }
        else if (bldg.BOPEqBldgCovExists  and bldg.BOPEqSpBldgCovExists and bldg.BOPPersonalPropCovExists and bldg.BOPPersonalPropCov.HasBOPBPPBldgLimTerm and bldg.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value > 0 and

            bldg.BOPPersonalPropCov.HasBOPInflationGuard_TDICTerm and bldg.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Value > 0)
        {
          var EQSLLIMIT=(bldg.BasedOn?.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPPersonalPropCov.BOPInflationGuard_TDICTerm.Value / 100))).setScale(0,RoundingMode.HALF_UP)
          bldg.BOPEqSpBldgCov.BOPEQSLInclLimit_TDICTerm.Value = EQSLLIMIT
        }
        else if (bldg.BOPEqBldgCovExists and bldg.BOPEqSpBldgCovExists and bldg.BOPBuildingCovExists and bldg.BOPBuildingCov.HasBOPBldgLimTerm and bldg.BOPBuildingCov.BOPBldgLimTerm.Value > 0 and
            bldg.BOPBuildingCov.HasBOPBldgAnnualIncreaseTerm and bldg.BOPBuildingCov.BOPBldgAnnualIncreaseTerm.Value > 0) {
          var EQSLLIMIT_BU=(bldg.BasedOn?.BOPBuildingCov.BOPBldgLimTerm.Value * (1 + (bldg.BasedOn?.BOPBuildingCov.BOPBldgAnnualIncreaseTerm.Value / 100))).setScale(0, RoundingMode.HALF_UP)
          bldg.BOPEqSpBldgCov.BOPEQSLInclLimit_TDICTerm.Value = EQSLLIMIT_BU
        }
      })
    })
  }

  override property get LocationsInUseOnOrAfterSliceDate() : Set<Key> {
    var bopReferencedLocations = _line.VersionList.BOPLocations.allVersionsFlat<BOPLocation>().where(\bopLoc -> bopLoc.ExpirationDate > _line.SliceDate)*.PolicyLocation*.FixedId
    return bopReferencedLocations.union({_line.Branch.PrimaryLocation.FixedId})
  }

  private function getAllBOPLocationsEverForLocation(location : PolicyLocation) : List<BOPLocation> {
    return _line.VersionList.BOPLocations.allVersionsFlat<BOPLocation>().where(\cpLoc -> cpLoc.Location.FixedId == location.FixedId)
  }

  override function onPrimaryLocationCreation(location : PolicyLocation) {
    _line.addToLineSpecificLocations(location.AccountLocation)
  }

  override function createPolicyLineValidation(validationContext : PCValidationContext) : PolicyLineValidation<entity.BusinessOwnersLine> {
    return new BOPLineValidation(validationContext, _line)
  }

  override function createPolicyLineDiffHelper(reason : DiffReason, policyLine : PolicyLine) : BOPDiffHelper {
    return new BOPDiffHelper(reason, this._line, policyLine as BusinessOwnersLine)
  }

  override function doGetTIVForCoverage(cov : Coverage) : BigDecimal {
    switch (cov.FixedId.Type) {
      //BOP Line
      case entity.BOPBuildingCov.Type:
        return getBOPBuildingCovLimit(cov as entity.BOPBuildingCov)
      case BusinessOwnersCov.Type:
        return getBusinessOwnersCovLimit(cov as BusinessOwnersCov)
      case BOPLocationCov.Type:
        return getBOPLocationCovLimit(cov as BOPLocationCov)
    }
    return BigDecimal.ZERO
  }

  override property get ContainsBuildings() : boolean {
    return true
  }

  override function getAllLineBuildingsEver() : List<LineSpecificBuilding> {
    return _line.VersionList.BOPLocations.arrays<BOPBuildingVersionList>("Buildings").allVersionsFlat<BOPBuilding>()
  }

  override protected function getCannotDeleteBuildingMessage(building : Building) : String {
    return DisplayKey.get("BusinessOwners.Building.CannotDelete.HasBOPBuilding", building)
  }

  override protected function getCannotDeleteBuildingFutureMessage(building : Building, dates : String) : String {
    return DisplayKey.get("BusinessOwners.Building.CannotDelete.HasFutureBOPBuilding", building, dates)
  }

  override property get EmergencyServiceFunding() : boolean {
    return true
  }

  override function validateLocations(location : PolicyLocation) {
    BOPLocationValidation.validateBOPLocation(location)
  }

  override function validateSubmissionWizardPolicyInfo(period : PolicyPeriod) {
    BOPPolicyInfoValidation.validateFields(period.BOPLine)
  }

  private function getCurrentOrFutureBOPBuildingsForBuilding(building : Building) : List<BOPBuilding> {
    var allBOPLocationsEver = _line.VersionList.BOPLocations.flatMap(\l -> l.AllVersions)
    var allBOPBuildingsEver = allBOPLocationsEver.flatMap(\l -> l.VersionList.Buildings.flatMap(\bvl -> bvl.AllVersions))
    return allBOPBuildingsEver.where(\bopb -> bopb.Building.FixedId == building.FixedId and bopb.ExpirationDate > building.SliceDate)
  }

  private function getBusinessOwnersCovLimit(cov : BusinessOwnersCov) : BigDecimal {
    switch (cov.PatternCode) {
      case "BOPLiabilityCov":
        return cov.BOPLine.BOPLiabilityCov.BOPLiabilityTerm.PackageValue.PackageTerms*.Value.max()
      case "BOPComputerFraudCov":
        return cov.BOPLine.BOPComputerFraudCov.BOPComputerFraudLimTerm.Value
      case "BOPToolsSchedCov":
        return cov.BOPLine.BOPToolsSchedCov.BOPToolsSchedLimTerm.Value
      case "BOPToolsInstallUnschedCov":
        return cov.BOPLine.BOPToolsInstallUnschedCov.BOPInstallationLimTerm.PackageValue.PackageTerms.singleWhere(\p -> p.AggregationModel == CovTermModelAgg.TC_AG).Value
      case "BOPEmpBenefits":
        return cov.BOPLine.BOPEmpBenefits.BOPEmpBenAggLimTerm.Value
      case "BOPEmpDisCov":
        var employeeLimit = cov.BOPLine.BOPEmpDisCov.BOPEmpDisLimitTerm.Value
        var numEmployees = cov.BOPLine.BOPEmpDisCov.BOPEmpDisNumEmpTerm.Value
        var numLocations = cov.BOPLine.BOPEmpDisCov.BOPEmpDisNumLocTerm.Value
        return employeeLimit * numEmployees * numLocations
      case "BOPForgeAltCov":
        return cov.BOPLine.BOPForgeAltCov.BOPForgeAltLimitTerm.Value
      case "BOPGuestPropCov":
        return cov.BOPLine.BOPGuestPropCov.GuestPropOccLimTerm.Value
      case "BOPGuestSafeDepCov":
        return cov.BOPLine.BOPGuestSafeDepCov.BOPGuestSafeDepLimitTerm.Value
      case "BOPFungiPropCov":
        return cov.BOPLine.BOPFungiPropCov.BOPFungiPropLimTerm.Value
      case "BOPLiquorCov":  //probably needs some work
        return cov.BOPLine.BOPLiquorCov.BOPLiquorAggLimTerm.Value
      default:
        return 0
    }
  }

  private function getBOPLocationCovLimit(cov : BOPLocationCov) : BigDecimal {
    var tiv : BigDecimal = 0
    var location = cov.BOPLocation
    switch (cov.PatternCode) {
      case "BOPBurgRobCov":
        return location.BOPBurgRobCov.BOPBurgRobLimTerm.Value
      case "BOPY2KIncomeExpenseCov":
        return location.BOPY2KIncomeExpenseCov.BOPY2KIncomeExpenseLimTerm.Value
      case "BOPMoneySecCov":
        tiv = addNullSafe(tiv, location.BOPMoneySecCov.BOPMoneyOnPremLimTerm.Value)
        tiv = addNullSafe(tiv, location.BOPMoneySecCov.BOPMoneyOffPremLimTerm.Value)
        return tiv
      case "BOPOutdoorProp":
        return location.BOPOutdoorProp.BOPOutdoorPropLimTerm.PackageValue.PackageTerms.singleWhere(\p -> p.AggregationModel == CovTermModelAgg.TC_AG).Value
      case "BOPOutSignCov":
        return location.BOPOutSignCov.BOPOutdoorSignLimTerm.Value
      case "BOPPersonalEffects":
        return location.BOPPersonalEffects.BOPPersEffectsLimTerm.Value
      default:
        return 0
    }
  }

  private function getBOPBuildingCovLimit(cov : entity.BOPBuildingCov) : BigDecimal {
    var building = cov.BOPBuilding
    switch (cov.PatternCode) {
      case "BOPReceivablesCov":
        return building.BOPReceivablesCov.BOPARonPremLimTerm.Value
      case "BOPBuildingCov":
        return building.BOPBuildingCov.BOPBldgLimTerm.Value
      case "BOPPersonalPropCov":
        return building.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value
      case "BOPCondoUnitOwnCov":
        return building.BOPCondoUnitOwnCov.CondoOwnerLimitTerm.Value
      case "BOPElectricalSchedCov":
        return building.BOPElectricalSchedCov.BOPElectricalSchedLimitTerm.Value
      case "BOPFuncPerPropCov":
        return building.BOPFuncPerPropCov.BOPFuncPerPropLimTerm.Value
      // GPC-823: Change to Remove Mine Sub Limit Term
/*      case "BOPMineSubCov":
          return building.BOPMineSubCov.BOPMineSubLimTerm.Value*/
      case "BOPOrdinanceCov":
        var tiv = BigDecimal.ZERO
        tiv = addNullSafe(tiv, building.BOPOrdinanceCov.BOPOrdLawCov23LimTerm.Value)
        tiv = addNullSafe(tiv, building.BOPOrdinanceCov.BOPOrdLawCov2LimTerm.Value)
        tiv = addNullSafe(tiv, building.BOPOrdinanceCov.BOPOrdLawCov3LimTerm.Value)
        return tiv
      case "BOPTenantsLiabilityCov":
        return building.BOPTenantsLiabilityCov.BOPTenantsLiabLimTerm.Value
      case "BOPValuablePapersCov":
        return building.BOPValuablePapersCov.BOPValPaperOnPremLimTerm.Value
      default:
        return 0
    }
  }

  override function createRatingEngine(method : RateMethod, parameters : Map<RateEngineParameter, Object>) : AbstractRatingEngine<BOPLine> {
    if (RateMethod.TC_SYSTABLE == method) {
      return new BOPSysTableRatingEngine(_line as BOPLine)
    }
    return new BOPRatingEngine(_line as BOPLine, parameters[RateEngineParameter.TC_RATEBOOKSTATUS] as RateBookStatus)
  }

  override property get BaseStateRequired() : boolean {
    return true
  }

  override function shouldQuoteAsynchronously() : boolean {
    return _line.Branch.BOPLine.BOPLocations*.Buildings.Count > AsyncQuoteCoverableThresholdByLine.BOPBuildingThreshold
  }

  override function createPolicyInfoValidation_TDIC(context : PCValidationContext) : AbstractPolicyInfoValidation {
    return new BOPPolicyInfoValidation(context, _line.Branch.BOPLine) as AbstractPolicyInfoValidation;
  }

  /**
   * BOP Worksheet display customization
   *
   * @param showConditionals BrittoS 02/17/2020
   */
  override function getWorksheetRootNode(showConditionals : boolean) : RowTreeRootNode {
    var costs = _line.Costs.cast(BOPCost).toSet()
    var treeNodes : List<WorksheetTreeNodeContainer> = {}
    var eqBreakdownMap = {"BOPBuildingCov" -> BOPCostType_TDIC.TC_EQUIPMENTBREAKBUILDINGLIM, "BOPPersonalPropCov" -> BOPCostType_TDIC.TC_EQUIPMENTBREAKBPPLIM}

    var lineContainer = new WorksheetTreeNodeContainer(_line.DisplayName.concat(" - ").concat(_line.Branch.Offering.Description))
    lineContainer.ExpandByDefault = true
    var locMainContainer = new WorksheetTreeNodeContainer(DisplayKey.get("Web.PolicyFile.Locations"))
    locMainContainer.ExpandByDefault = true
    for (location in _line.BOPLocations.orderBy(\l -> l.PolicyLocation.LocationNum)) {
      var locationCosts = costs.where(\elt -> elt.Location.FixedId == location.FixedId)
      var locContainer = new WorksheetTreeNodeContainer(location.DisplayName)
      locContainer.ExpandByDefault = true
      for (building in location.Buildings) {
        var bldgContainer = new WorksheetTreeNodeContainer(building.DisplayName)
        var buildingCosts = locationCosts.where(\elt -> elt.Building.FixedId == building.FixedId).toSet()
        //Property Worksheets
        var propertyCosts = buildingCosts.byProperty_TDIC()
        if(propertyCosts.HasElements) {
          var propContainer = new WorksheetTreeNodeContainer(DisplayKey.get("Web.Policy.BOP.Quote.Coverages.Building.Title.Property.Premium"))
          for (cost in propertyCosts) {
            var costTreeNodes = WorksheetTreeNodeUtil.buildTreeNodes(cost, showConditionals)
            if(costTreeNodes.HasElements and not isEquipmentBreakdown_TDIC(cost)) {
              var displayName = (cost.Coverage != null) ? cost.Coverage.Pattern.DisplayName : cost.DisplayName
              var costContainer = new WorksheetTreeNodeContainer(displayName)
              costContainer.addChildren(costTreeNodes)
              if (cost.Coverage != null and (cost.Coverage.Pattern.CodeIdentifier == "BOPBuildingCov" or cost.Coverage.Pattern.CodeIdentifier == "BOPPersonalPropCov")) {
                var eqBreakdown = getBuildingCost_TDIC(eqBreakdownMap.get(cost.Coverage.Pattern.CodeIdentifier), buildingCosts)
                costTreeNodes = WorksheetTreeNodeUtil.buildTreeNodes(eqBreakdown, showConditionals)
                if(eqBreakdown != null and costTreeNodes.HasElements) {
                  costContainer.addChildren(costTreeNodes)
                }
              }
              propContainer.addChild(costContainer)
            }
          }
          bldgContainer.addChild(propContainer)
        }

        //Liability Worksheets
        var liabCosts = buildingCosts.byLiability_TDIC()
        if(liabCosts.HasElements) {
          var liabContainer = new WorksheetTreeNodeContainer(DisplayKey.get("Web.Policy.BOP.Quote.Coverages.Building.Title.Liability.Premium"))
          for (cost in liabCosts) {
            var costTreeNodes = WorksheetTreeNodeUtil.buildTreeNodes(cost, showConditionals)
            if(costTreeNodes.HasElements) {
              var displayName = (cost.Coverage != null) ? cost.Coverage.Pattern.DisplayName : cost.DisplayName
              var costContainer = new WorksheetTreeNodeContainer(displayName)
              costContainer.addChildren(costTreeNodes)
              liabContainer.addChild(costContainer)
            }
          }
          bldgContainer.addChild(liabContainer)
        }

        //Taxes & Surcharges
        var taxCosts = buildingCosts.TaxSurcharges
        if(taxCosts.HasElements) {
          var taxContainer = new WorksheetTreeNodeContainer(DisplayKey.get("Web.Policy.BOP.Quote.Other.TaxesAndSurcharges.Title"))
          for (cost in buildingCosts.TaxSurcharges) {
            var costTreeNodes = WorksheetTreeNodeUtil.buildTreeNodes(cost, showConditionals)
            if(costTreeNodes.HasElements) {
              var displayName = (cost.Coverage != null) ? cost.Coverage.Pattern.DisplayName : cost.DisplayName
              var costContainer = new WorksheetTreeNodeContainer(displayName)
              costContainer.addChildren(costTreeNodes)
              taxContainer.addChild(costContainer)
            }
          }
          bldgContainer.addChild(taxContainer)
        }

        //bldgContainer.addChildren({propContainer, liabContainer, taxContainer})
        locContainer.addChild(bldgContainer)
      }
      locMainContainer.addChild(locContainer)
    }
    lineContainer.addChild(locMainContainer)
    treeNodes.add(lineContainer)
    return WorksheetTreeNodeUtil.buildRootNode(treeNodes)
  }

  override function getMultiLineDiscount_TDIC() : Map<String, boolean> {
    //TODO - implement me
    return {}
  }

  override function setMultiLineDiscount_TDIC(value : boolean) : void {
    //TODO - implement me
  }


  /********************************Private functions********************************/

  private function isEquipmentBreakdown_TDIC(cost : BOPCost) : Boolean {
    if (cost typeis BOPBuildingCost_TDIC) {
      if (cost.BOPCostType_TDIC == TC_EQUIPMENTBREAKBPPLIM
          or cost.BOPCostType_TDIC == TC_EQUIPMENTBREAKBUILDINGLIM) {
        return true
      }
    }
    return false
  }

  private function getBuildingCost_TDIC(costType : BOPCostType_TDIC, costs : Set<BOPCost>) : entity.BOPCost {
    return costs.byProperty_TDIC().firstWhere(\elt -> elt typeis BOPBuildingCost_TDIC and elt.BOPCostType_TDIC == costType)
  }

  /**
   *
   * @param period
   */
  private function removeExpiredBOPBuildingItems(period : PolicyPeriod) {
    var renewalEffDate = period.EditEffectiveDate
    for (building in period.BOPLine.BOPLocations*.Buildings) {

      //Additional Insureds
      var additionalInsured = building?.AdditionalInsureds*.PolicyAdditionalInsuredDetails
      additionalInsured.each(\addInsDetail -> {
        if (addInsDetail.ExpirationDate_TDIC != null and addInsDetail.ExpirationDate_TDIC.compareIgnoreTime(renewalEffDate) <= 0) {
          addInsDetail.remove()
        }
      })

      //Mortgagees
      var mortgagees = building?.BOPBldgMortgagees
      mortgagees.each(\mortgagee -> {
        if (mortgagee.ExpirationDate_TDIC != null and mortgagee.ExpirationDate_TDIC.compareIgnoreTime(renewalEffDate) <= 0) {
          mortgagee.remove()
        }
      })
      //Loss payees
      var lossPayees = building?.BOPBldgLossPayees
      lossPayees.each(\losspayee -> {
        if (losspayee.ExpirationDate_TDIC != null and losspayee.ExpirationDate_TDIC.compareIgnoreTime(renewalEffDate) <= 0) {
          losspayee.remove()
        }
      })
      //Manuscripts
      var manuScripts = building?.BOPManuscript
      manuScripts.each(\manuScript -> {
        if (manuScript.ManuscriptExpirationDate != null and manuScript.ManuscriptExpirationDate.compareIgnoreTime(renewalEffDate) <= 0) {
          manuScript.remove()
        }
      })
    }
  }
}
