package gw.lob.wc7.schedule

uses gw.api.locale.DisplayKey
uses gw.api.productmodel.ClausePattern
uses gw.api.domain.Schedule
uses gw.api.productmodel.SchedulePropertyInfo
uses gw.api.productmodel.ScheduleTypeKeyPropertyInfo
uses gw.api.util.DisplayableException
uses gw.entity.TypeKey
uses gw.lang.reflect.IPropertyInfo
uses gw.pl.persistence.core.Bundle

/**
 * Abstract implementation of the Schedule interface. Also provides implementation for StateScheduleAutoNumberSequence
 */
@Export
abstract class AbstractJurisdictionScheduleImpl<T extends WC7JurisdictionScheduleAutoNumberSequence  > implements Schedule {

  var _owner : T as Owner
  var _propertyInfoMap : Map<String, SchedulePropertyInfo> as PropertyInfoMap
  
  construct(delegateOwner : T) {
    Owner = delegateOwner
    PropertyInfoMap = PropertyInfos.partitionUniquely(\ propInfo -> propInfo.PropertyInfo.Name)
  }

  override property get MostDescriptivePropertyInfo() : SchedulePropertyInfo {
    if (PropertyInfoMap.get("StringCol1") != null) {
      return PropertyInfoMap.get("StringCol1")
    } else if (PropertyInfoMap.get("NamedInsured") != null) {
      return PropertyInfoMap.get("NamedInsured")
    } else if (PropertyInfoMap.get("TypeKeyCol1") != null) {
      return PropertyInfoMap.get("TypeKeyCol1")
    } else if (PropertyInfoMap.get("IntCol1") != null) {
      return PropertyInfoMap.get("IntCol1") 
    } else if (PropertyInfoMap.get("PosIntCol1") != null) {
      return PropertyInfoMap.get("PosIntCol1")
    } else if (PropertyInfoMap.get("DateCol1") != null) {
      return PropertyInfoMap.get("DateCol1")
    }
    return null
  } 
  
  override property get ScheduleName() : String {
    return Owner.DisplayName  
  }

  override property get PropertyInfos() : SchedulePropertyInfo[] {
    // Subtypes need to handle specific patterns
    throw new IllegalStateException("Unknown pattern type")
  }

  override property get PropertyInfosMap() :  Map<String, SchedulePropertyInfo> {
    var schedulePropertyInfos = new HashMap<String, SchedulePropertyInfo>()
    var propertyArray = PropertyInfos;
    if(propertyArray != null){
      for(var schedulePropertyInfo in propertyArray){
        schedulePropertyInfos.put(schedulePropertyInfo.Label, schedulePropertyInfo)
      }
    }
    return schedulePropertyInfos
  }

  protected function initializeScheduledItem(scheduledItem : Coverable & ScheduledItem) {
    scheduledItem.createCoveragesConditionsAndExclusions()
  }
  
  override function renumberAutoNumberSequence() {
    if (Owner.WC7JurisdicSchedAutoNumberSeq != null) { //this could be null if the schedule doesn't have scheduled items
      Owner.WC7JurisdicSchedAutoNumberSeq.renumber(CurrentAndFutureScheduledItems, ScheduleNumberPropInfo)
    }
  }
  
  override function renumberNewScheduledItems() {
    if (Owner.WC7JurisdicSchedAutoNumberSeq != null) {
      Owner.WC7JurisdicSchedAutoNumberSeq.renumberNewBeans(CurrentAndFutureScheduledItems, ScheduleNumberPropInfo)
    }
  }

  override function cloneAutoNumberSequence() {
    if (Owner.WC7JurisdicSchedAutoNumberSeq != null) {
      Owner.WC7JurisdicSchedAutoNumberSeq = Owner.WC7JurisdicSchedAutoNumberSeq.clone(Owner.Bundle)
    }
  }

  override function resetAutoNumberSequence() {
    if (Owner.WC7JurisdicSchedAutoNumberSeq != null) {
      Owner.WC7JurisdicSchedAutoNumberSeq.reset()
      renumberAutoNumberSequence()
    }
  }

  override function bindAutoNumberSequence() {
    if (Owner.WC7JurisdicSchedAutoNumberSeq != null) {
      renumberAutoNumberSequence()
      Owner.WC7JurisdicSchedAutoNumberSeq.bind(CurrentAndFutureScheduledItems, ScheduleNumberPropInfo)
    }
  }

  override function initializeAutoNumberSequence(bundle : Bundle) {
    if (Owner.WC7JurisdicSchedAutoNumberSeq == null) {
      Owner.WC7JurisdicSchedAutoNumberSeq = new AutoNumberSequence(bundle)
    }
  }

  override function createAutoNumber(scheduledItem : KeyableBean) {
    initializeAutoNumberSequence(scheduledItem.Bundle)
    Owner.WC7JurisdicSchedAutoNumberSeq.number(scheduledItem, CurrentAndFutureScheduledItems, ScheduleNumberPropInfo)
  }

  /**
   * Returns an array containing scheduled items from current and future slices. Used for autonumbering.
   */
  abstract protected property get CurrentAndFutureScheduledItems() : KeyableBean[]
  
  /**
   * Get scheduled item property via reflection. Used for autonumbering.
   */
  abstract protected property get ScheduleNumberPropInfo() : IPropertyInfo

  override function getScheduledItemDescription(scheduledItem : ScheduledItem) : String {
    var propInfo = MostDescriptivePropertyInfo
    if (propInfo != null) {
      var propValue = propInfo.PropertyInfo.Accessor.getValue(scheduledItem) as String
      if (propValue != null and propValue != "") {
      
      if (propInfo typeis ScheduleTypeKeyPropertyInfo){
        var getters = propInfo.ValueRange as List<TypeKey>
        propValue = getters.firstWhere(\ t -> t.Code == propValue).DisplayName
      }

        return DisplayKey.get("Web.Policy.ScheduledItem", propValue)
      } else {
        return DisplayKey.get("Web.Policy.ScheduledItem", "")
      }
    } 
    throw new DisplayableException(DisplayKey.get("Web.Policy.NoDescriptiveColumn", scheduledItem.DisplayName))
  } 
}
