package gw.lob.wc7.schedule

uses java.lang.IllegalStateException

@Export
class WC7ContactDetailScheduledItemImpl implements WC7SpecificScheduledItem {
  
  var _contactDetail : WC7ContactDetail as readonly Owner
  
  construct(contactDetail : WC7ContactDetail) {
    _contactDetail = contactDetail
  }

  override property get ParentClause() : Clause {
    validateParentClause()
    return _contactDetail.LaborContactCondition == null
        ? _contactDetail.LaborContactExclusion
        : _contactDetail.LaborContactCondition
  }

  private function validateParentClause() {
    if (_contactDetail.LaborContactCondition != null and _contactDetail.LaborContactExclusion != null)
      throw new IllegalStateException("Either the condition or exclusion must be null")
  }
}
