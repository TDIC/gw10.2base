package gw.lob.wc7.availability

uses gw.api.database.Query
uses gw.lang.reflect.features.PropertyReference
uses java.lang.IllegalStateException
uses java.lang.Iterable
uses java.util.LinkedHashMap
uses java.util.Map
uses java.util.Set
uses java.util.Collection
uses java.util.List

class WC7EmployeeLeasingPolicyTypeAvailabilityFinder {
  function availablePolicyTypesFor(jurisdictions : Iterable<Jurisdiction>) : List<WC7EmployeeLeasingPolicyType> {
    return jurisdictions.reduce<Set<WC7EmployeeLeasingPolicyType>>({},
        \ results, jurisdiction -> results.union(availablePolicyTypesForJurisdiction(jurisdiction))).order()
  }

  function availableJurisdictionsFor(policyType : WC7EmployeeLeasingPolicyType,
      coveredStates : Iterable<Jurisdiction>) : List<Jurisdiction> {
    return coveredStates.where(\ jurisdiction -> isAvailable(policyType, jurisdiction))
  }

  private function availablePolicyTypesForJurisdiction(jurisdiction: Jurisdiction)
      : Collection<WC7EmployeeLeasingPolicyType> {
    return WC7EmployeeLeasingPolicyType.getTypeKeys(false)
        .where(\ policyType -> isAvailable(policyType, jurisdiction))
  }

  private function isAvailable(policyType : WC7EmployeeLeasingPolicyType, jurisdiction : Jurisdiction) : boolean {
    return progressivelyGeneralLookupSearchOn({
        WC7EmployeeLeasingPolicyTypeLookup#PolicyType -> policyType,
        WC7EmployeeLeasingPolicyTypeLookup#Jurisdiction -> jurisdiction
    }).Availability == TC_AVAILABLE
  }

  private function progressivelyGeneralLookupSearchOn(
      fields : LinkedHashMap<PropertyReference<WC7EmployeeLeasingPolicyTypeLookup, Object>, Object>)
      : WC7EmployeeLeasingPolicyTypeLookup {
    var searchCriteria = fields.copy()
    var fieldNamesToGeneralize = fields.Keys.reverse().iterator()
    while (true) {
      var lookup = lookupMatching(searchCriteria)
      if (lookup != null)
        return lookup
      if (fieldNamesToGeneralize.hasNext())
        searchCriteria.put(fieldNamesToGeneralize.next(), null)
      else
        throw new IllegalStateException("No employee leasing policy type availability lookup found for " +
            fields.entrySet().map(\ f -> f.Key + " = " + f.Value).join(", "))
    }
  }

  private function lookupMatching(criteria : Map<PropertyReference<WC7EmployeeLeasingPolicyTypeLookup, Object>, Object>)
      : WC7EmployeeLeasingPolicyTypeLookup {
    var query = Query.make(WC7EmployeeLeasingPolicyTypeLookup)
    criteria.eachKeyAndValue(\ prop, value -> query.compare(prop.FeatureInfo.Name, Equals, value))
    return query.select().getAtMostOneRow()
  }
}