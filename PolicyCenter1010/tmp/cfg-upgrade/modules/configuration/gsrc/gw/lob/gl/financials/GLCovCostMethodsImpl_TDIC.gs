package gw.lob.gl.financials

uses gw.api.util.JurisdictionMappingUtil

/**
 * Created with IntelliJ IDEA.
 * User: AnkitaG
 * Date: 10/21/2019
 * Time: 3:28 PM
 * To change this template use File | Settings | File Templates.
 */
class GLCovCostMethodsImpl_TDIC extends GenericGLCostMethodsImpl<GLCovCost_TDIC> {
  construct(owner : GLCovCost_TDIC) {
    super(owner)
  }

  override property get OwningCoverable() : Coverable {
    return Cost.GeneralLiabilityLine
  }

  override property get Coverage() : Coverage {
    return Cost.GeneralLiabilityCoverage
  }

  override property get State() : Jurisdiction {
    return Cost.GeneralLiabilityLine.BaseState
  }

}