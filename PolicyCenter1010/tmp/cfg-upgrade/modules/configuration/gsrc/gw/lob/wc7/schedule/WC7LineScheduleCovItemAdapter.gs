package gw.lob.wc7.schedule

uses gw.api.productmodel.ClausePattern
uses gw.lob.common.AbstractScheduledItemAdapter
uses gw.api.domain.Schedule
uses gw.policy.PolicyLineConfiguration
uses java.util.List

/**
 * An implementation of {@link gw.api.productmodel.ScheduledItemAdapter} for {@link entity.WC7LineScheduleCovItem}
 */
@Export
class WC7LineScheduleCovItemAdapter extends AbstractScheduledItemAdapter {

  var _owner : WC7LineScheduleCovItem  as readonly Owner

  construct(item : WC7LineScheduleCovItem) {
    _owner = item
  }
  
  override property get ScheduleParent() : Schedule {
    return _owner.Schedule
  }

  override property get PolicyLine() : PolicyLine {
    return _owner.Schedule.WCLine
  }

  override property get Clause() : entity.Clause {
    return null
  }

  override function hasClause() : boolean {
    return false
  }
  
  override property get DefaultCurrency() : Currency {
    return _owner.Schedule.WCLine.PreferredCoverageCurrency
  }

  override property get AllowedCurrencies() : List<Currency> {
    return PolicyLineConfiguration.getByLine(InstalledPolicyLine.TC_WC7).AllowedCurrencies
  }

  override function hasClause(pattern: ClausePattern): boolean {
    return (getClause(pattern) != null)
  }

  override property get Clauses(): Clause[] {
    return {}
  }

  override function getClause(pattern: ClausePattern): Clause {
    return Clauses?.firstWhere( \ clause -> clause.Pattern == pattern)
  }

}
