package gw.lob.common

uses entity.RIRisk
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
uses gw.api.webservice.exception.SOAPServerException
uses gw.losshistory.ClaimSearchCriteria
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.Plugins
uses gw.plugin.claimsearch.IClaimSearchPlugin
uses gw.plugin.claimsearch.NoResultsClaimSearchException
uses gw.plugin.claimsearch.ResultsCappedClaimSearchException
uses gw.policy.PolicyEvalContext
uses gw.question.QuestionIssueAutoRaiser
uses tdic.pc.config.job.helper.JobProcessHelper
uses tdic.pc.integ.plugins.policyperiod.TDIC_PolicyPeriodIsMultiline

uses java.math.BigDecimal

@Export
class DefaultUnderwriterEvaluator extends AbstractUnderwriterEvaluator {

  construct(policyEvalContext : PolicyEvalContext) {
    super(policyEvalContext)
  }

  override function onPrequote() {
    periodStartAndEndDates()
    underwritingCompanySegmentNotValid()
    sumOfPreQuoteRiskFactor()
    producerChanged()
    //02062020 Britto S - ADA Number and Multi-Line discount valiations are not required for Cyber
    if (_policyEvalContext.Period.Offering.CodeIdentifier != "PLCyberLiab_TDIC") {
      // BrianS - GW-2910 - Don't check ADA numbers on Quick Quotes.
      if (_policyEvalContext.Period.Submission.QuoteType != QuoteType.TC_QUICK) {
        checkADANumbers_TDIC()
      }
      checkMultiLineDiscount_TDIC()
    }
  }

  override function onPrequoteRelease() {
    quoteHasManualOverrides()
  }

  override function onUWHold() {
    policyHold()
  }

  override function onRegulatoryHold() {
    policyHold()
  }

  override function onQuestion() {
    QuestionIssueAutoRaiser.autoRaiseIssuesForQuestions(_policyEvalContext)
  }

  override function onReferral() {
    var allReferralReasons = _policyEvalContext.Period.Policy.UWReferralReasons
    var openReferralReasons = allReferralReasons.where(\referral -> referral.Open)
    for (referralReason in openReferralReasons) {
      var issue = _policyEvalContext.addIssue(
          referralReason.IssueType.Code,
          referralReason.IssueKey,
          \-> referralReason.ShortDescription,
          \-> referralReason.LongDescription
      )
      issue.Value = referralReason.Value
    }
  }

  override function onReinsurance() {
    var period = _policyEvalContext.Period
    var nextOOSSliceDate = period.Policy.BoundEditEffectiveDates.firstWhere(\d -> d > period.SliceDate)
    // There may be more slices of RIRisk than OOSSlice (because of auto splitting on
    // program end for example. So we need to find those slices of RIRisk and
    // evaluate them.
    var allRIRiskVersions = period.AllReinsurables*.RIVersionList*.AllVersions.where(\ririsk -> ririsk.EffectiveDate >= period.SliceDate
        and (nextOOSSliceDate == null or ririsk.EffectiveDate < nextOOSSliceDate))

    allRIRiskVersions.each(\ririsk -> {
      checkTargetNetRetention(ririsk)
      checkFacNotCededTo(ririsk)
      checkAttachmentNotCededToCapacity(ririsk)
    })
  }

  private function checkTargetNetRetention(ririsk : RIRisk) {
    var risk = ririsk.Reinsurable
    var facRINeeded = ririsk.FacRINeeded
    if (facRINeeded.IsPositive) {
      var shortDescription = DisplayKey.get("UWIssue.Reinsurance.NetRetentionGreaterThanTarget.ShortDesc")
      var longDescription = DisplayKey.get("UWIssue.Reinsurance.NetRetentionGreaterThanTarget.LongDesc", risk, CurrencyUtil.renderAsCurrency(facRINeeded))
      _policyEvalContext.addIssue("RINetRetention", risk.UWIssueKey, \-> shortDescription, \-> longDescription, ririsk.NetRetention)
    }
  }

  private function checkFacNotCededTo(ririsk : RIRisk) {
    var nonCededFacs = ririsk.Attachments.FacAgreements.where(\fac -> fac.CededRisk.IsZero)
    if (nonCededFacs.HasElements) {
      var agreementsDisplay = nonCededFacs*.Agreement*.Name.toList()
//      nonCededFacs.each( \ fac -> {
//        agreementsDisplay += DisplayKey.get("Web.Reinsurance.RIRisk.Validation.Agreements", fac.Agreement.Name)
//      })

      var shortDescription = DisplayKey.get("Web.Reinsurance.RIRisk.Validation.AgreementsHaveNoCededRiskShort")
      var longDescription = DisplayKey.get("Web.Reinsurance.RIRisk.Validation.AgreementsHaveNoCededRiskLong", ririsk.Reinsurable, agreementsDisplay)
      _policyEvalContext.addIssue("AgreementsHaveNoCededRisk", ririsk.UWIssueKey, \-> shortDescription, \-> longDescription)
    }
  }

  private function checkAttachmentNotCededToCapacity(ririsk : RIRisk) {
    var attachmentsNotCededToMax = ririsk.Attachments.where(\att -> att.MaxCeding != null and att.MaxCeding > att.CededRisk)
    if (attachmentsNotCededToMax.HasElements) {
      var agreementsDisplay = attachmentsNotCededToMax*.Agreement*.Name.toList()
//      attachmentsNotCededToMax.each( \ att -> {
//        agreementsDisplay += DisplayKey.get("Web.Reinsurance.RIRisk.Validation.Agreements", att.Agreement.Name)
//      })

      var shortDescription = DisplayKey.get("Web.Reinsurance.RIRisk.Validation.AgreementsDoNotCedeToCapacityShort")
      var longDescription = DisplayKey.get("Web.Reinsurance.RIRisk.Validation.AgreementsDoNotCedeToCapacityLong", ririsk.Reinsurable, agreementsDisplay)
      _policyEvalContext.addIssue("AgreementsDoNotCedeToCapacity", ririsk.UWIssueKey, \-> shortDescription, \-> longDescription)
    }
  }

  override function onRenewal() {
    //BrittoS 04/07/2020 - disabled for PL & CP, heavy work for Underwriters
    if(not(_policyEvalContext.Period.GLLineExists or _policyEvalContext.Period.BOPLineExists)) {
      renewalLossClaim()
    }
  }

  /**
   * BrittoS 04/03/2020
   * final check for UW issues prior to Issuance
   */
  override function onPreissuance(){
    checkForUWReview_TDIC()
  }

  /*----------Private Helper Functions----------*/

  private function createClaimTotalIncurred(result : ClaimSet, basedOn : PolicyPeriod) {
    var claimWithMaxCost = result.Claims.maxBy(\c -> c.TotalIncurred_TDIC)
    var shortDescription = \-> DisplayKey.get("UWIssue.LossClaims.ClaimTotalIncurred.ShortDesc", claimWithMaxCost.ClaimNumber)
    var longDescription = \-> DisplayKey.get("UWIssue.LossClaims.ClaimTotalIncurred.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"), claimWithMaxCost.ClaimNumber, claimWithMaxCost.TotalIncurred)
    _policyEvalContext.addIssue("ClaimTotalIncurred", "ClaimTotalIncurred", shortDescription, longDescription, claimWithMaxCost.TotalIncurred_TDIC)
  }

  private function createIncidenceOfClaim(totalIncurred : MonetaryAmount, writtenPremium : BigDecimal, claimCount : int, basedOn : PolicyPeriod) {
    var shortDescription = \-> DisplayKey.get("UWIssue.LossClaims.IncidenceOfClaims.ShortDesc")
    var longDescription = \-> DisplayKey.get("UWIssue.LossClaims.IncidenceOfClaims.LongDesc", claimCount, basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"), writtenPremium)
    _policyEvalContext.addIssue("IncidenceOfClaims", "IncidenceOfClaims", shortDescription, longDescription, claimCount)

    if (not totalIncurred.IsZero) {
      // Create RatioOfClaimsTotalIncurredToWrittenPremium
      var value = totalIncurred.divide(writtenPremium, 5, HALF_UP)
      shortDescription = \-> DisplayKey.get("UWIssue.LossClaims.RatioOfClaimsTotalIncurredToWrittenPremium.ShortDesc")
      longDescription = \-> DisplayKey.get("UWIssue.LossClaims.RatioOfClaimsTotalIncurredToWrittenPremium.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"), totalIncurred, writtenPremium, value.multiply(100))
      _policyEvalContext.addIssue("RatioOfClaimsTotalIncurredToWrittenPremium", "RatioOfClaimsTotalIncurredToWrittenPremium", shortDescription, longDescription, value)
    }
  }

  private function periodStartAndEndDates() {
    if (_policyEvalContext.Period.Job typeis Rewrite and _policyEvalContext.Period.PeriodEnd != _policyEvalContext.Period.BasedOn.PeriodEnd) {
      var period = _policyEvalContext.Period
      var shortDescription = \-> DisplayKey.get("UWIssue.Rewrite.PeriodMismatch.ShortDesc")
      var longDescription =
          \-> DisplayKey.get("UWIssue.Rewrite.PeriodMismatch.LongDesc", period.PeriodStart,
              period.PeriodEnd, period.BasedOn.PeriodStart, period.BasedOn.PeriodEnd)
      _policyEvalContext.addIssue("RewritePeriodDates", "RewritePeriodDates",
          shortDescription, longDescription)
    }
  }

  private function policyHold() {
    var holdQuery = Query.make(PolicyHold)
        .join(PolicyHold#IssueType)
        .compare(UWIssueType#CheckingSet, Equals, _policyEvalContext.CheckingSet)

    var policyHolds = holdQuery.select().toList()
    for (hold in policyHolds) {
      if (hold.compareWithPolicyPeriod(_policyEvalContext.Period)) {
        var bundle = _policyEvalContext.Period.Bundle
        hold = bundle.add(hold)
        hold.updateLastEvalTime(_policyEvalContext.Period.Job, java.util.Date.CurrentDate, _policyEvalContext.Period)
        _policyEvalContext.addIssue(hold.IssueType.Code, hold.PolicyHoldCode, \-> hold.Description, \-> hold.UWIssueLongDesc)
      }
    }
  }

  private function quoteHasManualOverrides() {
    if (_policyEvalContext.Period.hasAtLeastOneCostOverride()) {
      var shortDescription = \-> DisplayKey.get("UWIssue.PersonalAuto.QuoteHasManualOverrides.ShortDesc")
      var longDescription = \-> DisplayKey.get("UWIssue.PersonalAuto.QuoteHasManualOverrides.LongDesc")
      _policyEvalContext.addIssue("QuoteHasManualOverrides", "QuoteHasManualOverrides",
          shortDescription, longDescription)
    }
  }

  private function renewalLossClaim() {
    if (not(_policyEvalContext.Period.Job typeis Renewal)) {
      return
    }

    var renewalPeriod = _policyEvalContext.Period
    var basedOn = renewalPeriod.BasedOn

    try {
      var result = searchForClaims(basedOn)
      var totalIncurred = result.Claims.where(\c -> c.TotalIncurred_TDIC != null).sum(_policyEvalContext.Period.PreferredSettlementCurrency, \c -> c.TotalIncurred_TDIC)
      var claimCount = result.Claims.Count

      if (not totalIncurred.IsZero) {
        createClaimTotalIncurred(result, basedOn)
      }

      var writtenPremium = basedOn.TotalPremiumRPT
      if (writtenPremium != null and not writtenPremium.IsZero and claimCount != 0) {
        createIncidenceOfClaim(totalIncurred, writtenPremium, claimCount, basedOn)
      }
    } catch (ex : NoResultsClaimSearchException) {
      // No actions necessary
    } catch (ex : ResultsCappedClaimSearchException) {
      // Create ManualClaimReviewNeeded
      var shortDescription = \-> DisplayKey.get("UWIssue.LossClaims.ManualClaimReviewNeeded.ShortDesc")
      var longDescription = \-> DisplayKey.get("UWIssue.LossClaims.ManualClaimReviewNeeded.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"))
      _policyEvalContext.addIssue("ManualClaimReviewNeeded", "ManualClaimReviewNeeded",
          shortDescription, longDescription)
    } catch (ex : SOAPServerException) {
      //Create UnableRetrieveClaimInfo
      var shortDescription = \-> DisplayKey.get("UWIssue.LossClaims.UnableRetrieveClaimInfo.ShortDesc")
      var longDescription = \-> DisplayKey.get("UWIssue.LossClaims.UnableRetrieveClaimInfo.LongDesc", basedOn.PeriodStart.format("short"), basedOn.EndOfCoverageDate.format("short"))
      _policyEvalContext.addIssue("UnableRetrieveClaimInfo", "UnableRetrieveClaimInfo",
          shortDescription, longDescription)
    }
  }

  private function searchForClaims(basedOn : PolicyPeriod) : ClaimSet {
    var result : ClaimSet
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var searchCriteria = new ClaimSearchCriteria()
      searchCriteria.Policy = basedOn.Policy
      if (basedOn.Status == typekey.PolicyPeriodStatus.TC_LEGACYCONVERSION) {
        searchCriteria.PolicyNumber = basedOn.LegacyPolicyNumber_TDIC
      }
      searchCriteria.DateCriteria.StartDate = basedOn.PeriodStart
      searchCriteria.DateCriteria.EndDate = basedOn.EndOfCoverageDate
      searchCriteria.DateCriteria.DateSearchType = TC_ENTEREDRANGE
      result = Plugins.get(IClaimSearchPlugin).searchForClaims(searchCriteria)
    })
    return result
  }

  private function sumOfPreQuoteRiskFactor() {
    if (_policyEvalContext.Period.Job?.createsNewPolicy() and
        _policyEvalContext.CheckingSet == TC_PREQUOTE and
        _policyEvalContext.Period.PreQualRiskPointSum >= 100) {

      var sum = _policyEvalContext.Period.PreQualRiskPointSum
      _policyEvalContext.addIssue("PreQualQuestionRiskPointSum",
          "PreQualQuestionRiskPointSum",
          \-> DisplayKey.get("UWIssue.Question.PreQualRiskPointSumDescription", sum),
          \-> DisplayKey.get("UWIssue.Question.PreQualRiskPointSumDescription", sum),
          sum)
    }
  }

  private function underwritingCompanySegmentNotValid() {
    if (_policyEvalContext.Period.Job typeis Submission or
        _policyEvalContext.Period.Job typeis Rewrite or
        _policyEvalContext.Period.Job typeis RewriteNewAccount) {
      var uwcompany = _policyEvalContext.Period.UWCompany
      var polperiod = _policyEvalContext.Period
      var segmentOkay = true
      var uwcompanysegment : typekey.Segment
      if (uwcompany.LicensedStates.Count > 0) {
        uwcompanysegment = uwcompany.LicensedStates
            .firstWhere(\l -> l.State == polperiod.BaseState).Segment
        //  if the polperiod segment is low, then any uw company is okay
        if (polperiod.Segment != TC_LOW) {
          // policy segment is not lows so if the uwcompanysegmemnt is low, then problem
          if (uwcompanysegment == TC_LOW) {
            segmentOkay = false
          } else {
            // if uw company is high then no problem
            // so only problem is if uw is medium and policy is high
            if (uwcompanysegment == TC_MED and polperiod.Segment == TC_HIGH) {
              segmentOkay = false
            }
          }
        }
      }
      if (!segmentOkay) {
        var shortDescription =
            \-> DisplayKey.get("UWIssue.PolicySegment.PolicyRiskSegmentInvalidForUWCompany")
        var longDescription =
            \-> DisplayKey.get("UWIssue.PolicySegment.PolicyRiskSegmentValueNotAllowedForUWCompany", polperiod.Segment.DisplayName, uwcompany.DisplayName)
        _policyEvalContext.addIssue("UWCompanySegmentValid", "UWCompanySegmentValid",
            shortDescription, longDescription)
      }
    }
  }

  protected function checkADANumbers_TDIC() : void {
    // Check if there is at least one applicable ADA Number
    var multiLineDiscount = new TDIC_PolicyPeriodIsMultiline(_policyEvalContext.Period)
    if (multiLineDiscount.ADANumbers.Empty) {
      // Jeff Lin, 4/9/2020, 11:45 AM, GPC-4345: This UW issue "ADA Number(s) have not been added to the policy" is not applicable to Migrated Renewal (PL and CP)
      if (_policyEvalContext.Period.Job.Subtype == typekey.Job.TC_RENEWAL and
         (_policyEvalContext.Period.GLLineExists or _policyEvalContext.Period.BOPLineExists) and
          _policyEvalContext.Period.BasedOn.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION) {
        return
      }
      var uwIssueCode = "TDIC_MissingADANumbers"
      var shortDesc = \-> DisplayKey.get("TDIC.UWIssue.TDIC_MissingADANumbers.ShortDesc")
      var longDesc = \-> DisplayKey.get("TDIC.UWIssue.TDIC_MissingADANumbers.LongDesc")
      _policyEvalContext.addIssue(uwIssueCode, uwIssueCode, shortDesc, longDesc)
    }
  }

  protected function checkMultiLineDiscount_TDIC() : void {
    // Check if MultiLine Discount matches
    var warnings = _policyEvalContext.Period.JobProcess.setMultiLineDiscount_TDIC()
    if (warnings.Count > 0) {
      var uwIssueCode = "TDIC_MismatchedMultiLineDiscount"
      var shortDesc = \-> DisplayKey.get("TDIC.UWIssue.TDIC_MismatchedMultiLineDiscount.ShortDesc")
      var longDesc = \-> warnings.join("  ")
      _policyEvalContext.addIssue(uwIssueCode, uwIssueCode, shortDesc, longDesc)
    }
  }

  /**
   * Lock period and send for review
   */
  private function checkForUWReview_TDIC() {
    var period = _policyEvalContext.Period
    if (not perm.System.editlockoverride or User.util.CurrentUser.ExternalUser or User.util.CurrentUser.UserType != UserType.TC_UNDERWRITER) {
      var hasBlockingUWIssue = period.UWIssuesActiveOnly.hasMatch(\uw -> uw.isBlocking(typekey.UWIssueBlockingPoint.TC_BLOCKSISSUANCE))
      if (hasBlockingUWIssue) {
        JobProcessHelper.createActivityForReview(period)
        period.JobProcess.setPostUWRequestChanges()
        period.Bundle.commit()
      }
    }
  }
}
