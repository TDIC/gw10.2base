package gw.lob.wc7

uses gw.coverage.ConditionAdapterBase

@Export
class WC7JurisdictionConditionAdapter extends ConditionAdapterBase {
  var _owner : WC7JurisdictionCond
  
  construct(owner : WC7JurisdictionCond)
  {
    super(owner)
    _owner = owner
  }

  override property get CoverageState() : Jurisdiction
  {
    return(_owner.WC7Jurisdiction.Jurisdiction)
  }

  override property get PolicyLine() : PolicyLine
  {
    return(_owner.WC7Jurisdiction.WCLine)
  }

  override property get OwningCoverable() : Coverable
  {
    return(_owner.WC7Jurisdiction)
  }

  override function addToConditionArray( condition: PolicyCondition ) : void
  {
    _owner.WC7Jurisdiction.addToConditions(condition as WC7JurisdictionCond)
  }

  override function removeFromParent() : void
  {
    _owner.WC7Jurisdiction.removeFromConditions( _owner )
  }

}
