package gw.lob.bop

uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses gw.pl.persistence.core.Bundle
uses gw.plugin.Plugins
uses gw.plugin.contact.IContactConfigPlugin
uses typekey.PolicyContactRole

/**
 * An enhancement for {@link entity.BusinessOwnersLine BusinessOwnersLine}
 */
enhancement BusinessOwnersLineEnhancement : entity.BusinessOwnersLine {
  
  /**
   * Returns an array containing BOPScheduledEquipments from current and future slices.
   */
   property get CurrentAndFutureBOPScheduledEquipments() : BOPScheduledEquipment[] { 
    var equipments = this.BOPScheduledEquipments.toList()
    this.Branch.OOSSlices.each(\p ->  p.BOPLine.BOPScheduledEquipments.each(\e -> {  if(!equipments.contains(e)) equipments.add(e) }));
    return equipments.toTypedArray()
  }

  /**
   * Get the {@link entity.BOPLocation BOPLocation} corresponding to the provided {@link entity.PolicyLocation PolicyLocation}
   * @param location the {@link entity.PolicyLocation PolicyLocation} to match.
   * @return the associated {@link entity.BOPLocation BOPLocation} or null if none is found.
   */
  //GWPS- 2166 To default control address
  function getBOPLocationForPolicyLocation(location : PolicyLocation) : BOPLocation {
    var bopLocation = this.BOPLocations.firstWhere(\ b -> b.Location == location)
    if(bopLocation.PolicyLocation.AddressLine1 != null and bopLocation.PolicyLocation.PostalCode != null){
      bopLocation.PolicyLocation.Description = bopLocation.PolicyLocation.AddressLine1.remove(" ").toUpperCase() +
          (bopLocation.PolicyLocation.PostalCode.contains("-") ? bopLocation.PolicyLocation.PostalCode.split("-").first() : bopLocation.PolicyLocation.PostalCode)
    }
    return bopLocation
  }



  /**
   * The additional coverage categories for this line.  This will vary depending on the  {@link entity.BusinessOwnersLine#SmallBusinessType}
   * @return an array of strings representing additional coverage categories
   */
  function getAdditionalCoverageCategories() : String[] {
    if (this.SmallBusinessType == TC_MOTEL) {
      // BOPGuesCovCat is an included category
      return new String[]{"BOPContractorCat", "BOPLiabilityOtherCat", "BOPCrimeCat", "BOPLiquorCat","BOPPolicyOtherCat","BOPProfessionalCat","BOPTerrorismCat", "BOPStateCat"}
    }
    else if (this.SmallBusinessType == TC_CONTRACTOR or this.SmallBusinessType == TC_CONTRACTOR_LAND) {
      // BOPConttractorCat is an included category
      return new String[]{"BOPGuestCovCat", "BOPLiabilityOtherCat", "BOPCrimeCat", "BOPLiquorCat","BOPPolicyOtherCat","BOPProfessionalCat","BOPTerrorismCat", "BOPStateCat"}
    }
    else {
      return new String[]{"BOPGuestCovCat","BOPContractorCat", "BOPLiabilityOtherCat", "BOPCrimeCat", "BOPLiquorCat","BOPPolicyOtherCat","BOPProfessionalCat","BOPTerrorismCat", "BOPStateCat"}
    }
  }

  /**
   * Create and add a new {@link entity.BOPScheduledEquipment BOPScheduledEquipment} to the line, the equipment schedule.
   * @return the newly created {@link entity.BOPScheduledEquipment BOPScheduledEquipment}
   */
  function createAndAddScheduledEquip() : BOPScheduledEquipment {
    var item = new BOPScheduledEquipment(this.Branch)
    item.BOPLine = this
    this.addToBOPScheduledEquipments(item)
    this.EquipmentAutoNumberSeq.number(item, CurrentAndFutureBOPScheduledEquipments, BOPScheduledEquipment.Type.TypeInfo.getProperty("EquipmentNumber"))
    return item
  }

  /**
   * Auto-Numbering methods for the Schedule of Equipment
   **/

  /**
   * Renumber the equipment
   * @see com.guidewire.pc.domain.AutoNumberSequence#renumber(com.guidewire.commons.entity.KeyableBean[], gw.lang.reflect.IPropertyInfo)
   */
  function renumberScheduledEquipments() {
    this.EquipmentAutoNumberSeq.renumber(CurrentAndFutureBOPScheduledEquipments, BOPScheduledEquipment.Type.TypeInfo.getProperty("EquipmentNumber"))
  }

  /**
   * Renumber the new equipment
   * @see com.guidewire.pc.domain.AutoNumberSequence#renumberNewBeans(com.guidewire.commons.entity.KeyableBean[], gw.lang.reflect.IPropertyInfo)
   */
  function renumberNewScheduledEquipments() {
    this.EquipmentAutoNumberSeq.renumberNewBeans(CurrentAndFutureBOPScheduledEquipments, BOPScheduledEquipment.Type.TypeInfo.getProperty("EquipmentNumber"))
  }

  /**
   * Remove the provided equipement item form the equipment schedule.
   * @param equipment the item to remove
   */
  function removeScheduledEquip(equipment : BOPScheduledEquipment) {
    this.removeFromBOPScheduledEquipments(equipment)
    renumberScheduledEquipments()
  }

  /**
   * Return a clone of the current auto-numbering sequence.  This is necessary whenever a new term is created.
   * @see com.guidewire.pc.domain.AutoNumberSequence#clone(gw.pl.persistence.core.Bundle)
   */
  function cloneEquipmentAutoNumberSequence() {
    this.EquipmentAutoNumberSeq =  this.EquipmentAutoNumberSeq.clone(this.Bundle)
  }

  /**
   * Reset the equipment auto-number sequence
   * @see com.guidewire.pc.domain.AutoNumberSequence#reset()
   */
  function resetEquipmentAutoNumberSequence() {
    this.EquipmentAutoNumberSeq.reset()
   renumberScheduledEquipments()
  }

  /**
   * Reserve all used auto-numbers for equipment
   * @see com.guidewire.pc.domain.AutoNumberSequence#bind(com.guidewire.commons.entity.KeyableBean[], gw.lang.reflect.IPropertyInfo)
   */
  function bindEquipmentAutoNumberSequence() {
    renumberScheduledEquipments()
    this.EquipmentAutoNumberSeq.bind(CurrentAndFutureBOPScheduledEquipments, BOPScheduledEquipment.Type.TypeInfo.getProperty("EquipmentNumber"))
  }

  /**
   * Initialize a new equipment auto number sequence
   * @param bundle the entity {@link gw.pl.persistence.core.Bundle Bundle} to initialize the sequence in.
   * @see com.guidewire.pc.domain.AutoNumberSequence
   */
  function initializeEquipmentAutoNumberSequence(bundle : Bundle) {  
    this.EquipmentAutoNumberSeq = new AutoNumberSequence(bundle)
  }

  /**
   * Deselect the contractors equipement coverage if there are no items in the schedule, otherwise sum the tools
   * @see #sumBOPToolsValue()
   */
  function maybeUnselectCoverage() {
    if (this.BOPScheduledEquipments.Count < 1) {
      this.BOPToolsSchedCov.remove()
    }
    else {
      sumBOPToolsValue()
    }
  }

  /**
   * Sum the total equipment value of tools and assign it to the limit of BOPToolsSchedCov.
   */
  function sumBOPToolsValue() {
    this.BOPToolsSchedCov.BOPToolsSchedLimTerm.Value = this.BOPScheduledEquipments.sum( \ item ->item.EquipmentValue )
  }

  /**
   * The default the {@link entity.BOPBuilding BOPBuilding} that will serve as the default container for a new {@link PolicyAddlInterest}
   * <br/>
   * <br/>
   * e.g. In the user interface for creating a new additional interest, a default building is provided.
   * The default building is determined by this method.
   *
   * @return the default {@link entity.BOPBuilding BOPBuilding}
   */
  function getDefaultContainerForAddlInterest() : BOPBuilding {
    var bopBuildings = this.BOPLocations*.Buildings
    return bopBuildings.orderBy( \ c -> c.BOPLocation.Location.LocationNum).thenBy(\ b -> b.Building.BuildingNum).first()
  }

  /**
   * @return an array of {@link entity.BOPTransaction BOPTransactions} for this line.
   */
  property get BOPTransactions() : BOPTransaction[] {
    var branch = this.Branch
    return branch.getSlice(branch.PeriodStart).BOPTransactions
  }

  /**
   * Add new [Policy] owner/officer to the BOPLine, and revision it to the Account.
   */
  function addNewBOPPolicyOwnerOfficerOfContactType_TDIC(contactType : ContactType) : BOPPolicyOwnerOfficer_TDIC {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return addBOPPolicyOwnerOfficer_TDIC(acctContact.Contact)
  }

  /**
   * throws exception if Owner Officer for the given Account Contact already exists on this line.
   */
  function addBOPPolicyOwnerOfficer_TDIC(contact : Contact) : BOPPolicyOwnerOfficer_TDIC {
    if (this.BOPPolicyOwnerOfficer_TDIC.firstWhere(\ p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyOwnerOfficer.Error.AlreadyExists", contact))
    }
    var policyOfficer = this.Branch.addNewPolicyContactRoleForContact(contact, PolicyContactRole.TC_BOPPOLICYOWNEROFFICER_TDIC) as BOPPolicyOwnerOfficer_TDIC
    this.addToBOPPolicyOwnerOfficer_TDIC(policyOfficer)
    return policyOfficer
  }

  /**
   * For each AccountContact returned by the UnassignedOwnerOfficer property,
   * add that AccountContact as an Included Owner/Officer to the BOPLine
   * throws an exception the owner officer exclusion is null
   */
  function addAllExistingBOPPolicyOwnerOfficer_TDIC() : BOPPolicyOwnerOfficer_TDIC[] {
    var newBOPPolicyOwnerOfficers = new ArrayList<BOPPolicyOwnerOfficer_TDIC>()
    for(ac in this.UnassignedOwnerOfficers_TDIC) {
      newBOPPolicyOwnerOfficers.add(addBOPPolicyOwnerOfficer_TDIC(ac.Contact))
    }
    return newBOPPolicyOwnerOfficers.toTypedArray()
  }

  /**
   * All the account Owner Officers that are not already assigned to this policy line.
   */
  property get UnassignedOwnerOfficers_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(PolicyContactRole.TC_BOPPOLICYOWNEROFFICER_TDIC)
    var assignedOwnerOfficers = this.BOPPolicyOwnerOfficer_TDIC.map(\ ownerOfficer -> ownerOfficer.AccountContactRole.AccountContact)
    return this.Branch.Policy.Account
        .getAccountContactsWithRole(accountContactRoleType)
        .subtract(assignedOwnerOfficers)
        .toTypedArray()
  }

  @Throws(DisplayableException, "if the specified contact is already an Entity Owner on this policy line")
  function addBOPPolicyEntityOwner_TDIC(contact : Contact) : BOPPolicyEntityOwner_TDIC {
    if (this.BOPPolicyOwnerOfficer_TDIC.firstWhere(\ p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyOwnerOfficer.Error.AlreadyExists", contact))
    }
    var entityOwner = this.Branch.addNewPolicyContactRoleForContact(contact, PolicyContactRole.TC_BOPPOLICYENTITYOWNER_TDIC) as BOPPolicyEntityOwner_TDIC
    this.addToBOPPolicyOwnerOfficer_TDIC(entityOwner)
    return entityOwner
  }

  /**
   * US890
   * Creates a new account contact of the specified contact type and creates a new Entity Owner policy contact role from that contact.
   */
  function addNewBOPPolicyEntityOwnerOfContactType_TDIC(contactType : ContactType) : BOPPolicyEntityOwner_TDIC {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return addBOPPolicyEntityOwner_TDIC(acctContact.Contact)
  }

  /**
   * US890, robk
   *
   * All the account Entity Owners that are not already assigned to this policy line.
   */
  property get UnassignedBOPEntityOwners_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(PolicyContactRole.TC_BOPPOLICYENTITYOWNER_TDIC)
    var assignedOwnerOfficers = this.BOPPolicyOwnerOfficer_TDIC.map(\ ownerOfficer -> ownerOfficer.AccountContactRole.AccountContact)
    return this.Branch.Policy.Account
        .getAccountContactsWithRole(accountContactRoleType)
        .subtract(assignedOwnerOfficers)
        .toTypedArray()
  }

  /**
   * US890
   * For each AccountContact returned by the UnassignedOwnerOfficer property,
   * add that AccountContact as an Included Owner/Officer to the BOPLine
   * throws an exception the owner officer exclusion is null
   */
  function addAllExistingBOPPolicyEntityOwners_TDIC() : BOPPolicyEntityOwner_TDIC[] {
    var newPolicyEntityOwners = new ArrayList<BOPPolicyEntityOwner_TDIC>()
    for(ac in this.UnassignedBOPEntityOwners_TDIC) {
      newPolicyEntityOwners.add(addBOPPolicyEntityOwner_TDIC(ac.Contact))
    }
    return newPolicyEntityOwners.toTypedArray()
  }

  /**
   * Any account contact that is not an Owner Officer.
   */
  property get BOPOwnerOfficerOtherCandidates_TDIC() : AccountContact[] {
    return otherContactsFor_TDIC(PolicyContactRole.TC_BOPPOLICYOWNEROFFICER_TDIC)
  }

  private function otherContactsFor_TDIC(policyContactRole : typekey.PolicyContactRole) : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(policyContactRole)
    return this.Branch.Policy.Account.ActiveAccountContacts
        .where(\ ac -> plugin.canBeRole(ac.ContactType, accountContactRoleType) and not ac.hasRole(accountContactRoleType))
  }
}
