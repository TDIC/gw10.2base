package gw.lob.wc7

uses java.util.Date
uses java.io.Serializable
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.effdate.EffDatedFinderUtil
uses java.util.regex.Pattern
uses java.util.ArrayList
uses java.lang.IllegalArgumentException
uses gw.api.database.IQueryBeanResult
uses java.util.List

@Export
class WC7ClassCodeSearchCriteria implements Serializable {
  var _code: String as Code
  var _classification: String as Classification
  var _shortDesc: String as ShortDesc
  var _jurisdiction : Jurisdiction as Jurisdiction
  var _effectiveAsOfDate: Date as EffectiveAsOfDate
  var _previousSelectedClassCode : WC7ClassCode as PreviousSelectedClassCode
  var _classCodeType : WC7ClassCodeType as ClassCodeType
  var _basisTypePublicId : String as BasisTypePublicId
  var _aRatedTypeType : Boolean as ARatedType
  var _coalMineType : Boolean as CoalMineType
  var _constructionType : Boolean as ConstructionType
  var _diseaseType : Boolean as DiseaseType
  var _programType : WC7ClassCodeProgramType as ProgramType
  var _excludedClassCodeTypes : List<WC7ClassCodeType> as ExcludedClassCodeTypes
  final static var nonWordPattern = "\\W+"

  function performSearch() : IQueryBeanResult<WC7ClassCode> {
    var query = constructBaseQuery()
    if (this.Code != null) {
      query.startsWith(WC7ClassCode#Code.getPropertyInfo() as String, this.Code, true )
    }
    
    if (this.EffectiveAsOfDate != null) {
      EffDatedFinderUtil.addRestrictionsForEffectiveOnDate(query, this.EffectiveAsOfDate)
    }
    
    // don't need to do union if it's searching for specific code and the code is not previous code
    if (this.PreviousSelectedClassCode != null and (this.Code == null or this.PreviousSelectedClassCode.Code.startsWith(this.Code))) { 
      return query.union( getQueryForPreviousClassCode()).select()
    }
    return query.select()
  }

  function performExactSearch() : IQueryBeanResult<WC7ClassCode> {
    var query = constructBaseQuery()
    if (this.Code != null) {
      query.compare(WC7ClassCode#Code.getPropertyInfo() as String, Relop.Equals, this.Code)
    }
    
    if (this.EffectiveAsOfDate != null) {
      EffDatedFinderUtil.addRestrictionsForEffectiveOnDate(query, this.EffectiveAsOfDate)
    }
    
    if (this.PreviousSelectedClassCode != null and (this.Code == null or this.PreviousSelectedClassCode.Code == this.Code)) {
      return query.union( getQueryForPreviousClassCode()).select()
    }
    return query.select()
  }

  function performExactCodeAndSameDescWordsLessPunctuationSearch(): WC7ClassCode {
    var shortDescWords: String[]
    var query = constructBaseQueryWithoutShortDesc()

    if (this.ShortDesc == null) {
      throw new IllegalArgumentException("Description cannot be null")
    }

    shortDescWords = this.ShortDesc.split(nonWordPattern)
    for (var word in shortDescWords) {
      query.contains("ShortDesc", word, true)
    }

    if (this.Code != null) {
      query.compare(WC7ClassCode#Code.getPropertyInfo() as String, Relop.Equals, this.Code)
    }

    if (this.EffectiveAsOfDate != null) {
      EffDatedFinderUtil.addRestrictionsForEffectiveOnDate(query, this.EffectiveAsOfDate)
    }

    var results = query.select()

    return findClosestMatchFromResults(results, shortDescWords)
  }

  function findClosestMatchFromResults(results: IQueryBeanResult<WC7ClassCode>, shortDescWords: String[]): WC7ClassCode {
    var matchingResult: WC7ClassCode
    //If there are multiple descriptions that only vary by whitespace this finds the correct code.
    var exactMatch = results.firstWhere(\r -> r.ShortDesc == this.ShortDesc)
    if (exactMatch != null) {
      matchingResult = exactMatch
    } else {
      //Since all words must be found by the DB query, a match with the same number of words should be the closest match
      matchingResult = results.firstWhere(\result -> {
        var resultWords = result.ShortDesc.split(nonWordPattern)

        return wordsAndOrderAreTheSame(resultWords, shortDescWords)
      })
    }
    return matchingResult
  }

  protected function wordsAndOrderAreTheSame(resultWords : String[], shortDescWords : String[]) : boolean {
    var matches = false
    if (resultWords.Count == shortDescWords.Count) {
      matches = true
      for (i in 0..|shortDescWords.Count) {
        if (not resultWords[i].equalsIgnoreCase(shortDescWords[i])) {
          matches = false
          break
        }
      }
    }
    return matches
  }
  
  private function constructBaseQuery() : Query<WC7ClassCode> {
    var aQuery = constructBaseQueryWithoutShortDesc()
    if (this.ShortDesc != null ) {
      aQuery.compare("ShortDesc", Relop.Equals, this.ShortDesc)
    }
    return aQuery
  }

  private function constructBaseQueryWithoutShortDesc() : Query<WC7ClassCode> {
    var aQuery = Query.make(WC7ClassCode)

    if (this.Classification != null ) {
      aQuery.contains("Classification", this.Classification, true)
    }
    if (this.Jurisdiction != null ) {
      aQuery.compare("Jurisdiction", Relop.Equals, this.Jurisdiction)
    }
    if (this.ClassCodeType != null) {
      aQuery.compare("ClassCodeType", Relop.Equals, this.ClassCodeType)
    } else if (this.ExcludedClassCodeTypes.HasElements) {
      aQuery.or(\ restriction -> restriction.compare("ClassCodeType", Relop.Equals, null)
             .compareNotIn("ClassCodeType", this.ExcludedClassCodeTypes.toArray()))
    }
    if(this._basisTypePublicId != null) {
      var classCodeBasis = Query.make(ClassCodeBasis).compare("PublicId", Relop.Equals, this.BasisTypePublicId)
          .select().AtMostOneRow
      aQuery.compare("Basis", Relop.Equals, classCodeBasis)
    }
    if (this.ConstructionType != null and this.ConstructionType == true) {
      aQuery.compare("ConstructionType", Relop.Equals, this.ConstructionType)
    }
    if (this.DiseaseType != null and this.DiseaseType == true) {
      aQuery.compare("DiseaseType", Relop.Equals, this.DiseaseType)
    }
    if (this.ARatedType != null) {
      aQuery.compare("ARatedType", Relop.Equals, this.ARatedType)
    }
    if (this.CoalMineType != null) {
      aQuery.compare("CoalMineType", Relop.Equals, this.CoalMineType)
    }
    if (this.ProgramType != null) {
      aQuery.compare("ProgramType", Relop.Equals, this.ProgramType)
    }
    return aQuery
  }

  private function getQueryForPreviousClassCode() : Query<WC7ClassCode> {
    var q = constructBaseQuery()
    if (this.PreviousSelectedClassCode != null) {
      q.compare(WC7ClassCode#Code.getPropertyInfo() as String, Relop.Equals, this.PreviousSelectedClassCode.Code)
    }
    return q
  }  
}
