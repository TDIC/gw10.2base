package gw.lob.wc7.schedule

uses gw.api.productmodel.ClausePattern
uses gw.api.productmodel.ClauseSchedulePattern
uses gw.api.productmodel.SchedulePropertyInfo
uses gw.entity.ITypeFilter
uses gw.lang.reflect.IPropertyInfo
uses gw.lob.common.schedules.ScheduleConfigSource
uses gw.lob.common.service.ServiceLocator

/**
 * The implementation of schedules for State conditions.
 * 
 * Any newly defined State condition schedules should include a switch case in #PropertyInfos to define what the schedule column values are.
 * 
 * @see entity.WC7StateScheduleCond
 * @see AbstractScheduleImpl
 */
@Export
class WC7JurisdictionScheduleCondImpl extends AbstractJurisdictionScheduleImpl<entity.WC7JurisdictionScheduleCond> {

  construct(delegateOwner : entity.WC7JurisdictionScheduleCond) {
    super(delegateOwner)
  }

  override property get ScheduledItems() : ScheduledItem[] {
    return Owner.WC7JurisdictionScheduleCondItems
  }

  override property get SchedulePattern() : ClauseSchedulePattern {
    return Owner.SchedulePattern
  }

  override function createAndAddScheduledItem() : ScheduledItem {
    var scheduledItem = new WC7JurisdictSchedCondItem(Owner.Branch)
    createAutoNumber(scheduledItem)
    Owner.addToWC7JurisdictionScheduleCondItems(scheduledItem)
    initializeScheduledItem(scheduledItem)
    return scheduledItem
  }

  override property get PropertyInfos() : SchedulePropertyInfo[] {
    switch (typeof Owner) {
      default:
        return ServiceLocator.get(ScheduleConfigSource).getPropertyInfos<WC7JurisdictionScheduleCond>(Owner)
    }
  }
  
  override function removeScheduledItem(item : ScheduledItem) {
    Owner.removeFromWC7JurisdictionScheduleCondItems(item as WC7JurisdictSchedCondItem)
    renumberAutoNumberSequence()
  }

  override property get CurrentAndFutureScheduledItems() : KeyableBean[] {
    var schedItems = Owner.ScheduledItems.toList()

    Owner.Branch.OOSSlices
      .where(\ p ->  p.WC7Line != null)
      .each(\ p ->  {
        var matchingSlicedScheduleCond = p.WC7Line.ConditionsFromCoverable.firstWhere(\ c -> c.FixedId == Owner.FixedId) as WC7JurisdictionScheduleCond
        if (matchingSlicedScheduleCond != null){
          matchingSlicedScheduleCond.ScheduledItems.each(\ s -> {
            if(!schedItems.contains(s)) {
              schedItems.add(s)
            }
          })
        }
      })

    return schedItems.map(\ item -> item as WC7JurisdictSchedCondItem).toTypedArray()
  }

  override property get ScheduleNumberPropInfo() : IPropertyInfo {
    return WC7JurisdictSchedCondItem.Type.TypeInfo.getProperty("ScheduleNumber")
  }

  private function stateFilterFor(clausePattern : ClausePattern) : ITypeFilter<Jurisdiction> {
    return this.Owner.WC7Jurisdiction.WCLine.stateFilterFor(clausePattern)
  }

  override property get ScheduledItemMultiPatterns() : ClausePattern[] {
    // Simple schedules without cov terms do not have a pattern
    return null
  }
}
