package gw.lob.wc7.stateconfigs

uses gw.api.database.Query
uses gw.api.domain.covterm.CovTerm
uses gw.api.productmodel.ClausePattern
uses gw.api.productmodel.CovTermPattern
uses gw.api.productmodel.CovTermPatternLookup
uses gw.api.productmodel.ModifierPatternBase
uses gw.api.web.job.JobWizardHelper
uses gw.lang.reflect.TypeSystem
uses java.math.BigDecimal
uses gw.api.upgrade.PCCoercions

class WC7StateConfig {
  static function forJurisdiction(jurisdiction : Jurisdiction) : WC7StateConfig {
    var lookup = Query.make(WC7StateConfigLookup).compare(WC7StateConfigLookup#Jurisdiction, Equals, jurisdiction)
        .select()
        .getAtMostOneRow()
    return lookup == null
        ? new WC7StateConfig()
        : instantiateStateConfigClassNamed(lookup.ConfigClass)
  }

  private static function instantiateStateConfigClassNamed(className : String) : WC7StateConfig {
    var constructor = TypeSystem.getByFullName(className).TypeInfo.getCallableConstructor({}).Constructor
    return constructor.newInstance({}) as WC7StateConfig
  }

  property get BenefitsDeductibleCovTermLabel() : String {
    return DeductibleCovTermPattern.DisplayName
  }

  property get IsBenefitsDeductibleCovTermRequired() : boolean {
    return DeductibleCovTermPattern.Required
  }

  function isTypeKeyModifierRequired(modifier : WC7Modifier) : boolean {
    return false
  }

/**
*  Only use for line level clauses as we grab the first jurisdiction that we find on the line which works
 *  for the existing clause names that we have which are modified if only a certain jurisdiction is on the policy on it's
 *  own
 */
  static function getLineClauseName(clausePattern : ClausePattern, wc7Line : WC7Line) : String {
    var firstJurisdiction = wc7Line.WC7Jurisdictions[0].Jurisdiction
    return forJurisdiction(firstJurisdiction).getClauseName(clausePattern, wc7Line)
  }

  function getClauseName(clausePattern : ClausePattern, wc7Line : WC7Line) : String {
    return clausePattern.DisplayName
  }

  /**
   *  Use to get the Modifer name overriden for a jurisdiction
   */
  static function getJurisdictionModifierName(modifier : Modifier, jurisdiction : Jurisdiction) : String {
    return forJurisdiction(jurisdiction).getModifierName(modifier)
  }

  /**
   *  Use to get the Modifer name overriden for a jurisdiction
   */
  static function getJurisdictionModifierName(modifierPattern : ModifierPatternBase, jurisdiction : Jurisdiction) : String {
    return forJurisdiction(jurisdiction).getModifierName(modifierPattern)
  }

  function getModifierName(modifierPattern : ModifierPatternBase) : String {
    return modifierPattern.Name
  }

  function getModifierName(modifierName : String) : String {
    return modifierName
  }

  function getModifierName(modifier : Modifier) : String {
    return getModifierName(modifier.Pattern)
  }

  function getCovTermName(covTerm : CovTerm) : String {
    return covTerm.DisplayName
  }

  private property get DeductibleCovTermPattern() : CovTermPattern {
    return CovTermPatternLookup.getByPublicID("WC7Deductible")
  }

  function getRateFactorNameFor(rateFactor : RateFactor) : String {
    return rateFactor.Pattern.Name
  }

  function getRateFactorDescriptionFor(rateFactor : RateFactor) : String {
    return rateFactor.Pattern.Description
  }

  function getClauseNameFromEntity(clausePattern : ClausePattern, wc7Line : WC7WorkersCompLine) : String {
    return clausePattern.DisplayName
  }

  function syncSupplementaryDisease(jobWizardHelper : JobWizardHelper, wc7Line : WC7WorkersCompLine, jurisdiction : WC7Jurisdiction) {}

  function getGoverningLaws(jurisdiction : WC7Jurisdiction) : WC7GoverningLaw[] {
    return jurisdiction.getPossibleGoverningLaws()
  }

  //functionality defined in iso state configs. Called from DiffTree.xml.  Used to display correct label in diffTree.
  function getTypeKeyModifierLabel(modifier : Modifier) : String {
    return null
  }

  //functionality defined in iso state configs. Called from DiffTree.xml. Used to display correct typekey value in diffTree.
  function getTypeKeyModifierValue(modifier : Modifier) : String {
       return null
  }

  //functionality defined in iso state configs.
  function setTypeKeyModifierDefaultValue(jurisdiction : WC7Jurisdiction) {}

  function isFelaSplitOptionAvailable(perPerson : int, perAccident : int) : boolean {
    return false
  }

  function isMaritimeSplitOptionAvailable(perPerson : int, perAccident : int) : boolean {
    return false
  }

  function isMaritimeProgramIIApplied(period : PolicyPeriod) : boolean {
    if(period.WC7Line.WC7MaritimeACovExists){
      var maritimeCoverage = period.WC7Line.WC7MaritimeACov
      var programType = maritimeCoverage.getCovTerm(PCCoercions.makeProductModel<CovTermPattern>("WC7MaritimeProgram"))
      return programType.ValueAsString == WC7CovProgramType.TC_PROGRAMII as String
    }
    return false
  }

  function getAuditSurchargeAmount(auditSurcharge: BigDecimal,  wc7Line : WC7WorkersCompLine) : BigDecimal {
    return  auditSurcharge
  }

  property get AuditSurchargeEditable() : boolean {
    return true
  }

  property get AuditSurchargeState () : boolean {
    return false
  }
}