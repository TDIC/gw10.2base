package gw.lob.gl

uses gw.api.locale.DisplayKey
uses gw.policy.PolicyAddlInsuredAndTypeUniqueValidation
uses gw.policy.PolicyLineValidation
uses gw.util.GosuStringUtil
uses gw.validation.PCValidationContext

@Export
class GLLineValidation extends PolicyLineValidation<entity.GeneralLiabilityLine> {

  construct(valContext : PCValidationContext, polLine : entity.GeneralLiabilityLine) {
    super(valContext, polLine)
  }

  static function validateExposures(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context -> new GLLineValidation(context, line).checkExposures())
  }

  static function validatePolicyContacts(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context ->
        new GLLineValidation(context, line).additionalInsuredAndTypeUnique())
  }

  static function validateAdditionalInsuredTypeDesc_TDIC(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context ->
        new GLLineValidation(context, line).validateAdditionalInsuredTypeAndDesc_TDIC(line))
  }

  static function validateSchedules(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context -> new GLLineValidation(context, line).checkSchedulesAreNotEmpty())
  }

  public static function validateGLScreenBeforeSave(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context -> {
      var val = new GLLineValidation(context, line)
      val.checkSchedulesAreNotEmpty()
      val.limitLocumTenensSchedule_TDIC(line)
      val.changeExpiryDate_TDIC(line)
      val.validateCovExtEndorsement_TDIC(line)
      val.validateAdditionalInsuredTypeAndDesc_TDIC(line)
    })
  }

  /**
   * create by: ChitraK
   *
   * @description: Function to validate BR-001 and BR-004 as part of PL Product model story
   * @create time: 9:16 PM 7/18/2019
   * @return:
   */
  static function validatePLCovLimits_TDIC(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context -> new GLLineValidation(context, line).limitLocumTenensSchedule_TDIC(line))
    /*if (line.BaseState == Jurisdiction.TC_NJ and line.GLNJDeductibleCov_TDICExists) {//GPC-865
      PCValidationContext.doPageLevelValidation(\context -> new GLLineValidation(context, line).checkNJDedLimits_TDIC(line))
    }*///Removed as per DEfect GPC-1514
  }

  static function validateCYBLimitUWQuestions_TDIC(line : GLLine) {
    PCValidationContext.doPageLevelValidation(\context ->
        new GLLineValidation(context, line).validateCYBLimitsUWQuestions_TDIC(line))
  }

  property get glLine() : entity.GeneralLiabilityLine {
    return Line
  }

  /**
   * Validate the GL Line.
   * <p>
   * Checks the following:
   * <ul>
   * <li>Additional Insured and Type are unique</li>
   * <li>Retro date is valid</li>
   * <li>At least one exposure</li>
   * <li>Exposures are valid</li>
   * </ul>
   */
  override function doValidate() {
    additionalInsuredAndTypeUnique()
    validateRetroDate()
    checkExposures()
    atLeastOneExposure()
    validateLocationState_TDIC()
    changeExpiryDateDIMS_TDIC()
    if (glLine.GLCoverageForm == null) {
      Result.addError(glLine, TC_QUOTABLE, DisplayKey.get("Web.Policy.GL.Validation.GLCoverageForm"), "GLLine")
    }
  }

  override function validateLineForAudit() {
    allAuditAmountsShouldBeFilledIn()
  }

  private function additionalInsuredAndTypeUnique() {
    Context.addToVisited(this, "additionalInsuredAndTypeUnique")
    for (var addlInsured in glLine.AdditionalInsureds) {
      new PolicyAddlInsuredAndTypeUniqueValidation(Context, addlInsured).validate()
    }
  }

    /*
   * create by: SureshB
   * @description: method to validate Additional Insured Type and Description
   * @create time: 6:33 PM 10/10/2019
    * @param null
   * @return:
   */

  private function validateAdditionalInsuredTypeAndDesc_TDIC(line : GLLine) {
    var warningRequired = false
    warningRequired = line.PolicyLine.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.hasMatch(\addlInsuredDetail ->
        addlInsuredDetail.AdditionalInsuredType == AdditionalInsuredType.TC_OTHER and GosuStringUtil.isBlank(addlInsuredDetail.Desciption))
    if (warningRequired) {
      Context.Result.addWarning(line, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.LocationContainer.Location.Building.AdditionalInsured.HasOtherTypeAndBlankDesc"))
    }
  }

  private function atLeastOneExposure() {
    Context.addToVisited(this, "atLeastOneExposure")
    if (glLine.GLExposuresWM.Count < 1 && glLine.Branch.Offering.CodeIdentifier != "PLCyberLiab_TDIC") {
      Result.addError(glLine, TC_DEFAULT, DisplayKey.get("Web.Policy.GL.Validation.AtLeastOneExposure"), "GLLineEU")
    }
  }

  /**
   * create by: ChitraK
   *
   * @description: Validation Multi Location
   * @create time: 6:41 PM 9/26/2019
   * @return:
   */

  private function validateLocationState_TDIC() {
    Context.addToVisited(this, "validateLocationState")
    var polLocation = glLine.Branch.PolicyLocations.where(\elt -> elt.State.Code != glLine.BaseState.Code)
    if (polLocation.Count > 0) {
      Result.addError(glLine, ValidationLevel.TC_QUOTABLE, DisplayKey.get("TDIC.Web.Policy.Validation.MultiState"), "GLLine")
      Result.addError(glLine, ValidationLevel.TC_QUOTABLE, DisplayKey.get("TDIC.Web.Policy.Validation.MultiLocation"), "GLLine")
    }
  }

  /**
   * Verify that all AuditedAmounts are filled in
   */
  function allAuditAmountsShouldBeFilledIn() {
    if (glLine.Branch.Job typeis Audit) {
      glLine.VersionList.Exposures.flatMap(\g -> g.AllVersions).each(\glExposure -> {
        if (glExposure.AuditedBasis == null) {
          Result.addError(glLine,
              TC_QUOTABLE,
              DisplayKey.get("Web.AuditWizard.Details.NullAmountsError"),
              DisplayKey.get("Web.AuditWizardMenu.Details"))
        }
      }
      )
    }
  }

  function validateRetroDate() {
    if (glLine.GLCoverageForm == GLCoverageFormType.TC_CLAIMSMADE) {
      if (glLine.RetroactiveDate.before(glLine.ClaimsMadeOrigEffDate)) {
        Result.addWarning(glLine, TC_DEFAULT, DisplayKey.get("Web.Policy.GL.Validation.RetroDateTooEarly"))
      }
      if (glLine.RetroactiveDate.after(glLine.Branch.PeriodStart)) {
        Result.addError(glLine, TC_DEFAULT, DisplayKey.get("Web.Policy.GL.Validation.RetroDateAfterPeriodStart"))
      }
    }
  }

  private function checkExposures() {
    glLine.Exposures.each(\exposure -> new GLExposureValidation(Context, exposure).validate())
  }

  /**
   * create by: ChitraK
   *
   * @description: Throw error for Incorrect combination NJ Ded Limits
   * @create time: 4:59 PM 7/17/2019
   * @return:
   */

  protected function checkNJDedLimits_TDIC(line : GLLine) {
    var bolCheck : Boolean = false
    if (line.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 5 && line.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Value == 15) {
      bolCheck = true
    }
    if (line.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 6 && line.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Value == 18) {
      bolCheck = true
    }
    if (line.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 7 && line.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Value == 21) {
      bolCheck = true
    }//GPC-872
    if (line.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 8 && line.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Value == 24) {
      bolCheck = true
    }
    if (line.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 9 && line.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Value == 36) {
      bolCheck = true
    }
    if (!bolCheck)
      Context.Result.addError(Line, ValidationLevel.TC_DEFAULT, DisplayKey.get("GLLine.Validation.Error2_TDIC"))

  }
  /*
   * create by: SureshB
   * @description: method to validate CYB UW Questions realted to Cyber limit values
   * @create time: 6:15 PM 10/21/2019
    * @param null
   * @return:
   */

  /**
   * create by: ChitraK
   *
   * @description: Restrict Locum Tenenes Schedule to 2
   * @create time: 8:11 PM 7/18/2019
   * @return:
   */
  protected function limitLocumTenensSchedule_TDIC(line : GLLine) {
    if (line.GLLocumTenensSched_TDIC.Count > 2) {
      Context.Result.addError(Line, ValidationLevel.TC_DEFAULT, DisplayKey.get("GLLine.Validation.Error3_TDIC"))
    }
  }

  /*
 * create by: SureshB
 * @description: method to validate CYB UW Questions realted to Cyber limit values
 * @create time: 6:15 PM 10/21/2019
  * @param null
 * @return:
 */
  private function validateCYBLimitsUWQuestions_TDIC(line : GLLine) {

    if (line.GLCyberLiabCov_TDICExists) {
      var cyberLimit = line.GLCyberLiabCov_TDIC?.GLCybLimit_TDICTerm?.Value
      var question1 = glLine.getAnswerForGLUWQuestion_TDIC("OfficeContentsInsuredWithTDICCYB_TDIC")
      var question2 = glLine.getAnswerForGLUWQuestion_TDIC("BreachOfPersonalInoIn12MonthsCYB_TDIC")
      var question3 = glLine.getAnswerForGLUWQuestion_TDIC("ConductBackgroundScreensForEmployeesCYB_TDIC")
      var question4 = glLine.getAnswerForGLUWQuestion_TDIC("HavePostedDocumentRetensionPolicyCYB_TDIC")
      var question5 = glLine.getAnswerForGLUWQuestion_TDIC("RegularlyUpdateComputerSecurityMeasuresCYB_TDIC")
      var question6 = glLine.getAnswerForGLUWQuestion_TDIC("CustomerPhysicalRecordsMaintainedInSecuredPlaceCYB_TDIC")
      var count = 0
      if (question3) count = count + 1
      if (question4) count = count + 1
      if (question5) count = count + 1
      if (question6) count = count + 1
      if (line.BaseState != Jurisdiction.TC_ND and question1 and cyberLimit == 100000 and !(!question2 and (question3 or question4))) {
        Result.addError(line, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.GL.CYB.100KQualificationValidation"))
      }
      if (question1 and cyberLimit == 250000 and !(!question2 and count >= 2)) {
        Result.addError(line, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.GL.CYB.250KQualificationValidation"))
      }
    }

    var warningRequired = false
    warningRequired = line.PolicyLine.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.hasMatch(\addlInsuredDetail ->
        addlInsuredDetail.AdditionalInsuredType == AdditionalInsuredType.TC_OTHER and GosuStringUtil.isBlank(addlInsuredDetail.Desciption))
    if (warningRequired) {
      Context.Result.addWarning(line, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.Web.Policy.LocationContainer.Location.Building.AdditionalInsured.HasOtherTypeAndBlankDesc"))
    }
  }

  /**
   * create by: ChitraK
   *
   * @description: Check Expiry Date
   * @create time: 9:06 PM 11/14/2019
   * @return:
   */

  public function changeExpiryDate_TDIC(line : GLLine) {
    if(line.Branch.PeriodStart != line.Branch.EditEffectiveDate.FirstDayOfMonth){
      if ((line.Branch.Job typeis Submission  && !line.GLNewDentistDiscount_TDICExists) && line.Branch.Offering.CodeIdentifier != "PLCyberLiab_TDIC")
      {
        line.Branch.TermType = TermType.TC_OTHER
        var periodEnd = line.Branch.PeriodEnd
        var normalizedPeriodEnd = line.Branch.EditEffectiveDate.addMonths(12).FirstDayOfMonth
        if (periodEnd != normalizedPeriodEnd) {
          line.Branch.PeriodEnd = normalizedPeriodEnd
          Context.Result.addWarning(line, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.GLLine.Validation.ExpiryDate.Warning", line.Branch.PeriodEnd.ShortFormat))
        }
      }
    }
  }

  /**
   * create by: AnudeepK
   *
   * @description: Check Expiry Date for DIMS Migrated Policies
   * @create time: 3:23 PM 11/14/2020
   * @return:
   */

  public function changeExpiryDateDIMS_TDIC() {
    if(glLine.Branch.PeriodStart != glLine.Branch.EditEffectiveDate.FirstDayOfMonth){
      if ((glLine.Branch.Job typeis Renewal && glLine.Branch.isDIMSPolicy_TDIC  && !glLine.GLNewDentistDiscount_TDICExists) && glLine.Branch.Offering.CodeIdentifier != "PLCyberLiab_TDIC")
      {
        glLine.Branch.TermType = TermType.TC_OTHER
        var periodEnd = glLine.Branch.PeriodEnd
        var normalizedPeriodEnd = glLine.Branch.EditEffectiveDate.addMonths(12).FirstDayOfMonth
        if (periodEnd != normalizedPeriodEnd) {
          glLine.Branch.PeriodEnd = normalizedPeriodEnd
          Context.Result.addWarning(glLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.GLLine.Validation.ExpiryDate.Warning", glLine.Branch.PeriodEnd.ShortFormat))
        }
      }
    }
  }


  /**
   * create by: SanjanaS
   *
   * @description: validate Coverage Extension Endorsement is selected when Service Members Civil Relief Act is selected
   * @create time: 11:00 AM 03/02/2020
   * @return:
   */
  public function validateCovExtEndorsement_TDIC(line : GLLine) {
    if (line.Branch.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and line.GLServiceMembersCRADiscount_TDICExists and !line.GLCovExtDiscount_TDICExists) {
      Context.Result.addError(line, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.GLLine.Validation.CoverageExtEndorsement.Error"))
    }
  }
}



