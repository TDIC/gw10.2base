package gw.lob.gl.rating

uses entity.windowed.GeneralLiabilityCovVersionList
uses gw.api.effdate.EffDatedUtil
uses gw.financials.PolicyPeriodFXRateCache
uses gw.pl.persistence.core.Key
uses gw.pl.persistence.core.effdate.EffDatedVersionList

/**
 * Created with IntelliJ IDEA.
 * User: AnkitaG
 * Date: 10/21/2019
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
class GLCovCostData_TDIC extends GLCostData<GLCovCost_TDIC> {
  var _covID : Key

  construct(effDate : Date, expDate : Date, __state : Jurisdiction, covID : Key) {
    super(effDate, expDate, __state,null, null)
    assertKeyType(covID, GeneralLiabilityCov)
    init(covID)
  }

  construct(effDate : Date, expDate : Date, c : Currency, rateCache : PolicyPeriodFXRateCache, __state : Jurisdiction,
            covID : Key,  basisScalable : boolean, __subline : GLCostSubline, __splitType : GLCostSplitType) {
    super(effDate, expDate, c, rateCache, __state, __subline, __splitType)
    assertKeyType(covID, GeneralLiabilityCov)
    init(covID)
  }

  construct(cost : GLCovCost_TDIC) {
    super(cost)
    init(cost.GeneralLiabilityCoverage.FixedId)
  }

  construct(cost : GLCovCost_TDIC, rateCache : PolicyPeriodFXRateCache) {
    super(cost, rateCache)
    init(cost.GeneralLiabilityCoverage.FixedId)
  }

  private function init(covID : Key) {
    _covID = covID
  }

  override function setSpecificFieldsOnCost(line : GeneralLiabilityLine, costEntity: GLCovCost_TDIC ) : void {
    super.setSpecificFieldsOnCost(line, costEntity)
    costEntity.setFieldValue("GeneralLiabilityCoverage", _covID)
  }
    override function getVersionedCosts(line : GeneralLiabilityLine) : List<EffDatedVersionList> {
    var glCovVL = EffDatedUtil.createVersionList(line.Branch, _covID) as GeneralLiabilityCovVersionList
    return glCovVL.GLCovCosts.where(\ v -> v.AllVersions.first() typeis GLCovCost_TDIC).toList()
  }


  override function toString() : String {
    return "Cov: ${_covID}"
        + "StartDate: ${EffectiveDate} EndDate: ${ExpirationDate}"
  }

  override property get GLKeyValues() : List<Object> {
    return {_covID}
  }

}