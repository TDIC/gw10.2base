package gw.lob.wc7.availability

uses java.math.BigDecimal

enhancement WC7EmpLiabLimitEnhancement : productmodel.PackageWC7EmpLiabLimitType {

  function policyDiseaseLimitIsGreater(value : java.util.List<gw.api.productmodel.PackageTerm>) : boolean {
    var packValue = value.firstWhere(\pv -> pv.getRestrictionModel() == CovTermModelRest.TC_BID).getValue()
    var limitIsGreater = true
    var policyLimit = this.WC7WorkersCompEmpLiabInsurancePolicyACov.WC7EmpLiabPolicyLimitTerm.Value
    if ((policyLimit != null) and (value != null)) {
      limitIsGreater = (policyLimit >= packValue)
    }
    return limitIsGreater
  }
}
