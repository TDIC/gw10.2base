package gw.lob.gl

uses entity.AccountContact
uses entity.Contact
uses gw.api.domain.Schedule
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses gw.lob.gl.schedule.GLScheduleHelper
uses gw.plugin.Plugins
uses gw.plugin.contact.IContactConfigPlugin

enhancement GeneralLiabilityLineEnhancement : GeneralLiabilityLine
{
  property get GLExposuresWM(): GLExposure[] {
    var exposures = this.VersionList.Exposures.map( \ g -> g.AllVersions.first() )
    return exposures.toTypedArray()
  }
  
  property get AllGLExposuresWM() : List<entity.GLExposure> {
    return this.VersionList.Exposures.flatMap(\e -> e.AllVersions)
  }
  
  function addExposureWM() : GLExposure {
    var eu = new GLExposure(this.Branch)
    eu.GLLine = this
    eu.Location = this.Branch.PolicyLocations?.first()
    return eu.VersionList.AllVersions.single()
  }

  function getPremiumModificationConditionCategories() : String[] {
    return new String[]{"zodg0qlpmuo90eqnfc1es7lko68"}  // for Discount Conditions
  }

  function getAdditionalCoverageCategories() : String[] {
    return new String[]{"GLPollutionAll",
                        "GLLiquorAll",
                        "GLContractualAll",
                        "GLOther",
                        "GLEmployment",
                        "GLY2K",
                        "GLDesignated", 
                        "GLClaimsMade",
                        "GLProfessionalEO",
                        "GLDiscountsCat_TDIC"}
  }
  
  function addExposureWM(location : PolicyLocation, classCode : GLClassCode, basis : Integer) : GLExposure{
    var newExposure = this.addExposureWM()
    newExposure.LocationWM = location.Unsliced
    newExposure.ClassCode = classCode
    newExposure.BasisAmount = basis
    return newExposure
  }

  property get GLTransactions() : GLTransaction[] {
    var branch = this.Branch
    return branch.getSlice(branch.PeriodStart).GLTransactions
  }

  /**
   * An array of schedule coverages with cov terms on the line.
   */
  property get GLScheduleCovsWithCovTerms() : Schedule[] {
    return GLScheduleHelper.filterScheduleCovsWithCovTerms(this.CoveragesFromCoverable)
  }
  
  /**
   * An array of coverages and schedule coverages with no cov terms on the line
   */
  property get GLLineCoveragesAndScheduleCovsWithNoCovTerms() : Coverage[] {
    return GLScheduleHelper.filterCoveragesAndScheduleCovsWithNoCovTerms(this.CoveragesFromCoverable)
  }

  /**
   * Returns a map of State to an array of GLExposures sorted by class code and then effective date
   */
  property get ExposuresByState() : java.util.Map<State, GLExposure[]> {
    return this.VersionList.Exposures
        .flatMap( \ g -> g.AllVersions )
        .partition( \ g -> g.Location.State )
        .mapValues( \ i -> i.orderBy( \ g -> g.ClassCode.Code ).thenBy(\g -> g.EffectiveDate).toTypedArray() )
  }
  /**
   * create by: ChitraK
   * @description: Create and add Locum Tenes new Schedule.
   * @create time: 9:06 PM 7/18/2019
   * @return:
   */

  function createAndAddLocumTenesSched_TDIC() : GLLocumTenensSched_TDIC {
    var item = new GLLocumTenensSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.LTEffectiveDate = this.Branch.EditEffectiveDate
    this.addToGLLocumTenensSched_TDIC(item)
    return item
  }

  /**
   * create by: ChitraK
   * @description: Create and add Special Event new Schedule.
   * @create time: 9:06 PM 7/18/2019
   * @return:
   */

  function createAndAddSpecialEventSched_TDIC() : GLSpecialEventSched_TDIC {
    var item = new GLSpecialEventSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.EventStartDate = this.Branch.EditEffectiveDate
    this.addToGLSpecialEventSched_TDIC(item)
    return item
  }
  /**
   * create by: ChitraK
   * @description: Create and add School Services new Schedule.
   * @create time: 9:05 PM 7/18/2019
   * @return:
   */
  function createAndAddSchoolServSched_TDIC() : GLSchoolServSched_TDIC {
    var item = new GLSchoolServSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.EventStartDate = this.Branch.EditEffectiveDate
    this.addToGLSchoolServSched_TDIC(item)
    return item
  }
  /**
   * @description: Create and add Risk Mangement Schedule.
   * @create time: 7:05 PM 12/21/2019
   * @return:
   */
  function createAndAddRiskManagement_TDIC() : RiskManagementDisc_TDIC {
    var item = new RiskManagementDisc_TDIC(this.Branch)
    item.IsUserEntry = true
    item.GeneralLiabilityLine = this
    if(item.Branch.Job typeis Submission) {
      item.StartDate = this.Branch.EditEffectiveDate
      item.EndDate = this.Branch.EditEffectiveDate.addYears(1)
    }if(item.Branch.Job typeis Renewal) {
      item.StartDate = this.Branch.EditEffectiveDate
      item.EndDate = this.Branch.EditEffectiveDate.addYears(2)
    }
    this.addToRiskManagementDisc_TDIC(item)
    return item
  }
  /**
   * create by: ChitraK
   * @description: Create and add PL Addtl Insured new Schedule.
   * @create time: 9:05 PM 7/18/2019
   * @return:
   */

  function createAndAddPLAISched_TDIC() : GLAdditionInsdSched_TDIC {
    var item = new GLAdditionInsdSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.LTEffectiveDate = this.Branch.EditEffectiveDate
    this.addToGLAdditionInsdSched_TDIC(item)
    return item
  }

  /**
   * create by: SanjanaS
   * @description: Remove PL Addtl Insured new Schedule.
   * @create time: 9:50 PM 5/22/2019
   * @return:
   */
  function toRemoveFromPLAISched_TDIC(scheduledItem: GLAdditionInsdSched_TDIC){
    if(scheduledItem.BasedOn == null or this.AssociatedPolicyPeriod.Job typeis Renewal){
      this.removeFromGLAdditionInsdSched_TDIC(scheduledItem)
    }
    else{
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.GLAdditionalCoverages.Error"))
    }
  }

 /**
 * create by: ChitraK
 * @description: Create and add Name on the Door new Schedule.
 * @create time: 9:03 PM 7/18/2019
 * @return:GLNameODSched_TDIC
 */

  function createAndAddNameODSched_TDIC() : GLNameODSched_TDIC {
    var item = new GLNameODSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.LTEffectiveDate = this.Branch.EditEffectiveDate
    item.LTExpirationDate = item.LTEffectiveDate.addYears(1)
    this.addToGLNameODSched_TDIC(item)
    return item
  }

  /**
   * create by: ChitraK
   * @description: Create and add Mobile DC new Schedule.
   * @create time: 8:58 PM 7/18/2019
   * @return:GLMobileDCSched_TDIC
   */

  function createAndAddMobileDCSched_TDIC() : GLMobileDCSched_TDIC {
    var item = new GLMobileDCSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.LTEffectiveDate = this.Branch.EditEffectiveDate
    this.addToGLMobileDCSched_TDIC(item)
    return item
  }

  /**
   * create by: SanjanaS
   * @description: Remove Mobile DC new Schedule.
   * @create time: 9:50 PM 5/22/2019
   * @return:
   */
  function toRemoveFromMobileDCSched_TDIC(scheduledItem: GLMobileDCSched_TDIC){
    if(scheduledItem.BasedOn == null or this.AssociatedPolicyPeriod.Job typeis Renewal){
      this.removeFromGLMobileDCSched_TDIC(scheduledItem)
    }
    else{
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.GLAdditionalCoverages.Error"))
    }
  }

  /**
   * create by: ChitraK
   * @description: Create and add State Excl new Schedule.
   * @create time: 8:58 PM 7/18/2019
   * @return:GLStateExclSched_TDIC
   */

  function createAndAddStateExclSched_TDIC() : GLStateExclSched_TDIC {
    var item = new GLStateExclSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.LTEffectiveDate = this.Branch.EditEffectiveDate
    this.addToGLStateExclSched_TDIC(item)
    return item
  }

  /**
   * create by: SanjanaS
   * @description: Remove State Excl new Schedule.
   * @create time: 9:50 PM 5/22/2019
   * @return:
   */
  function toRemoveFromStateExclSched_TDIC(scheduledItem: GLStateExclSched_TDIC){
    if(scheduledItem.BasedOn == null or this.AssociatedPolicyPeriod.Job typeis Renewal){
      this.removeFromGLStateExclSched_TDIC(scheduledItem)
    }
    else{
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.GLAdditionalCoverages.Error"))
    }
  }

  function createAndAddExcludedServiceSched_TDIC() : GLExcludedServiceSched_TDIC {
    var item = new GLExcludedServiceSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.LTEffecticeDate = this.Branch.EditEffectiveDate
    this.addToGLExcludedServiceSched_TDIC(item)
    return item
  }

  function toRemoveFromExcludedServiceSched_TDIC(scheduledItem: GLExcludedServiceSched_TDIC){
    if(scheduledItem.BasedOn == null){
      this.removeFromGLExcludedServiceSched_TDIC(scheduledItem)
    }
    else{
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.GLAdditionalCoverages.Error"))
    }
  }




  /**
   * create by: ChitraK
   * @description: Create and add Dental BL AI new Schedule.
   * @create time: 8:49 PM 7/18/2019
   * @return: GLDentalBLAISched_TDIC
   */

  function createAndAddDentalBLAISched_TDIC() : GLDentalBLAISched_TDIC {
    var item = new GLDentalBLAISched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.LTEffectiveDate = this.Branch.EditEffectiveDate
    this.addToGLDentalBLAISched_TDIC(item)
    return item
  }

  /**
   * create by: SanjanaS
   * @description: Remove Dental BL AI new Schedule.
   * @create time: 9:50 PM 5/22/2019
   * @return:
   */
  function toRemoveFromGLDentalBLAISched_TDIC(scheduledItem: GLDentalBLAISched_TDIC){
    if(scheduledItem.BasedOn == null or this.AssociatedPolicyPeriod.Job typeis Renewal){
      this.removeFromGLDentalBLAISched_TDIC(scheduledItem)
    }
    else{
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.GLAdditionalCoverages.Error"))
    }
  }

  /**
   * create by: ChitraK
   * @description: Create and add Cert of Ins new Schedule.
   * @create time: 8:50 PM 7/18/2019
   * @return: GLCertofInsSched_TDIC
   */

  function createAndAddCertofInsSched_TDIC() : GLCertofInsSched_TDIC {
    var item = new GLCertofInsSched_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.LTEffectiveDate = this.Branch.EditEffectiveDate
    this.addToGLCertofInsSched_TDIC(item)
    return item
  }

  /**
   * create by: SanjanaS
   * @description: Remove Cert of Ins new Schedule.
   * @create time: 9:50 PM 5/22/2019
   * @return:
   */
  function toRemoveFromGLCertofInsSched_TDIC(scheduledItem: GLCertofInsSched_TDIC){
      if(scheduledItem.BasedOn == null or this.AssociatedPolicyPeriod.Job typeis Renewal){
          this.removeFromGLCertofInsSched_TDIC(scheduledItem)
      }
    else{
        throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.GLAdditionalCoverages.Error"))
      }
  }

  /**
   * create by: ChitraK
   * @description: Check Incorrect Limits for Coverage A
   * @create time: 9:15 PM 7/18/2019
   * @return:
   */
   function checkCovALimits_TDIC() {
    if(this.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value == 500000 || this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value == 500000){
      var opt1500=this.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.Pattern.getCovTermOpt("1500")
        this.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.setOptionValue(opt1500)
    }
    if((this.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value == 1000000 || this.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value == 3000000) ||
        (this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value == 1000000 || this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value  == 3000000)) {
      var opt3000=this.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.Pattern.getCovTermOpt("3000")
      this.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.setOptionValue(opt3000)

    }
    if(this.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value == 1500000 || this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value == 1500000){
      var opt4500 = this.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.Pattern.getCovTermOpt("4500")
      this.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.setOptionValue(opt4500)
    }
    if(this.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value == 5000000 || this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value == 5000000){
      var opt5000 = this.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.Pattern.getCovTermOpt("5000")
      this.GLAggLimit_TDIC.GLAggLimitAB_TDICTerm.setOptionValue(opt5000)

    }
  }

  function checkAggLimitCovA_TDIC() {
    if(this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 500000 || this?.GLDentistProfLiabCov_TDIC?.GLDPLPerOccLimit_TDICTerm?.Value == 500000){
      var opt1500=this?.GLAggLimit_A_TDIC?.GLAggLimitCovTerm_A_TDICTerm?.Pattern?.getCovTermOpt("1500")
      this?.GLAggLimit_A_TDIC?.GLAggLimitCovTerm_A_TDICTerm?.setOptionValue(opt1500)
    }
    if((this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 1000000 || this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 3000000) ||
        (this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value == 1000000 || this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value  == 3000000)){
      var opt3000=this?.GLAggLimit_A_TDIC?.GLAggLimitCovTerm_A_TDICTerm?.Pattern?.getCovTermOpt("3000")
      this?.GLAggLimit_A_TDIC?.GLAggLimitCovTerm_A_TDICTerm?.setOptionValue(opt3000)
    }
    if(this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 1500000 || this?.GLDentistProfLiabCov_TDIC?.GLDPLPerOccLimit_TDICTerm?.Value == 1500000){
      var opt4500 = this?.GLAggLimit_A_TDIC?.GLAggLimitCovTerm_A_TDICTerm?.Pattern?.getCovTermOpt("4500")
      this?.GLAggLimit_A_TDIC?.GLAggLimitCovTerm_A_TDICTerm?.setOptionValue(opt4500)
    }
    if(this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 5000000 || this?.GLDentistProfLiabCov_TDIC?.GLDPLPerOccLimit_TDICTerm?.Value == 5000000){
      var opt5000 = this?.GLAggLimit_A_TDIC?.GLAggLimitCovTerm_A_TDICTerm?.Pattern?.getCovTermOpt("5000")
      this?.GLAggLimit_A_TDIC?.GLAggLimitCovTerm_A_TDICTerm?.setOptionValue(opt5000)
    }
  }

  function checkAggLimitCovB_TDIC() {
    if(this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 500000 || this?.GLDentistProfLiabCov_TDIC?.GLDPLPerOccLimit_TDICTerm?.Value == 500000){
      var opt1500=this?.GLAggLimit_B_TDIC?.GLAggLimitCovTerm_B_TDICTerm?.Pattern?.getCovTermOpt("1500")
      this?.GLAggLimit_B_TDIC?.GLAggLimitCovTerm_B_TDICTerm?.setOptionValue(opt1500)
    }
    if((this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 1000000 || this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 3000000) ||
        (this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value == 1000000 || this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value  == 3000000)){
      var opt3000=this?.GLAggLimit_B_TDIC?.GLAggLimitCovTerm_B_TDICTerm?.Pattern?.getCovTermOpt("3000")
      this?.GLAggLimit_B_TDIC?.GLAggLimitCovTerm_B_TDICTerm?.setOptionValue(opt3000)
    }
    if(this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 1500000 || this?.GLDentistProfLiabCov_TDIC?.GLDPLPerOccLimit_TDICTerm?.Value == 1500000){
      var opt4500 = this?.GLAggLimit_B_TDIC?.GLAggLimitCovTerm_B_TDICTerm?.Pattern?.getCovTermOpt("4500")
      this?.GLAggLimit_B_TDIC?.GLAggLimitCovTerm_B_TDICTerm?.setOptionValue(opt4500)
    }
    if(this?.GLDentistProfLiabCov_TDIC?.GLDPLPerClaimLimit_TDICTerm?.Value == 5000000 || this?.GLDentistProfLiabCov_TDIC?.GLDPLPerOccLimit_TDICTerm?.Value == 5000000){
      var opt5000 = this?.GLAggLimit_B_TDIC?.GLAggLimitCovTerm_B_TDICTerm?.Pattern?.getCovTermOpt("5000")
      this?.GLAggLimit_B_TDIC?.GLAggLimitCovTerm_B_TDICTerm?.setOptionValue(opt5000)
    }
  }
  /**
   * create by: ChitraK
   * @description: please add description
   * @create time: 6:00 PM 9/19/2019
   * @return:
   */

  function setCovBLimits_TDIC(){
    if(this.GLDentistProfLiabCov_TDICExists && this.GLDentalBusinessLiabCov_TDICExists){
      if(this.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value != null ) this.GLDentalBusinessLiabCov_TDIC.GLDBLPerOccLimit_TDICTerm.setValue(this.GLDentistProfLiabCov_TDIC.GLDPLPerClaimLimit_TDICTerm.Value)
      if(this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value != null) this.GLDentalBusinessLiabCov_TDIC.GLDBLPerOccLimit_TDICTerm.setValue(this.GLDentistProfLiabCov_TDIC.GLDPLPerOccLimit_TDICTerm.Value)
    }

  }
  /**
   * create by: ChitraK
   * @description: Set Cyber Deductible Limit
   * @create time: 11:33 AM 9/20/2019
   * @return:
   */
  function setCybDeductibleLimit_TDIC() {
    if (this.GLCyberLiabCov_TDICExists) {
      this.cyberUWQuestionsVisible_TDIC()
      switch (this.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value){
        case 50000 :
          this.GLCyberLiabDedCov_TDIC.GLCybLiabDedLimit_TDICTerm.setValue(1000)
          break;
        case 100000 :
          this.GLCyberLiabDedCov_TDIC.GLCybLiabDedLimit_TDICTerm.setValue(1000)
          break;
        case 250000 :
          this.GLCyberLiabDedCov_TDIC.GLCybLiabDedLimit_TDICTerm.setValue(2500)
          break;

      }
    }
  }
  /**
   * create by: ChitraK
   * @description: set NJ Aggregate Limits
   * @create time: 11:34 AM 9/20/2019
   * @return:
   */

  function setNJEndorsementLimits_TDIC(){
    if(this.GLNJDeductibleCov_TDICExists){
      if(this.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 5000) {
        var opt15 = this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Pattern.getCovTermOpt("15")
        this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.setOptionValue(opt15)
      }
      if(this.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 6000) {
        var opt18 = this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Pattern.getCovTermOpt("18")
        this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.setOptionValue(opt18)
      }
      if(this.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 7000) {
        var opt21 = this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Pattern.getCovTermOpt("21")
        this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.setOptionValue(opt21)
      }
      if(this.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 8000) {
        var opt24 = this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Pattern.getCovTermOpt("24")
        this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.setOptionValue(opt24)
      }
      if(this.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 9000) {
        var opt36 = this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Pattern.getCovTermOpt("36")
        this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.setOptionValue(opt36)
      }
      if(this.GLNJDeductibleCov_TDIC.GLNJDedPerClaimLimit_TDICTerm.Value == 99) {
        var opt99 = this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.Pattern.getCovTermOpt("99")
        this.GLNJDeductibleCov_TDIC.GLNJDedAggLimit_TDICTerm.setOptionValue(opt99)
      }
    }
  }
  /**
   * create by: SureshB
   * @description: method to add contact of type PolicyCertificateHolder_TDIC
   * @create time: 12:41 PM 10/09/2019
   * @param : ContactType
   * @return: PolicyCertificateHolder_TDIC
   */
  function addNewPolicyCertificateHolderOfContactType_TDIC(contactType : ContactType) : PolicyCertificateHolder_TDIC {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return addPolicyCertificateHolder_TDIC(acctContact.Contact)
  }
  /**
   * create by: SureshB
   * @description: method to add contact of type PolicyCertificateHolder_TDIC
   * @create time: 12:42 PM 10/09/2019
   * @param : ContactType
   * @return: PolicyCertificateHolder_TDIC
   */
  function addPolicyCertificateHolder_TDIC(contact : Contact) : PolicyCertificateHolder_TDIC {
    if (this.GLCertificateHolders_TDIC.firstWhere(\ p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("TDIC.Web.Contact.CertificateHolder.Error.AlreadyExists", contact))
    }
    var certifiateHolder = this.Branch.addNewPolicyContactRoleForContact(contact, typekey.PolicyContactRole.TC_POLICYCERTIFICATEHOLDER_TDIC) as PolicyCertificateHolder_TDIC
    this.addToGLCertificateHolders_TDIC(certifiateHolder)
    return certifiateHolder
  }
  /**
   * create by: SureshB
   * @description: property to get unassigned PolicyCertificateHolder_TDIC contacts
   * @create time: 12:44 PM 10/09/2019
   * @param null
   * @return: AccountContact[]
   */
  property get UnassignedPolicyCertificateHolder_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(typekey.PolicyContactRole.TC_POLICYCERTIFICATEHOLDER_TDIC)
    var assignedLossPayees = this.GLCertificateHolders_TDIC.map(\certficateHolder -> certficateHolder.AccountContactRole.AccountContact)
    return this.Branch.Policy.Account
        .getAccountContactsWithRole(accountContactRoleType)
        .subtract(assignedLossPayees)
        .toTypedArray()
  }

  property get ExistingCertificateHolder_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYCERTIFICATEHOLDER_TDIC)
    return this.Branch.Policy.Account.getAccountContactsWithRole(accountContactRoleType)
  }

  /**
   * create by: SureshB
   * @description: property to get contacts other than type PolicyLossPayee_TDIC
   * @create time: 12:47 PM 10/09/2019
   * @param null
   * @return: AccountContact[]
   */
  property get CertificateHolderOtherCandidates_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYCERTIFICATEHOLDER_TDIC)
    return this.Branch.Policy.Account.ActiveAccountContacts
        .where(\acr -> plugin.canBeRole(acr.ContactType, accountContactRoleType) and not acr.hasRole(accountContactRoleType))
  }

  /*
   * create by: SureshB
   * @description: method to get the boolean answer of the GL UW question
   * @create time: 8:47 PM 10/16/2019
    * @param questionCode as String
   * @return: PCAnswerDelegate
   */

  function getAnswerFromGLUWQuestionIfQuestionVisible_TDIC(passedInQuestionCode : String) : entity.PCAnswerDelegate {
    var tmpAnswer : entity.PCAnswerDelegate = null
    var tmpQuestion : gw.api.productmodel.Question
    var tmpQuestionSet : gw.api.productmodel.QuestionSet = this.Branch.Policy.Product.getQuestionSetByCodeIdentifier("GLUnderwriting")
    tmpQuestion = tmpQuestionSet.getQuestionByCodeIdentifier(passedInQuestionCode)
    if (this.hasUnderwritingQuestions_TDIC()) {
      if (tmpQuestion != null) {
        if (tmpQuestion.isQuestionVisible(this)) {
          tmpAnswer = this.getAnswer(tmpQuestion)
        }
      } else if (tmpQuestion == null) {
        //FIXME - question not found, log a warning?  May not be an error?
      }
    }
    return tmpAnswer
  }
  /*
   * create by: SureshB
   * @description: method to check if the PolicyLocation has UW questions
   * @create time: 8:47 PM 10/16/2019
    * @param null
   * @return: boolean
   */

  function hasUnderwritingQuestions_TDIC() : boolean {
    return this.Branch.Policy.Product.getAvailableQuestionSetsByType(QuestionSetType.TC_UNDERWRITING, this).Count > 0
  }
  /*
   * create by: SureshB
   * @description: method to get the GLUW question boolean answer
   * @create time: 8:58 PM 10/16/2019
    * @param question codeidentifier
   * @return: boolean
   */

  function getAnswerForGLUWQuestion_TDIC(questionCode : String) : boolean {
    var answerDelegate = getAnswerFromGLUWQuestionIfQuestionVisible_TDIC(questionCode)
    if (answerDelegate != null and answerDelegate.hasAnswer()) {
      return answerDelegate.BooleanAnswer
    } else {
      return false
    }
  }
  /*
   * create by: SureshB
   * @description: methot to add the GLDentLicenseStates_TDIC
   * @create time: 9:28 PM 10/16/2019
    * @param null
   * @return: GLDentLicenseStates_TDIC
   */

  function createAndAddGLDentLicenseStates_TDIC() : GLDentLicenseStates_TDIC {
    var item = new GLDentLicenseStates_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    this.addToGLDentLicenseStates_TDIC(item)
    return item
  }
/*
 * create by: SureshB
 * @description: method to create and add a new entry to GLDentistServices_TDIC
 * @create time: 10:20 PM 10/21/2019
  * @param null
 * @return: GLDentistServices_TDIC
 */

  function createAndAddGLDentistServices_TDIC() : GLDentistServices_TDIC {
    var item = new GLDentistServices_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    this.addToGLDentistServices_TDIC(item)
    return item
  }
/*
 * create by: SureshB
 * @description: method to get the DentistServices
 * @create time: 10:20 PM 10/21/2019
  * @param null
 * @return: DentistServices_TDIC[]
 */

  property get GLDentistServicesOfLine_TDIC() : DentistServices_TDIC[] {
    if(this.GLDentistServices_TDIC.Count > 0){
      return this.GLDentistServices_TDIC*.DentistService
    }
    return {}
  }
/*
 * create by: SureshB
 * @description: method to set the DentisServices
 * @create time: 10:19 PM 10/21/2019
  * @param DentistServices_TDIC[]
 * @return:
 */

  property set GLDentistServicesOfLine_TDIC(dentistServices : DentistServices_TDIC[]) {
    if (dentistServices.contains(typekey.DentistServices_TDIC.TC_NONE)) {
      for (dentistService in DentistServices_TDIC.AllTypeKeys) {
        if (dentistService != typekey.DentistServices_TDIC.TC_NONE) {
          this.removeFromGLDentistServices_TDIC(this.GLDentistServices_TDIC.firstWhere(\elt -> elt.DentistService == dentistService))
        }
      }
      if (!this.GLDentistServices_TDIC?.hasMatch(\elt1 -> elt1.DentistService == typekey.DentistServices_TDIC.TC_NONE)) {
        createAndAddGLDentistServices_TDIC().DentistService = typekey.DentistServices_TDIC.TC_NONE
      }
    } else {
      for (dentistService in DentistServices_TDIC.AllTypeKeys) {
        if (!dentistServices.contains(dentistService)) {
          this.removeFromGLDentistServices_TDIC(this.GLDentistServices_TDIC.firstWhere(\elt -> elt.DentistService == dentistService))
        }
      }
      for (addedDentistService in dentistServices) {
        if (!this.GLDentistServices_TDIC?.hasMatch(\elt1 -> elt1.DentistService == addedDentistService)) {
          createAndAddGLDentistServices_TDIC().DentistService = addedDentistService
        }
      }
    }
  }
  /*
   * create by: SureshB
   * @description: method to create and add a new entry to the GLAnestheticModalities_TDIC
   * @create time: 10:19 PM 10/21/2019
    * @param null
   * @return: GLAnestheticModalities_TDIC
   */

  function createAndAddGLAnestheticModalities_TDIC() : GLAnestheticModalities_TDIC {
    var item = new GLAnestheticModalities_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    this.addToGLAnestheticModalities_TDIC(item)
    return item
  }
/*
 * create by: SureshB
 * @description: method to get the Anesthetic Modalities
 * @create time: 10:18 PM 10/21/2019
  * @param null
 * @return: AnestheticModilities_TDIC[]
 */

  property get GLAnestheticModalitiesOfLine_TDIC() : AnestheticModilities_TDIC[] {
    if(this.GLAnestheticModalities_TDIC.Count > 0){
      return this.GLAnestheticModalities_TDIC*.AnestheticModality
    }
    return {}
  }
/*
 * create by: SureshB
 * @description: method to set the Anesthetic Modalities
 * @create time: 10:17 PM 10/21/2019
  * @param  * @return: AnestheticModilities_TDIC[]
  * @return:
 */

  property set GLAnestheticModalitiesOfLine_TDIC(anestheticModalities : AnestheticModilities_TDIC[]) {
    if (anestheticModalities.contains(typekey.AnestheticModilities_TDIC.TC_NONE)) {
      for (anestheticModality in AnestheticModilities_TDIC.AllTypeKeys) {
        if (anestheticModality != typekey.AnestheticModilities_TDIC.TC_NONE) {
          this.removeFromGLAnestheticModalities_TDIC(this.GLAnestheticModalities_TDIC.firstWhere(\elt -> elt.AnestheticModality == anestheticModality))
        }
      }
      if (!this.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == typekey.AnestheticModilities_TDIC.TC_NONE)) {
        createAndAddGLAnestheticModalities_TDIC().AnestheticModality = typekey.AnestheticModilities_TDIC.TC_NONE
      }
    } else {
      for (anestheticModality in AnestheticModilities_TDIC.AllTypeKeys) {
        if (!anestheticModalities.contains(anestheticModality) and this.GLAnestheticModalities_TDIC.hasMatch(\elt1 -> elt1.AnestheticModality == anestheticModality)) {
          this.removeFromGLAnestheticModalities_TDIC(this.GLAnestheticModalities_TDIC.firstWhere(\elt -> elt.AnestheticModality == anestheticModality))
        }
      }
      for (addedAnestheticModelity in anestheticModalities) {
        if (!this.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == addedAnestheticModelity)) {
          createAndAddGLAnestheticModalities_TDIC().AnestheticModality = addedAnestheticModelity
        }
      }
    }
  }
/*
 * create by: SureshB
 * @description: method to add IVIMSedationInHospital_TDIC to GLLine
 * @create time: 5:34 PM 10/22/2019
  * @param null
 * @return: IVIMSedationInHospital_TDIC
 */

  function createAndAddGLIVIMSedationInHospital_TDIC() : IVIMSedationInHospital_TDIC {
    var item = new IVIMSedationInHospital_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    this.addToGLIVIMSedationInHospital_TDIC(item)
    return item
  }
  /*
 * create by: SureshB
 * @description: method to add IVIMSedationByApplicant_TDIC to GLLine
 * @create time: 5:34 PM 10/22/2019
  * @param null
 * @return: IVIMSedationByApplicant_TDIC
 */

  function createAndAddGLIVIMSedationByApplicant_TDIC() : IVIMSedationByApplicant_TDIC {
    var item = new IVIMSedationByApplicant_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    this.addToGLIVIMSedationByApplicant_TDIC(item)
    return item
  }
  /*
 * create by: SureshB
 * @description: method to add GeneralAnesthesiaInOffice_TDIC to GLLine
 * @create time: 5:34 PM 10/22/2019
  * @param null
 * @return: GeneralAnesthesiaInOffice_TDIC
 */

  function createAndAddGLGeneralAnesthesiaInOffice_TDIC() : GeneralAnesthesiaInOffice_TDIC {
    var item = new GeneralAnesthesiaInOffice_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    this.addToGLGeneralAnesthesiaInOffice_TDIC(item)
    return item
  }
  /*
   * create by: SureshB
   * @description: method to set the GLAnesthetic Modalities typekey values
   * @create time: 6:32 PM 10/22/2019
    * @param AnestheticModilities_TDIC, boolean
   * @return:
   */

  function setGLAnestheticModalitiesTypeKey_TDIC(key : AnestheticModilities_TDIC, checked : boolean) {
      if (checked) {
        if (key == typekey.AnestheticModilities_TDIC.TC_NONE) {
          for (anestheticModality in AnestheticModilities_TDIC.AllTypeKeys) {
            if (anestheticModality != key) {
              this.removeFromGLAnestheticModalities_TDIC(this.GLAnestheticModalities_TDIC.firstWhere(\elt -> elt.AnestheticModality == anestheticModality))
            }
          }
          if (!this.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == key)) {
            createAndAddGLAnestheticModalities_TDIC().AnestheticModality = key
          }
        } else {
          if (!this.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == key)) {
            createAndAddGLAnestheticModalities_TDIC().AnestheticModality = key
          }
        }
      } else {
        if (this.GLAnestheticModalities_TDIC?.hasMatch(\elt1 -> elt1.AnestheticModality == key)) {
          this.removeFromGLAnestheticModalities_TDIC(this.GLAnestheticModalities_TDIC.firstWhere(\elt -> elt.AnestheticModality == key))
        }
      }
  }

  function setERECoverageAvailability_TDIC():Boolean{
    if(not (this.Branch.Job typeis PolicyChange)) {
      return false
    }
    return ((this.Branch.BasedOn.Canceled ||
        this.Branch.BasedOn.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code)/* &&
        (!this.Branch.GLLine.GLExtendedReportPeriodCov_TDICExists)*/)
  }

  /*
   * create by: SanjanaS
   * @description: method to set the visibility of ERE tab
   * @create time: 6:32 PM 04/20/2020
   * @param:
   * @return: boolean
   */

  //GPC-4430 changed the Visibility of ERE Tab
  function setERETabVisibility_TDIC() : Boolean {
    if ((this.Branch.Canceled ||
        this.Branch.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code)) {
      var changeReasonExtCov = this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_EREPLOFFER)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_EREPLOFFERNA)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_EREPLOFFERNR)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_EREPLRESCINDED)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_CANCELEPLI)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_ERECYBEROFFER)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_ERECYBERNA)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_ERECYBERNR)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_ERECYBER)==1 ||
          this.Branch.Job.ChangeReasons.countWhere(\elt -> elt.ChangeReason==ChangeReasonType_TDIC.TC_ERECYBERRESCINDED)==1 ||
          this.GLExtendedReportPeriodCov_TDICExists || this.GLCyvSupplementalERECov_TDICExists || this.GLDentalEmpPracLiabCov_TDICExists
      return changeReasonExtCov
    }
    return this.GLExtendedReportPeriodDPLCov_TDICExists
  }

  /**
   * create by: Ramk
   * @description: Create and add GL Manuscript.
   * @create time: 9:06 PM 5/6/2020
   * @return:
   */

  function createAndAddGLManuscript_TDIC() : GLManuscript_TDIC {
    var item = new GLManuscript_TDIC(this.Branch)
    item.GeneralLiabilityLine = this
    item.ManuscriptEffectiveDate = this.Branch.EditEffectiveDate
    this.addToGLManuscript_TDIC(item)
    return item
  }

  /**
   * create by: SanjanaS
   * @description: Method to remove the scheduled items on the coverages that are not present on the policy.
   * @create time: 4:00 PM 5/28/2020
   * @return:
   */
  function removeScheduledItems_TDIC(){
    if(!this.GLMobileDentalClinicCov_TDICExists and this.GLMobileDCSched_TDIC.HasElements){
      this.GLMobileDCSched_TDIC.each(\sched -> {
        this.removeFromGLMobileDCSched_TDIC(sched)
      })
    }
    if(!this.GLLocumTenensCov_TDICExists and this.GLLocumTenensSched_TDIC.HasElements){
      this.GLLocumTenensSched_TDIC.each(\sched -> {
        this.removeFromGLLocumTenensSched_TDIC(sched)
      })
    }
    if(!this.GLSpecialEventCov_TDICExists and this.GLSpecialEventSched_TDIC.HasElements){
      this.GLSpecialEventSched_TDIC.each(\sched -> {
        this.removeFromGLSpecialEventSched_TDIC(sched)
      })
    }
    if(!this.GLSchoolServicesCov_TDICExists and this.GLSchoolServSched_TDIC.HasElements){
      this.GLSchoolServSched_TDIC.each(\sched -> {
        this.removeFromGLSchoolServSched_TDIC(sched)
      })
    }
    if(!this.GLNameOTDCov_TDICExists and this.GLNameODSched_TDIC.HasElements){
      this.GLNameODSched_TDIC.each(\sched -> {
        this.removeFromGLNameODSched_TDIC(sched)
      })
    }
    if(!this.GLStateExclCov_TDICExists and this.GLStateExclSched_TDIC.HasElements){
      this.GLStateExclSched_TDIC.each(\sched -> {
        this.removeFromGLStateExclSched_TDIC(sched)
      })
    }
    if(!this.GLAdditionalInsuredCov_TDICExists and this.GLAdditionInsdSched_TDIC.HasElements){
      this.GLAdditionInsdSched_TDIC.each(\sched -> {
        this.removeFromGLAdditionInsdSched_TDIC(sched)
      })
    }
    if(!this.GLBLAICov_TDICExists and this.GLDentalBLAISched_TDIC.HasElements){
      this.GLDentalBLAISched_TDIC.each(\sched -> {
        this.removeFromGLDentalBLAISched_TDIC(sched)
      })
    }
    if(!this.GLCOICov_TDICExists and this.GLCertofInsSched_TDIC.HasElements){
      this.GLCertofInsSched_TDIC.each(\sched -> {
        this.removeFromGLCertofInsSched_TDIC(sched)
      })
    }
    if(!this.GLManuscriptEndor_TDICExists and this.GLManuscript_TDIC.HasElements){
      this.GLManuscript_TDIC.each(\sched -> {
        this.removeFromGLManuscript_TDIC(sched)
      })
    }
  }

  function addNewGLPolicyEntityOwnerOfContactType_TDIC(contactType : ContactType) : GLPolicyEntityOwner_TDIC {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return addGLPolicyEntityOwner_TDIC(acctContact.Contact)
  }

  function addGLPolicyEntityOwner_TDIC(contact : Contact) : GLPolicyEntityOwner_TDIC {
    if (this.GLPolicyOwnerOfficer_TDIC.firstWhere(\ p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyOwnerOfficer.Error.AlreadyExists", contact))
    }
    var entityOwner = this.Branch.addNewPolicyContactRoleForContact(contact, typekey.PolicyContactRole.TC_GLPOLICYENTITYOWNER_TDIC) as GLPolicyEntityOwner_TDIC
    this.addToGLPolicyOwnerOfficer_TDIC(entityOwner)
    return entityOwner
  }

  function addGLPolicyOwnerOfficer_TDIC(contact : Contact) : GLPolicyOwnerOfficer_TDIC {
    if (this.GLPolicyOwnerOfficer_TDIC.firstWhere(\ p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyOwnerOfficer.Error.AlreadyExists", contact))
    }
    var policyOfficer = this.Branch.addNewPolicyContactRoleForContact(contact, typekey.PolicyContactRole.TC_GLPOLICYOWNEROFFICER_TDIC) as GLPolicyOwnerOfficer_TDIC
    this.addToGLPolicyOwnerOfficer_TDIC(policyOfficer)
    return policyOfficer
  }

  /**
   * All the account Owner Officers that are not already assigned to this policy line.
   */
  property get UnassignedOwnerOfficers_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(typekey.PolicyContactRole.TC_GLPOLICYOWNEROFFICER_TDIC)
    var assignedOwnerOfficers = this.GLPolicyOwnerOfficer_TDIC.map(\ ownerOfficer -> ownerOfficer.AccountContactRole.AccountContact)
    return this.Branch.Policy.Account
        .getAccountContactsWithRole(accountContactRoleType)
        .subtract(assignedOwnerOfficers)
        .toTypedArray()
  }

  function addAllExistingGLPolicyOwnerOfficer_TDIC() : GLPolicyOwnerOfficer_TDIC[] {
    var newBOPPolicyOwnerOfficers = new ArrayList<GLPolicyOwnerOfficer_TDIC>()
    for(ac in this.UnassignedOwnerOfficers_TDIC) {
      newBOPPolicyOwnerOfficers.add(addGLPolicyOwnerOfficer_TDIC(ac.Contact))
    }
    return newBOPPolicyOwnerOfficers.toTypedArray()
  }

  property get UnassignedGLEntityOwners_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(typekey.PolicyContactRole.TC_GLPOLICYENTITYOWNER_TDIC)
    var assignedOwnerOfficers = this.GLPolicyOwnerOfficer_TDIC.map(\ ownerOfficer -> ownerOfficer.AccountContactRole.AccountContact)
    return this.Branch.Policy.Account
        .getAccountContactsWithRole(accountContactRoleType)
        .subtract(assignedOwnerOfficers)
        .toTypedArray()
  }

  function addAllExistingGLPolicyEntityOwners_TDIC() : GLPolicyEntityOwner_TDIC[] {
    var newPolicyEntityOwners = new ArrayList<GLPolicyEntityOwner_TDIC>()
    for(ac in this.UnassignedGLEntityOwners_TDIC) {
      newPolicyEntityOwners.add(addGLPolicyEntityOwner_TDIC(ac.Contact))
    }
    return newPolicyEntityOwners.toTypedArray()
  }

  property get GLOwnerOfficerOtherCandidates_TDIC() : AccountContact[] {
    return otherContactsFor_TDIC(typekey.PolicyContactRole.TC_GLPOLICYOWNEROFFICER_TDIC)
  }

  private function otherContactsFor_TDIC(policyContactRole : typekey.PolicyContactRole) : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(policyContactRole)
    return this.Branch.Policy.Account.ActiveAccountContacts
        .where(\ ac -> plugin.canBeRole(ac.ContactType, accountContactRoleType) and not ac.hasRole(accountContactRoleType))
  }
}