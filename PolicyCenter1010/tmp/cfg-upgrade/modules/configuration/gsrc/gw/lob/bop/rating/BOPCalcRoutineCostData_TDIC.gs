package gw.lob.bop.rating

uses gw.api.rating.flow.VisibleInRateflow
uses gw.rating.flow.domain.CalcRoutineCostData

uses java.math.BigDecimal
uses java.math.RoundingMode


class BOPCalcRoutineCostData_TDIC extends CalcRoutineCostData {
  construct(costData : BOPCostData, defaultRoundingLevel : Integer, defaultRoundingMode : RoundingMode){
    super(costData,defaultRoundingLevel,defaultRoundingMode)
  }

}