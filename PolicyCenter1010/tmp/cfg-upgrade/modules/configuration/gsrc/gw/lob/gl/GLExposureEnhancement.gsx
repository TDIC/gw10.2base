package gw.lob.gl

uses gw.api.locale.DisplayKey
uses gw.api.util.JurisdictionMappingUtil
uses gw.util.GosuStringUtil

enhancement GLExposureEnhancement : entity.GLExposure {

  property get BasisAmount() : Integer {
    return (IsBasisScalable ? this.ScalableBasisAmount : this.FixedBasisAmount)
  }

  property get BasisForRating() : Integer {
    return (this.Branch.Job typeis Audit ? this.AuditedBasis : this.BasisAmount)
  }

  property set BasisAmount(amount : Integer) {
    if (amount < 0 or amount > Integer.MAX_VALUE) {
      throw new gw.api.util.DisplayableException(DisplayKey.get("Java.Validation.Number.Range.Closed", DisplayKey.get("Web.Policy.GL.ExposureUnits.Basis"),
          0, Integer.MAX_VALUE))
    } else {
      if (IsBasisScalable) {
        this.ScalableBasisAmount = amount
      } else {
        this.FixedBasisAmount = amount
      }
    }
    return
  }

  property get EndOfCoverageDate() : Date {
    var d = this.Branch.CancellationDate
    if (d == null) {
      d = this.ExpirationDate
    }
    return d
  }

  property get IsBasisScalable() : boolean {
    // note that null is converted to false
    return this.ClassCode.Basis.Auditable
  }

  property get ClassCode() : GLClassCode {
    return this.getFieldValue("ClassCodeInternal") as GLClassCode
  }

  property set ClassCode(code : GLClassCode) {
    var wasBasisScalable = this.IsBasisScalable
    this.setFieldValue("ClassCodeInternal", code)

    // If we switched from being basis scalable to not, copy the value and then null out the inappropriate field
    if (wasBasisScalable != this.IsBasisScalable) {
      if (wasBasisScalable) {
        this.FixedBasisAmount = this.ScalableBasisAmount
        this.ScalableBasisAmount = null
      } else {
        this.ScalableBasisAmount = this.FixedBasisAmount
        this.FixedBasisAmount = null
      }
    }
  }

  property get NewExposure() : boolean {
    return this.BasedOn == null
  }

  property get LocationWM() : PolicyLocation {
    if (NewExposure) {
      // we have to do this or change location out-of-sequence will show 2 versions of the same
      // location. This should return the latest version of the location because it is unsliced
      return this.Location
    } else {
      return this.getSlice(this.EffectiveDate).Location.LastVersionWM
    }
  }

  property set LocationWM(_location : PolicyLocation) {
    this.assertWindowMode(_location)
    this.Location = _location
    if (_location != null) {
      var exposureDateRange = _location.EffectiveDateRangeWM
          .intersect(_location.Branch.EditEffectiveDateRange)
          .intersect(this.GLLine.EffectiveDateRangeWM)
      this.EffectiveDateRange = exposureDateRange
    }
  }

  function firstMatchingClassCode(code : String) : GLClassCode {
    var effectiveDate = this.GLLine.getReferenceDateForCurrentJob(JurisdictionMappingUtil.getJurisdiction(this.LocationWM))
    var previousCode = (this.GLLine.Branch.Job.NewTerm) ? null : this.BasedOn.ClassCode

    var criteria = new GLClassCodeSearchCriteria()
    criteria.Code = code
    criteria.EffectiveAsOfDate = effectiveDate
    criteria.PreviousSelectedClassCode = previousCode.Code
    return criteria.performSearch().getFirstResult()
  }

  function firstMatchingClassCodeOrThrow(code : String) : GLClassCode {
    var retVal = firstMatchingClassCode(code)
    if (retVal == null) {
      throw new gw.api.util.DisplayableException(DisplayKey.get("Java.ClassCodePickerWidget.InvalidCode", code))
    }
    return retVal
  }
  /*
   * create by: SureshB
   * @description: method to set the GLExposure Description property
   * @create time: 4:43 PM 10/22/2019
    * @param null
   * @return:
   */

  function setGLExposureDescription_TDIC() {
    if (this.SpecialityCode_TDIC != null and this.ClassCode_TDIC != null) {
      var specialityClassCodeComboDescriptions = new HashMap<String, String>(){
          "00generaldentist_tdic11" -> "General Dentist utilizing local, nitrous oxide or oral conscious sedation.",
          "00generaldentist_tdic20" -> "General Dentist utilizing local,nitrous oxide or oral conscious sedation and I.V. or I.M. sedation or general anesthesia when administered in office, hospital or surgi-center by M.D. Anesthesiologist, Dental Anesthesiologist, Certified Registered Nurse Anesthetist (CRNA)  or Oral Surgeon.",
          "00generaldentist_tdic40" -> "General Dentist utilizing local, nitrous oxide or oral conscious sedation. and I.V. or I.M. sedation or general anesthesia administered in office, hospital or surgi-center by M.D. Anesthesiologist, Dental Anesthesiologist, Certified Registered Nurse Anesthetist (CRNA)  or Oral Surgeon. Also includes parenteral (IV IM) sedation when administered by the insured. ",
          "00generaldentist_tdic50" -> "General Dentist using any anesthetic or modality of administration",
          "00generaldentist_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "00generaldentist_tdic02" -> "Licensed – non practicing dentist",
          "10oralsurgery_tdic50" -> "Oral Surgeon using any anesthetic or modality of administration",
          "10oralsurgery_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "10oralsurgery_tdic02" -> "Licensed – non practicing dentist",
          "15endodontics_tdic11" -> "Endodontist using any anesthetic or modality of administration",
          "15endodontics_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "15endodontics_tdic02" -> "Licensed – non practicing dentist",
          "20orthodontics_tdic12" -> "Orthodontist using any anesthetic or modality of administration",
          "20orthodontics_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "20orthodontics_tdic02" -> "Licensed – non practicing dentist",
          "30pediatricdentistry_tdic09" -> "Pediatric Dentist using any anesthetic or modality of administration",
          "30pediatricdentistry_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "30pediatricdentistry_tdic02" -> "Licensed – non practicing dentist",
          "40periodontics_tdic10" -> "Periodontist using any anesthetic or modality of administration",
          "40periodontics_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "40periodontics_tdic02" -> "Licensed – non practicing dentist",
          "50prosthodontics_tdic30" -> "Prosthodontist using any anesthetic or modality of administration",
          "50prosthodontics_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "50prosthodontics_tdic02" -> "Licensed – non practicing dentist",
          "60oralpathology_tdic10" -> "Oral Pathologist using any anesthetic or modality of administration",
          "60oralpathology_tdic14" -> "Oral Pathologist using any anesthetic or modality of administration",
          "60oralpathology_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "60oralpathology_tdic02" -> "Licensed – non practicing dentist",
          "90dentalanesthesiology_tdic50" -> "Dental Anesthesiologist using any anesthetic or modality of administration. Insured may administer the anesthetic and also perform the dental procedure.",
          "90dentalanesthesiology_tdic60" -> "Dental Anesthesiologist using any anesthetic or modality of administration. Classification does not include administration of IV IM or general anesthesia if the insured is also performing the dental procedure.",
          "90dentalanesthesiology_tdic01" -> "Dentist of any specialty who provide volunteer dental services without pay or other compensation in excess of actual expenses. Only local anesthetic, oral conscious sedation and nitrous oxide analgesia may be used.",
          "90dentalanesthesiology_tdic02" -> "Licensed – non practicing dentist"}

      var description = specialityClassCodeComboDescriptions.get(this.SpecialityCode_TDIC.Code.concat(this.ClassCode_TDIC.Code))
      if (description != null and GosuStringUtil.isNotBlank(description)) {
        this.Description_TDIC = description
      }
    } else {
      this.Description_TDIC = ""
    }
  }
}
