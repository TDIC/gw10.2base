package gw.lob.wc7

uses gw.api.productmodel.ClausePattern
uses java.util.Set

interface WC7ClauseClassifier {
  function isSimpleContactSchedule(pattern: ClausePattern) : boolean
  property get SimpleContactScheduleClauses() : Set<ClausePattern>
}