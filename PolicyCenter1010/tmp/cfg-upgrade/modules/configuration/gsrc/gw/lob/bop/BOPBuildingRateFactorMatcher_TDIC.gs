package gw.lob.bop

uses gw.entity.ILinkPropertyInfo
uses gw.lob.common.AbstractRateFactorMatcher

class BOPBuildingRateFactorMatcher_TDIC extends AbstractRateFactorMatcher<BOPBuildRateFactor_TDIC> {
    construct(owner : BOPBuildRateFactor_TDIC) {
    super(owner)
    }

    override property get ParentColumns() : Iterable<ILinkPropertyInfo> {
    return {BOPBuildRateFactor_TDIC.Type.TypeInfo.getProperty("BOPBuildingModifier_TDIC") as ILinkPropertyInfo};
    }
}