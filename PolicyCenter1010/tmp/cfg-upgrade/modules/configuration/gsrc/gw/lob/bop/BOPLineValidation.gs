package gw.lob.bop

uses gw.api.locale.DisplayKey
uses gw.validation.PCValidationContext
uses gw.policy.PolicyLineValidation
uses java.util.Set
uses gw.lob.common.AnswerValidation
uses gw.policy.PolicyAddlInsuredAndTypeUniqueValidation
uses typekey.Job

uses java.lang.UnsupportedOperationException

/**
 * Line level validation for the {@link entity.BusinessOwnersLine BusinesOwnersLine}
 */
@Export
class BOPLineValidation extends PolicyLineValidation<entity.BusinessOwnersLine> {

  /**
   * The {@link entity.BusinessOwnersLine BusinesOwnersLine} being validated.
   */
  property get bopLine() : entity.BusinessOwnersLine {
    return Line
  }

  construct(valContext : PCValidationContext, polLine : entity.BusinessOwnersLine) {
    super(valContext, polLine)
  }

  /**
   * Validate the Business Owners Line.
   *
   * Checks the following:
   * <ul>
   *   <li>Additional Insures are unique</li>
   *   <li>Locations are valid</li>
   *   <li>Answers are valid</li>
   *   <li>Buildings are valid.  {@link gw.lob.bop.BOPBuildingValidation}</li>
   *   <li>Coverages are valid.</li>
   *   <li>Small business type is set.</li>
   * </ul>
   */
  override function doValidate() {
    additionalInsuredAndTypeUnique()
    checkLocations()
    checkAnswers()
    checkBuildings()
    checkCoverages()
    //checkSmallBusinessType()                //Chitra.K: Disabling this validation as this field is removed from UI

    checkBaseStateAndLocationState_TDIC()     // Jeff Lin, Added for GPC-427 below
    checkBaseStateAndPNIState_TDIC()
    checkMultiStateOfThePolicy_TDIC()
    checkLRPPolicyHasPLCoverage_TDIC()
    checkBOPPolicyHasPLCoverage_TDIC()
    validateBOPManuscriptTDIC()
    changeExpiryDateDIMS_TDIC()
  }


  /**
   * Validate all buildings for the given line.
   * @param line a Business Owners line to validate businesses
   */
  static function validateBuildings(line : BOPLine) {
    PCValidationContext.doPageLevelValidation( \ context -> new BOPLineValidation(context, line).checkBuildings())
    PCValidationContext.doPageLevelValidation( \ context -> new BOPLineValidation(context, line).chkAtleastOneBuilding_TDIC())
    PCValidationContext.doPageLevelValidation(\context -> new BOPLineValidation(context, line).changeExpiryDate_TDIC())
  }

  /**
   * Check the answers of a Business Owners line
   * @param line a Business Owners line to validate answers for
   */
  static function validateSupplementalStep(line : BOPLine) {
    PCValidationContext.doPageLevelValidation( \ context -> {
      var val = new BOPLineValidation(context, line)
      val.checkAnswers()
    })
  }

  public function changeExpiryDate_TDIC() {
    if(bopLine.Branch.Job typeis Submission and bopLine.Branch.PeriodStart != bopLine.Branch.EditEffectiveDate.FirstDayOfMonth){
      bopLine.Branch.TermType = TermType.TC_OTHER
      var periodEnd = bopLine.Branch.PeriodEnd
      var normalizedPeriodEnd = bopLine.Branch.EditEffectiveDate.addMonths(12).FirstDayOfMonth
      if (periodEnd != normalizedPeriodEnd and bopLine.Branch.Job typeis Submission) {
        bopLine.Branch.PeriodEnd = normalizedPeriodEnd
        Context.Result.addWarning(bopLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.GLLine.Validation.ExpiryDate.Warning", bopLine.Branch.PeriodEnd.ShortFormat))
      }
    }
  }

  public function changeExpiryDateDIMS_TDIC() {
    if((bopLine.Branch.Job typeis Renewal and bopLine.Branch.isDIMSPolicy_TDIC) and bopLine.Branch.PeriodStart != bopLine.Branch.EditEffectiveDate.FirstDayOfMonth){
      bopLine.Branch.TermType = TermType.TC_OTHER
      var periodEnd = bopLine.Branch.PeriodEnd
      var normalizedPeriodEnd = bopLine.Branch.EditEffectiveDate.addMonths(12).FirstDayOfMonth
      if (periodEnd != normalizedPeriodEnd and (bopLine.Branch.Job typeis Renewal and bopLine.Branch.isDIMSPolicy_TDIC)) {
        bopLine.Branch.PeriodEnd = normalizedPeriodEnd
        Context.Result.addWarning(bopLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("TDIC.GLLine.Validation.ExpiryDate.Warning", bopLine.Branch.PeriodEnd.ShortFormat))
      }
    }
  }

  /**
   * Check that all generic schedules that are selected have at least one scheduled item.
   * @param line a Business Owners
   */
  static function validateSchedules(line : BOPLine) {
    PCValidationContext.doPageLevelValidation( \ context -> new BOPLineValidation(context, line).checkSchedulesAreNotEmpty())
  }

  /**
   * Check the answers of a Business Owners line
   * @param line a Business Owners line to validate answers for
   * @see gw.lob.common.AnswerValidation
   */
  function checkAnswers() {
    Context.addToVisited( this, "checkAnswers" )
    new AnswerValidation( Context, Line, Line.Answers, "BOPSupplemental" ).validate()
  }

  private function checkLocations() {
    bopLine.BOPLocations.each(\ location -> new BOPLocationValidation(Context, location).validate())
  }

  private function checkSmallBusinessType() {
    Context.addToVisited(this, "checkSmallBusinessType")
    if (bopLine.SmallBusinessType == null) {
      Result.addError(bopLine , TC_DEFAULT, DisplayKey.get("Web.Policy.BOP.Validation.SmallBusinessTypeRequired") )
    }
  }

  private function checkBuildings() {
    var validatedClassCodeSearchCriteria : Set<BOPClassCodeSearchCriteria> = {}
    for (building in bopLine.BOPLocations*.Buildings) {
     if(building.Branch.Job typeis PolicyChange && building.Branch.Job.MigrationJobInd_TDIC && building.BOPLocation.BasedOn != null)
     {}else {
       var buildingValidator = new BOPBuildingValidation(Context, building, validatedClassCodeSearchCriteria)
       buildingValidator.validate()
       validatedClassCodeSearchCriteria.add(buildingValidator.buildClassCodeSearchCriteria())
     }
    }
  }

  /**
   * Check BOP coverages
   *
   * <ul>
   *   <li>the forgery limit is set properly</li>
   *   <li>Only one coverage exists for the terrorism</li>
   *   <li>Only one coverage exists for the liquor</li>
   * </ul>
   */
  internal function checkCoverages() {
    Context.addToVisited(this, "allLineCoverages")
    //checkForNoSmallBusinessType()
    for (coverage in bopLine.BOPLineCoverages) {
      if (coverage typeis BOPForgeAltCov) {
        checkForgeryAlteration(coverage)
      }
    }
    checkOnlyOneCoverageInGroup("BOPTerrorismCat")
    checkOnlyOneCoverageInGroup("BOPLiquorCat")
  }

  /**
   * Check the BOPForgeAltLimitTerm limit matches the BOPEmpDisCov limit
   */
  private function checkForgeryAlteration(cov : BOPForgeAltCov) {
    if (cov == null) {
      return
    }
    Context.addToVisited(this, "checkForgeryAlteration")
    if  (not Context.isAtLeast(TC_DEFAULT)) {
      return
    }
    if (bopLine.BOPEmpDisCovExists) {
      if (bopLine.BOPEmpDisCov.BOPEmpDisLimitTerm.Value == bopLine.BOPForgeAltCov.BOPForgeAltLimitTerm.Value) {
        return
      }
    }
    Result.addError(cov, TC_DEFAULT, DisplayKey.get("Web.Policy.BOP.Validation.ForgeryAndEmployeeDishonesty", cov.Pattern.DisplayName))
  }

  private function checkOnlyOneCoverageInGroup(category : String) {
    Context.addToVisited( this, "checkOnlyOneCoverageInGroup" )
    var categoryname = bopLine.Pattern.getCoverageCategoryByPublicId( category ).Name
    var caList : String[]
    caList = {category}
    var covs = bopLine.getCoveragesInCategories( caList )
    if (covs.Count > 1) {
      var covnames = " -- "
      for (cov in covs) {
        covnames = covnames + cov.Pattern.Name + ", "
      }
      Result.addError(bopLine , TC_DEFAULT, DisplayKey.get("Web.Policy.BOP.Validation.OnlyOneCoverageinCategory", categoryname) + covnames )
    }
  }

  internal function additionalInsuredAndTypeUnique() {
    Context.addToVisited( this, "additionalInsuredAndTypeUnique" )
    for (var addlInsured in bopLine.AdditionalInsureds) {
      new PolicyAddlInsuredAndTypeUniqueValidation(Context, addlInsured).validate()
    }
  }

  override function validateLineForAudit() {
    throw new UnsupportedOperationException(DisplayKey.get("Validator.UnsupportedAuditLineError"))
  }
  /**
   * create by: ChitraK
   * @description: Validate for one building
   * @create time: 6:56 PM 8/8/2019
   * @return:
   */

  private function chkAtleastOneBuilding_TDIC(){
    if(bopLine.BOPLocations*.Buildings.IsEmpty)
      Result.addError(bopLine , ValidationLevel.TC_QUOTABLE, DisplayKey.get("Validator.AtleastOneBuilding_TDIC"))
  }

  /**
   * create by: Jeff Lin
   * @description: Validate for the Reason Text that is required for the specific Reason Codes, if it is CP transaction is WITHDRAWN
   * @create time: 10:53 AM 01/02/2020
   * @return:
   */
  static function checkRequiredWithdrawnAndDeclineReasonText_TDIC(line : BOPLine){
    if(line.Branch.Job.Subtype == Job.TC_SUBMISSION and
      (line.Branch.Status == PolicyPeriodStatus.TC_DRAFT or line.Branch.Status == PolicyPeriodStatus.TC_QUOTED) ) {
      PCValidationContext.doPageLevelValidation( \ context -> new BOPLineValidation(context, line).checkRequiredWithdrawnDeclineReasonText_TDIC())
    }
  }

  /**
   * create by: Jeff Lin
   * @description: Validate for checking the base state and lcation state are different or not
   * @create time: 11:34 AM 01/02/2020
   * @return:
   */
  private function checkBaseStateAndLocationState_TDIC(){
    if (bopLine.Branch.Job.Subtype == Job.TC_SUBMISSION or bopLine.Branch.Job.Subtype == Job.TC_REWRITE or
        bopLine.Branch.Job.Subtype == Job.TC_RENEWAL or bopLine.Branch.Job.Subtype == Job.TC_POLICYCHANGE) {
      if (not(bopLine.BOPLocations.allMatch(\loc -> loc.Location.State.Code == bopLine.Branch.BaseState.Code))) {
        Result.addError(bopLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("Validator.BaseStateAndLocationStateAreDifferent_TDIC"))
      }
    }
  }

  /**
   * create by: Jeff Lin
   * @description: Validate for checking the base state and PNI state are different or not
   * @create time: 02:00 PM 01/02/2020
   * @return:
   */
  private function checkBaseStateAndPNIState_TDIC(){
    if (bopLine.Branch.Job.Subtype == Job.TC_SUBMISSION or bopLine.Branch.Job.Subtype == Job.TC_REWRITE or
        bopLine.Branch.Job.Subtype == Job.TC_RENEWAL or bopLine.Branch.Job.Subtype == Job.TC_POLICYCHANGE) {
      var PNIState = bopLine.Branch.PolicyContactRoles.firstWhere(\elt -> elt typeis PolicyPriNamedInsured).ContactDenorm.PrimaryAddress.State.Code
      if (PNIState != bopLine.Branch.BaseState.Code && Level == ValidationLevel.TC_QUOTABLE) {
        Result.addWarning(bopLine, ValidationLevel.TC_QUOTABLE, DisplayKey.get("Validator.BaseStateAndPNIStateAreDifferent_TDIC"))
      }
    }
  }

  /**
   * create by: Jeff Lin
   * @description: Validate for checking if there exists multi-state in the policy
   * @create time: 12:02 AM 01/02/2020
   * @return:
   */
  private function checkMultiStateOfThePolicy_TDIC(){
    if (bopLine.Branch.Job.Subtype == Job.TC_SUBMISSION or bopLine.Branch.Job.Subtype == Job.TC_REWRITE or
        bopLine.Branch.Job.Subtype == Job.TC_RENEWAL or bopLine.Branch.Job.Subtype == Job.TC_POLICYCHANGE) {
      if (bopLine.BOPLocations.countWhere(\loc -> loc.Location.State.Code == bopLine.Branch.BaseState.Code) >= 1 and
          bopLine.BOPLocations.countWhere(\loc -> loc.Location.State.Code != bopLine.Branch.BaseState.Code) >= 1) {
        Result.addError(bopLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("Validator.MoreThanOneStateInPolicy_TDIC"))
      }
    }
  }
  /**
   * validate PL coverage for BOP
   */
  private function checkBOPPolicyHasPLCoverage_TDIC() {
    var branch = bopLine.Branch
    var isValidJobType = {typekey.Job.TC_SUBMISSION, Job.TC_POLICYCHANGE, Job.TC_REINSTATEMENT, typekey.Job.TC_RENEWAL, typekey.Job.TC_REWRITE}.contains(bopLine.JobType)

    if(branch.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" and isValidJobType) {
      var bopBuildings = bopLine.BOPLocations*.Buildings
      var noLiabilityBuildings = bopBuildings.where(\building -> not(building.BOPDentalGenLiabilityCov_TDICExists))
      var hasLiabilityCoverage = not noLiabilityBuildings.HasElements
      if (not hasLiabilityCoverage) {
        hasLiabilityCoverage = hasLiabilityPolicy_TDIC(branch)
      }
      if (not hasLiabilityCoverage) {
        if (Level == ValidationLevel.TC_QUOTABLE || branch.isLegacyPolicy_TDIC) {
          Result.addWarning(noLiabilityBuildings.first(), Level, DisplayKey.get("Validator.CannotWriteBOPWithoutPLCoverage_TDIC"))
        } else if (Level == ValidationLevel.TC_READYFORISSUE or Level == ValidationLevel.TC_BINDABLE) {
          Result.addError(noLiabilityBuildings.first(), Level, DisplayKey.get("Validator.CannotWriteBOPWithoutPLCoverage_TDIC"))
        }
      }
    }
  }

  /**
   *
   */
  private function checkLRPPolicyHasPLCoverage_TDIC(){
    var branch = bopLine.Branch
    var isValidJobType = {typekey.Job.TC_SUBMISSION, Job.TC_POLICYCHANGE, Job.TC_REINSTATEMENT, typekey.Job.TC_RENEWAL, typekey.Job.TC_REWRITE}.contains(bopLine.JobType)

    if(branch.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC" and isValidJobType) {
      var bopBuildings = bopLine.BOPLocations*.Buildings
      var noLiabilityBuildings = bopBuildings.where(\building -> not (building.BOPBuildingOwnersLiabCov_TDICExists))
      var hasLiabilityCoverage = not noLiabilityBuildings.HasElements
      if(not hasLiabilityCoverage) {
        hasLiabilityCoverage = hasLiabilityPolicy_TDIC(branch)
      }
      if(not hasLiabilityCoverage) {
        if(Level == ValidationLevel.TC_QUOTABLE) {
          Result.addWarning(bopLine, ValidationLevel.TC_QUOTABLE, DisplayKey.get("Validator.CannotWriteLRPPolicyHasNoLiabilityCov_TDIC"))
        } else if(Level == ValidationLevel.TC_READYFORISSUE or Level == ValidationLevel.TC_BINDABLE) {
          Result.addError(bopLine, ValidationLevel.TC_DEFAULT, DisplayKey.get("Validator.CannotWriteLRPPolicyHasNoLiabilityCov_TDIC"))
        }
      }
    }
  }

  /**
   *
   * @param branch
   * @return
   */
  private function hasLiabilityPolicy_TDIC(branch : PolicyPeriod) : boolean {
    //by-pass validation for first-time migrated renewals
    if(branch.Job typeis Renewal and branch.BasedOn.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION) {
      return true
    }

    var plOfferings = {"PLClaimsMade_TDIC", "PLOccurence_TDIC"}
    var hasLiabilityPolicy = false

    //Primary Named Insured match
    var accounts = branch.Policy.Account.RelatedAccountsBySharedNamedInsuredContacts_TDIC.toSet()
    accounts = accounts.union({branch.Policy.Account})
    var quotedPeriods = accounts.QuotedPolicyPeriods
    if(quotedPeriods.HasElements) {
      var plPeriods = quotedPeriods.where(\p -> plOfferings.contains(p.getSlice(p.EditEffectiveDate).Offering.CodeIdentifier))
      for(period in plPeriods) {
        var plPeriod = period.getSlice(period.EditEffectiveDate)
        var hasPNIMatch = plPeriod.PrimaryNamedInsured.ContactDenorm == branch.PrimaryNamedInsured.ContactDenorm
        var isCancelled = plPeriod.CancellationDate != null
        var hasEffDatesMatch = (plPeriod.Policy.OriginalEffectiveDate?.compareIgnoreTime(branch.PeriodStart) <= 0  //GWPS-2121 : Keshavg : correcting the condition for migrating policies aswell
            and plPeriod.PeriodEnd.compareIgnoreTime(branch.PeriodStart) > 0)
        if(hasPNIMatch and hasEffDatesMatch and not isCancelled) {
          hasLiabilityPolicy = true
          break
        }
      }
    }

    //Owner/Officers match
    if(not hasLiabilityPolicy and branch.BOPLine.BOPPolicyOwnerOfficer_TDIC.HasElements) {
      var matchingPolicies = 0
      accounts = branch.Policy.Account.RelatedAccountsBySharedNamedInsuredContacts_TDIC.toSet()
      accounts = accounts.union({branch.Policy.Account})  //including every possible account to find a match
      var owners = branch.BOPLine.BOPPolicyOwnerOfficer_TDIC
      for(owner in owners) {
        var matchingPNIPeriods = accounts.QuotedPolicyPeriods.where(\p -> p.getSlice(p.EditEffectiveDate).PrimaryNamedInsured.ContactDenorm == owner.ContactDenorm
                                              and plOfferings.contains(p.getSlice(p.EditEffectiveDate).Offering.CodeIdentifier) and not p.Canceled)
        if(matchingPNIPeriods.HasElements) {
          var hasEffDatesMatch = matchingPNIPeriods.hasMatch(\elt -> (elt.Policy.getOriginalEffectiveDate().compareIgnoreTime(branch.PeriodStart) <= 0 and elt.PeriodEnd.compareIgnoreTime(branch.PeriodStart) > 0)) //GWPS-2121 : Keshavg : correcting the condition for migrating policies aswell
          if(hasEffDatesMatch) {
            matchingPolicies += 1
          }
        } else {
          break //all Owner officers on the policy mut have a valid matching policy
        }
      }
      hasLiabilityPolicy = owners.Count == matchingPolicies ? true : hasLiabilityPolicy
    }

    return hasLiabilityPolicy
  }

  private function validateBOPManuscriptTDIC() {

    var hardstop : boolean = false

    for(building in this.bopLine?.BOPLocations*.Buildings)
    if (building.BOPManuscriptEndorsement_TDICExists &&  Level == ValidationLevel.TC_QUOTABLE && (this.bopLine?.JobType == Job.TC_SUBMISSION
        || this.bopLine?.JobType == Job.TC_POLICYCHANGE || this.bopLine?.JobType == Job.TC_RENEWAL  )) {
      //GWPS-2201 : Updating the validation rule by not considering the Manuscript’s which are expired and having Eff date is equal to Exp date then we wont invoke the validation rule.
      var bopManuEnd = building.BOPManuscript.where(\elt -> elt.ManuscriptEffectiveDate != elt.ManuscriptExpirationDate)
      for (elt in bopManuEnd) {
        if (elt.ManuscriptType == BOPManuscriptType_TDIC.TC_ADDITIONALINSUREDCONTINUED && elt.policyAddInsured_TDIC == null) {
          hardstop = true
          break
        }
        if (elt.ManuscriptType == BOPManuscriptType_TDIC.TC_MORTGAGEEAMENDEDTOREAD && elt.PolicyMortgagee == null) {
          hardstop = true
          break
        }
        if (elt.ManuscriptType == BOPManuscriptType_TDIC.TC_LOSSPAYEEAMENDEDTOREAD && elt.PolicyLossPayee == null) {
          hardstop = true
          break
        }
      }
    }

    if (hardstop){
      Result.addError(bopLine, Level, DisplayKey.get("Web.Policy.BOP.Validation.ManuscriptEndorsement"))

    }

  }



}
