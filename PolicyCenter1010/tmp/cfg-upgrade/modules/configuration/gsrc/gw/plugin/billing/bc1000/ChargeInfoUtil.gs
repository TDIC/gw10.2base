package gw.plugin.billing.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.util.Pair
uses org.apache.commons.collections.keyvalue.MultiKey
uses wsi.remote.gw.webservice.bc.bc1000.entity.types.complex.ChargeInfo

/**
 * External helper class for returning Charges and Installments
 *
 * @see gw.webservice.pc.bc1000.BillingInstructionInfoEnhancement
 */

@Export
class ChargeInfoUtil {
  /**
  * Create ChargeInfos array from all the CHARGED transactions of the period.
  *
  * Example of UNCHARGED transactions are premiums created by a submission with reporting plan.
  * In that case, only taxes transactions are charged.
  *
  * @param period Policy Period that contains the ChargeInfos
  * @return Array of ChargeInfo items that are charged
   */
  static function getChargeInfos(period : PolicyPeriod) : ChargeInfo[] {
    return getChargeOrWrittenInfos(period, true)
  }

  /**
   * Create ChargeInfos array from all the INSTALLMENT transactions of the period.
   *
   * @param period Policy Period that contains the ChargeInfos
   * @return Array of ChargeInfo items that are not charged
   */
  static function getInstallmentInfos(period : PolicyPeriod) : ChargeInfo[] {
    return getChargeOrWrittenInfos(period, false)
  }

  private static function getChargeOrWrittenInfos(period : PolicyPeriod, useCharged : boolean) : ChargeInfo[] {
    var chargesMap : Map<MultiKey, Pair<ChargeInfo, MonetaryAmount>> = {}

    //BrittoS 05/19/2020 - ERE charges
    if(period.isERERating_TDIC) {
      chargesMap = getEREChargesMap(period, useCharged)
    } else {    //All Non ERE transactions
      var chargedTransactions = period.AllTransactions.where(\ t -> (useCharged ? t.Charged : t.Written))
      var tmpChargePatternCode : String
      for (txn in chargedTransactions) {
        //20150227 TJ Talluto - BC:US1183 separate CIGA from normal taxes
        tmpChargePatternCode = txn.Cost.ChargePattern.Code // OOTB default
        if (txn.Cost.ChargePattern == ChargePattern.TC_TAXES
            && txn typeis WC7Transaction
            && txn.WC7Cost typeis WC7JurisdictionCost){
          var tmpWC7JurisdictionCostType = txn.WC7Cost.JurisdictionCostType
          if (tmpWC7JurisdictionCostType == typekey.WC7JurisdictionCostType.TC_CIGA){
            tmpChargePatternCode = "CIGASurcharge"
          } else {
            tmpChargePatternCode = "StateAssessmentFee"
          }
        }
        //var key = createChargeKey(txn)
        var key = createChargeKey(txn, tmpChargePatternCode) //OOTB had one parameter.
        if (chargesMap.containsKey(key)) {
          var chargePair = chargesMap.get(key)
          chargesMap.put(key, Pair.make(chargePair.First, chargePair.Second.add(txn.AmountBilling)))
        } else {
          var chargeInfo = new ChargeInfo() {
            :ChargePatternCode = tmpChargePatternCode, //OOTB: txn.Cost.ChargePattern.Code
            :ChargeGroup = txn.Cost.ChargeGroup,
            :WrittenDate = txn.WrittenDate.XmlDateTime
          }
          chargesMap.put(key, Pair.make(chargeInfo, txn.AmountBilling))
        }
      }
    }

    return chargesMap.Values.map( \ pair -> {
      pair.First.Amount = pair.Second.toString()
      return pair.First
    }).toTypedArray()
  }

  internal static function createChargeKey(transaction : Transaction) : MultiKey {
    return new MultiKey(transaction.EffDate, transaction.ExpDate, transaction.Cost.ChargePattern, transaction.Cost.ChargeGroup)
  }

  internal static function createChargeKey(transaction : Transaction, passedInChargePatternCode : String) : MultiKey {
    //return new MultiKey(transaction.EffDate, transaction.ExpDate, passedInChargePatternCode, transaction.Cost.ChargeGroup)
    //BrittoS 03/24 - Reinstament aggregation was creating Debit and Credit for the same ChargePattern.
    return new MultiKey(transaction.WrittenDate, passedInChargePatternCode, transaction.Cost.ChargeGroup)
  }

  /**
   * BrittoS 05/19/2020 - ERE Charges for BC
   * @return
   */
  private static function getEREChargesMap(period : PolicyPeriod, useCharged : boolean) : Map<MultiKey, Pair<ChargeInfo, MonetaryAmount>> {
    var chargesMap : Map<MultiKey, Pair<ChargeInfo, MonetaryAmount>> = {}

    var chargedTransactions = period.AllTransactions.where(\ t -> (useCharged ? t.Charged : t.Written))
    var ereChargedTxn = chargedTransactions.where(\elt -> isERECost(elt.Cost))
    var chargePattern = ChargePattern.TC_EREPREMIUM_TDIC
    for(txn in ereChargedTxn) {
      var key = createChargeKey(txn, chargePattern.Code)
      if (chargesMap.containsKey(key)) {
        var chargePair = chargesMap.get(key)
        chargesMap.put(key, Pair.make(chargePair.First, chargePair.Second.add(txn.AmountBilling)))
      } else {
        var chargeInfo = new ChargeInfo() {
          :ChargePatternCode = chargePattern.Code,
          :ChargeGroup = txn.Cost.ChargeGroup,
          :WrittenDate = txn.WrittenDate.XmlDateTime
        }
        chargesMap.put(key, Pair.make(chargeInfo, txn.AmountBilling))
      }
    }
    return chargesMap
  }
  /**
   * BrittoS 05/19/2020 - ERE Charge filter
   */
  private static function isERECost(cost : Cost) : Boolean {
    var stateERECosts = {
        GLStateCostType.TC_ERE_NJDEDUCTIBLE_TDIC,
        GLStateCostType.TC_ERE_NJWAIVEROFCONSENT_TDIC,
        GLStateCostType.TC_ERE_NJIGASURCHARGE_TDIC,
        GLStateCostType.TC_ERE_RISKMANAGEMENT_TDIC,
        GLStateCostType.TC_ERE_IRPMDISCOUNT_TDIC,
        GLStateCostType.TC_ERE_PLCLAIMSMADE_TDIC,
        GLStateCostType.TC_ERE_NEWDENTIST_TDIC
    }
    if((cost typeis GLCovExposureCost and cost.GLCostType == GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY)
        or (cost typeis GLHistoricalCost_TDIC and cost.CostType == GLCostType_TDIC.TC_ERE_PROFESSIONALLIABILITY)
        or (cost typeis GLAddnlInsdSchedCost_TDIC and cost.GLAddnlInsuredCostType_TDIC == GLAddnlInsuredCostType_TDIC.TC_ERE_ADDITIONALINSURED)
        or (cost typeis GLStateCost and stateERECosts.contains(cost.StateCostType))) {
      return true
    }
    return false
  }
}
