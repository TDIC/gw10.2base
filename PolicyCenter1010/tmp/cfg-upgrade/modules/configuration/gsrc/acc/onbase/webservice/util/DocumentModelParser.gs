package acc.onbase.webservice.util

uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Utility class to assist in deserializing document models received via web service calls.
 */
class DocumentModelParser {

  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  private final var _model : acc.onbase.api.si.model.documentmodel.Document


  /**
   * @param model document model
   */
  construct(model : acc.onbase.api.si.model.documentmodel.Document) {
    _model = model
  }

  /**
   * Retrieve DocumentType specified by OnBaseName
   * <p>
   * DocumentType is a required value.
   *
   * @return valid document type
   */
  public property get Type() : DocumentType {
    if (!_model.Type.OnBaseName.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocumentType"))
    }

    return DocumentType.findByOnBaseName(_model.Type.OnBaseName)
  }

  /**
   * Retrieve Subtype specified by OnBaseName
   * <p>
   * Subtype is optional, but must be a valid subtype for the provided document type if present.
   *
   * @return null or valid subtype
   */
  public property get Subtype() : OnBaseDocumentSubtype_Ext {
    if (!_model.Subtype.OnBaseName.HasContent) {
      // Subtype is optional
      return null
    }

    var subtype = OnBaseDocumentSubtype_Ext.findByOnBaseName(_model.Subtype.OnBaseName)
    var doctype = Type;
    if (!subtype.hasCategory(doctype)) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_SubTypeNotRelatedToDocumentType", subtype, doctype))
    }

    return subtype
  }

  /**
   * Retrieve Account by Account Number
   * <p>
   * Account Number is optional and must provide a valid account number if present
   *
   * @return null or valid account or account linked to the policy
   */
  public property get Account() : Account {
    var account : Account = null
    if (_model.Account == null || !_model.Account.AccountNumber.HasContent) {
      if (Policy != null) {
        account = Policy.Account
        return account
      } else {
        return null
      }
    }
    account = entity.Account.finder.findAccountByAccountNumber(_model.Account.AccountNumber)
    if (account == null) {
      //invalid account number
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", _model.Account.AccountNumber))
    }


    if (Policy != null && Policy.Account != account) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnrelatedAccountAndPolicy", account.AccountNumber, _model.PolicyPeriod.PolicyNumber))
    }
    return account
  }

  /**
   * Retrieve Policy by PolicyNumber
   * <p>
   * PolicyNumber is optional and must provide a valid policy number
   *
   * @return null or valid policy
   */
  public property get Policy() : Policy {
    var policy:Policy

    if (_model.PolicyPeriod == null || !_model.PolicyPeriod.PolicyNumber.HasContent) {
      if (_model.Policy?.MostRecentBoundPeriodOnMostRecentTerm == null || !_model.Policy?.MostRecentBoundPeriodOnMostRecentTerm?.PolicyNumber?.HasContent){
        return null
      } else {
        policy = entity.Policy.finder.findPolicyByPolicyNumber(_model.Policy?.MostRecentBoundPeriodOnMostRecentTerm?.PolicyNumber)
      }
    } else {
      policy = entity.Policy.finder.findPolicyByPolicyNumber(_model.PolicyPeriod.PolicyNumber)
    }

    if (policy == null) {
      //invalid policy number
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidPolicyNumber", _model.PolicyPeriod.PolicyNumber))
    }

    return policy
  }

  /**
   * Retrieve Submission by SubmissionNumber
   * <p>
   * SubmissionNumber is optional and must provide a valid submission number
   *
   * @return null or valid submission
   */
  public property get Job() : Job {
    if (_model.Job == null || !_model.Job.SubmissionNumber.HasContent) {
      return null
    }

    var job = entity.Job.finder.findJobByJobNumber(_model.Job.SubmissionNumber)
    if (job == null) {
      //invalid submission number
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidSubmissionNumber", _model.Job.SubmissionNumber))
    }

    if (Policy != null && job.Policy != Policy) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnrelatedSubmissionAndPolicy", job.SubmissionNumber, _model.PolicyPeriod.PolicyNumber))
    } else if (Account != null && job.Policy.Account != Account) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnrelatedSubmissionAndAccount", job.SubmissionNumber, _model.Account.AccountNumber))
    }
    return job
  }

  /**
   * Validate Status
   *
   * @return valid Status
   */
  public property get Status() : DocumentStatusType {
    if (_model.Status == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingStatus"))
    }

    return _model.Status
  }

  /**
   * Validate Mimetype
   *
   * @return valid mimetype
   */
  public property get MimeType() : String {
    if (!_model.MimeType.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingFileType"))
    }

    return _model.MimeType
  }

  /**
   * Validate Pending Document
   *
   * @return document found by the pending docuid
   */
  public function getPendingDocument() : Document {
    validateDocUID()
    var document = Document.findDocumentByOnBaseID(_model.DocUID)
    verifyDocumentIsNull(document)
    if (!_model.PendingDocUID.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingPendingID"))
    } else {
      document = Document.findDocumentByPendingDocUID(_model.PendingDocUID)
      if (document == null) {
        logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownPendingID", _model.PendingDocUID))
      }
    }
    return document
  }

  /**
   * Validate New Document
   *
   * @return new document instance
   */
  public function getNewDocument() : Document {
    validateDocUID()
    verifyDocumentIsNull(Document.findDocumentByOnBaseID(_model.DocUID))
    return new Document()
  }

  /**
   * Validate Existing Document
   *
   * @return document found by docuid
   */
  public function getExistingDocument() : Document {
    validateDocUID()
    var document = Document.findDocumentByOnBaseID(_model.DocUID)
    if (document == null) {
      //throw error if docuid doesn't exist for reindex
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", _model.DocUID))
    }
    return document
  }

  /**
   * Verify account or policy exists
   */
  public function verifyDocumentIsLinked() {

    if (Account == null && Policy == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingAccountOrPolicy"))
    }
  }

  /**
   * Verify if the document already exists
   */
  private function verifyDocumentIsNull(document : Document) {
    if (document != null) {
      //throw error if docuid already exists for async/ new document
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_DuplicateDocUID", _model.DocUID))
    }
  }

  /**
   * Verify if the docuid is passed in the model
   */
  private function validateDocUID() {
    if (!_model.DocUID.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
    }
  }

  private static function logAndThrow(message : String) {
    _logger.error(message)
    throw new IllegalArgumentException(message)
  }

}