package acc.onbase.api.service.interfaces

uses acc.onbase.api.service.interfaces.ArchiveDocumentAsyncInterface
uses acc.onbase.api.service.interfaces.ArchiveDocumentSyncInterface
uses acc.onbase.api.service.interfaces.DocumentContentInterface
uses acc.onbase.api.service.interfaces.SubmitMessageInterface
uses acc.onbase.api.service.interfaces.SubmitMessageWithResponseInterface

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */
interface ServicesManagerInterface {
  /** Async archive document service. */
  public property get ArchiveDocumentAsync(): ArchiveDocumentAsyncInterface

  /** Sync archive document service. */
  public property get ArchiveDocumentSync(): ArchiveDocumentSyncInterface

  /** Get document content service. */
  public property get DocumentContent() : DocumentContentInterface

  /** Submit message service. */
  public property get SubmitMessage() : SubmitMessageInterface

  /** Submit message with response service. */
  public property get SubmitMessageWithResponse() : SubmitMessageWithResponseInterface
}