package acc.onbase.api.messaging

uses acc.onbase.api.service.ServicesManager
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses gw.api.util.DateUtil
uses gw.plugin.InitializablePlugin
uses gw.plugin.messaging.MessageTransport

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Last Changes:
 * <p>
 * 02/03/2017 - Daniel Q. Yu
 * * Updated code for PC 9
 * 10/03/2017 - Anirudh Mohan
 * * Updated code to use XML Messaging
 */
class OnBaseMessageTransport implements MessageTransport, InitializablePlugin {

  private var _destinationId : int

  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  /**
   * Send a message.
   */
  override function send(message : Message, payload : String) {
    var service = ServicesManager.getSubmitMessage()

    try {
      var messageID = service.submitMessage(payload)
      message.SenderRefID = Long.toString(messageID)
      message.reportAck()
    } catch (ex : Exception) {
      if (message.RetryCount < OnBaseConfigurationFactory.Instance.MaxRetries) {
        _logger.debug("Error during message transport, scheduling retry", ex)
        var retryTime = DateUtil.addSeconds(DateUtil.currentDate(), (message.RetryCount + 1) * OnBaseConfigurationFactory.Instance.RetryIntervalSeconds)
        message.reportError(retryTime)
      }
      else {
        _logger.error("Reached maximum retry count, marking message as failed", ex)
        message.reportError(ErrorCategory.TC_MAXRETRIESREACHED)
      }
    }
  }

  /**
   * Shutdown the transport
   */
  override function shutdown() {
  }

  /**
   * Suspend the transport
   */
  override function suspend() {
  }

  /**
   * Resume transport
   */
  override function resume() {
  }

  override property set Parameters(map : Map) {

  }

  override property set DestinationID(destinationID : int) {
    _destinationId = destinationID
  }
}