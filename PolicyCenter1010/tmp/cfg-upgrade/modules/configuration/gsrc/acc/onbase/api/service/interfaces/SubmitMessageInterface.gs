package acc.onbase.api.service.interfaces

uses gw.xml.XmlElement

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 * <p>
 * Last Changes:
 * *10/03/2017 - Anirudh Mohan
 * * Interface to submit an outgoing message to OnBase
 */
interface SubmitMessageInterface {

  /**
   * Submit a message to OnBase
   *
   * @param payload string payload of message
   * @return submitted message id
   */
  function submitMessage(payload: String) : long

  /**
   * Submit a message to OnBase
   *
   * @param payload XML message
   * @return submitted message id
   */
  function submitMessage(payload: XmlElement) : long
}