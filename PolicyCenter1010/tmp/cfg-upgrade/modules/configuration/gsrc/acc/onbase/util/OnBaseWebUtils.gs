package acc.onbase.util

uses acc.onbase.api.security.*
uses acc.onbase.configuration.OnBaseConfigurationFactory

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */

/*
 * OnBaseWebUtils - Code for OnBaseDemoPop.pcf
 *
 */

class OnBaseWebUtils {

  public static function renderPopUrl(template: String, args: String[]):String{
    var encodedArgs = UrlEncodeArray(args)
    var constructWebURL = String.format(template, encodedArgs)
    var popURL = acc.onbase.configuration.OnBaseConfigurationFactory.Instance.PopURL
    var urlWithoutHash = popURL + constructWebURL

    if (OnBaseConfigurationFactory.Instance.EnableDocPopURLCheckSum){
      var urlWithHash = popURL + constructWebURL + "&chksum=" + SecurityManager.computeCheckSum(constructWebURL.split('\\?')[1])
      return urlWithHash
    }
    return urlWithoutHash
  }

  /**
   * Url Encoder
   */
  private static function UrlEncode(urlParam: String): String {
    return java.net.URLEncoder.encode(urlParam, java.nio.charset.StandardCharsets.UTF_8.toString())
  }

  private static function UrlEncodeArray(args: String[]): String[]{
    var newArray = new String[args.length]
    foreach(arg in args index i){
      newArray[i] = UrlEncode(arg)
    }
    return newArray
  }

}