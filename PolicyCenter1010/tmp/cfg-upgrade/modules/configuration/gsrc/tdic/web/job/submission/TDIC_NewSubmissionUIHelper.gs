package tdic.web.job.submission

uses gw.web.job.submission.NewSubmissionUIHelper
/**
 * Shane Sheridan 09/09/2014
 *
 * Extension of OOTB NewSubmissionUIHelper.
 */
class TDIC_NewSubmissionUIHelper extends NewSubmissionUIHelper{

  construct(currentLocation : pcf.NewSubmission) {
    super(currentLocation)
  }

  /**
   * Shane Sheridan 09/09/2014
  */
  function initializeProducerSelection_TDIC(acct : Account) : ProducerSelection
  {
    var producerSelection = super.initializeProducerSelection(acct)

    //var currentUser = User.util.CurrentUser
    // if(currentUser.isExternalUser()){
      //producerSelection.Producer = currentUser.Organization
      initializeProducerCode(producerSelection)

    /*****
    }
    else{
      producerSelection.Producer = null
      producerSelection.ProducerCode = null
    } *****/

    return producerSelection
  }

  /**
   * US975
   * Shane Sheridan 04/02/2015
   */
  private function initializeProducerCode(producerSelection : ProducerSelection){
    var availableProducerCodes = producerSelection.getRangeOfActiveProducerCodesForCurrentUser()
    var isOnlyOneCode = availableProducerCodes?.Count == 1
    if(isOnlyOneCode and availableProducerCodes != null){
      producerSelection.ProducerCode = availableProducerCodes.first()
    }
  }
}