package tdic.web.account.submgr

uses com.guidewire.pl.system.dependency.PLDependencies
uses com.guidewire.pc.system.internal.InternalMethods

/**
 * US975
 * Shane Sheridan 03/02/2015
 */
class TDIC_AccountCreateUtil {

  /**
   * US975
   * Shane Sheridan 03/02/2015
  */
  static function getProducerSelectionForCreateAccount() : ProducerSelection {
    var producerSelection = PLDependencies.Navigator.TopLocation.newInstance(ProducerSelection) as ProducerSelection
    InternalMethods.asProducerSelectionInternal(producerSelection).setForCurrentUserAndAccount(null)

    var user = User.util.CurrentUser
    if (user != null and user.isExternalUser()) {
      if (!user.getOrganization().equals(producerSelection.getProducer())) {
        producerSelection.Producer = user.Organization
      }

      initializeProducerCode(producerSelection)
    }

    return producerSelection
  }

  /**
   * US975
   * Shane Sheridan 04/02/2015
  */
  private static function initializeProducerCode(producerSelection : ProducerSelection){
    var availableProducerCodes = producerSelection.getRangeOfActiveProducerCodesForCurrentUser(true)
    var isOnlyOneCode = availableProducerCodes.Count == 1

    if(isOnlyOneCode and availableProducerCodes != null){
      producerSelection.ProducerCode = availableProducerCodes.first()
    }
  }
}