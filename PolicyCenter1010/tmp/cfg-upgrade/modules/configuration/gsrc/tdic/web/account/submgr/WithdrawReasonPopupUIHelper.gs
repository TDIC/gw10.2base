package tdic.web.account.submgr

uses pcf.JobComplete
uses tdic.pc.integ.plugins.hpexstream.util.TDIC_PCExstreamHelper
uses gw.api.util.DisplayableException
uses gw.api.locale.DisplayKey

/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 8/25/14
 * Time: 10:15 AM
 * To change this template use File | Settings | File Templates.
 */
@Export
class WithdrawReasonPopupUIHelper {
  // Loads the Submission from the DB into a separate bundle before
  // withdrawing it, to avoid interference from any validation issues on
  // the current page.

  public static function withdrawSubmission(submission: entity.Submission, policyPeriod: entity.PolicyPeriod, wizard: pcf.api.Wizard) {
    if (submission.RejectReason == null) {
      throw new gw.api.util.DisplayableException(DisplayKey.get("Web.DeclinedReasonPopup.EmptyReasonError"))
    }
    //GW-834
    if (ScriptParameters.TDIC_DataSource == typekey.DataSource_TDIC.TC_POLICYCENTER as String){ // KW 04-12-16  removed for POC
      var lob = TDIC_PCExstreamHelper.getLOBCode(policyPeriod)
      if(!(new TDIC_PCExstreamHelper()).getEventMapParams("${lob}.${typekey.TDIC_DocCreationEventType.TC_WITHDRAW.Code}", policyPeriod.Job.getJobDate())?.HasElements){
        throw new DisplayableException("Document template mappings not found. Please contact System Administrator.")
      }
    }
    var sub: Submission
    var branch: PolicyPeriod

    // Execute the withdraw in a separate bundle with the submission reloaded from the DB
    gw.transaction.Transaction.runWithNewBundle(\b -> {
      sub = b.loadBean(submission.ID) as Submission
      branch = b.loadBean(policyPeriod.ID) as PolicyPeriod

      // But, make sure to update the withdraw reason
      sub.RejectReason = submission.RejectReason
      sub.RejectReasonText = submission.RejectReasonText

      // Withdraw and commit
      branch.SubmissionProcess.withdrawJob()
      /**
       * US555: Document Production
       * 04/09/2015 Shane Murphy
       *
       * Creates documents for Submission Withdrawals.
       */
      //JIRA-545 - Moved doc creation process from its own bundle to this withdrawal process bundle
      if(branch.WC7LineExists)
        sub.createSingleDocumentsForEvent(TDIC_DocCreationEventType.TC_WITHDRAW as String)
    })

    // Go to JobComplete page if we're declining from a wizard
    if (wizard != null) {
      wizard.cancel()
      JobComplete.go(sub, policyPeriod)
    }
  }
}