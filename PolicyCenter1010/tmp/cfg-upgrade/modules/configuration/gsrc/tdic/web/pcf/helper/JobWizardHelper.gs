package tdic.web.pcf.helper

uses gw.api.productmodel.ClausePattern
uses tdic.pc.common.batch.enrole.EnroleHelper

class JobWizardHelper {

  public static function updateRiskManagementDiscount(branch : PolicyPeriod) {
    if (not(branch.Job typeis Submission)) {
      EnroleHelper.updateRiskManagementDiscount(branch)
    }
    if (not(branch.Job typeis Rewrite)) {
      toggleRiskManagement(branch.GLLine)
    }
  }

  public static function toggleCoveragePattern(coverable : Coverable, coveragePattern : gw.api.productmodel.ClausePattern, exists : Boolean) {

    coverable.setCoverageConditionOrExclusionExists(coveragePattern, exists)
    coverable.PolicyLine.Branch.setRetroDate_TDIC()

    coveragePattern.getEREDEPLAvailability_TDIC(coverable, !exists)

    if (coveragePattern.CodeIdentifier == "BOPEqBldgCov") {
      if (coverable typeis BOPBuilding) {
        coverable.setEarthquakeLimits_TDIC();
        coverable.setZipcodeTerritoryCodeForEQEQSLCov_TDIC()
      }
    }
    if (coveragePattern.CodeIdentifier == "BOPILMineSubCov_TDIC") {
      (coverable as BOPBuilding).setILSubLimit_TDIC()
    }

    /*if ((coverable.PolicyLine.AssociatedPolicyPeriod.Job typeis Renewal) and exists and coveragePattern.CodeIdentifier == "GLRiskMgmtDiscount_TDIC") {
      EnroleHelper.updateRiskManagementDiscount(coverable.PolicyLine.Branch)
    }*/

    if (exists) {
      coveragePattern.setEREEREDEPLCYBERECovEffExpDates_TDIC(coverable)
    }

    if (coveragePattern.CodeIdentifier == "GLDentalEmpPracLiabCov_TDIC" and !exists) {
      coveragePattern.setGLExtendedReportPeriodDPLCov_TDIC(coverable)
    }

    if (coveragePattern.CodeIdentifier == "GLNewDentistDiscount_TDIC" || coveragePattern.CodeIdentifier == "GLNewGradDiscount_TDIC") {
      coverable.PolicyLine.syncQuestions()
    }

    if (coveragePattern.CodeIdentifier == "BOPEqSpBldgCov") {
      if (coverable typeis BOPBuilding) {
        coverable.setEQSPExistence_TDIC()
        coverable.setZipcodeTerritoryCodeForEQSLCov_TDIC()
        coverable.setEarthquakeSLDeductibles_TDIC()
        coverable.setEQSLZone_TDIC()
      }
    }
  }

  public static function checkToggle(coverable : Coverable, coveragePattern : ClausePattern) : Boolean {
    if ({"BOPDEBLCov_TDIC", "BOPDMWLDCov_TDIC"}.contains(coveragePattern.CodeIdentifier) or
        (coveragePattern.CodeIdentifier == "GLNewDentistDiscount_TDIC" and coverable.PolicyLine.JobType == typekey.Job.TC_POLICYCHANGE)/*or
      (coveragePattern.CodeIdentifier == "GLRiskMgmtDiscount_TDIC" and coverable.PolicyLine.JobType == typekey.Job.TC_POLICYCHANGE)*/)
      return false
    //BR-026
    if (coverable.PolicyLine.Branch.Job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI).HasElements) {
      if (coveragePattern.CodeIdentifier == "GLDentalEmpPracLiabCov_TDIC")
        return false
    }
    //BR-027 of PL Policy Change
    if (coverable.PolicyLine.Branch.Job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM).HasElements) {
      if (coveragePattern.CodeIdentifier == "GLExtendedReportPeriodCov_TDIC") {
        return false
      }
    } else if (coveragePattern.CodeIdentifier == "GLExtendedReportPeriodCov_TDIC") {
      return false
    }
    //BR-028 of PL Policy Change
    if (coverable.PolicyLine.Branch.Job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER).HasElements) {
      if (coveragePattern.CodeIdentifier == "GLCyvSupplementalERECov_TDIC") {
        return false
      }
    } else if (coveragePattern.CodeIdentifier == "GLCyvSupplementalERECov_TDIC") {
      return false
    }
    //BR-029 of PL Policy Change
    if (coverable.PolicyLine.Branch.Job.ChangeReasons.where(\elt -> elt.ChangeReason == ChangeReasonType_TDIC.TC_CANCELEPLI).HasElements) {
      if (coveragePattern.CodeIdentifier == "GLExtendedReportPeriodDPLCov_TDIC") {
        return false
      }
    } else if (coveragePattern.CodeIdentifier == "GLExtendedReportPeriodDPLCov_TDIC") {
      return false
    }
    return coveragePattern.allowToggle(coverable)
  }

  /**
   *  GWPS-591 : New Dentist - New Dentist Discount is available on a policy change
   *   This method is used to Controle the visibility of a New discount during the PolicyChange.
   */
  public static function isGLNewDentistDiscountVisible(coverable : Coverable, coveragePattern : ClausePattern) : boolean {
    if(coveragePattern.CodeIdentifier == "GLNewDentistDiscount_TDIC" and coverable.PolicyLine.JobType == typekey.Job.TC_POLICYCHANGE
        and coverable.PolicyLine.Branch.GLLineExists and !coverable.PolicyLine.Branch.GLLine.GLNewDentistDiscount_TDICExists){
      return Boolean.FALSE
    }
    return Boolean.TRUE
  }

  /**
   * Premium Modification Tab selection
   *
   * @param glLine
   */
  public static function onSelectOfPremiumModificationTab(line : GLLine) {
    toggleRiskManagement(line)
  }

  /**
   * Auto selection/deselection of RiskManagement discount
   */
  public static function toggleRiskManagement(line : GLLine) {

    var enroleData = line.RiskManagementDisc_TDIC
    var periodStart = line.Branch.PeriodStart.trimToMidnight()
    var isEligible = false
    if (enroleData.HasElements) {
      for (data in enroleData) {
        if (data.StartDate != null and data.EndDate != null) {
          var enroleStart = data.StartDate.trimToMidnight()
          var enroleEnd = data.EndDate.trimToMidnight()
          if (periodStart.afterOrEqual(enroleStart) and periodStart.before(enroleEnd)) {
            isEligible = true
            break
          }
        }
      }
    }
    var discountCat = line.Pattern.getCoverageCategoryByCodeIdentifier("GLDiscountsCat_TDIC")
    var riskManagement = discountCat.ConditionPatterns.firstWhere(\elt -> elt.CodeIdentifier == "GLRiskMgmtDiscount_TDIC")
    if (riskManagement != null) {
      if (not isEligible) {
        line.setConditionExists(riskManagement, false)
      } else if (isEligible and not line.GLRiskMgmtDiscount_TDICExists) {
        line.setConditionExists(riskManagement, true)
      }
    }
  }

  /**
   * @param branch
   * @return
   */
  public static function isDentistServicesOfLineRequired(branch : PolicyPeriod) : Boolean {
    var offering = branch.Offering.CodeIdentifier
    var line = branch.GLLine
    //Do not display for Qiuck Quote, Policy Change & Legacy Renewals
    if (branch.isQuickQuote_TDIC
        or ((branch.Job typeis Renewal) and (branch.BasedOn?.Status == PolicyPeriodStatus.TC_LEGACYCONVERSION))
        or (branch.Job typeis PolicyChange)) {
      return false
    } else if (offering == "PLClaimsMade_TDIC" or offering == "PLOccurence_TDIC") {  //Display PL Experienced questions regardless of 'Yes' or 'No' selected for the question 'Were you licensed for the first time in the last 12 months?
      return true
    }
    return false
  }

  /**
   * @param branch
   * @return
   */
  public static function isAnestheticModalitiesOfLineRequired(branch : PolicyPeriod) : Boolean {
    var offering = branch.Offering.CodeIdentifier
    var line = branch.GLLine
    if (branch.isQuickQuote_TDIC) {
      return false
    } else if (offering == "PLClaimsMade_TDIC" or offering == "PLOccurence_TDIC") {  //Display PL Experienced questions regardless of 'Yes' or 'No' selected for the question 'Were you licensed for the first time in the last 12 months?
      return true
    }

    return false
  }

  public static function onSelectOfUWInformationTab(period : PolicyPeriod) {
    var glLine = period.GLLine
    glLine.syncConditions()
    glLine.syncQuestions()
    if (not period.isQuickQuote_TDIC and period.Offering.CodeIdentifier != "PLCyberLiab_TDIC") {
      glLine.setNewGradDiscountEffDates_TDIC()
    }
    if(period.GLLineExists and (period.Offering?.CodeIdentifier == "PLClaimsMade_TDIC" or period.Offering?.CodeIdentifier == "PLOccurence_TDIC")){
      glLine.setGLLineQuestionsWithDefaultValues()
    }
  }

  /**
   * @param period
   * @return
   */
  public static function canAdditionalCoveragesTabVisible(period : PolicyPeriod) : Boolean {
    var offering = period.Offering.CodeIdentifier
    if (period.isQuickQuote_TDIC and offering == "PLCyberLiab_TDIC") {
      return false
    }
    return true
  }

  /**
   * @param period
   * @return
   */
  public static function canAdditionalInsuredTabVisible(period : PolicyPeriod) : Boolean {
    return not(period.Offering.CodeIdentifier == "PLCyberLiab_TDIC") and not period.isQuickQuote_TDIC
  }

  /**
   * @param period
   * @return
   */
  public static function canCertificateHoldersTabVisible(period : PolicyPeriod) : Boolean {
    return period.Offering.CodeIdentifier != "PLCyberLiab_TDIC"
  }

  /**
   * @param period
   * @return
   */
  public static function canPremiumModificationsTabVisible(period : PolicyPeriod) : Boolean {
    return period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or period.Offering.CodeIdentifier == "PLOccurence_TDIC"
  }

  /**
   * @param period
   * @return
   */
  public static function canUWInformationTabVisible(period : PolicyPeriod) : Boolean {
    if (period.Offering.CodeIdentifier == "PLCyberLiab_TDIC" and period.isQuickQuote_TDIC) {
      return false
    }
    return true
  }

}