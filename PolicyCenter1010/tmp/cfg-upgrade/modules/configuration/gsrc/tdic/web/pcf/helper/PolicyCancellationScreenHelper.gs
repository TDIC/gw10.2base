package tdic.web.pcf.helper

uses java.util.Date


class PolicyCancellationScreenHelper {


  // Added by Jeff Lin, 11/11/2019, 4:04 PM, GPC-584/549, PL/CP Initiate Cancellation, Setting the DropDown Cancellation Reason Codes for scenarios.
  public static function getFilteredCancellationReasons(job: Cancellation, policyPeriod: PolicyPeriod) : typekey.ReasonCode[] {
    if (job.Source == null) {
      return null
    }
    //Work Comp
    if (policyPeriod.WC7LineExists) {
      if (job.Source == CancellationSource.TC_CARRIER) {
        return ReasonCode.TF_WC_CARRIERCANCELREASONS.TypeKeys.toTypedArray()
      } else if (job.Source == CancellationSource.TC_INSURED) {
        return ReasonCode.TF_WC_INSUREDCANCELREASONS.TypeKeys.toTypedArray()
      }
    }
    //Property
    if (policyPeriod.BOPLineExists) {
      if (job.Source == CancellationSource.TC_CARRIER) {
        if (policyPeriod.BaseState == Jurisdiction.TC_CA) {
          return ReasonCode.TF_BOP_CARRIERCANCELREASONS.TypeKeys.toTypedArray()
        }
        return ReasonCode.TF_BOP_CARRIERCANCELREASONS.TypeKeys?.where(\elt -> elt != ReasonCode.TC_STDENTALASSOCNONMEMBERAPPLICANT_TDIC)?.toTypedArray()
      } else if (job.Source == CancellationSource.TC_INSURED) {
        return ReasonCode.TF_BOP_INSUREDCANCELREASONS.TypeKeys.toTypedArray()
      }
    }
    //Liability
    if (policyPeriod.GLLineExists) {
      if (policyPeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or policyPeriod.Offering.CodeIdentifier == "PLOccurence_TDIC") {
        if (job.Source == CancellationSource.TC_CARRIER) {
          if (policyPeriod.BaseState == Jurisdiction.TC_CA) {
            return ReasonCode.TF_PL_CM_OCCARRIERCANCELREASONS.TypeKeys.toTypedArray()
          } else {
            return ReasonCode.TF_PL_CM_OCCARRIERCANCELREASONS.TypeKeys?.where(\elt -> elt != ReasonCode.TC_STDENTALASSOCNONMEMBERAPPLICANT_TDIC)?.toTypedArray()
          }
        } else if (job.Source == CancellationSource.TC_INSURED) {
          return ReasonCode.TF_PL_CM_OCINSUREDCANCELREASONS.TypeKeys.toTypedArray()
        }
      } else if (policyPeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
        if (job.Source == CancellationSource.TC_CARRIER) {
          if (policyPeriod.BaseState == Jurisdiction.TC_CA) {
            return ReasonCode.TF_PL_CYBCARRIERCANCELREASONS.TypeKeys.toTypedArray()
          }
          return ReasonCode.TF_PL_CYBCARRIERCANCELREASONS.TypeKeys?.where(\elt -> elt != ReasonCode.TC_STDENTALASSOCNONMEMBERAPPLICANT_TDIC)?.toTypedArray()
        } else if (job.Source == CancellationSource.TC_INSURED) {
          return ReasonCode.TF_PL_CYBINSUREDCANCELREASONS.TypeKeys.toTypedArray()
        }
      }
    }
    return typekey.ReasonCode.getTypeKeys(false).toTypedArray()
  }

  // Added by Jeff Lin, 11/11/2019, 4:04 PM, GPC-584/549, PL/CP Initiate Cancellation, On Policy Change of the PLCM/CYB Canceled Policies, set the two values on Extended Report Endorsements
  public static function setCancelReasonAndRatedForExtendedCovEndorsement(polChgPeriod: PolicyPeriod, inForcePeriod: PolicyPeriod) {
    if (inForcePeriod.Cancellation != null) {
      if (inForcePeriod.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
        var covEnabled = false
        if (not polChgPeriod.GLLine.GLExtendedReportPeriodCov_TDICExists) {
          covEnabled = true
          polChgPeriod.GLLine.setCoverageExists(polChgPeriod.GLLine.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLExtendedReportPeriodCov_TDIC"), true)
        }
        if (polChgPeriod.GLLine.GLExtendedReportPeriodCov_TDICExists) {
          polChgPeriod.GLLine.GLExtendedReportPeriodCov_TDIC.GLERPReason_TDICTerm.setValueFromString(inForcePeriod.Cancellation.CancelReasonCode.DisplayName)
          var isRated = false
          if (inForcePeriod.Cancellation.CancelReasonCode == ReasonCode.TC_NOLONGERINBUSINESSDEC_TDIC or inForcePeriod.Cancellation.CancelReasonCode == ReasonCode.TC_PERMANENTLYDISABLED_TDIC or
              (inForcePeriod.Cancellation.CancelReasonCode == ReasonCode.TC_NOLONGERINBUSINESSRET_TDIC and inForcePeriod.Policy.OriginalEffectiveDate.addYears(3) < inForcePeriod.EditEffectiveDate)) {
            isRated = true
          }
        }
      }
      if (inForcePeriod.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
        var covEnabled = false
        if ((not polChgPeriod.GLLine.GLCyvSupplementalERECov_TDICExists) and
            (!(polChgPeriod?.Job?.ChangeReasons?.hasMatch(\elt1 -> elt1?.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER)))) {
          covEnabled = true
          polChgPeriod.GLLine.setCoverageExists(polChgPeriod.GLLine.PolicyLine.Pattern.getCoveragePatternByCodeIdentifier("GLCyvSupplementalERECov_TDIC"), true)
        }
        if (polChgPeriod.GLLine.GLCyvSupplementalERECov_TDICExists) {
          polChgPeriod.GLLine.GLCyvSupplementalERECov_TDIC.GLCybEREReason_TDICTerm.setValue(inForcePeriod.Cancellation.CancelReasonCode.DisplayName)
          polChgPeriod.GLLine.GLCyvSupplementalERECov_TDIC.GLCybERERated_TDICTerm.setValue(true) // Always default to Yes
        }
      }
    }
  }


  public static function setExtendedReportingEffDates(pattern:gw.api.productmodel.ClausePattern,glLine : GLLine) {
    var additionalEREEREDEPLCYBERECoverages = {"GLExtendedReportPeriodCov_TDIC", "GLExtendedReportPeriodDPLCov_TDIC", "GLCyvSupplementalERECov_TDIC"}
    if (additionalEREEREDEPLCYBERECoverages.contains(pattern.CodeIdentifier)) {
      if((pattern.CodeIdentifier == "GLExtendedReportPeriodDPLCov_TDIC") and ((glLine?.PolicyLine as GLLine).CoverableState == Jurisdiction.TC_AK)
          and (glLine?.PolicyLine?.Branch?.Canceled or glLine?.PolicyLine?.Branch?.Status == PolicyPeriodStatus.TC_NONRENEWED)){
        (glLine?.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(glLine?.PolicyLine?.Branch?.PeriodStart)
        (glLine?.PolicyLine as GLLine)?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(glLine?.PolicyLine?.Branch?.PeriodStart.addYears(5))
      }
      else
      if (glLine?.Branch?.BasedOn?.Canceled) {
        var cancellationDate = glLine.Branch.CancellationDate
        if (glLine?.GLExtendedReportPeriodCov_TDICExists) {
          //glLine?.GLExtendedReportPeriodCov_TDIC?.GLERPEEffectiveDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.EditEffectiveDate)
          glLine?.GLExtendedReportPeriodCov_TDIC?.GLERPEEffectiveDate_TDICTerm?.setValue(cancellationDate)
        }
        if (glLine?.GLCyvSupplementalERECov_TDICExists) {
          /*glLine?.GLCyvSupplementalERECov_TDIC?.GLCybEREEffectiveDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.EditEffectiveDate)
          glLine?.GLCyvSupplementalERECov_TDIC?.GLCybEREExpirationDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.EditEffectiveDate.addYears(1))*/
          glLine?.GLCyvSupplementalERECov_TDIC?.GLCybEREEffectiveDate_TDICTerm?.setValue(cancellationDate)
          glLine?.GLCyvSupplementalERECov_TDIC?.GLCybEREExpirationDate_TDICTerm?.setValue(cancellationDate.addYears(1))
        }
        if (glLine?.GLExtendedReportPeriodDPLCov_TDICExists) {
          /*glLine?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.EditEffectiveDate)
          glLine?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.EditEffectiveDate.addYears(1))*/
          glLine?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(cancellationDate)
          glLine?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(cancellationDate.addYears(1))
        }
      } else if (glLine?.Branch?.BasedOn?.PeriodDisplayStatus == PolicyPeriodStatus.TC_EXPIRED.Code) {
        if (glLine?.GLExtendedReportPeriodCov_TDICExists) {
          glLine?.GLExtendedReportPeriodCov_TDIC?.GLERPEEffectiveDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.PeriodEnd)
        }
        if (glLine?.GLCyvSupplementalERECov_TDICExists) {
          glLine?.GLCyvSupplementalERECov_TDIC?.GLCybEREEffectiveDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.PeriodEnd)
          glLine?.GLCyvSupplementalERECov_TDIC?.GLCybEREExpirationDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.PeriodEnd.addYears(1))
        }
        if (glLine?.GLExtendedReportPeriodDPLCov_TDICExists) {
          glLine?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.PeriodEnd)
          glLine?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(glLine?.Branch?.BasedOn?.PeriodEnd.addYears(1))
        }
      } else if (pattern.CodeIdentifier == "GLExtendedReportPeriodDPLCov_TDIC" and glLine.JobType == typekey.Job.TC_POLICYCHANGE) {
        glLine?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLEffDate_TDICTerm?.setValue(glLine?.Branch?.EditEffectiveDate)
        glLine?.GLExtendedReportPeriodDPLCov_TDIC?.GLERPDLExpDate_TDICTerm?.setValue(glLine?.Branch?.EditEffectiveDate.addYears(1))
      }
    }
  }
}