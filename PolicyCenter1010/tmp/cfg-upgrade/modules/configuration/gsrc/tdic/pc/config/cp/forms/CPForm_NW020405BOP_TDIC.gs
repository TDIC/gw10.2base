package tdic.pc.config.cp.forms
/**
 * Created with IntelliJ IDEA.
 * User: AnudeepK
 * Date: 07/27/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class CPForm_NW020405BOP_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var bopLocations = context.Period.BOPLine?.BOPLocations
    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC" and bopLocations.HasElements and context.Period.isDIMSPolicy_TDIC) {
      if (context.Period.Job typeis Renewal and bopLocations*.Buildings.HasElements ) {
        if(bopLocations.hasMatch(\loc -> loc.Buildings.hasMatch(\building -> !building.BOPBuildingCovExists))){
          return  true
        }
      }
    }
    return false
  }
}