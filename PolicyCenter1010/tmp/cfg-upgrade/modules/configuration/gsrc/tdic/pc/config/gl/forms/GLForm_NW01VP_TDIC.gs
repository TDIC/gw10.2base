package tdic.pc.config.gl.forms

uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.forms.FormInferenceContext
/**
 * Created with IntelliJ IDEA.
 * User: AnudeepK
 * Date: 07/27/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */

class GLForm_NW01VP_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC"){
      if (context.Period.Job typeis Renewal && context.Period.isDIMSPolicy_TDIC ) {
        return (context.Period.GLLine.Exposures.hasMatch(\exp -> exp.ClassCode_TDIC == ClassCode_TDIC.TC_01))
      }
    }
    return false
  }
}