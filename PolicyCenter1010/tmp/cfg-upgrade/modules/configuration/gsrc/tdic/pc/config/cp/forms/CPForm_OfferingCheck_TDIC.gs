package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 12/23/2019
 * Time: 5:30 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_OfferingCheck_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var cPBOPFormPatterns = {"BOP1200AS", "BOP2030(A)AS", "BOP2030(B)AS", "BOP2055NJ", "BOP2122AS", "BOP2122IL",
        "BOP2190IL", "BOP2210NV", "BOP2215MN", "BOP2500AZ", "BOP2500CA", "BOP2500HI", "BOP2500IL", "BOP2500MN",
        "BOP2500ND", "BOP2500NJ", "BOP2500NV", "BOP2501MN", "BOP2501ND", "BOP2503PA",
        "BOP2506IL", "BOP2506PA", "BOP2508IL", "BOP2515(A)AS","BOP2515(B)AS", "BOP2517(A)AS", "BOP2517(B)AS",
        "BOP2500WA", "BOP2210WA", "BOP2510WA", "BOP2500OR", "BOP2210OR", "BOP2500ID", "BOP2500TN","BOP2220(A)WA","BOP2220(B)WA"}
    var cPLRPFormPattern = {"LRP1210AS", "LRP2055NJ", "LRP2122AS", "LRP2122IL", "LRP2190IL", "LRP2210NV",
        "LRP2215MN", "LRP2500AZ", "LRP2500CA", "LRP2500HI", "LRP2500IL", "LRP2500MN", "LRP2500NJ", "LRP2500NV", "LRP2501MN",
        "LRP2501ND", "LRP2506IL", "LRP2506PA", "LRP2508IL", "LRP2515(A)AS", "LRP2515(B)AS", "LRP2518(A)AS", "LRP2518(B)AS",
        "LRP2600AS", "LRP2500ND", "LRP2500WA", "LRP2500OR", "LRP2500ID", "LRP2500TN", "LRP2210OR", "LRP2210WA","LRP2220(A)WA","LRP2220(B)WA"}
    if (cPBOPFormPatterns.contains(this.Pattern.Code)) {
      return context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC"
    }
    if (cPLRPFormPattern.contains(this.Pattern.Code)) {
      return context.Period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC"
    }
    return false
  }
}