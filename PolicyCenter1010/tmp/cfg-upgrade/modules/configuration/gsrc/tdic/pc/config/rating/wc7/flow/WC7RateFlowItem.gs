package tdic.pc.config.rating.wc7.flow

uses gw.rating.CostData
uses tdic.pc.config.rating.wc7.context.WC7RateFlowContext


class WC7RateFlowItem extends RateFlowItem<WC7RateFlowContext> {
  construct(ord : int) {

    super(ord)

  }

  override function execute(context : WC7RateFlowContext) : List<CostData<Cost, PolicyLine>> {
    return {}
  }
}