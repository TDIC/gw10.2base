package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode
uses entity.FormAssociation

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 2/5/2020
 * Create Time: 4:49 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_LRP2502AS_TDIC extends AbstractMultipleCopiesForm<BOPLocation> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<BOPLocation> {
    if (context.Period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
      return context.Period.BOPLine?.BOPLocations?.where(\loc -> loc.Buildings?.hasMatch(\building -> building.BOPEqSpBldgCovExists)).toList()
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "BOPLocation_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("AddressLine1", _entity.Location.AddressLine1))
    contentNode.addChild(createTextNode("City", _entity.Location.City))
    contentNode.addChild(createTextNode("PostalCode", _entity.Location.PostalCode))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }
}

