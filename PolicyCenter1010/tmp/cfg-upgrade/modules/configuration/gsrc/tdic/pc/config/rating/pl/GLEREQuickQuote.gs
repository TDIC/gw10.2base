package tdic.pc.config.rating.pl

uses gw.api.profiler.PCProfilerTag
uses gw.lob.gl.rating.GLCostData
uses tdic.pc.config.rating.pl.rater.GLEREClaimsMadeRater

/**
 * BrittoS 04/28/2020, Quick Quote for ERE offer
 */

class GLEREQuickQuote extends GLRatingEngine {

  private var _ereDate : Date

  construct(line : GLLine, ereDate : Date, minimumRatingLevel : RateBookStatus) {
    super(line, minimumRatingLevel)
    _ereDate = ereDate
  }

  /**
   * @return
   */
  public function requestQuote() : Set<GLCostData> {
    try {
      var lineVersions = getVersionsOnDates(AllEffectiveDates).where(\l -> getSliceDate(l) >= _ereDate)
      if(not lineVersions.HasElements) {
        lineVersions = getVersionsOnDates(AllEffectiveDates).where(\l -> getSliceDate(l) >= Branch.EditEffectiveDate)
      }
      PCProfilerTag.RATE_WINDOW.execute(\-> rateWindow(lineVersions.first()))
    } catch (e : Exception) { throw e }
    return getCosts()
  }

  public function getCosts() : Set<GLCostData> {
    return CostCache.Values.toSet()
  }


  override protected function rateWindow(lineVersion : GLLine) {
    var rater = new GLEREClaimsMadeRater(lineVersion, this)
    rater.EREEffectiveDate = Branch.PeriodStart
    rater.EREExpirationDate = _ereDate
    rater.rate()
  }

  override function isERERating() : Boolean {
    return true
  }

  override function getCancellationDate() : Date {
    return _ereDate
  }

  override function getBranchEditEffectiveDate() : Date {
    if(_ereDate == Branch.PeriodStart.addDays(1)) { //one day after the policy effective date
      return _ereDate
    }
    return _ereDate.addDays(-1)
  }

  override function getNextSliceDate(start : Date) : Date {
    var nextSliceDate = getNextSliceDateAfter(start)
    if(Branch.isCanceled() and nextSliceDate.compareIgnoreTime(_ereDate.addDays(-1)) == 0) {
      return _ereDate
    }
    if(nextSliceDate.afterOrEqual(_ereDate)) {
      return _ereDate
    }
    return nextSliceDate
  }
}