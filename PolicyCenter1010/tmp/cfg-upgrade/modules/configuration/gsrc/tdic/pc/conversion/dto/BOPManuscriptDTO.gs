package tdic.pc.conversion.dto

class BOPManuscriptDTO {

  var addInsured : String as AdditionalInsured
  var manuscriptType : String as Manuscript_Type
  var manuscriptDes : String as Manuscript_Des
  var lossPayee : String as LossPayee
  var mortgagee : String as Mortgagee

}