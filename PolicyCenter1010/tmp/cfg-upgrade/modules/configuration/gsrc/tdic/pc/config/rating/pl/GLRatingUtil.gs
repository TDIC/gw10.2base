package tdic.pc.config.rating.pl

uses com.guidewire.pl.system.util.DateRange
uses entity.GLSplitParameters_TDIC
uses tdic.pc.config.gl.historicaldata.GLHistoryHelper

class GLRatingUtil {
  private static var _hisotySteps : Map<Integer, String>as readonly HistorySteps = {
      1 -> "1st Year",
      2 -> "2nd Year",
      3 -> "3rd Year",
      4 -> "4th Year",
      5 -> "Mature"
  }

  /**
   * @param newGradEffDate
   * @return
   */
  public static function createNewGradSplitYearMap(newGradEffDate : Date) : LinkedHashMap<DateRange, int> {
    var newGradDateMap = new LinkedHashMap<DateRange, int>()
    newGradDateMap.put(DateRange.allDatesBetween(newGradEffDate, newGradEffDate.addYears(1)), 1)
    newGradDateMap.put(DateRange.allDatesBetween(newGradEffDate.addYears(1), newGradEffDate.addYears(2)), 2)
    newGradDateMap.put(DateRange.allDatesBetween(newGradEffDate.addYears(2), newGradEffDate.addYears(3)), 3)
    return newGradDateMap
  }

  /**
   * @return
   */
  public static function getSplitParameters(line : GLLine) : GLSplitParameters_TDIC[] {

    var stepYearMap = createStepYear(line)
    var newSplitParams = createSplitParams(stepYearMap, line)

    var existingParams : List<GLSplitParameters_TDIC> = {}
    var newParams : List<GLSplitParameters_TDIC> = {}

    for (newParam in newSplitParams) {
      var matchedParams = line.GLSplitParameters_TDIC.firstWhere(\existingParam -> match(newParam, existingParam))
      if (matchedParams != null) {
        existingParams.add(matchedParams)
      } else {
        newParams.add(newParam)
        newParam.GLLine = line
      }
    }
    return existingParams.union(newParams).toTypedArray()
  }

  /**
   * @param param1
   * @param param2
   * @return
   */
  protected static function match(param1 : GLSplitParameters_TDIC, param2 : GLSplitParameters_TDIC) : Boolean {
    return param1.EffDate == param2.EffDate
        and param1.ExpDate == param2.ExpDate
        and param1.ClassCode == param2.ClassCode
        and param1.TerritoryCode == param2.TerritoryCode
        and param1.SpecialityCode == param2.SpecialityCode
        and param1.splitYear == param2.splitYear
        and param1.Discount == param2.Discount
  }

  public static function checkIfDateInRangeNotIncluded(inputDate : Date, startDate : Date, expirationDate : Date) : boolean {
    var dateInRange = false
    if (inputDate > startDate and inputDate < expirationDate) {
      dateInRange = true
    } else {
      dateInRange = false
    }
    return dateInRange
  }

  public static function checkIfDateInRangeIncluded(inputDate : Date, startDate : Date, expirationDate : Date) : boolean {
    var dateInRange = false
    if (inputDate >= startDate and inputDate <= expirationDate) {
      dateInRange = true
    } else {
      dateInRange = false
    }
    return dateInRange
  }



/*******************************************************************************Private Functions*******************************************************************/

  private static function initializeStepYearParam(line : GLLine) : Map<Integer, DateRange> {
    var glLineExpDate = line.ExpirationDate.trimToMidnight()
    var stepYears : Map<Integer, DateRange> = {}
    stepYears.put(1, DateRange.allDatesBetween(glLineExpDate, glLineExpDate.addYears(-1)))
    for (year in 1..4) {
      stepYears.put((year + 1), DateRange.allDatesBetween(glLineExpDate.addYears((-year)), glLineExpDate.addYears(-(year + 1))))
    }
    return stepYears
  }

  /**
   *
   */
  private static function populateRatingParameters(policyPeriod : PolicyPeriod, effDate : Date, expDate : Date, classCode : String, terrCode : String, specialityCode : String, discount : GLHistoricalDiscount_TDIC, stepYear : String) : GLSplitParameters_TDIC {
    var splitGlHistParam : GLSplitParameters_TDIC
    splitGlHistParam = new GLSplitParameters_TDIC(policyPeriod)
    splitGlHistParam.EffDate = effDate
    splitGlHistParam.ExpDate = expDate
    splitGlHistParam.ClassCode = classCode
    splitGlHistParam.TerritoryCode = terrCode
    splitGlHistParam.SpecialityCode = specialityCode
    splitGlHistParam.splitYear = stepYear
    splitGlHistParam.Discount = discount
    return splitGlHistParam
  }

  /**
   * @return
   */
  private static function createStepYear(line : GLLine) : LinkedHashMap<DateRange, String> {
    var stepYears = initializeStepYearParam(line)
    var stepYearMap = new LinkedHashMap<DateRange, String>()
    var retroDate = line.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm?.Value
    if (retroDate <= line.ExpirationDate.addYears(-5)) {
      for (year in 1..5) {
        stepYearMap.put(stepYears.get(year), _hisotySteps.get(year))
      }
    } else {
      stepYearMap = createStepYearForRetroDate(stepYears, line)
    }
    return stepYearMap
  }

  private static function createStepYearForRetroDate(stepYears : Map<Integer, DateRange>, Line : GLLine) : LinkedHashMap<DateRange, String> {
    var expDate = Line.ExpirationDate.trimToMidnight()
    var retroDate = Line.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm?.Value.trimToMidnight()
    var stepYearMap = new LinkedHashMap<DateRange, String>()

    for (year in stepYears.Keys.order()) {
      if (retroDate.afterOrEqual(stepYears.get(year).End)) {
        if (year == 1) {
          stepYearMap.put(DateRange.allDatesBetween(expDate, retroDate), _hisotySteps.get(year))
        } else {
          stepYearMap.put(DateRange.allDatesBetween(expDate.addYears(-(year - 1)), retroDate), _hisotySteps.get(year))
        }
        break
      } else {
        if (year == 1) {
          stepYearMap.put(DateRange.allDatesBetween(expDate, expDate.addYears(-year)), _hisotySteps.get(year))
        } else {
          stepYearMap.put(DateRange.allDatesBetween(expDate.addYears(-(year - 1)), expDate.addYears(-year)), _hisotySteps.get(year))
        }
      }
    }
    return stepYearMap
  }

  /**
   * create by: AnkitaG
   *
   * @param line : GLLine
   * @description: Function to create split period data
   * @create time:  10/23/2019
   * @return: null
   */
  private static function createSplitParams(stepYear : LinkedHashMap<DateRange, String>, Line : GLLine) : ArrayList<GLSplitParameters_TDIC> {
    var splitParams = new ArrayList<GLSplitParameters_TDIC>()
    var highDate : Date
    var lowDate : Date
    var currentClassCode : String
    var currentTerrCode : String
    var currentSpecialityCode : String
    var currentDiscountCode : GLHistoricalDiscount_TDIC
    var currentStepYearCode : String
    var histEffDate : Date
    var histExpDate : Date
    var histClassCode : String
    var histTerrCode : String
    var histSpecialityCode : String
    var histDiscountCode : GLHistoricalDiscount_TDIC
    var stepYearCode : String
    var stepEffDate : Date
    var stepExpDate : Date
    var ratingHistParams = GLHistoryHelper.getHistoryFor(Line.Branch).sortByDescending(\param -> param.EffectiveDate).toList()
    for (histParam in ratingHistParams) {
      histEffDate = histParam.EffectiveDate.trimToMidnight()
      histExpDate = histParam.ExpirationDate.trimToMidnight()
      histClassCode = histParam.ClassCode
      histTerrCode = histParam.TerritoryCode
      histSpecialityCode = histParam.SpecialityCode
      histDiscountCode = histParam.Discount
      var flag = 0
      for (stepHistoryParam in stepYear.entrySet()) {
        stepYearCode = stepHistoryParam.getValue()
        stepEffDate = stepHistoryParam.getKey().End
        stepExpDate = stepHistoryParam.getKey().Start
        if (histExpDate > stepEffDate and histExpDate <= stepExpDate) {
          if (stepExpDate == histExpDate and stepEffDate == histEffDate) {
            splitParams.add(populateRatingParameters(Line.Branch, histEffDate, histExpDate, histClassCode, histTerrCode, histSpecialityCode, histDiscountCode, stepYearCode))
            break
          } else if (checkIfDateInRangeNotIncluded(histExpDate, stepEffDate, stepExpDate) and histEffDate >= stepEffDate and currentClassCode == null and currentSpecialityCode == null) {
            splitParams.add(populateRatingParameters(Line.Branch, histEffDate, histExpDate, histClassCode, histTerrCode, histSpecialityCode, histDiscountCode, stepYearCode))
            currentClassCode = histClassCode
            currentTerrCode = histTerrCode
            currentSpecialityCode = histSpecialityCode
            currentDiscountCode = histDiscountCode
            currentStepYearCode = stepYearCode
            break
          } else if (checkIfDateInRangeNotIncluded(histExpDate, stepEffDate, stepExpDate) and histEffDate < stepEffDate and currentClassCode == null and currentSpecialityCode == null) {
            splitParams.add(populateRatingParameters(Line.Branch, stepEffDate, histExpDate, histClassCode, histTerrCode, histSpecialityCode, histDiscountCode, stepYearCode))
            currentClassCode = histClassCode
            currentTerrCode = histTerrCode
            currentSpecialityCode = histSpecialityCode
            currentDiscountCode = histDiscountCode
            currentStepYearCode = stepYearCode
            flag = 1
          }

          if (stepExpDate == histExpDate and checkIfDateInRangeIncluded(histEffDate, stepEffDate, stepExpDate)) {
            currentClassCode = histClassCode
            currentTerrCode = histTerrCode
            currentSpecialityCode = histSpecialityCode
            currentDiscountCode = histDiscountCode
            currentStepYearCode = stepYearCode
            lowDate = histEffDate
            highDate = histExpDate
            break
          } else if (currentClassCode == histClassCode and currentTerrCode == histTerrCode and currentSpecialityCode == histSpecialityCode and currentDiscountCode == histDiscountCode
              and checkIfDateInRangeNotIncluded(histExpDate, stepEffDate, stepExpDate)
              and checkIfDateInRangeNotIncluded(histEffDate, stepEffDate, stepExpDate)) {
            lowDate = histEffDate
            currentStepYearCode = stepYearCode
            break
          } else if (currentClassCode == histClassCode and currentTerrCode == histTerrCode and currentSpecialityCode == histSpecialityCode and currentDiscountCode == histDiscountCode
              and checkIfDateInRangeNotIncluded(histExpDate, stepEffDate, stepExpDate)) {
            if (histEffDate == stepEffDate) {
              lowDate = stepEffDate
              currentStepYearCode = stepYearCode
              splitParams.add(populateRatingParameters(Line.Branch, lowDate, highDate, histClassCode, histTerrCode, histSpecialityCode, histDiscountCode, stepYearCode))
              break
            } else if (histEffDate < stepEffDate and flag != 1) {
              lowDate = stepEffDate
              currentStepYearCode = stepYearCode
              splitParams.add(populateRatingParameters(Line.Branch, lowDate, highDate, histClassCode, histTerrCode, histSpecialityCode, histDiscountCode, stepYearCode))
            }
          } else if (currentClassCode != histClassCode or currentTerrCode != histTerrCode or currentSpecialityCode != histSpecialityCode or currentDiscountCode != histDiscountCode) {
            if (histEffDate > stepEffDate) {
              splitParams.add(populateRatingParameters(Line.Branch, lowDate, highDate, currentClassCode, currentTerrCode, currentSpecialityCode, currentDiscountCode, stepYearCode))
              currentClassCode = histClassCode
              currentTerrCode = histTerrCode
              currentSpecialityCode = histSpecialityCode
              currentDiscountCode = histDiscountCode
              currentStepYearCode = stepYearCode
              highDate = histExpDate
              lowDate = histEffDate
              break
            } else if (histEffDate <= stepEffDate and flag != 1) {
              splitParams.add(populateRatingParameters(Line.Branch, lowDate, highDate, currentClassCode, currentTerrCode, currentSpecialityCode, currentDiscountCode, stepYearCode))
              splitParams.add(populateRatingParameters(Line.Branch, stepEffDate, lowDate, histClassCode, histTerrCode, histSpecialityCode, histDiscountCode, stepYearCode))
            }
          }
        }
        if (histExpDate > stepExpDate and checkIfDateInRangeNotIncluded(histEffDate, stepEffDate, stepExpDate)) {
          highDate = stepExpDate
          lowDate = histEffDate
          currentClassCode = histClassCode
          currentTerrCode = histTerrCode
          currentSpecialityCode = histSpecialityCode
          currentDiscountCode = histDiscountCode
          currentStepYearCode = null
          break
        } else if (histExpDate > stepExpDate and histEffDate < stepExpDate and histEffDate == stepEffDate) {
          highDate = stepExpDate
          lowDate = histEffDate
          currentClassCode = histClassCode
          currentTerrCode = histTerrCode
          currentSpecialityCode = histSpecialityCode
          currentDiscountCode = histDiscountCode
          currentStepYearCode = null
          splitParams.add(populateRatingParameters(Line.Branch, lowDate, highDate, currentClassCode, currentTerrCode, currentSpecialityCode, currentDiscountCode, stepYearCode))
          break
        }
      }
    }
    return splitParams
  }
}