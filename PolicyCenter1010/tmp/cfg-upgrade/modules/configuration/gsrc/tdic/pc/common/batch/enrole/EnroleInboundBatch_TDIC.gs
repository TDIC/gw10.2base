package tdic.pc.common.batch.enrole

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.pc.common.batch.enrole.exception.EnroleException

/**
 * EnroleOutboundBatch_TDIC - File based integration to read dentiest attended course details from enrole System and
 * load into table: EnrolePolicyDetails of GWINT DB.
 * Created by RambabuN on 10/14/2019.
 */
class EnroleInboundBatch_TDIC extends BatchProcessBase {

  static final var _logger = LoggerFactory.getLogger("TDIC_Enrole")

  /**
   * Key for looking up the failure notification email addresses from the integration database
   * (value: InfraIssueNotificationEmail).
   */
  static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "PCInfraIssueNotificationEmail"
  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  var _failureNotificationEmail : String = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
  /**
   * Notification email's subject for EnroleInbound batch failures.
   */
  static final var EMAIL_SUBJECT_FAILURE : String =
      "enrole Inbound Batch Job Failure on server ::${gw.api.system.server.ServerUtil.ServerId}"

  /**
   * Notification email's subject for EnroleInbound batch failures.
   */
  static final var TERMINATE_SUB =
      "Terminate::${gw.api.system.server.ServerUtil.ServerId}-enrole Inbound batch Terminated"

  construct() {
    super(BatchProcessType.TC_ENROLEINBOUNDBATCH_TDIC)
  }

  /**
   * dowork() is responsibel to load Enrole inbound file course details policy center.
   */
  protected function doWork() {
    _logger.trace("EnroleInboundBatch_TDIC#doWork() - Entering")

    try {

      if (TerminateRequested) {
        _logger.warn("EnroleInboundBatch_TDIC#doWork() " +
            "- Terminate requested during doWork() before loading Enrole inbound to policycenter")
        EmailUtil.sendEmail(_failureNotificationEmail, TERMINATE_SUB, "enrole policy batch is terminated.")
        return
      }

      EnroleHelper.loadEnroleInboundFile()
      incrementOperationsCompleted()

    } catch (ex : EnroleException) {
      // Allowing Batch process to continue and logging the Exception
      _logger.error("Exception in the EnroleInboundBatch_TDIC", ex)
      incrementOperationsFailed()
      EmailUtil.sendEmail(_failureNotificationEmail,
          EMAIL_SUBJECT_FAILURE, "Error reading inbound file from enrole: " + ex.Message)
      throw ex
    }
    _logger.trace("EnroleInboundBatch_TDIC#doWork() - Exiting.")
  }

  /**
   * To terminate batch process.
   * @return boolean
   */
  override function requestTermination() : boolean {
    // This will set the Termination request flag
    super.requestTermination()
    _logger.info("EnroleInboundBatch_TDIC#doWork(): requestTermination Terminated.")
    // It will return true to signal that the attempt is made to stop the process in doWork method.
    return true
  }

}