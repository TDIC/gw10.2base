package tdic.pc.common.batch.generalledger.prmpolicies

uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLinesInterface
uses java.util.Date
uses java.util.List

/**
 * US627
 * 12/23/2014 Kesava Tavva
 *
 * This class is to calculate aggregated cost values for all premium line types
 * i.e Premium, Expense constant, Terrorism premium and state surcharges from the qualified
 * policy periods for a given batch run period.
 */
abstract class TDIC_PremiumPoliciesBase implements TDIC_PremiumPoliciesInterface {
  protected static final var CALC_ORDER_PRM : int = 400
  protected static final var CALC_ORDER_EXP_CONST : int = 410
  protected static final var CALC_ORDER_TERRORISM : int = 420
  protected static final var CALC_ORDER_SURCHARGES : int = 430

  private var batchPeriodStartDate : Date
  private var batchPeriodEndDate : Date
  private var _prmLines : TDIC_PremiumLinesInterface

  /**
   * US627
   * 12/23/2014 Kesava Tavva
   *
   * Constructor to initialize PremiumPolicies instance with Batch period start and end dates
   * and with respective premium lines
   */
  @Param("periodStartDate","GL report batch period start date")
  @Param("periodEndDate","GL report batch period End date")
  @Param("prmLines","Instance of premium line object related to GL report type")
  construct(periodStartDate : Date, periodEndDate: Date, prmLines : TDIC_PremiumLinesInterface){
    batchPeriodStartDate = periodStartDate
    batchPeriodEndDate = periodEndDate
    _prmLines =  prmLines
  }

  /**
   * US627
   * 12/23/2014 Kesava Tavva
   *
   * This function returns batch period start date for each batch run
   */
  @Returns("Returns Batch period start date")
  protected function getBatchPeriodStartDate() : Date {
    return batchPeriodStartDate.addMinutes(1)
  }

  /**
   * US627
   * 12/23/2014 Kesava Tavva
   *
   * This function returns batch period end date for each batch run
   */
  @Returns("Returns Batch period end date")
  protected function getBatchPeriodEndDate(psd: Date): Date {
    return psd.addMonths(1).addMinutes(- 1)
  }

  /**
   * US627
   * 12/23/2014 Kesava Tavva
   *
   * This function returns instance of Premium lines interface based on type of GL report.
   */
  @Returns("Returns instance of TDIC_PremiumLinesInterface")
  override function getPremiumLines(): TDIC_PremiumLinesInterface {
    return _prmLines
  }

  /**
   * US627
   * 12/23/2014 Kesava Tavva
   *
   * This function filters policy periods based on policy transaction that have been
   * available for processing batch reporting.
   */
  @Param("pps","List of all qualified policy periods for a given batch period")
  @Returns("Returns list of filtered policy periods.")
  protected function filterPolicyPeriods(pps : List<PolicyPeriod>) : List<PolicyPeriod> {
    return pps?.where( \ pp -> (pp.Job.Subtype == typekey.Job.TC_SUBMISSION
        || pp.Job.Subtype == typekey.Job.TC_POLICYCHANGE
        || pp.Job.Subtype == typekey.Job.TC_AUDIT
        || pp.Job.Subtype == typekey.Job.TC_RENEWAL
        || pp.Job.Subtype == typekey.Job.TC_CANCELLATION
        || pp.Job.Subtype == typekey.Job.TC_REINSTATEMENT
        || pp.Job.Subtype == typekey.Job.TC_REWRITE
        || pp.Job.Subtype == typekey.Job.TC_REWRITENEWACCOUNT
    )==true)
  }
}