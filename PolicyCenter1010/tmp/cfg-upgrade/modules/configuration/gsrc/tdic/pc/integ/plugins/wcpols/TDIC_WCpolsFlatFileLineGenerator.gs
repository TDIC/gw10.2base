package tdic.pc.integ.plugins.wcpols

uses java.util.HashMap
uses tdic.pc.integ.plugins.wcpols.helper.TDIC_FlatFileUtilityHelper
uses tdic.util.flatfile.model.classes.Record
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 1/5/15
 * Time: 4:50 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class TDIC_WCpolsFlatFileLineGenerator {

  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCPOLSREPORT")

  function objHashMap(): HashMap<String, String> {

    var  fields = (typeof this).TypeInfo.Properties
    var map = new HashMap<String, String>()
    for(var f in fields){
      //GINTEG_521 : Ignored 'Class' field mapping as the same is not present in the GX Model
      if(f.Name !="itype" and (not f.Name.equalsIgnoreCase("Class")) and f.Name != "IntrinsicType")
        map.put(f.Name, this[f.Name] as String)
    }
    return map
  }

  function createFlatFileLine(record:Record):String{

    var flatFileLine=""

    for(field in record.Fields){                     //For each field in the record
      var vals = objHashMap()
      var value = vals.get(field.Name)                //Get value from Xml
      flatFileLine += TDIC_FlatFileUtilityHelper.formatString(value, field.Length, field.Truncate, field.Justify, field.Fill, field.Format, field.Strip) //Format string and add it to flat file line
    }
    _logger.debug(flatFileLine)
    return flatFileLine
  }

}