package tdic.pc.config.gl.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode
uses entity.FormAssociation

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2010BAS_TDIC extends AbstractMultipleCopiesForm<GLCertofInsSched_TDIC> {
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLCertofInsSched_TDIC> {
    var period = context.Period
    var CertHolder : ArrayList<GLCertofInsSched_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      var GLCertHolders = period.GLLine?.GLCertofInsSched_TDIC
      if(period.GLLine.GLCOICov_TDICExists and GLCertHolders.HasElements){
        if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE){
          var GLCertHoldersOnPolicyChange = GLCertHolders.where(\certHolder -> certHolder.BasedOn == null)
          var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
          var certHolderModifiedOnPolicyChange = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis GLCertofInsSched_TDIC)*.Bean
          var polCertHolderModifOnPolChange = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyCertificateHolder_TDIC)*.Bean
          var mansuScriptItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
              changedItem typeis DiffProperty) and changedItem.Bean typeis GLManuscript_TDIC)*.Bean
          if(GLCertHoldersOnPolicyChange.HasElements){
            CertHolder.add(GLCertHoldersOnPolicyChange.first())
          }
          else if(GLCertHolders.hasMatch(\ch -> certHolderModifiedOnPolicyChange.contains(ch))){
            CertHolder.add(GLCertHolders?.firstWhere(\ch -> certHolderModifiedOnPolicyChange.contains(ch)))
          }
          /*GWPS-1764 Logic to Compare Certificate Holder modified on policy change and GL Cert holder schedule*/
          else if(polCertHolderModifOnPolChange.HasElements){
            polCertHolderModifOnPolChange.each(\policyCertHolder -> {
              GLCertHolders.each(\GLCertHolder ->{
                if(GLCertHolder.CertificateHolder.equals(policyCertHolder as PolicyCertificateHolder_TDIC) && CertHolder.Count==0){
                  CertHolder.add(GLCertHolder)
                }
              })
            })
          }
          else if(mansuScriptItemsModified.HasElements){
            var GLAddlCertHolderManuscriptModified=(mansuScriptItemsModified.firstWhere(\elt -> (elt as GLManuscript_TDIC).CertificateHolder!=null)) as GLManuscript_TDIC
            if(GLAddlCertHolderManuscriptModified!=null){
              CertHolder.add(GLAddlCertHolderManuscriptModified.GeneralLiabilityLine.GLCertofInsSched_TDIC.first())
            }
          }
        }
        else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
          if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
            CertHolder.add(GLCertHolders.first())
          }
        }
        else {
          CertHolder.add(GLCertHolders.first())
        }
      }
      return CertHolder.HasElements? CertHolder.toList() : {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLCertofInsSched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("LTEffectiveDate", _entity.LTEffectiveDate as String))
    contentNode.addChild(createTextNode("LTExpirationDate", _entity.LTExpirationDate as String))
    contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber as String))
    if (_entity.CertificateHolder.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.CertificateHolder.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.CertificateHolder.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.CertificateHolder.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.CertificateHolder.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.CertificateHolder.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.CertificateHolder.Suffix as String))
    }
    if (_entity.CertificateHolder.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.CertificateHolder.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.CertificateHolder.TaxID))
    }
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }

  override function getMatchKeyForForm( form : Form ) : String {
    var fassociation=form.FormAssociations
    if(form.FormAssociations.HasElements){
      if((form.FormAssociations[0] as GLFormAssociation_TDIC).PolicyCertificateHolder_TDIC !== null) {
        return form.FormAssociations[0].fixedIdValue("PolicyCertificateHolder_TDIC") as java.lang.String
      }
      return form.FormAssociations[0].fixedIdValue(FormAssociationPropertyName) as java.lang.String
    }
    return null
  }
}