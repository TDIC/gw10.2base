package tdic.pc.common.batch.finance.reports.prmlines

class TDIC_FinancePremiumLineAccountObject {
  private var _lobCode : String as readonly LobCode
  private var _lobLabel : String as readonly LobLabel
  private var _oldCompany: String as readonly OldCompany
  private var _acctNumber: String as readonly AccountNumber
  private var _description: String as readonly Description
  private var _negativeAdj : String as readonly NegativeAdjustment


  construct(oldCompany:String, acctNumber: String, description: String, negtiveAdj: String){
    _oldCompany = oldCompany
    _acctNumber = acctNumber
    _description = description
    _negativeAdj = negtiveAdj
  }
}