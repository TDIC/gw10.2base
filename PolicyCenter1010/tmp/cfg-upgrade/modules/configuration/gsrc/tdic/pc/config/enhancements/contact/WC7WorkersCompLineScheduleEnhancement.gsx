package tdic.pc.config.enhancements.contact

uses java.util.ArrayList
uses gw.api.util.DisplayableException
uses gw.plugin.Plugins
uses gw.plugin.contact.IContactConfigPlugin
uses gw.api.locale.DisplayKey

/**
 * Shane Sheridan 09/30/2014
 */
enhancement WC7WorkersCompLineScheduleEnhancement : entity.WC7WorkersCompLine {

  /**
   * Add new [Policy] owner/officer to the WC7Line, and revision it to the Account.
   */
  function addNewWC7PolicyOwnerOfficerOfContactType(contactType : ContactType) : WC7PolicyOwnerOfficer {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return addWC7PolicyOwnerOfficer(acctContact.Contact)
  }

  /**
   * throws exception if Owner Officer for the given Account Contact already exists on this line.
   */
  function addWC7PolicyOwnerOfficer(contact : Contact) : WC7PolicyOwnerOfficer {
    if (this.WC7PolicyOwnerOfficers.firstWhere(\ p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyOwnerOfficer.Error.AlreadyExists", contact))
    }
    var policyOfficer = this.Branch.addNewPolicyContactRoleForContact(contact, typekey.PolicyContactRole.TC_WC7POLICYOWNEROFFICER) as WC7PolicyOwnerOfficer
    this.addToWC7PolicyOwnerOfficers(policyOfficer)
    return policyOfficer
  }

  /**
   * For each AccountContact returned by the UnassignedOwnerOfficer property,
   * add that AccountContact as an Included Owner/Officer to the WC7Line
   * throws an exception the owner officer exclusion is null
   */
  function addAllExistingWC7PolicyOwnerOfficer() : WC7PolicyOwnerOfficer[] {
    var newWC7PolicyOwnerOfficers = new ArrayList<WC7PolicyOwnerOfficer>()
    for(ac in this.UnassignedOwnerOfficers) {
      newWC7PolicyOwnerOfficers.add(addWC7PolicyOwnerOfficer(ac.Contact))
    }
    return newWC7PolicyOwnerOfficers.toTypedArray()
  }

  /**
   * Set a WC7PolicyOwnerOfficer as excluded.
   */
  function addExcludedOwnerOfficer_TDIC(excludedOfficer : WC7PolicyOwnerOfficer) : WC7PolicyOwnerOfficer {
    excludedOfficer.isExcluded_TDIC = true
    return excludedOfficer
  }

  /**
   * Remove an excluded owner officer from exclusion.
   */
  function removeWC7ExcludedPolicyOwnerOfficer(excludedOfficer : WC7PolicyOwnerOfficer){
    excludedOfficer.isExcluded_TDIC = false
  }

  /**
   * Set all non-excluded Owner Officers as Excluded.
   */
  function addAllExistingOwnerOfficersToExclusion() : WC7PolicyOwnerOfficer[] {
    for(po in WC7PolicyOwnerOfficersSubtractExcluded) {
      addExcludedOwnerOfficer_TDIC(po)
    }
    return WC7PolicyOwnerOfficersSubtractExcluded
  }

  /**
   * Return array of excluded WC7PolicyOwnerOfficers on policy
  */
  property get WC7ExcludedPolicyOwnerOfficers() : WC7PolicyOwnerOfficer[] {
    var excludedOwnerOfficers = new ArrayList<WC7PolicyOwnerOfficer>()
    var policyOfficers = this.WC7PolicyOwnerOfficers.whereTypeIs(WC7PolicyOwnerOfficer)

    // find the excluded owner officers.
    for(po in policyOfficers){
      if(po.isExcluded_TDIC){
        excludedOwnerOfficers.add(po)
      }
    }
    return excludedOwnerOfficers.toTypedArray()
  }

  /**
   * Return array of existing WC7PolicyOwnerOfficers on policy minus the existing Excluded owner officers.
   */
  property get WC7PolicyOwnerOfficersSubtractExcluded() : WC7PolicyOwnerOfficer[] {
    var notExcludedOwnerOfficers = new ArrayList<WC7PolicyOwnerOfficer>()
    // DE171, robk: only Person Owner/Officers should be added to the exclusion
    var policyOfficers = this.WC7PolicyOwnerOfficers.where( \ o -> o.IntrinsicType != PolicyEntityOwner_TDIC and o.AccountContactRole.AccountContact.Contact.Subtype == typekey.Contact.TC_PERSON)

    // filter out the excluded owner officers.
    for(po in policyOfficers){
      if(not po.isExcluded_TDIC){
        notExcludedOwnerOfficers.add(po)
      }
    }
    return notExcludedOwnerOfficers.toTypedArray()
  }

  /**
   * US890, robk
   *
   * All the account Entity Owners that are not already assigned to this policy line.
   */
  property get UnassignedEntityOwners_TDIC() : AccountContact[] {
    var plugin = Plugins.get(IContactConfigPlugin)
    var accountContactRoleType = plugin.getAccountContactRoleTypeFor(TC_POLICYENTITYOWNER_TDIC)
    var assignedOwnerOfficers = this.WC7PolicyOwnerOfficers.map(\ ownerOfficer -> ownerOfficer.AccountContactRole.AccountContact)
    return this.Branch.Policy.Account
        .getAccountContactsWithRole(accountContactRoleType)
        .subtract(assignedOwnerOfficers)
        .toTypedArray()
  }

  /**
   * US890, robk
   *
   * Creates a new Entity Owner policy contact role from the specified contact and adds to the policy owner officers on this policy line.
   */
  @Throws(DisplayableException, "if the specified contact is already an Entity Owner on this policy line")
  function addPolicyEntityOwner_TDIC(contact : Contact) : PolicyEntityOwner_TDIC {
    if (this.WC7PolicyOwnerOfficers.firstWhere(\ p -> p.AccountContactRole.AccountContact.Contact == contact) != null) {
      throw new DisplayableException(DisplayKey.get("Web.Contact.PolicyOwnerOfficer.Error.AlreadyExists", contact))
    }
    var entityOwner = this.Branch.addNewPolicyContactRoleForContact(contact, TC_POLICYENTITYOWNER_TDIC) as PolicyEntityOwner_TDIC
    this.addToWC7PolicyOwnerOfficers(entityOwner)
    return entityOwner
  }

  /**
   * US890
   * For each AccountContact returned by the UnassignedOwnerOfficer property,
   * add that AccountContact as an Included Owner/Officer to the WC7Line
   * throws an exception the owner officer exclusion is null
   */
  function addAllExistingPolicyEntityOwners_TDIC() : PolicyEntityOwner_TDIC[] {
    var newPolicyEntityOwners = new ArrayList<PolicyEntityOwner_TDIC>()
    for(ac in this.UnassignedEntityOwners_TDIC) {
      newPolicyEntityOwners.add(addPolicyEntityOwner_TDIC(ac.Contact))
    }
    return newPolicyEntityOwners.toTypedArray()
  }

  /**
   * US890
   * Creates a new account contact of the specified contact type and creates a new Entity Owner policy contact role from that contact.
   */
  function addNewPolicyEntityOwnerOfContactType_TDIC(contactType : ContactType) : PolicyEntityOwner_TDIC {
    var acctContact = this.Branch.Policy.Account.addNewAccountContactOfType(contactType)
    return addPolicyEntityOwner_TDIC(acctContact.Contact)
  }

  /**
   * US890, DE171, robk
   *
   * Returns the Entity Owners on policy minus those that are excluded.
   */
  property get WC7PolicyEntityOwnersSubtractExcluded_TDIC() : PolicyEntityOwner_TDIC[] {
    var notExcludedPolicyEntityOwners = new ArrayList<PolicyEntityOwner_TDIC>()
    var policyEntityOwners = this.WC7PolicyOwnerOfficers.where( \ o -> o.IntrinsicType == PolicyEntityOwner_TDIC and o.AccountContactRole.AccountContact.Contact.Subtype == typekey.Contact.TC_PERSON)

    for(anEntityOwner in policyEntityOwners){
      if(not anEntityOwner.isExcluded_TDIC){
        notExcludedPolicyEntityOwners.add(anEntityOwner as PolicyEntityOwner_TDIC)
      }
    }
    return notExcludedPolicyEntityOwners.toTypedArray()
  }

  /**
   * Set all non-excluded Policy Entity Owners as Excluded.
   */
  function addAllExistingPolicyEntityOwnersToExclusion_TDIC() : PolicyEntityOwner_TDIC[] {
    for(anEntityOwner in WC7PolicyEntityOwnersSubtractExcluded_TDIC) {
      addExcludedOwnerOfficer_TDIC(anEntityOwner)
    }
    return WC7PolicyEntityOwnersSubtractExcluded_TDIC
  }
}
