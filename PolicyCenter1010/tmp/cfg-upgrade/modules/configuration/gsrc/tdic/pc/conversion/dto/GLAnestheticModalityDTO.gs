package tdic.pc.conversion.dto

class GLAnestheticModalityDTO {

  var anestheticModality : String as AnestheticModality
  var name : String as Name
  var permitNumber : String as PermitNumber
  var policyID : String as PolicyID
  var publicID : String as PublicID

}