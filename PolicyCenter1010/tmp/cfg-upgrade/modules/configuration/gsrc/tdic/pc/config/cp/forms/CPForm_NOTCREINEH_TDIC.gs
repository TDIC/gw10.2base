package tdic.pc.config.cp.forms

uses gw.api.util.DateUtil
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class CPForm_NOTCREINEH_TDIC extends AbstractSimpleAvailabilityForm {

  /*Availability script to allow form to be inferred on reinstatemnt transaction*/
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var period = context.Period
    if (period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC"
        or period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {

      var lossPayees = period.BOPLine?.BOPLocations*.Buildings*.BOPBldgLossPayees
      var mortgagees = period.BOPLine?.BOPLocations*.Buildings*.BOPBldgMortgagees
      var additionalInsureds =  period.BOPLine?.BOPLocations*.Buildings*.AdditionalInsureds
      if(lossPayees.HasElements) {
        return lossPayees.hasMatch(\lossPayee -> (lossPayee.ExpirationDate_TDIC != null
            and lossPayee.ExpirationDate_TDIC.after(DateUtil.currentDate())))
      }else if(mortgagees.HasElements) {
        return mortgagees.hasMatch(\mortgagee -> (mortgagee.ExpirationDate_TDIC != null
            and mortgagee.ExpirationDate_TDIC.after(DateUtil.currentDate())))
      }else if(additionalInsureds.HasElements) {
        return additionalInsureds.hasMatch(\additionalInsured -> (additionalInsured.ExpirationDate != null
            and additionalInsured.ExpirationDate.after(DateUtil.currentDate())))
      }else {
        return false
      }
    }else if(period.Offering.CodeIdentifier == "PLClaimsMade_TDIC"
        or period.Offering.CodeIdentifier =="PLOccurence_TDIC") {

      var additionalInsureds = period.GLLine.GLAdditionInsdSched_TDIC.HasElements ?
          period.GLLine.GLAdditionInsdSched_TDIC.where(\elt -> elt.LTExpirationDate == null
              || elt.LTExpirationDate?.after(DateUtil.currentDate())) : null
      var certificateHolders = period.GLLine.GLCertofInsSched_TDIC.HasElements ?
          period.GLLine.GLCertofInsSched_TDIC?.where(\elt -> elt.LTExpirationDate == null
              || elt.LTExpirationDate?.after(DateUtil.currentDate())) : null
      var dentaladlInsureds = period.GLLine.GLDentalBLAISched_TDIC.HasElements ?
          period.GLLine.GLDentalBLAISched_TDIC?.where(\elt -> elt.LTExpirationDate == null
              || elt.LTExpirationDate?.after(DateUtil.currentDate())) : null

      return additionalInsureds.HasElements or certificateHolders.HasElements or dentaladlInsureds.HasElements
    }
    return false
  }
}