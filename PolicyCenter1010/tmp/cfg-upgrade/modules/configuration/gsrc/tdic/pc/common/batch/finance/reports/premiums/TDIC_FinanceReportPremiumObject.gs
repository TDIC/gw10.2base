package tdic.pc.common.batch.finance.reports.premiums

uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLineTypeEnum

uses java.math.BigDecimal

class TDIC_FinanceReportPremiumObject {
  private var _productCode: String as readonly ProductCode
  private var _jurisdiction: String as readonly Jurisdiction
  private var _prmLineType: TDIC_FinancePremiumLineTypeEnum as readonly PremiumLineType
  private var _amount : BigDecimal as readonly Amount

  construct(productCode:String, jurisdiction:String, prmLineType : TDIC_FinancePremiumLineTypeEnum, amount: BigDecimal) {
    _productCode = productCode
    _jurisdiction = jurisdiction
    _prmLineType = prmLineType
    _amount = amount
  }

  override function toString(): String{
    return "ProductCode: ${_productCode}; Jurisdiction: ${_jurisdiction}; PremiumLineType: ${_prmLineType}"
  }
}