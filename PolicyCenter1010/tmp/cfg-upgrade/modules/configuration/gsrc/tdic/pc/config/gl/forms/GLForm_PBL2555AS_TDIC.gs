package tdic.pc.config.gl.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode


class GLForm_PBL2555AS_TDIC extends AbstractMultipleCopiesForm<GLExcludedServiceSched_TDIC> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLExcludedServiceSched_TDIC> {
    var period = context.Period
    var glExcServSchedItems = period.GLLine?.GLExcludedServiceSched_TDIC
    var excludeServiceItems : ArrayList<GLExcludedServiceSched_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and period.GLLine?.GLExcludedServiceCond_TDICExists and
        glExcServSchedItems.HasElements){
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var GLExclSerItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis GLExcludedServiceSched_TDIC )*.Bean
        if(glExcServSchedItems.hasMatch(\item -> item.BasedOn == null)){
          excludeServiceItems.add(glExcServSchedItems.firstWhere(\item -> item.BasedOn == null))
        }
        else if(glExcServSchedItems.hasMatch(\item -> GLExclSerItemsModified.contains(item))) {
          excludeServiceItems.add(glExcServSchedItems.firstWhere(\item -> GLExclSerItemsModified.contains(item)))
        }
      }
      else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
          excludeServiceItems.add(glExcServSchedItems.first())
        }
      }
      else{
        excludeServiceItems.add(glExcServSchedItems.first())
      }
      return excludeServiceItems.HasElements? excludeServiceItems.toList(): {}
      }

    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLExcludedServiceSched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber))
  }
  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }
}