package tdic.pc.conversion.gxmodel

uses gw.api.productmodel.ClausePatternLookup
uses gw.api.productmodel.CovTermPatternLookup
uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.gxmodel.generalliabilitycovmodel.anonymous.elements.GeneralLiabilityCov_CovTerms_Entry
uses tdic.pc.conversion.gxmodel.generalliabilitycovmodel.types.complex.GeneralLiabilityCov
uses tdic.pc.conversion.util.ConversionUtility


enhancement GeneralLiabilityCovModelEnhancement : GeneralLiabilityCov {

  function populateCoverages(line : GLLine, glLine : tdic.pc.conversion.gxmodel.gllinemodel.types.complex.GLLine) {
    var pattern = ClausePatternLookup.getCoveragePatternByCodeIdentifier(this.PatternCode)
    try {
      var cov = line.getOrCreateCoverage(pattern)
      line.setCoverageExists(pattern, true)
      for (covTerm in this.CovTerms.Entry) {
        //var newPattern = ConversionConstants.COVERAGETERM_MAPPER.get(covTerm.PatternCode)
        var covTermPattern = CovTermPatternLookup.getByCodeIdentifier(covTerm.PatternCode)
        var term = covTermPattern == null ? null : cov.getCovTerm(covTermPattern)
        if (term == null) {
          LoggerUtil.CONVERSION_ERROR.info("Pol :" + line.Branch.LegacyPolicyNumber_TDIC + " ; base stae : " + line.Branch.BaseState + " ;Coverage : " + this.PatternCode + ";Coverage Term : " + covTerm.PatternCode)
        } else {
          var value = ConversionConstants.GL_COVERAGETERM_OPTIONS_MAPPER.get(covTerm.PatternCode)
          term.setValueFromString(value == null ? covTerm.DisplayValue : value.get(covTerm.DisplayValue))
        }
      }
      if(cov.Pattern.CodeIdentifier == ConversionConstants.GL_MANUSCRIPT_PATTERN_CODE) {
        handleManuscriptSchedule(this.CovTerms.Entry, line, glLine)
      }
    } catch (e : Exception) {
      LoggerUtil.CONVERSION_ERROR.info(" Coverage :" + this.PatternCode + " ;" + e.LocalizedMessage)
    }
  }

  private function handleManuscriptSchedule(covTerms : List<GeneralLiabilityCov_CovTerms_Entry>, line : GLLine, glLine : tdic.pc.conversion.gxmodel.gllinemodel.types.complex.GLLine){
    for (manuscript in glLine.GLManuscript_TDIC.Entry) {
      var sched = line.createAndAddGLManuscript_TDIC()
      sched.ManuscriptEffectiveDate = line.Branch.EditEffectiveDate.addYears(1)
      if (manuscript.PolicyAddInsured_TDIC != null) {
        var policyAddlInsDetail =
            line.addNewAdditionalInsuredDetailOfContactType(ConversionUtility.
                getContactType(manuscript.PolicyAddInsured_TDIC.PolicyAddlInsured.AccountContactRole.AccountContact.Contact.Subtype))
        manuscript.PolicyAddInsured_TDIC.PolicyAddlInsured.AccountContactRole.AccountContact.
            Contact.$TypeInstance.populateContact(policyAddlInsDetail.PolicyAddlInsured.AccountContactRole.AccountContact.Contact)
        policyAddlInsDetail.AdditionalInsuredType = manuscript.PolicyAddInsured_TDIC.AdditionalInsuredType
        policyAddlInsDetail.Desciption = manuscript.PolicyAddInsured_TDIC.Desciption
        sched.PolicyAddInsured_TDIC = policyAddlInsDetail
      }

      if (manuscript.CertificateHolder != null) {
        var certHolder =
            line.addNewPolicyCertificateHolderOfContactType_TDIC(ConversionUtility.
                getContactType(manuscript.CertificateHolder.AccountContactRole.AccountContact.Contact.Subtype))
        manuscript.CertificateHolder.AccountContactRole.AccountContact.
            Contact.$TypeInstance.populateContact(certHolder.AccountContactRole.AccountContact.Contact)
        sched.CertificateHolder = certHolder
      }

      sched.ManuscriptDescription = manuscript.ManuscriptDescription
      sched.ManuscriptType = manuscript.ManuscriptType
    }
  }

}
