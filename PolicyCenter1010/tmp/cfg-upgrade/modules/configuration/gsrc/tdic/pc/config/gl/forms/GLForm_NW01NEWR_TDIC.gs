package tdic.pc.config.gl.forms

/**
 * Created with IntelliJ IDEA.
 * User: AnudeepK
 * Date: 07/27/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class GLForm_NW01NEWR_TDIC extends AbstractSimpleAvailabilityForm{
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC"){
      if (context.Period.Job typeis Renewal && context.Period.isDIMSPolicy_TDIC) {
        return (context.Period?.GLLine?.GLPriorActsCov_TDIC?.GLPriorActsRetroDate_TDICTerm?.Value != context.Period.PeriodStart) and
            !context.Period?.GLLine?.Exposures*.ClassCode_TDIC?.toList()?.contains(ClassCode_TDIC.TC_01)
      }
    }
    return false
  }
}