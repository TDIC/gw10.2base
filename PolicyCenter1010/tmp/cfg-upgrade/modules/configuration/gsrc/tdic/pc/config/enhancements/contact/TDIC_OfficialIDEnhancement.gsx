package tdic.pc.config.enhancements.contact

/**
 * Created with IntelliJ IDEA.
 * User: jlane
 * Date: 9/30/14
 * Time: 1:15 PM
 * To change this template use File | Settings | File Templates.
 */
enhancement TDIC_OfficialIDEnhancement : entity.OfficialID  {

  /**
   * John Lane - TDIC
   * If the OfficialIDType is BureauID and Experience Rating is used the BureauID is required
   * This function will return true in cases where the state is using Experience Rating.  The modifier is both "Eligible
   *   and selected (boolean) on the State Coverages Screen for Workers Compensation
   */
  function isStateIDRequired(jurisdiction : WC7Jurisdiction) : boolean {
    var modifiers =  jurisdiction.AllModifierVersions
    if (this.OfficialIDType == typekey.OfficialIDType.TC_BUREAUID) {
      var rateModifiers = modifiers.where( \ mod -> mod.DataType == TC_RATE and not mod.ScheduleRate).toTypedArray()
      for (aModifier in rateModifiers) {
        if(aModifier.Eligible and aModifier.ModifierType == "WC7ExpMod") {
          return true
        }
      }
    }
    return false
  }
}
