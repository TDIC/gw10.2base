package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode
uses typekey.Job

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_CYB1205AS_TDIC extends AbstractSimpleAvailabilityForm {
  var period : PolicyPeriod

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    period = context.Period
    if (context.Period.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
      var changeReasons = context.Period.Job.ChangeReasons
      if(context.Period.Job typeis PolicyChange and changeReasons != null) {
        if ( changeReasons.hasMatch(\reason -> ( reason.ChangeReason == ChangeReasonType_TDIC.TC_LOCATION)
            or (reason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBEROFFER)
            or (reason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBER))) {
          return false
        } else if (changeReasons.hasMatch(\reason -> reason.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBDOCGEN)) {
          return true
        }
      }
      return (period.Job.Subtype == typekey.Job.TC_CANCELLATION) ? period.RefundCalcMethod != CalculationMethod.TC_FLAT : true
    }
    return false
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    if (period?.isChangeReasonOnlyRegenerateDeclaration_TDIC) {
      contentNode.addChild(createTextNode("JobNumber", period.Job.JobNumber))
    }

    if (period?.Job?.ChangeReasons.hasMatch(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_LIMITSINCREASEDECREASE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MAILINGADDRESS ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MANUSCRIPENDORSEMENT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NAMEDINSURED)) {
      contentNode.addChild(createTextNode("JobNumber", period.Job.JobNumber))
    }
  }
}