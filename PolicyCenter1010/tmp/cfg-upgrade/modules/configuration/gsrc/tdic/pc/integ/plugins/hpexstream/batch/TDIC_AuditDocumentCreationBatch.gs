package tdic.pc.integ.plugins.hpexstream.batch

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.util.DateUtil
uses gw.processes.BatchProcessBase
uses tdic.pc.common.batch.unistat.util.TDIC_WCstatUtil
uses com.tdic.util.misc.EmailUtil
uses java.util.ArrayList
uses gw.api.database.DBFunction
uses java.util.List
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory
uses typekey.Job

/**
 * This needs to run nightly to determine events like final audit complete on the date of copmpletion
 */
class TDIC_AuditDocumentCreationBatch extends BatchProcessBase {
  private static var CLASS_NAME = "TDIC_AuditDocumentCreationBatch"
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  private var _failureEmailTo : String as readonly FailureEmailTo
  private static final var TERMINATE_SUB = "Terminate::${gw.api.system.server.ServerUtil.ServerId}-Audit payroll statement document creation batch Terminated"

  construct() {
    super(BatchProcessType.TC_AUDITDOCUMENTCREATIONBATCH)
  }

  /**
   * US555
   * Shane Murphy 10/13/2014
   *
   * Extracts payload data from this staging area and sends them in a SOAP request to HP Exstream Command Center to start a composition job.
   */
  override function doWork(): void {
    var logPrefix = "${CLASS_NAME}#doWork()"
    _logger.trace("TDIC_AuditDocumentCreationBatch#doWork() - Entering")

    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during doWork() method before physicalAuditLetterForExpiredPeriods().")
      EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Audit documents creation batch job is terminated.")
      return
    }
    physicalAuditLetterForExpiredPeriods()

    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during doWork() method before auditLetterEstimatedDocuments().")
      EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Audit documents creation batch job is terminated..")
      return
    }
    //JIRA 2402 - Need to suppress this doc generation until requirements are finalized. Refer JIRA for more details.
    //auditLetterEstimatedDocuments()

    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during doWork() method before processAuditFirstRequestForExpiredPolicies().")
      EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Audit documents creation batch job is terminated.")
      return
    }
    processAuditFirstRequestForExpiredPolicies()

    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during doWork() method before processAuditSecondRequestForMissedDueDatePolicies().")
      EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Audit documents creation batch job is terminated.")
      return
    }
    processAuditSecondRequestForMissedDueDatePolicies()

    //GW-2284
    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during doWork() method before processAuditFirstRequestForCancelledPolicies().")
      EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Audit documents creation batch job is terminated.")
      return
    }
    processAuditFirstRequestForCancelledPolicies()
    /* Kesava Tavva - 08/27 - Commented these document creation as Karen found that these are duplicate
      firstVoluntaryAuditDocuments()
      secondVoluntaryAuditDocuments()
      physicalAuditDocument()
      finalAuditCompletionDocuments()
   */
    if (TerminateRequested) {
      _logger.warn("${logPrefix} - Terminate requested during doWork() method before processPhysicalAuditForNonpaymentCancellation().")
      EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Audit documents creation batch job is terminated.")
      return
    }
    processPhysicalAuditForNonpaymentCancellation()

    _logger.trace("TDIC_AuditDocumentCreationBatch#doWork() - Exiting")
  }





  /**
   * US645
   * Shane Murphy 1/19/2015
   *
   * 75 Days after renewal for Physical Audits unless payroll received
   */
  function auditLetterEstimatedDocuments(): void {
    _logger.trace("TDIC_AuditDocumentCreationBatch#auditLetterEstimatedDocuments() - Entering")

    var seventyFiveDayOldPolicies = getRenewalsFrom(- ScriptParameters.TDIC_WC7DaysAfterRenewalToSendAuditLetterEstimationDocuments, TDIC_DocCreationEventType.TC_AUDITLETTERESTIMATED)

    var payrollReceived = new ArrayList<PolicyPeriod>()
    seventyFiveDayOldPolicies.each(\pol -> {
      var physAudit = pol.DisplayableAuditInfoList.firstWhere(\audit -> (audit.Method == AuditMethod.TC_PHYSICAL as String && audit.ActualInfo.ReceivedDate == null && audit.Complete == false))
      if (physAudit != null){
        payrollReceived.add(pol)
      }
    })
    if (payrollReceived.Count > 0) {
      _logger.debug("TDIC_AuditDocumentCreationBatch#auditLetterEstimatedDocuments() - Raising Second notification on ${payrollReceived.Count} Policies")
      startDocumentProduction(payrollReceived, TDIC_DocCreationEventType.TC_AUDITLETTERESTIMATED as String)
    } else {
      _logger.debug("TDIC_AuditDocumentCreationBatch#auditLetterEstimatedDocuments() - No policies for Audit Letter - Estimated")
    }
    _logger.trace("TDIC_AuditDocumentCreationBatch#auditLetterEstimatedDocuments() - Exiting")
  }

  /**
   * US645
   * Shane Murphy 1/19/2015
   *
   * Gets Renewals from some days in the past
   */
  @Param("daysAgo", "Number of days ago to retrieve policies for")
  function getRenewalsFrom(daysAgo: int, documentEventType:TDIC_DocCreationEventType): List<PolicyPeriod> {
    _logger.trace("TDIC_AuditDocumentCreationBatch#getRenewalsFrom() - Entering ")
    _logger.debug("TDIC_AuditDocumentCreationBatch#getRenewalsFrom() - Retrieving Renewals from ${daysAgo} days")
    var daysOfBatchRecords =  ScriptParameters.TDIC_DAYSOFBATCHRECORDS

    var endingDateOfRecords = DateUtil.currentDate().addDays(daysAgo)
    var startingDateOfRecords = gw.api.util.DateUtil.addDays(endingDateOfRecords, - daysOfBatchRecords)

    var isDocumentSentEndDateCheck = DateUtil.currentDate()
    var isDocumentSentStartDateCheck = DateUtil.currentDate().addDays(- daysOfBatchRecords)

    var renewals = Query.make(PolicyPeriod).and(\andCriteria -> {
         andCriteria.between(PolicyPeriod#PeriodStart, startingDateOfRecords, endingDateOfRecords)
    })
        .join(PolicyPeriod#Job)
        .compare(entity.Job#Subtype, Equals, TC_RENEWAL)

    var document = Query.make(Document).compare(Document#Event_TDIC, Equals, documentEventType as String)
    document.between(Document#CreateTime, isDocumentSentStartDateCheck, isDocumentSentEndDateCheck)
    document.compare(Document#DeliveryStatus_TDIC, NotEquals, DocDlvryStatusType_TDIC.TC_ERROR)

    renewals.subselect("ID", CompareNotIn, document, "PolicyPeriod")
    var rs = renewals.select()

    _logger.trace("TDIC_AuditDocumentCreationBatch#getRenewalsFrom() - Exiting")
    return rs.toList()
  }

  /**
   * US645
   * Shane Murphy 1/20/2015
   *
   *  Trigger the document production code for the event on each policyperiod
   */
  @Param("aPolicyList", "List<PolicyPeriod> to raise event on")
  @Param("anEvent", "Name of the event to raise")
  function startDocumentProduction(aPolicyList: List<PolicyPeriod>, anEvent: String) {
    _logger.trace("TDIC_AuditDocumentCreationBatch#startDocumentProduction() - Exiting")
    _logger.debug("TDIC_AuditDocumentCreationBatch#startDocumentProduction() - Raising ${anEvent} event on ${aPolicyList.length} policies")
    aPolicyList.each(\pol -> {
      gw.transaction.Transaction.runWithNewBundle(\newBundle -> {
        try {
          incrementOperationsCompleted()
          newBundle.add(pol)
          pol.Job.createSingleDocumentsForEvent(anEvent)
        } catch(e) {
          _logger.debug("Exception occurred while generating an event and payload "+ anEvent + ":" + e)
          if(TDIC_WCstatUtil.EMAIL_RECIPIENT != null){
            incrementOperationsFailed()
            EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, anEvent + " audit letter payload creation falied on server: " + gw.api.system.server.ServerUtil.ServerId + ", for Policy#:" + pol.PolicyNumber, "Below is the detail of the exception: \n\n" + e)
          }
        }
      })
    })
    _logger.trace("TDIC_AuditDocumentCreationBatch#startDocumentProduction() - Exiting")
  }

  /**
   * GW 140
   * 08/24/2015 Kesava Tavva
   *
   * Retrieves expired Policy periods that were expired 30 days ago and generate physical audit letter
   */
  function physicalAuditLetterForExpiredPeriods(): void {
    var logPrefix = "${CLASS_NAME}#physicalAuditDocumentForExpiredPeriods()"
    _logger.trace("${logPrefix} - Entering")
    var daysOfBatchRecords =  ScriptParameters.TDIC_DAYSOFBATCHRECORDS

    var endingDateOfRecords = DateUtil.currentDate().addDays(-ScriptParameters.TDIC_WC7DaysAfterExpirationToSendPhysicalAuditLetter)
    var startingDateOfRecords = gw.api.util.DateUtil.addDays(endingDateOfRecords, - daysOfBatchRecords)

    var isDocumentSentEndDateCheck = DateUtil.currentDate()
    var isDocumentSentStartDateCheck = DateUtil.currentDate().addDays(- daysOfBatchRecords)

    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compare(PolicyPeriod#Status,Equals,PolicyPeriodStatus.TC_BOUND)
    ppQuery.and(\andCriteria -> {
      andCriteria.between(PolicyPeriod#PeriodEnd, startingDateOfRecords, endingDateOfRecords)
    })

    var document = Query.make(Document).compare(Document#Event_TDIC, Equals, typekey.TDIC_DocCreationEventType.TC_PHYSICALAUDITLETTER as String)
    document.between(Document#CreateTime, isDocumentSentStartDateCheck, isDocumentSentEndDateCheck)
    document.compare(Document#DeliveryStatus_TDIC, NotEquals, DocDlvryStatusType_TDIC.TC_ERROR)

    ppQuery.subselect("ID", CompareNotIn, document, "PolicyPeriod")
    var rs = ppQuery.select()

    var expiredPolicyPeriods = rs
    OperationsExpected = OperationsExpected+expiredPolicyPeriods?.Count
    expiredPolicyPeriods?.each( \ epp -> {

      _logger.info("${logPrefix} - ${epp}-PolicyPeriodStatus:${epp.PeriodDisplayStatus}")
      if(epp.PeriodDisplayStatus == typekey.PolicyPeriodStatus.TC_EXPIRED.Code  && epp.Job.Subtype != typekey.Job.TC_POLICYCHANGE){
        var mostRecentCancellation = epp.Policy.BoundPeriods?.where(\elt -> elt.Job.Subtype == Job.TC_CANCELLATION and elt.TermNumber==epp.TermNumber)?.orderByDescending(\elt -> elt.Job.CloseDate)?.first()
        var mostRecentReinstatement = epp.Policy.BoundPeriods?.where(\elt -> elt.Job.Subtype == Job.TC_REINSTATEMENT and elt.TermNumber==epp.TermNumber)?.orderByDescending(\elt -> elt.Job.CloseDate)?.first()
        var policyCancelledWithPhyAuditLetter : boolean = false
        if(mostRecentReinstatement!=null && mostRecentCancellation.CreateTime!=null) {
          policyCancelledWithPhyAuditLetter = (mostRecentCancellation.CreateTime>mostRecentReinstatement.CreateTime) ? (mostRecentCancellation.Job.Documents?.hasMatch(\elt1 -> elt1.Event_TDIC== TDIC_DocCreationEventType.TC_PHYSICALAUDITLETTER as String)) : false
        } else if(mostRecentCancellation != null) {
          policyCancelledWithPhyAuditLetter = mostRecentCancellation.Job.Documents?.hasMatch(\elt1 -> elt1.Event_TDIC== TDIC_DocCreationEventType.TC_PHYSICALAUDITLETTER as String)
        }
        var auditMethod = epp.AuditInformations.firstWhere( \ ai -> (ai.PolicyTerm==epp.PolicyTerm)==true)?.AuditMethod
        _logger.info("${logPrefix} - Process ${epp}-AuditMethod:${auditMethod}")
        if( auditMethod == typekey.AuditMethod.TC_PHYSICAL && !policyCancelledWithPhyAuditLetter){
          try {
            incrementOperationsCompleted()
            startDocumentProduction(epp, typekey.TDIC_DocCreationEventType.TC_PHYSICALAUDITLETTER.Code)
          }catch(e) {
            _logger.error("Exception occurred while generating an event and payload TC_PHYSICALAUDITLETTER" + e)
            if(TDIC_WCstatUtil.EMAIL_RECIPIENT != null){
              incrementOperationsFailed()
              EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, "Physical audit letter payload creation falied on server: " + gw.api.system.server.ServerUtil.ServerId + ", for Policy#:" + epp.PolicyNumber, "Below is the detail of the exception: \n\n" + e)
             }
          }
        }
      }
    })
    _logger.trace("${logPrefix} - Exiting")
  }

  /**
   * GW 140
   * 08/25/2015 Kesava Tavva
   *
   *  Trigger the document production code for the event on each policy period
   */
  @Param("aPolicyPeriod", "PolicyPeriod to raise event on")
  @Param("anEvent", "Name of the event to raise")
  function startDocumentProduction(aPolicyPeriod: PolicyPeriod, anEvent: String) {
    _logger.info("${CLASS_NAME}#startDocumentProduction(aPolicyPeriod, anEvent) - Create single documnets for ${aPolicyPeriod}-Event:${anEvent} - Entering")
    gw.transaction.Transaction.runWithNewBundle(\newBundle -> {
      aPolicyPeriod = newBundle.add(aPolicyPeriod)
      aPolicyPeriod.Job.createSingleDocumentsForEvent(anEvent)
    })
    _logger.info("${CLASS_NAME}#startDocumentProduction(aPolicyPeriod, anEvent) - Create single documnets for ${aPolicyPeriod}-Event:${anEvent} - Exiting")
  }



  /**
   * GW 142
   * 08/19/2015 Kesava Tavva
   *
   * Retrieves expired Policy periods for generating audit 1st notice for payroll statement documents and delegates
   * request for document production
   */
  function processAuditFirstRequestForExpiredPolicies(): void {
    var logPrefix = "${CLASS_NAME}#processAuditFirstRequestForExpiredPolicies()"
    _logger.trace("${logPrefix} - Entering")
    var daysOfBatchRecords =  ScriptParameters.TDIC_DAYSOFBATCHRECORDS
    var endingDateOfRecords = DateUtil.currentDate().addDays(-ScriptParameters.TDIC_WC7DaysAfterExpirationToSendAuditFirstRequestDocuments)
    var startingDateOfRecords = gw.api.util.DateUtil.addDays(endingDateOfRecords, - daysOfBatchRecords)

    var isDocumentSentEndDateCheck = DateUtil.currentDate()
    var isDocumentSentStartDateCheck = DateUtil.currentDate().addDays(- daysOfBatchRecords)
    var auditDocAccounts = new ArrayList<String>()
    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compare(PolicyPeriod#Status,Equals,PolicyPeriodStatus.TC_BOUND)
    ppQuery.between(PolicyPeriod#PeriodEnd, startingDateOfRecords, endingDateOfRecords)

    var document = Query.make(Document).compare(Document#Event_TDIC, Equals, typekey.TDIC_DocCreationEventType.TC_AUDITNOTICE as String)
    document.between(Document#CreateTime, isDocumentSentStartDateCheck, isDocumentSentEndDateCheck)
    document.compare(Document#DeliveryStatus_TDIC, NotEquals, DocDlvryStatusType_TDIC.TC_ERROR)

    ppQuery.subselect("ID", CompareNotIn, document, "PolicyPeriod")

    var expiredPolicyPeriods = ppQuery.select()

    OperationsExpected = OperationsExpected+expiredPolicyPeriods?.Count
    var latestPeriod:PolicyPeriod = null
    expiredPolicyPeriods?.each( \ epp -> {
      latestPeriod = getLatestPolicyPeriodInPolicyTerm(epp)
      _logger.info("${logPrefix} - ${epp}-PolicyPeriodStatus:${epp.PeriodDisplayStatus}-JobType: ${epp.Job.Subtype}; LatestPeriod: ${latestPeriod}")
      if(!auditDocAccounts.contains(latestPeriod.Policy.Account.AccountNumber) && epp.PeriodDisplayStatus == typekey.PolicyPeriodStatus.TC_EXPIRED.Code
          && (epp.Job.Subtype == typekey.Job.TC_SUBMISSION or epp.Job.Subtype == typekey.Job.TC_RENEWAL or epp.Job.Subtype == typekey.Job.TC_REINSTATEMENT)
          && latestPeriod?.Job.Subtype != typekey.Job.TC_CANCELLATION
          && not latestPeriod?.Documents?.hasMatch( \ doc -> doc.Event_TDIC == typekey.TDIC_DocCreationEventType.TC_AUDITNOTICE.Code
                                                      and doc.DeliveryStatus_TDIC != typekey.DocDlvryStatusType_TDIC.TC_ERROR)){
        var audInfo = epp.AuditInformations.firstWhere( \ ai -> (ai.PolicyTerm==epp.PolicyTerm && ai.ReceivedDate == null)==true)
        if(("In Progress".equalsIgnoreCase(audInfo.DisplayStatus) or "Scheduled".equalsIgnoreCase(audInfo.DisplayStatus)) && not audInfo.IsReversal){
          var auditMethod = audInfo.AuditMethod
          _logger.info("${logPrefix} - Process ${epp}-AuditMethod:${auditMethod}")
          if( auditMethod == typekey.AuditMethod.TC_VOLUNTARY){
           try{
             incrementOperationsCompleted()
             startDocumentProduction(latestPeriod, typekey.TDIC_DocCreationEventType.TC_AUDITNOTICE.Code)
             auditDocAccounts.add(latestPeriod.Policy.Account.AccountNumber)
           }catch(e) {
             _logger.debug("Exception occurred while generating an event and payload TC_AUDITNOTICE" + e)
             if(TDIC_WCstatUtil.EMAIL_RECIPIENT != null){
               incrementOperationsFailed()
               EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, "TC_AUDITNOTICE payload creation falied on server: " + gw.api.system.server.ServerUtil.ServerId + ", for Policy#:" + epp.PolicyNumber, "Below is the detail of the exception: \n\n" + e)
             }
           }
          }
        }
      }
    })
    _logger.trace("${logPrefix} - Exiting")
  }

  /**
   * GW 142
   * 08/20/2015 Kesava Tavva
   *
   * Retrieves Policy periods for generating audit 2nd reminder notice for payroll statement documents and delegates
   * request for document production
   */
  function processAuditSecondRequestForMissedDueDatePolicies(): void {
    var logPrefix = "${CLASS_NAME}#processAuditSecondRequestForMissedDueDatePolicies()"
    _logger.trace("${logPrefix} - Entering")
    var daysOfBatchRecords =  ScriptParameters.TDIC_DAYSOFBATCHRECORDS

    var endingDateOfRecords = DateUtil.currentDate().addDays(-1)
    var startingDateOfRecords = gw.api.util.DateUtil.addDays(endingDateOfRecords, - daysOfBatchRecords)

    var isDocumentSentEndDateCheck = DateUtil.currentDate()
    var isDocumentSentStartDateCheck = DateUtil.currentDate().addDays(- daysOfBatchRecords )


    var auditInfo = Query.make(AuditInformation).and(\andCriteria -> {
        andCriteria.compare(AuditInformation#DueDate, NotEquals, null)
      andCriteria.between(AuditInformation#DueDate, startingDateOfRecords, endingDateOfRecords)
      andCriteria.compare(AuditInformation#ReceivedDate, Equals, null)
      andCriteria.compare(AuditInformation#AuditMethod, Equals, typekey.AuditMethod.TC_VOLUNTARY)
    })

    var document = Query.make(Document).compare(Document#Event_TDIC, Equals, typekey.TDIC_DocCreationEventType.TC_AUDITREMINDER as String)
    document.between(Document#CreateTime, isDocumentSentStartDateCheck, isDocumentSentEndDateCheck)
    document.compare(Document#DeliveryStatus_TDIC, NotEquals, DocDlvryStatusType_TDIC.TC_ERROR)

    auditInfo.subselect("Policy", CompareNotIn, document, "Policy")

    var rs =  auditInfo.select()

    var polPeriods = new ArrayList<PolicyPeriod>()
    var audits = rs.where( \ info -> ("In Progress".equalsIgnoreCase(info.DisplayStatus) or "Scheduled".equalsIgnoreCase(info.DisplayStatus)) and not info.IsReversal)
    audits?.each(\info -> polPeriods.add(info.PolicyTerm.getPeriodAsOf(info.AuditPeriodStartDate)))
    OperationsExpected = OperationsExpected+polPeriods?.Count
    polPeriods?.each( \ pp -> {
      try{
        incrementOperationsCompleted()
        if(pp.RefundCalcMethod != typekey.CalculationMethod.TC_FLAT){
          _logger.info("${logPrefix} - Process ${pp}")
          startDocumentProduction(pp, TDIC_DocCreationEventType.TC_AUDITREMINDER.Code)
        }
      }catch(e) {
        _logger.debug("Exception occurred while generating an event and payload TC_AUDITREMINDER" + e)
        if(TDIC_WCstatUtil.EMAIL_RECIPIENT != null){
          incrementOperationsFailed()
          EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, "TC_AUDITREMINDER audit letter payload creation falied on server: " + gw.api.system.server.ServerUtil.ServerId + ", for Policy#:" + pp.PolicyNumber, "Below is the detail of the exception: \n\n" + e)
        }
      }
    })
    _logger.trace("${logPrefix} - Exiting")
  }

  /**
   * GW-2284
   * 12/06/2016 Kesava Tavva
   *
   * Retrieves cancelled Policy periods for generating audit 1st notice for payroll statement documents and delegates
   * request for document production
   */
  function processAuditFirstRequestForCancelledPolicies(): void {
    var logPrefix = "${CLASS_NAME}#processAuditFirstRequestForCancelledPolicies()"
    _logger.trace("${logPrefix} - Entering")
    var daysOfBatchRecords =  ScriptParameters.TDIC_DAYSOFBATCHRECORDS
    var endingDateOfRecords = DateUtil.currentDate().addDays(-ScriptParameters.TDIC_WC7AdditionalToCalculateFinalAuditStartDateIfCancelled)
    var startingDateOfRecords = gw.api.util.DateUtil.addDays(endingDateOfRecords, - daysOfBatchRecords)

    var isDocumentSentEndDateCheck = DateUtil.currentDate()
    var isDocumentSentStartDateCheck = DateUtil.currentDate().addDays(- daysOfBatchRecords)

    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compare(PolicyPeriod#Status,Equals,PolicyPeriodStatus.TC_BOUND)
    ppQuery.compare(PolicyPeriod#RefundCalcMethod, NotEquals, typekey.CalculationMethod.TC_FLAT)
    ppQuery.or(\orCriteria -> {
      orCriteria.and(\andCriteria1 ->{
        andCriteria1.between(PolicyPeriod#ModelDate, startingDateOfRecords, endingDateOfRecords)
        andCriteria1.compare(DBFunction.DateDiff(DAYS,ppQuery.getColumnRef("ModelDate"),ppQuery.getColumnRef("CancellationDate")),LessThan, 0)
      })
      orCriteria.and(\andCriteria2 ->{
        andCriteria2.between(PolicyPeriod#CancellationDate, startingDateOfRecords, endingDateOfRecords)
        andCriteria2.compare(DBFunction.DateDiff(DAYS,ppQuery.getColumnRef("ModelDate"),ppQuery.getColumnRef("CancellationDate")),GreaterThanOrEquals, 0)
      })
    })

    ppQuery.join(PolicyPeriod#Job).compare(entity.Job#Subtype, Equals,typekey.Job.TC_CANCELLATION)

    var document = Query.make(Document).compare(Document#Event_TDIC, Equals, typekey.TDIC_DocCreationEventType.TC_AUDITNOTICE as String)
    document.between(Document#CreateTime, isDocumentSentStartDateCheck, isDocumentSentEndDateCheck)
    document.compare(Document#DeliveryStatus_TDIC, NotEquals, DocDlvryStatusType_TDIC.TC_ERROR)

    ppQuery.subselect("ID", CompareNotIn, document, "Job")

    var cancelledPolicyPeriods = ppQuery.select()

    OperationsExpected = OperationsExpected+cancelledPolicyPeriods?.Count
    cancelledPolicyPeriods?.each( \ epp -> {
      _logger.info("${logPrefix} - ${epp}-PolicyPeriodStatus:${epp.PeriodDisplayStatus}-JobType: ${epp.Job.Subtype}")
      if(epp.LatestPeriod.PeriodDisplayStatus == DisplayKey.get("PolicyPeriod.PeriodDisplayStatus.Canceled")){
        var audInfo = epp.AuditInformations.firstWhere( \ ai -> (ai.PolicyTerm==epp.PolicyTerm && ai.ReceivedDate == null)==true)
        if(("In Progress".equalsIgnoreCase(audInfo.DisplayStatus) or "Scheduled".equalsIgnoreCase(audInfo.DisplayStatus)) && not audInfo.IsReversal){
          var auditMethod = audInfo.AuditMethod
          _logger.info("${logPrefix} - Process ${epp}-AuditMethod:${auditMethod}")
          if( auditMethod == typekey.AuditMethod.TC_VOLUNTARY){
            try{
              incrementOperationsCompleted()
              startDocumentProduction(epp, typekey.TDIC_DocCreationEventType.TC_AUDITNOTICE.Code)
            }catch(e) {
              _logger.debug("Exception occurred while generating an event and payload TC_AUDITNOTICE" + e)
              if(TDIC_WCstatUtil.EMAIL_RECIPIENT != null){
                incrementOperationsFailed()
                EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, "TC_AUDITNOTICE payload creation falied on server: " + gw.api.system.server.ServerUtil.ServerId + ", for Policy#:" + epp.PolicyNumber, "Below is the detail of the exception: \n\n" + e)
              }
            }
          }
        }
       }
    })
    _logger.trace("${logPrefix} - Exiting")
  }

  /**
   * GW-2647
   * 02/02/2017 Kesava Tavva
   *
   * Returns latest policy period for a given policy period's term.
   */
  private function getLatestPolicyPeriodInPolicyTerm(period: PolicyPeriod): PolicyPeriod{
    var fwdPeriod:PolicyPeriod
    var nextFwdPeriod:PolicyPeriod
    fwdPeriod = period.PolicyTerm.Periods.firstWhere( \ pp -> (pp.BasedOn == period && pp.Status == PolicyPeriodStatus.TC_BOUND) == Boolean.TRUE)
    if(fwdPeriod!=null){
      nextFwdPeriod = period.PolicyTerm.Periods.firstWhere( \ pp -> (pp.BasedOn == fwdPeriod && pp.Status == PolicyPeriodStatus.TC_BOUND) == Boolean.TRUE)
      while(nextFwdPeriod != null){
        fwdPeriod = nextFwdPeriod
        nextFwdPeriod = period.PolicyTerm.Periods.firstWhere( \ pp -> (pp.BasedOn == fwdPeriod && pp.Status == PolicyPeriodStatus.TC_BOUND) == Boolean.TRUE)
      }
    }
    return (fwdPeriod != null) ? fwdPeriod : period
  }

/*

  /**
   * US645
   * Shane Murphy 1/19/2015
   *
   * 15 Days after renewal
   */
  function firstVoluntaryAuditDocuments(): void {
    _logger.trace("TDIC_AuditDocumentCreationBatch#firstVoluntaryAuditDocuments() - Entering")
    var fifteenDayOldPolicies = getRenewalsFrom(- ScriptParameters.TDIC_WC7DaysAfterRenewalToSendFirstVoluntaryAuditDocuments, TDIC_DocCreationEventType.TC_FIRSTVOLUNTARYAUDIT)
    var firstVoluntaryAudits = new ArrayList<PolicyPeriod>()
    fifteenDayOldPolicies.each(\pol -> {
      var physAudit = pol.DisplayableAuditInfoList.firstWhere(\audit -> (audit.Method == AuditMethod.TC_VOLUNTARY as String))
      if (physAudit != null){
        firstVoluntaryAudits.add(pol)
      }
    })
    if (firstVoluntaryAudits.Count > 0) {
      _logger.debug("TDIC_AuditDocumentCreationBatch#auditLetterEstimatedDocuments() - Raising First Voluntary Audit notification on ${firstVoluntaryAudits.Count} Policies")
      startDocumentProduction(firstVoluntaryAudits, TDIC_DocCreationEventType.TC_FIRSTVOLUNTARYAUDIT)
    }
    _logger.trace("TDIC_AuditDocumentCreationBatch#firstVoluntaryAuditDocuments() - Exiting")
  }

  /**
   * US645
   * Shane Murphy 1/19/2015
   *
   * Voluntary Audit on due date
   */
  function secondVoluntaryAuditDocuments(): void {
    _logger.trace("TDIC_AuditDocumentCreationBatch#secondVoluntaryAuditDocuments() - Entering")
    // US645, shanem 01/20/2015: Get audits due today
    var auditInfo = Query.make(AuditInformation).and(\andCriteria -> {
      andCriteria.compare(AuditInformation#DueDate, NotEquals, null)
      andCriteria.compare(DBFunction.DateFromTimestamp(andCriteria.getColumnRef("DueDate")), Equals, DateUtil.currentDate())
    }).select().toTypedArray()

    // US645, shanem 01/20/2015: Start document production on the policy period for these final audits
    var polPeriods = new ArrayList<PolicyPeriod>()
    auditInfo.each(\info -> polPeriods.add(info.PolicyTerm.getPeriodAsOf(DateUtil.currentDate())))
    if (polPeriods != null) {
      startDocumentProduction(polPeriods, TDIC_DocCreationEventType.TC_SECONDVOLUNTARYAUDIT)
    }
    _logger.trace("TDIC_AuditDocumentCreationBatch#secondVoluntaryAuditDocuments() - Exiting")
  }

  /**
   * US645
   * Shane Murphy 1/19/2015
   *
   * Night of final audit completion
   */
  /*function finalAuditCompletionDocuments(): void {
    var logPrefix = "${CLASS_NAME}#finalAuditCompletionDocuments()"
    _logger.trace("${logPrefix} - Entering")
    var dateToCheckFor = DateUtil.currentDate()
    var ppQuery =Query.make(PolicyPeriod).compare("Status",Equals,PolicyPeriodStatus.TC_AUDITCOMPLETE)
    var jobQuery = ppQuery.join(PolicyPeriod#Job).compare(Job#Subtype, Equals,typekey.Job.TC_AUDIT.Code)
    jobQuery.compare(DBFunction.DateFromTimestamp(jobQuery.getColumnRef("CloseDate")), Equals, dateToCheckFor)
    var auditCompletePeriods = ppQuery.select()
    OperationsExpected = OperationsExpected+auditCompletePeriods?.Count
    auditCompletePeriods?.each( \ acp -> {
      incrementOperationsCompleted()
      _logger.debug("${logPrefix} - ${acp}-PolicyPeriodStatus:${acp.PeriodDisplayStatus}")
      //GW-480
      if(acp.Audit.AuditInformation.ActualAuditMethod.Code == typekey.AuditMethod.TC_ESTIMATED.Code)
        startDocumentProduction(acp, typekey.TDIC_DocCreationEventType.TC_AUDITLETTERESTIMATED.Code)
      else
        startDocumentProduction(acp, typekey.TDIC_DocCreationEventType.TC_FINALAUDITCOMPLETED.Code)
    })
    _logger.trace("${logPrefix} - Exiting")
  }*/

  /**
   * US645
   * Shane Murphy 1/19/2015
   *
   * 30 days after renewal
   */
  function physicalAuditDocument(): void {
    _logger.trace("TDIC_AuditDocumentCreationBatch#physicalAuditDocument() - Entering")
    var thirtyDayOldPolicies = getRenewalsFrom(- ScriptParameters.TDIC_WC7DaysAfterRenewalToSendPhysicalAuditDocuments, TDIC_DocCreationEventType.TC_PHYSICALAUDIT)
    var physicalAudits = new ArrayList<PolicyPeriod>()
    thirtyDayOldPolicies.each(\pol -> {
      var physAudit = pol.DisplayableAuditInfoList.firstWhere(\audit -> (audit.Method == AuditMethod.TC_PHYSICAL as String))
      if (physAudit != null){
        physicalAudits.add(pol)
      }
    })
    if (physicalAudits.Count > 0) {
      _logger.debug("TDIC_AuditDocumentCreationBatch#auditLetterEstimatedDocuments() - Raising Physical Audit notification on ${physicalAudits.Count} Policies")
      startDocumentProduction(physicalAudits, TDIC_DocCreationEventType.TC_PHYSICALAUDIT)
    }
    _logger.trace("TDIC_AuditDocumentCreationBatch#physicalAuditDocument() - Exiting")
  }

  /**
   * GW 141, 244
   * Kesava Tavva 09/02/2015
   *
   * Night of final audit completion
   */

  /*function finalAuditCompletionDocuments(): void {
    var logPrefix = "${CLASS_NAME}#finalAuditCompletionDocuments()"
    _logger.trace("${logPrefix} - Entering")
    var dateToCheckFor = DateUtil.currentDate()
    var ppQuery =Query.make(PolicyPeriod).compare("Status",Equals,PolicyPeriodStatus.TC_AUDITCOMPLETE)
    var jobQuery = ppQuery.join(PolicyPeriod#Job).compare(Job#Subtype, Equals,typekey.Job.TC_AUDIT.Code)
    jobQuery.compare(DBFunction.DateFromTimestamp(jobQuery.getColumnRef("CloseDate")), Equals, dateToCheckFor)
    var auditCompletePeriods = ppQuery.select()
    OperationsExpected = OperationsExpected+auditCompletePeriods?.Count
    auditCompletePeriods?.each( \ acp -> {
      incrementOperationsCompleted()
      _logger.debug("${logPrefix} - ${acp}-PolicyPeriodStatus:${acp.PeriodDisplayStatus}")
      startDocumentProduction(acp, typekey.TDIC_DocCreationEventType.TC_FINALAUDITCOMPLETED.Code)
    })
    _logger.trace("${logPrefix} - Exiting")
  }   */

  */

  function processPhysicalAuditForNonpaymentCancellation() : void {
    var logPrefix = "${CLASS_NAME}#process Physical Audit RequestForCancelledPolicies()"
    _logger.trace("${logPrefix} - Entering")
    var daysOfBatchRecords = ScriptParameters.TDIC_DAYSOFBATCHRECORDS
    var endingDateOfRecords = DateUtil.currentDate().addDays(-ScriptParameters.TDIC_WC7AdditionalToCalculateFinalAuditStartDateIfCancelled)
    var startingDateOfRecords = gw.api.util.DateUtil.addDays(endingDateOfRecords, -daysOfBatchRecords)

    var isDocumentSentStartDateCheck = DateUtil.currentDate().addDays(-daysOfBatchRecords)

    var ppQuery = Query.make(PolicyPeriod)
    ppQuery.compare(PolicyPeriod#Status, Equals, PolicyPeriodStatus.TC_BOUND)
    ppQuery.compare(PolicyPeriod#RefundCalcMethod, NotEquals, typekey.CalculationMethod.TC_FLAT)
    ppQuery.join(PolicyPeriod#Job).compare(entity.Job#Subtype, Equals, typekey.Job.TC_CANCELLATION)
    ppQuery.and(\andCriteria -> {
      andCriteria.between(PolicyPeriod#EditEffectiveDate, startingDateOfRecords, endingDateOfRecords)
    })

    var document = Query.make(Document).compare(Document#Event_TDIC, Equals, TDIC_DocCreationEventType.TC_PHYSICALAUDITLETTER as String)
    document.between(Document#CreateTime, isDocumentSentStartDateCheck, DateUtil.currentDate())
    document.compare(Document#DeliveryStatus_TDIC, NotEquals, DocDlvryStatusType_TDIC.TC_ERROR)

    ppQuery.subselect("ID", CompareNotIn, document, "Job")

    var cancelledPolicyPeriods = ppQuery.select()

    cancelledPolicyPeriods?.each(\epp -> {
      var latestPeriod = getLatestPolicyPeriodInPolicyTerm(epp)
      _logger.info("${logPrefix} - ${latestPeriod}-PolicyPeriodStatus:${latestPeriod.PeriodDisplayStatus}-JobType: ${latestPeriod.Job.Subtype}")
      if (latestPeriod.PeriodDisplayStatus == DisplayKey.get("PolicyPeriod.PeriodDisplayStatus.Canceled") and latestPeriod.Cancellation.CancelReasonCode == ReasonCode.TC_NONPAYMENT) {
        var auditMethod = latestPeriod.AuditInformations.firstWhere(\ai -> (ai.PolicyTerm == latestPeriod.PolicyTerm) == true)?.AuditMethod
        _logger.info("${logPrefix} - Process ${latestPeriod}-AuditMethod:${auditMethod}")
        if (auditMethod == typekey.AuditMethod.TC_PHYSICAL) {
          try {
            startDocumentProduction(latestPeriod, TDIC_DocCreationEventType.TC_PHYSICALAUDITLETTER.Code)
          } catch (e) {
            _logger.error("Exception occurred while generating an event and payload TC_PHYSICALAUDITLETTER" + e)
            if (TDIC_WCstatUtil.EMAIL_RECIPIENT != null) {
              incrementOperationsFailed()
              EmailUtil.sendEmail(TDIC_WCstatUtil.EMAIL_RECIPIENT, "Physical audit letter for nonpayment payload creation falied on server: " + gw.api.system.server.ServerUtil.ServerId + ", for Policy#:" + latestPeriod.PolicyNumber, "Below is the detail of the exception: \n\n" + e)
            }
          }
        }
      }
    })
    _logger.trace("${logPrefix} - Exiting")
  }
}