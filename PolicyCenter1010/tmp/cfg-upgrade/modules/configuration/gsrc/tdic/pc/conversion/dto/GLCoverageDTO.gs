package tdic.pc.conversion.dto

class GLCoverageDTO {

  var dateTerm : String as DateTerm
  var choiceTerm : String as ChoiceTerm
  var directTerm : String as DirectTerm
  var covPatternCode : String as CovPatternCode
  var covTermPatternCode : String as CovTermPatternCode
  var policyID : String as PolicyID
  var publicID : String as PublicID

}