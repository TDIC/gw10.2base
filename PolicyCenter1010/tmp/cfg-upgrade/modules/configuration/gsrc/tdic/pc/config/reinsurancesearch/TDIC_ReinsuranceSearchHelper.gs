package tdic.pc.config.reinsurancesearch

uses java.lang.StringBuilder
uses org.apache.commons.lang3.text.WordUtils

/**
 * US1378
 * ShaneS 05/11/2015
 */
class TDIC_ReinsuranceSearchHelper {

  /**
   * US1378
   * ShaneS 04/29/2015
   */
  static function convertAddress(addressLine1 : String, userDisplayable : boolean) : String{
    addressLine1 = addressLine1.toUpperCase()
    var splitAddress = addressLine1.split("\\s+")

    var strBuilder = new StringBuilder()
    var isPreviousElementIgnored = false
    var loopIndex = 1

    for(word in splitAddress){
      // Remove all punctuation except '#'.
      word = word.replaceAll("[\\p{P}&&[^\\#]]","")

      // If previous word was ignored, then ignore the rest of the string.
      if(not isPreviousElementIgnored){
        if(not shouldIgnoreWord(word)){
          word = standardizeWord(word)

          if(userDisplayable){
            var isWordStandardizedToBlank = (word == "")
            if(not isWordStandardizedToBlank){
              word = makeWordUserDisplayable(word, loopIndex)
            }
          }

          strBuilder.append(word)
        }
        else{
          isPreviousElementIgnored = true
        }
      }

      loopIndex++
    }

    return strBuilder.toString()
  }

  /**
   * US1378
   * ShaneS 05/18/2015
   */
  private static function makeWordUserDisplayable(word : String, loopIndex : int) : String{
    var userDisplayableWord = word.toLowerCase()
    userDisplayableWord = WordUtils.capitalize(userDisplayableWord)
    // Don't add white space before first word.
    var shouldAddWhiteSpace = (loopIndex != 1)
    if(shouldAddWhiteSpace){
      userDisplayableWord = " "+ userDisplayableWord
    }

    return userDisplayableWord
  }

  /**
   * US1378
   * ShaneS 04/29/2015
   *
   * Additional address information should be ignored (e.g. SUITE #)
   */
  private static function shouldIgnoreWord(word : String) : boolean{
    var additionalInfo = {"APT","SUITE","STE","ROOM","RM","SPACE","SPC","FLOOR","FL","UNIT","BLDG","BUILDING",
        "DOWNSTAIRS","DOWNSTAIR","UPSTAIRS","UPSTAIR","#"}
    for(addInfo in additionalInfo){
      if(word == addInfo or word.matches(addInfo+ "[0-9]+")){
        return true
      }
    }

    return false
  }

  /**
   * US1378
   * ShaneS 04/29/2015
   *
   * Most addresses will already be standardized but this is not guaranteed, so must standardize AddressLine1 when
   * generating RI Search Tag.
   */
  private static function standardizeWord(word : String) : String{
    switch(word){
      case "STREET":
      case "STR":
      case "SAINT":
          return "ST"
      case "LANE":
      case "LNE":
          return "LN"
      case "DRIVE":
      case "DRIV":
          return "DR"
      case "COURT":
      case "CRT":
          return "CT"
      case "ROAD":
          return "RD"
      case "AVENUE":
          return "AVE"
      case "CIRCLE":
          return "CR"
      case "ROUTE":
          return "RT"
      case "PLAZA":
          return "PLZ"
      case "PARKWAY":
          return ""
      case "NORTH":
          return "N"
      case "SOUTH":
          return "S"
      case "EAST":
          return "E"
      case "WEST":
          return "W"
    }

    // No need to standardize, just return the word unchanged.
    return word
  }

  /**
   * DE264
   * ShaneS 05/21/2015
  */
  static function convertZipCode(zip : String) : String{
    zip = zip.toUpperCase()
    // Ignore the last digits of the zip code after the hyphen.
    var splitAddress = zip.split("\\-")
    zip = splitAddress[0].trim()

    return zip
  }
}