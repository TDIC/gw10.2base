package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.PolicyConversionDTO
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.query.PolicyConversionBatchQueries

uses java.math.BigDecimal
uses java.sql.Connection

class PolicyConversionDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<PolicyConversionDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"
  private static var CREATED_BY : String

  var _batchType : String

  var _params : Object[]

  construct(batchType : String, params : Object[]) {
    _batchType = batchType
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : List<PolicyConversionDTO> {
    return null
  }

  override function update(aTransientObject : PolicyConversionDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      //var params = new Object[2]
      var batchParams = new ArrayList<Object[]>()
      var params = new ArrayList<Object>()

      updatedRows = runUpdate(connection, null, params.toTypedArray(), _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(policyConversionDetail : PolicyConversionDTO) : int {
    var connection : Connection
    var insertedRow : BigDecimal = 0
    try {
      connection = getConnection(_logger)
      var batchParams = new ArrayList<Object[]>()
      var dateTime = CommonConversionHelper.getCurrentDateTime()
      var params = new ArrayList<Object>()
      params.add(policyConversionDetail?.AccountNumber)
      params.add(policyConversionDetail?.PolicyNumber)
      params.add(policyConversionDetail?.Offering_LOB)
      params.add(policyConversionDetail?.Counter)
      params.add(policyConversionDetail?.BaseState)
      params.add(policyConversionDetail?.TermNumber)
      params.add(policyConversionDetail?.IndustryCode)
      params.add(policyConversionDetail?.BatchID)
      params.add(policyConversionDetail?.TotalCostRpt)
      batchParams.add(params.toTypedArray())
      insertedRow = runUpdate(connection, PolicyConversionBatchQueries.INSERT_GW_POLICY_CONVERSION_STATUS_QUERY, params.toTypedArray(), _logger)
      connection.commit()
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
    return insertedRow as int
  }

  override function persistAll(payloads : List<PolicyConversionDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : PolicyConversionDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<PolicyConversionDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<PolicyConversionDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<PolicyConversionDTO>) {
  }

}