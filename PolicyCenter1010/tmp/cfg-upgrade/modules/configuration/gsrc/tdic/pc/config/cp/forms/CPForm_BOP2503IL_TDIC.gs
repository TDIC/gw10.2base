package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 04/09/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_BOP2503IL_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    var ilCounties = {
        "Bond", "Bureau", "Christian", "Clinton", "Douglas", "Franklin", "Fulton", "Gallatin", "Grundy", "Jackson", "Jefferson",
        "Knox", "LaSalle", "Logan", "McDonough", "Macoupin", "Madison", "Marion", "Marshall", "Menard", "Mercer", "Montgomery",
        "Peoria", "Perry", "Putnam", "Randolph", "Rock Island", "St. Clair", "Saline", "Sangamon", "Tazewell", "Vermilion",
        "Washington", "Williamson"}
    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC"){
      var hasILMineSubsidenceCov = context.Period.BOPLine?.BOPLocations?.hasMatch(\loc -> loc?.Buildings?.hasMatch(\bldg -> bldg.BOPILMineSubCov_TDICExists))
      var hasBuildingCov = context.Period.BOPLine?.BOPLocations?.hasMatch(\loc -> loc.Buildings?.hasMatch(\bldg -> bldg.BOPBuildingCovExists))
      if(ilCounties.contains(context.Period.PolicyAddress.Address?.County?.trim()) and hasBuildingCov
          and !hasILMineSubsidenceCov){
        return true
      }
    }
    return false
  }
}