package tdic.pc.conversion.lob.bop

uses org.apache.commons.lang.StringUtils
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.dao.AddlInsuredDAO
uses tdic.pc.conversion.dao.BOPBuildingCovDAO
uses tdic.pc.conversion.dao.BOPManuscriptDAO
uses tdic.pc.conversion.dao.BuildingDAO
uses tdic.pc.conversion.dao.BuildingModifierDAO
uses tdic.pc.conversion.dao.BuildingRateFactorDAO
uses tdic.pc.conversion.dao.ContactDAO
uses tdic.pc.conversion.dao.PolicyAddlInterestsDAO
uses tdic.pc.conversion.dto.BOPBuildingCovDTO
uses tdic.pc.conversion.dto.BuildingDTO
uses tdic.pc.conversion.dto.ContactDTO
uses tdic.pc.conversion.dto.PolicyLocationDTO
uses tdic.pc.conversion.dto.PolicyPeriodDTO
uses tdic.pc.conversion.gxmodel.accountcontactrolemodel.anonymous.elements.AccountContactRole_AccountContact
uses tdic.pc.conversion.gxmodel.bopbuildingcovmodel.anonymous.elements.BOPBuildingCov_CovTerms
uses tdic.pc.conversion.gxmodel.bopbuildingcovmodel.anonymous.elements.BOPBuildingCov_CovTerms_Entry
uses tdic.pc.conversion.gxmodel.bopbuildingmodel.anonymous.elements.BOPBuilding_Coverages_Entry
uses tdic.pc.conversion.gxmodel.boplocationmodel.anonymous.elements.BOPLocation_Buildings_Entry
uses tdic.pc.conversion.gxmodel.contactmodel.anonymous.elements.Contact_OfficialIDs
uses tdic.pc.conversion.gxmodel.contactmodel.anonymous.elements.Contact_OfficialIDs_Entry
uses tdic.pc.conversion.gxmodel.policylosspayee_tdicmodel.anonymous.elements.PolicyLossPayee_TDIC_AccountContactRole
uses tdic.pc.conversion.gxmodel.policymortgagee_tdicmodel.anonymous.elements.PolicyMortgagee_TDIC_AccountContactRole
uses tdic.pc.conversion.gxmodel.policyperiodmodel.PolicyPeriod
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.helper.PolicyConversionHelper
uses tdic.pc.conversion.util.ConversionUtility
uses typekey.AccountContactRole
uses typekey.PolicyContactRole

class BOPPolicyConversionHelper extends PolicyConversionHelper {

  private var _productCode : ConversionConstants.ProductCode
  private var _policyPeriod : PolicyPeriod
  private var _policyPeriodDTO : PolicyPeriodDTO

  construct(policyPeriodDTO : PolicyPeriodDTO, productCode : ConversionConstants.ProductCode) {
    super(policyPeriodDTO)
    _productCode = productCode
    _policyPeriodDTO = policyPeriodDTO
  }


  override function generatePayloadXML() : String {
    super.preparePayload()
    _policyPeriod = super.PolicyPeriod
    _policyPeriod.Policy.ProductCode = _productCode.Type
    prepareBOPLine()
    return _policyPeriod.asUTFString()
  }


  private function prepareBOPLine() {
    _policyPeriod.BOPLine.EffectiveDate = _policyPeriod.PeriodStart
    var locations = retrieveLocations()
    if (locations != null && locations.size() != 0) {
      populateBOPLocationsAndBuildings(_policyPeriod, locations)
    }
  }

  private function populateBOPLocationsAndBuildings(policyPeriod : PolicyPeriod, locations : List<PolicyLocationDTO>) {
    var primaryLocationCount = 0
    var remainingPNI : String = null
    var pni = _policyPeriod.PolicyContactRoles?.Entry?.firstWhere(\elt -> elt.Subtype == typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED)
    if(pni.AccountContactRole.AccountContact.Contact.Name.length > 64) {
      var longName = pni.AccountContactRole.AccountContact.Contact.Name
      pni.AccountContactRole.AccountContact.Contact.Name = ConversionUtility.formatNamesGreaterThan64Chars(longName, false)
      remainingPNI = ConversionUtility.formatNamesGreaterThan64Chars(longName, true)
    }
    for (location in locations index i) {
      prepareLocation(policyPeriod, location, i)
      prepareBOPBuilding(location, i, remainingPNI)
      prepareBOPManuscript_TDIC(location)
    }
    if (primaryLocationCount > 1) {
      // throw new ConversionException(106,"Multiple primary location found!!")
    }
  }

  private function prepareBOPBuilding(locationDTO : PolicyLocationDTO, i : Integer, remainingPNI : String ) {
    var buildingDAO = new BuildingDAO({locationDTO.PublicID})
    var buildings = buildingDAO.retrieveAllUnprocessed()
    for (building in buildings index count) {
      _policyPeriod.BOPLine.BOPLocations.Entry[i].PolicyLocation.AccountLocation.LocationNum =
          locationDTO.LocationNum
      _policyPeriod.BOPLine.BOPLocations.Entry[i].PolicyLocation.OriginalEffDate_TDIC =
          CommonConversionHelper.parseDate(locationDTO.OriginalEffectiveDate)
      prepareBuilding(building, i, count, locationDTO.PublicID, remainingPNI )
    }
  }

  private function prepareBuilding(building : BuildingDTO, i : Integer, count : Integer, locationID : String, remainingPNI : String) {
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.BOPOccupancyStatus_TDIC =
        BOPOccupancyStatus_TDIC.get(building.BOPOccupancyStatus_TDIC)
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.BurglarAlarm_TDIC =
        building.BurglarAlarm_TDIC == null ? null : "Y".equalsIgnoreCase(building.BurglarAlarm_TDIC) ? true : false
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].ConstructionType =
        BOPConstructionType.get(building.ConstructionType)
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.NumOperatories_TDIC =
        building.NumOperatories_TDIC
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.NumStories =
        building.NumStories
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.NumUnits_TDIC =
        building.NumUnits_TDIC
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.ProtectionClass_TDIC =
        ProtectionClass_TDIC.get(StringUtils.leftPad(building.ProtectionCLass_TDIC, 2, '0'))
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.RentalIncome_TDIC =
        building.RentalIncome_TDIC
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.Sprinklered_TDIC =
        building.Sprinklered_TDIC == null ? null : "Y".equalsIgnoreCase(building.Sprinklered_TDIC) ? true : false
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.TerritoryDetails_TDIC =
        TerritoryDetails_TDIC.get(building.TerritoryDetails_TDIC)
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.TotalArea =
        building.TotalArea
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.TotalOfficeArea_TDIC =
        building.TotalOfficeArea_TDIC
    _policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count].Building.YearBuilt =
        building.YearBuilt == 0 ? null : building.YearBuilt

    prepareBuildingCoverages(_policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count], locationID)

    prepareLongManuscriptPNI(_policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count], remainingPNI)

    prepareBuildingModifiers(_policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count], locationID)

    prepareBuildingAdditionalInterests(_policyPeriod.BOPLine.BOPLocations.Entry[i].Buildings.Entry[count], building.PublicID)

  }

  private function prepareBuildingAdditionalInterests(bopBuilding : BOPLocation_Buildings_Entry, buildingID : String) {
    var policyContactDAO = new PolicyAddlInterestsDAO(new Object[]{buildingID})
    var policyContacts = policyContactDAO.retrieveAllUnprocessed()
    if (policyContacts.Empty) {
      return
    }
    var mortgagees = policyContacts.where(\elt -> elt.Subtype == typekey.PolicyContactRole.TC_POLICYMORTGAGEE_TDIC.Code)
    for (mortagee in mortgagees index i) {
      var contactDAO = new ContactDAO({mortagee.ContactID})
      var contactDTO = contactDAO.retrieveAllUnprocessed().first()
      bopBuilding.BOPBldgMortgagees.Entry[i].AccountContactRole = prepareMortgageeContactRole(contactDTO, typekey.AccountContactRole.TC_MORTGAGEE_TDIC)
      bopBuilding.BOPBldgMortgagees.Entry[i].LoanNumberString = mortagee.LoanNumber
    }
    var lossPayees = policyContacts.where(\elt -> elt.Subtype == typekey.PolicyContactRole.TC_POLICYLOSSPAYEE_TDIC.Code)
    for (lossPayee in lossPayees index i) {
      var contactDAO = new ContactDAO({lossPayee.ContactID})
      var contactDTO = contactDAO.retrieveAllUnprocessed().first()
      bopBuilding.BOPBldgLossPayees.Entry[i].AccountContactRole = prepareLossPayeeContactRole(contactDTO, typekey.AccountContactRole.TC_LOSSPAYEE_TDIC)
      bopBuilding.BOPBldgLossPayees.Entry[i].LoanNumberString = lossPayee.LoanNumber
    }
    if (bopBuilding.Coverages.Entry.hasMatch(\coverage -> coverage.PatternCode == "BOPDentalGenLiabilityCov_TDIC" or coverage.PatternCode == "BOPBuildingOwnersLiabCov_TDIC")) {
      var additionalInsureds = policyContacts.where(\elt -> elt.Subtype == PolicyContactRole.TC_POLICYADDLINSURED.Code)
      for (additionalInsured in additionalInsureds index i) {
        var contactDAO = new ContactDAO({additionalInsured.ContactID})
        var contactDTO = contactDAO.retrieveAllUnprocessed().first()
        bopBuilding.AdditionalInsureds.Entry[i].AccountContactRole = preparePolicyContactRole(contactDTO)
      }
    }
  }

  private function prepareMortgageeContactRole(contactDTO : ContactDTO, subtype : typekey.AccountContactRole) : PolicyMortgagee_TDIC_AccountContactRole {
    var model = new PolicyMortgagee_TDIC_AccountContactRole()
    model.AccountContact = prepareAccountContactRole(contactDTO)
    model.Subtype = subtype
    return model
  }

  private function prepareLossPayeeContactRole(contactDTO : ContactDTO, subtype : typekey.AccountContactRole) : PolicyLossPayee_TDIC_AccountContactRole {
    var model = new PolicyLossPayee_TDIC_AccountContactRole()
    model.AccountContact = prepareAccountContactRole(contactDTO)
    model.Subtype = subtype
    return model
  }

  private function prepareAccountContactRole(contactDTO : ContactDTO) : AccountContactRole_AccountContact {
    var accountContact = new AccountContactRole_AccountContact()
    accountContact.Contact.PrimaryAddress.AddressLine1 = " "
    if ("person".equalsIgnoreCase(contactDTO.SubType)) {
      accountContact.Contact.entity_Person.FirstName = contactDTO.FirstName?.length() > 30 ? contactDTO.FirstName?.substring(0, 29) : contactDTO.FirstName?.trim()
      accountContact.Contact.entity_Person.LastName = contactDTO.LastName?.length() > 30 ? contactDTO.LastName?.substring(0, 29) : contactDTO.LastName?.trim()
      accountContact.Contact.entity_Person.DateOfBirth = CommonConversionHelper.parseDate(contactDTO.DateOfBirth)
      accountContact.Contact.entity_Person.CellPhone = contactDTO.CellPhone == null ? null : contactDTO.CellPhone.contains("000-") ? null : contactDTO.CellPhone
      accountContact.Contact.entity_Person.LicenseNumber_TDIC = contactDTO.LicenseNumber
      accountContact.Contact.entity_Person.LicenseState = contactDTO.LicenseState == null ? null : Jurisdiction.get(contactDTO.LicenseState)
      accountContact.Contact.entity_Person.Credential_TDIC = contactDTO.Credential_TDIC == null ? null : Credential_TDIC.get(contactDTO.Credential_TDIC)
      accountContact.Contact.entity_Person.Component_TDIC = contactDTO.Component_TDIC == null ? null : Component_TDIC.get(contactDTO.Component_TDIC)

    } else {
      accountContact.Contact.Name = contactDTO.Name?.length() > 60 ? contactDTO.Name?.substring(0, 59) : contactDTO.Name?.trim()
    }
    var contact = accountContact.Contact.$TypeInstance
    populatePolicyContactAddress(contact.PrimaryAddress.$TypeInstance, contactDTO.PrimaryAddressID)
    contact.EmailAddress1 = contactDTO.EmailAddress1
    var officialIDCount : Integer = 0
    contact.OfficialIDs = contact.OfficialIDs == null ? new Contact_OfficialIDs() : contact.OfficialIDs
    contact.OfficialIDs.Entry = contact.OfficialIDs.Entry == null ? new ArrayList<Contact_OfficialIDs_Entry>() : contact.OfficialIDs.Entry
    officialIDCount = prepareOfficialID(accountContact.Contact, contactDTO.TaxID, false, officialIDCount)
    officialIDCount = prepareOfficialID(accountContact.Contact, contactDTO.SSNbr, true, officialIDCount)
    prepareADANumber(accountContact.Contact, contactDTO.getADANumberOfficialID_TDIC(contact.PrimaryAddress.State), officialIDCount)

    if (contactDTO.PrimaryPhone?.equalsIgnoreCase(PrimaryPhoneType.TC_HOME.Code)) {
      contact.PrimaryPhone = PrimaryPhoneType.TC_HOME
    } else {
      contact.PrimaryPhone = PrimaryPhoneType.TC_WORK
    }
    contact.WorkPhone = contactDTO.WorkPhone == null ? null : contactDTO.WorkPhone.contains("000-") ? null : contactDTO.WorkPhone
    contact.HomePhone = contactDTO.HomePhone == null ? null : contactDTO.HomePhone.contains("000-") ? null : contactDTO.HomePhone
    contact.FaxPhone = contactDTO.FaxPhone == null ? null : contactDTO.FaxPhone.contains("000-") ? null : contactDTO.FaxPhone
    return accountContact
  }

  private function prepareBuildingCoverages(bopBuilding : BOPLocation_Buildings_Entry, locationID : String) {
    var bldgCovDAO = new BOPBuildingCovDAO({locationID})
    var bopCoverages = bldgCovDAO.retrieveAllUnprocessed()
    var bopCoveragesMap = new HashMap<String, List<BOPBuildingCovDTO>>()
    bopCoverages.each(\elt -> {
      if (!bopCoveragesMap.containsKey(elt.CovPatternCode)) {
        var list = new ArrayList<BOPBuildingCovDTO>()
        list.add(elt)
        bopCoveragesMap.put(elt.CovPatternCode, list)
      } else {
        bopCoveragesMap.get(elt.CovPatternCode).add(elt)
      }
    })
    for (cov in bopCoveragesMap.entrySet()index i) {
      bopBuilding.Coverages.Entry[i].PatternCode  = cov.Key
      for (covTerm in cov.Value index j) {
        if (covTerm.CovTermPatternCode != null) {
          bopBuilding.Coverages.Entry[i].CovTerms.Entry[j].PatternCode = covTerm.CovTermPatternCode
          bopBuilding.Coverages.Entry[i].CovTerms.Entry[j].DisplayValue =
              covTerm.DirectTerm == null ?
                  covTerm.ChoiceTerm == null ?
                      null :
                      "Y".equalsIgnoreCase(covTerm.ChoiceTerm) ? "true" : "N".equalsIgnoreCase(covTerm.ChoiceTerm) ? "false" : covTerm.ChoiceTerm
                  : covTerm.DirectTerm
        }
      }
    }
  }

  private function prepareBuildingModifiers(bopBuilding : BOPLocation_Buildings_Entry, locationID : String) {
    var bldgModDAO = new BuildingModifierDAO({locationID})
    var modRes = bldgModDAO.retrieveAllUnprocessed() //Below logic is due to ETL design of having closed end credit as the base modifier table link to rate factors table
    var modifierID : String
    var count = 0
    for (mod in modRes index i) {
      bopBuilding.Modifiers.Entry[i].Pattern.CodeIdentifier = "BOPClosedEndCredit_TDIC"
      bopBuilding.Modifiers.Entry[i].BooleanModifier = "Y".equalsIgnoreCase(mod.ValueFinal) ? true : false
      bopBuilding.Modifiers.Entry[i].DataType = ModifierDataType.TC_BOOLEAN
      modifierID = mod.PublicID ///this is used to map IRPM Property
      count++
    }
    var bldgRateFactorDAO = new BuildingRateFactorDAO({modifierID})
    var factorsRes = bldgRateFactorDAO.retrieveAllUnprocessed()
    if (!factorsRes.Empty) {
      bopBuilding.Modifiers.Entry[count].Pattern.CodeIdentifier = "BOPIRPMProperty_TDIC"
      bopBuilding.Modifiers.Entry[count].DataType = ModifierDataType.TC_RATE
      for (factor in factorsRes index i) {
        bopBuilding.Modifiers.Entry[count].RateFactors.Entry[i].Assessment = factor.Assessment
        //bopBuilding.Modifiers.Entry[count].RateFactors.Entry[i].Pattern.CodeIdentifier = ConversionConstants.RATEFACTOR_MAPPER.get(factor.PatternCode)
        bopBuilding.Modifiers.Entry[count].RateFactors.Entry[i].RateFactorType = RateFactorType.get(ConversionConstants.RATEFACTOR_MAPPER.get(factor.PatternCode))
        bopBuilding.Modifiers.Entry[count].RateFactors.Entry[i].Justification = ConversionConstants.DEFAULT_JUSTIFICATION
      }
    }
  }

  private function prepareLongManuscriptPNI(bopBuilding : BOPLocation_Buildings_Entry, remainingPNI : String){
    if(null == remainingPNI or null == bopBuilding.Coverages.Entry or bopBuilding.Coverages.Entry.Empty){
      return
    }
    var manuscriptCov = new BOPBuilding_Coverages_Entry()
    manuscriptCov.PatternCode = ConversionConstants.CP_MANUSCRIPT_PATTERN_CODE
    var covTerms = new BOPBuildingCov_CovTerms()
    covTerms.Entry = new ArrayList<BOPBuildingCov_CovTerms_Entry>()
    var term = new BOPBuildingCov_CovTerms_Entry()
    term.PatternCode = ConversionConstants.CP_MANUSCRIPT_TYPE_PATTERN_CODE
    term.DisplayValue = ConversionConstants.CP_MANUSCRIPT_TYPE_NAMEDINSURED_CONTINUED
    covTerms.Entry.add(term)
    var term2 = new BOPBuildingCov_CovTerms_Entry()
    term2.PatternCode = ConversionConstants.CP_MANUSCRIPT_DESC_PATTERN_CODE
    term2.DisplayValue = remainingPNI
    covTerms.Entry.add(term2)
    manuscriptCov.CovTerms = covTerms
    bopBuilding.Coverages.Entry.add(manuscriptCov)
  }

  private function prepareBOPManuscript_TDIC(locationDTO : PolicyLocationDTO) {
    var manuScriptTypes = ConversionConstants.BOP_COVERAGETERM_OPTIONS_MAPPER.get("BOPManuscriptType_TDIC")
    var manuscriptDAO = new BOPManuscriptDAO({locationDTO.PublicID})
    var manuscripts = manuscriptDAO.retrieveAllUnprocessed()
    for (manuscript in manuscripts index i) {
      var addlInsuredDAO = new AddlInsuredDAO(new Object[]{manuscript.AdditionalInsured})
      var addlContacts = addlInsuredDAO.retrieveAllUnprocessed()
      for (contactRole in addlContacts) {
        var contactDAO = new ContactDAO({contactRole.ContactID})
        var contactDTO = contactDAO.retrieveAllUnprocessed().first()
        _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].
            policyAddInsured_TDIC.PolicyAddlInsured.AccountContactRole = preparePolicyContactRole(contactDTO)
        _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].policyAddInsured_TDIC.Desciption = contactRole.Description
        _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].policyAddInsured_TDIC.AdditionalInsuredType = typekey.AdditionalInsuredType.TC_OTHER
      }
      var mortgageesDAO = new AddlInsuredDAO(new Object[]{manuscript.Mortgagee})
      var mortgagees = mortgageesDAO.retrieveAllUnprocessed()
      for (mortagee in mortgagees) {
        var contactDAO = new ContactDAO({mortagee.ContactID})
        var contactDTO = contactDAO.retrieveAllUnprocessed().first()
        _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].
            PolicyMortgagee.AccountContactRole = prepareMortgageeContactRole(contactDTO, typekey.AccountContactRole.TC_MORTGAGEE_TDIC)
        _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].PolicyMortgagee.LoanNumberString = mortagee.LoanNumber
      }
      var lossPayeeDAO = new AddlInsuredDAO(new Object[]{manuscript.LossPayee})
      var lossPayees = lossPayeeDAO.retrieveAllUnprocessed()
      for (lossPayee in lossPayees) {
        var contactDAO = new ContactDAO({lossPayee.ContactID})
        var contactDTO = contactDAO.retrieveAllUnprocessed().first()
        _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].PolicyLossPayee.AccountContactRole = prepareLossPayeeContactRole(contactDTO, typekey.AccountContactRole.TC_LOSSPAYEE_TDIC)
        _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].PolicyLossPayee.LoanNumberString = lossPayee.LoanNumber
      }
      _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].ManuscriptDescription = manuscript.Manuscript_Des
      _policyPeriod.BOPLine.BOPManuscript_TDIC.Entry[i].ManuscriptType = typekey.BOPManuscriptType_TDIC.get(manuScriptTypes.get(manuscript.Manuscript_Type))
    }
  }

}