package tdic.pc.integ.plugins.contactconfigplugin

uses gw.plugin.contact.impl.ContactConfigPlugin
uses gw.plugin.contact.impl.ContactConfig
uses gw.plugin.contact.IContactConfigPlugin
uses typekey.AccountContactRole
uses typekey.PolicyContactRole

/**
 * Shane Sheridan 08/22/2014
 * BrittoS 02/18/2020 refactored for other LoBs
 * Extends ContactConfigPlugin for additional AccountContactRoles required by TDIC.
 */
@Export
class TDIC_ContactConfigPlugin extends ContactConfigPlugin implements IContactConfigPlugin{

  /**
   * Shane Sheridan 08/22/2014
   *
   * Must override to add TDIC AccountContactRoles.
  */
  protected property get DefaultConfigs() : ContactConfig[] {
    return {
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, TC_ACCOUNTHOLDER,      {}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, TC_ACCOUNTINGCONTACT,  {}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, TC_ADDITIONALINSURED,  {TC_POLICYADDLINSURED}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, TC_BILLINGCONTACT,     {TC_POLICYBILLINGCONTACT}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, TC_CLAIMSINFOCONTACT,  {}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, TC_NAMEDINSURED,       {TC_POLICYPRINAMEDINSURED, TC_POLICYSECNAMEDINSURED, TC_POLICYADDLNAMEDINSURED}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, TC_OWNEROFFICER,       {TC_POLICYOWNEROFFICER, TC_WC7POLICYOWNEROFFICER, TC_WC7EXCLUDEDOWNEROFFICER, TC_WC7INCLUDEDOWNEROFFICER,PolicyContactRole.TC_BOPPOLICYOWNEROFFICER_TDIC, PolicyContactRole.TC_GLPOLICYOWNEROFFICER_TDIC}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, typekey.AccountContactRole.TC_MORTGAGEE_TDIC,        {PolicyContactRole.TC_POLICYMORTGAGEE_TDIC}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, typekey.AccountContactRole.TC_LOSSPAYEE_TDIC,        {PolicyContactRole.TC_POLICYLOSSPAYEE_TDIC}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, AccountContactRole.TC_CERTIFICATEHOLDER_TDIC,        {PolicyContactRole.TC_POLICYCERTIFICATEHOLDER_TDIC}),
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, typekey.AccountContactRole.TC_AUDITCONTACT_TDIC,     {}),
        new ContactConfig(true, {           TC_PERSON}, typekey.AccountContactRole.TC_OFFICEMANAGER_TDIC,    {}),
        /*
         * US890, robk
         * New EntityOwner contact role.
         */
        new ContactConfig(true, {TC_COMPANY, TC_PERSON}, typekey.AccountContactRole.TC_ENTITYOWNER_TDIC,       {TC_POLICYENTITYOWNER_TDIC,PolicyContactRole.TC_BOPPOLICYENTITYOWNER_TDIC, PolicyContactRole.TC_GLPOLICYENTITYOWNER_TDIC})
    }
  }

  /**
   * US740
   * Shane Sheridan 03/06/2015
   */
  private property get AvailableAccountContactRoles_TDIC() : typekey.AccountContactRole[] {
    var listOfAvailableRole = {
        typekey.AccountContactRole.TC_ACCOUNTINGCONTACT,
        typekey.AccountContactRole.TC_AUDITCONTACT_TDIC,
        typekey.AccountContactRole.TC_BILLINGCONTACT,
        typekey.AccountContactRole.TC_CLAIMSINFOCONTACT,
        typekey.AccountContactRole.TC_LOSSPAYEE_TDIC,
        typekey.AccountContactRole.TC_MORTGAGEE_TDIC,
        typekey.AccountContactRole.TC_NAMEDINSURED,
        typekey.AccountContactRole.TC_OFFICEMANAGER_TDIC,
        typekey.AccountContactRole.TC_OWNEROFFICER,
        typekey.AccountContactRole.TC_ENTITYOWNER_TDIC
    }

    return listOfAvailableRole.toTypedArray()
  }

  /**
   * US740
   * Shane Sheridan 03/06/2015
  */
  private function isAccountContactTypeAvailable_TDIC(subtypeKey : typekey.AccountContactRole) : boolean {
    return AvailableAccountContactRoles_TDIC.contains(subtypeKey)
  }

  /**
   * US740
   * Shane Sheridan 03/06/2015
   */
   property get AvailableAccountContactRoleTypes() : typekey.AccountContactRole[] {
    return typekey.AccountContactRole.getTypeKeys(false).where(\ a -> isAccountContactTypeAvailable(a) and isAccountContactTypeAvailable_TDIC(a)).toTypedArray()
  }
}