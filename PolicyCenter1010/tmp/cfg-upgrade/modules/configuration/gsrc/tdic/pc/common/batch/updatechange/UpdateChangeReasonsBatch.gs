package tdic.pc.common.batch.updatechange

uses gw.api.database.Query
uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses java.lang.Exception

uses tdic.pc.config.job.policychange.ChangeReasons
uses org.slf4j.LoggerFactory

class UpdateChangeReasonsBatch extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${UpdateChangeReasonsBatch.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_UPDATECHANGEREASONS_TDIC);
  }

  /*
    Used to determine if conditions are met to begin processing
      Return true: doWork() will be called
      Return false: only a history event will be written
      Avoids lengthy processing in doWork() if there is nothing to do
   */
  override function checkInitialConditions() : boolean {
    return true;
  }

  override function doWork() {
    var changeReasons = new ChangeReasons();

    // Get PolicyChange entities that were started during automatic or manual migration.
    var query = Query.make(PolicyChange);
    query.compare ("DataSource_TDIC", NotEquals, DataSource_TDIC.TC_POLICYCENTER);

    _logger.info (_LOG_TAG + "Querying for PolicyChange entities.");
    var results = query.select();
    _logger.debug (_LOG_TAG + "Found " + results.Count + " potential records.");

    OperationsExpected = results.Count;

    for (policyChange in results) {
      try {
        // Copy encrypted value to unencrypted value
        Transaction.runWithNewBundle (\bundle -> {
          _logger.trace (_LOG_TAG + "Updating change reasons for Policy Change " + policyChange.JobNumber);
          policyChange = bundle.add(policyChange);
          changeReasons.validateChangeReasons (policyChange.LatestPeriod, policyChange.ChangeReasons);
        }, "iu");
      } catch (e : Exception) {
        _logger.error (_LOG_TAG + "Exception occurred while updating change reasons for Policy Change "
                          + policyChange.JobNumber + ": " + e.toString());
        incrementOperationsFailed();
      }
      incrementOperationsCompleted();
    }

    _logger.info (_LOG_TAG + "Updated " + (OperationsCompleted - OperationsFailed) + " Policy Changes.  "
                    + OperationsFailed + " updates failed.");
  }

  /*
  Called by Guidewire to request that process self-terminate
    Return true if request will be honored
    Return false if request will not be honored
  Handled by setting monitor flag in code and exiting any loops
  */
  override function requestTermination() : boolean {
    return false;
  }
}