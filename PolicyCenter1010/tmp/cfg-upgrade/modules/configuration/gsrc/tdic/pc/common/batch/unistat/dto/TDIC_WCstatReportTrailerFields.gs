package tdic.pc.common.batch.unistat.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator
/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/19/14
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCstatReportTrailerFields extends TDIC_FlatFileLineGenerator {

  var _filler : String as Filler
  var _recordTypeCode : int as RecordTypeCode
  var _detailRecordCountTotal : long as DetailRecordCountTotal
  var _unitReportsSubmittedTotal : long as UnitReportsSubmittedTotal
  var _primaryEffectiveYear : String as PrimaryEffectiveYear
  var _primaryEffectiveMonth : String as PrimaryEffectiveMonth
  var _icrTotal : String as ICRTotal
  var _futureReserved : String as FutureReserved

}