package tdic.pc.config.rating.wc7

uses com.google.common.base.Optional
uses gw.api.domain.Clause
uses gw.lob.wc7.rating.WC7RatingPeriod


public interface WC7StepData {
  property get PolicyLineVersion() : WC7Line

  property get RateBook() : RateBook

  property set RateBook(book : RateBook)

  property get OptionalCoverage() : Optional<Clause>

  property get FocusBean() : EffDated

  property set FocusBean(bean : EffDated)

  property get RatingPeriod() : WC7RatingPeriod

  property set RatingPeriod(period : WC7RatingPeriod)
}