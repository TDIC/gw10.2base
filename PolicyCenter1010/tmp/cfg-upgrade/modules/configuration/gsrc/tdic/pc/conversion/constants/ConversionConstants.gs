package tdic.pc.conversion.constants

class ConversionConstants {

  public static final var CONVERSION_USER_FIRSTNAME : String = "Conversion"
  public static final var CONVERSION_USER_LASTNAME : String = "User"
  public static final var DEFAULT_PAYMENT_PLAN : String = "DirectBill"
  public static final var DEFAULT_ORGTYPE_DESC : String = "Migrated Renewal"
  //public static final var DEFAULT_ACCOUNT_ORG_TYPE : String ="other"
  public static final var DEFAULT_UWCOMPANY : String = "The Dentists Insurance Company"
  public static final var DEFAULT_INDUSTRYCODE : String = "8021"
  public static final var DEFAULT_PRODUCERCODE : String = "TDIC"
  public static final var DEFAULT_ADA : String = "000000000"
  public static final var WORKERSCOMP_PRODUCTCODE : String = "WC7WorkersComp"
  public static final var PRODUCERCODE_BY_STATE :Map<String, String> =
      new HashMap<String, String>(){"CA" -> "TDIC", "AZ" -> "TDIC", "IL" -> "TDIC", "MN" -> "TDIC", "NV" -> "TDIC", "ND" -> "TDIC",
          "AK" -> "CHI", "HI" -> "JHI", "NJ" -> "MAIR", "PA" -> "PDAIS","ID" -> "IDIA","WA" -> "WDIA"}
  public static final var DEFAULT_LEGACYSOURCE : String = "AS400" //For R2 it will always be AS400
  public static final var DEFAULT_TERMTYPE : String = "Annual"
  public static final var DEFAULT_JUSTIFICATION : String = "Migrated Renewal"
  public static final var TAXID_FORMAT : String = "\\d{3}-\\d{2}-\\d{4}"
  public static final var CONTACT_PERSON : String = "person"
  public static final var NEWDENTIST_AS400_PTYPE : String = "7"
  public static final var NEWDENTIST_CONDITION_PATTERNCODE : String = "GLNewDentistDiscount_TDIC"
  public static final var DENTALSCHOOL_QUESTIONCODE : String = "DentalScool_TDIC"
  public static final var LICENSEDFIRSTTIME_QUESTIONCODE : String = "LicensedFirstTimeInLast12Months_TDIC"
  public static final var GLUNDERWRITING_QS : String = "GLUnderwriting"
  public static final var GLUNDERWRITING_QS_CHANGES :  Map<String, String> = new HashMap<String, String>(){
      "CompletedSpecialityProgramPLCMOC_TDIC" ->	"HaveYouCompletedASpecialityProgram_TDIC",
      "DentalLicenseExpDatePLCMOC_TDIC" ->	"DentalLicenseExpDate_TDIC",
      "DentalLicenseNumberPLCMOC_TDIC" ->	"DentalLicenseNumber_TDIC",
      "DentalLicenseStatePLCMOC_TDIC" ->	"DentalLicenseState_TDIC",
      "DentalSchoolPLCMOC_TDIC"	-> "DentalScool_TDIC",
      "FullTimeMemberOfDentalSchoolFacultyPLCMOC_TDIC"	->	"AreYouFullTimeMemberOfDentalSchool_TDIC",
      "FullTimeStudentDentalPostGradProgramPLCMOC_TDIC"	->	"AreYouFullTimeStudentEnrolledInDentalPostGrad_TDIC",
      "HaveYouCompletedGPRAEGDPLCMOC_TDIC"	->	"CompletedGPRORAEGD_TDIC",
      "PerformSleepApneaSnoringTherapyPLCMOC_TDIC"	->	"PerformSleepApneaSnoringTherapy_TDIC",
      "PracticeAsOfficerDirectorShareHolderPLCMOC_TDIC"	->	"PracticeAsOfficerDirectorShareholder_TDIC",
      "PracticeAsPartnerInDentalPartnershipPLCMOC_TDIC"	->	"PracticeAsPartnerInDentalPartnership_TDIC",
      "YearFirstBeganPracticingINUSPLCMOC_TDIC"	->	"YearFristPracticingInUS_TDIC",
      "YearGrantedPLCMOC_TDIC"	->	"YearGraduated_TDIC"
  }
  public static final var GLUNDERWRITING_ANESTHETIC_QS :  Map<typekey.AnestheticModilities_TDIC, String> =
      new HashMap<typekey.AnestheticModilities_TDIC, String>(){
          AnestheticModilities_TDIC.TC_NONE -> "None",
          AnestheticModilities_TDIC.TC_CONSCIOUSSEDATIONINHOSPITAL -> "Conscioussedation",
          AnestheticModilities_TDIC.TC_CONSCIOUSSEDATIONINOFFICE -> "Conscioussedationoffice",
          AnestheticModilities_TDIC.TC_GENERALANESTHESIAINOFFICE -> "Generalanesthesiaoffice",
          AnestheticModilities_TDIC.TC_LOCALANESTHESIA -> "Localanesthesia",
          AnestheticModilities_TDIC.TC_N2OO2ANALGESIA -> "N2OO2analgesia",
          AnestheticModilities_TDIC.TC_ORALCONSCIOUSSEDATION -> "Oralconscioussedation"
      }

  public static final var CHOICEANSWER_MAPPER : Map<String, Map<String, String>> = new HashMap<String, Map<String, String>>(){
      "TypeOfIdentityRecoveryCoverageDesiredPLCMOC_TDIC" -> {"Individual" -> "zfcg8edq0gtjsemljacea47nd68",
          "Family" -> "z3tiopt8h5b1e10pdb948oochqb",
          "None" -> "zbficvsoffbo07d7b3v5jgtt35b"
      },
      "TreatPatientsSpecialtyPLCMOC_TDIC" -> {"00generaldentist_tdic" -> "z6jhco566mhbo2hh3976kudgtm9",
          "30pediatricdentistry_tdic" -> "z98goa5ov2dgue7qbsqd5o9s8i8",
          "10oralsurgery_tdic" -> "zaaje5b6tpca685bcbbbrg0ocg9",
          "90dentalanesthesiology_tdic" -> "znuigfep4fuv66r8s9e0rr820s8",
          "15endodontics_tdic" -> "zppjcqkh02ng3abaar0rhmj30b9",
          "60oralpathology_tdic" -> "z9ljeaq4jv4d8c161br0jshb54a",
          "20orthodontics_tdic" -> "zu8iucenivt3hcjpgfban5cfb19",
          "40periodontics_tdic" -> "zl7iuov691n77887fgieftahqab",
          "50prosthodontics_tdic" -> "z0nhqj8hh8r4n0mcgni3uhvk3sb",
          "other" -> "z6rie0ikiaut1f5lq7ps2iv4698"
      },
      "SpecialtyPLCMOC_TDIC" -> {"generaldentist" -> "zocjk6ugjmkfo8vtmb8qm7oq9o9",
          "pediatricdentistry" -> "zjej429br8qr0c9rd5mpl8te0h9",
          "oralsurgeon" -> "zp4jm75ladk883h7ccnov2lkdd9",
          "dentistsanesthesiology" -> "zqjhu0a7lj6f34d76q9u8qp3c7a",
          "endodontics" -> "znai61m5bsqip52qjbga5f99dq8",
          "oralpathologist" -> "zagiq4f2v509p1uci3j3sr9g3qa",
          "orthodontics" -> "zvmgq9qsl2ai44q6dl0m9jmvbl8",
          "periodontics" -> "zogh6c8mn3kitf1ji4vm2e089g8",
          "prosthodontics" -> "z9ig6thpa1dvqavhklt1alavj59",
          "other" -> "zr6hcei4de75qegfk4q7hmkbe0a"
      },
      "DentalLicenseStatePLCMOC_TDIC" -> {"CA" -> "zf0hcm54um50h0p745ad9jbcc8b",
          "California" -> "zf0hcm54um50h0p745ad9jbcc8b",
          "AK" -> "zk6immabntj6955qmke7ujkfq08",
          "Alaska" -> "zk6immabntj6955qmke7ujkfq08",
          "Arizona" -> "zm5ikp8jkbme6em2ouq7jm6obcb",
          "AZ" -> "zm5ikp8jkbme6em2ouq7jm6obcb",
          "HI" -> "zaphut44acba63o5fjuhdtabjda",
          "Hawaii" -> "zaphut44acba63o5fjuhdtabjda",
          "IA" -> "z7li2avf7m4gcecpc6folsijk4a",
          "Iowa" -> "z7li2avf7m4gcecpc6folsijk4a",
          "IL" -> "z18j0iiin14eifkhoru4b6tq18a",
          "Illinois" -> "z18j0iiin14eifkhoru4b6tq18a",
          "MN" -> "zojhe631h1vcqfolja2m7ac15ea",
          "Minnesota" -> "zojhe631h1vcqfolja2m7ac15ea",
          "NC" -> "zdbgo6jjinlmc6rl4rn7jg503j9",
          "North Carolina" -> "zdbgo6jjinlmc6rl4rn7jg503j9",
          "ND" -> "zfbhihjmbgnm94cguu0710hlve9",
          "North Dakota" -> "zfbhihjmbgnm94cguu0710hlve9",
          "NJ" -> "zmrjmln4qkndo70qdrrvoaijjc8",
          "New Jersey" -> "zmrjmln4qkndo70qdrrvoaijjc8",
          "NV" -> "znajqne89024s7p8ns34d6qb4k8",
          "Nevada" -> "znajqne89024s7p8ns34d6qb4k8",
          "NY" -> "z17g0qedktc0cf7o1ftrbueqgdb",
          "New York" -> "z17g0qedktc0cf7o1ftrbueqgdb",
          "PA" -> "zt1g26uv0j3e2aq58voefbam978",
          "Pennsylvania" -> "zt1g26uv0j3e2aq58voefbam978",
          "VA" -> "z1cic5jtlc7mvarvtj4hss6mcf8",
          "Virginia" -> "z1cic5jtlc7mvarvtj4hss6mcf8",
          "WI" -> "zi2hae94hgk41acdlqhompm2h2a",
          "Wisconsin" -> "zi2hae94hgk41acdlqhompm2h2a",
          "ID" -> "zftjgl0g4fasf2ol1rh99moku1a",
          "Idaho" -> "zftjgl0g4fasf2ol1rh99moku1a",
          "OR" -> "zcai23q835fqn31lt83tl046pf8",
          "Oregon" -> "zcai23q835fqn31lt83tl046pf8",
          "TN" -> "zjbj6q85s6vdk35camk7n61ahqb",
          "Tennessee" -> "zjbj6q85s6vdk35camk7n61ahqb",
          "WA" -> "zetj61mqvihdb76n5umr8276fma",
          "Washington" -> "zetj61mqvihdb76n5umr8276fma",
          "MT" -> "z3fjct5rlh0e94tpp7s9vh7svn9",
          "Montana" -> "z3fjct5rlh0e94tpp7s9vh7svn9"},
      "DentalLicenseState_TDIC" -> {"CA" -> "zf0hcm54um50h0p745ad9jbcc8b",
          "California" -> "zf0hcm54um50h0p745ad9jbcc8b",
          "AK" -> "zk6immabntj6955qmke7ujkfq08",
          "Alaska" -> "zk6immabntj6955qmke7ujkfq08",
          "Arizona" -> "zm5ikp8jkbme6em2ouq7jm6obcb",
          "AZ" -> "zm5ikp8jkbme6em2ouq7jm6obcb",
          "HI" -> "zaphut44acba63o5fjuhdtabjda",
          "Hawaii" -> "zaphut44acba63o5fjuhdtabjda",
          "IA" -> "z7li2avf7m4gcecpc6folsijk4a",
          "Iowa" -> "z7li2avf7m4gcecpc6folsijk4a",
          "IL" -> "z18j0iiin14eifkhoru4b6tq18a",
          "Illinois" -> "z18j0iiin14eifkhoru4b6tq18a",
          "MN" -> "zojhe631h1vcqfolja2m7ac15ea",
          "Minnesota" -> "zojhe631h1vcqfolja2m7ac15ea",
          "NC" -> "zdbgo6jjinlmc6rl4rn7jg503j9",
          "North Carolina" -> "zdbgo6jjinlmc6rl4rn7jg503j9",
          "ND" -> "zfbhihjmbgnm94cguu0710hlve9",
          "North Dakota" -> "zfbhihjmbgnm94cguu0710hlve9",
          "NJ" -> "zmrjmln4qkndo70qdrrvoaijjc8",
          "New Jersey" -> "zmrjmln4qkndo70qdrrvoaijjc8",
          "NV" -> "znajqne89024s7p8ns34d6qb4k8",
          "Nevada" -> "znajqne89024s7p8ns34d6qb4k8",
          "NY" -> "z17g0qedktc0cf7o1ftrbueqgdb",
          "New York" -> "z17g0qedktc0cf7o1ftrbueqgdb",
          "PA" -> "zt1g26uv0j3e2aq58voefbam978",
          "Pennsylvania" -> "zt1g26uv0j3e2aq58voefbam978",
          "VA" -> "z1cic5jtlc7mvarvtj4hss6mcf8",
          "Virginia" -> "z1cic5jtlc7mvarvtj4hss6mcf8",
          "WI" -> "zi2hae94hgk41acdlqhompm2h2a",
          "Wisconsin" -> "zi2hae94hgk41acdlqhompm2h2a",
          "ID" -> "zftjgl0g4fasf2ol1rh99moku1a",
          "Idaho" -> "zftjgl0g4fasf2ol1rh99moku1a",
          "OR" -> "zcai23q835fqn31lt83tl046pf8",
          "Oregon" -> "zcai23q835fqn31lt83tl046pf8",
          "TN" -> "zjbj6q85s6vdk35camk7n61ahqb",
          "Tennessee" -> "zjbj6q85s6vdk35camk7n61ahqb",
          "WA" -> "zetj61mqvihdb76n5umr8276fma",
          "Washington" -> "zetj61mqvihdb76n5umr8276fma",
          "MT" -> "z3fjct5rlh0e94tpp7s9vh7svn9",
          "Montana" -> "z3fjct5rlh0e94tpp7s9vh7svn9"},
      "WhatCaptaincyYouProvideServicesPLCMOC_TDIC" -> {"owner" -> "znsgi5h9fsolaa39dqinamm7eda",
          "1" -> "znsgi5h9fsolaa39dqinamm7eda",
          "employee" -> "zrbi4dd02onru1g74rlivls3iv8",
          "2" -> "zrbi4dd02onru1g74rlivls3iv8",
          "indcontractor" -> "zinjmfqo8s6mab9bi17tgid9ci8",
          "3" -> "zinjmfqo8s6mab9bi17tgid9ci8",
          "other" -> "zg3jg6jpv3qok8uidto4avu1o28",
          "4" -> "zg3jg6jpv3qok8uidto4avu1o28"
      }
  }
  public static final var OFFERRINGTYPE_MAPPER : Map<String, String> = new HashMap<String, String>(){
      "PL-CM" -> "PLClaimsMade_TDIC",
      "CYB" -> "PLCyberLiab_TDIC",
      "BOP" -> "BOPBusinessOwnersPolicy_TDIC",
      "LRP" -> "BOPLessorsRisk_TDIC",
      "PL-OCC" -> "PLOccurence_TDIC"
  }
  public static final var COVERAGE_MAPPER : Map<String, String> = new HashMap<String, String>(){
      "BOPEqSpBldgCov" -> "BOPEqBldgCov",
      "BOPILMineSubCond_TDIC" -> "BOPILMineSubCov_TDIC"
  }

  public static final var PAYMENTPLAN_MAPPER : Map<String, String> = new HashMap<String, String>(){
      "1 pay" -> "TDIC Annual v1",
      "2 pay" -> "TDIC Semi-Annual v1",
      "12 pay" -> "TDIC Monthly APW"
  }

  public static final var PAYMENT_FREQUENCY_MAPPER : Map<String, String> = new HashMap<String, String>(){
      "1 pay" -> "everyyear",
      "2 pay" -> "everysixmonths",
      "12 pay" -> "monthly"
  }

  public static final var PAYMENTPLAN_BILLING_MAPPER : Map<String, String> = new HashMap<String, String>(){
      "1 pay" -> "bc-loc:1003",
      "2 pay" -> "bc-loc:1004",
      "12 pay" -> "bc-loc:1005"
  }

  public static final var RATEFACTOR_MAPPER : Map<String, String> = new HashMap<String, String>(){
      "Building Features" -> "Building",
      "Location" -> "Location",
      "Premises and Equipment" -> "Premises",
      "Protection" -> "Protection"
  }

  public static final var GL_CONDITION_TERM_OPTIONS_MAPPER : Map<String, Map<String, String>> = new HashMap<String, Map<String, String>>(){
      "GLCEReason_TDIC" -> new HashMap<String, String>(){
          "other" -> "7",
          "active military service" -> "6",
          "dependent care leave" -> "2",
          "educational leave" -> "4",
          "maternal/paternal leave" -> "1",
          "physical damage to my office/lack of access to office/lack of essential services" ->"5",
          "short term disability leave" -> "3"
      }
  }

  public static final var GL_COVERAGETERM_OPTIONS_MAPPER : Map<String, Map<String, String>> = new HashMap<String, Map<String, String>>(){
      "GLDPLPerClaimLimit_TDIC" -> new HashMap<String, String>(){
          "500K" -> "500",
          "1M" -> "1000",
          "1500K" -> "1500",
          "3M" -> "3000",
          "5M" -> "5000"
      },
      "GLDPLPerOccLimit_TDIC" -> new HashMap<String, String>(){
          "500K" -> "500",
          "1M" -> "1000",
          "1500K" -> "1500",
          "3M" -> "3000",
          "5M" -> "5000"
      },
      "GLDEPLLimit_TDIC" -> new HashMap<String, String>(){
          "50,000" -> "50",
          "100,000" -> "100"
      },
      "GLCybLimit_TDIC" -> new HashMap<String, String>(){
          "50,000" -> "50",
          "100,000" -> "100",
          "250,000" -> "250"
      },
      "GLManuscriptType_TDIC" -> new HashMap<String, String>(){
          "Additional Insured Amended To Read:" -> "additionalInsuredContinued",
          "1" -> "additionalInsuredContinued",
          "Named Insured Amended To Read:" -> "namedInsuredContinued",
          "2" -> "namedInsuredContinued",
          "Blank Manuscript Endorsement Option" -> "blankmanuscript",
          "3" -> "blankmanuscript",
          "As respects to Cyber Liability coverage only, the named insured is amended to read:" -> "respectsCyberLiability",
          "4" -> "respectsCyberLiability",
          "Event Schedule CDA/PDA" -> "eventSchedule",
          "5" -> "eventSchedule",
          "Schedule of named insureds to include" -> "scheduleNamed",
          "6" -> "scheduleNamed",
          "Certificate Holder Name Amended To Read:" -> "certificateholdernameamendedtoread",
          "7" -> "certificateholdernameamendedtoread"
      }

  }

  public static final var GLFULLTIMEFACULTY_DISCOUNT_TERMS : List<String> = {"GLFTFSchoolCode_TDIC", "GLFTPGGSchoolCode_TDIC"}

  public static final var BOP_COVERAGETERM_OPTIONS_MAPPER : Map<String, Map<String, String>> = new HashMap<String, Map<String, String>>(){
      "BOPEQBldgClass_TDIC" -> new HashMap<String, String>(){
          "1C" -> "1",
          "1D" -> "2",
          "2A" -> "3",
          "2B" -> "4",
          "3A" -> "5",
          "3B" -> "6",
          "3C" -> "7",
          "4A" -> "8",
          "4B" -> "9",
          "4C" -> "10",
          "4D" -> "11",
          "5A" -> "12",
          "5AA" -> "13",
          "5B" -> "14",
          "5C" -> "15"
      },
      "BOPManuscriptType_TDIC" -> new HashMap<String, String>(){
          "As respects to Building coverage only, the named insured is amended to read:" -> "buildingcoverageonly",
          "As respects to Business Personal Property coverage only, the named insured is amended to read" -> "businesspersonalpropertycoverage",
          "As respects to Building & Business Personal Property coverage only, the named insured is amended to read:" -> "businesspropertycoverage",
          "As respects to Business Personal Property coverage only, the covered location is amended to read:" -> "propertycoverageonly",
          "Additional Insured Amended To Read:" -> "additionalinsuredcontinued",
          "Named Insured Amended To Read:" -> "namedinsuredcontinued",
          "Blank Manuscript Endorsement" -> "blankmanuscriptendorsement",
		      "Loss Payee Amended To Read:" -> "losspayeeamendedtoread",
		      "Mortgagee Amended To Read:" -> "mortgageeamendedtoread",
          "1" ->"buildingcoverageonly",
          "2" ->"businesspersonalpropertycoverage",
          "3" ->"businesspropertycoverage",
          "4" ->"propertycoverageonly",
          "5" ->"additionalinsuredcontinued",
          "6" ->"namedinsuredcontinued",
          "7" ->"blankmanuscriptendorsement",
		      "8" ->"losspayeeamendedtoread",
		      "9" ->"mortgageeamendedtoread"
      },
      "BOPEDTotInclLimit_TDIC" -> new HashMap<String, String>(){
          "100000" -> "100",
          "110000" -> "110",
          "125000" -> "125",
          "150000" -> "150",
          "175000" -> "175",
          "200000" -> "200",
          "225000" -> "225",
          "250000" -> "250",
          "50000" -> "50",
          "75000" -> "75"
      },
      "BOPFungiTotIncLimit_TDIC" -> new HashMap<String, String>(){
          "25000.0000" -> "25",
          "50000.0000" -> "50",
          "25000" -> "25",
          "50000" -> "50"
      }

  }

  public static final var AUTO_POPULATE_ENDORS_LIST : List<String> = new ArrayList<String>(){
      "GLMobileDentalClinicCov_TDIC",
      "GLLocumTenensCov_TDIC",
      "GLAdditionalInsuredCov_TDIC",
      "GLNameOTDCov_TDIC",
      "GLStateExclCov_TDIC",
      "GLCOICov_TDIC",
      "GLBLAICov_TDIC",
      "GLExcludedServiceSched_TDIC"
  }

  public enum ProductCode {
    GENERALLIABILITY("GeneralLiability"),
    BUSINESSOWNERS("BusinessOwners")
    private var _type : String as Type

    private construct(type : String) {
      _type = type
    }
  }

  public static final var PRODUCTCODE_MAPPER : Map<String, ProductCode> = new HashMap<String, ProductCode>(){
      "PL-OCC" -> ProductCode.GENERALLIABILITY,
      "PL-CM" -> ProductCode.GENERALLIABILITY,
      "BOP" -> ProductCode.BUSINESSOWNERS,
      "LRP" -> ProductCode.BUSINESSOWNERS,
      "CYB" -> ProductCode.GENERALLIABILITY
  }

  public enum OfferingCodeIdentifier {
    BOP("BOPBusinessOwnersPolicy_TDIC"),
    LRP("BOPLessorsRisk_TDIC"),
    CYB("PLCyberLiab_TDIC"),
    PL_CM("PLClaimsMade_TDIC"),
    PL_OCC("PLOccurence_TDIC")
    private var _type : String as Type

    private construct(type : String) {
      _type = type
    }
  }

  public static final var OFFERRING_IDENTIFIER_MAP : Map<String, String> = {
      ConversionConstants.OfferingCodeIdentifier.PL_CM.Code.replace("_", "-") -> "1",
      ConversionConstants.OfferingCodeIdentifier.PL_OCC.Code.replace("_", "-") -> "2",
      ConversionConstants.OfferingCodeIdentifier.BOP.Code -> "5",
      ConversionConstants.OfferingCodeIdentifier.LRP.Code -> "8",
      ConversionConstants.OfferingCodeIdentifier.CYB.Code -> "9"
  }

  public enum BatchType {
    ACCOUNT_CONVERSION("AC"),
    ACCOUNT_PAYLOAD("AP"),
    POLICY_CONVERSION("PC"),
    POLICY_PAYLOAD("PP")
    private var _type : String as Type

    private construct(type : String) {
      _type = type
    }
  }

  public static final var MAX_ACCOUNTS_PAYLOAD : int = 1000
  public static final var MAX_POLICY_PAYLOAD : int = 1000000

  public static final var ERROR_MAPPER : Map<ErrorKey, String> = {
      ErrorKey.INVALID_PRODUCTCODE -> "Found invalid ProductCode {ProductCode}"
  }

  public static final var SPECIAL_ACCOUNTS : List<String> = {"0752022"}
  public static final var PNI_NAME_CONTINUED : String = "(CONT.)"
  public static final var CP_MANUSCRIPT_PATTERN_CODE : String = "BOPManuscriptEndorsement_TDIC"
  public static final var CP_MANUSCRIPT_TYPE_PATTERN_CODE : String = "BOPManuscriptType_TDIC"
  public static final var CP_MANUSCRIPT_DESC_PATTERN_CODE : String = "BOPManuscriptDesc_TDIC"
  public static final var CP_MANUSCRIPT_TYPE_NAMEDINSURED_CONTINUED : String = "6"
  public static final var GL_MANUSCRIPT_PATTERN_CODE : String = "GLManuscriptEndor_TDIC"
  public static final var GL_MANUSCRIPT_TYPE_PATTERN_CODE : String = "GLManuscriptType_TDIC"
  public static final var GL_MANUSCRIPT_DESC_PATTERN_CODE : String = "GLManuscriptDesc_TDIC"
  public static final var GL_MANUSCRIPT_TYPE_NAMEDINSURED_CONTINUED : String = "2"

  public enum ErrorKey {
    INVALID_PRODUCTCODE()

    private construct() {
    }
  }
}