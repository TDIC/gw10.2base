package tdic.pc.common.batch.finance.reports.util

uses com.tdic.util.properties.PropertyUtil

class TDIC_FinanceReportUtil {

  @Returns("String object, external file path location for writing GL report files")
  public static function getFilePath() : String {
    return PropertyUtil.getInstance().getProperty(TDIC_FinanceReportConstants.FILE_PATH_PROP_FIELD)
  }

  @Returns("Date object that represents Batch period end date")
  public static function getBatchRunPeriodEndDate(startDate : Date) : Date {
    return startDate.addMonths(1).addDays(-1)
  }

  @Returns("String object that represents formatted current date and time as per Lawson file name requirements.")
  public static function getFileNameSuffix() : String {
    return TDIC_FinanceReportConstants.FILE_SUFFIX_DATE_FORMAT.format(Calendar.getInstance().getTime())
  }

  @Param("month", "Month of Batch run period")
  @Param("year", "Year of Batch run period")
  @Returns("Date object that represents Batch period start date")
  public static function getBatchRunPeriodStartDate(month : int, year : int) : Date {
    var calendar = Calendar.getInstance()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month-1)
    calendar.set(Calendar.DATE, 1)
    return calendar.getTime().trimToMidnight()
  }
}