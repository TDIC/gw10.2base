/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package tdic.pc.integ.plugins.hpexstream.batch

uses com.tdic.plugins.hpexstream.core.util.TDIC_DocumentEventSender
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory

/**
 * US555
 * 01/23/2015 Shane Murphy
 *
 *  HP Exstream Composition process batch implementation for high-volume batch runs of asynchronous document creation that are to be scheduled on periodic or manual basis.
 *
 *  Throughout a day, composition payload data are being generated and stored in an intermediary staging area in the integration database
 *  by the Exstream Transport plugin (see {@link "tdic.plugin.hpexstream.core.messaging.TDIC_ExstreamTransport"}).
 *
 *  Once a doWork() method is triggered, it extracts payload data from this staging area and sends them in a SOAP request to HP Exstream Command Center to start a composition job.
 */
class TDIC_DocumentCreationBatch extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  construct() {
    super(BatchProcessType.TC_DOCUMENTCREATIONBATCH)
  }

  /**
   * US555
   * 01/23/2015 Shane Murphy
   *
   * Extracts payload data from this staging area and sends them in a SOAP request to HP Exstream Command Center to start a composition job.
   */
  override function doWork(): void {
    _logger.debug("TDIC_DocumentCreationBatch#doWork() - Entering")
    TDIC_DocumentEventSender.sendMessage()
    _logger.debug("TDIC_DocumentCreationBatch#doWork() - Exiting")
  }
}

