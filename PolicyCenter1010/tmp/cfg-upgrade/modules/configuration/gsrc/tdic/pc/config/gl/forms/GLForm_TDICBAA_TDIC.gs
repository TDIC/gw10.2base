package tdic.pc.config.gl.forms

uses gw.api.database.DBFunction
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.database.Relop
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses typekey.Job


/**
 * Description :
 * Create User: ChitraK
 * Create Date: 4/6/2020
 * Create Time: 7:26 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_TDICBAA_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if ((context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" || context.Period.Offering.CodeIdentifier == "PLOccurence_TDIC") &&
        (context.Period.Job.Subtype == Job.TC_SUBMISSION)) {
      if (context.Period.Status == PolicyPeriodStatus.TC_BOUND) {
        return true
      }
    }
      if ((context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" || context.Period.Offering.CodeIdentifier == "PLOccurence_TDIC") &&
          (context.Period.Job.Subtype == Job.TC_RENEWAL && context.Period.isDIMSPolicy_TDIC) && (context.Period.Job.JobNumber.startsWithIgnoreCase("MIG"))) {
        return true
      }
      return false
    }
  }
