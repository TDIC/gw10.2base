package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class GLForm_TDIC1166AS_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLCyberLiab_TDIC"){
      if(context.Period.Job typeis Cancellation){
        return (context.Period.Cancellation.CancelReasonCode == ReasonCode.TC_NONPAYMENT or
            (context.Period.Cancellation.CancelReasonCode == ReasonCode.TC_NOTTAKEN))
      }
    }
    return false
  }
}