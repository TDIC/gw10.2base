package tdic.pc.integ.plugins.account
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.system.database.SequenceUtil
uses gw.plugin.account.impl.AccountPlugin
uses org.slf4j.LoggerFactory

class TDIC_AccountPlugin extends AccountPlugin {
  private static var LOGGER = LoggerFactory.getLogger("TDIC_INTEGRATION")

  private static final var CLASS_NAME = TDIC_AccountPlugin.Type.Name

  private final var _propUtil = PropertyUtil.getInstance()
  private final var _accountSequence = Long.parseLong(_propUtil.getProperty("new.account.sequence"))
  private final var _accountSequenceKey = _propUtil.getProperty("account.sequence.key") as String




  override function generateAccountNumber(account: Account): String {
    var methodName = "generateAccountNumber"
    LOGGER.info(CLASS_NAME + ": " + methodName + ": " + "Account number generation started")
    var accountNumber: String
    try {
      accountNumber = SequenceUtil.next(_accountSequence, _accountSequenceKey) as String
      while(Query.make(Account).compare(Account#AccountNumber, Relop.Equals, accountNumber).select().first() != null) {
        accountNumber = generateAccountNumber(account)
      }
    } catch(e:Exception){
      LOGGER.error(CLASS_NAME + ": " + methodName + ": " + "Error generating Account number - " + e.StackTraceAsString)
      throw(e)
    }
    LOGGER.info(CLASS_NAME + ": " + methodName + ": " + "Account number generation successful - " + accountNumber)
    return accountNumber

  }

}