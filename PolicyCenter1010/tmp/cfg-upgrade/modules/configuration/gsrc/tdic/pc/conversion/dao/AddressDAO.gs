package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.AddressDTO
uses tdic.pc.conversion.query.AccountConversionBatchQueries

uses java.sql.Connection

class AddressDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<AddressDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"

  var _params : Object[]
  var _batchType : String

  construct(params : Object[]) {
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct(params : Object[], batchType : String) {
    _params = params
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : ArrayList<AddressDTO> {
    _logger.info("Inside AddressDAO.retrieveAllUnprocessed() ")
    var handler = new BeanListHandler(AddressDTO)
    var rows = runQuery(AccountConversionBatchQueries.ADDRESS_DETAILS_SELECT_QUERY, _params, handler, _logger) as ArrayList<AddressDTO>
    if (rows.Empty) {
      return rows
    }
    rows.each(\addressDTO -> {
      if (addressDTO.AddressType == "mailing") {
        addressDTO.AddressType = "mailing_tdic"
      }
    })
    return rows
  }

  override function update(aTransientObject : AddressDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      var params = new Object[2]
      params[0] = ""
      params[1] = ""
      updatedRows = runUpdate(connection, "", params, _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(aPersistentObject : AddressDTO) : int {
    return 0
  }

  override function persistAll(persistentObjects : List<AddressDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : AddressDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<AddressDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<AddressDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<AddressDTO>) {
  }

}