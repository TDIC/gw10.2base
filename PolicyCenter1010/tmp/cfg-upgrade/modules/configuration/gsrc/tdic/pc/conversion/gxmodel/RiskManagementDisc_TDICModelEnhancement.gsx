package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator

enhancement RiskManagementDisc_TDICModelEnhancement : tdic.pc.conversion.gxmodel.riskmanagementdisc_tdicmodel.types.complex.RiskManagementDisc_TDIC {

  function populateRiskManagementDiscount(line : GLLine) {
    var riskMgmt = new RiskManagementDisc_TDIC(line.Branch)
    SimpleValuePopulator.populate(this, riskMgmt)
    line.addToRiskManagementDisc_TDIC(riskMgmt)
  }

}
