package tdic.pc.conversion.dto


class GLRiskManagementDTO {

  var name : String as Name
  var attended : String as Attended
  var lpsDate : String as LPSDate
  var locationCode : String as LocationCode
  var startDate : String as StartDate
  var endDate : String as EndDate

}