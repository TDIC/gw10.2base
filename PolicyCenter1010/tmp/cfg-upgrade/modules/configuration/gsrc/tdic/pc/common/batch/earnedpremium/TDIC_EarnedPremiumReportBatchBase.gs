package tdic.pc.common.batch.earnedpremium

uses com.tdic.util.properties.PropertyUtil
uses gw.api.system.server.ServerUtil
uses gw.pl.exception.GWConfigurationException
uses gw.pl.logging.LoggerCategory
uses gw.processes.BatchProcessBase
uses org.slf4j.Logger
uses gw.datatype.DataTypes
uses java.lang.Exception
uses java.lang.StringBuffer
uses java.text.DecimalFormat
uses java.util.Calendar
uses java.util.Date
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

abstract class TDIC_EarnedPremiumReportBatchBase extends BatchProcessBase{

  protected static var _logger : Logger = LoggerFactory.getLogger("TDIC_BATCH_EPREPORT")
  private var _batchRunPeriodStartDate : Date as readonly PeriodStartDate
  private var _batchRunPeriodEndDate : Date as readonly PeriodEndDate
  private var _emailTo : String as readonly EmailTo
  private var _emailRecipient : String as readonly EmailRecipient
  private static final var EMAIL_TO_PROP_FIELD = "PCEPReportNotificationEmail"
  private static final var EMAIL_RECIPIENT = "PCInfraIssueNotificationEmail"
  protected static final var TERMINATE_SUB : String = "Terminate::${gw.api.system.server.ServerUtil.ServerId}-PC EarnedPremiumReport Batch Job Terminated"
  protected static final var DECIMAL_FORMAT: DecimalFormat = new DecimalFormat("0.000000")
  private var RateCodeMaxSize = DataTypes.get(EarnedPremiumReport_TDIC.Type.TypeInfo.getProperty("RateCode")).asPersistentDataType().Length

  private static var CLASS_NAME = "TDIC_EarnedPremiumReportBatchBase"

  construct(args : Object[]){
    super(BatchProcessType.TC_EARNEDPREMIUMREPORT_TDIC)
    initialize(args)
  }

  /**
   *
   * 12/28/2015 Kesava Tavva
   *
   * This is override function to handle termination request during batch process
   */
  @Returns("Returns Boolean status of termination request")
  override function requestTermination() : boolean {
    super.requestTermination()
    return Boolean.TRUE
  }

  /**
   *
   * 12/28/2015 Kesava Tavva
   *
   * This function initializes start and end periods to process Earned Premiums for a given period
   */
  @Param("args","Argugments received from batch command")
  private function initialize(args : Object[]){
    var logPrefix = "${CLASS_NAME}#initialize(args)"
    if(args == null || args?.Count == 0){
      if(ScriptParameters.TDIC_UseEarnedPremiumReportPeriodStartDate){
        _logger.info("${logPrefix}- value: ${PropertyUtil.getInstance().getProperty("TDIC_EarnedPremiumReportPeriodStartDate")}")
        try{
          var _EarnedPremiumReportPeriodStartDate = Coercions.makeDateFrom(PropertyUtil.getInstance().getProperty("TDIC_EarnedPremiumReportPeriodStartDate"))
          _batchRunPeriodStartDate = getBatchRunPeriodStartDate(_EarnedPremiumReportPeriodStartDate.MonthOfYear, _EarnedPremiumReportPeriodStartDate.YearOfDate)
        }catch(e : Exception){
          _logger.error("Incorrect value(${PropertyUtil.getInstance().getProperty("TDIC_EarnedPremiumReportPeriodStartDate")}) set for TDIC_EarnedPremiumReportPeriodStartDate. Enter valid date(yyyy-MM-dd) and rerun batch process.")
          throw new Exception("Incorrect value(${PropertyUtil.getInstance().getProperty("TDIC_EarnedPremiumReportPeriodStartDate")}) set for TDIC_EarnedPremiumReportPeriodStartDate. Enter valid date(yyyy-MM-dd) and rerun batch process.")
        }
      }
      else
        _batchRunPeriodStartDate = getBatchRunPeriodStartDate(Calendar.getInstance().CalendarMonth, Calendar.getInstance().CalendarYear)
    } else {
      validateArguments(args)
      _batchRunPeriodStartDate = getBatchRunPeriodStartDate(Coercions.makePIntFrom(args[0]), Coercions.makePIntFrom(args[1]))
    }
    _batchRunPeriodEndDate = getBatchRunPeriodEndDate(_batchRunPeriodStartDate)
  }

  /**
   *
   * 12/28/2015 Kesava Tavva
   *
   * This function validates arguments received as part of batch command. Expected values are
   *  args[0] - format: <mm>; valid values: 01,02...12
   *  args[1] - format: <yyyy>; 4 digit year
   */
  @Param("args","Argugments received from batch command")
  @Throws(Exception,"Exception, details about invalid arguments received")
  private function validateArguments(args : Object[]) {
    var logPrefix = "${CLASS_NAME}#validateArguments(args)"
    if(args.Count == 2){
      try{
        var month = args[0].toString().toInt()
        var year = args[1].toString().toInt()
        _logger.info("${logPrefix}- Arguments received are ${month}, ${year}")
        return
      }catch(e : Exception){
        _logger.error("Invalid arguments (${args[0]}, ${args[1]}) received for batch run. Expected batch command arguments are <mm>(2 digit month. Valid values 01,02,..12) and <yyyy>(4 digit year).")
        throw new Exception("Invalid arguments (${args[0]}, ${args[1]}) received for batch run. Expected batch command arguments are <mm>(2 digit month. Valid values 01,02,..12) and <yyyy>(4 digit year).")
      }
    } else {
      _logger.error("Invalid number of arguments (${args.Count}) received for batch run. Expected batch command arguments are <mm>(valid values 01,02,..12) and <yyyy>(4 digit year).")
      throw new Exception("Invalid number of arguments (${args.Count}) received for batch run. Expected batch command arguments are <mm>(valid values 01,02,..12) and <yyyy>(4 digit year).")
    }
  }

  /**
   *
   * 12/28/2015 Kesava Tavva
   *
   * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    _logger.trace("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(EMAIL_TO_PROP_FIELD)
    if (_emailTo == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '${EMAIL_TO_PROP_FIELD}' from integration database.")
    }
    _emailRecipient = PropertyUtil.getInstance().getProperty(EMAIL_RECIPIENT)
    if (_emailRecipient == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '${EMAIL_RECIPIENT}' from integration database.")
    }
    _logger.trace("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  /**
   *
   * 12/28/2015 Kesava Tavva
   *
   * Builds Email Subject for either batch failure or success
   */
  @Param("flag", "Success or Failure flag")
  @Returns("String, Email subject message")
  protected function getEmailSubject(flag : Boolean) : String {
    var subject = new StringBuffer()
    if(flag){
      subject.append("SUCCESS::${gw.api.system.server.ServerUtil.ServerId}-PC EarnedPremiumReport Batch Job Completed for the period: ${PeriodStartDate} to ${PeriodEndDate}")
    }else{
      subject.append("FAILURE::${gw.api.system.server.ServerUtil.ServerId}-PC EarnedPremiumReport Batch Job Failed for the period: ${PeriodStartDate} to ${PeriodEndDate}")
    }
    return subject.toString()
  }

  /**
   *
   * 12/29/2015 Kesava Tavva
   *
   * This function returns Batch period start date for generating GL reports
   * based on arguments received in batch command.
   *
   */
  @Param("month", "Month of Batch run period")
  @Param("year", "Year of Batch run period")
  @Returns("Date object that represents Batch period start date")
  private function getBatchRunPeriodStartDate(month : int, year : int) : Date {
    var calendar = Calendar.getInstance()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month-1)
    calendar.set(Calendar.DATE, 1)
    return calendar.getTime().trimToMidnight()
  }

  /**
   *
   * 12/29/2015 Kesava Tavva
   *
   * This function returns batch period end date for generating GL reports
   * based on batch period start date.
   *
   */
  @Returns("Date object that represents Batch period end date")
  private function getBatchRunPeriodEndDate(startDate : Date) : Date {
    return startDate.addMonths(1).addDays(-1)
  }

  /**
   *
   * 01/05/2016 Kesava Tavva
   *
   * This function returns batch period start date for each batch run
   */
  @Returns("Returns Batch period start date")
  protected function getBatchPeriodStartDate() : Date {
    return PeriodStartDate.addMinutes(1)
  }

  /**
   *
   * 01/05/2016 Kesava Tavva
   *
   * This function returns batch period end date for each batch run
   */
  @Returns("Returns Batch period end date")
  protected function getBatchPeriodEndDate(psd: Date): Date {
    return psd.addMonths(1).addMinutes(- 1)
  }

  /**
   * JIRA GW-989
   * 02/04/2016 Kesava Tavva
   *
   * returns the ratecode for this object but first checks that the value will fit into
   * the underlying database field. If it is too long it is truncated before being stored.
   */
  protected function safeRateCode(rateCode : String): String {
    if (rateCode.length > RateCodeMaxSize) {
      rateCode = rateCode.substring(0, RateCodeMaxSize)
    }
    return rateCode
  }
}