package tdic.pc.conversion.dto

class NoteDTO {

  var _policyID : String as PolicyID
  var _authorID : String as Author_ID
  var _topic : String as Topic
  var _subject : String as Subject
  var _securityType : String as SecurityType
  var _body : String as Body
  var _authoringDate : String as AuthoringDate
  var _publicID : String as PublicID

}