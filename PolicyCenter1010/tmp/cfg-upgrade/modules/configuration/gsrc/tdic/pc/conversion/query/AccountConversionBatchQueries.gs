package tdic.pc.conversion.query

class AccountConversionBatchQueries {

  public static final var param : String = "?,"
  public var paramRepeat : String //= param.repeat(ConversionConstants.MAX_ACCOUNTS_PAYLOAD - 1)

  function getParamRepeat() : String {
    final var paramString : String = paramRepeat.substring(0, paramRepeat.length() - 1)
    return paramString
  }

  function setParamRepeat(repeatValue : int) {
    paramRepeat = param.repeat(repeatValue)
  }

  public static final var ACCOUNT_DETAILS_SELECT_QUERY : String = "SELECT "
      + "ACCOUNTORGTYPE,"
      + "BUSOPSDESC,  "
      + "YEARBUSINESSSTARTED,  "
      + "PUBLICID "
      + "FROM STG.PCST_ACCOUNT "
      + "WHERE PUBLICID = ? "
  //+ "ROWNUM < " + ConversionConstants.MAX_ACCOUNTS_PAYLOAD

  public static final var ACCOUNT_CONTACT_SELECT_QUERY : String = "SELECT "
      + " CONTACTID, "
      + " ACCOUNTID, "
      + " PUBLICID  "
      + " FROM STG.PCST_ACCOUNTCONTACT "
      + " WHERE ACCOUNTID = ?"
      + " ORDER BY LOADID DESC"

  public static final var INSERT_CONVERSION_ERROR_QUERY : String = "INSERT INTO STG.DSM_GW_ERROR ( "
      + " POLICYNUMBER,  "
      + " ACCOUNTID,  "
      + " OFFERING_LOB,  "
      + " BATCHID,  "
      + " BATCHTYPE,  "
      + " ERRORMSG_BUSINESS,  "
      + " ERRORMSG_TECHNICAL )  "
      + " VALUES  (?,?,?,?,?,?,?) "

  public static final var CONTACT_DETAILS_SELECT_QUERY : String = " SELECT "
      + "NAME, "
      + "FIRSTNAME, "
      + "LASTNAME, "
      + "SUBTYPE, "
      + "PRIMARYADDRESSID, "
      + "DATEOFBIRTH, "
      + "PRIMARYPHONE, "
      + "HOMEPHONE, "
      + "WORKPHONE, "
      + "CELLPHONE, "
      + "FAXPHONE, "
      + "EMAILADDRESS1, "
      + "TAXID, "
      + "SSNBR, "
      + "COMPONENT_TDIC, "
      + "LICENSENUMBER, "
      + "LICENSESTATE, "
      + "CREDENTIAL_TDIC, "
      + "ADANUMBEROFFICIALID_TDIC, "
      + "PUBLICID "
      + "FROM STG.PCST_CONTACT WHERE PUBLICID=? "
      + "ORDER BY LOADID DESC"

  public static final var ADDRESS_DETAILS_SELECT_QUERY : String = " SELECT "
      + "LOCATIONCODE, "
      + "LOCATIONNAME, "
      + "COUNTRY, "
      + "ADDRESSLINE1, "
      + "ADDRESSLINE2, "
      + "ADDRESSLINE3, "
      + "CITY, "
      + "COUNTY, "
      + "STATE, "
      + "POSTALCODE, "
      + "ADDRESSTYPE, "
      + "DESCRIPTION, "
      + "PUBLICID "
      + "FROM STG.PCST_ADDRESS WHERE PUBLICID=?  "
      + "ORDER BY LOADID DESC"

}