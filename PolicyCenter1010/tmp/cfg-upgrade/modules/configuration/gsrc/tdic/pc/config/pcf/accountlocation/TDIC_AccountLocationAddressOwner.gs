package tdic.pc.config.pcf.accountlocation

uses tdic.pc.config.pcf.contacts.name.TDIC_AddressInputSetAddressOwner
/**
 * Created with IntelliJ IDEA.
 * User: ssheridan
 * Date: 05/09/14
 * Time: 13:27
 * This class replaces the OOTB AccountLocationAddressOwner class for TDIC modifications of Account Location address fields.
 */
class TDIC_AccountLocationAddressOwner extends TDIC_AddressInputSetAddressOwner{
  construct(accountLocation: AccountLocation) {
    super(accountLocation, accountLocation.NonSpecific, accountLocation.New)
  }
}