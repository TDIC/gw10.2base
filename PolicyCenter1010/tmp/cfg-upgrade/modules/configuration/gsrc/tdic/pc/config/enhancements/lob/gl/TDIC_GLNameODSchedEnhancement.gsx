package tdic.pc.config.enhancements.lob.gl

enhancement TDIC_GLNameODSchedEnhancement : GLNameODSched_TDIC {

  property get EffectiveDate_TDIC() : Date {
    if (this.LTEffectiveDate.afterOrEqual(this.Branch.PeriodStart)) {
      return this.LTEffectiveDate
    }
    return this.Branch?.PeriodStart
  }

  property get EffDate_TDIC() : Date {
    if (this.LTEffectiveDate.afterOrEqual(this.Branch.PeriodStart)) {
      return this.LTEffectiveDate
    }
    return this.Branch?.PeriodStart
  }

}
