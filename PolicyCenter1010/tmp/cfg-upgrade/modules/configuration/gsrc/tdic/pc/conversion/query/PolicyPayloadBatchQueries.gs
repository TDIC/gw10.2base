package tdic.pc.conversion.query

class PolicyPayloadBatchQueries {

  public static final var param : String = "?,"

  public static final var INSERT_PAYLOAD_QUERY : String = "INSERT INTO STG.DSM_GW_PAYLOAD ( "
      + " POLICYNUMBER, "
      + " GWPOLICYNUMBER, "
      + " ACCT_KEY, "
      + " PAYLOAD_XML, "
      + " BATCH_ID, "
      + " BATCH_TYPE, "
      + " OFFERING_LOB, "
      + " COUNTER, "
      + " CREATED_DATE, "
      + " CREATED_BY ) "
      + " VALUES (?,?,?,?,?,?,?,?,?,?)"

  public static final var INSERT_DSM_GW_LOAD_STATUS_QUERY : String = "INSERT INTO STG.DSM_LOAD_STATUS ( "
      + " ACCOUNTNUMBER, "
      + " BATCHTYPE,  "
      + " POLICYNUMBER, "
      + " BATCHID, "
      + " OFFERING_LOB, "
      + " COUNTER) "
      + " VALUES (?,?,?,?,?,?)"

  public static final var UPDATE_DSM_GW_LOAD_STATUS_QUERY : String = "UPDATE STG.DSM_LOAD_STATUS SET "
      + " BATCHTYPE = ?,  "
      + " BATCHID = ? "
      + " WHERE ACCOUNTNUMBER = ?"


}