package tdic.pc.common.batch.enrole


uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.pc.common.batch.enrole.exception.EnroleException

/**
 * EnroleOutboundBatch_TDIC - File based integration to send bounded policy info to enrole system.
 * Fetch unprocessed policy records from table: EnrolePolicyDetails and write to enrole policy outbound file.
 * Updates EnrolePolicyDetails processed to true once the policy record is sent to enrole.
 * Created by RambabuN on 10/14/2019.
 */
class EnroleOutboundBatch_TDIC extends BatchProcessBase {

  static final var _logger = LoggerFactory.getLogger("TDIC_Enrole")

  /**
   * Key for looking up the failure notification email addresses from the integration database
   * (value: InfraIssueNotificationEmail).
   */
  static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "PCInfraIssueNotificationEmail"
  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  var _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
  /**
   * Notification email's subject for EnroleOutbound batch failures.
   */
  static final var TERMINATE_SUB =
      "Terminate::${gw.api.system.server.ServerUtil.ServerId}-enrole Outbound batch Terminated"
  /**
   * Notification email's subject for EnroleOutbound batch failures.
   */
  static final var EMAIL_SUBJECT_FAILURE =
      "enrole Outbound Batch Job Failure on server ${gw.api.system.server.ServerUtil.ServerId}"


  construct() {
    super(BatchProcessType.TC_ENROLEOUTBOUNDBATCH_TDIC)
  }

  /**
   * dowork() is responsibel to write Enrole Policy outbound file.
   */
  protected override function doWork() {
    _logger.trace("EnroleOutboundBatch_TDIC#doWork() - Entering.")

    try {

      if (TerminateRequested) {
        _logger.warn("EnroleOutboundBatch_TDIC#doWork() - " +
            "Terminate requested during doWork() method before writing outbound file().")
        EmailUtil.sendEmail(_failureNotificationEmail, TERMINATE_SUB, "enrole policy batch is terminated.")
        return
      }

      EnroleHelper.writeEnrolePolicyDetails()
      incrementOperationsCompleted()

    } catch (e : EnroleException) {
      _logger.error("EnroleOutboundBatch_TDIC - Exception While inserting enrole policy record !!!", e.toString())
      incrementOperationsFailed()
      EmailUtil.sendEmail(_failureNotificationEmail,
          EMAIL_SUBJECT_FAILURE, "Error writting oubound file to enrole: " + e.Message)
      throw e
    }

    _logger.debug("EnroleOutboundBatch_TDIC#doWork() - Exiting")
  }

  /**
   * To terminate batch process.
   * @return boolean
   */
  override function requestTermination() : boolean {
    // This will set the Termination request flag
    super.requestTermination()
    _logger.info("EnroleOutboundBatch_TDIC#doWork(): requestTermination Terminated.")
    // It will return true to signal that the attempt is made to stop the process in doWork method.
    return true
  }
}