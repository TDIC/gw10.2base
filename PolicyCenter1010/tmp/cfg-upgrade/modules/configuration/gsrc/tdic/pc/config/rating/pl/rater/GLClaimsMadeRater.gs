package tdic.pc.config.rating.pl.rater

uses gw.lob.gl.rating.GLAddnlInsdSchedCostData_TDIC
uses gw.lob.gl.rating.GLCovExposureCostData
uses gw.lob.gl.rating.GLHistoricalCostData_TDIC
uses gw.lob.gl.rating.GLMinPremCostData_TDIC
uses gw.lob.gl.rating.GLStateCostData
uses tdic.pc.config.rating.RatingConstants
uses tdic.pc.config.rating.pl.GLRatingEngine
uses tdic.pc.config.rating.pl.GLRatingUtil

class GLClaimsMadeRater implements GLRater {

  private var _ratingEngine : GLRatingEngine as RatingEngine
  private var _line : GLLine as Line
  private var _branch : PolicyPeriod as Branch

  private construct() { /*do nothing*/ }

  construct(line : GLLine, ratingEngine : GLRatingEngine) {
    _ratingEngine = ratingEngine
    _line = line
    _branch = _ratingEngine.Branch
    initRatingInfo()
  }

  override function rate() {
    rateClaimsMade()
  }

  /**
   * create by: AnkitaG
   *
   * @description: Function to rate PL Claims Made Policies
   * @create time:  10/24/2019
   */
  private function rateClaimsMade() {
    rateProfessionalLiability()
    rateFixedExpense()
    rateHistoricalParameters()
    ratePolicyLevelCoverages()
  }

  /*
  * create by: AnkitaG, BidishS, IndramaniP
  * @description: Function to rate all Policy level coverages
  * @create time:  11/22/2019
  * @param glLine
  * @return: null
  */
  private function ratePolicyLevelCoverages() {
    rateAddnlInsuredEndorsement()
    rateRiskManagement()
    rateMultiLineDiscount()
    rateIRPMDiscount()
    rateNJPolicyCoverages()
    rateMinimumPremiumAdjustment()
    rateNewDentist()
    rateLineCoverages()
    rateServiceMemberDiscount()
    rateIGASurcharge()
  }

  /**
   * NewJersey specific coverages
   *
   * @param ratingInfo
   */
  private function rateNJPolicyCoverages() {
    if (Line.Branch.BaseState == TC_NJ) {
      rateNewToCompany()
      rateDeductible()
      rateWaiverConsent()
      rateNonMemberDiscount()
    }
  }

  /**
   * Rate the liability for portion for the current term, NJ portion will be calculated seperatetly
   *
   * @param ratingInfo
   */
  private function rateProfessionalLiability() {
    rateLiability()
    //NewJersey portion of Liability
    //North West Portion of liability
    var isNWNJState={Jurisdiction.TC_ID,Jurisdiction.TC_TN,Jurisdiction.TC_MT,
                     Jurisdiction.TC_OR,Jurisdiction.TC_WA,Jurisdiction.TC_NJ}.contains(Branch.BaseState)
    if (isNWNJState) {
      rateNWNJLiability()
    }
  }

  /**
   * Line Coverages
   *
   * @param ratingInfo
   */
  private function rateLineCoverages() {
    for (cov in Line.GLLineCoverages) {
      rateLineCoverage(cov)
    }
  }

  /**
   * @param _ratingInfo
   */
  private function rateFixedExpense() {
    if ({Jurisdiction.TC_CA, Jurisdiction.TC_IL}.contains(Branch.BaseState)) { // CA & IL only
      var exp = Line.Exposures.first()
      var cov = Line.GLDentistProfLiabCov_TDIC

      var discount = RatingEngine.RatingInfo.getCurrentPeriodDiscount()
      var cost = new GLCovExposureCostData(cov.SliceDate, RatingEngine.getNextSliceDate(cov.SliceDate),
          Branch.getBaseState(), cov.FixedId, exp.FixedId, false, GLCostType_TDIC.TC_FIXEDEXPENSE, null, null, discount)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(cov.GLLine)
      RatingEngine.execute(RatingConstants.plCMFixedExpenseRoutine, map, cost)
      RatingEngine.CostCache.put(GLCostType_TDIC.TC_FIXEDEXPENSE.Code, cost)
    }
  }

  /**
   * Rating Historical information from the RiskAnalysis,
   * historical parameters will be split and each split will be rated seperately
   *
   * @param ratingInfo
   */
  private function rateHistoricalParameters() {
    if(Branch.Job typeis Submission or Branch.Job typeis Renewal) {
      Line.GLSplitParameters_TDIC.each(\elt -> Line.removeFromGLSplitParameters_TDIC(elt))
    }
    var splitParameters = GLRatingUtil.getSplitParameters(Line)
    var cov = Line.GLDentistProfLiabCov_TDIC
    for (param in splitParameters) {
      RatingEngine.RatingInfo.populateRoutineParams(param)
      RatingEngine.RatingInfo.populateNewDoctorInfoForHistoricalTerms(param.EffDate.trimToMidnight(), param.ExpDate.trimToMidnight())
      var cost = new GLHistoricalCostData_TDIC(param.SliceDate, RatingEngine.getNextSliceDate(param.SliceDate), Branch.BaseState, cov.FixedId, param.FixedId, GLCostType_TDIC.TC_PROFESSIONALLIABILITY)
      cost.init(Line)
      var paramMap : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      RatingEngine.execute(RatingConstants.plClaimsMadeRoutine, paramMap, cost)
      RatingEngine.CostCache.put(param.DisplayName, cost)
    }
  }


  /**
   * @param ratingInfo
   */
  private function rateLiability() {
    var exp = Line.Exposures.first()
    var cov = Line.GLDentistProfLiabCov_TDIC
    var discount = RatingEngine.RatingInfo.getCurrentPeriodDiscount()
    var cost = new GLCovExposureCostData(cov.SliceDate, RatingEngine.getNextSliceDate(cov.SliceDate),
        Branch.getBaseState(), cov.FixedId, exp.FixedId, false, GLCostType_TDIC.TC_PROFESSIONALLIABILITY, null, null, discount)
    var map : Map<CalcRoutineParamName, Object> = {
        TC_POLICYLINE -> Line,
        TC_RATINGINFO -> RatingEngine.RatingInfo
    }
    cost.init(Line)
    RatingEngine.execute(RatingConstants.plClaimsMadeRoutine, map, cost)
    RatingEngine.CostCache.put(GLCostType_TDIC.TC_PROFESSIONALLIABILITY.Code, cost)
  }

  /**
   * Function to rate GL Portion of Current Period for NJ State
   *
   * @param ratingInfo
   */
  private function rateNWNJLiability() {
    var exp = Line.Exposures.first()
    var cov = Line.GLDentistProfLiabCov_TDIC
    var discount = RatingEngine.RatingInfo.getCurrentPeriodDiscount()
    var cost = new GLCovExposureCostData(cov.SliceDate, RatingEngine.getNextSliceDate(cov.SliceDate),
        Branch.getBaseState(), cov.FixedId, exp.FixedId, false, GLCostType_TDIC.TC_NJPROFESSIONALLIABILITY, null, null, discount)
    var map : Map<CalcRoutineParamName, Object> = {
        TC_POLICYLINE -> Line,
        TC_RATINGINFO -> RatingEngine.RatingInfo
    }
    cost.init(Line)
    RatingEngine.execute(RatingConstants.plCMNJCurrentPeriodRoutine, map, cost)
    RatingEngine.CostCache.put(GLCostType_TDIC.TC_NJPROFESSIONALLIABILITY.Code, cost)
  }

  private function rateDeductible() {
    if (Line.GLNJDeductibleCov_TDICExists) {
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_NJDEDUCTIBLE_TDIC, null, null)
      RatingEngine.RatingInfo.DeductibleBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plCMNJDeductibleRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_NJDEDUCTIBLE_TDIC.Code, cost)
    }
  }

  private function rateWaiverConsent() {
    if (Line.GLNJWaiverCov_TDICExists and Line.GLNJWaiverCov_TDIC.GLNJWaiverSelection_TDICTerm.OptionValue.Value == 1) {
      RatingEngine.RatingInfo.WaiverOfConsentBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_NJWAIVEROFCONSENT_TDIC, null, null)
      cost.init(Line)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      RatingEngine.execute(RatingConstants.plCMWaiverOfConsentRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_NJWAIVEROFCONSENT_TDIC.DisplayName, cost)
    }
  }

  private function rateNonMemberDiscount() {
    if (Line.GLNJNonMemberDiscount_TDICExists) {
      RatingEngine.RatingInfo.NonMemberSurchage = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_NJNONMEMBERSURCHARGE_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plCMNonMemberSurchargeRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_NJNONMEMBERSURCHARGE_TDIC.Code, cost)
    }
  }

  /*
   * create by: AnkitaG
   * @description: Please add description
   * @create time:  11/02/2019
   * @param null
   * @return:
   */

  private function rateIGASurcharge() {
    if (Line.Branch.BaseState == TC_NJ) { //Only for NJ for now
      RatingEngine.RatingInfo.IGASurchage = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
          GLStateCostType.TC_NJIGASURCHARGE_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
     /*
     * 02/26/2020 Britto S
     * Per BillingCenter requirement and downstream system impacts, changing to state specific code,
     * may need to revist and aggregate to one in upcoming relases, as it gets included in other states
     */
      if (Line.Branch.BaseState == Jurisdiction.TC_NJ) {
        cost.ChargePattern = ChargePattern.TC_NJIGASURCHARGE_TDIC
      } else {
        cost.ChargePattern = ChargePattern.TC_IGASURCHARGE_TDIC
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plCMIGASurchargeRoutine, map, cost)

    }
  }

  /*
  * create by: AnkitaG
  * @description: Function to rate coverages
  * @create time:  12/02/2019
  * @param line
  * @return: null
  */
  private function rateLineCoverage(cov : Coverage) {
    var routine : String
    var costType : GLStateCostType
    switch (typeof cov) {
      case GLSchoolServicesCov_TDIC:
        routine = RatingConstants.plCMSchoolServiceRoutine
        RatingEngine.RatingInfo.SchoolServiceEventCount = Line.GLSchoolServSched_TDIC?.where(\elt -> elt.CanrecComp and RatingEngine.checkIfDateInRangeIncluded(elt.EventEndDate, Line.EffectiveDate, Line.ExpirationDate)).Count
        costType = GLStateCostType.TC_SCHOOLSERVICES_TDIC
        break
      case GLSpecialEventCov_TDIC:
        routine = RatingConstants.plCMSpecialEvenRoutine
        RatingEngine.RatingInfo.SpecialEventCount = Line.GLSpecialEventSched_TDIC?.where(\elt -> !(elt.isOnsite()) and RatingEngine.checkIfDateInRangeIncluded(elt.EffectiveDate, Line.EffectiveDate, Line.ExpirationDate)).Count
        costType = GLStateCostType.TC_SPECIALEVENT_TDIC
        break
      case GLIDTheftREcoveryCov_TDIC:
        routine = RatingConstants.plCMIDRRoutine
        RatingEngine.RatingInfo.IDRType = cov.GLTRTypeTDICTerm.Value
        costType = GLStateCostType.TC_IDENTITYTHEFTREC_TDIC
        break
      case GLDentalEmpPracLiabCov_TDIC:
        routine = RatingConstants.plCMEPLIRoutine
        RatingEngine.RatingInfo.EPLIMinNumOfEmployees = (cov.GLDEPLEmpNumber_TDICTerm.Value) as int
        RatingEngine.RatingInfo.EPLIClaimsInLast2Years = cov.GLDEPLClaimslasttwoyears_TDICTerm.Value
        RatingEngine.RatingInfo.EPLIRMRequirements = cov.GLDEPLRishMgtReq_TDICTerm.Value
        RatingEngine.RatingInfo.EPLILimits = cov.GLDEPLLimit_TDICTerm.Value
        costType = GLStateCostType.TC_EPLI_TDIC
        break
    }
    if (routine != null) {
      var cost = new GLStateCostData(cov.SliceDate, RatingEngine.getNextSliceDate(cov.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, costType, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      if (cost.StateCostType == GLStateCostType.TC_SPECIALEVENT_TDIC or cost.StateCostType == GLStateCostType.TC_SCHOOLSERVICES_TDIC) {
        cost.ProrationMethod = TC_FLAT
      }
      RatingEngine.execute(routine, map, cost)
      RatingEngine.CostCache.put(cost.StateCostType.Code, cost)
    }
  }

  private function rateAddnlInsuredEndorsement() {
    if (Line.GLAdditionalInsuredCov_TDICExists and Line.GLAdditionInsdSched_TDIC.Count > 0) {
      RatingEngine.RatingInfo.PLCMAddnlInsuredBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var additionlaInsuredSchedItems = Line.GLAdditionInsdSched_TDIC
      for (addInsured in additionlaInsuredSchedItems) {
        if (addInsured.LTExpirationDate == null or addInsured.LTExpirationDate.compareIgnoreTime(Branch.EditEffectiveDate) > 0) {
          var addnlInsuredExpDate = addInsured.LTExpirationDate != null ? addInsured.LTExpirationDate : RatingEngine.getNextSliceDate(Line.SliceDate)
          var cost = new GLAddnlInsdSchedCostData_TDIC(Line.SliceDate, addnlInsuredExpDate,
              Line.Branch.BaseState, addInsured, GLAddnlInsuredCostType_TDIC.TC_ADDITIONALINSURED)
          cost.init(Line)
          var map : Map<CalcRoutineParamName, Object> = {
              TC_POLICYLINE -> Line,
              TC_RATINGINFO -> RatingEngine.RatingInfo
          }
          RatingEngine.execute(RatingConstants.plCMAddnlInsuredEndorsementRoutine, map, cost)
          RatingEngine.CostCache.put(addInsured.FixedId.toString(), cost)
        }
      }
    }
  }

  private function rateRiskManagement() {
    if (Line.GLRiskMgmtDiscount_TDICExists) {
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLStateCostType.TC_RISKMANAGEMENT_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.RatingInfo.PLCMRiskMgmtBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      RatingEngine.execute(RatingConstants.plCMRiskManagementRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_RISKMANAGEMENT_TDIC.Code, cost)
    }
  }

  /**
   * @param ratingInfo
   */
  private function rateMultiLineDiscount() {
    if (Line.Branch.MultiLineDiscount_TDIC != null and
        Line.Branch.MultiLineDiscount_TDIC != MultiLineDiscount_TDIC.TC_N) {
      RatingEngine.RatingInfo.PLCMMultiLineDiscountBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLStateCostType.TC_MULTILINEDISCOUNT_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plCMMultiLineDiscountRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_MULTILINEDISCOUNT_TDIC.Code, cost)
    }
  }

  /**
   * @param ratingInfo
   */
  private function rateIRPMDiscount() {
    if (Line.GLIRPMDiscount_TDICExists
        and (Line.Branch.BaseState == TC_NJ or Line.Branch.BaseState == TC_PA)) {
      RatingEngine.RatingInfo.IRPMDiscountBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      RatingEngine.RatingInfo.IRPMFactor = Line.GLModifiers?.firstWhere(\mod -> mod.Pattern.CodeIdentifier == "GLIRPMModifier_TDIC")?.RateFactorsSum
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLStateCostType.TC_IRPMDISCOUNT_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plCMIRPMRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_IRPMDISCOUNT_TDIC.Code, cost)
    }
  }

  /*
   * create by: AnkitaG
   * @description: Method to rate New To Company
   * @create time:  11/21/2019
    * @param null
   * @return:
   */

  private function rateNewToCompany() {
    if (Line.GLNewToCompanyDiscount_TDICExists) {
      RatingEngine.RatingInfo.PLNewToCompanyBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLStateCostType.TC_NEWTOCOMPANY_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plCMNewToCompanyRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_NEWTOCOMPANY_TDIC.Code, cost)
    }
  }

  /*
 * create by: AnkitaG
 * @description: Method  to rate new Dentist Dsicount
 * @create time:  11/21/2019
  * @param null
 * @return:
 */
  private function rateNewDentist() {
    if (Line.GLNewDentistDiscount_TDICExists) {
      RatingEngine.RatingInfo.PLNewDentistBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLStateCostType.TC_NEWDENTIST_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      var routine = RatingConstants.plCMNewDentistRoutine
      RatingEngine.execute(routine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_NEWDENTIST_TDIC.Code, cost)
    }
  }
  /*
   * create by: AnkitaG
   * @description: Please add description
   * @create time:  11/22/2019
    * @param ratingInfo
   * @return:
   */

  private function rateServiceMemberDiscount() {
    if (Line.GLServiceMembersCRADiscount_TDICExists) {
      RatingEngine.RatingInfo.ServiceMemberDiscountBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLStateCostType.TC_SERVICEMEMBERDISC_TDIC, null, null)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plCMServiceMemberRoutine, map, cost)
      RatingEngine.CostCache.put(GLStateCostType.TC_SERVICEMEMBERDISC_TDIC.Code, cost)
    }
  }

  /**
   *
   * @return
   */
  private function initRatingInfo() {
    RatingEngine.RatingInfo.populateNewDoctorInfoForCurrentTerm(Line.EffectiveDate.trimToMidnight(), Line.ExpirationDate.trimToMidnight())
  }

  /**
   * This methos checks if the total calculated premium is less than 100, if so then it sets the Minimum Premium as the
   * difference between the 100 and calculated premium
   */
  private function rateMinimumPremiumAdjustment() {
    if (Line.GLExposuresWM.hasMatch(\glExposure -> (glExposure.ClassCode_TDIC == ClassCode_TDIC.TC_01 or glExposure.ClassCode_TDIC == ClassCode_TDIC.TC_02))) {
      RatingEngine.RatingInfo.MinimumPremiumAdjustmentBasis = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
      var cost = new GLMinPremCostData_TDIC(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency,
          RatingEngine.RateCache, Line.Branch.BaseState, GLCostType_TDIC.TC_GLMINPREMIUMADJ)
      var map : Map<CalcRoutineParamName, Object> = {
          TC_POLICYLINE -> Line,
          TC_RATINGINFO -> RatingEngine.RatingInfo
      }
      cost.init(Line)
      RatingEngine.execute(RatingConstants.plCMMinimumPremiumAdjustmentRoutine, map, cost)
      RatingEngine.CostCache.put(GLCostType_TDIC.TC_GLMINPREMIUMADJ.Code, cost)
    }
  }

}