package tdic.pc.common.batch.finance.reports.prmlines

uses tdic.pc.common.batch.finance.reports.premiums.TDIC_FinanceReportPremiumObject
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportConstants

uses java.math.BigDecimal

class TDIC_FinanceEarnedPremiumLines extends TDIC_FinancePremiumLinesBase {
  private static final var RUN_GROUP : String = formatString("GW-LGL-EPM")
  private static final var CLASS_NAME = TDIC_FinanceEarnedPremiumLines.Type.RelativeName

  construct(ped : Date) {
    super(ped)
  }

  override function generatePremiumLines(reportPremiums: List<TDIC_FinanceReportPremiumObject>) : List<String> {
    var logPrefix = "${CLASS_NAME}#generatePremiumLines(map)"
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - Entering")
    var premiumLines = new ArrayList<String>()

    if (!reportPremiums.HasElements)
      return null

    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - Premium lines build started")

    var productCodes = reportPremiums*.ProductCode?.toSet()
    var jurisdictions:Set<String>
    var reportPremiumLineAmounts:List<TDIC_FinanceReportPremiumObject>
    var refSeqNum = 0
    var premiumLineAccounts: List<TDIC_FinancePremiumLineAccountObject>
    for(productCode in productCodes){
      jurisdictions = (reportPremiums.where(\elt -> elt.ProductCode == productCode))*.Jurisdiction?.toSet()
      for(jurisdiction in jurisdictions){
        refSeqNum = 0
        reportPremiumLineAmounts = reportPremiums.where(\elt -> elt.ProductCode == productCode and elt.Jurisdiction == jurisdiction)
        for(premiumLineAmount in reportPremiumLineAmounts){
          TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - ${premiumLineAmount}")
          if(premiumLineAmount.Amount !=0 ){
            premiumLineAccounts = getPremiumLineAccounts(premiumLineAmount)
            for(lineAccount in premiumLineAccounts){
              refSeqNum = refSeqNum+1
              premiumLines.add(buildPremiumLine(premiumLineAmount, lineAccount, refSeqNum))
            }
          }
        }
      }
    }

    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - All Premium lines build completed")

    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - Exiting")
    return premiumLines
  }

  private function buildPremiumLine(prmObj : TDIC_FinanceReportPremiumObject, prmLineAccount: TDIC_FinancePremiumLineAccountObject, refSeqNum : int) : String {
    var prmLine = new StringBuilder()
    prmLine.append(getRunGroup())
        .append(formatInteger(getSeqNumber(), TDIC_FinancePremiumLineConstants.INT_PATTERN_6))
        .append(COMPANY).append(formatString(getOldCompany(prmObj, prmLineAccount)))
        .append(formatString(prmLineAccount.AccountNumber))
        .append(SOURCE_CODE).append(getSystemDate())
        .append(getReference(prmObj.Jurisdiction, refSeqNum))
        .append(formatString(prmLineAccount.Description))
        .append(BLANK_STRING)
        .append(BLANK_STRING)
        .append(formatBigDecimalWithDecimals(((TDIC_FinancePremiumLineConstants.NEG_ADJ_Y == prmLineAccount.NegativeAdjustment) ? prmObj.Amount*-1 : prmObj.Amount)))
        .append(PrmSubLine1)
        .append(getPostingDate()).append(PrmSubLine2)
        .append(BLANK_STRING)
        .append(PrmSubLine3)
    return prmLine.toString()
  }

  @Returns("String object value of RUN_GROUP")
  override function getRunGroup() : String {
    return RUN_GROUP
  }

}