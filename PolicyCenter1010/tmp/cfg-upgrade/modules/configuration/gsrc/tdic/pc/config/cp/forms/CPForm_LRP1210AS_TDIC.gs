package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_LRP1210AS_TDIC extends AbstractMultipleCopiesForm<BOPLocation> {
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<BOPLocation> {
    if(context.Period.Job typeis Cancellation and context.Period.RefundCalcMethod == CalculationMethod.TC_FLAT){
      return {}
    }
    if (context.Period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC") {
      var listOfLocations : ArrayList<BOPLocation> = {}
      listOfLocations.add(context.Period.BOPLine?.BOPLocations.first())
      return listOfLocations.HasElements ? listOfLocations?.toList() : {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "BOPLocation_TDIC"
  }

  function isValidPolicyChangeReasonsLRP_TDIC(pp:PolicyPeriod) : boolean{
    var reason = pp?.Job?.ChangeReasons
    if (reason.hasMatch(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_ADDITIONALINSUREDBUILDINGOWNERSLIABILITY ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_BLDGLIMITSINCREASEDECREASE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_BUILDINGOWNERSLIAB ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_CONSTRUCTIONTYPE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_DEDUCTIBLECHANGE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_EQEQSL ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_ILMINESUBSIDENCE||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_LOCATION ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MAILINGADDRESS ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MANUSCRIPENDORSEMENT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MORTGAGEE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_MULTILINEDISCOUNT ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_NAMEDINSURED ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_PROTECTIONCLASSTERRITORYCHANGE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_SPRINKLER ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_BUILDINGOWNERSLIABAI ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_BUILDINGOWNERSLIABILITYCHANGE ||
        elt1.ChangeReason == ChangeReasonType_TDIC.TC_SQFT
    )) {
      return true
    }
    return false

  }
  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    if (_entity.Branch?.isChangeReasonOnlyRegenerateDeclaration_TDIC) {
      contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber))
    }
    if(isValidPolicyChangeReasonsLRP_TDIC(_entity.Branch) && _entity.Branch.Offering.CodeIdentifier=="BOPLessorsRisk_TDIC"){
      contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber))
    }
    contentNode.addChild(createTextNode("RateAsOfDate", _entity.Branch.RateAsOfDate.trimToMidnight().toString()))
    contentNode.addChild(createTextNode("AddressLine1", _entity.Location.AddressLine1))
    contentNode.addChild(createTextNode("City", _entity.Location.City))
    contentNode.addChild(createTextNode("PostalCode", _entity.Location.PostalCode))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }
}