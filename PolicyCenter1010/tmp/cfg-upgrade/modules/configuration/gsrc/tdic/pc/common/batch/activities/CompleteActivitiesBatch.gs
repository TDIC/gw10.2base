package tdic.pc.common.batch.activities

uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses gw.api.database.Query
uses gw.api.system.PLLoggerCategory
uses java.lang.Exception
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory

class CompleteActivitiesBatch extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${CompleteActivitiesBatch.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_COMPLETEACTIVITIES_TDIC);
  }

  /*
    Used to determine if conditions are met to begin processing
      Return true: doWork() will be called
      Return false: only a history event will be written
      Avoids lengthy processing in doWork() if there is nothing to do
   */
  override function checkInitialConditions() : boolean {
    return true;
  }

  override function doWork() {
    var msg : String;

    // Get Activities whose status is "Open"
    var query = Query.make(Activity);
    query.compare("Status", Equals, ActivityStatus.TC_OPEN);

    _logger.info (_LOG_TAG + "Querying for Activities.");
    var results = query.select();
    _logger.debug (_LOG_TAG + "Found " + results.Count + " potential records.");

    // Don't set Operations Expected, because we don't know yet how many records need to be updated.
    // OperationsExpected = results.Count;

    for (activity in results) {
      if (shouldCompleteActivity(activity)) {
        if (_logger.isTraceEnabled()) {
          msg = "Completing activity \"" + activity.Subject + "\" (ID = " + activity.ID
                    + ") - AccountNumber = " + activity.Account.AccountNumber
                    + ", PolicyNumber = " + activity.Policy.MostRecentBoundPeriodOnMostRecentTerm.PolicyNumber
                    + ", JobNumber = " + activity.Job.JobNumber;
          _logger.trace (_LOG_TAG + msg);
        }

        try {
          Transaction.runWithNewBundle (\bundle -> {
            activity = bundle.add(activity);
            activity.complete();
          }, "iu");
        } catch (e : Exception) {
          _logger.error (_LOG_TAG + "Exception occurred while completing activity (ID = " + activity.ID + "): "
              + e.toString());
          incrementOperationsFailed();
        }
        incrementOperationsCompleted();
      }
    }

    _logger.info (_LOG_TAG + "Completed " + (OperationsCompleted - OperationsFailed) + " activities.  "
        + OperationsFailed + " updates failed.");
  }

  protected function shouldCompleteActivity (activity : Activity) : boolean {
    var retval = false;
    var msg : String;

    switch (activity.ActivityPattern.Code) {
      case "audit_fee_service":                 // Close the activity if the audit has been completed.
      case "audit_reversed_reprocess_audit":    // Close the activity if the audit has been reprocessed.
      case "new_audit_assigmnent":              // Close the activity if the audit has been completed.
        retval = isAuditComplete (activity);
        break;

      case "preemption":                        // Close the activity if the audit has been completed.
          retval = isJobComplete (activity);
          break;

      case "multiline_discount_policy_cancelled":   // Close activity during manual migration.
      case "reprocess_renewal":                     // Close activity during manual migration.
        retval = isJobMigratedComplete (activity);
        break;

      case "notification":
        if (activity.Subject == DisplayKey.get("Job.Cancellation.CancellationForRenewal")) {
          retval = isJobComplete (activity);    // Close activity if the cancellation is completed.
        } else {
          if (_logger.isTraceEnabled()) {
            msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
                + ", AccountNumber = " + activity.Account.AccountNumber
                + ", PolicyNumber = " + activity.Policy.MostRecentBoundPeriodOnMostRecentTerm.PolicyNumber
                + ", JobNumber = " + activity.Job.JobNumber + ") will be left open.";
            _logger.trace (_LOG_TAG + msg);
          }
        }
        break;

      case "general_reminder":
        if (_logger.isTraceEnabled()) {
          msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
              + ", AccountNumber = " + activity.Account.AccountNumber
              + ", PolicyNumber = " + activity.Policy.MostRecentBoundPeriodOnMostRecentTerm.PolicyNumber
              + ", JobNumber = " + activity.Job.JobNumber + ") will be left open.";
          _logger.trace (_LOG_TAG + msg);
        }
        break;

      default: _logger.warn (_LOG_TAG + "shouldCompleteActivity not implemented for ActivityPattern "
                               + activity.ActivityPattern.Code);
    }

    return retval;
  }

  protected function isJobComplete (activity : Activity) : boolean {
    var retval = false;
    var job = activity.Job;
    var msg : String;
    var policyPeriod = job.LatestPeriod;

    if (job.Complete) {
      retval = true;
    } else {
      if (_logger.isTraceEnabled()) {
        msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
            + ", PolicyNumber = " + policyPeriod.PolicyNumber
            + ") is associated to " + job.Subtype.DisplayName + " " + job.JobNumber
            + " which has a status of " + policyPeriod.Status.DisplayName + ".  Activity will be left open.";
        _logger.trace (_LOG_TAG + msg);
      }
    }

    return retval;
  }

  protected function isAuditComplete (activity : Activity) : boolean {
    var retval = false;
    var job = activity.Job;
    var msg : String;
    var policyPeriod = job.LatestPeriod;

    if (job typeis Audit) {
      // Check if the audit is completed.
      if (policyPeriod.Status == PolicyPeriodStatus.TC_AUDITCOMPLETE) {
        retval = true;
      } else {
        if (_logger.isTraceEnabled()) {
          msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
                      + ", PolicyNumber = " + policyPeriod.PolicyNumber + ") is associated to Audit " + job.JobNumber
                      + " which has a status of " + policyPeriod.Status.DisplayName + ".  Activity will be left open.";
          _logger.trace (_LOG_TAG + msg);
        }
      }
    } else {
      // Get the latest audit on the term.
      var audit = getFinalAudit(policyPeriod.PolicyTerm);
      policyPeriod = audit.LatestPeriod;
      // Check if the audit is completed.
      if (policyPeriod.Status == PolicyPeriodStatus.TC_AUDITCOMPLETE) {
        retval = true;
      } else if (audit == null) {
        msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
                + ", PolicyNumber = " + job.LatestPeriod.PolicyNumber
                + ") is associated to a term without an Audit.  Activity will be left open.";
        _logger.warn (_LOG_TAG + msg);
      } else {
        if (_logger.isTraceEnabled()) {
          msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
                  + ", PoliciyNumber = " + policyPeriod.PolicyNumber
                  + ") is associated to a term with Audit " + audit.JobNumber
                  + " which has a status of " + policyPeriod.Status.DisplayName + ".  Activity will be left open.";
          _logger.trace (_LOG_TAG + msg);
        }
      }
    }

    return retval;
  }

  /**
   * Find the audit for the policy term that hasn't been reversed, revised, withdrawn, or waived.
   */
  protected function getFinalAudit (policyTerm : PolicyTerm) : Audit {
    var retval : Audit;
    var auditInformation : AuditInformation;
    for (audit in policyTerm.Policy.Jobs.whereTypeIs(Audit)) {
      if (audit.LatestPeriod.PolicyTerm == policyTerm) {
        auditInformation = audit.AuditInformation;
        if (auditInformation.HasBeenReversed == false and auditInformation.RevisingAudit == null
        and auditInformation.IsWithdrawn == false and auditInformation.IsWaived == false) {
          retval = audit;
          break;
        }
      }
    }

    return retval;
  }

  /*
   * Returns true if job was completed during migration.
   */
  protected function isJobMigratedComplete (activity : Activity) : boolean {
    var retval = false;
    var currentMode = ScriptParameters.TDIC_DataSource;
    var job = activity.Job;
    var msg : String;
    var policyPeriod = job.LatestPeriod;

    if (currentMode != DataSource_TDIC.TC_POLICYCENTER.Code) {
      if (job.Complete) {
        retval = true;
      } else {
        if (_logger.isTraceEnabled()) {
          msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
                  + ", PolicyNumber = " + policyPeriod.PolicyNumber
                  + ") is associated to " + job.Subtype.DisplayName + " " + job.JobNumber
                  + " which has a status of " + policyPeriod.Status.DisplayName + ".  Activity will be left open.";
          _logger.trace (_LOG_TAG + msg);
        }
      }
    } else {
      if (_logger.isTraceEnabled()) {
        if (job.Complete) {
          msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
                  + ", PolicyNumber = " + policyPeriod.PolicyNumber
                  + ") is associated to " + job.Subtype.DisplayName + " " + job.JobNumber
                  + ".  It cannot be determined whether the job completed before or after migration.  "
                  + "Activity will be left open.";
        } else {
          msg = "Activity \"" + activity.Subject + "\" (ID = " + activity.ID
                  + ", PolicyNumber = " + policyPeriod.PolicyNumber
                  + ") is associated to " + job.Subtype.DisplayName + " " + job.JobNumber
                  + " which has a status of " + policyPeriod.Status.DisplayName + ".  Activity will be left open.";
        }
        _logger.trace (_LOG_TAG + msg);
      }
    }

    return retval;
  }

  /*
  Called by Guidewire to request that process self-terminate
    Return true if request will be honored
    Return false if request will not be honored
  Handled by setting monitor flag in code and exiting any loops
  */
  override function requestTermination() : boolean {
    return false;
  }
}