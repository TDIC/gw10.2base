package tdic.pc.config.cp.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode
uses entity.FormAssociation

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 2/5/2020
 * Create Time: 4:49 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_LRP2115AS_TDIC extends AbstractMultipleCopiesForm<BOPManuscript_TDIC> {

  var policyPeriod=PolicyPeriod
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<BOPManuscript_TDIC> {
    var period = context.Period
    var bopManuscriptSchedItems = period.BOPLine.BOPLocations*.Buildings*.BOPManuscript
    var bopManuscriptEndorExists = period.BOPLine.BOPLocations?.hasMatch(\loc -> loc.Buildings?.
        hasMatch(\building -> building.BOPManuscriptEndorsement_TDICExists))
    var manuscriptItems : ArrayList<BOPManuscript_TDIC> = {}
    if (context.Period.Offering.CodeIdentifier == "BOPLessorsRisk_TDIC" and bopManuscriptEndorExists and bopManuscriptSchedItems.HasElements){
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var manuscriptSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis BOPManuscript_TDIC)*.Bean
        var additionalInsuredSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyAddlInsured)*.Bean
        var mortgageeSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyMortgagee_TDIC)*.Bean
        var policyAddInsuredDetailModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyAddlInsuredDetail)*.Bean

        if(bopManuscriptSchedItems.hasMatch(\item -> item.BasedOn == null)){
          manuscriptItems.add(bopManuscriptSchedItems.where(\item -> item.BasedOn == null).first())
        }
        else if(bopManuscriptSchedItems.hasMatch(\item -> manuscriptSchedLineItemsModified.contains(item))) {
          manuscriptItems.add(bopManuscriptSchedItems.where(\item -> manuscriptSchedLineItemsModified.contains(item)).first())
        }
        if (additionalInsuredSchedLineItemsModified.HasElements) {
          var additionalInsuredManuscript= (additionalInsuredSchedLineItemsModified.firstWhere(\elt ->  (elt as PolicyAddlInsured).PolicyAdditionalInsuredDetails*.BOPManuScript.Count>0)) as PolicyAddlInsured
          if (additionalInsuredManuscript != null &&  manuscriptItems.Count==0) {
            manuscriptItems.add(additionalInsuredManuscript*.PolicyAdditionalInsuredDetails*.BOPManuScript.first())
          }
        }

        if(policyAddInsuredDetailModified.HasElements){
          var policyAddInsuredDetail=(policyAddInsuredDetailModified.firstWhere(\elt -> (elt as PolicyAddlInsuredDetail).BOPManuScript.Count>0)) as PolicyAddlInsuredDetail
          if(policyAddInsuredDetail!=null && manuscriptItems.Count==0){
            manuscriptItems.add(policyAddInsuredDetail*.BOPManuScript.first())
          }
        }

         if(mortgageeSchedLineItemsModified.HasElements){
          var mortgageeManuscript=(mortgageeSchedLineItemsModified.firstWhere(\elt -> (elt as PolicyMortgagee_TDIC).BOPManuscript.Count>0)) as PolicyMortgagee_TDIC
          if(mortgageeManuscript!=null && manuscriptItems.Count==0){
            manuscriptItems.add(mortgageeManuscript.BOPManuscript.first())
          }
        }
      }
      else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(period.RefundCalcMethod != CalculationMethod.TC_FLAT) {
          manuscriptItems.add(bopManuscriptSchedItems.first())
        }
      }
      else {
        manuscriptItems.add(bopManuscriptSchedItems.first())
      }
      return manuscriptItems.HasElements? manuscriptItems.toList(): {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "BOPManuscript_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    //contentNode.addChild(createTextNode("EffectiveDate", _entity.ManuscriptEffectiveDate.toString()))
    contentNode.addChild(createTextNode("ManuscriptEffectiveDate", _entity.ManuscriptEffectiveDate?.toString()))
    contentNode.addChild(createTextNode("ManuscriptExpirationDate", _entity.ManuscriptExpirationDate?.toString()))
    contentNode.addChild(createTextNode("ManuscriptType", _entity.ManuscriptType.toString()))
    contentNode.addChild(createTextNode("ManuscriptDescription", _entity.ManuscriptDescription))
    contentNode.addChild(createTextNode("policyAddInsured_TDIC", _entity.policyAddInsured_TDIC?.toString()))
    contentNode.addChild(createTextNode("policyMortgagee", _entity.PolicyMortgagee?.toString()))
    contentNode.addChild(createTextNode("Desciption", _entity.policyAddInsured_TDIC?.Desciption?.toString()))
    contentNode.addChild(createTextNode("LoanNumberString", _entity.PolicyMortgagee?.LoanNumberString?.toString()))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }

  override function getMatchKeyForForm( form : Form ) : String {
    if(form.FormAssociations.HasElements){
      if((form.FormAssociations[0] as CPFormAssociation_TDIC).BOPLocation_TDIC !== null) {
        return form.FormAssociations[0].fixedIdValue("BOPLocation_TDIC") as java.lang.String
      }
      return form.FormAssociations[0].fixedIdValue(FormAssociationPropertyName) as java.lang.String
    }
    return null
  }
}

