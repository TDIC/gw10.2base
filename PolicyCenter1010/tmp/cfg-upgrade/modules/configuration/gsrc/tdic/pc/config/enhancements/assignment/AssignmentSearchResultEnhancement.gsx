package tdic.pc.config.enhancements.assignment

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.assignment.AssignmentSearchCriteria

/**
 * Created with IntelliJ IDEA.
 * User: RadhakrishnaS
 * Date: 1/10/16
 * Time: 10:27 PM
 */
enhancement AssignmentSearchResultEnhancement : gw.api.assignment.AssignmentSearchResult {

  /**
   * Does a search on the GroupUser table for external users.
  */
  public function getUsersForExternalUser(criteria : AssignmentSearchCriteria) : IQueryBeanResult {
    var groupUser = Query.make(GroupUser)
    var grpUserTable = groupUser.join("User")
    var credTable = grpUserTable.join("Credential")
    var contTab = grpUserTable.join("Contact")
    var userName = criteria.UserCriteria.Username
    if( not String.isEmpty(userName) ) {
      credTable.contains("UserName", userName, true)
    }
    var firstName = criteria.UserCriteria.Contact.FirstName
    if( not String.isEmpty(firstName) ) {
      contTab.contains("FirstName", firstName, true)
    }
    var lastName = criteria.UserCriteria.Contact.Keyword
    if( not String.isEmpty(lastName) ) {
      contTab.contains("LastName", lastName, true)
    }
    return groupUser.select()
  }

  /**
   * GW -237:: Retrieving all the groups in the Organization.
   * Does a search on the Group table for external Groups.
   */
  public function getExternalGroups(criteria :AssignmentSearchCriteria) :IQueryBeanResult
  {
    var groups = Query.make(Group)
    // .compare("Organization", Equals, currUser.Organization) -
    // retrieves only based on the Organization type
      return groups?.select()
  }
}
