package tdic.pc.common.batch.lossratio

uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses com.tdic.util.misc.EmailUtil
uses java.lang.Exception
uses java.lang.StringBuilder
uses java.lang.System
uses org.slf4j.LoggerFactory

/**
 *  GW-2971 - Batch process to Recalculate Loss Ratio for all Policy Terms and persist
 *  Loss Ratio and Earned premium for each Policy Term.
 */
class TDIC_LossRatioRecalculateBatch extends BatchProcessBase {

  private static final var _logger = LoggerFactory.getLogger("TDIC_BATCH_LOSSRATIORECLACULATE")
  private static var CLASS_NAME = "TDIC_LossRatioRecalcBatch"
  private static final var BATCH_USER = "iu"
  private var _emailTo : String as readonly EmailTo
  private static final var EMAIL_RECIPIENT_PROP = "PCInfraIssueNotificationEmail"
  private static final var FAILURE_SUB = "Failure::${gw.api.system.server.ServerUtil.ServerId}-Loss Ratio Recalculation Batch Job Failed"
  private static final var TERMINATE_SUB = "Terminate::${gw.api.system.server.ServerUtil.ServerId}-Loss Ratio Recalculation Batch Job Terminated"
  private static final var SUCCESS_SUB = "Success::${gw.api.system.server.ServerUtil.ServerId}-Loss Ratio Recalculation Batch Job completed successfully"

  construct(){
    super(BatchProcessType.TC_LOSSRATIORECALC_TDIC)
  }

  override function doWork() {
    var logPrefix = "${CLASS_NAME}#doWork()"
    _logger.info("${logPrefix} - Entering")
    var batchStartTime = System.currentTimeMillis()

    //Retrieve Policy Terms
    var policyTermsQuery = Query.make(entity.PolicyTerm)
    var policyTerms = policyTermsQuery.compare(PolicyTerm#Bound, Equals, true).select() //GWPS-670 : Keshavg : putting condition for the batch to run for only Bound Terms
    var message: String
    //Process policy terms for loss ratio recalculation
    for(term in policyTerms){
      if (TerminateRequested) {
        _logger.warn("${logPrefix} - Terminate requested while processing Loss ratio recalculation for retrieved terms.")
        EmailUtil.sendEmail(EmailTo, TERMINATE_SUB, "PC Loss Ratio Recalculation batch is terminated during the process of loss ratio recalculation for retrieved terms.")
        return
      }
      try{
        //Recalculate Loss ratio and persist values
        message = null
        gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
          term = bundle.add(term)
          message = term.recalculateLossRatio()
          if(_logger.isDebugEnabled()){
            if(message.HasContent){
              _logger.debug("${logPrefix} - LossRatioRecalculation message for term: ${term}; publicid: ${term.PublicID} - ${message}")
            }
          }
        },BATCH_USER)
      }catch(e: Exception){
        OperationsFailed++
        _logger.error("${logPrefix} - LossRatioRecalculation failed for term: ${term} - ${e.LocalizedMessage}")
      }
      OperationsCompleted++
    }
    EmailUtil.sendEmail(EmailTo, SUCCESS_SUB, "Loss Ratio Recalculation batch job has been compelted successfully.")
    _logger.info("${logPrefix} - Total time taken for completing this batch process: ${System.currentTimeMillis()-batchStartTime}(ms)")
    _logger.info("${logPrefix} - Exiting")
  }

  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    _logger.trace("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(EMAIL_RECIPIENT_PROP)
    _logger.trace("${logPrefix} - ${EMAIL_RECIPIENT_PROP}-${_emailTo}")

    var initFailureMsg:StringBuilder
    if(_emailTo == null){
      initFailureMsg = new StringBuilder()
      initFailureMsg.append("${logPrefix} - Failed to retrieve notification email addresses with the key '${EMAIL_RECIPIENT_PROP}' from integration database.")
    }

    if (initFailureMsg?.toString().HasContent) {
      if(_emailTo != null)
        EmailUtil.sendEmail(EmailTo, FAILURE_SUB, "Unable to initiate batch process. Please review log file for more details.")
      throw new GWConfigurationException(initFailureMsg.toString())
    }
    _logger.trace("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  @Returns("Returns Boolean status of termination request")
  override function requestTermination() : boolean {
    super.requestTermination()
    return Boolean.TRUE
  }


}