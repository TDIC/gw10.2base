package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 4/7/2020
 * Create Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_CXERE_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" || context.Period.Offering.CodeIdentifier == "PLCyberLiab_TDIC") {
      if (context.Period.Status == PolicyPeriodStatus.TC_BINDING &&
          context.Period.Job.ChangeReasons.hasMatch(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_EREPLRESCINDED || elt1.ChangeReason == ChangeReasonType_TDIC.TC_ERECYBERRESCINDED)){
        return true
      }
    }
    return false
  }

}