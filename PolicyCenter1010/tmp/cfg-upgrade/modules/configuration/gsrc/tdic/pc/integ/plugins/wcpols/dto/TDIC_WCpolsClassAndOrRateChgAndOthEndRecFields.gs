package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator
/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 9/15/16
 * Time: 11:34 AM
 * This class includes variables for all fields in class code or rate change record for policy specification report.
 */
class TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields extends  TDIC_FlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _stateCode : int as StateCode
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _endorsementNumber : String as EndorsementNumber
  var _bureauVersionIdentifier : String as BureauVersionIdentifier
  var _carrierVersionIdentifier : String as CarrierVersionIdentifier
  var _exposurePeriodEffDate : String as ExposurePeriodEffDate
  var _classificationCodeRevisionCode : String as ClassificationCodeRevisionCode
  var _classificationCode : String as ClassificationCode
  var _exposureActOrExposureCovCode : int as ExposureActOrExposureCovCode
  var _manualOrChargedRate : String as ManualOrChargedRate
  var _estimatedExposureAmount : long as EstimatedExposureAmount
  var _estimatedPremiumAmount : String as EstimatedPremiumAmount
  var _classificationWordingSuffix : String as ClassificationWordingSuffix
  var _classificationWording : String as ClassificationWording
  var _nameLinkIdentifier : int as NameLinkIdentifier
  var _stateCodeLink : int as StateCodeLink
  var _exposureRecordLinkForExposureCode : int as ExposureRecordLinkForExposureCode
  var _classificationUseCode : String as ClassificationUseCode
  var _exposurePeriodCode : int as ExposurePeriodCode
  var _numberOfPiecesOfApparatus : String as NumberOfPiecesOfApparatus
  var _numberOfVolunteers : String as NumberOfVolunteers
  var _futureReserved2 : String as FutureReserved2
  var _nameOfInsured : String as NameOfInsured
  var _endorsementEffectiveDate : String as EndorsementEffectiveDate
  var _futureReserved3 : String as FutureReserved3


}