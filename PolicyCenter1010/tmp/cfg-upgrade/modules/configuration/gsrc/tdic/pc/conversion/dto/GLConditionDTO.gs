package tdic.pc.conversion.dto

class GLConditionDTO {

  var dateTerm : String as DateTerm
  var choiceTerm : String as ChoiceTerm
  var directTerm : String as DirectTerm
  var condPatternCode : String as CondPatternCode
  var condTermPatternCode : String as CondTermPatternCode
  var policyID : String as PolicyID
  var publicID : String as PublicID

}