package tdic.pc.integ.plugins.wcpols.dto

uses java.util.Date
/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/3/14
 * Time: 10:24 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCpolsTransaction {
  protected var _id : String as ID
  protected var _createdDate : Date as CreatedDate
  protected var _payload:String as Payload
  protected var _publicID:String as PublicID
}