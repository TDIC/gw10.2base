package tdic.pc.config.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses gw.api.database.upgrade.before.IBeforeUpgradeUpdateBuilder
uses org.slf4j.LoggerFactory

// BrianS - Class that can be used to change a typekey column.
class ChangeTypekeyColumn  extends BeforeUpgradeVersionTrigger {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade")
  private static final var _LOG_TAG = "${ChangeTypekeyColumn.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber
  private var _tableName : String as readonly TableName
  private var _oldColumnName : String as readonly OldColumnName
  private var _oldTypelist : String as readonly OldTypelist
  private var _newColumnName : String as readonly NewColumnName
  private var _newTypelist : String as readonly NewTypelist

  construct(versionNumber : int, tableName : String, oldColumnName : String, oldTypelist : String,
            newColumnName : String, newTypelist : String) {
    super(versionNumber)
    _versionNumber = versionNumber
    _tableName = tableName
    _oldColumnName = oldColumnName
    _oldTypelist = oldTypelist
    _newColumnName = newColumnName
    _newTypelist = newTypelist
  }

  override public property get Description() : String {
    return "Move data from " + _tableName + "." + _oldColumnName + " to " + _newColumnName
  }

  // BrianS - No version checks are needed for this upgrade.
  override function hasVersionCheck() : boolean {
    return false
  }

  override function execute() : void {
    var table = getTable(_tableName)
    var oldColumn = table.getColumn(_oldColumnName)
    var newColumn = table.getColumn(_newColumnName)

    // Step 1:  Create new column.
    _logger.info (_LOG_TAG + "Creating column: " + _tableName + "." + _newColumnName)
    newColumn.create()

    var oldTypelistMap = getTypeKeys(_oldTypelist)
    var newTypelistMap = getTypeKeys(_newTypelist)
    var builder : IBeforeUpgradeUpdateBuilder

    // Step 2:  Copy data to the new column.
    for (value in oldTypelistMap.Keys) {
      _logger.info (_LOG_TAG + "Copying " + value + " from " + _tableName + "." + _oldColumnName
          + " to " + _tableName + "." + _newColumnName)
      builder = table.update()
      builder.compare(oldColumn, Equals, oldTypelistMap.get(value))
      builder.set(newColumn, newTypelistMap.get(value))
      builder.execute()
    }

    // Step 3:  Delete old column.
    _logger.info (_LOG_TAG + "Removing column " + _tableName + "." + _oldColumnName)
    oldColumn.drop()

    _logger.info (_LOG_TAG + "Completed " + Description)
  }
}