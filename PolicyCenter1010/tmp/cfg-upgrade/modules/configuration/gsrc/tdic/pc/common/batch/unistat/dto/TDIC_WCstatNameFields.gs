package tdic.pc.common.batch.unistat.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator


/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/18/14
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCstatNameFields extends TDIC_FlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _exposureStateCode : int as ExposureStateCode
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _reportLevelCode : int as ReportLevelCode
  var _correctionSeqNum : String as CorrectionSeqNum
  var _recordTypeCode : int as RecordTypeCode
  var _nameOfInsured : String as NameOfInsured
  var _futureReserved1 : String as FutureReserved1

}