package tdic.pc.integ.plugins.wcpols.helper

uses org.apache.commons.lang.StringUtils
uses tdic.util.flatfile.excel.ExcelDefinitionReader
uses tdic.util.flatfile.model.classes.FlatFile
uses java.io.PrintWriter
uses java.lang.Exception
uses java.util.ArrayList

uses tdic.util.flatfile.model.classes.Record
uses tdic.pc.integ.plugins.wcpols.TDIC_WCPolsCompleteRecord
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/6/14
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_FlatFileUtilityHelper {
 private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCPOLSREPORT")

  /**
   * Default Constructor
   */
  construct() {
  }

  /**
   * This Method takes a String as input along with various vendor specifications for the particular string
   * The String is formatted according to the Vendor Specifications and returned.
   * Vendor Specifications include String (Length,Truncate,Strip,Fill,Justify,Format)
   */
  public static function formatString(value: String, destLength: int, truncate: String, justify: String, fill: String, format: String, strip: String): String {
    //First strip unnecessary characters
    if (!strip.equals("'")){//Default value is "'". So, remove chars if it is not the default value
      value = StringUtils.remove(value, strip)//Strip the values
    }
    if (fill == "'"){//Default value of fill is "'" which is equal to a white space
      fill = " "
    }
    if (value == null){//Handle null values
      value = fill
    }
    if (format == "S9(9)v99") {// Financial fields has a special requirement. Last two digits are considered as decimals. $ is replaced by +
      var currencyArray = value.split("\\.")//Split at dot.
      if (currencyArray.length == 2){//Valid currency
        value = currencyArray[0] + StringUtils.rightPad(currencyArray[1], Coercions.makePIntFrom("0"), 2 as char)//Right pad decimals with 0 and append currency value to it
        value = StringUtils.leftPad(value, destLength - 1, "0")//Left pad value to destLength-1 with zeros
        value = "+" + value//Append + sign
      }
    }
    else {
      if (format[0] == "9"){//If value is an integer, discard decimals
        value = value.split("\\.")[0]
      }
    }
    var newValue = value
    if (value.length < destLength) {//String length is less than required length. Do padding
      if (justify == "Right") {//Right pad with fill values
        newValue = StringUtils.leftPad(value, destLength, fill)
      }
      else if (justify == "Left"){//Left pad with fill values
        newValue = StringUtils.rightPad(value, destLength, fill)
      }
    }
    else if (value.length > destLength) {//String length is greater than required length. Truncate
      if (truncate == "Right") {//Truncate right
        newValue = value.substring(0, destLength)
      }
      else if (truncate == "Left"){//Truncate Left
        newValue = value.substring(value.length - destLength, value.length)
      }
    }
    return newValue
  }

  public function encodeandReturnEncodedString(excelFilePath: String, excelFileHeaderPath: String, completeRecord: TDIC_WCPolsCompleteRecord, flatFilePath: String): String {
    var excel: ExcelDefinitionReader
    var vendorStructure: ArrayList<Record>
    try {
      excel = new ExcelDefinitionReader(excelFileHeaderPath)
    }
        catch (var e: Exception) {
          _logger.error("Unable To Read Excel File Header")
          return false as String
        }
    try {

      vendorStructure = excel.read(excelFilePath)//Read Excel Definition
    }
        catch (var e: Exception) {
          e.printStackTrace()
          _logger.error("Unable To Read Excel Desination File")
          return false as String
        }
    return completeRecord.convertToFlatFileRecord(vendorStructure?.toTypedArray())
  }

  public function writeEncodedFlatfile(reportData: String, flatFilePath: String): boolean {
    var flatFile: PrintWriter
    var excel: ExcelDefinitionReader
    var vendorStructure: FlatFile
    try {
      flatFile = new PrintWriter(flatFilePath)//Open flat file for writing
    }
        catch (var e: Exception) {
          _logger.error("Unable To Create Flat File")
          return false
        }

    try {
      var flatFileLine = ""//Create a new flat file line
      flatFile.println(reportData)//Write to flat file
      //}
      flatFile.close()//Close flat file
    }
        catch (var e: Exception) {
          e.printStackTrace()
          _logger.error("Error While Generating Flat File")
          return false
        }
    return true
  }
}