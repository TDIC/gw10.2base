package tdic.pc.config.rating.wc7.factory

uses gw.api.upgrade.PCCoercions
uses gw.lob.wc7.rating.WC7RatingStepData
uses tdic.pc.config.rating.wc7.WC7CovStepData
uses tdic.pc.config.rating.wc7.WC7ROCEntry
uses tdic.pc.config.rating.wc7.WC7RatingFunctions
uses tdic.pc.config.rating.wc7.WC7RatingUtil
uses tdic.pc.config.rating.wc7.context.WC7RateFlowContext
uses tdic.pc.config.rating.wc7.flow.RateFlowFactory
uses tdic.pc.config.rating.wc7.flow.WC7CovCostRateFlowItem
uses tdic.pc.config.rating.wc7.flow.WC7RateFlowItem
uses gw.api.productmodel.ConditionPattern
uses gw.api.productmodel.CoveragePattern
uses typekey.WC7CoveredEmployeeBase
uses java.util.concurrent.atomic.AtomicInteger
uses tdic.pc.config.rating.RatingException
uses gw.api.productmodel.ClausePattern
uses gw.lob.wc7.rating.WC7RatingPeriod
uses gw.api.productmodel.PolicyLinePattern
uses gw.api.productmodel.ModifierPattern

/**
 * A rate flow factory to populate rate order entries, other factory methods
 *
 * @author- Ankita Gupta
 */

class WC7RateFlowFactory implements RateFlowFactory<WC7Line, WC7RateFlowItem> {

  var _rateOrderEntries : List<WC7ROCEntry>
  var _paramsFactory : WC7ParamsFactory
  var _costDataFactory : WC7CostDataFactory
  var _rateFlowContext : WC7RateFlowContext as readonly Context

  construct(rateFlowContext : WC7RateFlowContext) {
    _rateFlowContext = rateFlowContext
    init()
  }

  private function init() {
    var rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String)
    _rateOrderEntries = WC7RatingUtil.readRateOrderEntries(rateBook).where(\entry -> WC7RatingUtil.isROCPremiumReportEligible(Context, entry)).orderBy(\elt -> elt.Order)
    _paramsFactory = new WC7ParamsFactory(Context)
    _costDataFactory = new WC7CostDataFactory(Context.NumberOfDaysInTerm)
    //Context.PolicyLine.Branch.MinimumPremium = null
  }

  function createRatingItems() : List<WC7RateFlowItem> {
    final var counter = new AtomicInteger()
    return _rateOrderEntries.flatMap(\elt -> createEntries(elt, counter)).orderBy(\elt -> elt.Order)
  }

  private function createEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    if (entry.ClausePatternCode.HasContent) {
      if ((PCCoercions.makeProductModel<ClausePattern>(entry.ClausePatternCode)) typeis CoveragePattern) {
        return createCoverageEntries(entry, counter)
      } else if ((PCCoercions.makeProductModel<ClausePattern>(entry.ClausePatternCode)) typeis ConditionPattern) {
        return createConditionEntries(entry, counter)
      } else if (PCCoercions.makeProductModel<ModifierPattern>(entry.ClausePatternCode) != null) {
        return createModifierEntries(entry, counter)
      } else if ((PCCoercions.makeProductModel<PolicyLinePattern>(entry.ClausePatternCode)) != null or entry.ClausePatternCode.equals(WC7Jurisdiction.Type.RelativeName)) {
        return createNonCoverageEntries(entry, counter)
      }
    }

    if (!entry.CostEntityName.HasContent and entry.RoutineCode.HasContent) {
      return createNoCostEntries(entry, counter)
    }
    throw new RatingException("Invalid Entry for CoveragePattern Code: " + entry.toString())
  }


  override function createRateFlow(line : WC7Line) : List<WC7RateFlowItem> {
    return {}
  }

  private function createCoverageEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    var owningEntityType = PCCoercions.makeProductModel<ClausePattern>(entry.ClausePatternCode).OwningEntityType
    var coverableRatingLevel = WC7RatingUtil.getRatingLevelForCoveredObject(owningEntityType)
    switch (coverableRatingLevel) {

      case TC_WC7LINE:
        return createLineCoverageEntries(entry, counter)

      case TC_WC7JURISDICTION:
        return createJurisdictionEntries(entry, counter)

      default:
        throw new RatingException(" Unexpected coverable level: " + coverableRatingLevel + " for owning type '" + owningEntityType + "'")
    }
  }

  private function createLineCoverageEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    var items : List<WC7RateFlowItem> = {}
    var rateBook : RateBook
    var ratingPeriod : WC7RatingPeriod
    if (Context.PolicyLine.CoveragesFromCoverable.hasMatch(\elt1 -> elt1.PatternCode == entry.ClausePatternCode)) {
      var cov = Context.PolicyLine.CoveragesFromCoverable.singleWhere(\elt -> elt.PatternCode == entry.ClausePatternCode)
      if (entry.RatingLevelExt == TC_WC7COVEMP) {
        var covEmps = Context.PolicyLine.WC7CoveredEmployeeVLs
            .flatMap(\empVL -> empVL.AllVersions)
            .where(\genericEmp -> filterClassesByCov(cov, genericEmp))

        for (covEmp in covEmps) {
          if (rateBook == null or (ratingPeriod != null and ratingPeriod.Jurisdiction != covEmp.Jurisdiction)) {
            rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String, covEmp.Jurisdiction)
          }
          ratingPeriod = new WC7RatingPeriod(covEmp.Jurisdiction, covEmp.EffectiveDateForRating, covEmp.ExpirationDateForRating, 1)
          if (WC7RatingUtil.isROCEntryEligible(entry, ratingPeriod.Jurisdiction.Jurisdiction)) {
            var stepData = new WC7CovStepData(rateBook, cov, covEmp, ratingPeriod)
            items.add(new WC7CovCostRateFlowItem(_paramsFactory, _costDataFactory, stepData, entry, counter.AndIncrement))
          }
        }
      } else if (entry.RatingLevelExt == TC_WC7JURISDICTION) {
        if (entry.IsCrossJurisdiction) {
          return createCrossJurisdictionEntry(entry, cov, counter)
        }

        for (aJurisdiction in Context.PolicyLine.WC7Jurisdictions) {
          var periods = getRatingPeriods(entry, aJurisdiction)
          if (WC7RatingUtil.isROCEntryEligible(entry, aJurisdiction.Jurisdiction)) {
            for (period in periods) {
              rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String, aJurisdiction)
              var stepData = new WC7CovStepData(rateBook, cov, aJurisdiction, period)
              items.add(new WC7CovCostRateFlowItem(_paramsFactory, _costDataFactory, stepData, entry, counter.AndIncrement))
            }
          }
        }
      } else if (entry.RatingLevelExt == TC_WC7LINE) {
        items.add(createLineEntry(entry, counter))
      }
    }
    return items
  }

  private function createJurisdictionEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    if (entry.IsCrossJurisdiction) {
      return createCrossJurisdictionEntry(entry, null, counter)
    }
    var items : List<WC7RateFlowItem> = {}
    for (aJurisdiction in Context.PolicyLine.RepresentativeJurisdictions) {
      if (WC7RatingUtil.isROCEntryEligible(entry, aJurisdiction.Jurisdiction)) {
        var periods = getRatingPeriods(entry, aJurisdiction)
        for (period in periods) {
          if (aJurisdiction.CoveragesFromCoverable.hasMatch(\c -> c.PatternCode == entry.ClausePatternCode)) {
            var cov = aJurisdiction.CoveragesFromCoverable.singleWhere(\c -> c.PatternCode == entry.ClausePatternCode)
            if (entry.RatingLevelExt == TC_WC7JURISDICTION) {
              var rateBook = WC7RatingUtil.selectRateBook(
                  Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String, aJurisdiction)

              var stepData = new WC7CovStepData(rateBook, cov, aJurisdiction, period)
              items.add(new WC7CovCostRateFlowItem(_paramsFactory, _costDataFactory, stepData, entry, counter.AndIncrement))
            }
          }
        }
      }
    }
    return items
  }

  private function createModifierEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    var items : List<WC7RateFlowItem> = {}
    var modifier : WC7Modifier
    if (entry.RatingLevelExt == TC_WC7JURISDICTION) {
      for (aJurisdiction in Context.PolicyLine.WC7Jurisdictions) {
        if (WC7RatingUtil.isROCEntryEligible(entry, aJurisdiction.Jurisdiction)) {
          var periods = getRatingPeriods(entry, aJurisdiction)
          for (period in periods) {
            // TODO All Modifiers check will come under this loop
            var rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String, aJurisdiction)
            if (entry.ClausePatternCode.equalsIgnoreCase("WC7ExpMod")
                or entry.ClausePatternCode.equalsIgnoreCase("WC7ScheduleMod")){
              if (WC7RatingUtil.isModifierRatable(period, entry)) {
                modifier = WC7RatingUtil.getModifier(period, entry)
              }
              } else if (entry.ClausePatternCode.equalsIgnoreCase("WC7CertifiedSafety")
                or entry.ClausePatternCode.equalsIgnoreCase("WC7TDICMultiLineDiscount")) {
                modifier = period.Jurisdiction.VersionList.WC7ModifiersAsOf(period.RatingStart)
                    .firstWhere(\w -> w.PatternCode == entry.ClausePatternCode)
              }
              if(modifier != null) {
                var stepData = new WC7CovStepData(rateBook, null, modifier, period)
                items.add(new WC7CovCostRateFlowItem(_paramsFactory, _costDataFactory, stepData, entry, counter.AndIncrement))
              }
            }
          
        }
      }
    } else if (entry.RatingLevelExt == TC_WC7LINE) {
      items.add(createLineEntry(entry, counter))
    }
    return items
  }

  /**
   * Handling of coverages that don't fit elsewhere, i.e. deductible credit, terror, cat
   */
  private function createNonCoverageEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    if (entry.IsCrossJurisdiction) {
      return createCrossJurisdictionEntry(entry, null, counter)
    }
    var items : List<WC7RateFlowItem> = {}
    if (entry.RatingLevelExt == typekey.WC7RatingLevel_TDIC.TC_WC7JURISDICTION) {
      for (aJurisdiction in Context.PolicyLine.WC7Jurisdictions) {
        if (WC7RatingUtil.isROCEntryEligible(entry, aJurisdiction.Jurisdiction)) {
          var periods = getRatingPeriods(entry, aJurisdiction)
          for (period in periods) {
            var rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String, aJurisdiction)
            var stepData = new WC7CovStepData(rateBook, null, aJurisdiction, period)
            items.add(createNonCoverageRateFlowItem(stepData, entry, aJurisdiction, counter, null))
          }
        }
      }
    } else if (entry.RatingLevelExt == TC_WC7LINE) {
      /*if (WC7RatingUtil.isManuscriptOption(entry)) {
        items.addAll(createManuscriptEntries(entry, counter))
      } else {*/
        items.add(createLineEntry(entry, counter))
      //}
    }
    return items
  }

  /**
   * Use custom rate flow items for non coverage items
   */
  private function createNonCoverageRateFlowItem(stepData : WC7CovStepData, entry : WC7ROCEntry, jurisdiction : WC7Jurisdiction, counter : AtomicInteger, waiver: WC7WaiverOfSubro) : WC7RateFlowItem {
    return new WC7CovCostRateFlowItem(_paramsFactory, _costDataFactory, stepData, entry, counter.AndIncrement)
  }

  private function createConditionEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    var owningEntityType = (PCCoercions.makeProductModel<ClausePattern>(entry.ClausePatternCode)).OwningEntityType
    var coverableRatingLevel = WC7RatingUtil.getRatingLevelForCoveredObject(owningEntityType)
    var rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String)
    switch (coverableRatingLevel) {

      case TC_WC7LINE:
        return createLineLevelConditionEntries(entry, counter)
      default:
        throw new RatingException(" Unexpected coverable level: " + coverableRatingLevel + " for owning type '" + owningEntityType + "'")
    }
  }

  /**
   * Manuscript rate fow items
   */
/*  private function createManuscriptEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    var items : List<WC7RateFlowItem> = {}
    var mScriptOptions = Context.PolicyLine.WC7ManuscriptOptions
    if (mScriptOptions != null and mScriptOptions.Count > 0) {
      for (mScriptOption in mScriptOptions) {
        if (mScriptOption.Premium != null and mScriptOption.Premium > 0) {
          var rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String)
          var wc7Jurisdiction = Context.PolicyLine.WC7Jurisdictions.first()
          var stepData = new WC7CovStepData(rateBook, null, mScriptOption, new WC7RatingPeriod(wc7Jurisdiction, Context.PolicyLine.Branch.PeriodStart, Context.PolicyLine.Branch.PeriodEnd, 1))
          items.add(new WC7CovCostRateFlowItem(_paramsFactory, _costDataFactory, stepData, entry, counter.AndIncrement))
        }
      }
    }
    return items
  }*/

  private function createLineEntry(entry : WC7ROCEntry, counter : AtomicInteger) : WC7RateFlowItem {
    var rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String)
    var wc7Jurisdiction = Context.PolicyLine.WC7Jurisdictions.first()
    var stepData = new WC7CovStepData(rateBook, null, wc7Jurisdiction, new WC7RatingPeriod(wc7Jurisdiction, Context.PolicyLine.Branch.PeriodStart, Context.PolicyLine.Branch.PeriodEnd, 1))
    return new WC7CovCostRateFlowItem(_paramsFactory, _costDataFactory, stepData, entry, counter.AndIncrement)
  }

  private function createCrossJurisdictionEntry(entry : WC7ROCEntry, cov : Coverage, counter : AtomicInteger) : List<WC7RateFlowItem> {
    var crossJuriFlowItems : List<WC7RateFlowItem> = {}
    var wc7Jurisdiction = Context.PolicyLine.WC7Jurisdictions.first()
    var periods = getRatingPeriods(entry, wc7Jurisdiction)
    for (period in periods) {
      var rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String)
      var stepData = new WC7CovStepData(rateBook, cov, wc7Jurisdiction, period)
      crossJuriFlowItems.add(createCrossJurisdictionFlowItem(stepData, entry, counter))
    }
    return crossJuriFlowItems
  }

  private function createCrossJurisdictionFlowItem(stepData : WC7CovStepData, entry : WC7ROCEntry, counter : AtomicInteger) : WC7RateFlowItem {

    return new WC7CovCostRateFlowItem(_paramsFactory, _costDataFactory, stepData, entry, counter.AndIncrement)
  }

  private function createLineLevelConditionEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    var items : List<WC7RateFlowItem> = {}
    if (entry.RatingLevelExt == TC_WC7JURISDICTION) {
          var waivers = Context.PolicyLine.WC7WaiverOfSubros
          for (waiver in waivers) {
            var aJurisdiction = waiver.WC7WaiverJurisdiction
            if (WC7RatingUtil.isROCEntryEligible(entry, aJurisdiction.Jurisdiction)) {
            var periods = getRatingPeriods(entry, aJurisdiction)
            var rateBook = WC7RatingUtil.selectRateBook(Context.PolicyLine, _rateFlowContext.MinimumRatingLevel, Context.PolicyLine.Branch.Offering as String, aJurisdiction)
            for (period in periods) {
              var stepData = new WC7CovStepData(rateBook, null, waiver, period)
              items.add(createNonCoverageRateFlowItem(stepData, entry, aJurisdiction, counter, waiver))
            }
          }
        }
     }
    return items
  }

  private function createNoCostEntries(entry : WC7ROCEntry, counter : AtomicInteger) : List<WC7RateFlowItem> {
    switch (entry.RatingLevelExt) {

      default:
        throw new RatingException(" Unexpected RatingExt level: " + entry.RatingLevelExt)
    }
  }

  private function filterClassesByCov(cov : Coverage, covEmpBase : entity.WC7CoveredEmployeeBase) : boolean {
    switch (typeof cov) {
      case WC7WorkersCompEmpLiabInsurancePolicyACov:
        return covEmpBase.Subtype == typekey.WC7CoveredEmployeeBase.TC_WC7COVEREDEMPLOYEE
      case WC7FederalEmployersLiabilityActACov:
        return covEmpBase.Subtype == WC7CoveredEmployeeBase.TC_WC7FEDCOVEREDEMPLOYEE
      case WC7MaritimeACov:
        return covEmpBase.Subtype == WC7CoveredEmployeeBase.TC_WC7MARITIMECOVEREDEMPLOYEE
      default:
        return false
    }
  }

  /**
   * Method to get rating periods
   */
  private function getRatingPeriods(entry : WC7ROCEntry, juri : WC7Jurisdiction) : List<WC7RatingPeriod> {
    return WC7RatingUtil.getRatingPeriods(Context, juri, entry)
  }

}