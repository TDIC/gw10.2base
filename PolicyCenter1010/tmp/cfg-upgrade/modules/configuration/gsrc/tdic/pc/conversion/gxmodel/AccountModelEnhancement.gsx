package tdic.pc.conversion.gxmodel

uses gw.api.database.Query
uses gw.pl.persistence.core.Bundle
uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.web.admin.shared.SharedUIHelper

enhancement AccountModelEnhancement : tdic.pc.conversion.gxmodel.policyperiodmodel.anonymous.elements.PolicyPeriod_Policy_Account {

  function createAccount(bundle : Bundle) : Account {
    var contact = this.AccountHolderContact.$TypeInstance.findOrCreateContact(bundle)
    var account = Account.createAccountForContact(contact)
    SimpleValuePopulator.populate(this.$TypeInstance, account)
    var iCodeQuery = Query.make(IndustryCode)
        .compare("Code", Equals, this.IndustryCode.Code)
        .compare("Domain", Equals, this.IndustryCode.Domain)
        .select()
    account.IndustryCode = iCodeQuery.AtMostOneRow
    for (entry in this.ProducerCodes.Entry) {
      var producerCode = Query.make(ProducerCode)
          .compare("Code", Equals, entry.ProducerCode.Code)
          .select().AtMostOneRow
      account.addProducerCode(producerCode)
    }
    return account
  }

}
