package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/30/14
 * Time: 11:07 AM
 * This class includes variables for all fields in address record for policy specification report.
 */
class TDIC_WCpolsAddressFields  extends  TDIC_FlatFileLineGenerator  {
  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _addressTypeCode : int as AddressTypeCode
  var _foreignAddressInd : String as ForeignAddressInd
  var _addressStructureCode : int as AddressStructureCode
  var _street : String as Street
  var _city : String as City
  var _state : String as State
  var _zipCode : String as ZipCode
  var _nameLinkIdentifier : int as NameLinkIdentifier
  var _stateCodeLink : int as StateCodeLink
  var _exposureRecLinkForLocCode : int as ExposureRecLinkForLocCode
  var _futureReserved2 : String as FutureReserved2
  var _phoneNumOfInsrd : String as PhoneNumOfInsured
  var _numOfEmployees : String as NumOfEmployees
  var _industryCode : String as IndustryCode
  var _geographicArea : String as GeographicArea
  var _emailAddress : String as EmailAddress
  var _futureReserved3 : String as FutureReserved3
  var _countryCode : String as CountryCode
  var _nameLinkCounterIdntfr : String as NameLinkCounterIdntf
  var _futureReserved4 : String as FutureReserved4
  var _polChngEffDate : String as PolChngEffDate
  var _polChngExpDate: String as PolChngExpDate
}