package tdic.pc.common.batch.enrole.vo

/**
 * EnroleCourseVO contains enrole policy info felds for enrole outbound.
 * Created by RambabuN on 10/15/2019.
 */
class EnrolePolicyVO {

  var _policyNumber : String as PolicyNBR
  var _firstName : String as FirstName
  var _middleInitial : String as MiddleInitial
  var _lastName : String as LastName
  var _salutation : String as Salutation
  var _gender : String as Gender
  var _address1 : String as Address1
  var _address2 : String as Address2
  var _city : String as City
  var _state : String as State
  var _zip : String as Zip
  var _country : String as Country
  var _lastUpdated : Date as LastUpdated
  var _email : String as Email
  var _phone : String as Phone
  var _ext : String as Ext
  var _fax : String as Fax
  var _title : String as Title
  var _titleCode : String as TitleCode
  var _functCode : String as FunctCode
  var _firmSize : int as FirmSize
  var _industryCode : String as IndustryCode
  var _ada : String as ADA
  var _personType : String as PersonType
  var _policyIndicator : String as PolicyIndicator
  var _status : String as Status
  var _specType : String as SpecType
  var _componentState : String as ComponentState
  var _componentID : String as ComponentID
  var _compDesc : String as CompDesc
  var _renewDT : Date as RenewDT
  var _dscExpDT : Date as DscExpDT
  var _personID : String as PersonID
  var _source : String as Source
  var _semtekStatus : String as SemtekStatus
  var d3c9c3c5d5e2c5 : String as D3C9C3C5D5E2C5


}