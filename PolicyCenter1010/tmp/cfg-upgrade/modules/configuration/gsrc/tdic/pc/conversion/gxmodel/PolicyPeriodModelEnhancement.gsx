package tdic.pc.conversion.gxmodel

uses gw.api.database.Query
uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.util.ConversionUtility

enhancement PolicyPeriodModelEnhancement : tdic.pc.conversion.gxmodel.policyperiodmodel.types.complex.PolicyPeriod {

  function populatePolicyPeriod(period : PolicyPeriod, start : Date, end : Date) {
    SimpleValuePopulator.populate(this, period)
    period.PeriodStart = start
    period.PeriodEnd = end
    //period.LegacyPolicySource_TDIC = ConversionConstants.DEFAULT_LEGACYSOURCE
    period.LegacyPolicySource_TDIC = this.LegacyPolicySource_TDIC
    var product = period.Policy.Product
    if (this.Offering.Product.CodeIdentifier != null) {
      var offering = product.Offerings.firstWhere(\o -> o.CodeIdentifier == this.Offering.Product.CodeIdentifier)
      if (offering == null) {
        throw new IllegalArgumentException("Could not find offering with code ${this.Offering.Product.CodeIdentifier} for product ${product}")
      }
      period.Offering = offering
    }
    period.syncOffering()
    period.Policy.OriginalEffectiveDate = this.Policy.OriginalEffectiveDate
    period.EffectiveDatedFields.PolicyOrgType_TDIC = this.EffectiveDatedFields.PolicyOrgType_TDIC
    if(period.EffectiveDatedFields.PolicyOrgType_TDIC == PolicyOrgType_TDIC.TC_OTHER){
      period.Policy.Account.OtherOrgTypeDescription = this.Policy.Account.OtherOrgTypeDescription
    }
    var primNamedIns = this.PolicyContactRoles.Entry.firstWhere(\elt -> elt.Subtype == typekey.PolicyContactRole.TC_POLICYPRINAMEDINSURED)
    primNamedIns.$TypeInstance.populateContactRole(period)
    if(!this.PolicyContactRoles.Entry.Empty) {
      for (role in this.PolicyContactRoles.Entry.where(\elt -> elt.Subtype == typekey.PolicyContactRole.TC_POLICYENTITYOWNER_TDIC)) {
        role.$TypeInstance.populateContactRole(period)
      }
    }
    period.PrimaryNamedInsured.IndustryCode = Query.make(IndustryCode).compare(IndustryCode#Code, Equals, ConversionConstants.DEFAULT_INDUSTRYCODE).select().first()
    updateLocation(period)

    period.changePolicyAddressTo(period.PrimaryNamedInsured.AccountContactRole.AccountContact.Contact.PrimaryAddress)

    period.UWCompany = CommonConversionHelper.getUWCompany(this.UWCompany.Code)

    period.PreferredCoverageCurrency = (this.PreferredCoverageCurrency != null) ? Currency.get(this.PreferredCoverageCurrency as String) : period.Policy.Account.PreferredCoverageCurrency
    period.PreferredSettlementCurrency = (this.PreferredSettlementCurrency != null) ? Currency.get(this.PreferredSettlementCurrency as String) : period.Policy.Account.PreferredSettlementCurrency
   /* period.SelectedPaymentPlan = new PaymentPlanSummary()
    period.SelectedPaymentPlan.PaymentPlanType = PaymentMethod.get(this.SelectedPaymentPlan.PaymentPlanType.Code)
    period.SelectedPaymentPlan.Name = this.SelectedPaymentPlan.Name
    //period.SelectedPaymentPlan.BillingId =
    period.SelectedPaymentPlan.PolicyPeriod = period*/
    period.Status = PolicyPeriodStatus.TC_LEGACYCONVERSION
    period.syncPolicyTerm()
    populateLine(period)
    period.syncQuestions()
    period.SelectedPaymentPlan = new PaymentPlanSummary(period)
    period.SelectedPaymentPlan.PaymentPlanType = PaymentMethod.TC_INSTALLMENTS
    period.SelectedPaymentPlan.Name = this.SelectedPaymentPlan.Name
    period.SelectedPaymentPlan.BillingId = this.SelectedPaymentPlan.BillingId
    period.SelectedPaymentPlan.InvoiceFrequency = this.SelectedPaymentPlan.InvoiceFrequency
    period.SelectedPaymentPlan.PolicyPeriod = period
    period.BankAccountNumber_TDIC = period.BankAccountNumber_TDIC == null ? null : ConversionUtility.encrypt(period.BankAccountNumber_TDIC)
    for(note in this.Notes.Entry){
      note.$TypeInstance.populateNote(period)
    }
    /*var planSummary = new PaymentPlanSummary(period)
    planSummary.PaymentPlanType = PaymentMethod.TC_INSTALLMENTS
    period.SelectedPaymentPlan = planSummary*/
    /*for (modelNote in this.Notes.Entry){
      modelNote.$TypeInstance.populateNote(period)
    }*/
   /* if (period.SelectedPaymentPlan == null) {
      var paymentPlans = period.retrievePaymentPlans()
      final var defaultInstallmentsPlan = paymentPlans.InstallmentPlans.orderBy(\elt -> elt.DownPayment)
          .thenBy(\elt -> (elt as InstallmentPlanDataImpl).Priority_TDIC)
          .first();
      //_logger.logInfo ("Selecting " + defaultInstallmentsPlan.Name + " payment plan.");
      period.selectPaymentPlan(defaultInstallmentsPlan)
    }*/
  }

  private function updateLocation(period : PolicyPeriod) {

    //Delete default inherited account location from policy location
    period.PolicyLocations?.each(\elt -> {
      elt.remove()
    })
    var policyLocation : PolicyLocation
    var accountLocation : AccountLocation
    for (modelLocation in this.PolicyLocations.Entry?.orderBy(\elt -> elt.LocationNum)) {
      accountLocation = modelLocation.AccountLocation.$TypeInstance.findMatchedLocation(period.Policy.Account)
      if (accountLocation != null) {
        policyLocation = period.newLocation(accountLocation)
      } else {
        policyLocation = period.newLocation()
      }
      if(period.PrimaryLocation == null){
        period.setPrimaryLocation(policyLocation)
      }
      modelLocation.$TypeInstance.populatePolicyLocation(policyLocation)
    }
  }

  private function populateLine(period : PolicyPeriod) {
    switch (period.Offering.CodeIdentifier) {
      case ConversionConstants.OfferingCodeIdentifier.BOP.Type:
        this.BOPLine.$TypeInstance.populateBOPLineModel(period)
        break
      case ConversionConstants.OfferingCodeIdentifier.LRP.Type:
        this.BOPLine.$TypeInstance.populateBOPLineModel(period)
        break
      case ConversionConstants.OfferingCodeIdentifier.CYB.Type:
        this.GLLine.$TypeInstance.populateGLLineModel(period)
        break
      case ConversionConstants.OfferingCodeIdentifier.PL_CM.Type:
        this.GLLine.$TypeInstance.populateGLLineModel(period)
        break
      case ConversionConstants.OfferingCodeIdentifier.PL_OCC.Type:
        this.GLLine.$TypeInstance.populateGLLineModel(period)
        break

    }


  }
}

