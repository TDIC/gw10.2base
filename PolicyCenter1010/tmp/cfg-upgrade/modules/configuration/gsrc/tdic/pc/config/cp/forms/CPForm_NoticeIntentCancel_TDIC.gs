package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 4/9/2020
 * Create Time: 12:49 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_NoticeIntentCancel_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Job typeis Cancellation) {
      return (context.Period.Job.CancelReasonCode == ReasonCode.TC_NONPAYMENT ||
          context.Period.Job.CancelReasonCode == ReasonCode.TC_NOTTAKEN)
    }
   return false
  }

}