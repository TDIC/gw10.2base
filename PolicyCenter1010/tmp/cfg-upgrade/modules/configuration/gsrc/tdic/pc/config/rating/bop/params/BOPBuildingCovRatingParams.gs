package tdic.pc.config.rating.bop.params

uses entity.BOPBuildingCov
uses gw.api.productmodel.CovTermPack
uses gw.api.upgrade.PCCoercions

uses java.math.BigDecimal

/**
 * Rating Param it will populate params of coverages
 * required in rating algorithm
 *
 * @author- Ankita Gupta
 * 06/20/2019
 * .
 */
class BOPBuildingCovRatingParams extends BOPBuildingRatingParams {

  var _personalProplimit : BigDecimal as PersonalPropLimit
  var _buildingLimit : BigDecimal as BuildingLimit
  var _coverage : Coverage as Coverage
  var _moneyAndSecurityLimit : BigDecimal as MoneyAndSecurityLimit
  var _lossOfIncomeLimit : String as LossOfincomeLimit
  var _empDishonestyLimit : BigDecimal as EmpDishonestyLimit
  var _goldPrecMetalLimit : BigDecimal as GoldPreciousMaterialLimit
  var _extraExpenseLimit : BigDecimal as ExtraExpenseLimit
  var _valuablePapersLimit : BigDecimal as ValuablePapersLimit
  var _signsLimit : BigDecimal as SignsLimit
  var _signsIncreaseLimit : BigDecimal as SignsIncreaseLimit
  var _signsIncludedLimit : BigDecimal as SignsIncludedLimit
  var _accountRecievablesLimit : BigDecimal as AccountRecievablesLimit
  var _accountReceivablesIncreaseLimit : BigDecimal as AccountReceivablesIncreaseLimit
  var _accountReceivablesIncludedLimit : BigDecimal as AccountReceivablesIncludedLimit
  var _fineArtsLimit : BigDecimal as FineArtsLimit
  var _fineArtsIncreaseLimit : BigDecimal as FineArtsIncreaseLimit
  var _fineArtsIncludedLimit : BigDecimal as FineArtsIncludedLimit
  var _eqBuildingClass : String as EQBuildingClass
  var _eqZone : String as EQZone
  var _eqSLBuildingClass : String as EQSLBuildingClass
  var _eqSLZone : String as EQSLZone
  var _fungiLimit : BigDecimal as FungiLimit
  var _buldgOwnerLiabLimit : String as BuldgOwnerLiabLimit
  var _dentalEquipmentLimit : String as DentalEquipmentLimit
  var _equipmentBreakDownLimit : BigDecimal as EquipmentBreakDownLimit
  var _occupancyStatus : String as OccupancyStatus
  var _multiLineDiscountPrem : BigDecimal as MultiLineDiscountPrem
  var _closedEndWaterCreditPrem : BigDecimal as ClosedEndWaterCreditPrem
  var _IRPMPropertyPrem : BigDecimal as IRPMPropertyPrem
  var _IRPMLiabilityPrem : BigDecimal as IRPMLiabilityPrem
  var _fireFighterReliefSurchargePrem : BigDecimal as FireFighterReliefSurPrem
  var _IGASurchargePrem  : BigDecimal as IGASurchargePrem
  var _fireSafetySurchargePrem : BigDecimal as FireSafetySurchargePrem
  var _minimumAdjustmentPrem : BigDecimal as MinimumAdjustmentPrem

  construct(cov : BOPBuildingCov) {
    populateMandatoryCoverageParams(cov)
    switch (typeof cov) {
      case productmodel.BOPBuildingCov:
        _buildingLimit = cov.BOPBldgLimTerm.Value
        break
      case productmodel.BOPPersonalPropCov:
        _personalProplimit = cov.BOPBPPBldgLimTerm.Value
        break
      case BOPMoneySecCov_TDIC:
        _moneyAndSecurityLimit = cov.BOPMoneySecTotIncLimit_TDICTerm.Value != null ? (cov.BOPMoneySecTotIncLimit_TDICTerm.Value - cov.BOPMoneySecInclBasicLimit_TDICTerm.Value) : 0
        break
      case BOPLossofIncomeTDIC:
        _lossOfIncomeLimit = cov.BOPTotalIncLimit_TDICTerm?.OptionValue?.DisplayName
        break
      case BOPEmpDisCov_TDIC:
        _empDishonestyLimit = cov.BOPEDTotInclLimit_TDICTerm?.Value != null ? (cov.BOPEDTotInclLimit_TDICTerm?.Value): 0
        break
      case BOPGoldPreMetals_TDIC:
        _goldPrecMetalLimit = cov.BOPGoldTotalIncLimit_TDICTerm.Value != null ? (cov.BOPGoldTotalIncLimit_TDICTerm.Value - cov.BOPGoldIncBasicLimit_TDICTerm.Value) : 0
        break
      case BOPValuablePapersCov_TDIC:
        _valuablePapersLimit = cov.BOPValTotIncLimit_TDICTerm.Value != null ? (cov.BOPValTotIncLimit_TDICTerm.Value - cov.BOPValIncBasicLimit_TDICTerm.Value) : 0
        break
      case BOPExtraExpenseCov_TDIC:
        _extraExpenseLimit = cov.BOPExtraExpTotalIncLimit_TDICTerm.Value != null ? (cov.BOPExtraExpTotalIncLimit_TDICTerm.Value - cov.BOPExtraExpIncBasicLimit_TDICTerm.Value) : 0
        break
      case BOPSigns_TDIC:
        _signsLimit = cov.BOPTotalInLimitSigns_TDICTerm.Value != null ? (cov.BOPTotalInLimitSigns_TDICTerm.Value - cov.BOPIncBasicLimitSigns_TDICTerm.Value) : 0
        _signsIncreaseLimit = cov.BOPTotalInLimitSigns_TDICTerm.Value != null ? cov.BOPTotalInLimitSigns_TDICTerm.Value : 0
        _signsIncludedLimit = cov.BOPIncBasicLimitSigns_TDICTerm.Value != null ? cov.BOPIncBasicLimitSigns_TDICTerm.Value : 0
        break
      case BOPAccReceivablesCov_TDIC:
        _accountRecievablesLimit = cov.BOPACcTotalIncLimit_TDICTerm.Value != null ? (cov.BOPACcTotalIncLimit_TDICTerm.Value - cov.BOPAccIncBasicLimit_TDICTerm.Value) : 0
        _accountReceivablesIncreaseLimit = cov.BOPACcTotalIncLimit_TDICTerm.Value != null ? cov.BOPACcTotalIncLimit_TDICTerm.Value : 0
        _accountReceivablesIncludedLimit = cov.BOPAccIncBasicLimit_TDICTerm.Value != null ? cov.BOPAccIncBasicLimit_TDICTerm.Value : 0
        break
      case BOPFineArtsCov_TDIC:
        _fineArtsLimit = cov.BOPFineArtsTotIncLimit_TDICTerm.Value != null ? (cov.BOPFineArtsTotIncLimit_TDICTerm.Value - cov.BOPFineArtsIncBasicLimit_TDICTerm.Value) : 0
        _fineArtsIncreaseLimit = cov.BOPFineArtsTotIncLimit_TDICTerm.Value != null ? cov.BOPFineArtsTotIncLimit_TDICTerm.Value : 0
        _fineArtsIncludedLimit = cov.BOPFineArtsIncBasicLimit_TDICTerm.Value != null ? cov.BOPFineArtsIncBasicLimit_TDICTerm.Value : 0
        break
      case BOPEqBldgCov:
        _eqBuildingClass = cov.BOPEQBldgClass_TDICTerm.DisplayValue
        _eqZone = cov.BOPEQZone_TDICTerm.DisplayValue
        break
      case BOPEqSpBldgCov:
        _eqSLBuildingClass = cov.BOPEQSLBldgClass_TDICTerm.DisplayValue
        _eqSLZone = cov.BOPEQSLZone_TDICTerm.DisplayValue
        break
      case BOPFungiCov_TDIC:
        _fungiLimit = cov.BOPFungiTotIncLimit_TDICTerm.Value != null ? cov.BOPFungiTotIncLimit_TDICTerm.Value : 0
        break
      case BOPBuildingOwnersLiabCov_TDIC:
        _buldgOwnerLiabLimit = getLimit(cov)
            _occupancyStatus = cov.BOPBuilding.Building.BOPOccupancyStatus_TDIC.Code
        break
      case BOPDentalGenLiabilityCov_TDIC:
        _dentalEquipmentLimit = getLimit(cov)
        _occupancyStatus = cov.BOPBuilding.Building.BOPOccupancyStatus_TDIC.Code

    }
  }

  /**
   * Function to populate any field other than rating coverage which is required in routine
   *
   * @param cov
   */
  public function populateMandatoryCoverageParams(cov : BOPBuildingCov) {
    _buildingLimit = cov.BOPBuilding.BOPBuildingCov.BOPBldgLimTerm.Value
    _personalProplimit =  cov.BOPBuilding.BOPPersonalPropCov.BOPBPPBldgLimTerm.Value
  }

  private static var limitValue = new HashMap<String,String>() {
      "1000/2000" -> "1M/2M",
      "2000/4000" ->"2M/4M"
  }
  /**
   * Function to populate Limit value which is required in routine
   *
   * @param cov
 */
  static function getLimit(cov : BOPBuildingCov) : String {
    var limit : String
    switch (typeof cov){
      case BOPDentalGenLiabilityCov_TDIC:
        var displayVal = cov.BOPGLLimits_TDICTerm.PackageValue.PackageCode
        limit = limitValue.get(displayVal.toString())
        break
      case BOPBuildingOwnersLiabCov_TDIC:
        var displayVal = cov.BOPBOLiabLimits_TDICTerm.PackageValue.PackageCode
        limit = limitValue.get(displayVal.toString())
        break
    }
    return  limit
  }
}