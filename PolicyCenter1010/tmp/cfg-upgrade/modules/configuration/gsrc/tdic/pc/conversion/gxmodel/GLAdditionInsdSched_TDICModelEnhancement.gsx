package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator
uses tdic.pc.conversion.gxmodel.gladditioninsdsched_tdicmodel.types.complex.GLAdditionInsdSched_TDIC
uses tdic.pc.conversion.util.ConversionUtility

enhancement GLAdditionInsdSched_TDICModelEnhancement : GLAdditionInsdSched_TDIC {

  function populateGLAdditionInsdSched_TDIC(glLine : GLLine) {
    var sched = glLine.createAndAddPLAISched_TDIC()
    SimpleValuePopulator.populate(this, sched)
    var policyAddlInsDetail : PolicyAddlInsuredDetail
    if (glLine.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.hasMatch(\polAddlIns -> polAddlIns.PolicyAddlInsured.AccountContactRole.AccountContact.Contact.Name == this.AdditionalInsured.PolicyAddlInsured.AccountContactRole.AccountContact.Contact.Name_elem.$Value)) {
      policyAddlInsDetail = glLine.AdditionalInsureds*.PolicyAdditionalInsuredDetails?.firstWhere(\polAddlIns -> polAddlIns.PolicyAddlInsured.AccountContactRole.AccountContact.Contact.Name == this.AdditionalInsured.PolicyAddlInsured.AccountContactRole.AccountContact.Contact.Name_elem.$Value)
    } else {
      policyAddlInsDetail = glLine.addNewAdditionalInsuredDetailOfContactType(ConversionUtility.
          getContactType(this.AdditionalInsured.PolicyAddlInsured.AccountContactRole.AccountContact.Contact.Subtype))
      this.AdditionalInsured.PolicyAddlInsured.AccountContactRole.AccountContact.
          Contact.$TypeInstance.populateContact(policyAddlInsDetail.PolicyAddlInsured.AccountContactRole.AccountContact.Contact)
      this.AdditionalInsured.AdditionalInsuredType = this.AdditionalInsured.AdditionalInsuredType
      policyAddlInsDetail.Desciption = this.AdditionalInsured.Desciption
    }
    sched.AdditionalInsured = policyAddlInsDetail
  }
}
