package tdic.pc.integ.plugins.wcpols.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/30/14
 * Time: 12:03 PM
 * This class includes variables for all fields in exposure record for policy specification report.
 */
class TDIC_WCpolsExposureFields extends TDIC_FlatFileLineGenerator {
  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _stateCode : int as StateCode
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _classificationCode : String as ClassificationCode
  var _classificationUseCode : String as ClassificationUseCode
  var _futureReserved2 : String as FutureReserved2
  var _classificationWordingSuffix : String as ClassificationWordingSuffix
  var _exposureActOrExposureCovCode : int as ExposureActOrExposureCovCode
  var _manualOrChargedRate : String as ManualOrChargedRate
  var _exposurePeriodEffDate : String as ExposurePeriodEffDate
  var _futureReserved3 : String as FutureReserved3
  var _estimatedExposureAmount : long as EstimatedExposureAmount
  var _estimatedPremiumAmount : String as EstimatedPremiumAmount
  var _exposurePeriodCode : int as ExposurePeriodCode
  var _classificationWording : String as ClassificationWording
  var _futureReserved4 : String as FutureReserved4
  var _nameLinkIdentifier : int as NameLinkIdentifier
  var _stateCodeLink : int as StateCodeLink
  var _exposureRecordLinkForExposureCode : int as ExposureRecordLinkForExposureCode
  var _nameLinkCounterIdentifier : String as NameLinkCounterIdentifier
  var _futureReserved5 : String as FutureReserved5
  var _numberOfPiecesOfApparatus : String as NumberOfPiecesOfApparatus
  var _numberOfVolunteers : String as NumberOfVolunteers
  var _policySurchargeFactor : String as PolicySurchargeFactor
  var _planPremiumAdjustmentFactor : String as PlanPremiumAdjustmentFactor
  var _futureReserved6 : String as FutureReserved6
  var _polChngEffDate : String as PolChngEffDate
  var _polChngExpDate : String as PolChngExpDate
}