package tdic.pc.config.rating.pl.rater

uses gw.lob.gl.rating.GLCovCostData_TDIC
uses gw.lob.gl.rating.GLStateCostData
uses tdic.pc.config.rating.RatingConstants
uses tdic.pc.config.rating.pl.GLRatingEngine

class GLCyberLiabilityRater implements GLRater {

  private var _ratingEngine : GLRatingEngine as RatingEngine
  private var _line : GLLine as Line
  private var _branch : PolicyPeriod as Branch

  private construct() { /*do nothing*/ }

  construct(line : GLLine, ratingEngine : GLRatingEngine) {
    _ratingEngine = ratingEngine
    _line = line
    _branch = _ratingEngine.Branch
  }

  override function rate() {
    rateCyberCoverages()
    if (Line.Branch.BaseState == TC_NJ) {
      rateCyberIGASurcharge()
    }
  }

  /*
 * create by: AnkitaG
 * @description: Function to initialize Step Year
 * @create time:  10/24/2019
  * @param null
 * @return:
 */
  function rateCyberCoverages() {
    var covs = Line.GLLineCoverages.where(\elt -> elt typeis GLCyberLiabCov_TDIC or elt typeis GLCyvSupplementalERECov_TDIC)
    for (cov in covs) {
      var routine : String
      switch (typeof cov) {
        case GLCyberLiabCov_TDIC:
          routine = RatingConstants.plCyberLiabilityRoutine
          break
        case GLCyvSupplementalERECov_TDIC:
          routine = RatingConstants.plCyberERELiabilityRoutine
          break
      }
      if (routine != null) {
        var map : Map<CalcRoutineParamName, Object> = {
            TC_POLICYLINE -> Line,
            TC_RATINGINFO -> RatingEngine.RatingInfo
        }
        var cost = new GLCovCostData_TDIC(cov.SliceDate, RatingEngine.getNextSliceDate(cov.SliceDate), cov.Currency, RatingEngine.RateCache,
                        cov.GLLine.BaseState, cov.FixedId, false, null, null)
        cost.init(Line)
        if (cov typeis GLCyvSupplementalERECov_TDIC) {
          cost.ChargePattern = ChargePattern.TC_EREPREMIUM_TDIC
          cost.ProrationMethod = ProrationMethod.TC_FLAT
        }
        RatingEngine.execute(routine, map, cost)
        RatingEngine.CostCache.put(cov.PatternCode, cost)
      }
    }
  }

  private function rateCyberIGASurcharge() {
    RatingEngine.RatingInfo.IGASurchage = RatingEngine.CostCache.Values*.ActualTermAmount.sum()
    var cost = new GLStateCostData(Line.SliceDate, RatingEngine.getNextSliceDate(Line.SliceDate), Line.PreferredCoverageCurrency, RatingEngine.RateCache, Line.Branch.BaseState,
        GLStateCostType.TC_NJIGASURCHARGE_TDIC, null, null)
    var map : Map<CalcRoutineParamName, Object> = {
        TC_POLICYLINE -> Line,
        TC_RATINGINFO -> RatingEngine.RatingInfo
    }
    cost.init(Line)
    RatingEngine.execute(RatingConstants.plCyberIGARoutine, map, cost)
    cost.ChargePattern = ChargePattern.TC_NJIGASURCHARGE_TDIC
  }

}