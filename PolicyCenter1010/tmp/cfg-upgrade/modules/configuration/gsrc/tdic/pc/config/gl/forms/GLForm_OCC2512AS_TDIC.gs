package tdic.pc.config.gl.forms

uses entity.FormAssociation
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_OCC2512AS_TDIC  extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLOccurence_TDIC") {
      if (context.Period.GLLine?.GLManuscriptEndor_TDICExists &&
          context.Period.GLLine.GLManuscript_TDIC.where(\elt -> elt.ManuscriptType==GLManuscriptType_TDIC.TC_EVENTSCHEDULE).Count > 0)
      return true
    }
    return false
  }
}