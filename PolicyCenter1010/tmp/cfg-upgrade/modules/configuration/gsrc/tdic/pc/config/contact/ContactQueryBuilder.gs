package tdic.pc.config.contact

uses gw.address.AddressQueryBuilder
uses gw.api.database.ISelectQueryBuilder
uses gw.api.database.InOperation
uses gw.api.util.PhoneUtil
uses gw.search.EntityQueryBuilder
uses gw.search.StringColumnRestrictor
uses java.lang.IllegalStateException
uses gw.lang.reflect.features.PropertyReference
uses gw.contact.OfficialIDQueryBuilder
uses gw.api.locale.DisplayKey
uses tdic.web.pcf.helper.ProducerContactSearchScreenHelper

/**
 * Created with IntelliJ IDEA.
 * User: JohnL
 * Date: 9/25/14
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
class ContactQueryBuilder extends EntityQueryBuilder<Contact> {

  var _firstName : String
  var _firstNameRestrictor : StringColumnRestrictor
  var _lastName : String
  var _lastNameRestrictor : StringColumnRestrictor
  var _personNameRestrictor : PersonNameRestrictor = FirstAndLast
  var _companyName : String
  var _companyNameRestrictor : StringColumnRestrictor

  var _firstNameKanji : String
  var _firstNameKanjiRestrictor : StringColumnRestrictor
  var _lastNameKanji : String
  var _lastNameKanjiRestrictor : StringColumnRestrictor
  var _companyNameKanji : String
  var _companyNameKanjiRestrictor : StringColumnRestrictor
  var _particle: String
  var _particleRestrictor : StringColumnRestrictor

  var _workPhone : String
  var _taxId : String
  var _officialId: String

  var _cityDenorm : String
  var _cityDenormRestrictor : StringColumnRestrictor
  var _cityKanjiDenorm : String
  var _cityKanjiDenormRestrictor : StringColumnRestrictor
  var _stateDenorm : State
  var _postalCodeDenorm : String
  var _postalCodeDenormRestrictor : StringColumnRestrictor
  var _country : Country
  var _primaryAddress : AddressQueryBuilder

  var _contactType : ContactType

  // Property
  function contactType(value : ContactType) : ContactType {
    _contactType = value
    return value
  }

  // First Name Compare
  function withFirstName(value : String) : ContactQueryBuilder {
    return withFirstNameRestricted(value, Equals)
  }

  // First Name Starting Ignore Case
  function withFirstNameStarting(value : String) : ContactQueryBuilder {
    return withFirstNameRestricted(value, StartsWithIgnoringCase)
  }

  // First Name Contains
  function withFirstNameContains(value : String) : ContactQueryBuilder {
    return withFirstNameRestricted(value, ContainsIgnoringCase)
  }

  // First Name Restricted - Used to set the restrictor for the Contact Query Builder
  function withFirstNameRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _firstName = value
    _firstNameRestrictor = restrictor
    return this
  }

  // Last Name Compare
  function withLastName(value : String) : ContactQueryBuilder {
    return withLastNameRestricted(value, Equals)
  }

  // Last Name Staring With Ignore Case
  function withLastNameStarting(value : String) : ContactQueryBuilder {
    return withLastNameRestricted(value, StartsWithIgnoringCase)
  }

  // Last Name Contains
  function withLastNameContains(value : String) : ContactQueryBuilder {
    return withLastNameRestricted(value, ContainsIgnoringCase)
  }

  // Last Name Restricted - Used to set the restrictor for the Contact Query Builder
  function withLastNameRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _lastName = value
    _lastNameRestrictor = restrictor
    return this
  }

  //
  function withPersonNameRelationship(value : PersonNameRestrictor) : ContactQueryBuilder {
    _personNameRestrictor = value
    return this
  }

  // Company Compare
  function withCompanyName(value : String) : ContactQueryBuilder {
    return withCompanyNameRestricted(value, Equals)
  }

  // Company Starting with Ignore Case
  function withCompanyNameStarting(value : String) : ContactQueryBuilder {
    return withCompanyNameRestricted(value, StartsWithIgnoringCase)
  }

  // Company Contains with Ignore Case
  function withCompanyNameContains(value : String) : ContactQueryBuilder {
    return withCompanyNameRestricted(value, ContainsIgnoringCase)
  }

  // Company Restricted - used to set the restrictore for the Contact Query Builder
  function withCompanyNameRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _companyName = value
    _companyNameRestrictor = restrictor
    return this
  }

  //
  function withFirstNameKanji(value : String) : ContactQueryBuilder {
    withFirstNameKanjiRestricited(value, StartsWith)
    return this
  }

  //
  function withFirstNameKanjiRestricited(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _firstNameKanji = value
    _firstNameKanjiRestrictor = restrictor
    return this
  }

  //
  function withLastNameKanji(value : String) : ContactQueryBuilder {
    withLastNameKanjiRestricted(value, StartsWith)
    return this
  }

  //
  function withLastNameKanjiRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _lastNameKanji = value
    _lastNameKanjiRestrictor = restrictor
    return this
  }

  //
  function withCompanyNameKanji(value : String) : ContactQueryBuilder {
    withCompanyNameKanjiRestricted(value, StartsWith)
    return this
  }

  //
  function withCompanyNameKanjiRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _companyNameKanji = value
    _companyNameKanjiRestrictor = restrictor
    return this
  }

  //
  function withParticleRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _particle = value
    _particleRestrictor = restrictor
    return this
  }

  //
  function withParticle(value : String) : ContactQueryBuilder {
    return withParticleRestricted(value, StartsWithIgnoringCase)
  }

  // Work Number
  function withWorkPhone(value : String) : ContactQueryBuilder {
    _workPhone = value
    return this
  }

  // Tax ID
  function withTaxId(value : String) : ContactQueryBuilder {
    _taxId = value
    return this
  }

  // Official ID
  function withOfficialId(value : String) : ContactQueryBuilder {
    _officialId = value
    return this
  }

  // City Demoralize
  function withCityDenormStarting(value : String) : ContactQueryBuilder {
    withCityDenormRestricted(value, StartsWithIgnoringCase)
    return this
  }

  // City Demoralize Restrictor
  function withCityDenormRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _cityDenorm = value
    _cityDenormRestrictor = restrictor
    return this
  }

  //
  function withCityKanjiDenormStarting(value : String) : ContactQueryBuilder {
    withCityKanjiDenormRestricted(value, StartsWith)
    return this
  }

  //
  function withCityKanjiDenormRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _cityKanjiDenorm = value
    _cityKanjiDenormRestrictor = restrictor
    return this
  }

  // Country Demoralize
  function withCountryDenorm(value : Country) : ContactQueryBuilder {
    _country = value
    return this
  }

  // Postal Code Demoralize
  function withPostalCodeDenormStarting(value : String) : ContactQueryBuilder {
    withPostalCodeDenormRestricted(value, StartsWith)
    return this
  }

  // Postal Code Demoralize Restrictor
  function withPostalCodeDenormRestricted(value : String, restrictor : StringColumnRestrictor) : ContactQueryBuilder {
    _postalCodeDenorm = value
    _postalCodeDenormRestrictor = restrictor
    return this
  }

  // State Demoralize
  function withStateDenorm(value : State) : ContactQueryBuilder {
    _stateDenorm = value
    return this
  }

  //
  function withPrimaryAddress(value : AddressQueryBuilder) : ContactQueryBuilder {
    _primaryAddress = value
    return this
  }

  override function doRestrictQuery(selectQueryBuilder : ISelectQueryBuilder) {
    if (HasPersonName and HasCompanyName) {
      throw new IllegalStateException(DisplayKey.get("ContactQueryBuilder.Error.PersonAndCompanyNameCriteria"))
    } else if (HasPersonName) {
      selectQueryBuilder.cast(Person)
      _personNameRestrictor.restrict(selectQueryBuilder, _firstNameRestrictor, _firstName, _lastNameRestrictor, _lastName)
    } else if (HasCompanyName) {

      selectQueryBuilder.cast(Company)
      if (this._companyName == null) {
        this._companyName = ""
      }

      _companyNameRestrictor.restrict(selectQueryBuilder, Company#Name, _companyName)
    }

    if (_firstNameKanji.NotBlank) {
      selectQueryBuilder.cast(Person)
      _firstNameKanjiRestrictor.restrict(selectQueryBuilder, Person#FirstNameKanji, _firstNameKanji)
    }
    if (_lastNameKanji.NotBlank) {
      selectQueryBuilder.cast(Person)
      _lastNameKanjiRestrictor.restrict(selectQueryBuilder, Person#LastNameKanji, _lastNameKanji)
    }
    if (_companyNameKanji.NotBlank) {
      selectQueryBuilder.cast(Company)
      _companyNameKanjiRestrictor.restrict(selectQueryBuilder, Company#NameKanji, _companyNameKanji)
    }
    if (_particle.NotBlank) {
      selectQueryBuilder.cast(Person)
      _particleRestrictor.restrict(selectQueryBuilder, Person#Particle, _particle)
    }
    if (_workPhone.NotBlank) {
      var country = PhoneUtil.getDefaultPhoneCountryCode()
      var gwPhone = PhoneUtil.parse(_workPhone, country);
      if (gwPhone != null) {
        selectQueryBuilder.compare(Contact#WorkPhone.PropertyInfo.Name, Equals, gwPhone.NationalNumber)
      } else {
        throw new gw.api.util.DisplayableException(DisplayKey.get("Java.PhoneUtil.Error.ParseError", _workPhone))
      }
    }

    // Amended TaxID -- SSN/FEIN filed mapping as part of GW-1623 & GW-753
    if (_taxId.NotBlank) {
      selectQueryBuilder.compare(Contact#TaxID.PropertyInfo.Name, Equals, _taxId)
    }

    // Krishna - GW-414 - Ensure correct subtype tis picked up in search
    if (HasPersonName) {
      selectQueryBuilder.compare(Contact#Subtype, NotEquals, typekey.Contact.TC_USERCONTACT)
    }
    if (_officialId.NotBlank) {
      var officialIdTable = selectQueryBuilder.join(entity.OfficialID, OfficialID#Contact.PropertyInfo.Name)
      new OfficialIDQueryBuilder().withValue(_officialId).withType(OfficialIDType.TC_ADANUMBER_TDIC).restrictQuery(officialIdTable)
    }

    if (_cityDenorm.NotBlank) {
      _cityDenormRestrictor.restrict(selectQueryBuilder, Contact#CityDenorm, _cityDenorm)
    }

    var haveCityKanjiDenorm = Contact.Type.TypeInfo.getProperty("CityKanjiDenorm") != null
    if (haveCityKanjiDenorm and _cityKanjiDenorm.NotBlank) {
      _cityKanjiDenormRestrictor.restrict(selectQueryBuilder, new PropertyReference(Contact, "CityKanjiDenorm"), _cityKanjiDenorm)
    }

    // Jeff Lin, 4/3/2020, 2:35 PM, GPC-3723: User Contact Search - Able to pull up other agent clients (Should search/retrieve user's own clients
    if (User.util.CurrentUser.ExternalUser) {
      var stateTypeKeys = ProducerContactSearchScreenHelper.findPrducerOrganizationStateTypeKeys()
      selectQueryBuilder.compareIn(Contact#State.PropertyInfo.Name, stateTypeKeys.toTypedArray())
    } else if (_stateDenorm != null) {
      selectQueryBuilder.compare(Contact#State.PropertyInfo.Name, Equals, _stateDenorm)
    }

    if (_postalCodeDenorm.NotBlank) {
      _postalCodeDenormRestrictor.restrict(selectQueryBuilder, Contact#PostalCodeDenorm, _postalCodeDenorm)
    }
    if (_country != null) {
      selectQueryBuilder.compare(Contact#Country.PropertyInfo.Name, Equals, _country)
    }

    if (_primaryAddress != null) {
      var addressTable = selectQueryBuilder.subselect(Contact#PrimaryAddress, InOperation.CompareIn, Address#ID)
      _primaryAddress.restrictQuery(addressTable)
    }
  }

  protected property get HasPersonName() : boolean {
    if (_contactType == ContactType.TC_PERSON) {
      return true
    }

    return false
    //return _firstName.NotBlank or _lastName.NotBlank
  }

  protected property get HasCompanyName() : boolean {
    if (_contactType == ContactType.TC_COMPANY) {
      return true
    }

    return false
    //return _companyName.NotBlank
  }

  /**
   * Enum defining restrictions we can apply to the first and last name.  In addition to handling
   * 'and' or 'or', the restrictors allow the use of a StringColumnRestrictor for each of the
   * columns.
   */
  public static enum PersonNameRestrictor {
    FirstAndLast(\ selectQueryBuilder, firstNameRestrictor, firstName, lastNameRestrictor, lastName -> {
      if (firstName.NotBlank) {
        firstNameRestrictor.restrict(selectQueryBuilder, Person#FirstName, firstName)
      }
      if (lastName.NotBlank) {
        lastNameRestrictor.restrict(selectQueryBuilder, Person#LastName, lastName)
      }
    }),
    FirstOrLast(\ selectQueryBuilder, firstNameRestrictor, firstName, lastNameRestrictor, lastName -> {
      if (firstName.NotBlank and lastName.NotBlank) {
        selectQueryBuilder.or(\ restriction -> firstNameRestrictor.restrict(restriction, Person#FirstName, firstName))
        selectQueryBuilder.or(\ restriction -> lastNameRestrictor.restrict(restriction, Person#LastName, lastName))
      } else {
        // if one of the names isn't filled in, then there's no point in creating an OR join and since the
        // name restrictors won't apply a restriction for a blank name, just use the FirstAndLast restrictor
        FirstAndLast.restrict(selectQueryBuilder, firstNameRestrictor, firstName, lastNameRestrictor, lastName)
      }
    }),

    var _restrictor : block(selectQueryBuilder : ISelectQueryBuilder, firstNameRestrictor : StringColumnRestrictor, firstName : String, lastNameRestrictor : StringColumnRestrictor, lastName : String)

    private construct(restrictor : block(selectQueryBuilder : ISelectQueryBuilder, firstNameRestrictor : StringColumnRestrictor, firstName : String, lastNameRestrictor : StringColumnRestrictor, lastName : String)) {
      _restrictor = restrictor
    }

    function restrict(selectQueryBuilder : ISelectQueryBuilder, firstNameRestrictor : StringColumnRestrictor, firstName : String, lastNameRestrictor : StringColumnRestrictor, lastName : String) {
      _restrictor(selectQueryBuilder, firstNameRestrictor, firstName, lastNameRestrictor, lastName)
    }

  }
}