package tdic.pc.integ.plugins.wcpols.dto

uses java.util.ArrayList

uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsAddressFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm10Fields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCAApprovedForm11Fields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsCancellationFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsDataElementsChgEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsEndorsementIdentificationFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExpRatingModChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsExposureFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameChangeEndRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsNameFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPartnershipCovOrExclusionEndCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsPolicyHeaderFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReinstatementFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumChangeRecFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsStatePremiumFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields
uses tdic.util.flatfile.model.classes.Record
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/30/14
 * Time: 10:42 PM
 * This class includes variables for all record types in policy specification report.
 */
class TDIC_WCpolsRecordTypes{

  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCPOLSREPORT")

  var _policyHeaderRecord : TDIC_WCpolsPolicyHeaderFields as PolicyHeaderRecord
  var _nameRecord : ArrayList<TDIC_WCpolsNameFields> as NameRecord
  var _addressRecord : ArrayList<TDIC_WCpolsAddressFields> as AddressRecord
  var _statePremiumRecord : ArrayList<TDIC_WCpolsStatePremiumFields> as StatePremiumRecord
  var _exposureRecord : ArrayList<TDIC_WCpolsExposureFields> as ExposureRecord
  var _endorsementIdentificationRecord :  ArrayList<TDIC_WCpolsEndorsementIdentificationFields> as EndorsementIdentificationRecord
  var _cancellationRecord : TDIC_WCpolsCancellationFields as CancellationRecord
  var _reinstatementRecord : TDIC_WCpolsReinstatementFields as ReinstatementRecord
  var _partnershipCovOrExclusionEndCAReecord :ArrayList<TDIC_WCpolsPartnershipCovOrExclusionEndCAFields> as PartnershipCovOrExclusionEndCARecord
  var _officersAndDirectorsCovOrExcCARecord : ArrayList<TDIC_WCpolsOfficersAndDirectorsCovOrExcCAFields> as OfficersAndDirectorsCovOrExcCARecord
  var _volCompAndEmprsLiaCovEndCARecord : ArrayList<TDIC_WCpolsVolCompAndEmprsLiaCovEndCAFields> as VolCompAndEmprsLiaCovEndCARecord
  var _WaiOfOurRightToRecFromOthsEndCARecord : ArrayList<TDIC_WCpolsWaiOfOurRightToRecFromOthsEndCAFields> as WaiOfOurRightToRecFromOthsEndCARecord
  var _caApprovedForm10CARecord : ArrayList<TDIC_WCpolsCAApprovedForm10Fields> as CAApprovedForm10CARecord
  var _caApprovedForm11CARecord :ArrayList<TDIC_WCpolsCAApprovedForm11Fields> as CAApprovedForm11CARecord
  var _expRatingModChangeEndRecord : ArrayList<TDIC_WCpolsExpRatingModChangeEndRecFields> as ExpRatingModChangeEndRecord
  var _statePremiumChangeRecord : ArrayList<TDIC_WCpolsStatePremiumChangeRecFields> as StatePremiumChangeRecord
  var _classAndOrRateChgAndOthEndRecord : ArrayList<TDIC_WCpolsClassAndOrRateChgAndOthEndRecFields> as ClassAndOrRateChgAndOthEndRec
  var _dataElementsChangeEndorsementRecord : ArrayList<TDIC_WCpolsDataElementsChgEndRecFields> as DataElementsChangeEndRecord
  var _nameChangeEndorsementRecord : ArrayList<TDIC_WCpolsNameChangeEndRecFields> as NameChangeEndRecord
  var _addressChangeEndRecord : ArrayList<TDIC_WCpolsAddressChangeEndRecFields> as AddressChangeEndRecord


  function getCount():int{

    var count = 0
    if(this._policyHeaderRecord != null ) count++
    count += this._nameRecord.Count
    count += this._addressRecord.Count
    count += this._statePremiumRecord.Count
    count += this._exposureRecord.Count
    count += this._endorsementIdentificationRecord.Count
    if(this._cancellationRecord != null ) count++
    if(this._reinstatementRecord != null ) count++
    if(this._partnershipCovOrExclusionEndCAReecord != null) count+= this._partnershipCovOrExclusionEndCAReecord.Count
    if(this._officersAndDirectorsCovOrExcCARecord != null) count+= this._officersAndDirectorsCovOrExcCARecord.Count
    if(this._volCompAndEmprsLiaCovEndCARecord != null) count+= this._volCompAndEmprsLiaCovEndCARecord.Count
    if(this._WaiOfOurRightToRecFromOthsEndCARecord != null) count+= this._WaiOfOurRightToRecFromOthsEndCARecord.Count
    if(this._caApprovedForm10CARecord != null) count+= this._caApprovedForm10CARecord.Count
    if(this._caApprovedForm11CARecord != null) count+= this._caApprovedForm11CARecord.Count
    if(this._expRatingModChangeEndRecord != null) count+= this._expRatingModChangeEndRecord.Count
    if(this._statePremiumChangeRecord != null) count+= this._statePremiumChangeRecord.Count
    if(this._classAndOrRateChgAndOthEndRecord != null) count+= this._classAndOrRateChgAndOthEndRecord.Count
    if(this._dataElementsChangeEndorsementRecord != null) count+= this._dataElementsChangeEndorsementRecord.Count
    if(this._nameChangeEndorsementRecord != null)  count+= this._nameChangeEndorsementRecord.Count
    if(this._addressChangeEndRecord != null)  count+= this._addressChangeEndRecord.Count
    return count
  }

  function convertToFlatFileRecord(records:Record[]) :String{
    var flatFileContents=""

    for(record in records){
    _logger.debug("************************"+record.RecordType+"*****************************")
      switch (record.RecordType) {
        case "Policy Header Record":
            if(PolicyHeaderRecord != null)
              flatFileContents += PolicyHeaderRecord.createFlatFileLine(record)+"\r\n"
            break
        case "Name Record":
            NameRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Address Record":
            AddressRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "State Premium Record":
            StatePremiumRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Exposure Record":
            ExposureRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Endorsement Identification Record":
            EndorsementIdentificationRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Cancellation Record":
            if(CancellationRecord != null)
              flatFileContents += CancellationRecord.createFlatFileLine(record)+"\r\n"
            break
        case "Reinstatement Record":
            if(ReinstatementRecord != null)
              flatFileContents += ReinstatementRecord.createFlatFileLine(record)+"\r\n"
            break
        case "Partnership Coverage/Exclusion Endorsement - California Record":
            if(PartnershipCovOrExclusionEndCARecord != null)
              PartnershipCovOrExclusionEndCARecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Officers And Directors Coverage/Exclusion Endorsement - California Record":
          if(OfficersAndDirectorsCovOrExcCARecord != null)
            OfficersAndDirectorsCovOrExcCARecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Voluntary Compensation And Employers Liability Coverage Endorsement - California Record":
          if(VolCompAndEmprsLiaCovEndCARecord != null)
            VolCompAndEmprsLiaCovEndCARecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Waiver Of our Right To Recover From Others Endorsement - California Record":
          if(WaiOfOurRightToRecFromOthsEndCARecord != null)
            WaiOfOurRightToRecFromOthsEndCARecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Endorsement Agreement Limiting And restricting This Insurance - Form 10 California Record":
            if(CAApprovedForm10CARecord != null)
              CAApprovedForm10CARecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Endorsement Agreement Limiting And restricting This Insurance - Form 11 California Record":
            if(CAApprovedForm11CARecord != null)
              CAApprovedForm11CARecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Experience Rating Modification Change Endorsement Record":
            if(ExpRatingModChangeEndRecord != null)
              ExpRatingModChangeEndRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Policy Information Page State Premium Change Record":
            if(StatePremiumChangeRecord != null)
              StatePremiumChangeRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Policy Information Page Class And Or Rate Change And Other Endorsement Record":
            if(ClassAndOrRateChgAndOthEndRec != null)
              ClassAndOrRateChgAndOthEndRec.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Policy Information Page Data Elements Change Endorsement Record":
            if(DataElementsChangeEndRecord != null)
              DataElementsChangeEndRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Policy Information Page Name Change Endorsement Record":
            if(NameChangeEndRecord != null)
              NameChangeEndRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        case "Policy Information Page Address Change Endorsement Record":
            if(AddressChangeEndRecord != null)
            AddressChangeEndRecord.each( \ elt -> {flatFileContents += elt.createFlatFileLine(record)+"\r\n"})
            break
        default:
            break
    }

    }
    return flatFileContents

   }
}