package tdic.pc.conversion.dto

class BOPBuildingCovDTO {

  var choiceTerm : String as ChoiceTerm
  var directTerm : String as DirectTerm
  var covPatternCode : String as CovPatternCode
  var covTermPatternCode : String as CovTermPatternCode
  var locationID : String as LocationID
  var publicID : String as PublicID

}