package tdic.pc.config.wc7.forms

uses gw.forms.FormInferenceContext
uses gw.xml.XMLNode
uses java.util.Set

class WC7Form_WC_04_03_41 extends WC7Form_AlternateCoverage {
  @Param("context", "The context contains information such as the PolicyPeriod, the set of forms in the group, and the period diffs.")
  @Param("specialCaseStates", " The set of available states contains all states in which this form was found to be available")
  override function populateInferenceData( context : FormInferenceContext, specialCaseStates: Set<Jurisdiction> ) : void {
    _wc7Exclusion = context.Period.WC7Line.WC7DesignatedLocationsExcl_TDIC;
  }

  /**
   * Form contains a schedule of Operations and Alternate Coverages.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    super.addDataForComparisonOrExport(contentNode);

    var designatedLocationNode : XMLNode;
    var designatedLocationsNode = new XMLNode("DesignatedLocations");
    contentNode.addChild(designatedLocationsNode);

    for (operation in _wc7Exclusion.WCLine.WC7DesignatedLocationExclOperations_TDIC.orderBy(\o -> o.FixedId)) {
      designatedLocationNode = new XMLNode("Operation");
      designatedLocationNode.addChild(createTextNode("OperationName", operation.OperationName));
      designatedLocationNode.addChild(createTextNode("OperationTitle", operation.OperationTitle));
      designatedLocationNode.addChild(createTextNode("LocationAddress", operation.LocationAddress));
      designatedLocationsNode.addChild(designatedLocationNode);
    }
  }
}