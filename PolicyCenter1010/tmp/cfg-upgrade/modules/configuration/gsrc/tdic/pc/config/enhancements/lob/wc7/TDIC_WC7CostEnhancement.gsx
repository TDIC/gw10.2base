package tdic.pc.config.enhancements.lob.wc7

uses gw.api.locale.DisplayKey

enhancement TDIC_WC7CostEnhancement: entity.WC7Cost {

  /**
  * US669
  * 02/16/2015 Shane Murphy
   *
   * WC7Costs need to be grouped under headings such as Manual Premium, Standard Premium etc,
   * so that they can be displayed appropriately on the document
   */
  property get PremiumsGroup(): String {
    if (this.CalcOrder >= 0 and this.CalcOrder <= 100) {
      return DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.ManualPremium")
    } else {
      if (this.CalcOrder >= 0 && this.CalcOrder <= 200) {
        return DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.SubjectPremium")
      } else {
        if (this.CalcOrder >= 0 && this.CalcOrder <= 300) {
          return DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.ModifiedPremium")
        } else {
          if (this.CalcOrder >= 0 && this.CalcOrder <= 400) {
            return DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.StandardPremium")
          } else {
            if (this.CalcOrder >= 0 && this.CalcOrder <= 500) {
              return DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.TotalPremium", this.JurisdictionState)
            } else {
              if (this.CalcOrder >= 0 && this.CalcOrder <= 10000000) {
                return DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.TotalCost", this.JurisdictionState)
              } else {
                return "Unknown Group"
              }
            }
          }
        }
      }
    }
  }

  /**
   * GW141
   * 04/11/2017 Kesava Tavva
   *
   * WC7Cost needs associated Rating Period Number for Document purpose.
   */
  property get RatingPeriodNumber_TDIC() : int {
    return this.WC7WorkersCompLine.WC7Jurisdictions.firstWhere( \ j -> (j.Jurisdiction == this.JurisdictionState)==Boolean.TRUE)
            ?.RatingPeriods.firstWhere( \ rp -> ( (this.EffDate >= rp.RatingStart and this.EffDate <= rp.RatingEnd and this.ExpDate == rp.RatingEnd)
                                                      or(this.ExpDate >= rp.RatingStart and this.ExpDate <= rp.RatingEnd and this.EffDate == rp.RatingStart))==Boolean.TRUE)
            ?.Number
  }
}
