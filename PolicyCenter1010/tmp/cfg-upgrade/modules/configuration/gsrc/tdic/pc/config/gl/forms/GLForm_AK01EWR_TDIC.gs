package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 01/25/2020
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_AK01EWR_TDIC extends AbstractSimpleAvailabilityForm{
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" or context.Period.Offering.CodeIdentifier =="PLOccurence_TDIC"){
      if (context.Period.Job typeis Submission) {
        return (context.Period.GLLine.GLDentalEmpPracLiabCov_TDICExists and
            (context.Period.GLLine.GLPriorActsCov_TDICExists and
                context.Period.GLLine.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm?.Value != context.Period.PeriodStart))
      }
    }
    return false
  }
}