package tdic.pc.conversion.util

uses com.google.common.base.CaseFormat
uses gw.api.locale.DisplayKey
uses gw.api.productmodel.QuestionChoice
uses gw.api.productmodel.QuestionChoiceLookup
uses gw.api.system.PCLoggerCategory
uses gw.plugin.Plugins
uses gw.plugin.util.IEncryption
uses org.apache.commons.lang.StringUtils
uses org.apache.commons.lang.text.StrSubstitutor
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.exception.ConversionException
uses tdic.pc.conversion.exception.DataConversionException
uses tdic.pc.conversion.helper.CommonConversionHelper

abstract class ConversionUtility {

  static function getProductCode(offering : String) : ConversionConstants.ProductCode {
    var productCode = ConversionConstants.PRODUCTCODE_MAPPER.get(offering)
    if (productCode == null) {
      throw new ConversionException("Offering :" + offering + " not mapped to GW Product Code")
    }
    return productCode
  }

  static function prepareErrorMsg(message : String, args : Map) : String {
    if (args.Empty) {
      return message
    }
    var sub = new StrSubstitutor(args, "{", "}");
    return sub.replace(message);
  }

  static function getContactType(subtype : typekey.Contact) : typekey.ContactType {
    return subtype == typekey.Contact.TC_PERSON ? ContactType.TC_PERSON : ContactType.TC_COMPANY
  }

  static function formatFEIN(taxid : String) : String {
    var fein = taxid?.replace("-", "");
    if (fein.length > 8) {
      fein = fein?.substring(0, 2) + "-" + fein?.substring(2, fein.length)
    }
    return fein
  }

  static function formatSSN(taxid : String) : String {
    var ssn = taxid?.replace("-", "");
    if (ssn.length > 8) {
      ssn = ssn?.substring(0, 3) + "-" + ssn?.substring(3, 5) + "-" + ssn?.substring(5, ssn.length())
    }
    return ssn
  }

  static function parseDateFromString(dateString : String, parameterName : String) : Date {
    try {
      //return new Date(dateString)
      return CommonConversionHelper.parseDate(dateString)
    } catch (iae : IllegalArgumentException) {
      PCLoggerCategory.API.error(iae.getMessage(), iae)
      throw new DataConversionException(DisplayKey.get("PolicyRenewalAPI.Error.ParseError", parameterName, dateString))
    }
  }

  static function getChoiceAnswer(ans : String, q : String) : QuestionChoice {
    var choiceMap = ConversionConstants.CHOICEANSWER_MAPPER.get(q)
    var choiceCode = choiceMap == null ? null : choiceMap.get(ans)
    var qChoice : QuestionChoice
    try {
      qChoice = QuestionChoiceLookup.getByCodeIdentifier(choiceCode)
    }catch(e : Exception){
      LoggerUtil.CONVERSION_ERROR.error("ChoiceCode :" + choiceCode + "; " + ans + "; " + q)
      LoggerUtil.CONVERSION_ERROR.error(e.LocalizedMessage)
      throw e
    }
    return qChoice
  }

  static function getNoOfInstallments(periodicity : BillingPeriodicity) : Integer{
    switch(periodicity){
      case BillingPeriodicity.TC_EVERYYEAR : return 0
      case BillingPeriodicity.TC_EVERYSIXMONTHS : return 1
      case BillingPeriodicity.TC_MONTHLY : return 12
      default: throw new ConversionException("Invalid Payment Plan")
    }
  }

  static function encrypt(value : String): String {
    var encryptionPlugin= Plugins.get("EncryptionByAESPlugin")as IEncryption
    return encryptionPlugin.encrypt(value)
  }

  static function decrypt(value : String): String {
    var encryptionPlugin= Plugins.get("EncryptionByAESPlugin")as IEncryption
    return encryptionPlugin.decrypt(value)
  }

  static function formatPostalCode(postalCode : String) : String {
    if(postalCode == null) {
      return null
    }
    if(postalCode.matches("\\d{5}([ \\-]\\d{4})")){
      if(postalCode.split("-")[1].matches("[0]+")){
        return postalCode.split("-")[0]
      }
    }
    if(postalCode.matches("\\d{5}([ \\-]\\d{4})?")){
      return postalCode
    }
    if(postalCode.length < 5){
      return StringUtils.leftPad(postalCode, 5, "0")
    }
    if(postalCode.contains("-")){
      var sp = postalCode.split("-")
      if(sp.Count == 1 or sp[1].matches("[0]+")){
        return StringUtils.leftPad(sp[0], 5, "0")
      }
      return StringUtils.leftPad(sp[0], 5, "0")+"-"+ StringUtils.leftPad(sp[1], 4, "0")
    }
    var p1 = postalCode.substring(0,5)
    var p2 = postalCode.substring(5)
    if(p2 == null or p2.matches("[0]+")) {
      return p1
    }else{
      return p1+"-"+StringUtils.leftPad(p2, 4, "0")
    }
  }

  static function formatName(name : String) : String {
    if(name == null){
      return name
    }
    name = name+" "
    typekey.Credential_TDIC.AllTypeKeys.each(\elt -> {
      name.split(" ").each(\w -> {
        var t = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,elt.Code)
        if(t ==  w){
          name = name.replaceAll(t+" ", elt.Code+" ")
        }
        if(t.toLowerCase() ==  w){
          name = name.replaceAll(t.toLowerCase()+" ", elt.Code+" ")
        }
      })
    })
    return name.trim().replaceAll("\\s+", " ")
  }

  static function formatNamesGreaterThan64Chars(name : String, remaining : boolean) : String {
    if(name == null){
      return name
    }
    var words = name.split(" ")
    var sb = new StringBuilder("")
    var splitIndex=0
    for(word in words index i){
      if(ConversionConstants.PNI_NAME_CONTINUED.length + sb.toString().length + word.length + 1 >64 ){
        splitIndex = i
        break
      }
      sb.append(word).append(" ")
    }
    sb.append(ConversionConstants.PNI_NAME_CONTINUED)
    if(!remaining){
      return sb.toString()
    }
    var remainingName = new StringBuilder("")
    if(splitIndex > 0) {
      for (word in words index i) {
        if (i >= splitIndex) {
          remainingName.append(word).append(" ")
        }
      }
    }
    return remainingName.toString().trim()
  }

}