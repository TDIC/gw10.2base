package tdic.pc.conversion.exception

class DataConversionException extends Exception {

  var _message : String as Message

  construct(message : String) {
    super(message)
    _message = message
  }

  construct(exception : Exception) {
    super(exception)
    _message = exception.Message
  }
}