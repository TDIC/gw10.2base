package tdic.pc.common.batch.unistat.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator


/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 11/21/14
 * Time: 1:26 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCstatPolicyHeaderFields extends TDIC_FlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _exposureStateCode : int as ExposureStateCode
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _reportLevelCode : int as ReportLevelCode
  var _correctionSeqNum : String as CorrectionSeqNum
  var _recordTypeCode : int as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _polExpOrCancDate : String as PolExpOrCancDate
  var _riskIdNum : String as RiskIdNum
  var _futureReserved2 : String as FutureReserved2
  var _originalAdminNumIdentifier : String as OriginalAdminNumIdentifier
  var _futureReserved3 : String as FutureReserved3
  var _empLeasingCode : String as EmpLeasingCode
  var _futureReserved4 : String as FutureReserved4
  var _replacementReportCode : String as ReplacementReportCode
  var _businessSegmentIdentifier : String as BusinessSegmentIdentifier
  var _thirdPartyEntityFeinNum : String as ThirdPartyEntityFeinNum
  var _correctionTypeCode : String as CorrectionTypeCode
  var _stateEffectiveDate : String as StateEffectiveDate
  var _feinNumber : String as FeinNumber
  var _futureReserved5 : String as FutureReserved5
  var _threeYearFixedRatePolInd : String as ThreeYearFixedRatePolInd
  var _multiStatePolInd : String as MultiStatePolInd
  var _interStateRatedPolInd : String as InterStateRatedPolInd
  var _estimatedAuditCode : String as EstimatedAuditCode
  var _retrospectiveRatedPolInd : String as RetrospectivePolInd
  var _cancelledMidTermPolInd : String as CancelledMidTermPolInd
  var _managedCareOrgPolInd : String as ManagedCareOrgPolInd
  var _certifiedHealthCareNetworkPolInd : String as CertifiedHealthCareNetworkPolInd
  var _futureReserved6 : String as FutureReserved6
  var _typeOfCovIdCode : int as TypeOfCovIdCode
  var _typeOfPlanIdCode : int as TypeOfPlanIdCode
  var _typeOfNonStandardIdCode : int as TypeOfNonStandardIdCode
  var _futureReserved7 : String as FutureReserved7
  var _lossesSubjectToDeductibleCode : int as LossesSubjectToDeductibleCode
  var _basisOfDeductibleCalcCode : int as BasisOfDeductibleCalcCode
  var _deductiblePercentage : int as DeductiblePercentage
  var _deductibleAmountPerClaimOrAccident : int as DeductibleAmountPerClaimOrAccident
  var _deductibleAmountAggregate : int as DeductibleAmountAggregate
  var _previousReportLevelCode : int as PreviousReportLevelCode
  var _futureReserved8 : String as FutureReserved8
  var _previousCorrectionSeqNum : String as PreviousCorrectionSeqNum
  var _previousCarrierCode : String as PreviousCarrierCode
  var _previousPolicyNumber : String as PreviousPolicyNumber
  var _previousPolicyEffectiveDate : String as PreviousPolicyEffectiveDate
  var _previousExposureStateCode : String as PreviousExposureStateCode
  var _futureReserved9 : String as FutureReserved9
  var _reservedForInsurerUse : String as ReservedForInsurerUse
  var _reservedForDCOUse : String as ReservedForDCOUse
  var _beepEditByPassCode : String as BeepEditByPassCode
  var _unitFormatSubmissionCode : String as UnitFormatSubmissionCode

}
