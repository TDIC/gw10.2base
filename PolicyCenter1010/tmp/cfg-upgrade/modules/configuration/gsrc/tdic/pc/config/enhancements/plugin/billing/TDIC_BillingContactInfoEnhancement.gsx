package tdic.pc.config.enhancements.plugin.billing

uses gw.api.database.Query
uses gw.api.database.Relop

/**
 * US877
 * Shane Sheridan 02/17/2015
 */
enhancement TDIC_BillingContactInfoEnhancement : gw.plugin.billing.BillingContactInfo {

  /**
   * US877
   * Shane Sheridan 02/17/2015
  */
  property get ContactDisplayName() : String {
    // AddressBookUID received from BillingCenter
    var addressBookUID = (this as gw.plugin.billing.bc1000.BCContactSummaryWrapper).AddressBookUID_TDIC
    var associatedContact = Query.make(Contact).compare(Contact#AddressBookUID, Relop.Equals, addressBookUID).select().AtMostOneRow

    if(associatedContact != null){
      return associatedContact.DisplayName
    }

    // Name as received from BillingCenter
    return this.Name
  }

}
