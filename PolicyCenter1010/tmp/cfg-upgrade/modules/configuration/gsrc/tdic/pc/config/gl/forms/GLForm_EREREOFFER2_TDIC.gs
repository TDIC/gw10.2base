package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses typekey.Job

class GLForm_EREREOFFER2_TDIC extends AbstractSimpleAvailabilityForm {
  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (context.Period.Job typeis PolicyChange and context.Period.Job.ChangeReasons.
          hasMatch(\changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER)
          and (context.Period.Policy.Jobs.hasMatch(\elt -> elt.Subtype == Job.TC_RENEWAL && elt.DisplayStatus == "Non-renewed") or
          context.Period.Policy.Jobs.hasMatch(\elt -> elt.LatestPeriod.Status == PolicyPeriodStatus.TC_EXPIRED))) {
        return true
      }
    }
    return false
  }
}