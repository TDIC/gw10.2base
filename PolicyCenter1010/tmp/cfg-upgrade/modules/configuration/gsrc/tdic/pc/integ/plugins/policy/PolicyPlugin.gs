package tdic.pc.integ.plugins.policy

uses java.util.Date
uses gw.api.locale.DisplayKey

/**
 * TDIC Policy Plugin class used to implement additional functionality for TDIC.  The OOTB class is read-only, so
 * this class subclasses the original class.
*/

class PolicyPlugin extends gw.plugin.policy.impl.PolicyPlugin {
  // GW-3058 - Prevent Policy Changes and Reinstatement on policy terms that are out of sync between PolicyCenter
  //           and BillingCenter.
  static protected var preventPolicyChangeReinstatement : String[] = {
      "002086-1", "003776-1", "006703-2", "008715-2", "009014-1"
  };

  override function canStartPolicyChange (policy : Policy, effectiveDate : Date) : String {
    var retval = super.canStartPolicyChange (policy, effectiveDate);

    if (retval == null) {
      var policyPeriod = Policy.finder.findPolicyPeriodByPolicyAndAsOfDate (policy, effectiveDate);
      if (preventPolicyChangeReinstatement.contains(policyPeriod.PolicyNumberLong_TDIC)) {
        retval = DisplayKey.get("Job.Error.JobNotAllowed_TDIC", typekey.Job.TC_POLICYCHANGE.DisplayName);
      }
    }

    return retval;
  }

  override function canStartReinstatement (policyPeriod : PolicyPeriod) : String {
    var retval = super.canStartReinstatement (policyPeriod);

    if (retval == null) {
      if (preventPolicyChangeReinstatement.contains(policyPeriod.PolicyNumberLong_TDIC)) {
        retval = DisplayKey.get("Job.Error.JobNotAllowed_TDIC", typekey.Job.TC_REINSTATEMENT.DisplayName);
      }
    }

    return retval;
  }
}