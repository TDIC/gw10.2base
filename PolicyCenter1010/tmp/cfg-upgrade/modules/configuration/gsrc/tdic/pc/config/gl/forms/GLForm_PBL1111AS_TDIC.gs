package tdic.pc.config.gl.forms

uses entity.FormAssociation
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode
uses typekey.Job

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL1111AS_TDIC extends AbstractSimpleAvailabilityForm {
  var period : PolicyPeriod

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    period = context.Period
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      var changeReasons = period.Job.ChangeReasons
      //GWPS-2353 : Do not infer PL Declaration (PBL1111AS) when policy change ‘ERE-PL Offer' is processed
        if (period.Job.Subtype == Job.TC_POLICYCHANGE and changeReasons != null) {
            if (changeReasons.hasMatch(\changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLOFFER)) {
              return false
            }else if(changeReasons.hasMatch(\ changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EXTENDEDREPORTINGPERIODENDORSEMENTPLCM)) {
              if (period.GLLine.GLExtendedReportPeriodCov_TDICExists) {
                var extendedCOverage = period.GLLine.GLExtendedReportPeriodCov_TDIC
                return extendedCOverage.GLERPRated_TDICTerm?.DisplayValue?.equalsIgnoreCase("No")
              }
            }else if(changeReasons.hasMatch(\ changeReason -> changeReason.ChangeReason == ChangeReasonType_TDIC.TC_EREPLDOCGEN)) {
              return true
            }
        }
      return (period.Job.Subtype == typekey.Job.TC_CANCELLATION || period.Job.Subtype == Job.TC_POLICYCHANGE) ? period.RefundCalcMethod != CalculationMethod.TC_FLAT : true
    }
    return false
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    if (period?.isChangeReasonOnlyRegenerateDeclaration_TDIC) {
      contentNode.addChild(createTextNode("JobNumber", period.Job.JobNumber))
    }
    // GWPS 568 re-generate form based on policy chnage Available based on policy
    if(period?.Job?.isValidPolicyChangeReasons_TDIC(period) || period?.Job?.ChangeReasons.hasMatch(\elt1 -> elt1.ChangeReason==ChangeReasonType_TDIC.TC_COVERAGEEXTENSIONPROVISION)){
      contentNode.addChild(createTextNode("JobNumber", period.Job.JobNumber))
    }
    }

  }
