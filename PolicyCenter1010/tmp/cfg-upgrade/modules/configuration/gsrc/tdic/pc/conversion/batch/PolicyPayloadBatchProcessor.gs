package tdic.pc.conversion.batch

uses gw.api.util.DateUtil
uses gw.api.webservice.exception.DataConversionException
uses gw.processes.BatchProcessBase
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.dao.PayloadDAO
uses tdic.pc.conversion.dao.PayloadStatusDAO
uses tdic.pc.conversion.dao.PolicyPeriodDAO
uses tdic.pc.conversion.dto.PayloadDTO
uses tdic.pc.conversion.dto.PolicyPeriodDTO
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.helper.PolicyConversionHelperFactory

class PolicyPayloadBatchProcessor extends BatchProcessBase {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CLASS_NAME = PolicyPayloadBatchProcessor.Type.Name

  construct() {
    super(BatchProcessType.TC_POLICYPAYLOAD_EXT)
  }

  override function requestTermination() : boolean {
    var methodName = "requestTermination"
    super.requestTermination()
    _logger.info(CLASS_NAME + " : " + methodName + ": Terminated")
    /**
     * This will set the Termination Request Flag
     */
    return true
    /**
     * It will return true to signal that the attempt is made to stop the process in doWork method.
     */
  }

  override function doWork() : void {

    var methodName = "doWork"
    var batchID = DateUtil.currentDate().Time
    var accountId : String
    var policyNum : String
    var offering : String

    _logger.info(CLASS_NAME + " : " + methodName + ": Started")
    OperationsCompleted = 0

    try {
      var payloadTimeDTO = new PayloadDTO()
      payloadTimeDTO.BatchID = batchID
      payloadTimeDTO.BatchType = "PP"
      //new PayloadDAO().persist(payloadTimeDTO)
      //while (true) {
     // var payloads = new ArrayList<PayloadDTO>()
      //Retrieve To-be Converted policies from the Staging DB
      var policyList = new PolicyPeriodDAO().retrieveAllUnprocessed()
      _logger.info("${CLASS_NAME} :${methodName} :Processing total policies : {}", policyList.Count)

      for (policyDTO in policyList) {
        _logger.info("${CLASS_NAME} :${methodName} :starting with policy number: {}", policyDTO.PolicyNumber)
        try {
          accountId = policyDTO.AccountId
          policyNum = policyDTO.PolicyNumber
          offering = policyDTO.OFFERING_LOB
          //Start creating account payload xml
          defaultValues(policyDTO)
          var policyXML = PolicyConversionHelperFactory.getConversionHelper(policyDTO).generatePayloadXML()
          //Save the policy payload XML
          var payloadStatusDTO = new PayloadDTO()

          payloadStatusDTO.AccountKey = policyDTO.AccountId
          payloadStatusDTO.PolicyNumber = policyDTO.PolicyNumber
          payloadStatusDTO.BatchID = batchID
          payloadStatusDTO.BatchType = "PP"
          payloadStatusDTO.OfferingLOB = policyDTO.OFFERING_LOB
          payloadStatusDTO.Counter = policyDTO.Counter == null ? 0 : policyDTO.Counter

          var payloadDTO = new PayloadDTO(batchID, policyDTO.AccountId, policyXML, policyDTO.PolicyNumber, policyDTO.GWPolicyNumber, "PP",
              policyDTO.OFFERING_LOB, policyDTO.Counter == null ? 0 : policyDTO.Counter)
          new PayloadDAO().persist(payloadDTO)
          new PayloadStatusDAO().persist(payloadStatusDTO)
          incrementOperationsCompleted()
        } catch (dce : DataConversionException) {
          _logger.error("${CLASS_NAME} :${methodName} : error details " + dce.StackTraceAsString)
          CommonConversionHelper.saveConversionError(policyNum, accountId, offering, "PP", batchID, dce)
          incrementOperationsCompleted()
          incrementOperationsFailed()
//            new PayloadDAO().update(payloadTimeDTO)
          if (terminate())
            return
            /*CommonConversionHelper.saveConversionError(policyPeriodDTO.PolicyNumber, policyPeriodDTO.AccountKey,
                BatchType.POLICY_PAYLOAD, batchID, e)*/
        } catch (e : Exception) {
          _logger.error("${CLASS_NAME} :${methodName} : error details " + e.StackTraceAsString)
          CommonConversionHelper.saveConversionError(policyNum, accountId, offering, "PP", batchID, e)
          incrementOperationsCompleted()
          incrementOperationsFailed()
          if (terminate())
            return
        }
        if (terminate()) {
          /*if (payloads.Count != 0) {
            new PayloadDAO().persistAll(payloads)
          }*/
          return
        }
      }

      //Save the converted payloads to database.
      /*if (payloads.Count != 0)
        new PayloadDAO().persistAll(payloads)*/
      //new PayloadDAO().update(payloadTimeDTO)
      //Break the loop if there are no more policies.
        /*if (policyList.Count == 0)
          break*/
      //}
      if (terminate())
        return
    } catch (e : Exception) {
      _logger.error("${CLASS_NAME} :${methodName} :Error details", e)
      incrementOperationsCompleted()
      incrementOperationsFailed()
      var payloadTimeDTO = new PayloadDTO()
      payloadTimeDTO.BatchID = batchID
      payloadTimeDTO.BatchType = "PP"
//      new PayloadDAO().update(payloadTimeDTO)
      if (terminate())
        return
      //CommonConversionHelper.saveConversionError(null, " ", BatchType.POLICY_PAYLOAD, batchID, e)
    }

    _logger.info(CLASS_NAME + " : " + methodName + ": Completed")
  }

  private function terminate() : boolean {
    var methodName = "terminate"
    if (TerminateRequested) {
      _logger.warn("${CLASS_NAME} :${methodName} :Termination of the process requested.")
      return true
    }
    return false
  }

  private function defaultValues(policyPeriodDTO : PolicyPeriodDTO) {
    policyPeriodDTO.UWCompany = ConversionConstants.DEFAULT_UWCOMPANY
    policyPeriodDTO.IndustryCode = ConversionConstants.DEFAULT_INDUSTRYCODE
    var producerCode = ConversionConstants.PRODUCERCODE_BY_STATE.get(policyPeriodDTO.BaseState)
    policyPeriodDTO.ProducerCode =  producerCode == null ? ConversionConstants.DEFAULT_PRODUCERCODE : producerCode
    policyPeriodDTO.TermType = ConversionConstants.DEFAULT_TERMTYPE
  }
}