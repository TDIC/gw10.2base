package tdic.pc.common.batch.finance.reports.premiums

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLinesInterface


abstract class TDIC_FinancePremiumsBase implements TDIC_FinancePremiumsInterface {

  private var batchPeriodStartDate : Date as BatchPeriodStartDate
  private var batchPeriodEndDate : Date as BatchPeriodEndDate
  private var _prmLinesProcessor : TDIC_FinancePremiumLinesInterface

  construct(periodStartDate : Date, periodEndDate: Date, prmLines : TDIC_FinancePremiumLinesInterface){
    batchPeriodStartDate = periodStartDate
    batchPeriodEndDate = periodEndDate
    _prmLinesProcessor =  prmLines
  }

  override function getPremiumLinesProcessor(): TDIC_FinancePremiumLinesInterface {
    return _prmLinesProcessor
  }

}