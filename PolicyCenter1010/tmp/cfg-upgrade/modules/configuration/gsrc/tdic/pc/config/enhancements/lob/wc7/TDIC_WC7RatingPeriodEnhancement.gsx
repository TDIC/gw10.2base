package tdic.pc.config.enhancements.lob.wc7

uses java.util.Set
uses gw.pl.currency.MonetaryAmount
uses gw.api.locale.DisplayKey

enhancement TDIC_WC7RatingPeriodEnhancement : gw.lob.wc7.rating.WC7RatingPeriod {


  /**
   * GW141
   * 02/09/2017 Kesava T
   *
   * PeriodCosts required for Document Production
   */
  @Returns("Period Costs")
  property get PeriodCosts_TDIC(): Set<entity.WC7Cost> {
    var lineCosts = this.Jurisdiction.PolicyLine.Costs.cast(WC7Cost)
    var partitionCosts = lineCosts.partition(\ c -> c.JurisdictionState).toAutoMap(\ st -> java.util.Collections.emptySet<WC7Cost>())
    var stateCosts = partitionCosts.get(this.Jurisdiction.Jurisdiction)
    return stateCosts.byRatingPeriod(this)
  }

  /**
   * GW141
   * 02/09/2017 Kesava T
   *
   * Manual Premium required for Document Production
   */
  @Returns("Manual Premium")
  property get ManualPremium_TDIC(): MonetaryAmount {
    var manualPremium = this.PeriodCosts_TDIC.getStandardPremiums(this.Jurisdiction.PolicyLine.AssociatedPolicyPeriod.PreferredCoverageCurrency).firstWhere(\s -> s.Description == DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.ManualPremium"))
    return manualPremium.Total
  }

  /**
   * GW141
   * 02/09/2017 Kesava T
   *
   * Subject Premium required for Document Production
   */
  @Returns("Subject Premium")
  property get SubjectPremium_TDIC(): MonetaryAmount {
    var subjectPremium = this.PeriodCosts_TDIC.getStandardPremiums(this.Jurisdiction.PolicyLine.AssociatedPolicyPeriod.PreferredCoverageCurrency).firstWhere(\s -> s.Description == DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.SubjectPremium"))
    return subjectPremium.Total
  }

  /**
   * GW141
   * 02/09/2017 Kesava T
   *
   * Modified Premium required for Document Production
   */
  @Returns("Modified Premium")
  property get ModifiedPremium_TDIC(): MonetaryAmount {
    var modifiedPremium = this.PeriodCosts_TDIC.getStandardPremiums(this.Jurisdiction.PolicyLine.AssociatedPolicyPeriod.PreferredCoverageCurrency).firstWhere(\m -> m.Description == DisplayKey.get("Web.SubmissionWizard.Quote.WC.Subtotal.ModifiedPremium"))
    return modifiedPremium.Total
  }

  /**
   * GW141
   * 02/09/2017 Kesava T
   *
   * Standard Premium required for Document Production
   */
  @Returns("Standard Premium")
  property get StandardPremium_TDIC(): MonetaryAmount {
    return this.PeriodCosts_TDIC.StandardPremium.sum(this.Jurisdiction.PolicyLine.AssociatedPolicyPeriod.PreferredCoverageCurrency, \ c -> c.ActualAmount)
  }

  /**
   * GW141
   * 02/09/2017 Kesava T
   *
   * Total Fares and Taxes required for Document Production.
   * Obtained by subtracting the California Total Premium from the California Total Cost.
   */
  @Returns("Total Taxes and Fares")
  property get TaxesAndFares_TDIC(): MonetaryAmount {
    return this.Jurisdiction.PolicyLine.AssociatedPolicyPeriod.TotalCostRPT - this.Jurisdiction.PolicyLine.AssociatedPolicyPeriod.TotalPremiumRPT
  }

  /**
   * GW141
   * 02/24/2017 Kesava T
   *
   * Waiver Of Subrogation Cost required for Document Production.
   */
  @Returns("WaiverOfSubrogation Cost")
  property get WaiverOfSubrogation_TDIC(): WC7Cost {
    return this.PeriodCosts_TDIC.firstWhere( \ elt -> elt.Subtype == typekey.WC7Cost.TC_WC7WAIVEROFSUBROCOST)
  }

  /**
   * GW141
   * 02/24/2017 Kesava T
   *
   * Experience Modification Cost required for Document Production.
   */
  @Returns("ExperienceModification Cost")
  property get ExperienceModification_TDIC(): WC7Cost {
    return this.PeriodCosts_TDIC.firstWhere( \ elt -> elt.Description == typekey.WC7JurisdictionCostType.TC_EXPMOD.DisplayName)
  }

  /**
   * GW141
   * 02/24/2017 Kesava T
   *
   * ScheduleRating Cost required for Document Production.
   */
  @Returns("ScheduleRating Cost")
  property get ScheduleRating_TDIC(): WC7Cost {
    return this.PeriodCosts_TDIC.firstWhere( \ elt -> elt.Description == typekey.WC7JurisdictionCostType.TC_SCHEDCREDIT.DisplayName)
  }

  /**
   * GW141
   * 02/24/2017 Kesava T
   *
   * CDASafetyGroupDiscount Cost required for Document Production.
   */
  @Returns("CDASafetyGroupDiscount Cost")
  property get CDASafetyGroupDiscount_TDIC(): WC7Cost {
    return this.PeriodCosts_TDIC.firstWhere( \ elt -> elt.Description == typekey.WC7JurisdictionCostType.TC_CERTIFIEDSAFETYCREDIT.DisplayName)
  }

  /**
   * GW141
   * 02/24/2017 Kesava T
   *
   * Multiline Discount Cost required for Document Production.
   */
  @Returns("MultilineDiscount Cost")
  property get MultilineDiscount_TDIC(): WC7Cost {
    return this.PeriodCosts_TDIC.firstWhere( \ elt -> elt.Description == typekey.WC7JurisdictionCostType.TC_MULTILINEDISCOUNT_TDIC.DisplayName)
  }

  /**
   * GW141
   * 02/24/2017 Kesava T
   *
   * Minimum Premium Adjustment Cost required for Document Production.
   */
  @Returns("MinimumPremiumAdjustment Cost")
  property get MinPremAdjustment_TDIC(): WC7Cost {
    return this.PeriodCosts_TDIC.firstWhere( \ elt -> elt.Description == typekey.WC7JurisdictionCostType.TC_MINPREM.DisplayName)
  }

  /**
   * GW141
   * 03/02/2017 Kesava T
   *
   * ShortRate Cost required for Document Production.
   */
  @Returns("ShortRate Cost")
  property get ShortRate_TDIC(): WC7Cost {
    return this.PeriodCosts_TDIC.firstWhere( \ elt -> elt.Description == typekey.WC7JurisdictionCostType.TC_SHORTRATE_TDIC.DisplayName)
  }
}
