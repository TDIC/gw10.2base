package tdic.pc.conversion.dto

class AccountContactDTO {

  var _contactID : String as ContactID
  var _accountID : String as AccountID
  var _publicID : String as PublicID

}