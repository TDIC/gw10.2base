package tdic.pc.config.enhancements.job

enhancement CancellationEnhancement : entity.Cancellation {
  /**
   * Is this cancellation because of a delinquency.
  */
  public property get IsDelinquencyCancellation_TDIC() : boolean {
    return ReasonCode.TF_DELINQUENCYREASONS_TDIC.TypeKeys.contains(this.CancelReasonCode);
  }
}
