package tdic.pc.config.enhancements.policy

uses java.util.Date

/**
 * DE54
 * Shane Sheridan 11/27/2014
 *
 * This enhancement was created to implement a workaround to a PolicyCenter OOTB defect that was found and submitted as DE54.
 *
 * Description of Defect:
 * The beforeEnter property of PolicyFile_Audits.pcf is calling the PolicyPeriod.PolicyFileMessages virtual property, but the exact
 * piece of code is calling PolicyPeriod.getSlice(asOfDate).PolicyFileMessages; which is attempting to get the slice of this
 * policy period that java.util.Date.CurrentDate falls into. But when java.util.Date.CurrentDate is out of range, an exception is thrown.
 *
 * However, this defect is actually more wide spread than was first noticed in DE54, and so this is workaround fix is more effective than the original fix for DE54.
 *
 * Note: This workaround was proposed by Consulting Enablement.
 */
enhancement TDIC_PolicyPeriodFixAsOfDateDefectEnhancement : entity.PolicyPeriod {

  @Param("date", "The asOfDate that will be passed in the .getSliceDate() function and potentially cause an error.")
  @Returns("The validated value for asOfDate; if it was invalid then this is PeriodEnd.addBusinessDays(-1)")
  function validateRetrieveAsOfDate(date : Date) : Date {
    if(date == null or (date.before(this.PeriodEnd) && date.afterOrEqual(this.PeriodStart))){
      //do nothing, it's fine
    }
    else {
      //need to check that it falls within another period or reset the date to one where there is actually a period
      var candidates = this.Policy?.Periods?.where( \ elt -> isCandidateForDate(elt, date)==true)
      if(candidates?.length==0){
        //no matches, need to recalibrate the date choice according to the period we know does exist
        return this.PeriodEnd.addBusinessDays(-1)
      }
      else {
        //got at least one match, recalibrate the date according to the match
        return candidates?.first()?.PeriodEnd.addBusinessDays(-1)
      }
    }
    return date
  }

  private function isCandidateForDate(period : PolicyPeriod, date : Date) : boolean {
    if(period?.Promoted){
      if(period?.PeriodEnd.after(date)){
        //date does at least occur before the end of the period
        if(period?.PeriodStart.beforeOrEqual(date)){
          return true
        }
      }
    }
    return false
  }
}
