package tdic.pc.config.rating.bop.params

uses java.math.BigDecimal

/**
 * Rating Param it will populate params of building
 * required in rating algorithm
 *
 * @author- Ankita Gupta
 * 06/20/2019
 * .
 */
class BOPBuildingRatingParams{

  var territoryCode : String as TerritoryCode
  var protetionClass : String as ProtectionClass
  var buildingConstruction : String as BuildingConstruction
  var hasSprinklerCredit : boolean as HasSprinklerCredit
  var deductibleLimit : BigDecimal as DeductibleLimit
  var buildingOfficeArea : BigDecimal as BuildingOfficeArea
  var hasBuildingCoverage : boolean as HasBuildingCoverage

  construct(){}

  construct(building: BOPBuilding){
    territoryCode =  building.Building.TerritoryDetails_TDIC.Code
    protetionClass = building.Building.ProtectionClass_TDIC.Code
    buildingConstruction = building.ConstructionType.Code
    hasSprinklerCredit = building.Building.Sprinklered_TDIC?.booleanValue()
    deductibleLimit = building.BOPDeductibleCov_TDIC?.BOPDeductibleOptionsTDICTerm?.Value
    buildingOfficeArea = building.Building.TotalArea
    hasBuildingCoverage = building.BOPBuildingCovExists
  }
}