package tdic.pc.common.batch.as400

uses com.tdic.util.properties.PropertyUtil
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory
uses gw.processes.BatchProcessBase
uses com.tdic.util.database.DatabaseManager
uses java.lang.Exception
uses java.sql.Connection
uses java.sql.SQLException
uses java.sql.PreparedStatement
uses java.io.File
uses java.io.FileReader
uses java.io.FileNotFoundException
uses java.lang.IllegalArgumentException
uses java.text.SimpleDateFormat
uses org.apache.commons.csv.CSVFormat
uses tdic.pc.common.batch.as400.dto.AS400InboundCSVFields_TDIC

/**
 * Sprint - 4 , GINTEG-237 : This batch job reads the data in inbound AS400 csv file and loads the same into a table in 'GWINT' (Integration) database
 * Also archives the inbound file.
 * @Author : Sudheendra V
 * @Date: 08/12/2019
 */

class AS400InboundBatch_TDIC extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("TDIC_AS400")

  /**
   * Property with the directory name which has inbound file from AS400
   */
  private static final var AS400FEED_POLICYFILE_NAME_PROPERTY = PropertyUtil.getInstance().getProperty("as400.inbound.filesdirectory.name")

  /**
   * Property with the directory name which has Archive file from AS400
   */
  private static final var AS400FEED_ARCHIVE_DIRECTORY = PropertyUtil.getInstance().getProperty("as400.inbound.archivedirectory.name")

  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = "PCInfraIssueNotificationEmail"

  /**
   * Notification email's subject for batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE : String = "AS400InboundBatch_TDIC Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Query to insert contents from 'ACTAS400CA.csv'file into table 'AS400PolicyDetails'
   */
  private static final var _prepSqlStmt = "INSERT INTO AS400PolicyDetails(ADA,AS400PolNumbr,LastName,FirstName,Title,LongName) VALUES(?,?,?,?,?,?)"

  /**
   * Query to cleanup data in 'AS400PolicyDetails' table
   */
  private static final var _cleanupdata = "DELETE FROM AS400PolicyDetails"
  private static final var _intdburl = PropertyUtil.getInstance().getProperty("IntDBURL")

  static final var _dateformat = "M_dd_yyyy_HHMMSS"
  static final var _actas400ca= "ACTAS400CA_"
  static final var _csv = ".csv"
  construct() {
    super(BatchProcessType.TC_AS400INBOUNDBATCH_TDIC)
  }

  protected override function doWork() {
    var file = new File(AS400FEED_POLICYFILE_NAME_PROPERTY)
    var _con : Connection = null
    var _prepStatement : PreparedStatement = null
    var reader : FileReader = null
    var isCleanUpDone = false
    if (!file.exists()) {
      throw new FileNotFoundException()
    }
    try {
      reader = new FileReader(file)
      var details = CSVFormat.RFC4180.parse(reader)
      _con = DatabaseManager.getConnection(_intdburl)
      details.where(\elt -> elt.get(0) != null).each(\detail -> {
        if(not isCleanUpDone){
          _prepStatement = _con.prepareStatement(_cleanupdata)
          _prepStatement.execute()
          _con.commit()
          isCleanUpDone = true
        }
        var _as400FieldsMap = new AS400InboundCSVFields_TDIC()
        _as400FieldsMap.ADA = Coercions.makeLongFrom(detail.get(0))
        _as400FieldsMap.AS400PolNumbr = Coercions.makeLongFrom(detail.get(1))
        _as400FieldsMap.LastName = Coercions.makeStringFrom(detail.get(2))
        _as400FieldsMap.FirstName = Coercions.makeStringFrom(detail.get(3))
        _as400FieldsMap.Title = Coercions.makeStringFrom(detail.get(4))
        _as400FieldsMap.LongName = Coercions.makeStringFrom(detail.get(5))
        _prepStatement = _con.prepareStatement(_prepSqlStmt)
        insertRecords(_as400FieldsMap, _prepStatement)
        _con.commit()
      })
      reader.close()
      //Move the inbound csv file to 'Archive' folder
      var _currentDate = new SimpleDateFormat(_dateformat).format(new Date().CurrentDate)
      file.renameTo(new File(AS400FEED_ARCHIVE_DIRECTORY + _actas400ca + _currentDate + _csv))
    } catch (sql : SQLException) {
      _logger.debug("AS400InboundBatch_TDIC - SQLException While Closing Connection !!!" + sql.toString())
      throw sql
    } catch (e : Exception) {
      _logger.debug("AS400InboundBatch_TDIC - Exception While Closing Connection !!!" + e.toString())
      throw e
    } finally {
      try {
        if (reader != null) reader.close()
        if(_prepStatement != null) _prepStatement.close()
        if (_con != null) _con.close()
      } catch (fe : Exception) {
        _logger.debug("AS400InboundBatch_TDIC - Error While Closing Connection !!!")
        throw fe
      }
    }
  }

  /**
   * Below function will insert the details in 'ACTAS400CA.csv' file into table 'AS400PolicyDetails'
   */
  private function insertRecords(as400FieldsMap: AS400InboundCSVFields_TDIC, prepardStmnt : PreparedStatement){
    prepardStmnt.setLong(1, as400FieldsMap.ADA)
    prepardStmnt.setLong(2, as400FieldsMap.AS400PolNumbr)
    prepardStmnt.setString(3, as400FieldsMap.LastName)
    prepardStmnt.setString(4, as400FieldsMap.FirstName)
    prepardStmnt.setString(5, as400FieldsMap.Title)
    prepardStmnt.setString(6, as400FieldsMap.LongName)
    prepardStmnt.executeUpdate()
  }
}