package tdic.pc.common.batch.unistat.helper

uses java.util.ArrayList

uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatReportHeaderFields
uses tdic.pc.common.batch.unistat.dto.TDIC_WCstatReportTrailerFields
uses tdic.util.flatfile.model.classes.Record
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 1/23/15
 * Time: 5:39 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCStatCompleteRecord {

  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCSTATREPORT")

  var _reportHeaderRecord : TDIC_WCstatReportHeaderFields as ReportHeaderRecord
  var _wcpolsrecords : ArrayList<TDIC_WCstatRecordTypes> as WCStatRecords
  var _reportTrailerRecord : TDIC_WCstatReportTrailerFields as ReportTrailerRecord



  function convertToFlatFileRecord(records:Record[]) :String{
    var flatFileContents=""
    var headerContents=""
    var trailerContents=""
    var recordContents=""


    for(record in records){
       _logger.debug("************************"+record.RecordType+"*****************************")
      switch (record.RecordType) {
        case "Report Header Record":
            headerContents += this.ReportHeaderRecord.createFlatFileLine(record)+"\r\n"
            break
        case "Trailer Record":
            trailerContents += ReportTrailerRecord.createFlatFileLine(record)
            break
          default:
          break
      }

    }
    this.WCStatRecords.each(\elt -> {
      recordContents += elt.convertToFlatFileRecord(records)
    })

    return headerContents+  recordContents + trailerContents

  }




}