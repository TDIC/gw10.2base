package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.PolicyPeriodDTO
uses tdic.pc.conversion.query.PolicyConversionBatchQueries

uses java.sql.Connection

class PolicyPeriodDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<PolicyPeriodDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"

  var _params : Object[]
  var _batchType : String

  construct(params : Object[]) {
    _params = params
  }

  construct(batchType : String) {
    _batchType = batchType
  }

  construct(params : Object[], batchType : String) {
    _params = params
    _batchType = batchType
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : ArrayList<PolicyPeriodDTO> {
    _logger.info("Inside PolicyPeriodDAO.retrieveAllUnprocessed() ")
    var query : String

    if (_batchType == "PC") {
      query = PolicyConversionBatchQueries.POLICY_CONVERSION_SELECT_QUERY
    } else {
      query = PolicyConversionBatchQueries.POLICY_DETAILS_SELECT_QUERY
    }

    var handler = new BeanListHandler(PolicyPeriodDTO)
    var rows = runQuery(query, null, handler, _logger) as ArrayList<PolicyPeriodDTO>

    return rows
  }

  override function update(aTransientObject : PolicyPeriodDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      var params = new Object[2]
      params[0] = ""
      params[1] = ""
      updatedRows = runUpdate(connection, "", params, _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(aPersistentObject : PolicyPeriodDTO) : int {
    return 0
  }

  override function persistAll(persistentObjects : List<PolicyPeriodDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : PolicyPeriodDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<PolicyPeriodDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<PolicyPeriodDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<PolicyPeriodDTO>) {
  }

}