package tdic.pc.config.wc7.forms

uses gw.lob.wc7.forms.WC7FormData
uses gw.xml.XMLNode

abstract class WC7Form_AlternateCoverage extends WC7FormData {
  protected var _wc7Exclusion : WC7WorkersCompExcl;

  @Returns("A return value of true indicates that the form should be on the policy, while a value of false indicates that the form is not relevant to the policy.")
  override property get InferredByCurrentData(): boolean {
    return _wc7Exclusion != null;
  }

  /**
   * Form contains a schedule of Alternative Coverages.
   */
  override function addDataForComparisonOrExport(contentNode: XMLNode) {
    var alternateCoverageNode : XMLNode;
    var alternateCoveragesNode = new XMLNode("AlternateCoverages");
    contentNode.addChild(alternateCoveragesNode);

    for (alternateCoverage in _wc7Exclusion.WC7AlternateCoverages_TDIC.orderBy(\ac -> ac.FixedId)) {
      alternateCoverageNode = new XMLNode("AlternateCoverage");
      alternateCoverageNode.addChild(createTextNode("InsuredName", alternateCoverage.InsuredName));
      alternateCoverageNode.addChild(createTextNode("Insurer", alternateCoverage.InsurerCode.DisplayName));
      alternateCoverageNode.addChild(createTextNode("PolicyNumber", alternateCoverage.PolicyNumber));
      alternateCoverageNode.addChild(createTextNode("PolicyEffectiveDate", alternateCoverage.PolicyEffectiveDate as String));
      alternateCoveragesNode.addChild(alternateCoverageNode);
    }

    for (covTerm in _wc7Exclusion.CovTerms.orderBy(\ct -> ct.Pattern.Priority)) {
      contentNode.addChild(createTextNode(covTerm.PatternCode, covTerm.DisplayValue));
    }
  }
}