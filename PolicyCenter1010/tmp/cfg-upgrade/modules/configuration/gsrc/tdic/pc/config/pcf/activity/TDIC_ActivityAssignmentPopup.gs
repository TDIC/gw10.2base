package tdic.pc.config.pcf.activity

uses gw.api.assignment.AssignmentPopup
uses gw.api.assignment.Assignee
uses gw.api.database.Relop
uses gw.api.database.Query

/**
 * DE248, robk
 *
 * TDIC Wrapper object for assign activities UI screen. Stores user's selection of who or what activities
 * should be assigned to. Also contains search criteria for user/group/queue search.
 */
class TDIC_ActivityAssignmentPopup extends AssignmentPopup {

  private var _activities : Activity[]
  private var _reinstatesAndAppealsQueue : AssignableQueue
  private var _policyChangeQueue : AssignableQueue

  construct(activities : entity.Activity[] , defaultUser : entity.User) {
    super(entity.Activity.Type, activities, new com.guidewire.pc.domain.activity.PCSuggestedAssigneeBuilder(activities, defaultUser))
    _activities = activities
  }

  override property get SuggestedAssignees() : Assignee[] {
    if (shouldAssignToReinstatesAndAppealsQueueOnly()) {
      if (_reinstatesAndAppealsQueue == null) {
        setReinstatesAndAppealsQueue()
      }
      return {_reinstatesAndAppealsQueue}
    }
    if (shouldAssignToPolicyChangeQueueOnly()) {
      if (_policyChangeQueue == null) {
        setPolicyChangeQueue()
      }
      return {_policyChangeQueue}
    }
    return super.SuggestedAssignees
  }

  private function shouldAssignToReinstatesAndAppealsQueueOnly() : boolean {
    return _activities != null and _activities.length == 1
            and (_activities[0].ActivityPattern.Code == "reinstatement_request_received"
                or _activities[0].ActivityPattern.Code == "review_appeal_nonrenewal")
  }

  private function shouldAssignToPolicyChangeQueueOnly() : boolean {
    return _activities != null and _activities.length == 1
        and (_activities[0].ActivityPattern.Code == "risk_management_seminar")
  }

  private function setReinstatesAndAppealsQueue() {
    var reinstatementsGroup = Query.make(Group).compare(Group#PublicID, Relop.Equals, "pc-tdic:6").select().AtMostOneRow
    if (reinstatementsGroup != null and reinstatementsGroup.AssignableQueues != null) {
      _reinstatesAndAppealsQueue = reinstatementsGroup.AssignableQueues.firstWhere( \ q -> q.PublicID == "pc-tdic:5")
    }
  }

  private function setPolicyChangeQueue() {
    var policyChangeGroup = Query.make(Group).compare(Group#PublicID, Relop.Equals, "tdic-sb:206").select().AtMostOneRow
    if (policyChangeGroup != null and policyChangeGroup.AssignableQueues != null) {
      _policyChangeQueue = policyChangeGroup.AssignableQueues.firstWhere( \ q -> q.PublicID == "tdic-sb:105")
    }
  }
}