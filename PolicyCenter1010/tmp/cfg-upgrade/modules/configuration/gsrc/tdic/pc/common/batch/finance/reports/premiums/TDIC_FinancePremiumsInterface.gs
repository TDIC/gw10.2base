package tdic.pc.common.batch.finance.reports.premiums

uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLineTypeEnum
uses tdic.pc.common.batch.finance.reports.prmlines.TDIC_FinancePremiumLinesInterface


interface TDIC_FinancePremiumsInterface {
  //function getPremiumAmounts(productCode: String) : Map<typekey.Jurisdiction, Map<TDIC_FinancePremiumLineTypeEnum, TDIC_FinanceReportPremiumObject>>
  function getPremiumLinesProcessor() : TDIC_FinancePremiumLinesInterface
  function getPremiumAmounts(productCodes: List<String>) : List<TDIC_FinanceReportPremiumObject>
}