package tdic.pc.conversion.helper

uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.dto.PolicyPeriodDTO
uses tdic.pc.conversion.exception.ConversionException
uses tdic.pc.conversion.lob.bop.BOPPolicyConversionHelper
uses tdic.pc.conversion.lob.gl.GLPolicyConversionHelper

class PolicyConversionHelperFactory {

  static function getConversionHelper(policyPeriodDTO : PolicyPeriodDTO) : PolicyConversionHelper {
    var product = ConversionConstants.PRODUCTCODE_MAPPER.get(policyPeriodDTO.OFFERING_LOB)
    switch (product) {
      case ConversionConstants.ProductCode.BUSINESSOWNERS:
        return new BOPPolicyConversionHelper(policyPeriodDTO, product)
      case ConversionConstants.ProductCode.GENERALLIABILITY:
        return new GLPolicyConversionHelper(policyPeriodDTO, product)
      default:
        throw new ConversionException(ConversionConstants.ERROR_MAPPER.get(ConversionConstants.ErrorKey.INVALID_PRODUCTCODE), {"productCode" -> policyPeriodDTO.OFFERING_LOB})
    }
  }

}