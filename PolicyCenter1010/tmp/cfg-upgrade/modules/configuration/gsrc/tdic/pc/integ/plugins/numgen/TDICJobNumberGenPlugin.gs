package tdic.pc.integ.plugins.numgen

uses gw.plugin.jobnumbergen.IJobNumberGenPlugin
uses gw.api.util.StringUtil
uses gw.api.system.database.SequenceUtil
uses com.guidewire.pl.web.controller.UserDisplayableException
uses gw.plugin.InitializablePlugin
uses java.util.Map
uses gw.api.locale.DisplayKey

/**
 * US945
 * 11/07/2014 Rob Kelly
 *
 * Job number plugin implementation for TDIC.
 */
class TDICJobNumberGenPlugin implements IJobNumberGenPlugin, InitializablePlugin {

  /**
   * The first job number.
   */
  private static final var FIRST_JOB_NUMBER = 1

  /**
   * The sequence to use for generating job numbers.
   */
  private var sequenceName : String

  construct() {

  }

  /**
   * Processes the parameters passed to this plugin.
   * Looks for a single parameter specifying the sequence to use for generating job numbers.
   */
  @Param("aParamMap", "the parameters passed to this plugin as a name-value map")
  override property set Parameters(aParamMap : Map<Object, Object>) {

    this.sequenceName = aParamMap.get("SequenceName") as String
  }

  /**
   * Generates a job number for the specified PolicyPeriod.
   */
  @Param("aJob", "the job for which the job number is required")
  @Returns("the job number as a String")
  @Throws(UserDisplayableException, "if a job number could not be generated")
  override function genNewJobNumber(aJob : Job): String {

    if (this.sequenceName == null) {

      throw new UserDisplayableException(DisplayKey.get("TDIC.NumberGenerator.Job.NoSequence"))
    }

    if (aJob == null) {

      throw new UserDisplayableException(DisplayKey.get("TDIC.NumberGenerator.Job.NullJob"))
    }

    var prefix : String
    switch (aJob.Subtype) {

      case typekey.Job.TC_AUDIT:
          prefix = "AUD"
          break
      case typekey.Job.TC_CANCELLATION:
          prefix = "CAN"
          break
      case typekey.Job.TC_ISSUANCE:
          prefix = "ISS"
          break
      case typekey.Job.TC_POLICYCHANGE:
          prefix = "CHG"
          break
      case typekey.Job.TC_REINSTATEMENT:
          prefix = "REI"
          break
      case typekey.Job.TC_RENEWAL:
        // Jeff, US-274, adding transaction number for migrated renewal for Commercial Property on account screen.
          if (aJob.MigrationJobInd_TDIC or aJob.MigrationJobInfo_Ext != null or (aJob.LatestPeriod != null and aJob.LatestPeriod.isLegacyConversion)) {
            prefix = "MIG"
          } else {
            prefix = "REN"
          }
          break
      case typekey.Job.TC_REWRITE:
          prefix = "REW"
          break
      case typekey.Job.TC_SUBMISSION:
          prefix = "SUB"
          break
      default:
        throw new UserDisplayableException(DisplayKey.get("TDIC.NumberGenerator.Job.UnsupportedJobType", aJob.Subtype))
    }

    var suffix = StringUtil.formatNumber(SequenceUtil.next(FIRST_JOB_NUMBER, this.sequenceName), "000000")
    return prefix + suffix
  }
}



