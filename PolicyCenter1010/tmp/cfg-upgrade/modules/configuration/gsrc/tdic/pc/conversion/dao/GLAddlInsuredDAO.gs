package tdic.pc.conversion.dao

uses com.tdic.pc.common.base.dao.AbstractPrepStmtJdbcDAO
uses com.tdic.pc.common.base.dao.IJdbcPrepStmtDAO
uses org.apache.commons.dbutils.DbUtils
uses org.apache.commons.dbutils.handlers.BeanListHandler
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.dto.GLAddlInsuredDTO
uses tdic.pc.conversion.query.GLConversionQueries

uses java.sql.Connection

class GLAddlInsuredDAO extends AbstractPrepStmtJdbcDAO implements IJdbcPrepStmtDAO<GLAddlInsuredDTO> {

  private static final var _logger = LoggerUtil.CONVERSION
  private static final var CONVERSION_INTEGRATION_NAME = "conversion.integration.name"

  var _params : Object[]

  construct(params : Object[]) {
    _params = params
  }

  construct() {
  }

  override function getConnectionProperty() : String {
    return CONVERSION_INTEGRATION_NAME
  }

  override function retrieveAllUnprocessed() : ArrayList<GLAddlInsuredDTO> {
    _logger.info("Inside GLAddlInsuredDAO.retrieveAllUnprocessed() ")
    var handler = new BeanListHandler(GLAddlInsuredDTO)
    var rows = runQuery(GLConversionQueries.ADDL_INSURED_SELECT_QUERY, _params, handler, _logger) as ArrayList<GLAddlInsuredDTO>

    return rows
  }

  override function update(aTransientObject : GLAddlInsuredDTO) : int {
    var connection : Connection
    var updatedRows : int
    try {
      connection = getConnection(_logger)
      var params = new Object[2]
      params[0] = ""
      params[1] = ""
      updatedRows = runUpdate(connection, "", params, _logger)
      if (updatedRows > 0) {
        connection.commit()
      }
      return updatedRows
    } finally {
      DbUtils.rollbackAndCloseQuietly(connection)
    }
  }

  override function persist(aPersistentObject : GLAddlInsuredDTO) : int {
    return 0
  }

  override function persistAll(persistentObjects : List<GLAddlInsuredDTO>) : int[] {
    return null
  }

  override function delete(aTransientObject : GLAddlInsuredDTO) : int {
    return 0
  }

  override function deleteAll(persistentObjects : List<GLAddlInsuredDTO>) : int[] {
    return null
  }

  override function retrieveAll() : List<GLAddlInsuredDTO> {
    return null
  }

  override function markAsProcessed(persistentObjects : List<GLAddlInsuredDTO>) {
  }

}