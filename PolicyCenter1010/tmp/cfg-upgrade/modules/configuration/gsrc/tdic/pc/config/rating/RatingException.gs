package tdic.pc.config.rating

uses java.lang.RuntimeException

class RatingException extends RuntimeException {

  construct(msg : String) {
    super(msg)
  }

}
