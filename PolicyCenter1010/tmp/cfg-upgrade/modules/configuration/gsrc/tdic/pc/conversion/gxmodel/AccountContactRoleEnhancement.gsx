package tdic.pc.conversion.gxmodel

uses entity.Contact
uses tdic.pc.conversion.common.logging.LoggerUtil

enhancement AccountContactRoleEnhancement : tdic.pc.conversion.gxmodel.accountcontactrolemodel.types.complex.AccountContactRole {

  function populateContactRole(period : PolicyPeriod) : entity.Contact {
    final var _logger = LoggerUtil.CONVERSION
    var contact = this.AccountContact.Contact.$TypeInstance.findOrCreateContact(period)
    return contact

  }

  private function isDuplicate(period : PolicyPeriod, contact : Contact, subType : typekey.AccountContactRole) : boolean {
    for (role in period.PolicyContactRoles*.AccountContactRole.where(\elt -> elt.Subtype == subType)) {
      if (role.AccountContact.Contact.DisplayName == contact.DisplayName) {
        return true
      }
    }
    return false
  }

}

