package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2210NJ_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if (context.Period.GLLine?.BasedOn != null and !(context.Period.Job typeis Renewal)) {
        if (context.Period.GLLine?.GLNJDeductibleCov_TDICExists and
            (context.Period.GLLine?.GLNJDeductibleCov_TDIC?.GLNJDedPerClaimLimit_TDICTerm?.Value != 99 or
                context.Period.GLLine?.GLNJDeductibleCov_TDIC?.GLNJDedAggLimit_TDICTerm?.Value != 99)) {
          return context.Period.GLLine?.BasedOn?.GLNJDeductibleCov_TDICExists ?
              context.Period.GLLine?.BasedOn?.GLNJDeductibleCov_TDIC?.GLNJDedPerClaimLimit_TDICTerm?.Value !=
                  context.Period.GLLine?.BasedOn?.GLNJDeductibleCov_TDIC?.GLNJDedPerClaimLimit_TDICTerm?.Value or
                  context.Period.GLLine?.BasedOn.GLNJDeductibleCov_TDIC?.GLNJDedAggLimit_TDICTerm?.Value !=
                      context.Period.GLLine?.GLNJDeductibleCov_TDIC?.GLNJDedAggLimit_TDICTerm?.Value : true
        }
      } else {
        return context.Period.GLLine?.GLNJDeductibleCov_TDICExists and
            context.Period.GLLine?.GLNJDeductibleCov_TDIC?.GLNJDedPerClaimLimit_TDICTerm?.Value != 99 and
            context.Period.GLLine?.GLNJDeductibleCov_TDIC?.GLNJDedAggLimit_TDICTerm?.Value != 99
      }
    }
    return false
  }
}