package tdic.pc.integ.plugins.numgen

uses com.guidewire.pl.web.controller.UserDisplayableException
uses com.tdic.util.properties.PropertyUtil
uses gw.api.locale.DisplayKey
uses gw.api.system.database.SequenceUtil
uses gw.api.upgrade.Coercions
uses gw.api.util.StringUtil
uses gw.plugin.InitializablePlugin
uses gw.plugin.policynumgen.IPolicyNumGenPlugin
uses org.slf4j.LoggerFactory

/**
 * US945, DE69
 * 01/11/2015 Rob Kelly
 *
 * Policy number plugin implementation for TDIC.
 * Policy numbers are ten digits long and start from 0000008000 to facilitate continuity with legacy policy numbers.
 */
class TDICPolicyNumGenPlugin implements IPolicyNumGenPlugin, InitializablePlugin {

  /**
   * GW-818 - changed value to scriptparamter value for first policy number.
   */
  private static final var _WC7FirstPolicyNumberGen = PropertyUtil.getInstance().getProperty("TDIC_WC7FirstPolicyNumberGen")
  private static var LOGGER = LoggerFactory.getLogger("TDIC_INTEGRATION")
  private static final var CLASS_NAME = TDICPolicyNumGenPlugin.Type.Name

  /**
   * The sequence to use for generating policy numbers.
   */
  private var sequenceName : String

  construct() {
  }

  /**
   * Processes the parameters passed to this plugin.
   * Looks for a single parameter specifying the sequence to use for generating policy numbers.
   */
  @Param("aParamMap", "the parameters passed to this plugin as a name-value map")
  override property set Parameters(aParamMap : Map<Object, Object>) {
    this.sequenceName = aParamMap.get("SequenceName") as String
  }

  /**
   * Generates a policy number for the specified PolicyPeriod.
   * If the specified PolicyPeriod already has a valid policy number then that policy number is returned.
   */
  @Param("aPolicyPeriod", "the policy period for which the poliy number is required")
  @Returns("the policy number as a String")
  @Throws(UserDisplayableException, "if a policy number could not be generated")
  override function genNewPeriodPolicyNumber(aPolicyPeriod : PolicyPeriod) : String {

    if (this.sequenceName == null) {

      throw new UserDisplayableException(DisplayKey.get("TDIC.NumberGenerator.Policy.NoSequence"))
    }

    if (aPolicyPeriod == null) {

      throw new UserDisplayableException(DisplayKey.get("TDIC.NumberGenerator.Policy.NullPolicyPeriod"))
    }
    if (aPolicyPeriod.isLegacyConversion && aPolicyPeriod.LegacyPolicyNumber_TDIC != null
        && aPolicyPeriod.LegacyPolicySuffix_TDIC != null && aPolicyPeriod.LegacyPolicySource_TDIC != null) {
      if (aPolicyPeriod.LegacyPolicySource_TDIC == "DIMS") {
        aPolicyPeriod.PolicyNumber = null
      } else {
        return getConvertedPolicyNumber(aPolicyPeriod)
      }
    }
    if (aPolicyPeriod.PolicyNumber != null and aPolicyPeriod.PolicyNumber != "Unassigned" and not isNewPolicyNumberRequired(aPolicyPeriod)) {

      return aPolicyPeriod.PolicyNumber
    }

    return StringUtil.formatNumber(SequenceUtil.next(Coercions.makePLongFrom(_WC7FirstPolicyNumberGen), this.sequenceName), "0000000000")
  }

  /**
   * Returns true if the specified PolicyPeriod requires a new policy number; false otherwise.
   */
  private function isNewPolicyNumberRequired(aPolicyPeriod : PolicyPeriod) : boolean {

    if (aPolicyPeriod.Job typeis Rewrite and aPolicyPeriod.Job.ChangePolicyNumber) {

      return true
    }

    return false
  }

  private function getConvertedPolicyNumber(aPolicyPeriod : PolicyPeriod) : String {
    var methodName = "getConvertedPolicyNumber"
    LOGGER.info(CLASS_NAME + ": " + methodName + ": " + "Policy number generation for legacy policy  - START")
    var convertedPolicyNumber : String
    try {
      // Legacy Policy Number = 0771626 ----> New Policy Number = 8077162601
      //var _legacyPolicySource = aPolicyPeriod.LegacyPolicySource_TDIC
      //var _legacyIdentifier  = PolicyNumberUtility.getLegacyIdentifier(_legacyPolicySource)
      //var _legacyPolicyNumber = aPolicyPeriod.LegacyPolicyNumber_TDIC
      //var _legacyPolicySuffix = aPolicyPeriod.LegacyPolicySuffix_TDIC
      convertedPolicyNumber = aPolicyPeriod.PolicyNumber//_legacyIdentifier + _legacyPolicyNumber + _legacyPolicySuffix
    } catch (ex : Exception) {
      LOGGER.error(CLASS_NAME + ": " + methodName + ": " + "Error generating Converted policy number - " + ex.StackTraceAsString)
      throw (ex)
    }
    LOGGER.info("Legacy Policy Number : " + aPolicyPeriod.PolicyNumber + " -- Converted Policy Number : " + convertedPolicyNumber)
    LOGGER.info(CLASS_NAME + ": " + methodName + ": " + "Policy number generation for legacy policy  - END")
    return convertedPolicyNumber
  }
}
