package tdic.pc.conversion.gxmodel

uses gw.webservice.pc.pc1000.gxmodel.SimpleValuePopulator


enhancement BOPBldgMortgageesEnhancement : tdic.pc.conversion.gxmodel.policymortgagee_tdicmodel.types.complex.PolicyMortgagee_TDIC {

  function populateBOPBldgMortgagee(bldg : BOPBuilding, mortgagee : PolicyMortgagee_TDIC) : PolicyMortgagee_TDIC {
    SimpleValuePopulator.populate(this, mortgagee)
    this.AccountContactRole.AccountContact.Contact.$TypeInstance.populateContact(mortgagee.AccountContactRole.AccountContact.Contact)
    return mortgagee
  }


}
