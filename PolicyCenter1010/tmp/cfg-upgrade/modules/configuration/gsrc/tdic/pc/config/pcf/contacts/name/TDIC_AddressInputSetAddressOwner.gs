package tdic.pc.config.pcf.contacts.name

uses gw.pcf.contacts.AbstractInputSetAddressOwner
uses gw.api.address.AddressOwnerFieldId
uses java.util.Set
uses gw.api.address.AddressCountrySettings

/**
 * Created with IntelliJ IDEA.
 * User: ssheridan
 * Date: 19/08/14
 * Time: 14:14
 * This class is used to determine the properties of UI fields in AccountSearchGlobalAddressInputSet.default.pcf.
 */
/**
 * GW103 Hermia Kho 09/29/2015
 * Exclude County from var _visible.
 */
class TDIC_AddressInputSetAddressOwner extends AbstractInputSetAddressOwner {

  var _address : Address
  /* Set of gw.api.name.NameOwnerFieldId objects to specify visible fields. */
  var _visible : Set<AddressOwnerFieldId> = {AddressOwnerFieldId.COUNTRY,
  //                                            AddressOwnerFieldId.COUNTY,
                                                AddressOwnerFieldId.ADDRESSTYPE}

  construct(theAddress : Address, isNonSpecific : boolean, isMovable : boolean) {
    super(isNonSpecific, isMovable)
    _address = theAddress
  }

  override property get Address(): entity.Address {
    return _address
  }

  override property set Address(value: entity.Address) {
    _address = value
  }

  /**
   * This method is used to determine the visibility of UI fields in AccountSearchGlobalAddressInputSet.default.pcf.
   */
  @Param("fieldId", "representation of a UI field")
  @Returns("Boolean based on the required visibilty of the field.")
  override function isVisible(fieldId : AddressOwnerFieldId) : boolean {
    // if field is specified as visible - return true
    if(_visible.contains(fieldId)){
      return true
    }
    else{
      return false
    }
  }

}