package tdic.pc.common.batch.unistat.dto

uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator


/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/18/14
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCstatUnitTotalFields extends TDIC_FlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _exposureStateCode : int as ExposureStateCode
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _reportLevelCode : int as ReportLevelCode
  var _correctionSeqNum : String as CorrectionSeqNum
  var _recordTypeCode : int as RecordTypeCode
  var _exposurePayrollTotal : long as ExposurePayrolltotal
  var _futureReserved1 : String as FutureReserved1
  var _subjectPremiumTotal : long as SubjectPremiumTotal
  var _standardPremiumTotal : long as StandardPremiumTotal
  var _claimCountTotal : int as ClaimCountTotal
  var _incurredIndemnityAmountTotal : long as IncurredIndemnityAmountTotal
  var _incurredMedicalAmountTotal : long as IncurredMedicalAmountTotal
  var _recordsInUnitReportTotal : int as RecordsInUnitReportTotal
  var _previouslyReportedCode : String as PreviouslyReportedCode
  var _futureReserved2 : String as FutureReserved2
  var _paidIndemnityAmountTotal : String as PaidIndemnityAmountTotal
  var _paidMedicalAmountTotal : String as PaidMedicalAmountTotal
  var _claimantsAttorneyFeesIncAmntTotal : String as ClaimantsAttorneyFeesIncAmntTotal
  var _employersAttorneyFeesIncAmntTotal : String as EmployersAttorneyFeesIncAmntTotal
  var _paidAllocatedLossAdjExpenseAmntTotal : String as PaidAllocatedLossAdjExpenseAmntTotal
  var _incurredAllocatedLossAdjExpenseAmntTotal : String as IncurredAllocatedLossAdjExpenseAmntTotal
  var _futureReserved3 : String as FutureReserved3

}