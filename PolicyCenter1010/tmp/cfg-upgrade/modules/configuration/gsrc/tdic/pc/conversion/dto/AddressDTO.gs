package tdic.pc.conversion.dto

uses tdic.pc.conversion.util.ConversionUtility

class AddressDTO {

  var _locationCode : String as LocationCode
  var _locationName : String as LocationName
  var _addressLine1 : String as AddressLine1
  var _addressLine2 : String as AddressLine2
  var _addressLine3 : String as AddressLine3
  var _addressType : String as AddressType
  var _city : String as City
  var _state : String as State
  var _postalCode : String as PostalCode
  var _county : String as County
  var _country : String as Country

  property get PostalCode() : String {
    return ConversionUtility.formatPostalCode(this._postalCode)
  }

}