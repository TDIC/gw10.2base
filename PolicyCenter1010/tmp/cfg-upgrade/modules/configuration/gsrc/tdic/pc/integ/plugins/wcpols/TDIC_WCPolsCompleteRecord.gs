package tdic.pc.integ.plugins.wcpols

uses java.util.ArrayList

uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsRecordTypes
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReportHeaderFields
uses tdic.pc.integ.plugins.wcpols.dto.TDIC_WCpolsReportTrailerFields
uses tdic.util.flatfile.model.classes.Record
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 1/5/15
 * Time: 4:29 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_WCPolsCompleteRecord {

  private var _logger = LoggerFactory.getLogger("TDIC_BATCH_WCPOLSREPORT")

  var _reportHeaderRecord : TDIC_WCpolsReportHeaderFields as ReportHeaderRecord
  var _wcpolsrecords : ArrayList<TDIC_WCpolsRecordTypes> as WCPolsRecords
  var _reportTrailerRecord : TDIC_WCpolsReportTrailerFields as ReportTrailerRecord



  function convertToFlatFileRecord(records:Record[]) :String{
    var flatFileContents=""
    var headerContents=""
    var trailerContents=""
    var recordContents=""


    for(record in records){
      _logger.debug("************************"+record.RecordType+"*****************************")
      switch (record.RecordType) {
        case "Report Header Record":
            headerContents += this.ReportHeaderRecord.createFlatFileLine(record)+"\r\n"
            break
        case "Trailer Record":
            trailerContents += ReportTrailerRecord.createFlatFileLine(record)
            break
          default:
          break
      }

    }
    this.WCPolsRecords.each(\elt -> {
      recordContents += elt.convertToFlatFileRecord(records)
    })

    return headerContents+  recordContents + trailerContents

  }

}