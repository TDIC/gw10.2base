package tdic.pc.config.rating.pl.params

uses java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: AnkitaG
 * Date: 10/21/2019
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */
class PLCovRatingParams {
  var _cyberLimit : BigDecimal as CyberLimit


  construct(line: GLLine){
    _cyberLimit = (line.GLCyberLiabCov_TDICExists)? line.GLCyberLiabCov_TDIC.GLCybLimit_TDICTerm.Value : null
  }
}