package tdic.pc.config.job.audit

uses gw.job.audit.AuditScheduler
uses gw.plugin.job.IAuditSchedulePatternSelectorPlugin
uses gw.plugin.Plugins
uses gw.api.productmodel.AuditSchedulePattern
uses gw.plugin.messaging.BillingMessageTransport
uses gw.api.util.DateUtil
uses java.math.BigDecimal

/**
 * DE48
 * Shane Sheridan 11/21/2014
 */
class TDIC_AuditScheduler extends AuditScheduler {

  construct() {
    super()
  }

  /**
   * DE48
   * Shane Sheridan 11/24/2014
   *
   * This is a copy of the OOTB scheduleAllAudits function with added modifications for TDIC.
   */
  static function scheduleAllAudits_TDIC(period : PolicyPeriod) {
    if (period.AuditInformations.Count > 0) {
      throw "Policy period should have no audits scheduled when \"scheduleAllAudits()\" is called"
    }

    scheduleFinalAudit_TDIC(period, false)
    scheduleSeriesCheckingAudits(period)
    scheduleSingleCheckingAudit(period)
    schedulePremiumReports(period)
  }

  /**
   * DE48
   * Shane Sheridan 11/21/2014
   *
   * This is a copy of the OOTB scheduleFinalAudit function with modifications for TDIC.
   */
  static function scheduleFinalAudit_TDIC(period : PolicyPeriod, isCancellation : boolean) {
    var todaysDate = DateUtil.currentDate()

    var isPolicyChangeOnCancelledPolicy : boolean = (period.Job typeis PolicyChange) && (period.CancellationDate != null)

    if (period.FinalAuditOption != TC_NO) {
      var pattern = selectFinalAuditSchedulePattern(period, isCancellation)
      if (pattern != null) {
        var auditMethod = getPlannedAuditMethod(period)

        // calculate audit start date
        var baseInitDate = period.EndOfCoverageDate

        var auditStartDate : java.util.Date
        // if job is a Cancellation or PolicyChange on a policy cancelled mid-term.
        if((isCancellation or isPolicyChangeOnCancelledPolicy ) and (period.EndOfCoverageDate > period.PeriodStart) ){
          //GW-424: For midterm cancellations, the audit start date should be the day after the cancellation was processed
          //GW-625: For future dated cancelled policies; audit process/start date should be
          // “Cancellation effective date + 1 day”
          if(period.EndOfCoverageDate.before(todaysDate)){
            if(isCancellation){
            baseInitDate = period.Cancellation?.CancelProcessDate
          }
            else  {  // if isPolicyChangeOnCancelledPolicy find the relevant CancelProcessDate
              var thisCancel = period.Policy.Jobs.sortBy(\j -> j.LatestPeriod.ModelNumber).whereTypeIs(Cancellation)
                                               .where(  \ c ->  c.LatestPeriod.CancellationDate == period.CancellationDate ).last()
              baseInitDate = thisCancel.CancelProcessDate
              /*Check that this cancel process date is not in the past - this could occur if a policy had multiple cancellations and we have only reinstated one */
              if((baseInitDate).before(todaysDate))
                baseInitDate = todaysDate
            }
          }
          //setting start date to be the same for cancel and policychange on Cancelled Policy
          auditStartDate = baseInitDate.addDays(ScriptParameters.TDIC_WC7AdditionalToCalculateFinalAuditStartDateIfCancelled)
        }
        else{  //For Submission, Renewal, Reinstatement, Policychange
             if(period.EndOfCoverageDate.before(todaysDate)){  // for submissions, renewal, reinstatement where coverage expires before today
               baseInitDate = todaysDate
               auditStartDate = baseInitDate.addDays(1)     //schedule audit for tomorrow
             }
             else {
               auditStartDate = baseInitDate.addDays(ScriptParameters.TDIC_WC7AdditionalToCalculateFinalAuditStartDate)   //else use expiration + script parameter
             }

        }

        var auditDueDate = auditStartDate.addDays(ScriptParameters.TDIC_WC7DaysAfterStartDateToSetDueDate)
        pattern.createAuditInformation(period,
            period.PeriodStart,
            period.EndOfCoverageDate,
            auditStartDate,
            auditDueDate,
            AuditScheduleType.TC_FINALAUDIT,
            auditMethod,
            false)
        period.addEvent(PolicyPeriod.SCHEDULEFINALAUDIT_EVENT)

        var thisAuditInfo = period.Policy.AllAuditInformations.firstWhere( \ elt -> elt.PolicyTerm == period.PolicyTerm)
        if(auditMethod == typekey.AuditMethod.TC_PHYSICAL){
          thisAuditInfo.PhysicalAuditFlagged_TDIC = todaysDate
        }

        thisAuditInfo.NoticeSentHolder_TDIC = period.EndOfCoverageDate.addDays(ScriptParameters.TDIC_WC7DaysAfterRenewalToSendPolicyHolderNotice)
      }
    }
  }

  /**
   * DE133
   * Shane Sheridan 02/23/2015
   *
   * this must be static because it is called from a static function.
  */
  public static function getPlannedAuditMethod(policyPeriod : PolicyPeriod) : typekey.AuditMethod {
    final var MAX_PHYSICAL_THRESHOLD_PREMIUM = 10000
    final var MAX_PHYSICAL_THRESHOLD_PAYROLL = 800000
    //20180206 TJT GW-3038: Physical if payroll exceeds $800,000
    if(policyPeriod.WC7LineExists) {
      var payrollAmount = policyPeriod.WC7Line.AllWC7CoveredEmployeeBaseWM.where(\elt -> elt.BasisTypeIsPayroll == true).sum(\elt -> elt.BasisAmount)

      //20180306 TJT GW-3038: exclude the terrorism cost
      var terrorismAmount : BigDecimal = 0.00
      for (eachWC7JurisdictionCost in policyPeriod.WC7Line.WC7Jurisdictions*.Costs) {
        if (eachWC7JurisdictionCost.JurisdictionCostType == typekey.WC7JurisdictionCostType.TC_TERRORPREM) {
          terrorismAmount += (eachWC7JurisdictionCost.ActualAmount_amt == null ? 0 : eachWC7JurisdictionCost.ActualAmount_amt)
        }
      }

      var premiumAmount = policyPeriod.TotalPremiumRPT.Amount - terrorismAmount
      if (premiumAmount > MAX_PHYSICAL_THRESHOLD_PREMIUM or payrollAmount > MAX_PHYSICAL_THRESHOLD_PAYROLL) {
        return typekey.AuditMethod.TC_PHYSICAL
      }
    }

    return typekey.AuditMethod.TC_VOLUNTARY
  }

  /**
   * DE48
   * Shane Sheridan 11/21/2014
   *
   * This is a complete copy from the superclass but needed to be defined here because it is private and superclass
   * is locked from configuration.
   */
  private static function selectFinalAuditSchedulePattern(period : PolicyPeriod, isCancellation : boolean) : AuditSchedulePattern {
    var plugin = Plugins.get(IAuditSchedulePatternSelectorPlugin)
    return (isCancellation
        ? plugin.selectFinalAuditSchedulePatternForCancellation(period)
        : plugin.selectFinalAuditSchedulePatternForExpiredPolicy(period))
  }

}