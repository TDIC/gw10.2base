package tdic.pc.config.gl.historicaldata


uses gw.api.database.Query
uses gw.api.database.Relop
uses entity.GLHistoricalParameters_TDIC

/**
 * Britto S - 01/28/2020
 * GL Historical information helper
 */
class GLHistoryHelper {
  public static var _retroActiveThreshold : Integer as RetroActiveThresholdYears = 5

  /**
   *
   * @return
   */
  public static function canEditHistory(period : PolicyPeriod) : boolean {
    return perm.System.updatehistoricalratingparameters_tdic
  }

  /**
   *
   * @return
   */
  public static function canRefreshAS400History(period : PolicyPeriod) : Boolean {
    return perm.System.updatehistoricalratingparameters_tdic and not(period.Job typeis Submission)
  }

  public static function canApplyHistory(period : PolicyPeriod) : boolean {
    return (period.GLLineExists and period.Offering.CodeIdentifier == "PLClaimsMade_TDIC")
  }
  /**
   * @param period
   * @return
   */
  public static function getHistoryFor(period : PolicyPeriod) : GLHistoricalParameters_TDIC[] {
    var history : GLHistoricalParameters_TDIC[] = {}
    var lowestRetroActiveDate = period.PeriodStart.addYears(-RetroActiveThresholdYears)
    var retroActiveDate = period.GLLine.GLPriorActsCov_TDIC.GLPriorActsRetroDate_TDICTerm.Value
    lowestRetroActiveDate = (lowestRetroActiveDate.before(retroActiveDate)) ? retroActiveDate : lowestRetroActiveDate
    history = period.GLHistoricalParameters_TDIC.where(\elt -> elt.refresh() != null)
    if(history != null and history.Count > 0) {
      history = history.where(\elt -> elt.ExpirationDate.afterOrEqual(lowestRetroActiveDate) and elt.ExpirationDate.beforeOrEqual(period.PeriodStart))
    }
    return history
  }
}