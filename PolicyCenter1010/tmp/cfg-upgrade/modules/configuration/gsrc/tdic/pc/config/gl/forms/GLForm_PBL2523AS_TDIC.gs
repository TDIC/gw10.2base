package tdic.pc.config.gl.forms

uses entity.FormAssociation
uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_PBL2523AS_TDIC extends AbstractMultipleCopiesForm<GLStateExclSched_TDIC> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLStateExclSched_TDIC> {
    var period = context.Period
    var glStateExclSchedItems = period.GLLine?.GLStateExclSched_TDIC
    var stateExclItems : ArrayList<GLStateExclSched_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLClaimsMade_TDIC" and period.GLLine?.GLStateExclCov_TDICExists and
        glStateExclSchedItems.HasElements){
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var stateExclSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis GLStateExclSched_TDIC)*.Bean
        if(glStateExclSchedItems.hasMatch(\item -> item.BasedOn == null)){
          stateExclItems.add(glStateExclSchedItems.firstWhere(\item -> item.BasedOn == null))
        }
        else if(glStateExclSchedItems.hasMatch(\item -> stateExclSchedLineItemsModified.contains(item))) {
          stateExclItems.add(glStateExclSchedItems.firstWhere(\item -> stateExclSchedLineItemsModified.contains(item)))
        }
      }
      else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(period.RefundCalcMethod != CalculationMethod.TC_FLAT){
          stateExclItems.add(glStateExclSchedItems.first())
        }
      }
      else {
        stateExclItems.add(glStateExclSchedItems.first())
      }
      return stateExclItems.HasElements? stateExclItems.toList(): {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "GLStateExclSched_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("LTEffectiveDate", _entity.LTEffectiveDate as String))
    contentNode.addChild(createTextNode("LTExpirationDate", _entity.LTExpirationDate as String))
    contentNode.addChild(createTextNode("State", _entity.State))
    contentNode.addChild(createTextNode("LicenseNumber", _entity.LicenseNumber))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }
}