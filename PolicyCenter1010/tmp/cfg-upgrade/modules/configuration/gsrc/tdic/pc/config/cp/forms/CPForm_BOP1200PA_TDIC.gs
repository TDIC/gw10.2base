package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: Osman
 * Date: 9/3/2020
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_BOP1200PA_TDIC extends AbstractMultipleCopiesForm<BOPLocation> {
    override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<BOPLocation> {
    var listOfLocations : ArrayList<BOPLocation> = {}
    if(context.Period.Job typeis Cancellation and context.Period.RefundCalcMethod == CalculationMethod.TC_FLAT){
    return {}
    }
    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
    listOfLocations.add(context.Period.BOPLine?.BOPLocations.orderByDescending(\loc -> loc.CreateTime).first())
    return listOfLocations.HasElements ? listOfLocations?.toList() : {}
    }
    return {}
    }

    override property get FormAssociationPropertyName() : String {
    return "BOPLocation_TDIC"
    }

    function isValidPolicyChangeReasonsBOP_TDIC(pp:PolicyPeriod) : boolean{
    var reason = pp?.Job?.ChangeReasons
    if (reason.hasMatch(\elt1 -> elt1.ChangeReason == ChangeReasonType_TDIC.TC_ACCOUNTSRECEIVABLECHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_ACTUALCASHVALUEENDROSEMENT ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_ADDITIONALINSURED ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_BLDGLIMITSINCREASEDECREASE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_BPPLIMITSINCREASEDECREASE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_CLOSEDENDWATERCREDIT||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_CONSTRUCTIONTYPE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_DEDUCTIBLECHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_EMPLOYEEDISHONESTYCHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_ENTITYORGANIZATIONTYPE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_EQEQSL ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_EXTRAEXPENSE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_FEIN ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_FINEARTSCHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_FUNGIMOLDCHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_GOLDANDOTHERPRECIOUSMETALS ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_ILMINESUBSIDENCE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_LOCATIONCHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_LOSSOFINCOMECHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_LOSSPAYEE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_MAILINGADDRESS ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_MANUSCRIPENDORSEMENT ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_MONEYANDSECURITIESCHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_MORTGAGEE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_MULTILINEDISCOUNT ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_NAMEDINSURED ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_PROTECTIONCLASSTERRITORYCHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_SIGNSCOVERAGELIMITCHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_SPRINKLER ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_VALUABLEPAPERSCHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_DENTALGENERALLIABAI ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_DENTALGENERALLIAB ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_PROFILECHANGE ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_SQFT ||
    elt1.ChangeReason == ChangeReasonType_TDIC.TC_ENHANCEDCOVERAGEENDORSEMENT
    )) {
    return true
    }
    return false

    }
    override function addDataForComparisonOrExport(contentNode : XMLNode) {
    if (_entity.Branch.isChangeReasonOnlyRegenerateDeclaration_TDIC) {
    contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber))
    }
    if(isValidPolicyChangeReasonsBOP_TDIC(_entity.Branch) && _entity.Branch.Offering.CodeIdentifier=="BOPBusinessOwnersPolicy_TDIC"){
    contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber))
    }
    }

    override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
    }


    }