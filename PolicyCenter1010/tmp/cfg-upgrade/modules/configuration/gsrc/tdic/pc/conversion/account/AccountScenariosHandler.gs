package tdic.pc.conversion.account

uses com.guidewire.pl.system.exception.DBDuplicateKeyException
uses gw.account.AccountQueryBuilder
uses gw.address.AddressQueryBuilder
uses gw.api.database.ISelectQueryBuilder
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths
uses gw.contact.ContactQueryBuilder
uses tdic.pc.conversion.common.logging.LoggerUtil
uses tdic.pc.conversion.constants.ConversionConstants
uses tdic.pc.conversion.exception.DataConversionException
uses tdic.pc.conversion.gxmodel.policyperiodmodel.anonymous.elements.PolicyPeriod_Policy_Account
uses tdic.pc.conversion.helper.CommonConversionHelper
uses tdic.pc.conversion.services.TDICAccountAPI

class AccountScenariosHandler {

  private static final var CLASS_NAME = AccountConversionHelper.Type.Name
  private var _accountModel : PolicyPeriod_Policy_Account
  private var _policyNumber : String
  private var _logger = LoggerUtil.CONVERSION

  var _account : Account

  construct(accountModel : PolicyPeriod_Policy_Account, policyNumber : String) {
    _accountModel = accountModel
    _policyNumber = policyNumber
  }

  function findAccount() : Account {
    if (!canCreateAccount()) {
      return _account
    }
    createAccount()
    return _account
  }

  private function canCreateAccount() : boolean {
    if(ConversionConstants.SPECIAL_ACCOUNTS.contains(_policyNumber.substring(1,_policyNumber.length))){
      return true
    }

    var accountQueryBuilder = new AccountQueryBuilder()

    // account holder contact restrictions
    if (_accountModel.AccountHolderContact.entity_Person.FirstName.NotBlank
        or _accountModel.AccountHolderContact.entity_Person.LastName.NotBlank
        or _accountModel.AccountHolderContact.Name.NotBlank
        or _accountModel.AccountHolderContact.PrimaryAddress.AddressLine1.NotBlank
        or _accountModel.AccountHolderContact.PrimaryAddress.AddressLine2.NotBlank
        or _accountModel.AccountHolderContact.PrimaryAddress.City.NotBlank
        or _accountModel.AccountHolderContact.PrimaryAddress.Country != null
        or _accountModel.AccountHolderContact.PrimaryAddress.County.NotBlank
        or _accountModel.AccountHolderContact.PrimaryAddress.PostalCode.NotBlank
        or _accountModel.AccountHolderContact.PrimaryAddress.State != null) {
      var contactQueryBuilder = new ContactQueryBuilder()
          .withFirstNameRestricted(_accountModel.AccountHolderContact.entity_Person.FirstName, StartsWithIgnoringCase)
          .withLastNameRestricted(_accountModel.AccountHolderContact.entity_Person.LastName, StartsWithIgnoringCase)
          .withCompanyNameRestricted(_accountModel.AccountHolderContact.Name, StartsWithIgnoringCase)
          .withCityDenormStarting(_accountModel.AccountHolderContact.PrimaryAddress.City)
          .withPostalCodeDenormStarting(_accountModel.AccountHolderContact.PrimaryAddress.PostalCode)
          .withStateDenorm(_accountModel.AccountHolderContact.PrimaryAddress.State)
      if (_accountModel.AccountHolderContact.PrimaryAddress.AddressLine1.NotBlank
          or _accountModel.AccountHolderContact.PrimaryAddress.AddressLine2.NotBlank
          or _accountModel.AccountHolderContact.PrimaryAddress.Country != null
          or _accountModel.AccountHolderContact.PrimaryAddress.County.NotBlank) {
        contactQueryBuilder.withPrimaryAddress(new AddressQueryBuilder()
            .withAddressLine1(_accountModel.AccountHolderContact.PrimaryAddress.AddressLine1)
            .withAddressLine2(_accountModel.AccountHolderContact.PrimaryAddress.AddressLine2)
            .withCountry(_accountModel.AccountHolderContact.PrimaryAddress.Country)
        )
      }

      accountQueryBuilder.withAccountHolderContact(contactQueryBuilder)
    }
    var foundAccountNumbers = (accountQueryBuilder.build() as ISelectQueryBuilder<Account>).select({QuerySelectColumns.path(Paths.make(Account#AccountNumber))}).transformQueryRow(\row -> row.getColumn(0) as String).toTypedArray()
    if (foundAccountNumbers.IsEmpty) {
      return true
    }
    _account = Query.make(Account).compare(Account#AccountNumber, Equals, foundAccountNumbers[0]).select().first()
    if(_account.AccountStatus == typekey.AccountStatus.TC_WITHDRAWN){
      return true
    }
    if(!_account.Policies.IsEmpty){
      return _account.Policies.firstWhere(\elt -> elt.ProductCode == ConversionConstants.WORKERSCOMP_PRODUCTCODE) != null
    }
    return true
  }

  private function createAccount() {
    var methodName = "createAccount"
    var accountNumber : String
    var accountDetails = new ArrayList<String>()
    try {
      _logger.info("AccountScenariosHandler : createAccount : start account creating for account key:${_accountModel.AccountHolderContact.Name}")
      //var account = tdic.pc.conversion.gxmodel.policyperiodmodel.anonymous.elements.PolicyPeriod_Policy_Account.parse(_accountModel.asUTFString())
      var producerCode : ProducerCode
      var producerCodeErrorString : String
      //accountNumber = account.AccountNumber
      //Check if the producer code is available

      for (prodCode in _accountModel.ProducerCodes.Entry) {

        producerCode = Query.make(ProducerCode).compare(ProducerCode#Code, Equals, prodCode.ProducerCode.Code).select().AtMostOneRow
        producerCodeErrorString = producerCodeErrorString + "," + prodCode.ProducerCode.Code
        if (producerCode != null)
          break
      }

      /*var producerCode = Query.make(ProducerCode).compare(ProducerCode#Code, Equals,
          account.ProducerCodes.Entry[0].ProducerCode.Code).select().AtMostOneRow*/

      if (producerCode == null)
        throw new DataConversionException("ProducerCodes " + producerCodeErrorString + " not found")

      var accountAPI = new TDICAccountAPI()
      accountNumber = accountAPI.addAccount(_accountModel)
      _account = Query.make(Account).compare(Account#AccountNumber, Equals, accountNumber).select().AtMostOneRow
      accountDetails.add(accountNumber)
      accountDetails.add(_accountModel.AccountHolderContact.Name)
      accountDetails.add(producerCode.Code)
      accountDetails.add(_accountModel.AccountOrgType.DisplayName)

    } catch (dce : DataConversionException) {
      _logger.error("${CLASS_NAME} :${methodName} : error details" + dce.StackTraceAsString)
      CommonConversionHelper.saveConversionError(null, accountNumber, null, "AC", 0, dce)
    } catch (dke : DBDuplicateKeyException) {
      _logger.error("${CLASS_NAME} :${methodName} : error details" + dke.StackTraceAsString)
      CommonConversionHelper.saveConversionError(null, accountNumber, null, "AC", 0, dke)
    } catch (e : Exception) {
      _logger.error("${CLASS_NAME} :${methodName} :error details " + e.StackTraceAsString)
      CommonConversionHelper.saveConversionError(null, accountNumber, null, "AC", 0, e)
      // throw new DataConversionException(e.Message)
    }

  }

}