package tdic.pc.common.batch.generalledger.prmlines

uses java.util.Map
uses java.math.BigDecimal
uses java.util.ArrayList
uses java.lang.StringBuilder
uses tdic.pc.common.batch.generalledger.prmpolicies.TDIC_PremiumObject
uses java.util.Date
uses java.util.List

/**
 * US627
 * 11/21/2014 Kesava Tavva
 *
 * This class to generate premium lines for reporting in Written premium batch file to Lawson
 *
 */
class TDIC_WrittenPremiumLines extends TDIC_PremiumLinesBase {

  private static final var RUN_GROUP : String = formatString("GW-LGL-WPM")
  private static var CLASS_NAME : String = "TDIC_WrittenPremiumLines"

  construct(ped : Date){
    super(ped)
  }

  /**
   * US627
   * 11/21/2014 Kesava Tavva
   *
   * This function builds premium line for each of aggregated amounts calculated based on LOB, State and
   * policy premium types i.e Premium, Expense constant, Terrorism Premium and state assessments. Also builds
   * corresponding offset line that needs to be reported for each premium line reported in premium file.
   *
   */
  @Param("map","type of Jurisdiction and map of Premium line type and its aggregated amounts")
  @Returns("List of Premium lines in string objects")
  override function generatePremiumLines(map: Map<typekey.Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>>): List<String> {
    var logPrefix = "${CLASS_NAME}#generatePremiumLines(map)"
    _logger.debug("${logPrefix} - Entering")
    var gwPrmLines : List<String>
    var offsetPrmLines : List<String>

    if(map?.Count == 0)
      return null

    _logger.debug("${logPrefix} - Premium lines build started")
    gwPrmLines = new ArrayList<String>()
    map.eachKeyAndValue( \ state, prmLineTypeMap -> {
      var refSeqNum : int = 0
      prmLineTypeMap?.eachKeyAndValue( \ lineType, prmObj -> {
        refSeqNum = refSeqNum + 1
        gwPrmLines.add(buildPremiumLine(typekey.PolicyLine.TC_WC7WORKERSCOMPLINE, state, lineType,prmObj, refSeqNum, Boolean.FALSE))
      })
    })
    _logger.debug("${logPrefix} - Premium lines build completed")

    _logger.debug("${logPrefix} - offset Premium lines build started")
    offsetPrmLines = new ArrayList<String>()
    map.eachKeyAndValue( \ state, prmLineTypeMap -> {
      var refSeqNum : int = prmLineTypeMap?.Count
      prmLineTypeMap?.eachKeyAndValue( \ lineType, prmObj -> {
        refSeqNum = refSeqNum + 1
        offsetPrmLines.add(buildPremiumLine(typekey.PolicyLine.TC_WC7WORKERSCOMPLINE, state, TDIC_PremiumLineTypeEnum.PRM_RCV_ACCOUNT, prmObj, refSeqNum,Boolean.TRUE))
      })
    })
    _logger.debug("${logPrefix} - offset Premium lines build completed")

    var prmLines : List<String> = new ArrayList<String>()
    if(gwPrmLines?.Count  > 0)
      prmLines.addAll(gwPrmLines)
    if(offsetPrmLines?.Count > 0)
      prmLines.addAll(offsetPrmLines)

    _logger.debug("${logPrefix} - All Premium lines build completed")
    _logger.debug("${logPrefix} - Exiting")
    return prmLines
  }

  /**
   * US627
   * 12/22/2014 Kesava Tavva
   *
   * This function builds premium line for each of aggregated amounts calculated based on LOB, State and
   * policy premium types i.e Premium, Expense constant, Terrorism Premium and state assessments. Also builds
   * corresponding offset line that needs to be reported for each premium line reported in premium file.
   *
   */
  @Param("lob", "typekey.PolicyLine")
  @Param("state", "typekey.Jurisdiction")
  @Param("lineType", "TDIC_PremiumLineTypeEnum")
  @Param("prmObj", "Premium object contains units and aggregated premium amount")
  @Param("refSeqNum", "int value of reference sequence number")
  @Param("offSet", "Boolean value. TRUE for offset lines and FALSE for non-offset lines")
  @Returns("String, premium line to be reported in batch output file")
  private function buildPremiumLine(lob : typekey.PolicyLine, state : typekey.Jurisdiction,
                                    lineType : TDIC_PremiumLineTypeEnum,
                                    prmObj : TDIC_PremiumObject,
                                    refSeqNum : int, offSet : Boolean) : String {
    var prmLine = new StringBuilder()
    prmLine.append(getRunGroup())
        .append(formatInteger(getSeqNumber(),TDIC_PremiumLineConstants.INT_PATTERN_6))
        .append(COMPANY).append(formatString(getOldCompany(lob, state, lineType, offSet)))
        .append(getOldAcctNumber(lineType))
        .append(SOURCE_CODE).append(getSystemDate())
        .append(getReference(state,refSeqNum))
        .append(getDescription(lob,state,lineType,offSet))
        .append(BLANK_STRING)
        .append(formatInteger(prmObj.Units))
        .append(formatBigDecimalWithDecimals(getTransAmount(prmObj.Amount, offSet)))
        .append(PrmSubLine1)
        .append(getPostingDate()).append(PrmSubLine2)
        .append(formatString(getNegativeAdj(prmObj.Amount, offSet)))
        .append(PrmSubLine3)
    return prmLine.toString()
  }
  /**
   * US627
   * 12/01/2014 Kesava Tavva
   *
   * This function returns static value of run group to be reported in each premium line
   */
  @Returns("String object value of RUN_GROUP")
  override function getRunGroup(): String {
    return  RUN_GROUP
  }

  /**
   * US627
   * 12/01/2014 Kesava Tavva
   *
   * This function returns Old company value based on lob and Jurisdiction state values
   */
  @Param("lob", "typekey.PolicyLine")
  @Param("state", "typekey.Jurisdiction")
  @Param("prmLineType", "TDIC_PremiumLineTypeEnum")
  @Param("offSet", "Boolean value. TRUE for offset lines and FALSE for non-offset lines")
  @Returns("String Object value of Old Company")
  override function getOldCompany(lob: typekey.PolicyLine, state: typekey.Jurisdiction, lineType: TDIC_PremiumLineTypeEnum, offSet : Boolean): String {
    var logPrefix = "${CLASS_NAME}#getOldCompany(lob, state, lineType)"
    _logger.debug("${logPrefix} - Entering")
    var oldCompany : String
    if(offSet)
       oldCompany = TDIC_PremiumLineConstants.ACCT_UNIT_PREFIX
              + TDIC_PremiumLineConstants.BAL_SHEET_ACCT_UNIT
              + TDIC_PremiumLineConstants.DOT_SEPARATOR
              + getMappedJurisdictionAccountCode(state as String)
              + TDIC_PremiumLineConstants.DOT_SEPARATOR
              + getMappedLOBAccountCode(lob as String)
    else{
      var auComponent = (lineType != TDIC_PremiumLineTypeEnum.STATE_ASSMNTS) ? TDIC_PremiumLineConstants.SALES_ACCT_UNIT : TDIC_PremiumLineConstants.BAL_SHEET_ACCT_UNIT
      oldCompany =TDIC_PremiumLineConstants.ACCT_UNIT_PREFIX
            + auComponent
            + TDIC_PremiumLineConstants.DOT_SEPARATOR
            + getMappedJurisdictionAccountCode(state as String)
            + TDIC_PremiumLineConstants.DOT_SEPARATOR
            + getMappedLOBAccountCode(lob as String)
    }
    _logger.debug("${logPrefix} - Exiting")
    return oldCompany
  }

  /**
   * US627
   * 12/01/2014 Kesava Tavva
   *
   * This function returns Old company value based on lob and Jurisdiction state and Premium line type values
   */
  @Param("lob", "typekey.PolicyLine")
  @Param("state", "typekey.Jurisdiction")
  @Param("prmLineType", "TDIC_PremiumLineTypeEnum")
  @Param("offSet", "Boolean value. TRUE for offset lines and FALSE for non-offset lines")
  @Returns("String Object value of Old Company")
  override function getDescription(lob : typekey.PolicyLine, state : typekey.Jurisdiction, prmLineType : TDIC_PremiumLineTypeEnum, offSet : Boolean): String {
    if(offSet) {
      return formatString("PREM RCVD FOR ${state} ${reportPeriodEndDate}")
    } else {
      if(prmLineType == TDIC_PremiumLineTypeEnum.STATE_ASSMNTS)
        return formatString("SURCH WC FOR ${state} ${reportPeriodEndDate}")
      else
        return formatString("WC PREMIUM FOR ${state} ${reportPeriodEndDate}")
    }
  }

  /**
   * US627
   * 12/18/2014 Kesava Tavva
   *
   * This function returns transaction amounts by applying negative adjustments
   * based on premium line type and offset lines
   */
  @Returns("BigDecimal value of Transaction amount")
  override function getTransAmount(amount : BigDecimal, offset : Boolean): BigDecimal {
    if(not offset)
      amount = amount * -1

    return amount
  }

  /**
   * US627
   * 12/02/2014 Kesava Tavva
   *
   * This function returns Negative adjustment value based on offset line type.
   * Returns blank for offset lines and Y for GW premium lines.
   */
  @Returns("String object value of negative adjustment")
  override function getNegativeAdj(amount : BigDecimal, offset : Boolean): String {
    return TDIC_PremiumLineConstants.BLANK_STRING
  }

}