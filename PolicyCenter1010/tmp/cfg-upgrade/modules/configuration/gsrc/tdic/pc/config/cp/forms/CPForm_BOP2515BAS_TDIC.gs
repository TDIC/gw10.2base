package tdic.pc.config.cp.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses entity.FormAssociation
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SanjanaS
 * Date: 02/01/2020
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_BOP2515BAS_TDIC extends AbstractMultipleCopiesForm<PolicyMortgagee_TDIC>{
  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<PolicyMortgagee_TDIC> {
    var listOfMortgagees : ArrayList<PolicyMortgagee_TDIC> = {}
    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
      var mortgagees =  context.Period.BOPLine?.BOPLocations*.Buildings*.BOPBldgMortgagees
      var mortgageesAddedOnPolicyChange = mortgagees?.where(\mortgagee -> mortgagee.BasedOn == null)
      if(context.Period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var mortgageesModifiedOnPolicyChange = changeList.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis PolicyMortgagee_TDIC)*.Bean
        var manuScriptModifiedOnPolicyChange = changeList.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis BOPManuscript_TDIC)*.Bean
        if (mortgageesAddedOnPolicyChange.HasElements) {
          listOfMortgagees.add(mortgageesAddedOnPolicyChange.first())
        }
        else if(mortgagees.hasMatch(\mg -> mortgageesModifiedOnPolicyChange.contains(mg))) {
          listOfMortgagees.add(mortgagees.firstWhere(\mg -> mortgageesModifiedOnPolicyChange.contains(mg)))
        }

        else if(manuScriptModifiedOnPolicyChange.HasElements){
          var mortgageeManuscript= (manuScriptModifiedOnPolicyChange.firstWhere(\elt ->  (elt as BOPManuscript_TDIC).PolicyMortgagee!=null)) as BOPManuscript_TDIC
          if(mortgageeManuscript!=null)
          listOfMortgagees.add(mortgageeManuscript.PolicyMortgagee)
        }
      }
      else if(context.Period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(context.Period.RefundCalcMethod != CalculationMethod.TC_FLAT and mortgagees.HasElements){
          listOfMortgagees.add(mortgagees.first())
        }
      }
      else if(!(context.Period.Job.Subtype == typekey.Job.TC_POLICYCHANGE or context.Period.Job.Subtype == typekey.Job.TC_CANCELLATION) and mortgagees.HasElements){
        listOfMortgagees.add(mortgagees.first())
      }
    }
    return listOfMortgagees.HasElements? listOfMortgagees.toList(): {}
  }

  override property get FormAssociationPropertyName() : String {
    return "PolicyMortgagee_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("LoanNumber", _entity.LoanNumber as String))
    contentNode.addChild(createTextNode("ExpirationDate_TDIC", _entity.ExpirationDate_TDIC?.toString()))
    contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber))
    if (_entity.ContactDenorm typeis Person) {
      contentNode.addChild(createTextNode("FirstName", _entity.FirstName))
      contentNode.addChild(createTextNode("MiddleName", _entity.MiddleName))
      contentNode.addChild(createTextNode("LastName", _entity.LastName))
      contentNode.addChild(createTextNode("DateOfBirth", _entity.DateOfBirth as String))
      contentNode.addChild(createTextNode("MaritalStatus", _entity.MaritalStatus as String))
      contentNode.addChild(createTextNode("Suffix", _entity.Suffix as String))
    }
    if (_entity.ContactDenorm typeis Company) {
      contentNode.addChild(createTextNode("CompanyName", _entity.CompanyName))
      contentNode.addChild(createTextNode("TaxID", _entity.TaxID))
    }
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }
}