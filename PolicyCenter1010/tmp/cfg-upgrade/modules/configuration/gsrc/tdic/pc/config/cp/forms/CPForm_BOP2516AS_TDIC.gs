package tdic.pc.config.cp.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.xml.XMLNode
uses entity.FormAssociation

/**
 * Description :
 * Create User: ChitraK
 * Create Date: 2/5/2020
 * Create Time: 4:49 PM
 * To change this template use File | Settings | File Templates.
 */
class CPForm_BOP2516AS_TDIC extends AbstractMultipleCopiesForm<BOPLocation> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<BOPLocation> {
    var listOfLocations: ArrayList<BOPLocation> = {}
    if (context.Period.Offering.CodeIdentifier == "BOPBusinessOwnersPolicy_TDIC") {
      var bopLocations = context.Period.BOPLine?.BOPLocations
      var buildingsWithBOPACVCov = bopLocations?.where(\loc -> loc.Buildings?.hasMatch(\building -> building.BOPACVEndCov_TDICExists))
      if(context.Period.Job.Subtype == typekey.Job.TC_POLICYCHANGE){
        var bopACVEndCovAddedOnPolicyChange = bopLocations.where(\loc -> loc.Buildings.hasMatch(\building -> building.BasedOn != null and !building.BasedOn.BOPACVEndCov_TDICExists
            and building.BOPACVEndCov_TDICExists))
        var bopACVEndCovRemovedOnPolicyChange = bopLocations.where(\loc -> loc.Buildings.hasMatch(\building -> building.BasedOn != null and building.BasedOn.BOPACVEndCov_TDICExists
            and !building.BOPACVEndCov_TDICExists))
        var buildingsWithNewlyAddedBOPACVEndCov = bopLocations.where(\loc -> loc.Buildings.hasMatch(\building ->  building.BasedOn == null and building.BOPACVEndCov_TDICExists))
        if(bopACVEndCovAddedOnPolicyChange.HasElements){
          listOfLocations.add(bopACVEndCovAddedOnPolicyChange.first())
        }
        else if(bopACVEndCovRemovedOnPolicyChange.HasElements){
          listOfLocations.add(bopACVEndCovRemovedOnPolicyChange.first())
        }
        else if(buildingsWithNewlyAddedBOPACVEndCov.HasElements){
          listOfLocations.add(buildingsWithNewlyAddedBOPACVEndCov.first())
        }
      }
      else if(context.Period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(context.Period.RefundCalcMethod != CalculationMethod.TC_FLAT and buildingsWithBOPACVCov.HasElements){
          listOfLocations.add(buildingsWithBOPACVCov.first())
        }
      }
      else if((context.Period.Job.Subtype != typekey.Job.TC_POLICYCHANGE or context.Period.Job.Subtype != typekey.Job.TC_CANCELLATION)
          and buildingsWithBOPACVCov.HasElements){
        listOfLocations.add(buildingsWithBOPACVCov.first())
      }
      return listOfLocations.HasElements? listOfLocations.toList(): {}
    }
    return {}
  }

  override property get FormAssociationPropertyName() : String {
    return "BOPLocation_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    contentNode.addChild(createTextNode("JobNumber", _entity.Branch.Job.JobNumber))
    contentNode.addChild(createTextNode("AddressLine1", _entity.Location.AddressLine1))
    contentNode.addChild(createTextNode("AddressLine2", _entity.Location.AddressLine2))
    contentNode.addChild(createTextNode("City", _entity.Location.City))
    contentNode.addChild(createTextNode("PostalCode", _entity.Location.PostalCode))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new CPFormAssociation_TDIC(form.Branch)
  }
}

