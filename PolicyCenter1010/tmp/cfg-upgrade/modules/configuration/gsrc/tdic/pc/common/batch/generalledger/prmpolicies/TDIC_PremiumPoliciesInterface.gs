package tdic.pc.common.batch.generalledger.prmpolicies

uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLineTypeEnum

uses java.util.Map
uses tdic.pc.common.batch.generalledger.prmlines.TDIC_PremiumLinesInterface

/**
 * US627
 * 11/26/2014 Kesava Tavva
 *
 * Interface defined to get premium amounts for each batch period file.
 */
interface TDIC_PremiumPoliciesInterface {
  function getPremiumAmounts() : Map<typekey.Jurisdiction, Map<TDIC_PremiumLineTypeEnum, TDIC_PremiumObject>>
  function getPremiumLines() : TDIC_PremiumLinesInterface
}