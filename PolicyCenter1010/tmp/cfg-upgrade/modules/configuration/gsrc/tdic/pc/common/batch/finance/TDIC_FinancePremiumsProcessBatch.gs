package tdic.pc.common.batch.finance

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.DBFunction
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.pc.common.batch.finance.reports.util.TDIC_FinanceReportConstants

class TDIC_FinancePremiumsProcessBatch extends BatchProcessBase {
  private static final var CLASS_NAME = TDIC_FinancePremiumsProcessBatch.Type.RelativeName
  private static final var TERMINATE_SUB : String = "Terminate::${gw.api.system.server.ServerUtil.ServerId}-PC Finance Premiums Batch Job Terminated"

  private var _jobCloseDate : Date as readonly JobCloseDate
  private var _emailTo : String as readonly EmailTo
  private var _emailRecipient : String as readonly EmailRecipient

  construct(args : Object[]) {
    super(BatchProcessType.TC_FINANCEPREMIUMSPROCESSBATCH_TDIC)
    initialize(args)
  }

  protected override function doWork() {

    try{
      var failedJobs = new ArrayList<String>()
      //Retrieve daily bound and audit completed transactions with jobs closed in previous day
      TDIC_FinanceReportConstants.LOGGER.info("${CLASS_NAME}#doWork(): retrieve policy periods for job close date: ${JobCloseDate}")
      var policyPeriods = retrievePolicyPeriods()
      TDIC_FinanceReportConstants.LOGGER.info("${CLASS_NAME}#doWork(): retrieve policy periods for job close date: ${JobCloseDate} completed; Count: ${policyPeriods?.Count}")
      //process finance
      for(pp in policyPeriods){
        incrementOperationsCompleted()
        try {
          TDIC_FinanceReportConstants.LOGGER.info("${CLASS_NAME}#doWork(): ${pp.PolicyNumber}-${pp.TermNumber}; ${pp.Job.JobNumber}; ${pp.Policy.ProductCode} - start")
          generatePremiums(pp)
          TDIC_FinanceReportConstants.LOGGER.info("${CLASS_NAME}#doWork(): ${pp.PolicyNumber}-${pp.TermNumber}; ${pp.Job.JobNumber}; ${pp.Policy.ProductCode} - end")
          if (TerminateRequested) {
            TDIC_FinanceReportConstants.LOGGER.warn("${CLASS_NAME}#doWork(): - Terminate requested during doWork() method")
            EmailUtil.sendEmail(EmailRecipient, TERMINATE_SUB, "PC Finance Premiums process daily batch for job close date: ${_jobCloseDate} is terminated.")
            return
          }
        }catch(e: Exception){
          incrementOperationsFailed()
          TDIC_FinanceReportConstants.LOGGER.error("${CLASS_NAME}#doWork(): Premiums process failed for ${pp.PolicyNumber}-${pp.TermNumber}; ${pp.Job.JobNumber}; Error: ${e.StackTraceAsString}")
          failedJobs.add(pp.Job.JobNumber)
        }
      }
      if(failedJobs.isEmpty())
        EmailUtil.sendEmail(EmailTo, getEmailSubject(Boolean.TRUE), "PC Finance premiums daily process completed for policies with job close date: ${JobCloseDate}.")
      else
        EmailUtil.sendEmail(EmailTo, getEmailSubject(Boolean.FALSE), "PC Finance premiums daily process failed for below jobs with job close date: ${JobCloseDate}. \n ${failedJobs}")
    }catch(e: Exception){
      TDIC_FinanceReportConstants.LOGGER.error("${CLASS_NAME}#send(): Policy premiums process failed. Job close date: ${JobCloseDate}. Error details are: ${e.StackTraceAsString}")
    }
  }

  private function retrievePolicyPeriods(): IQueryBeanResult<PolicyPeriod> {
    var financePremiumsQuery = Query.make(FinancePremiums_TDIC)
    financePremiumsQuery.compare(DBFunction.DateFromTimestamp(financePremiumsQuery.getColumnRef("JobCloseDate")),Equals, JobCloseDate)

    var ppQuery = Query.make(entity.PolicyPeriod).compareIn(PolicyPeriod#Status, {PolicyPeriodStatus.TC_BOUND,PolicyPeriodStatus.TC_AUDITCOMPLETE})
    var jobQuery = ppQuery.join(PolicyPeriod#Job)
    jobQuery.compare(DBFunction.DateFromTimestamp(jobQuery.getColumnRef("CloseDate")),
        Equals, JobCloseDate)
    jobQuery.subselect("JobNumber", CompareNotIn, financePremiumsQuery, "JobNumber")

    return ppQuery.select()
  }

  private function generatePremiums(pp: PolicyPeriod) {
    try{
      var transactions = pp.AllTransactions

      var transactionPremiums = new ArrayList<TDIC_FinanceTransactionPremiumObject>()

      for(t in transactions){
        transactionPremiums.add(new TDIC_FinanceTransactionPremiumObject(pp,t))
      }

      //Populate premiums
      var financePremium:FinancePremiums_TDIC
      var financeEarnedPremium: FinanceEarnedPremiums_TDIC
      gw.transaction.Transaction.runWithNewBundle(\bundle->{
        for(tp in transactionPremiums){
          financePremium = new FinancePremiums_TDIC()
          financePremium.PolicyPeriodID = pp.ID.Value
          financePremium.PolicyNumber = pp.PolicyNumber
          financePremium.TermNumber = pp.TermNumber.intValue()
          financePremium.JobID = pp.Job.ID.Value
          financePremium.JobNumber = pp.Job.JobNumber
          financePremium.JobCloseDate = pp.Job.CloseDate
          financePremium.ProductCode = pp.Policy.Product.Name
          //financePremium.Offering = pp.Policy.ProductCode
          financePremium.Offering = pp.getSlice(pp.EditEffectiveDate)?.Offering.CodeIdentifier
          financePremium.JobType = pp.Job.Subtype.Code
          financePremium.Jurisdiction = pp.BaseState.Code
          financePremium.TransactionID = tp.TransactionID
          financePremium.TransactionCreateTime = tp.TransactionCreateTime
          financePremium.TransactionEffDate = tp.TransactionEffDate
          financePremium.TransactionExpDate = tp.TransactionExpDate
          financePremium.AccountDate = tp.AccountDate
          financePremium.ChargePattern = tp.ChargePattern
          financePremium.CostType = tp.CostType
          financePremium.StateCostType = tp.StateCostType
          financePremium.CMP_Type = tp.CMPType
          financePremium.ReportingMonth = tp.ReportingMonth
          financePremium.ReportingYear = tp.ReportingYear
          financePremium.PremiumDays = tp.PremiumDays
          financePremium.Amount = tp.Amount
          TDIC_FinanceReportConstants.LOGGER.info("${CLASS_NAME}#generatePremiums tp.ReportPeriodPremiumsList.Count : " + tp.ReportPeriodPremiumsList.Count )
          for(rpPremiums in tp.ReportPeriodPremiumsList) {
            financeEarnedPremium = new FinanceEarnedPremiums_TDIC()
            financeEarnedPremium.ReportingMonth = rpPremiums.ReportingMonth
            financeEarnedPremium.ReportingYear = rpPremiums.ReportingYear
            financeEarnedPremium.PremiumDays = rpPremiums.PremiumDays
            financeEarnedPremium.Amount = rpPremiums.Amount
            financePremium.addToEarnedPremiums(financeEarnedPremium)
          }
          bundle.add(financePremium)
        }
      },User.util.UnrestrictedUser)
    } catch(e : Exception) {
      throw(e)
    }
  }

  private function initialize(args : Object[]){
    try {
      if (args.IsEmpty) {
        _jobCloseDate = DateUtil.currentDate().addDays(-1)
      } else {
        if(args.Count == 1) {
          TDIC_FinanceReportConstants.LOGGER.info("${CLASS_NAME}#validateArgument - date argument received is ${args.first()}")
          _jobCloseDate = TDIC_FinanceReportConstants.DATE_FORMAT.parse(args.first().toString())
        }else {
          throw new Exception("Invalid argument (${args.toList()}) received for batch run. Expected batch command argument is in the format MM/dd/yyyy.")
        }
      }
    }catch(e : Exception){
      throw new Exception("Invalid argument (${args.toList()}) received for batch run. Expected batch command argument is in the format MM/dd/yyyy")
    }
  }

  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(TDIC_FinanceReportConstants.EMAIL_TO_PROP_FIELD)
    if (_emailTo == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '${TDIC_FinanceReportConstants.EMAIL_TO_PROP_FIELD}' from integration database.")
    }
    _emailRecipient = PropertyUtil.getInstance().getProperty(TDIC_FinanceReportConstants.EMAIL_RECIPIENT)
    if (_emailRecipient == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '${TDIC_FinanceReportConstants.EMAIL_RECIPIENT}' from integration database.")
    }
    TDIC_FinanceReportConstants.LOGGER.debug("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  protected function getEmailSubject(flag : Boolean) : String {
    var subject : StringBuffer = new StringBuffer()
    if(flag){
      subject.append("SUCCESS::${gw.api.system.server.ServerUtil.ServerId}-PC Finance premiums daily process completed for policies with job close date: ${JobCloseDate}")
    }else{
      subject.append("FAILURE::${gw.api.system.server.ServerUtil.ServerId}-PC Finance premiums daily process failed for policies with job close date: ${JobCloseDate}")
    }
    return subject.toString()
  }
}