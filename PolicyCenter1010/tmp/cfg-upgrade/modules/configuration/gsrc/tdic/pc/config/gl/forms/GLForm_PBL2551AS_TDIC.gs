package tdic.pc.config.gl.forms

uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractSimpleAvailabilityForm

class GLForm_PBL2551AS_TDIC extends AbstractSimpleAvailabilityForm {

  override function isAvailable(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : boolean {
    if (context.Period.Offering.CodeIdentifier == "PLClaimsMade_TDIC") {
      if(context.Period.Job typeis PolicyChange and ( (context.Period.GLLine?.BasedOn?.GLCovExtDiscount_TDICExists and !context.Period.GLLine?.GLCovExtDiscount_TDICExists)
      or ((context.Period.GLLine?.BasedOn?.GLCovExtDiscount_TDICExists and !context.Period.GLLine?.GLCovExtDiscount_TDICExists) and
          (context.Period.GLLine?.BasedOn?.GLServiceMembersCRADiscount_TDICExists and !context.Period.GLLine?.GLServiceMembersCRADiscount_TDICExists)))){
        return true
      }
      else if(context.Period.Job typeis Renewal and ((context.Period.GLLine?.BasedOn?.GLCovExtDiscount_TDICExists and !context.Period.GLLine?.GLCovExtDiscount_TDICExists)
          or ((context.Period.GLLine?.BasedOn?.GLCovExtDiscount_TDICExists and !context.Period.GLLine?.GLCovExtDiscount_TDICExists) and
          (context.Period.GLLine?.BasedOn?.GLServiceMembersCRADiscount_TDICExists and !context.Period.GLLine?.GLServiceMembersCRADiscount_TDICExists)))){
        return true
      }
    }
    if (context.Period.Offering.CodeIdentifier == "PLOccurence_TDIC") {
      if(context.Period.Job typeis PolicyChange and (context.Period.GLLine?.BasedOn?.GLServiceMembersCRADiscount_TDICExists and
      !context.Period.GLLine?.GLServiceMembersCRADiscount_TDICExists)){
        return true
      }
      else if(context.Period.Job typeis Renewal and (context.Period.GLLine?.BasedOn?.GLServiceMembersCRADiscount_TDICExists and
      !context.Period.GLLine?.GLServiceMembersCRADiscount_TDICExists)){
        return true
      }
    }
    return false
  }

}