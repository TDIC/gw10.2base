package tdic.pc.common.batch.finance.reports.prmlines

uses tdic.pc.common.batch.finance.reports.premiums.TDIC_FinanceReportPremiumObject

uses java.math.BigDecimal

abstract class TDIC_FinancePremiumLinesBase implements TDIC_FinancePremiumLinesInterface {
  private static var CLASS_NAME : String = TDIC_FinancePremiumLinesBase.Type.RelativeName
  private static final var ZERO_DOUBLE = formatBigDecimal(TDIC_FinancePremiumLineConstants.ZERO_AMOUNT_DOUBLE)
  private static final var ZERO_INT = formatInteger(TDIC_FinancePremiumLineConstants.ZERO_AMOUNT_INT)

  protected static final var BLANK_STRING : String = formatString(TDIC_FinancePremiumLineConstants.BLANK_STRING)
  protected static final var COMPANY : String = formatInteger(TDIC_FinancePremiumLineConstants.GTR_COMPANY)
  protected static final var SOURCE_CODE : String = formatString(TDIC_FinancePremiumLineConstants.GTR_SOURCE_CODE)

  private var PRM_SUBLINE_1 : String
  private var PRM_SUBLINE_2 : String
  private var PRM_SUBLINE_3 : String

  private var systemDate = formatString(getDate())
  private var batchRunPeriodEndDate : Date
  private var monthEndDateRef : String
  private var seqNumber : int = 0

  protected var monthEndDate : String
  protected var reportPeriodEndDate : String


  construct(periodEndDate : Date) {
    initialize(periodEndDate)
  }


  override function getSeqNumber() : int {
    seqNumber = seqNumber + 1
    return seqNumber
  }

  /**
   * GWPS-2442 : Changing the code for 0012651230 to hit 000.00.00 instead of 000.state.line
   * @param reportPremium
   * @param prmLineAccount
   * @return
   */
  override function getOldCompany(reportPremium: TDIC_FinanceReportPremiumObject, prmLineAccount: TDIC_FinancePremiumLineAccountObject) : String {
    if("0012651230" == prmLineAccount.AccountNumber){
      return TDIC_FinancePremiumLineConstants.ACCT_UNIT_PREFIX
          + prmLineAccount.OldCompany
          + TDIC_FinancePremiumLineConstants.DOT_SEPARATOR
          + "00"
          + TDIC_FinancePremiumLineConstants.DOT_SEPARATOR
          + "00"
      } else {
      return TDIC_FinancePremiumLineConstants.ACCT_UNIT_PREFIX
          + prmLineAccount.OldCompany
          + TDIC_FinancePremiumLineConstants.DOT_SEPARATOR
          //+ getMappedJurisdictionAccountCode(reportPremium.Jurisdiction)
          + TDIC_FinancePremiumLineConstants.STATE_UNIT_MAP.get(reportPremium.Jurisdiction)
          + TDIC_FinancePremiumLineConstants.DOT_SEPARATOR
          + TDIC_FinancePremiumLineConstants.LOB_CODE_MAP.get(reportPremium.PremiumLineType)
      }
  }

  override function getSystemDate() : String {
    return systemDate
  }

  override function getReference(state : String, seqNum : int) : String {
    return formatString("${state}${monthEndDateRef}" + String.format(TDIC_FinancePremiumLineConstants.INT_PATTERN_2, {seqNum}))
  }

  override function getPostingDate() : String {
    return formatString(reportPeriodEndDate)
  }

  protected property get PrmSubLine1() : String {
    return PRM_SUBLINE_1
  }

  protected property get PrmSubLine2() : String {
    return PRM_SUBLINE_2
  }

  protected property get PrmSubLine3() : String {
    return PRM_SUBLINE_3
  }

  private function buildPrmSubLine1() {
    var prmSubLine = new StringBuilder()
    prmSubLine.append(ZERO_DOUBLE)//GTR-BASE-AMOUNT
    prmSubLine.append(ZERO_DOUBLE)//GTR-BASERATE
    prmSubLine.append(BLANK_STRING)//GTR-SYSTEM
    prmSubLine.append(BLANK_STRING)//GTR-PROGRAM-CODE
    prmSubLine.append(BLANK_STRING)//GTR-AUTO-REV
    PRM_SUBLINE_1 = prmSubLine.toString()
  }

  private function buildPrmSubLine2() {
    var prmSubLine = new StringBuilder()
    prmSubLine.append(BLANK_STRING)//GTR-ACTIVITY
    prmSubLine.append(BLANK_STRING)//GTR-ACCT-CATEGORY
    prmSubLine.append(BLANK_STRING)//GTR-DOCUMENT-NBR
    prmSubLine.append(ZERO_DOUBLE)//GTR-TO-BASE-AMT
    prmSubLine.append(formatString(reportPeriodEndDate))//GTR-EFFECT-DATE
    prmSubLine.append(BLANK_STRING)//GTR-JRNL-BOOK-NBR
    prmSubLine.append(BLANK_STRING)//GTR-MX-VALUE1
    prmSubLine.append(BLANK_STRING)//GTR-MX-VALUE2
    prmSubLine.append(BLANK_STRING)//GTR-MX-VALUE3
    prmSubLine.append(ZERO_INT)//GTR-JBK-SEQ-NBR
    PRM_SUBLINE_2 = prmSubLine.toString()
  }

  private function buildPrmSubLine3() {
    var prmSubLine = new StringBuilder()
    prmSubLine.append(BLANK_STRING)//GTR-SEGMENT-BLOCK
    prmSubLine.append(ZERO_DOUBLE)//GTR-RPT-AMOUNT-1
    prmSubLine.append(ZERO_DOUBLE)//GTR-RPT-RATE-1
    prmSubLine.append(ZERO_INT)//GTR-RPT-ND-1
    prmSubLine.append(ZERO_DOUBLE)//GTR-RPT-AMOUNT-2
    prmSubLine.append(ZERO_DOUBLE)//GTR-RPT-RATE-2
    prmSubLine.append(TDIC_FinancePremiumLineConstants.ZERO_AMOUNT_INT)//GTR-RPT-ND-2
    PRM_SUBLINE_3 = prmSubLine.toString()
  }

  protected static function formatString(value : String) : String {
    return TDIC_FinancePremiumLineConstants.DOUBLE_QUOTE + value + TDIC_FinancePremiumLineConstants.DOUBLE_QUOTE + TDIC_FinancePremiumLineConstants.COMMA_SEPARATOR
  }

  protected static function formatBigDecimalWithDecimals(value : BigDecimal) : String {
    return TDIC_FinancePremiumLineConstants.DECIMAL_FORMAT.format(value) + TDIC_FinancePremiumLineConstants.COMMA_SEPARATOR
  }

  protected static function formatBigDecimal(value : BigDecimal) : String {
    return value + TDIC_FinancePremiumLineConstants.COMMA_SEPARATOR
  }

  protected static function formatInteger(value : Integer, pattern : String) : String {
    return String.format(pattern, {value}) + TDIC_FinancePremiumLineConstants.COMMA_SEPARATOR
  }

  protected static function formatInteger(value : Integer) : String {
    return value + TDIC_FinancePremiumLineConstants.COMMA_SEPARATOR
  }

  private function getDate() : String {
    var sysDate = Calendar.getInstance().getTime()
    return TDIC_FinancePremiumLineConstants.DATE_FORMAT_MMDDYYYY.format(sysDate)
  }

  /*
  @Param("sdf", "Simple date format object for formatting period end date.")
  @Returns("Returns batch period end date in string object")
  private static function getBatchRunPeriodEndDate(sdf : SimpleDateFormat) : String {
    var calendar = Calendar.getInstance()
    calendar.set(Calendar.MONTH, calendar.CalendarMonth - 1)
    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE))
    var date = calendar.getTime()
    return sdf.format(date)
  }*/

  @Param("ped", "Period end date for batch run")
  private function initialize(ped : Date) {
    batchRunPeriodEndDate = ped
    monthEndDate = TDIC_FinancePremiumLineConstants.DATE_FORMAT_MMDDYY.format(batchRunPeriodEndDate)
    monthEndDateRef = TDIC_FinancePremiumLineConstants.DATE_FORMAT_MMDDYY.format(batchRunPeriodEndDate).replaceAll("/", "")
    reportPeriodEndDate = TDIC_FinancePremiumLineConstants.DATE_FORMAT_MMDDYYYY.format(batchRunPeriodEndDate)
    initializeSubLines()
  }

  private function initializeSubLines() {
    buildPrmSubLine1()
    buildPrmSubLine2()
    buildPrmSubLine3()
  }

  protected function getPremiumLineAccounts(reportPremium: TDIC_FinanceReportPremiumObject): List<TDIC_FinancePremiumLineAccountObject>{
    var premiumLineAccounts = new ArrayList<TDIC_FinancePremiumLineAccountObject>()
    var lobLabel = TDIC_FinancePremiumLineConstants.LOB_LABEL_MAP.get(reportPremium.PremiumLineType)
    switch (reportPremium.PremiumLineType){
      case TDIC_FinancePremiumLineTypeEnum.WC_EP_PREMIUM:
      case TDIC_FinancePremiumLineTypeEnum.CP_EP_GENERAL_LIAB:
      case TDIC_FinancePremiumLineTypeEnum.CP_EP_EQUIP_BREAKDOWN:
      case TDIC_FinancePremiumLineTypeEnum.CP_EP_PROPERTY:
      case TDIC_FinancePremiumLineTypeEnum.PL_EP_CYBER_LIAB:
      case TDIC_FinancePremiumLineTypeEnum.PL_EP_EMPL_PRACTICES:
      case TDIC_FinancePremiumLineTypeEnum.PL_EP_IDENITY_THEFT:
      case TDIC_FinancePremiumLineTypeEnum.PL_EP_PROF_LIAB:
      case TDIC_FinancePremiumLineTypeEnum.PL_EP_PROF_LIAB_OCCUR:
      case TDIC_FinancePremiumLineTypeEnum.PL_EP_REP_ENDORSEMENTS:
      {
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, TDIC_FinancePremiumLineConstants.ACC_NBR_UNEARNED_PREMIUM,
                                                      "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, TDIC_FinancePremiumLineConstants.ACC_NBR_CHG_UNEARNED_PREMIUM,
                                                      "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
      case TDIC_FinancePremiumLineTypeEnum.WC_WP_STD_PREMIUM:{
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0012651230",
                "PREM RCVD ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}", TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0040502203",
                "${lobLabel} PREMIUM ${reportPremium.Jurisdiction} ${monthEndDate}", TDIC_FinancePremiumLineConstants.NEG_ADJ_Y),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0041701200",
                "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}", TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0020201200",
                "UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}", TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
      case TDIC_FinancePremiumLineTypeEnum.CP_WP_GENERAL_LIAB:
      case TDIC_FinancePremiumLineTypeEnum.CP_WP_EQUIP_BREAKDOWN:
      case TDIC_FinancePremiumLineTypeEnum.CP_WP_PROPERTY:
      case TDIC_FinancePremiumLineTypeEnum.PL_WP_CYBER_LIAB:
      case TDIC_FinancePremiumLineTypeEnum.PL_WP_EMPL_PRACTICES:
      case TDIC_FinancePremiumLineTypeEnum.PL_WP_IDENITY_THEFT:
      case TDIC_FinancePremiumLineTypeEnum.PL_WP_PROF_LIAB:
      case TDIC_FinancePremiumLineTypeEnum.PL_WP_PROF_LIAB_OCCUR:
      case TDIC_FinancePremiumLineTypeEnum.PL_WP_REP_ENDORSEMENTS:
      {
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0012651230",
                "PREM RCVD ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0040501200",
                "${lobLabel} PREMIUM ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0041701200",
                "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0020201200",
                "UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
      case TDIC_FinancePremiumLineTypeEnum.WC_WP_STD_PREMIUM_AUDIT:{
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0012651230",
                "PREM RCVD ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0040502206",
                "${lobLabel} PREMIUM ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0041701200",
                "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0020201200",
                "UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
      case TDIC_FinancePremiumLineTypeEnum.WC_WP_TERR_PREMIUM:{
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0012651230",
                "PREM RCVD ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0040502201",
                "${lobLabel} PREMIUM ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0041701200",
                "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0020201200",
                "UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
      case TDIC_FinancePremiumLineTypeEnum.WC_WP_TERR_PREMIUM_AUDIT:{
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0012651230",
                "PREM RCVD ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0040502204",
                "${lobLabel} PREMIUM ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0041701200",
                "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0020201200",
                "UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
      case TDIC_FinancePremiumLineTypeEnum.WC_WP_EXP_CONSTANT:{
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0012651230",
                "PREM RCVD ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0040502202",
                "${lobLabel} PREMIUM ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0041701200",
                "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0020201200",
                "UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
      case TDIC_FinancePremiumLineTypeEnum.WC_WP_EXP_CONSTANT_AUDIT:{
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0012651230",
                "PREM RCVD ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0040502205",
                "${lobLabel} PREMIUM ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.SALES_ACCT_UNIT, "0041701200",
                "CHG UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0020201200",
                "UEP ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
      case TDIC_FinancePremiumLineTypeEnum.WC_WP_SURCHARGES:
      case TDIC_FinancePremiumLineTypeEnum.CP_WP_SURCHARGES:
      case TDIC_FinancePremiumLineTypeEnum.PL_WP_SURCHARGES:
      {
        premiumLineAccounts = {
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0012651230",
                "PREM RCVD ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.BLANK_STRING),
            new TDIC_FinancePremiumLineAccountObject(TDIC_FinancePremiumLineConstants.BAL_SHEET_ACCT_UNIT, "0026601200",
                "SURCH ${lobLabel} ${reportPremium.Jurisdiction} ${monthEndDate}",TDIC_FinancePremiumLineConstants.NEG_ADJ_Y)
        }
        break
      }
    }
    return premiumLineAccounts
  }
}