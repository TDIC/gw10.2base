package tdic.pc.config.gl.forms

uses gw.api.diff.DiffAdd
uses gw.api.diff.DiffProperty
uses gw.forms.FormInferenceContext
uses gw.forms.generic.AbstractMultipleCopiesForm
uses gw.forms.generic.AbstractSimpleAvailabilityForm
uses gw.xml.XMLNode

/**
 * Created with IntelliJ IDEA.
 * User: SureshB
 * Date: 12/3/2019
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
class GLForm_CYB2115AS_TDIC extends AbstractMultipleCopiesForm<GLManuscript_TDIC> {

  override function getEntities(context : FormInferenceContext, availableStates : Set<Jurisdiction>) : List<GLManuscript_TDIC> {
    var period = context.Period
    var glManuscriptSchedItems = period.GLLine?.GLManuscript_TDIC
    var manuscriptItems : ArrayList<GLManuscript_TDIC> = {}
    if (period.Offering.CodeIdentifier == "PLCyberLiab_TDIC" and period.GLLine?.GLManuscriptEndor_TDICExists
        and glManuscriptSchedItems.HasElements){
      if (period.Job.Subtype == typekey.Job.TC_POLICYCHANGE) {
        var changeList = context.Period?.getDiffItems(DiffReason.TC_COMPAREJOBS)
        var manuscriptSchedLineItemsModified = changeList?.where(\changedItem -> (changedItem typeis DiffAdd or
            changedItem typeis DiffProperty) and changedItem.Bean typeis GLManuscript_TDIC)*.Bean
        if(glManuscriptSchedItems.hasMatch(\item -> item.BasedOn == null)){
          manuscriptItems.add(glManuscriptSchedItems.where(\item -> item.BasedOn == null).first())
        }
        else if(glManuscriptSchedItems.hasMatch(\item -> manuscriptSchedLineItemsModified.contains(item))) {
          manuscriptItems.add(glManuscriptSchedItems.where(\item -> manuscriptSchedLineItemsModified.contains(item)).first())
        }
      }
      else if(period.Job.Subtype == typekey.Job.TC_CANCELLATION){
        if(period.RefundCalcMethod != CalculationMethod.TC_FLAT and period.GLLine?.GLManuscriptEndor_TDICExists){
          manuscriptItems.add(glManuscriptSchedItems.first())
        }
      }
      else {
        manuscriptItems.add(glManuscriptSchedItems.first())
      }
      return manuscriptItems.HasElements? manuscriptItems.toList(): {}
    }
    return {}
  }
  override property get FormAssociationPropertyName() : String {
    return "GLManuscript_TDIC"
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    //contentNode.addChild(createTextNode("EffectiveDate", _entity.ManuscriptEffectiveDate.toString()))
    contentNode.addChild(createTextNode("ManuscriptEffectiveDate", _entity.ManuscriptEffectiveDate?.toString()))
    contentNode.addChild(createTextNode("ManuscriptExpirationDate", _entity.ManuscriptExpirationDate?.toString()))
    contentNode.addChild(createTextNode("ManuscriptType", _entity.ManuscriptType.toString()))
    contentNode.addChild(createTextNode("ManuscriptDescription", _entity.ManuscriptDescription))
  }

  override protected function createFormAssociation(form : Form) : FormAssociation {
    return new GLFormAssociation_TDIC(form.Branch)
  }

  override function getMatchKeyForForm(form: Form): String {
    if(form.FormAssociations == null or form.FormAssociations.Count == 0){
      return null
    }
    return form.FormAssociations[0].fixedIdValue(FormAssociationPropertyName) as String
  }
}