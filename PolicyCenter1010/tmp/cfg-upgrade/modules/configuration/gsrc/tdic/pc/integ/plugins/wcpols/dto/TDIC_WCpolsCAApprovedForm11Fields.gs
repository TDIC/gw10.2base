package tdic.pc.integ.plugins.wcpols.dto

uses java.util.ArrayList
uses tdic.util.flatfile.model.classes.Record
uses tdic.util.flatfilegenerator.TDIC_FlatFileUtilityHelper
uses tdic.util.flatfilegenerator.TDIC_FlatFileLineGenerator

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 12/17/14
 * Time: 1:42 PM
 * This class includes variables for all fields in CAF 11 record for policy specification report.
 */
class TDIC_WCpolsCAApprovedForm11Fields extends  TDIC_FlatFileLineGenerator {

  var _carrierCode : int as CarrierCode
  var _policyNumber : String as PolicyNumber
  var _futureReserved : String as FutureReserved
  var _unitCertificateIdentifier : String as UnitCertificateIdentifier
  var _policyEffectiveDate : String as PolicyEffectiveDate
  var _transactionIssueDate : String as TransactionIssueDate
  var _transactionCode : int as TransactionCode
  var _stateCode : int as StateCode
  var _recordTypeCode : String as RecordTypeCode
  var _futureReserved1 : String as FutureReserved1
  var _endorsementNumber : String as EndorsementNumber
  var _bureauVersionIdentifier : String as BureauVersionIdentifier
  var _carrierVersionIdentifier : String as CarrierVersionIdentifier
  var _endorsementSerialNumber : String as EndorsementSerialNumber
  var _excludedOperationDescription : ArrayList<String> as ExcludedOperationDescription
  var _futureReserved2 : String as FutureReserved2
  var _endorsementSequenceNumber : int as EndorsementSequenceNumber
  var _nameOfInsured : String as NameOfInsured
  var _endorsementEffectiveDate : String as EndorsementEffectiveDate
  var _endorsementExpirationDate : String as EndorsementExpirationDate

  override function createFlatFileLine(record:Record):String{

    var flatFileLine=""
    var nameAndTitleCount=0

    for(field in record.Fields){

      var value =""
      if(field.Name=="ExcludedOperationDescription"){
        if(this.ExcludedOperationDescription.Count > nameAndTitleCount ) {
          value = this.ExcludedOperationDescription.get(nameAndTitleCount)
          nameAndTitleCount++
        }
      }
      else{

        value = objHashMap().get(field.Name)
      }
      flatFileLine += TDIC_FlatFileUtilityHelper.formatString(value, field.Length as int, field.Truncate, field.Justify, field.Fill, field.Format, field.Strip) //Format string and add it to flat file line
    }
    return flatFileLine
  }

}