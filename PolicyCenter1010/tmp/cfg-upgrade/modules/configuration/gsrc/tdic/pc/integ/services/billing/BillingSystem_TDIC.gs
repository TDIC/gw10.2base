package tdic.pc.integ.services.billing

uses wsi.remote.gw.webservice.bc.bc1000.billingapi.BillingAPI
uses java.util.Date
uses gw.api.util.LocaleUtil

uses wsi.remote.gw.webservice.bc.bc1000.billingapi.types.complex.DelinquencyInfo_TDIC


class BillingSystem_TDIC {
  private property get BillingAPIWithLanguage() : BillingAPI {
    var billingAPI = new BillingAPI()
    billingAPI.Config.Guidewire.Locale = LocaleUtil.CurrentLanguage.Code
    return billingAPI
  }

  public function getNextInvoiceDateAfterDate (policyNumber : String, date : Date) : Date {
    return BillingAPIWithLanguage.getNextInvoiceDateAfterDate_TDIC (policyNumber, date);
  }

  public function getClosureStatus (policyNumber : String, termNumber : int) : String {
    return BillingAPIWithLanguage.getClosureStatus_TDIC (policyNumber, termNumber).Code;
  }

  public function getDelinquencyInfo (delinquencyProcessPublicID : String) : DelinquencyInfo_TDIC[] {
    return BillingAPIWithLanguage.getDelinquencyInfo_TDIC(delinquencyProcessPublicID);
  }
}