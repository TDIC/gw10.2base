package tdic.util.gunit.test

uses gw.testharness.TestBase
uses gw.api.system.server.Runlevel
uses gw.testharness.RunLevel
uses gw.testharness.ServerTest
uses tdic.util.gunit.TestConstants

@ServerTest
@RunLevel(Runlevel.NONE)
class TestConstantsTest extends TestBase {

  function test_PROJECT_ROOT_DIRECTORY_PATH_value() {
    assertEquals("./", TestConstants.PROJECT_ROOT_DIRECTORY_PATH)
  }

  function test_MODULES_DIRECTORY_PATH_value() {
    assertEquals("./modules/", TestConstants.MODULES_DIRECTORY_PATH)
  }

  function test_MODULES_CONFIGURATION_DIRECTORY_PATH_value() {
    assertEquals("./modules/configuration/", TestConstants.MODULES_CONFIGURATION_DIRECTORY_PATH)
  }

}
