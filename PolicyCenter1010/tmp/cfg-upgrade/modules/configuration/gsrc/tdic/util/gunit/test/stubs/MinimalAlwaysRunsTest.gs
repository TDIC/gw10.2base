package tdic.util.gunit.test.stubs

uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses gw.testharness.RunLevel
uses gw.api.system.server.Runlevel

@ServerTest
@RunLevel(Runlevel.NONE)
class MinimalAlwaysRunsTest extends TestBase {

  function testNothing() {
    // This test always runs. See TestRunnerTest.
  }

}
