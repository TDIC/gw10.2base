package tdic.util.gunit.test.stubs

uses gw.testharness.ServerTest
uses gw.testharness.RunLevel
uses gw.api.system.server.Runlevel

@ServerTest
@RunLevel(Runlevel.NONE)
class SubclassTest extends AbstractBaseClassTest {

  function testInSubclass() {
    logMethodCall("testInSubclass")
  }

}
