package tdic.util.gunit.test

uses gw.api.system.server.Runlevel
uses gw.lang.reflect.IType
uses gw.testharness.RunLevel
uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses tdic.util.gunit.GUnitUtil
uses tdic.util.gunit.TestConstants
uses tdic.util.gunit.TextTestRunner
uses tdic.util.gunit.test.stubs.*

@ServerTest
@RunLevel(Runlevel.NONE)
class TextTestRunnerTest extends TestBase {

  function test_PATH_TO_GTEST_DIR_value() {
    assertEquals("./modules/configuration/gtest/", TestConstants.CUSTOMER_GTEST_PATH)
  }

  function testRunAllTests() {
    var runner = new MockTextTestRunnerTracksClasses()

    runner.runAllTests()

    assertTestRunContains(runner, MinimalAlwaysRunsTest)
    assertTestRunContains(runner, TextTestRunnerTest)  // This test.
  }

  function testRunTestInPackage() {
    var runner = new MockTextTestRunnerTracksClasses()

    runner.runTestsInPackage("tdic.util.gunit.test.stubs")

    assertTestRunContains(runner, MinimalAlwaysRunsTest)
    assertTestRunDoesNotContain(runner, TextTestRunnerTest, "it is one level higher in the package hierarchy")
  }

  function testClassNameDoesNotEndWith_Test() {
    var runner = new MockTextTestRunnerTracksClasses()

    runner.runTestsInPackage("tdic.util.gunit.test.stubs")

    assertTestRunContains(runner, MinimalAlwaysRunsTest)
    assertTestRunDoesNotContain(runner, TestNotAtEndOfClassName, "class name does NOT end with 'Test'")
  }

  function testClassDoesNotExtend_TestBase_Class() {
    var runner = new MockTextTestRunnerTracksClasses()

    runner.runTestsInPackage("tdic.util.gunit.test.stubs")

    assertTestRunContains(runner, MinimalAlwaysRunsTest)
    assertTestRunDoesNotContain(runner, DoesNotExtendTestBaseTest, "test class does NOT extend TestBase class")
  }

  function testClassDoesNotHave_ServerTest_Annotation() {
    var runner = new MockTextTestRunnerTracksClasses()

    runner.runTestsInPackage("tdic.util.gunit.test.stubs")

    assertTestRunContains(runner, MinimalAlwaysRunsTest)
    assertTestRunContains(runner, DoesNotHaveServerTestAnnotationTest)
  }

  function testAbstractBaseAndSubclassTest() {
    var runner = new MockTextTestRunnerTracksClasses()

    runner.runTestsInPackage("tdic.util.gunit.test.stubs")

    // See also AbstractBaseAndSubclassTest
    assertTestRunDoesNotContain(runner, AbstractBaseClassTest, "this is an abstract classs; no instances can be created")
    assertTestRunContains(runner, SubclassTest)
  }

  function testOnePassingTest() {
    var runner = new TextTestRunner()

    runner.runTestsInClass(OnePassingTest)

    assertEquals("PassingTests", 1, runner.TestReporter.PassingTests)
    assertEquals("FailingTests", 0, runner.TestReporter.FailingTests)
    assertEquals("TotalTests",   1, runner.TestReporter.TotalTests)
  }

  function testTwoPassingTests() {
    var runner = new TextTestRunner()

    runner.runTestsInClass(TwoPassingTest)

    assertEquals("PassingTests", 2, runner.TestReporter.PassingTests)
    assertEquals("FailingTests", 0, runner.TestReporter.FailingTests)
    assertEquals("TotalTests",   2, runner.TestReporter.TotalTests)
  }

  function testHasNoTestMethods() {
    var runner = new TextTestRunner()

    var output = GUnitUtil.captureStandardOutput(\ -> {
        runner.runTestsInClass(HasNoTestMethods)
      })

    assertEquals("tdic.util.gunit.test.stubs.HasNoTestMethods has no test methods.\r\n", output)
    assertEquals("PassingTests", 0, runner.TestReporter.PassingTests)
    assertEquals("FailingTests", 1, runner.TestReporter.FailingTests)
    assertEquals("TotalTests",   1, runner.TestReporter.TotalTests)
  }

  private static function assertTestRunContains(runner : MockTextTestRunnerTracksClasses, iType : IType) {
    if (not runner.TestClassesRun.contains(iType)) {
      fail("TestRunner did NOT find and run tests in class <" + iType.Name + ">, but it should have.\n"
          + "  Found and ran these tests: " + runner.NamesOfAllTestClassRun)
    }
  }

  private static function assertTestRunDoesNotContain(runner : MockTextTestRunnerTracksClasses, iType : IType, reasonTestClassShouldBeIgnored : String) {
    if (runner.TestClassesRun.contains(iType)) {
      fail("TestRunner found and and run tests in class <" + iType.Name + ">, but it should NOT have because "
          + reasonTestClassShouldBeIgnored + ".")
    }
  }

}
