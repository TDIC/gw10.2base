package tdic.util.gunit

uses gw.api.system.server.Runlevel

interface IRunlevelAccessor {

  function getCurrentServerRunlevel() : Runlevel

}
