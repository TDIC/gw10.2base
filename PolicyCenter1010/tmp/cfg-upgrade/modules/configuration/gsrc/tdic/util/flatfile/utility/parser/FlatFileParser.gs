package tdic.util.flatfile.utility.parser

uses gw.pl.logging.LoggerCategory
uses tdic.util.flatfile.model.classes.Field
uses tdic.util.flatfile.model.classes.FlatFile
uses tdic.util.flatfile.model.classes.Record
uses java.io. *
uses java.lang.Exception
uses java.util.ArrayList
uses org.slf4j.LoggerFactory

class FlatFileParser extends FileParser {
  final var RECORD_LENGTH = 80
  private var _parsedFile: Record[] as ParsedFile
  private var _logger = LoggerFactory.getLogger("FLAT_FILE_UTILITY")
  /**
   * US688
   * 09/03/2014 shanem
   *
   * Calls the parse() method and assigns the returned Record[] to the value of ParsedFileRecords
   */
  @Param("aPath", "The path of the file to read from")
  construct(aPath: String, vendorDef: tdic.util.flatfile.model.gx.flatfilemodel.FlatFile) {
    ParsedFile = parse(aPath, vendorDef)
  }

  /**
   * US688
   * 09/010/2014 shanem
   *
   * Parses a file to a Record[] by creating instances of Record Types
   * based on the first character of each record line in the file
   */
  @Param("aPath", "The path of the file to read from")
  @Param("aVenodorDef", "The vendor definition parsed as a FlatFile")
  @Returns("The parsed file as a Record[]")
  final override function parse(aPath: String, aVenodorDef: tdic.util.flatfile.model.gx.flatfilemodel.FlatFile): Record[] {
    _logger.trace("FlatFileParser#parse() - Entering")
    var fileArray = new ArrayList<Record>()
    using (var br = new BufferedReader(new FileReader(aPath))) {
      var record = br.readLine()
      var vendorDefRecordInfo: tdic.util.flatfile.model.gx.flatfilemodel.anonymous.elements.FlatFile_Records_Entry
      while (record != null) {
        if (record.length == RECORD_LENGTH) {
          var recordType = record.charAt(0)
          var parsedRecord: Record
          switch (recordType) {
            case '1':
                // File Header record
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '1')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")
                break
            case '2':
                // Lockbox Header record
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '2')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")
                break
            case '3':
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '3')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")
                break
            case '4':
                // Invoice record
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '4')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")

                break
            case '5':
                // Batch Header record
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '5')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")
                break
            case '6':
                // Transaction record
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '6')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")
                break
            case '7':
                // Batch Trailer record
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '7')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")
                break
            case '8':
                // Lockbox Trailer record
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '8')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")
                break
            case '9':
                // File Trailer record
                vendorDefRecordInfo = aVenodorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == '9')
                parsedRecord = parseRecord(record, vendorDefRecordInfo)
                _logger.debug("FlatFileParser#parse() - ${parsedRecord.RecordType} parsed")
                break
              default:
              _logger.debug("FlatFileParser#parse() - Unknown record type encountered")
          }
          fileArray.add(parsedRecord)
        }
        record = br.readLine()
      }
    }
      _logger.debug("FlatFileParser#parse() - ${fileArray.Count} records parsed.")
      _logger.trace("FlatFileParser#parse() - Exiting")
    return fileArray?.toTypedArray()
  }

  /**
   * US688
   * 09/09/2014 shanem
   *
   * Writes the parsed contents as XML to a file
   */
  @Param("aWritePath", "The path to write the xml file to")
  override function writeToFile(aWritePath: String): void {
    _logger.trace("FlatFileParser#writeToFile(String) - Entering")
    var file = new File(aWritePath);
    if (!file.exists()) {
      _logger.debug("FlatFileParser#writeToFile(String) - file does not exist at ${aWritePath}, creating one.")
      file.createNewFile();
    }
    var parsedFileXml = this.Xml
    try {
      using (var writer = new BufferedWriter(new FileWriter(file))) {
        writer.write(parsedFileXml.asUTFString())
        _logger.debug("FlatFileParser#writeToFile(String) - XML written to file: ${aWritePath}}")
      }
    } catch (var e: Exception) {
      _logger.error("FlatFileParser#writeToFile(String) - Unexpected Exception thrown.")
    }
    _logger.trace("FlatFileParser#writeToFile(String) - Exiting")
  }

  /**
   * US688
   * 09/10/2014 shanem
   *
   * Generate XML using the GXModel for this file type
   */
  property get Xml(): tdic.util.flatfile.model.gx.flatfilemodel.FlatFile {
    return new tdic.util.flatfile.model.gx.flatfilemodel.FlatFile(new FlatFile(ParsedFile))
  }

  /**
   * US688
   * 09/10/2014 shanem
   *
   * Creates a record object using the string as the data element, and populating all other elements by using the vendor definition
   */
  @Param("record", "The line/record in the file to be parsed")
  @Param("vendorRecordDef", "The Vendor Definition for a line of this type, determined by the first character in the file")
  private function parseRecord(record: String, vendorRecordDef: tdic.util.flatfile.model.gx.flatfilemodel.anonymous.elements.FlatFile_Records_Entry): Record {
    _logger.trace("FlatFileParser#parseRecord(String, FlatFile_Records_Entry) - Entering")
    var recordFields = new ArrayList<Field>()
    vendorRecordDef.Fields.Entry.each(\entry -> recordFields.add(new Field(){
        : StartPosition = entry.StartPosition,
        : Length = entry.Length,
        : Name = entry.Name,
        : Parameter1 = entry.Parameter1,
        : Format = entry.Format,
        : Justify = entry.Justify,
        : Fill = entry.Fill,
        : Truncate = entry.Truncate,
        : StripType = entry.StripType,
        : Strip = entry.Strip,
        : Data = record.substring(entry.StartPosition, (entry.Length + entry.StartPosition)),
        : Parameter2 = entry.Parameter2
    }))
    var parsedRecord = new Record(){
        : RecordType = vendorRecordDef.RecordType,
        : RecordSection = vendorRecordDef.RecordSection,
        : RecordDelimiter = vendorRecordDef.RecordDelimiter,
        : RecordFormat = vendorRecordDef.RecordFormat,
        : Execute = vendorRecordDef.Execute,
        : Source = vendorRecordDef.Source,
        : FileStart = vendorRecordDef.FileStart,
        : Fields = recordFields?.toTypedArray(),
        : Identifier = vendorRecordDef.Identifier
    }
    _logger.trace("FlatFileParser#parseRecord(String, FlatFile_Records_Entry) - Entering")
    return parsedRecord
  }
}