<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Screen
    id="DocumentsScreen">
    <Require
      name="account"
      type="Account"/>
    <Require
      name="viewOnly"
      type="boolean"/>
    <Variable
      name="docActionsHelper"
      type="gw.document.DocumentsActionsUIHelper"/>
    <AlertBar
      id="DocumentsScreen_DocumentStoreSuspendedWarning"
      label="DisplayKey.get(&apos;Web.DocumentsLV.Button.Asynchronous.DocumentStoreDisabled&apos;)"
      visible="docActionsHelper.ShowDocumentStoreSuspendedWarning"/>
    <AlertBar
      id="DocumentsScreen_IDCSNotEnabledAlertBar"
      label="DisplayKey.get(&apos;Web.ContactDetail.Documents.DocumentManagementSystem.ContentPlugin.Disabled&apos;)"
      visible="not docActionsHelper.ContentSourceEnabled"/>
    <AlertBar
      id="DocumentsScreen_IDCSDownAlertBar"
      label="DisplayKey.get(&apos;Web.ContactDetail.Documents.DocumentManagementSystem.ContentPlugin.Unavailable&apos;)"
      visible="docActionsHelper.ContentSourceEnabled and not docActionsHelper.DocumentContentServerAvailable"/>
    <AlertBar
      id="DocumentsScreen_IDMSDownAlertBar"
      label="DisplayKey.get(&apos;Web.ContactDetail.Documents.DocumentManagementSystem.MetadataPlugin.Unavailable&apos;)"
      visible="docActionsHelper.ShowMetadataServerDownWarning"/>
    <SearchPanel
      criteriaName="searchCriteria"
      resultCachingEnabled="false"
      resultsName="documents"
      search="searchCriteria.performSearch() as gw.api.database.IQueryBeanResult&lt;Document&gt;"
      searchCriteria="return createCriteria()"
      searchCriteriaType="entity.DocumentSearchCriteria"
      searchOnEnter="true"
      searchResultsType="gw.api.database.IQueryBeanResult&lt;Document&gt;">
      <PanelRef
        def="DocumentSearchDV(searchCriteria, null)"/>
      <PanelRef
        def="DocumentsLV(documents, searchCriteria, viewOnly)"
        editable="true">
        <TitleBar
          title="DisplayKey.get(&quot;Web.PolicyFile.Documents&quot;)"/>
        <Toolbar>
          <CheckedValuesToolbarButton
            checkedRowAction=" CheckedValue.Obsolete = true"
            flags="all editableDocument,no hidden"
            id="PolicyFile_Documents_ObsolesceButton"
            iterator="DocumentsLV.DocumentsLV"
            label="DisplayKey.get(&quot;Web.PolicyFile.Documents.Obsolesce&quot;)"
            shortcut="O"/>
          <CheckedValuesToolbarButton
            checkedRowAction=" CheckedValue.Obsolete = false"
            flags="all editableDocument,all hidden"
            id="PolicyFile_Documents_DeobsolesceButton"
            iterator="DocumentsLV.DocumentsLV"
            label="DisplayKey.get(&quot;Web.PolicyFile.Documents.Deobsolesce&quot;)"
            shortcut="O"
            visible="searchCriteria.IncludeObsoletes"/>
          <ToolbarButton
            available="docActionsHelper.DocumentContentServerAvailable"
            id="PolicyFile_Documents_NewDocumentButton"
            label="DisplayKey.get(&apos;Web.DocumentsLV.Button.NewDocument&apos;)">
            <MenuItemSetRef
              def="AccountNewDocumentMenuItemSet(account)"/>
          </ToolbarButton>
          <CheckedValuesToolbarButton
            available="docActionsHelper.DocumentContentServerAvailable"
            checkedRowAction="gw.api.web.document.DocumentsHelper.deleteDocument(CheckedValue)"
            confirmMessage="DisplayKey.get(&apos;Web.DocumentsLV.Button.Delete.Confirm&apos;)"
            flags="all canDeleteDocument"
            id="PolicyFile_Documents_DeleteSelectedDocumentButton"
            iterator="DocumentsLV.DocumentsLV"
            label="DisplayKey.get(&apos;Web.DocumentsLV.Button.DeleteSelected&apos;)"
            tooltip="DisplayKey.get(&apos;Web.DocumentsLV.Button.DeleteSelected.Tooltip&apos;)"/>
          <ToolbarButton
            action="null"
            available="docActionsHelper.DocumentContentServerAvailable"
            id="RefreshAsyncContent"
            label="DisplayKey.get(&apos;Web.DocumentsLV.Button.AsynchronousRefresh&apos;)"
            tooltip="DisplayKey.get(&apos;Web.DocumentsLV.Button.AsynchronousRefresh.Tooltip&apos;)"
            visible="docActionsHelper.isShowAsynchronousRefreshAction(documents.toTypedArray())"/>
        </Toolbar>
      </PanelRef>
    </SearchPanel>
    <Code><![CDATA[function createCriteria() : DocumentSearchCriteria {
  var criteria = new DocumentSearchCriteria()
  criteria.setFixedContextWith(account)
  criteria.IncludeObsoletes = false
  return criteria
}]]></Code>
  </Screen>
</PCF>