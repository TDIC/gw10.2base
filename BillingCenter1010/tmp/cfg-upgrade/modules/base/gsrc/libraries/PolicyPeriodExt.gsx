package libraries

uses gw.api.database.DBFunction
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.locale.DisplayKey
uses gw.api.path.Paths
uses gw.api.upgrade.Coercions
uses gw.api.util.DateUtil
uses gw.api.web.payment.DirectBillPaymentFactory
uses gw.bc.archive.ImpactedByArchiving
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.pas.PASMessageTransport
uses gw.transaction.TransactionWrapper

uses java.lang.*
uses java.math.BigDecimal
uses java.math.RoundingMode
uses java.util.ArrayList
uses java.util.Date

/**
 * Defines enhancements for a {@link PolicyPeriod}.
 */
@Export
enhancement PolicyPeriodExt : entity.PolicyPeriod {
  /**
   * A reversible {@link ChargePatternKey#POLICYLATEFEE Policy Late Fee}
   * {@link entity.ReversibleChargeSearchCriteria SearchCriteria} for
   * this {@link PolicyPeriod}.
   */
  property get ReversibleLateFeeSearchCriteria() : gw.search.ReversibleChargeSearchCriteria {
    final var criteria = new gw.search.ReversibleChargeSearchCriteria()
    criteria.PolicyPeriod = this
    criteria.ChargePattern = ChargePatternKey.POLICYLATEFEE.get()
    return criteria
  }

  /**
   * The {@link InvoiceDeliveryMethod} for this {@link PolicyPeriod}.
   */
  property get InvoiceDeliveryType() : InvoiceDeliveryMethod {
    return this.OverridingPayerAccount != null
        ? this.OverridingPayerAccount.InvoiceDeliveryType
        : (this.DefaultPayer typeis Account
            ? this.DefaultPayer.InvoiceDeliveryType : null)
  }

  /**
   * The {@link PaymentInstrument} for this {@link PolicyPeriod}.
   */
  property get PaymentInstrument() : PaymentInstrument {
    // For policy level billing, check the overriding invoice stream
    if (this.BillingMethod != TC_AGENCYBILL
        && this.Account.BillingLevel == BillingLevel.TC_POLICYDEFAULTUNAPPLIED) {
      if (this.OverridingInvoiceStream != null) return this.OverridingInvoiceStream.PaymentInstrument
    }

    return this.OverridingPayerAccount != null
        ? this.OverridingPayerAccount.DefaultPaymentInstrument
        : (this.DefaultPayer typeis Account
            ? this.DefaultPayer.DefaultPaymentInstrument : null)
  }

  /**
   * The next {@link Invoice}(s) where the {@link Invoice#PaymentDueDate
   * PaymentDueDate} is the nearest in the future (i.e., after today). There
   * might be more than one on that date, but not any for another date.
   */
  @ImpactedByArchiving
  property get NextDueInvoices() : Invoice[] {
    final var itemQuery = Query.make(InvoiceItem)
    itemQuery.compare(InvoiceItem#PolicyPeriod, Equals, this)
    final var invoiceQuery = Query.make<Invoice>(Invoice)
    invoiceQuery.subselect(Invoice#ID, CompareIn, itemQuery, InvoiceItem#Invoice)
    invoiceQuery.compare(Invoice#PaymentDueDate, GreaterThan, Date.Today)
    var min = invoiceQuery.select({QuerySelectColumns.dbFunction(
        DBFunction.Min(invoiceQuery.getColumnRef(Invoice#EventDate.PropertyInfo.Name)))})
    invoiceQuery
        .compare(Invoice#EventDate, Equals, Coercions.makeDateFrom(min.AtMostOneRow.getColumn(0)))
    return invoiceQuery.select().toTypedArray()
  }

  /**
   * The primary default {@link ProducerCode} for this {@link PolicyPeriod}.
   * This is independent of whether the {@code PolicyPeriod} is archived.
   */
  property get PrimaryProducerCode() : ProducerCode {
    return this.Archived
        ? this.PrimaryCommissionArchiveSummary.ProducerCode
        : this.PrimaryPolicyCommission.ProducerCode
  }

  @ImpactedByArchiving
  function getPaymentDueDateToAvoidDelinquencyOn(date : Date) : Date {
    var dueInvoice = getMostRecentPastDueInvoiceAsOf(date)
    if (dueInvoice != null) {
      return dueInvoice.PaymentDueDate
    } else {
      return null
    }
  }

  /**
   * The most recent {@link Invoice} where the {@link Invoice#PaymentDueDate
   * PaymentDueDate} is "date" or before.
   */
  @ImpactedByArchiving
  function getMostRecentPastDueInvoiceAsOf(date : Date) : Invoice {
    final var itemQuery = Query.make(InvoiceItem)
    itemQuery.compare(InvoiceItem#PolicyPeriod, Equals, this)
    final var invoiceQuery = Query.make<Invoice>(Invoice)
    invoiceQuery.subselect(Invoice#ID, CompareIn, itemQuery, InvoiceItem#Invoice)
    invoiceQuery.compare(Invoice#PaymentDueDate, LessThanOrEquals, date)
    var min = invoiceQuery.select({QuerySelectColumns.dbFunction(
        DBFunction.Max(invoiceQuery.getColumnRef(Invoice#EventDate.PropertyInfo.Name)))})
    invoiceQuery
        .compare(Invoice#EventDate, Equals, Coercions.makeDateFrom(min.AtMostOneRow.getColumn(0)))
    return invoiceQuery.select().FirstResult
  }

  @ImpactedByArchiving
  function getSafeEarnedPremium(): MonetaryAmount {
    return (this.Closed ? this.getTotalValue(TC_PREMIUM): this.getEarnedValue(TC_PREMIUM))
  }

  @ImpactedByArchiving
  function getPolicyEquityPercentage(): Double {
    return getPolicyEquityPercentage(this.TotalEquityChargeValue, this.PolicyEquity)
  }

  /**
   * Performant version of getPolicyEquityPercentage() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param totalEquityChargeValue Typically from PolicyPeriod.TotalEquityChargeValue
   * @param policyEquity typically from PolicyPeriod.PolicyEquity
   * @return policyEquity as a percentage of the totalEquityChargeValue
   */
  function getPolicyEquityPercentage(totalEquityChargeValue: MonetaryAmount, policyEquity: MonetaryAmount): Double {
    return totalEquityChargeValue.IsZero ? null : (100 * policyEquity / totalEquityChargeValue) as Double
  }

  @ImpactedByArchiving
  function getPercentEarned(): Double {
    return getPercentEarned(this.getTotalValue(null), this.getEarnedValue(null))
  }

  /**
   * Performant version of getPercentEarned() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param totalValue Typically from PolicyPeriod.getTotalValue(null)
   * @param earned Typically from PolicyPeriod.getEarnedValue(null)
   * @return The PolicyPeriod's earned value as a numeric percentage of total value
   */
  function getPercentEarned(totalValue: MonetaryAmount, earned: MonetaryAmount): Double {
    return totalValue.IsZero ? null : (100 * earned / totalValue) as Double
  }

  @ImpactedByArchiving
  function getDaysTillPaidThruDate(): Double {
    return getDaysTillPaidThruDate(this.PaidThroughDate)
  }

  /**
   * Performant version of getDaysTillPaidThruDate() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param paidThroughDate Typically from PolicyPeriod.PaidThroughDate
   * @return Days until the PolicyPeriod is paid through
   */
  function getDaysTillPaidThruDate(paidThroughDate: Date): Double {
    if (paidThroughDate == null) {
      return null
    }
    var todayOrPolicyPerExpirDate = DateUtil.currentDate()
    if (this.PolicyPerExpirDate.before(todayOrPolicyPerExpirDate)) {
      todayOrPolicyPerExpirDate = this.PolicyPerExpirDate
    }
    return todayOrPolicyPerExpirDate.differenceInDays(paidThroughDate)
  }

  @ImpactedByArchiving
  function getPaidToBilledRatio(): Double {
    var billedToday = this.getTotalValue(null) - this.UnbilledAmount
    return getPaidToBilledRatio(billedToday, this.PaidAmount)
  }

  /**
   * Performant version of getPaidToBilledRatio() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param totalBilled Typically from PolicyPeriod.getTotalValue(null) - PolicyPeriod.UnbilledAmount
   * @param paidAmount Typically from PolicyPeriod.PaidAmount
   * @return The PolicyPeriod's ratio of Paid to Billed as a numeric percentage
   */
  function getPaidToBilledRatio(totalBilled: BigDecimal, paidAmount: BigDecimal): Double {
    return totalBilled == 0 ? null : (100 * paidAmount / totalBilled) as Double
  }

  function getPaidToValueRatio(): Double {
    return getPaidToValueRatio(this.getTotalValue(null), this.PaidAmount)
  }

  /**
   * Performant version of getPaidToValueRatio() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param totalValue Typically from PolicyPeriod.getTotalValue(null)
   * @param paidAmount Typically from PolicyPeriod.PaidAmount
   * @return The PolicyPeriod's ratio of Paid to Value as a numeric percentage
   */
  function getPaidToValueRatio(totalValue: BigDecimal, paidAmount: BigDecimal): Double {
    return totalValue == 0 ? null : (100 * paidAmount / totalValue) as Double
  }

  /**
   * The {@link Cancellation} billing instruction for this {@link PolicyPeriod},
   * if any. (The {@code Cancellation} is archived along with the
   * {@code PolicyPeriod} and is not available when that is true; the
   * {@link #getCancellationDate CancellationDate} will always be not
   * {@code null}, if the {@code PolicyPeriod} was cancelled independent of
   * whether it is archived).
   */
  @ImpactedByArchiving
  property get Cancellation() : Cancellation {
    if (this.Canceled) {
      var query = Query.make(entity.Cancellation)
          .compare("AssociatedPolicyPeriod", Equals, this)
          .select().orderByDescending(QuerySelectColumns.path(Paths.make(entity.Cancellation#ModificationDate)))
      return query.FirstResult
    }
    return null
  }

  /**
   * The {@link Cancellation} date of this {@link PolicyPeriod}, if any. This is
   * independent of whether the {@code PolicyPeriod} is archived so a {@code
   * null} value always indicates that {@code PolicyPeriod} was never cancelled.
   */
  property get CancellationDate() : Date {
    return this.Archived
        ? this.PolicyPeriodArchiveSummary.CancellationDate
        : this.Cancellation.ModificationDate
  }

  /**
   * The end date of this {@link PolicyPeriod}, which is either its
   * {@link #getCancellationDate cancellation date} or its
   * {@link PolicyPeriod#getPolicyPerExpirDate effective expiration date}.
   */
  property get EndDate() : Date {
    return this.Canceled ? this.CancellationDate : this.PolicyPerExpirDate
  }

  function cancelByDelinquencyProcess(process: DelinquencyProcess) {
    if (canBeCancelled()) {
      this.CancelStatus = typekey.PolicyCancelStatus.TC_PENDINGCANCELLATION
      this.addHistoryFromGosu(gw.api.util.DateUtil.currentDate(),
          typekey.HistoryEventType.TC_POLICYCANCELED,
          DisplayKey.get("Java.PolicyHistory.PolicyCanceled"), null, null, false)
      addCancellationActivity(process)
      this.addEvent(PASMessageTransport.EVENT_CANCEL_NOW)
    }
  }

  private function canBeCancelled(): boolean {
    return this.ClosureStatus != typekey.PolicyClosureStatus.TC_CLOSED and
        this.CancelStatus != typekey.PolicyCancelStatus.TC_CANCELED and
        this.CancelStatus != typekey.PolicyCancelStatus.TC_PENDINGCANCELLATION
  }

  private function addCancellationActivity(process: DelinquencyProcess) {
    // create an activity for Policy Delinquency
    var actvty = new Activity(process.Bundle)
    actvty.ActivityPattern = gw.api.web.admin.ActivityPatternsUtil.getActivityPattern("general")
    actvty.Subject = DisplayKey.get("Java.PolicyActivity.Cancellation.Subject", this.DisplayName)
    actvty.Description = DisplayKey.get("Java.PolicyActivity.Cancellation.Description")
    actvty.Priority = typekey.Priority.TC_NORMAL
    gw.api.assignment.AutoAssignAssignee.INSTANCE.assignToThis(actvty)
    // Assign by Assignment rules
    process.addToActivities(actvty)
  }

  function rescindOrReinstateInternal() {
    if (this.CancelStatus == typekey.PolicyCancelStatus.TC_PENDINGCANCELLATION) {
      this.CancelStatus = typekey.PolicyCancelStatus.TC_PENDINGRESCINDCANCEL
      this.addEvent(PASMessageTransport.EVENT_RESCIND_CANCELLATION)
    } else {
      // TODO: Minh VU: it may be more appropriate to create an activity for UW
      // rather than to go and reinstate the policy period
      this.CancelStatus = typekey.PolicyCancelStatus.TC_PENDINGREINSTATEMENT
    }
  }

  function sendDunningLetterInternal() {
    this.addHistoryFromGosu(gw.api.util.DateUtil.currentDate(),
        HistoryEventType.TC_DUNNINGLETTERSENT,
        DisplayKey.get("Java.PolicyHistory.DunningLetterSent"), null, null, false)
  }

  function getDelinquencyReasons(): String {
    return this.ActiveDelinquencyProcesses.reduce("", \q, t -> {
      var currentReason = t.Reason.DisplayName
      if (not q.contains(currentReason)) {
        return q + ( q == "" ? "" : ", " ) + currentReason
      } else {
        return q
      }
    })
  }

  function getDelinquencyReason() : DelinquencyReason {
    var reason : DelinquencyReason
    var hasNotTakenReason = this.DelinquencyPlan.DelinquencyPlanReasons
        .hasMatch(\d -> d.DelinquencyReason == TC_NOTTAKEN)
    if (hasNotTakenReason
        and this.getActiveDelinquencyProcesses(TC_NOTTAKEN).Empty
        and this.PaidAmount.IsZero) {
      reason = TC_NOTTAKEN
    } else if (this.getActiveDelinquencyProcesses(TC_PASTDUE).Empty){
      reason = TC_PASTDUE
    }
    return reason
  }

  property get CancellationProcessEvent(): DelinquencyProcessEvent {
    var event: DelinquencyProcessEvent;
    for (dlnqProcess in this.DelinquencyProcesses) {
      var events = dlnqProcess.DelinquencyEvents
      var cancelEvent = events.firstWhere(
          \t -> t.EventName == DelinquencyEventName.TC_CANCELLATION)
      if (cancelEvent != null
          and (event == null or event.TargetDate.after(cancelEvent.TargetDate))){
        event = cancelEvent
      }
    }
    return event;
  }

  @ImpactedByArchiving
  function getChargeTransactionWrappers(): TransactionWrapper[] {
    var transactions = new ArrayList<TransactionWrapper>()
    for (charge in this.Charges)
    {
      transactions.add(
          new TransactionWrapper(charge.ChargeInitialTxn, TransactionWrapper.TransactionType.CHARGE)
      )
    }
    // commission adjust transactions
    var txns = this.getCommissionAdjustedTransactions()
    for (txn in txns.iterator())
    {
      // don't show the reversed transaction, just show the commission amount changed
      if (not txn.isReversal()){
        transactions.add(
            new TransactionWrapper(txn, TransactionWrapper.TransactionType.CHARGE)
        )
      }
    }
    transactions.addAll(
        (this.getChargePaymentTransactions().map(\transaction -> new TransactionWrapper(transaction, TransactionWrapper.TransactionType.PAYMENTANDCREDIT)))?.toList()
    )
    return transactions.sortBy(\c -> c.transaction.TransactionDate).toTypedArray()
  }

  function getTotalCommission(): MonetaryAmount {
    return this.PrimaryPolicyCommission.CommissionExpenseBalance
  }

  @ImpactedByArchiving
  function getChargeCommissionRate(): BigDecimal {
    if (this.TotalValue.signum() == 0){
      return 0;
    }
    return getTotalCommission().multiply(100).divide(this.TotalValue, 2, RoundingMode.DOWN)
  }

  @ImpactedByArchiving
  @java.lang.Deprecated
  /**
   *  This method uses the deprecated field CommissionPaid, which returns the "Commission Expense" - "Commission Reserve"
   *  (commission settled instead of commission that has been paid). If interested in the same amount, please use
   *  {@link #getSettledCommissionRate()} if interesed on the same amount, or {@link #getCommissionPaidRate()} if interested
   *  on the paid commission rate.
   */
  function getPaidCommissionRate(): BigDecimal {
    return getCommissionSettledRate()
  }

  function getCommissionSettledRate() : BigDecimal {
    if (this.PaidOrWrittenOffAmount.signum() == 0){
      return 0;
    }
    return this.PrimaryPolicyCommission.CommissionSettled.multiply(100).divide(this.PaidOrWrittenOffAmount, 2, RoundingMode.DOWN)
  }

  function getCommissionPaidRate(): BigDecimal {
    if (this.PaidOrWrittenOffAmount.signum() == 0){
      return 0;
    }
    return this.PrimaryPolicyCommission.PaidCommission.multiply(100).divide(this.PaidOrWrittenOffAmount, 2, RoundingMode.DOWN)
  }

  property set RequireFinalAudit(required: boolean) {
    if (required) {
      this.scheduleFinalAudit()
    } else {
      this.waiveFinalAudit()
    }
  }

  property get RequireFinalAudit(): boolean {
    return this.ClosureStatus == PolicyClosureStatus.TC_OPENLOCKED ? true : false
  }

  @ImpactedByArchiving
  function hasPastInvoiceItems(): boolean {
    var invoices = this.InvoiceItemsSortedByEventDate
    if (invoices.length == 0) return false;
    return invoices[0].EventDate.before(DateUtil.currentDate())
  }

  function makeSingleCashPaymentUsingNewBundle(amount: MonetaryAmount) {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var policyPeriod = bundle.add(Query.make(PolicyPeriod).compare("ID", Equals, this.ID).select().AtMostOneRow)
      DirectBillPaymentFactory.payPolicyPeriod(policyPeriod.Account, policyPeriod, amount)
    })
  }

  property get IsInForce(): boolean {
    return (this.Status == PolicyPeriodStatus.TC_INFORCE) &&
        (
            this.CancelStatus == PolicyCancelStatus.TC_OPEN ||
                this.CancelStatus == PolicyCancelStatus.TC_PENDINGCANCELLATION ||
                this.CancelStatus == PolicyCancelStatus.TC_PENDINGRESCINDCANCEL
        )
  }
}
