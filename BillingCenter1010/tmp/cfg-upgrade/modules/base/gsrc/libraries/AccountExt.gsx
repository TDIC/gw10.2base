package libraries

uses gw.api.database.DBFunction
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.domain.delinquency.DelinquencyTarget
uses gw.api.locale.DisplayKey
uses gw.api.upgrade.Coercions
uses gw.api.util.DateUtil
uses gw.api.web.payment.DirectBillPaymentFactory
uses gw.pl.currency.MonetaryAmount
uses gw.search.ReversibleChargeSearchCriteria

/**
 * Defines enhancements for an {@link Account}.
 */
@Export
enhancement AccountExt : Account {
  /**
   * A reversible {@link ChargePatternKey#ACCOUNTLATEFEE Account Late Fee}
   * or {@link ChargePatternKey#POLICYLATEFEE Policy Late Fee}
   * {@link ReversibleChargeSearchCriteria SearchCriteria} for this
   * {@link Account}.
   */
  property get ReversibleLateFeeSearchCriteria() : ReversibleChargeSearchCriteria {
    final var criteria = new ReversibleChargeSearchCriteria()
    criteria.Account = this
    criteria.ChargePatterns =
        {ChargePatternKey.ACCOUNTLATEFEE.get(), ChargePatternKey.POLICYLATEFEE.get()}
    return criteria
  }

  property get OpenDelinquencyTargets(): DelinquencyTarget[] {
    var all: List<DelinquencyTarget> = {}
    all.add(this)
    all.addAll(this.OpenPolicyPeriods?.toList())
    return all.toTypedArray()
  }

  function getPaymentDueDateToAvoidDelinquencyOn(date : Date) : Date {
    var dueInvoice = getMostRecentPastDueInvoiceAsOf(date)
    if (dueInvoice != null) {
      return dueInvoice.PaymentDueDate
    } else {
      return null
    }
  }

  /**
   * The most recent {@link AccountInvoice} where the {@link AccountInvoice#PaymentDueDate
   * PaymentDueDate} is "date" or before.
   */
  function getMostRecentPastDueInvoiceAsOf(date : Date) : Invoice {
    final var invoiceQuery = Query.make<Invoice>(AccountInvoice)
    invoiceQuery.compare(AccountInvoice#Account, Equals, this)
    invoiceQuery.compare(AccountInvoice#PaymentDueDate, LessThanOrEquals, date)
    var min = invoiceQuery.select({QuerySelectColumns.dbFunction(
        DBFunction.Max(invoiceQuery.getColumnRef(AccountInvoice#EventDate.PropertyInfo.Name)))})
    invoiceQuery
        .compare(AccountInvoice#EventDate, Equals, Coercions.makeDateFrom(min.AtMostOneRow.getColumn(0)))
    return invoiceQuery.select().FirstResult
  }

  function discoverDelinquencies(): List<DelinquencyProcess> {
    var newDelinquencyProcesses: List<DelinquencyProcess> = {}

    // start policyDlnqProcesses on not taken or past due policies
    for (policyPeriod in this.OpenPolicyPeriods) {
      var isPendingCancellation = policyPeriod.CancelStatus == TC_PENDINGCANCELLATION

      if (policyPeriod.EligibleToStartDelinquency
            and not policyPeriod.isDirectOrImpliedTargetOfHoldType(TC_DELINQUENCY)
            and not isPendingCancellation) {
        var reason = policyPeriod.getDelinquencyReason()
        if (reason != null) {
          var process = policyPeriod.startDelinquency(reason)
          newDelinquencyProcesses.add(process)
        }
      }
    }

    // start DelinquencyProcesses on past due account level charges
    if (( this.getActiveAccountLevelDelinquencyProcess(TC_PASTDUE) == null )
        and this.EligibleToStartDelinquency) {
      var process = this.startDelinquency(TC_PASTDUE)
      newDelinquencyProcesses.add(process)
    }

    if (this.Collateral.DueUnsettledAmount.compareTo(this.DelinquencyPlan.getPolEnterDelinquencyThreshold(this.getCurrency())) == 1) {
      // var process = start a delinquency process for past due collateral charges
      // newDelinquencyProcesses.add(process)

      // NOTE: Collateral Delinquency Processes do not exist, so if you choose to instead start a Policy or
      // Account level delinquency process, this should be carefully marked, so that is recognizable
      // as a Collateral Delinquency Process in the future.  See DelinquencyProcessExt.onChargesPaid()
    }
    return newDelinquencyProcesses
  }

  property get DelinquencyReasons(): String {
    var reasons: List<String> = {}
    for (p in this.ActiveDelinquencyProcesses) {
      var reason = p.Reason.DisplayName
      if (not reasons.contains(reason)) {
        reasons.add(reason)
      }
    }
    var result: String
    for (s in reasons index i) {
      if (i == 0) {
        result = s
      }
      else {
        result = result + ", " + s
      }
    }
    return result
  }

  // methods used for Account-level delinquency processes (via workflow) ==========================================

  function sendDunningLetterInternal() {
    this.addHistoryFromGosu(gw.api.util.DateUtil.currentDate(),
        HistoryEventType.TC_DUNNINGLETTERSENT,
        DisplayKey.get("Java.AccountHistory.DunningLetterSent"), null, null, false)
  }

  // methods used by new transaction wizards ======================================================================

  function makeSingleCashPaymentUsingNewBundle(amount: MonetaryAmount) {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var account = bundle.add(gw.api.database.Query.make(Account).compare("ID", Equals, this.ID).select().AtMostOneRow)
      DirectBillPaymentFactory.pay(account, amount)
    })
  }

  function makeSingleCashDirectBillMoneyReceivedUsingNewBundle(amount: MonetaryAmount) {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var account = bundle.add(gw.api.database.Query.make(Account).compare("ID", Equals, this.ID).select().AtMostOneRow)
      var cashInstrument = new PaymentInstrument()
      cashInstrument.PaymentMethod = PaymentMethod.TC_CASH
      var moneyReceived = DirectBillPaymentFactory.createDirectBillMoneyRcvd(account, cashInstrument, amount)
      moneyReceived.execute()
    })
  }
}