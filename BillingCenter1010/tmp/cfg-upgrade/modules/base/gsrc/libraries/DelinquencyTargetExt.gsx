package libraries

uses gw.api.locale.DisplayKey
uses gw.api.domain.delinquency.DelinquencyTarget
uses gw.api.web.accounting.UIWriteOffCreation
uses gw.api.web.accounting.WriteOffFactory

uses java.util.Date

@Export
enhancement DelinquencyTargetExt : DelinquencyTarget
{
  final function hasActiveDelinquenciesOutOfGracePeriod() : boolean {
    if ( this.hasActiveDelinquencyProcess() ) {
      var delinquencyProcesses = this.ActiveDelinquencyProcesses
      return delinquencyProcesses.hasMatch( \ dp -> dp.InceptionDate != null )
    }
    return false
  }

  function sendDunningLetter() {
    if ( this typeis Account ) {
      this.sendDunningLetterInternal()
    } else if ( this typeis PolicyPeriod ) {
      this.sendDunningLetterInternal();
    } else {
      throw DisplayKey.get("Java.DelinquencyTarget.CannotAccess.SendDunningLetter",  typeof this )
    }
  }

  function cancel(process : DelinquencyProcess) {
    if ( this typeis Account ) {
      // No-op: cannot cancel an account
    } else if ( this typeis PolicyPeriod ) {
      this.cancelByDelinquencyProcess(process)
    } else {
      throw DisplayKey.get("Java.DelinquencyTarget.CannotAccess.Cancel",  typeof this )
    }
  }

  function rescindOrReinstate() {
    if ( this typeis Account ) {
      // No-op: cannot rescind or reinstate an account
    } else if ( this typeis PolicyPeriod ) {
      this.rescindOrReinstateInternal()
    } else {
      throw DisplayKey.get("Java.DelinquencyTarget.CannotAccess.RescindOrReinstate",  typeof this )
    }
  }

  function writeoff() {
    var writeOff : UIWriteOffCreation
    if ( this typeis Account ) {
      writeOff = new UIWriteOffCreation(new WriteOffFactory(this.Bundle).createChargeGrossWriteOff(this))
      writeOff.setAmount(this.RemainingUnsettledBalance)

    } else if ( this typeis PolicyPeriod ) {
      writeOff = new UIWriteOffCreation(new WriteOffFactory(this.Bundle).createChargeGrossWriteOff(this))
      writeOff.setAmount(this.RemainingUnsettledBalance)
    } else {
      throw DisplayKey.get("Java.DelinquencyTarget.CannotAccess.DoWriteoff",  typeof this )
    }
    writeOff.setReason(WriteoffReason.TC_UNCOLLECTABLE)
    writeOff.doWriteOff()
  }

  function addHistoryEvent( eventDate : Date, type : typekey.HistoryEventType, reference : Object ) {
    ( this as HistoryEventContainer)
        .addHistoryFromGosu( eventDate, type, reference as String, null, null, false )
  }
}
