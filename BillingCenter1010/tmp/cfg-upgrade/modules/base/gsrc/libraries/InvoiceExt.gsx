package libraries

uses gw.api.locale.DisplayKey

@Export
enhancement InvoiceExt : Invoice {
  /**
   * @returns the Invoice Status, plus the Frozen status.  This is primarily for use in the UI where we want to display the two values together.
   */
  property get StatusForUI() : String {
    var statusString = this.Status.DisplayName

    if (this.Frozen) {
      return DisplayKey.get('DisplayName.Invoice.Status.Frozen', statusString)
    } else {
      return statusString
    }
  }
}
