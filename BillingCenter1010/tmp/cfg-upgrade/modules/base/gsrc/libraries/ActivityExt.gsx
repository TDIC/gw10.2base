package libraries

uses gw.api.database.IQueryBeanResult
uses gw.api.util.BCEmptyQueryProcessor
uses gw.plugin.Plugins
uses gw.plugin.approval.IActivityApprovablePlugin

uses java.lang.Deprecated

@Export
enhancement ActivityExt : Activity {
  /**
   * Assigns the ActivityCreatedByAppr upon its initial creation
   * @deprecated 10.0.0 - Please migrate all transaction approval relevant code to ActivityApprovablePlugin.gs
   */
  @Deprecated
  public static function assignActivityCreatedByAppr(activity : Activity) {
    Plugins.get(IActivityApprovablePlugin).assignActivityCreatedByAppr(activity)
  }

  /**
   * Reassigns the ActivityCreatedByAppr. Useful if the prior assignee of the Activity lacked sufficient authority to approve it. 
   * In the base product, this method is called in AutomaticDisbursementBatchProcess.
   * @deprecated 10.0.0 - Please migrate all transaction approval relevant code to ActivityApprovablePlugin.gs
   */
  @Deprecated
  public static function reassignActivityCreatedByApprToUserWithSufficientAuthority(activity : Activity) {
    Plugins.get(IActivityApprovablePlugin).reassignActivityCreatedByApprToUserWithSufficientAuthority(activity)
  }

  property get DocumentContainer(): DocumentContainer {
    if (this.Account != null) {
      return this.Account
    } else if (this.PolicyPeriod?.Policy != null) {
      return this.PolicyPeriod.Policy
    }
    return null
  }

  property get DocumentSearchCriteria(): DocumentSearchCriteria {
    var searchCriteria = new DocumentSearchCriteria()
    searchCriteria.DateCriterionChoice.DateRangeChoice = null
    if (this.Account != null) {
      searchCriteria.Account = this.Account
      return searchCriteria
    } else if (this.PolicyPeriod?.Policy != null) {
      searchCriteria.Policy = this.PolicyPeriod.Policy
      return searchCriteria
    }
    return null
  }

  property get DocumentSearchResult(): IQueryBeanResult<Document> {
    var searchCriteria = DocumentSearchCriteria
    if (searchCriteria != null) {
      return searchCriteria.performSearch(false) as IQueryBeanResult<Document>
    } else {
      return new BCEmptyQueryProcessor<Document>(Document)
    }
  }
}