package gw.document

@Export
public class DocumentMetadataBCHelper extends DocumentDetailsApplicationHelper {
  private var _documentNameEnabled : boolean
  private var _descriptionEnabled : boolean
  private var _mimeTypeEnabled : boolean
  private var _languageEnabled : boolean
  private var _authorEnabled : boolean
  private var _recipientEnabled : boolean
  private var _statusEnabled : boolean
  private var _securityTypeEnabled : boolean
  private var _typeEnabled : boolean

  public construct(documents : Document[]) {
    super(documents);
  }

  public construct(document : Document) {
    this({document});
  }

  override public property set AllFieldsEnabled(allFieldsEnabled : Boolean) {
    super.AllFieldsEnabled = allFieldsEnabled
    DocumentNameEnabled = allFieldsEnabled
    DescriptionEnabled = allFieldsEnabled
    MimeTypeEnabled = allFieldsEnabled
    LanguageEnabled = allFieldsEnabled
    AuthorEnabled = allFieldsEnabled
    RecipientEnabled = allFieldsEnabled
    StatusEnabled = allFieldsEnabled
    SecurityTypeEnabled = allFieldsEnabled
    TypeEnabled = allFieldsEnabled
  }

  public property get DocumentNameEnabled() : Boolean {
    return _documentNameEnabled
  }

  public property set DocumentNameEnabled(documentNameEnabled : Boolean) {
    if (_documentNameEnabled && !documentNameEnabled) {
      restoreFields(Document.NAME_PROP.get())
    }
    _documentNameEnabled = documentNameEnabled
  }

  public property get Name() : String {
    return getFields(Document.NAME_PROP.get()) as String
  }

  public property set Name(name : String) {
    setFields(Document.NAME_PROP.get(), name)
  }

  public property get DescriptionEnabled() : Boolean {
    return _descriptionEnabled
  }

  public property set DescriptionEnabled(descriptionEnabled : Boolean) {
    if (_descriptionEnabled && !descriptionEnabled) {
      restoreFields(Document.DESCRIPTION_PROP.get())
    }
    _descriptionEnabled = descriptionEnabled
  }

  public property get Description() : String {
    return getFields(Document.DESCRIPTION_PROP.get()) as String
  }

  public property set Description(description : String) {
    setFields(Document.DESCRIPTION_PROP.get(), description)
  }

  public property get MimeTypeEnabled() : Boolean {
    return _mimeTypeEnabled
  }

  public property set MimeTypeEnabled(mimeTypeEnabled : Boolean) {
    if (_mimeTypeEnabled && !mimeTypeEnabled) {
      restoreFields(Document.MIMETYPE_PROP.get())
    }
    _mimeTypeEnabled = mimeTypeEnabled
  }

  public property get MimeType() : String {
    return getFields(Document.MIMETYPE_PROP.get()) as String
  }

  public property set MimeType(mimeType : String) {
    setFields(Document.MIMETYPE_PROP.get(), mimeType)
  }

  public property get LanguageEnabled() : Boolean {
    return _languageEnabled
  }

  public property set LanguageEnabled(languageEnabled : Boolean) {
    if (_languageEnabled && !languageEnabled) {
      restoreFields(Document.LANGUAGE_PROP.get())
    }
    _languageEnabled = languageEnabled
  }

  public property get Language() : LanguageType {
    return (getFields(Document.LANGUAGE_PROP.get())) as LanguageType
  }

  public property set Language(language : LanguageType) {
    setFields(Document.LANGUAGE_PROP.get(), language)
  }

  public property get AuthorEnabled() : Boolean {
    return _authorEnabled
  }

  public property set AuthorEnabled(authorEnabled : Boolean) {
    if (_authorEnabled && !authorEnabled) {
      restoreFields(Document.AUTHOR_PROP.get())
    }
    _authorEnabled = authorEnabled
  }

  public property get Author() : String {
    return getFields(Document.AUTHOR_PROP.get()) as String
  }

  public property set Author(author : String) {
    setFields(Document.AUTHOR_PROP.get(), author)
  }

  public property get RecipientEnabled() : Boolean {
    return _recipientEnabled
  }

  public property set RecipientEnabled(recipientEnabled : Boolean) {
    if (_recipientEnabled && !recipientEnabled) {
      restoreFields(Document.RECIPIENT_PROP.get())
    }
    _recipientEnabled = recipientEnabled
  }

  public property get Recipient() : String {
    return getFields(Document.RECIPIENT_PROP.get()) as String
  }

  public property set Recipient(recipient : String) {
    setFields(Document.RECIPIENT_PROP.get(), recipient)
  }

  public property get StatusEnabled() : Boolean {
    return _statusEnabled
  }

  public property set StatusEnabled(statusEnabled : Boolean) {
    if (_statusEnabled && !statusEnabled) {
      restoreFields(Document.STATUS_PROP.get())
    }
    _statusEnabled = statusEnabled
  }

  public property get Status() : DocumentStatusType {
    return getFields(Document.STATUS_PROP.get()) as DocumentStatusType
  }

  public property set Status(status : DocumentStatusType) {
    setFields(Document.STATUS_PROP.get(), status)
  }

  public property get SecurityTypeEnabled() : Boolean {
    return _securityTypeEnabled
  }

  public property set SecurityTypeEnabled(securityTypeEnabled : Boolean) {
    if (_securityTypeEnabled && !securityTypeEnabled) {
      restoreFields(Document.SECURITYTYPE_PROP.get())
    }
    _securityTypeEnabled = securityTypeEnabled
  }

  public property get SecurityType() : DocumentSecurityType {
    return getFields(Document.SECURITYTYPE_PROP.get()) as DocumentSecurityType
  }

  public property set SecurityType(securityType : DocumentSecurityType) {
    // Checking if user has permission to create document with this securityType
    // DocumentUID will be null if it is a new document and will be non null for an existing document
    // For existing document the metadata is not editable in UI if the user does not have permission to edit
    if(_documents!=null && _documents.length>0 && _documents[0].DocUID == null) {
      // Setting security type on the first document using the setter method to check if exception is thrown
      _documents[0].SecurityType = securityType
    }

    setFields(Document.SECURITYTYPE_PROP.get(), securityType)
  }

  public property get TypeEnabled() : Boolean {
    return _typeEnabled
  }

  public property set TypeEnabled(typeEnabled : Boolean) {
    if (_typeEnabled && !typeEnabled) {
      restoreFields(Document.TYPE_PROP.get())
    }
    _typeEnabled = typeEnabled
  }

  public property get Type() : DocumentType {
    return getFields(Document.TYPE_PROP.get()) as DocumentType
  }

  public property set Type(type : DocumentType) {
    setFields(Document.TYPE_PROP.get(), type)
  }
}
