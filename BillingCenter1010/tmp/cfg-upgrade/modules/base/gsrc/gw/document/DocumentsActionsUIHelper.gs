package gw.document

uses gw.api.admin.MessagingUtil
uses gw.api.locale.DisplayKey
uses gw.api.system.BCLoggerCategory
uses gw.api.system.PLConfigParameters
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentContentSource
uses gw.plugin.document.IDocumentMetadataSource
uses gw.plugin.document.IDocumentProduction
uses gw.plugin.document.IDocumentTemplateSerializer
uses gw.plugin.document.IDocumentTemplateSource

/**
 * UI helper functions for the Documents UI in DocumentsLVs and DocumentPropertiesPopups
 */
@Export
class DocumentsActionsUIHelper {

  var _contentOutboundAvailable : Boolean = null
  var _metadataOutboundAvailable : Boolean = null

  /********************* Plugins Helpers *********************************************
   *
   *  IDocumentContentSource and IDocumentMetadataSource plugin helpers.
   *
   ***********************************************************************************/

  /**
   * Determines the asynchronous message to display in the documents actions while the
   * document is being stored in the DMS.
   * @return Asynchronous message for document actions
   */
  property get AsynchronousActionsMessage() : String {
    return DocumentStoreSuspended ?
        DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.Pending") :
        DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.Pending.Refresh")
  }

  /**
   * Check if the message for the DocumentStore disabled needs to
   * show in the Documents Tab and the Document properties page.
   * @return whether the DocumentStore suspended message should show
   */
  property get ShowDocumentStoreSuspendedWarning() : boolean {
    return DocumentStoreSuspended
  }

  /**
   * If the Metadata plugin is enabled, this returns whether it is available
   * Otherwise, it returns true
   */
  property get ShowMetadataServerDownWarning() : boolean {
    return MetadataSourceEnabled and not DocumentMetadataServerAvailable
  }

  /**
   * If the Content plugin is enabled, this returns whether it is available
   * Otherwise, it returns true
   */
  property get ShowContentServerDownWarning() : boolean {
    return ContentSourceEnabled and not DocumentContentServerAvailable
  }

  /**
   * Gets the status of the DocumentStore transport for the Asynchronous configuration
   * of the IDocumentContentSource plugin.
   *
   * @return the MessageDestinationStatus for DocumentStore
   */
  property get DocumentStoreSuspended () : boolean {
    var documentStoreDestination = MessagingUtil.getDestinationInfo().where(\destInfo -> destInfo.DestId == 324)
    if (documentStoreDestination.HasElements) {
      if (documentStoreDestination.Count > 1) {
        BCLoggerCategory.DOCUMENT.error("You have defined more than one configuration for the destination 324 in your messaging-config.xml. Please" +
            "take a look and make sure you only have one definition")
      }
      return documentStoreDestination.first().Status == MessageDestinationStatus.TC_SUSPENDED

    } else {
      // If the DocumentStore destination is disabled, there is not a DestinationInfo for it in the MessagingUtil
      return false
    }
  }

  /**
   * Checks whether the actions in the Documents ListView are not visible because document is 'in flight'
   * @param document
   * @return whether the document is 'in flight'
   */
  function isDocumentPending(document: Document) : boolean {
    return document.PendingDocUID != null and document.DMS
  }

  /**
   * Determines if the IDocumentContentSource plugin is enabled and available to communicate with the DMS
   * @return whether the DMS is available to do actions with the documents
   */
  property get DocumentContentServerAvailable() : boolean {
    if(_contentOutboundAvailable == null) {
      //We cache the value because the call is expensive
      _contentOutboundAvailable = gw.plugin.Plugins.get(IDocumentContentSource).isOutboundAvailable()
    }
    return ContentSourceEnabled and _contentOutboundAvailable
  }

  /**
   * Determines if the IDocumentMetadataSource plugin is enabled and available to communicate with the DMS
   * @return whether the DMS is available to do access document metadata
   */
  property get DocumentMetadataServerAvailable() : boolean {
    if(_metadataOutboundAvailable == null) {
      //We cache the value because the call is expensive
      _metadataOutboundAvailable = (gw.plugin.Plugins.get(IDocumentMetadataSource)).isOutboundAvailable()
    }
    return MetadataSourceEnabled and _metadataOutboundAvailable
  }

  /**
   * Checks if the IDocumentContentSource plugin is configured enabled.
   * @return whether the IDCS plugin is enabled
   */
  property get ContentSourceEnabled () : boolean {
    return gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentContentSource)
  }

  /**
   * Checks if the IDocumentMetadataSource plugin is configured enabled.
   * @return whether the IDMS plugin is enabled
   */
  property get MetadataSourceEnabled () : boolean {
    return gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentMetadataSource)
  }

  /**
   * Checks if the document is stored in the DMS and it is accessible. The documents that
   * are in the temporary location are not in the DMS.
   * @param document
   * @return
   */
  private function documentStoredInDMS(document : Document) : boolean {
    return ContentSourceEnabled and document.PendingDocUID == null and document.DMS
  }

  /********************* Available Helpers *********************************************
   *
   *  Document actions' availability helpers.
   *
   *************************************************************************************/

  /**
   * Tells whether to enable document actions related to the metadata of the
   * document that are independent on the content.
   * @return true if the document metadata server is available, false otherwise
   */
  property get DocumentMetadataActionsAvailable() : boolean {
    if (MetadataSourceEnabled) {
      return DocumentMetadataServerAvailable
    }
    return ContentSourceEnabled
  }

  /**
   * Whether or not Content actions are available. This depends on whether the Content Server is available according
   * to the enabled plugin
   */
  property get DocumentContentActionsAvailable() : boolean {
    return DocumentContentServerAvailable
  }

  /**
   * For template actions to be available, all 4 plugins must be available
   * The document must be picked (TemplateSource), generated (DocumentProduction),
   * and stored(Metadata and Content)
   * @return
   */
  property get DocumentTemplateActionsAvailable() : boolean {
    return DocumentProductionAvailable
        and DocumentTemplateSourceAvailable
        and DocumentMetadataActionsAvailable
        and DocumentContentActionsAvailable
  }

  property get DocumentLinkActionsAvailable() : boolean {
    return DocumentMetadataActionsAvailable
        and DocumentContentActionsAvailable
  }

  /**
   * returns whether the DocumentProduction plugin is enabled
   */
  property get DocumentProductionAvailable() : boolean {
    return Plugins.isEnabled(IDocumentProduction)
  }

  /**
   * returns whether the TemplateSource plugin is available
   * For the LocalDocumentTemplateSource, the TemplateSerializer needs to be enabled as well
   */
  property get DocumentTemplateSourceAvailable() : boolean {
    return Plugins.isEnabled(IDocumentTemplateSource) and Plugins.isEnabled(IDocumentTemplateSerializer)
  }

  function isViewDocumentContentAvailable(document: Document) : boolean {
    return DocumentContentServerAvailable and perm.Document.view(document) and document.DMS and
        document.ContentExists and document.PendingDocUID == null and
        document.DocumentMimeTypeAllowed
  }

  function isDownloadDocumentContentAvailable(document: Document) : boolean {
    return DocumentContentServerAvailable and document.ContentExists
  }

  function isDeleteDocumentLinkAvailable(document: Document) : boolean {
    return DocumentMetadataActionsAvailable
  }

  function isRemoveDocumentLinkAvailable() : boolean {
    return DocumentMetadataActionsAvailable
  }

  function isUploadDocumentContentAvailable(document: Document) : boolean {
    return DocumentContentServerAvailable and
        DocumentMetadataActionsAvailable and
        document.ContentExists
  }

  function isDocumentContentActionsAvailableInDocumentProperties(editable: boolean, document: Document) : boolean {
    return not editable and document.ContentExists and document.PendingDocUID == null
        and DocumentContentServerAvailable
  }

  /********************* Visible Helpers *********************************************
   *
   *  Document actions' visibility helpers.
   *
   ***********************************************************************************/

  function isDownloadDocumentContentVisible(document: Document) : boolean {
    return perm.Document.view(document) and documentStoredInDMS(document)
  }

  function isUploadDocumentContentVisible(document: Document) : boolean {
    return PLConfigParameters.DisplayDocumentEditUploadButtons.Value and
        isDocumentEditable(document) and documentStoredInDMS(document)

  }

  /**
   * This is intended to control the visibility of the delete document from claim action.
   * Represented by the "delete" icon.
   * @param document to delete
   * @return whether the document can be deleted
   */
  function isDeleteDocumentLinkVisible(document: Document) : boolean {
    return document.PendingDocUID == null and perm.Document.delete(document)
  }

  function isEditDocumentDetailsVisibleInDocumentProperties(document: Document) : boolean {
    return PLConfigParameters.DisplayDocumentEditUploadButtons.Value
        and document.PendingDocUID == null
        and isDocumentEditable(document)
  }

  function isUploadDocumentContentVisibleInDocumentProperties(editable: boolean, document: Document) : boolean {
    return not editable and ContentSourceEnabled and isDocumentEditable(document) and
        document.DMS and PLConfigParameters.DisplayDocumentEditUploadButtons.Value
  }

  function isDownloadDocumentContentVisibleInDocumentProperties(editable: boolean, document: Document) : boolean {
    return not editable and ContentSourceEnabled and perm.Document.view(document) and document.DMS
  }

  function isIconSpacerVisible(editable: boolean, document: Document) : boolean {
    return isDownloadDocumentContentVisibleInDocumentProperties(editable, document)
  }

  function isShowAsynchronousRefreshAction(documents : Document[]) : boolean {
    return documents.hasMatch( \ d -> isDocumentPending(d)) and not ShowDocumentStoreSuspendedWarning
  }

  /********************* Tooltip Helpers *********************************************
   *
   *  Document actions' tooltips helpers. There are different tooltips based on the
   *  availability and visibility.
   *
   ***********************************************************************************/

  property get UploadDocumentContentTooltip () : String{
    return DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.Upload.Tooltip")
  }

  property get DownloadDocumentContentTooltip () : String {
    return DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.Download.Tooltip")
  }

  function RemoveDocumentLinkTooltip () : String {
    return DisplayKey.get("Web.DocumentsLV.Document.Remove.Tooltip")
  }

  function DeleteDocumentTooltip (document : Document, undeletableDocumentPublicIds: java.util.Set<String> = null
      ) : String {
    if (undeletableDocumentPublicIds != null and undeletableDocumentPublicIds.contains(document.PublicID)) {
      return DisplayKey.get("Web.DocumentsLV.Document.Delete.LinkToServiceRequest.Tooltip")
    }
    return DisplayKey.get("Web.DocumentsLV.Document.Delete.Tooltip")
  }

  // Refer to unlinking a document from an entity other than removing it
  property get RemoveDocumentReferenceLinkTooltip() : String {
    return DisplayKey.get("Web.DocumentsLV.Document.Remove.Tooltip")
  }

  property get ViewDocumentPropertiesTooltip() : String {
    return DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.ViewProperties.Tooltip")
  }

  public function getViewDocumentContentTooltip(document: Document) : String {
    if (document.getDocUID() == DocumentsUtilBase.NO_FILE_CONTENT_UID) {
      return DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.ViewContent.IndicateExistence")
    } else if (!isDocumentMimeTypeAllowed(document.MimeType)) {
      return DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.ViewContent.MissingMimetype.Tooltip")
    } else if (!perm.Document.view(document)) {
      return DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.ViewContent.MissingViewPermission.Tooltip")
    }

    return DisplayKey.get("Web.DocumentsLV.Document.ViewAndEdit.ViewContent.Tooltip")
  }

  public function isDocumentMimeTypeAllowed(documentMimeType: String) : boolean {
    if (documentMimeType != null ) {
      return DocumentsUtilBase.getMimeTypes().contains(documentMimeType)
    }
    return false;
  }

  private function isDocumentEditable(document : Document) : boolean {
    return document != null
        and perm.Document.edit(document)
        and document.Status != DocumentStatusType.TC_FINAL
  }

}