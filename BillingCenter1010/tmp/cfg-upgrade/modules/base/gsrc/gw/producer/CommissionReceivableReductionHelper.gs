package gw.producer
uses gw.api.locale.DisplayKey
uses gw.pl.currency.MonetaryAmount
uses com.google.common.collect.Lists

@Export
class CommissionReceivableReductionHelper {
  
  private var _producer : Producer as Producer
  private var _producerCode : ProducerCode as ProducerCode
  private var _amount : MonetaryAmount as Amount

  construct(producerToWriteoff : Producer) {
    Producer = producerToWriteoff
  }

  function doWriteoff() {
    ProducerCode.writeoffCommissionRecovery( Amount )
  }


}
