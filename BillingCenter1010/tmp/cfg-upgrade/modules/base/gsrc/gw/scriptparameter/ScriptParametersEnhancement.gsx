package gw.scriptparameter

@Export
enhancement ScriptParametersEnhancement: ScriptParameters {

  public static property get StandardDelinquencyPlan(): String {
    return ScriptParameters.getParameterValue("StandardDelinquencyPlan") as String;
  }

  public static property get LegacyDelinquencyPlan(): String {
    return ScriptParameters.getParameterValue("LegacyDelinquencyPlan") as String;
  }

  public static property get StandardAgencyBillPlan(): String {
    return ScriptParameters.getParameterValue("StandardAgencyBillPlan") as String;
  }

  public static property get LegacyAgencyBillPlan(): String {
    return ScriptParameters.getParameterValue("LegacyAgencyBillPlan") as String;
  }

}
