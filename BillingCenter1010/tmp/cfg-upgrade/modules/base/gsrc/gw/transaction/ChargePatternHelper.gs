package gw.transaction

uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.locale.DisplayKey
uses gw.lang.reflect.IType

uses java.lang.IllegalArgumentException
uses java.util.Collections
uses java.util.List

@Export
class ChargePatternHelper {

  static function getChargePatterns( ownerType : IType ) : List<ChargePattern> {
    return getChargePatterns( ownerType, UserTransactionType.NONE)
  }

  static function getChargePatterns( ownerType : IType, transactionType : UserTransactionType) : List<ChargePattern> {
    if (!gw.api.util.TypeUtil.isNominallyOrStructurallyAssignable(TAccountOwner.Type, ownerType)) {
      throw new IllegalArgumentException( "ownerType must be a subtype of TAccountOwner. Got: " + ownerType )
    }
    var allChargePatterns = gw.api.web.accounting.ChargePatternHelper.getAllChargePatterns()
    var chargePatterns = allChargePatterns.where(\ chargePattern -> ownerType == chargePattern.getTAccountOwnerType())

    chargePatterns = chargePatterns.where(\chargePattern -> chargePattern != ChargePatternKey.INVOICEFEE.get())
    chargePatterns = chargePatterns.where(\chargePattern -> chargePattern != ChargePatternKey.INSTALLMENTFEE.get())
    if (transactionType == UserTransactionType.FEE_OR_GENERAL) {
      return chargePatterns.where(\chargePattern -> chargePattern.Category == ChargeCategory.TC_FEE or (not chargePattern.InternalCharge))
    } else if (transactionType == UserTransactionType.RECAPTURE) {
      return chargePatterns.where(\chargePattern -> chargePattern.Recapture)
    } else if (transactionType == UserTransactionType.NONE) {
      return chargePatterns // return them all unfiltered
    } else {
      throw new IllegalArgumentException(DisplayKey.get("Web.ChargePatternHelper.Error.UnsupportedUserTransactionType", transactionType))
    }
  }

  static function getChargePattern( chargePatternName : String ) : ChargePattern {
    return gw.api.web.accounting.ChargePatternHelper.getChargePattern( chargePatternName )
  }

  static function getChargePatterns( owner : TAccountOwner, transactionType : UserTransactionType ) : List<ChargePattern> {
    return getChargePatterns( typeof owner, transactionType)
  }

  static property get ReversableChargePatterns() : List<ChargePattern> {
      var chargePatterns = gw.api.web.accounting.ChargePatternHelper.getAllChargePatterns()
      return chargePatterns.where(\ chargePattern -> chargePattern.Reversible)
    }

  static function getAvailableInvoiceTreatments( chargePattern : ChargePattern ) : List<InvoiceTreatment> {
    if (chargePattern.getTAccountOwnerPattern() != null && PolicyPeriod.Type == chargePattern.TAccountOwnerType) {
      return InvoiceTreatment.getTypeKeys(false)
    } else {
      return Collections.singletonList( InvoiceTreatment.TC_ONETIME )
    }
  }
}
