package gw.plugin.account.impl

uses gw.plugin.account.IAutomaticDisbursement

@Export
class AutomaticDisbursement implements IAutomaticDisbursement {
  construct() {
  }

  override function shouldHold(account: Account) : boolean  {
    return false
  }

  override function shouldHold(unappliedFund : UnappliedFund) : boolean  {
    return false
  }
}