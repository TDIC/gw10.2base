package gw.plugin.payment.impl

uses gw.plugin.payment.IBatchPayment

@Export
class BatchPayment implements IBatchPayment {

  override function selectDirectBillPaymentUnappliedFund(batchPaymentEntry : BatchPaymentEntry, defaultUnappliedFund : UnappliedFund) : UnappliedFund {
    return defaultUnappliedFund
  }
}