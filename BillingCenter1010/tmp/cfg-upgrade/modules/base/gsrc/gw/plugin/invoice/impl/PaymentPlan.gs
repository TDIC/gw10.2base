package gw.plugin.invoice.impl

uses gw.api.domain.charge.ChargeInitializer
uses gw.api.domain.invoice.ChargeInstallmentChanger
uses gw.plugin.invoice.IPaymentPlan

@Export
class PaymentPlan implements IPaymentPlan {

  construct() {
  }

  override function recreatePlannedInstallmentEventDatesForPaymentPlanChange(
                        charge : Charge, defaultRecreatedPlannedInstallmentEventDates : List<Date>) : List<Date> {
    return defaultRecreatedPlannedInstallmentEventDates
  }

  override function recreateInvoiceItemsForPaymentPlanChange(changer: ChargeInstallmentChanger) {
    // make no changes by default.
  }

  override function createFullSetOfInstallmentEventDates(
                        initializer: ChargeInitializer, defaultCreatedInstallmentEventDates: List <Date>): List <Date> {
    return defaultCreatedInstallmentEventDates
  }
}