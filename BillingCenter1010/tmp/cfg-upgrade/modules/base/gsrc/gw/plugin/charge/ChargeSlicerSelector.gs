package gw.plugin.charge

uses com.google.common.collect.ImmutableList
uses gw.api.database.Queries
uses gw.api.database.Relop
uses gw.api.domain.charge.ChargeInitializer
uses gw.api.domain.invoice.ChargeSlicer
uses gw.api.system.BCConfigParameters
uses gw.invoice.MatchPlannedInstallmentsChargeSlicer
uses gw.invoice.MatchPlannedInstallmentsMethods
uses gw.invoice.ReinstatementMatchPlannedInstallmentsChargeSlicer
uses gw.invoice.MatchCancellationSnapshotsChargeSlicer
uses gw.pl.currency.MonetaryAmount

/**
 * This plugin lets you select a ChargeSlicer for a given ChargeInitializer.
 */
@Export
class ChargeSlicerSelector implements IChargeSlicerSelector {

  override function selectChargeSlicer(chargeInitializer : ChargeInitializer) : ChargeSlicer {
    var billingInstruction = chargeInitializer.BillingInstruction

    if (billingInstruction typeis PolicyChange
        and noPaymentPlanModifiers(billingInstruction)
        and hasMatchablePlannedInstallments(chargeInitializer)) {
      return new MatchPlannedInstallmentsChargeSlicer()
    }

    else if (billingInstruction typeis Reinstatement
            and noPaymentPlanModifiers(billingInstruction)) {
      if (canUseMatchCancellationSnapshotsChargeSlicer(chargeInitializer)) {
        return new MatchCancellationSnapshotsChargeSlicer()
      } else if (!BCConfigParameters.CollapseFutureInvoicesUponCancellation.Value
                  and hasMatchablePlannedInstallmentsForReinstatement(chargeInitializer)) {
        return new ReinstatementMatchPlannedInstallmentsChargeSlicer()
      }
    }

    return billingInstruction.PaymentPlan
  }

  private function canUseMatchCancellationSnapshotsChargeSlicer(chargeInitializer : ChargeInitializer): boolean {
    return hasCancellationSnapshots(chargeInitializer)
        and noOtherChargeAlsoHasSameChargeGroupAndSameChargePattern(chargeInitializer)
  }

  private function hasCancellationSnapshots(chargeInitializer: ChargeInitializer) : boolean {
    var query = Queries.createQuery(InvoiceItemCancellationSnapshot)
    query.compare(InvoiceItemCancellationSnapshot#PolicyPeriod, Relop.Equals, chargeInitializer.PolicyPeriod)
    query.compare(InvoiceItemCancellationSnapshot#ChargeGroup, Relop.Equals, chargeInitializer.ChargeGroup)
    query.compare(InvoiceItemCancellationSnapshot#ChargePattern, Relop.Equals, chargeInitializer.ChargePattern)
    return !query.select().Empty
  }

  private function noOtherChargeAlsoHasSameChargeGroupAndSameChargePattern(chargeInitializer: ChargeInitializer) : boolean {
    return chargeInitializer.BillingInstruction.ChargeInitializers
        .countWhere(\ ci -> ci.ChargePattern == chargeInitializer.ChargePattern && ci.ChargeGroup == chargeInitializer.ChargeGroup) == 1
  }

  /**
   * Returns true if the total amount of the matching InvoiceItems is not zero
   * and there is at least one installment on the reference Charge(s) of the PolicyPeriod to match.
   */
  private function hasMatchablePlannedInstallments(chargeInitializer : ChargeInitializer) : boolean {
    var referenceCharges = referenceCharges(chargeInitializer)
    var matchingInvoiceItems = matchingInvoiceItems(referenceCharges, chargeInitializer)
    return hasMatchableInvoiceItems(matchingInvoiceItems, chargeInitializer.Amount.Currency)
  }

  /**
   * Returns true if the total amount of the matching InvoiceItems (excluding cancellation items) is not zero
   * and there is at least one installment on the reference Charge(s) of the PolicyPeriod to match.
   */
  private function hasMatchablePlannedInstallmentsForReinstatement(chargeInitializer : ChargeInitializer) : boolean {
    var referenceCharges = referenceChargesForReinstatement(chargeInitializer)
    var matchingInvoiceItems = matchingInvoiceItemsForReinstatement(referenceCharges, chargeInitializer)
    return hasMatchableInvoiceItems(matchingInvoiceItems, chargeInitializer.Amount.Currency)
  }

  private function hasMatchableInvoiceItems(invoiceItems: InvoiceItem[], currency : Currency): boolean {
    if (invoiceItems*.Amount.sum(currency).IsZero) {
      return false
    }
    return invoiceItems
        .where(\invoiceItem -> invoiceItem.Amount.IsPositive and
                               totalOfInvoiceItemsOnInvoice(invoiceItem.Invoice, invoiceItems, currency).IsPositive)
        .HasElements
  }

  private function totalOfInvoiceItemsOnInvoice(invoice: Invoice, invoiceItems: InvoiceItem[], currency: Currency) : MonetaryAmount {
    var itemsOnInvoice = invoiceItems.where(\item -> item.Invoice.equals(invoice))
    return itemsOnInvoice*.Amount.sum(currency)
  }

  /**
   * Returns the Charges of the ChargeInitializer's PolicyPeriod which match the ChargeInitializer's ChargePattern.
   */
  private function referenceCharges(chargeInitializer: ChargeInitializer): ImmutableList<Charge> {
    return MatchPlannedInstallmentsMethods.referenceCharges(chargeInitializer)
  }

  /**
   * Returns the Charges of the ChargeInitializer's PolicyPeriod which match the ChargeInitializer's ChargePattern excluding Cancellation charges.
   */
  private function referenceChargesForReinstatement(chargeInitializer: ChargeInitializer): ImmutableList<Charge> {
    return MatchPlannedInstallmentsMethods.referenceChargesForReinstatement(chargeInitializer)
  }

  /**
   * Returns the planned InvoiceItems of the reference Charge(s) of a ChargeInitializer where the EventDate is on or
   * after the ChargeInitializer's EffectiveDate
   */
  private function matchingInvoiceItems(referenceCharges : ImmutableList<Charge>, chargeInitializer : ChargeInitializer) : InvoiceItem[] {
    return MatchPlannedInstallmentsMethods.matchingInvoiceItems(referenceCharges, chargeInitializer)
  }

  /**
   * Returns the planned InvoiceItems of the reference Charge(s) of a ChargeInitializer where the EventDate is on or
   * after the ChargeInitializer's EffectiveDate
   */
  private function matchingInvoiceItemsForReinstatement(referenceCharges : ImmutableList<Charge>, chargeInitializer : ChargeInitializer) : InvoiceItem[] {
    return MatchPlannedInstallmentsMethods.matchingInvoiceItemsForReinstatement(referenceCharges, chargeInitializer)
  }

  /**
   * Returns true if there are no PaymentPlan modifiers for the BillingInstruction's PaymentPlan
   */
  private function noPaymentPlanModifiers(billingInstruction : ExistingPlcyPeriodBI) : boolean {
    return billingInstruction.PaymentPlanModifiers.IsEmpty
  }

}