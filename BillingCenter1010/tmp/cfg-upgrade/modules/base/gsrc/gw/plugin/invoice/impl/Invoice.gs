package gw.plugin.invoice.impl;

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.domain.accounting.ChargeReturnObject
uses gw.api.domain.delinquency.DelinquencyTarget
uses gw.api.domain.invoice.FeesThresholdsManager
uses gw.api.util.TypeUtil
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.invoice.IInvoice

@Export
class Invoice implements IInvoice {

  construct() {
  }

  override function getPaymentReversalFeeChargeInfo(directBillMoneyRcvd : DirectBillMoneyRcvd) : ChargeReturnObject {
    var chargeData : ChargeReturnObject

    if (directBillMoneyRcvd.PolicyPeriod == null) {
      chargeData = new ChargeReturnObject(ChargePatternKey.PAYMENTREVERSALFEE.get())
    } else {
      var overridingPayer = (directBillMoneyRcvd.PolicyPeriod.DefaultPayer typeis Account)
          ? directBillMoneyRcvd.PolicyPeriod.DefaultPayer
          : directBillMoneyRcvd.Account
      chargeData = new ChargeReturnObject(ChargePatternKey.POLICYPAYMENTREVERSALFEE.get(),
          directBillMoneyRcvd.PolicyPeriod,
          overridingPayer)
    }
    return chargeData
  }

  public override function getLateFeeChargeInfo(delinquencyTarget : DelinquencyTarget) : ChargeReturnObject {
    var isPolicyPeriod = TypeUtil.isNominallyOrStructurallyAssignable(typeof delinquencyTarget, PolicyPeriod)
    return new ChargeReturnObject((isPolicyPeriod) ? ChargePatternKey.POLICYLATEFEE.get() : ChargePatternKey.ACCOUNTLATEFEE.get(),
        null,
        null)
  }

  override function getLateFeeAmount(delinquencyTarget : DelinquencyTarget) : MonetaryAmount {
    return delinquencyTarget.DelinquencyPlan.getLateFee(delinquencyTarget.Currency)
  }

  public override function getReinstatementFeeAmount(plan : DelinquencyPlan, target : DelinquencyTarget) : MonetaryAmount {
    return plan.getReinstatementFee(target.Currency)
  }

  public override function getInstallmentFeeAmount(policy : PolicyPeriod, invoice : Invoice) : MonetaryAmount {
    return policy.PaymentPlan.getInstallmentFee(policy.Currency)
  }

  /**
   * This method is called during the Invoice batch process, and is used to determine if an invoice item should be held.
   * <p>
   * The default implementation returns true if
   * (a) the invoice item is positive and isn't fully paid, or if the invoice item is negative to start with.
   * and one of the following conditions is met:
   * (b1) the invoice item's charge is held
   * (b2) the invoice item's account is subject to an invoice sending hold
   * (b3) the invoice item's policy is subject to an invoice sending hold
   * (b4) the invoice item is held because of delinquency
   * <p>
   * See CC-32138 and CC-33027 for some background.
   */
  public override function shouldHoldDirectBillInvoiceItem(invoiceItem : InvoiceItem) : Boolean {
    var invoiceDirectBill = invoiceItem.Invoice typeis AccountInvoice;
    var unpaid = invoiceItem.PaidAmount.IsZero;
    var chargeHeld = invoiceItem.Charge.isHeld();
    var accountTargetOfInvoiceSendingHold = invoiceDirectBill and (invoiceItem.Invoice as AccountInvoice).Account.isTargetOfHoldType(TC_INVOICESENDING);
    var policyPeriodTargetOfInvoiceSendingHold = invoiceItem.PolicyPeriod != null and invoiceItem.PolicyPeriod.isTargetOfHoldType(TC_INVOICESENDING);
    var policyTargetOfInvoiceSendingHold = invoiceItem.PolicyPeriod != null and invoiceItem.PolicyPeriod.Policy.isTargetOfHoldType(TC_INVOICESENDING);
    var invoiceItemHeldBecauseOfDelinquency = invoiceItem.isHeldBecauseOfDelinquency();

    return unpaid and
        (chargeHeld or accountTargetOfInvoiceSendingHold
            or policyPeriodTargetOfInvoiceSendingHold
            or policyTargetOfInvoiceSendingHold
            or invoiceItemHeldBecauseOfDelinquency);
  }

  /**
   * This method is called during the Invoice batch process, and is used to determine if an invoice should be carried
   * forward.
   * The default implementation returns true if the following conditions are met:
   * (a) the invoice's account's billing plan's SuppressLowBalInvoices flag is set to true
   * (b) the invoice's amount due is greater than 0
   * (c) either the invoice's amount or its amount due is less than the billing plan's low balance threshold
   * (d) the billing plan's low balance method is "carry forward"
   * <p>
   * See CC-33838 for some background.
   */
  public override function shouldCarryForwardInvoice(invoice : AccountInvoice) : Boolean {
    var billingPlan = invoice.Account.BillingPlan
    if (invoice.AmountDue.IsPositive && isBillingPlanSetupToCarryForwardInvoice(billingPlan)) {
      var lowBalanceThreshold = FeesThresholdsManager.getLowBalanceThreshold(invoice)
      return invoice.Amount < lowBalanceThreshold || invoice.AmountDue < lowBalanceThreshold
    }
    return false
  }

  /**
   * This method is called during the Invoice batch process, and is used to determine if an invoice should be written off
   * The default implementation returns true if the following conditions are met:
   * (a) the invoice's account's billing plan's SuppressLowBalInvoices flag is set to true
   * (b) the invoice's amount due is greater than 0
   * (c) the invoice's amount due is less than the billing plan's low balance threshold
   * (d) the billing plan's low balance method is "write off"
   * <p>
   */
  public override function shouldWriteOffInvoice(invoice : AccountInvoice) : Boolean {
    var billingPlan = invoice.Account.BillingPlan
    if (invoice.AmountDue.IsPositive && isBillingPlanSetupToWriteOffInvoice(billingPlan)) {
      var lowBalanceThreshold = FeesThresholdsManager.getLowBalanceThreshold(invoice)
      return invoice.AmountDue < lowBalanceThreshold
    }
    return false
  }

  /**
   * Populates the Invoice.OutstandingAmount field at the point that the invoice is billed.  This field is immutable,
   * and is only populated via this plugin method.  This method is called after
   * Invoice.payChargesOnAssociatedTAccountOwners() is called, so all balance data takes into account charges
   * paid right before the invoice is sent.
   * <p>
   * The default implementation returns invoice.Account.OutstandingAmount
   */
  public override function getOutstandingAmount(invoice : AccountInvoice) : MonetaryAmount {
    return invoice.Account.OutstandingUnsettledAmount
  }

  /**
   * Populates the Invoice.RemainingBalance field at the point that the invoice is billed.  This field is immutable,
   * and is only populated via this plugin method.  This method is called after
   * Invoice.payChargesOnAssociatedTAccountOwners() is called, so all balance data takes into account charges
   * paid right before the invoice is sent.
   */
  public override function getRemainingBalance(invoice : AccountInvoice) : MonetaryAmount {
    return invoice.Account.RemainingUnsettledBalance
  }

  /**
   * Populates the Invoice.UnappliedAmount field at the point that the invoice is billed.  This field is immutable,
   * and is only populated via this plugin method.  This method is called after
   * Invoice.payChargesOnAssociatedTAccountOwners() is called, so all balance data takes into account charges
   * paid right before the invoice is sent.
   */
  public override function getUnappliedAmount(invoice : AccountInvoice) : MonetaryAmount {
    return invoice.Account.UnappliedAmount
  }

  /**
   * Populates the Invoice.ColOutstandingAmount field at the point that the invoice is billed.  This field is immutable,
   * and is only populated via this plugin method.  This method is called after
   * Invoice.payChargesOnAssociatedTAccountOwners() is called, so all balance data takes into account charges
   * paid right before the invoice is sent.
   */
  public override function getCollateralOutstandingAmount(invoice : AccountInvoice) : MonetaryAmount {
    return invoice.Account.Collateral.OutstandingUnsettledAmount
  }

  /**
   * Populates the Invoice.ColRemainingBalance field at the point that the invoice is billed.  This field is immutable,
   * and is only populated via this plugin method.  This method is called after
   * Invoice.payChargesOnAssociatedTAccountOwners() is called, so all balance data takes into account charges
   * paid right before the invoice is sent.
   */
  public override function getCollateralRemainingBalance(invoice : AccountInvoice) : MonetaryAmount {
    return invoice.Account.Collateral.RemainingUnsettledBalance
  }

  /**
   * Populates the Invoice.ColUnappliedAmount field at the point that the invoice is billed.  This field is immutable,
   * and is only populated via this plugin method.  This method is called after
   * Invoice.payChargesOnAssociatedTAccountOwners() is called, so all balance data takes into account charges
   * paid right before the invoice is sent.
   */
  public override function getCollateralUnappliedAmount(invoice : AccountInvoice) : MonetaryAmount {
    return invoice.Account.Collateral.UnappliedAmount
  }

  /**
   * Populates the invoice's policies' snapshot fields at the point that the invoice is billed.  This fields are immutable,
   * and is only populated via this plugin method.  This method is called after
   * Invoice.payChargesOnAssociatedTAccountOwners() is called, so all balance data takes into account charges
   * paid right before the invoice is sent.
   */
  override property set PolicyPeriodInvoiceSnapshot(snapshot : AccountInvoicePolicyPeriodSnapshot) {
    snapshot.OutstandingAmount = snapshot.PolicyPeriod.OutstandingAmount
    snapshot.RemainingBalance = snapshot.PolicyPeriod.RemainingBalance
  }

  override function shouldCreatePaymentRequest(invoice : AccountInvoice) : boolean {
    var paymentMethodForInvoice = invoice.InvoiceStream.PaymentInstrument.PaymentMethod
    var isPaymentInstrumentBillable = paymentMethodForInvoice == PaymentMethod.TC_CREDITCARD
        || paymentMethodForInvoice == PaymentMethod.TC_ACH
        || paymentMethodForInvoice == PaymentMethod.TC_WIRE
    return isPaymentInstrumentBillable and invoice.getAmount().IsPositive
  }

  /**
   * Plugin call to determine whether or not we should push the invoice items being paid through to "Due"
   * before paying it.  Invoice Items returned from this plugin (if they are not already due) will be removed
   * from the Invoice they are currently on, and place on a new ad hoc Invoice (Bill and Due date today), and then
   * immediately made billed and due.
   * NOTE: we will *not* move any Agency Bill invoice items.
   *
   * @param paymentItems The payment items about to be executed
   * @return A list of all Invoice Items that we should make billed and due before completing the execution of this
   * payment
   */
  override function shouldMakeInvoiceItemsDueBeforePaying(paymentItems : Iterable<BaseDistItem>) : List<InvoiceItem> {
    return {}
  }

  override function getPaidStatus(invoice : Invoice) : InvoicePaidStatus {
    var invoiceItems = invoice.InvoiceItems

    if (invoiceItems.length == 0) {
      return InvoicePaidStatus.TC_FULLYPAID
    }

    var isFullyPaid = true
    for (var invoiceItem in invoiceItems) {
      if (!invoiceItem.GrossUnsettledAmount.IsZero) {
        isFullyPaid = false
        break
      }
    }
    if (isFullyPaid) {
      return InvoicePaidStatus.TC_FULLYPAID
    } else {
      var query = Query.make<InvoiceItem>(InvoiceItem)
      query.join(InvoiceItem#Invoice)
          .compare("ID", Relop.Equals, invoice)
      query.join(BaseDistItem, "InvoiceItem")
          .compare("ExecutedDate", Relop.NotEquals, null)
          .compare("ReversedDate", Relop.Equals, null)
      var hasPaymentApplied = !query.select().Empty

      return hasPaymentApplied ? InvoicePaidStatus.TC_PARTIALLYPAID : InvoicePaidStatus.TC_UNPAID
    }
  }

  private function isBillingPlanSetupToCarryForwardInvoice(billingPlan : BillingPlan) : Boolean {
    return billingPlan.SuppressLowBalInvoices && billingPlan.LowBalanceMethod == LowBalanceMethod.TC_CARRYFORWARD
  }

  private function isBillingPlanSetupToWriteOffInvoice(billingPlan : BillingPlan) : Boolean {
    return billingPlan.SuppressLowBalInvoices && billingPlan.LowBalanceMethod == LowBalanceMethod.TC_WRITEOFF
  }
}
