package gw.contactmapper.ab900

uses gw.api.system.BCLoggerCategory

/**
 * Returns the ContactIntegrationMapper to be used by BillingCenter
 * for integration.  It's @Export so customers can make the get() method return
 * a different ContactIntegrationMapper.
 */
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
class ContactIntegrationMapperFactory {
  private static var _logger = BCLoggerCategory.CONTACT_MANAGER_INTEGRATION

  public static function get() : ContactIntegrationMapper {

    var mapper = new ContactMapper()
    _logger.trace("ContactIntegrationMapperFactory.get() returned a " + mapper.IntrinsicType.Name)

    return mapper
  }
}
