package gw.admin.paymentplan

uses java.lang.Integer
uses java.util.ArrayList
uses java.math.BigDecimal
uses java.util.List

/**
 * Subclass of InstallmentViewHelper that wraps a PaymentPlan
 *
 */
@Export
class PaymentPlanViewHelper extends InstallmentViewHelper {
  private var _paymentPlan: PaymentPlan as PaymentPlan
  private var _isClone : boolean
  private var _isNew : boolean // new but not a clone...
  //

  /* constructor and initialization code ******************************************************************************/

  construct(paymentPlan : PaymentPlan, isClone : boolean = false) {
    super()
    _paymentPlan = paymentPlan
    _isClone = isClone
    // new instance, but not a clone...
    _isNew = (_paymentPlan.PublicID == null) && !_isClone
    //initialize values for NewPaymentPlanWizard (brand new PaymentPlan)
    if (_isNew) {
      FrequencyOfDownPayment = EVERY_TERM
      _paymentPlan.HasDownPayment = true
      SecondInstallmentChoice = NO
      initPaymentScheduledAfterFields()
      clearSecondInstallmentFields()
    }
        //initialize values for PaymentPlanDetailScreen (already existing PaymentPlan or PaymentPlan being cloned)
    else {
      FrequencyOfDownPayment = _paymentPlan.HasDownPayment ? YES : NO
      SecondInstallmentChoice = _paymentPlan.HasSecondInstallment ? YES : NO
    }

    initWhenFields()
  }

  /* overridden getters and setters (public) *************************************************************************/

  //// Down Payment and Maximum Number of Installment fields

  override property set HasDownPayment(value: Boolean) {
    _paymentPlan.HasDownPayment = value
  }

  override property get HasDownPayment(): Boolean {
    return _paymentPlan.HasDownPayment
  }

  override property set DownPaymentPercent(value: BigDecimal) {
    _paymentPlan.DownPaymentPercent = value
  }

  override property get DownPaymentPercent(): BigDecimal {
    return _paymentPlan.DownPaymentPercent
  }

  override property set MaximumNumberOfInstallments(value: Integer) {
    _paymentPlan.MaximumNumberOfInstallments = value
  }

  override property get MaximumNumberOfInstallments(): Integer {
    return _paymentPlan.MaximumNumberOfInstallments
  }

  override final property get SubViewHelper() : InstallmentViewHelper {
    if (super.SubViewHelper == null) {
      super.SubViewHelper = new SubsequentTermsViewHelper()
    }
    return super.SubViewHelper
  }

  override property get RequiredFieldCalculator() : gw.admin.paymentplan.RequiredFieldCalculator {
    var downPaymentMode = DownPaymentMode
    var screenLocation = (downPaymentMode == "FirstTermOnly")
      ? gw.admin.paymentplan.RequiredFieldCalculator.ScreenLocation.FIRST_TERM
      : gw.admin.paymentplan.RequiredFieldCalculator.ScreenLocation.PAYMENT_PLAN_DETAIL
    return new RequiredFieldCalculator(downPaymentMode, screenLocation, this)
  }

  //// PaymentScheduledAfter fields

  override property set DownPaymentAfter(value: PaymentScheduledAfter) {
    _paymentPlan.DownPaymentAfter = value
  }

  override property get DownPaymentAfter(): PaymentScheduledAfter {
    return _paymentPlan.DownPaymentAfter
  }

  override property set FirstInstallmentAfter(value: PaymentScheduledAfter) {
    _paymentPlan.FirstInstallmentAfter = value
  }

  override property get FirstInstallmentAfter(): PaymentScheduledAfter {
    return _paymentPlan.FirstInstallmentAfter
  }

  override property set SecondInstallmentAfter(value: PaymentScheduledAfter) {
    _paymentPlan.SecondInstallmentAfter = value
  }

  override property get SecondInstallmentAfter(): PaymentScheduledAfter {
    return _paymentPlan.SecondInstallmentAfter
  }

  override  property set OneTimeChargeAfter(value: PaymentScheduledAfter) {
    _paymentPlan.OneTimeChargeAfter = value
  }

  override  property get OneTimeChargeAfter(): PaymentScheduledAfter {
    return _paymentPlan.OneTimeChargeAfter
  }

  //// Align InvoiceItem to Installments fields

  override property set AlignInstallmentsToInvoices(value: Boolean) {
    _paymentPlan.AlignInstallmentsToInvoices = value
  }

  override property get AlignInstallmentsToInvoices(): Boolean {
    return _paymentPlan.AlignInstallmentsToInvoices
  }

  //// Off Sequence Installment fields

  override property set HasSecondInstallment(value: Boolean) {
    _paymentPlan.HasSecondInstallment = value;
  }

  override property get HasSecondInstallment(): Boolean {
    return _paymentPlan.HasSecondInstallment;
  }

  /* overridden getters and setters (protected) *************************************************************************/

  //// callbacks for DaysFromReferenceDateToXXX fields

  override protected property get InternalDaysFromReferenceDateToDownPayment(): Integer {
    return _paymentPlan.DaysFromReferenceDateToDownPayment
  }

  override protected property set InternalDaysFromReferenceDateToDownPayment(value: Integer) {
    _paymentPlan.DaysFromReferenceDateToDownPayment = value
  }

  override protected property get InternalDaysFromReferenceDateToFirstInstallment(): Integer {
    return _paymentPlan.DaysFromReferenceDateToFirstInstallment
  }

  override protected property set InternalDaysFromReferenceDateToFirstInstallment(value: Integer) {
    _paymentPlan.DaysFromReferenceDateToFirstInstallment = value == null ? 0 : value
  }

  override protected property get InternalDaysFromReferenceDateToSecondInstallment(): Integer {
    return _paymentPlan.DaysFromReferenceDateToSecondInstallment
  }

  override protected property set InternalDaysFromReferenceDateToSecondInstallment(value: Integer) {
    _paymentPlan.DaysFromReferenceDateToSecondInstallment = value
  }

  override protected property get InternalDaysFromReferenceDateToOneTimeCharge(): Integer {
    return _paymentPlan.DaysFromReferenceDateToOneTimeCharge
  }

  override protected property set InternalDaysFromReferenceDateToOneTimeCharge(value: Integer) {
    _paymentPlan.DaysFromReferenceDateToOneTimeCharge = value == null ? 0 : value
  }

  /* overridden functions (public) **************************************************************************************/

  /**
   * If the user selects "First Term Only," add overrides for Renewal and NewRenewal (the overrides will not have a down payment).
   * If the user selects a different down payment frequency option, remove any overrides for Renewal and NewRenewal.
   */
  override function onDownPaymentFrequencyChange() {
    switch (FrequencyOfDownPayment) {
      case YES:
      case EVERY_TERM:
          _paymentPlan.HasDownPayment = true
          initDownPaymentFields()
          clearSecondInstallmentFields()

          if (_isNew) {
            _paymentPlan.removeOverridesFor(TC_RENEWAL)
            _paymentPlan.removeOverridesFor(TC_NEWRENEWAL)
            (SubViewHelper as SubsequentTermsViewHelper).OverridesList = new ArrayList<ChargeSlicingOverrides>()
          }

          break
      case DownPaymentFrequency.FIRST_TERM_ONLY:
          _paymentPlan.HasDownPayment = true
          initDownPaymentFields()
          clearSecondInstallmentFields()

          // Note: _isOnWizard check may not needed here because FIRST_TERM_ONLY is applicable only for wizard
          if (_isNew) {
            var renewalOverride = new ChargeSlicingOverrides()
            var newRenewalOverride = new ChargeSlicingOverrides()
            _paymentPlan.setOverridesFor(TC_RENEWAL, renewalOverride)
            _paymentPlan.setOverridesFor(TC_NEWRENEWAL, newRenewalOverride)
            (SubViewHelper as SubsequentTermsViewHelper).OverridesList = ChargeSlicingOverridesFromPaymentPlan

            SubViewHelper.HasSecondInstallment = false
            SubViewHelper.SecondInstallmentChoice = NO
          }
          break
      case NO:
      case NONE:
          _paymentPlan.HasDownPayment = false
          clearDownPaymentFields()
          _paymentPlan.HasSecondInstallment = false
          SecondInstallmentChoice = NO

          if (_isNew) {
            _paymentPlan.removeOverridesFor(TC_RENEWAL)
            _paymentPlan.removeOverridesFor(TC_NEWRENEWAL)
            (SubViewHelper as SubsequentTermsViewHelper).OverridesList = new ArrayList<ChargeSlicingOverrides>()
          }
          break
    }
  }

  override function validateDownPayment(): String {
    return _paymentPlan.validateDownPayment()
  }

  override function validateSecondInstallmentFields(): String {
    return _paymentPlan.validateSecondInstallmentFields()
  }

  override final property get DownPaymentMode() : String {
    var modeName = ""
    switch (FrequencyOfDownPayment) {
      case YES:
      case EVERY_TERM:
          modeName = "EveryTerm"
          break
      case FIRST_TERM_ONLY:
          modeName = "FirstTermOnly"
          break
      case NO:
      case NONE:
          modeName = "None"
          break
    }
    return modeName
  }

  override property get BasePaymentPlanHasDownPayment() : Boolean {
    return false
  }

  override property get BasePaymentPlanHasSecondInstallment() : Boolean {
    return false
  }

  /* private helper functions *****************************************************************************************/

  final private property get ChargeSlicingOverridesFromPaymentPlan() : List < ChargeSlicingOverrides > {
    var chargeSlicingOverrides = new ArrayList < ChargeSlicingOverrides > ()
    var billingInstructions = _paymentPlan.getTypesThatCanHaveOverrides()
        .where(\billingInstructionType -> _paymentPlan.getOverridesFor(billingInstructionType) != null)

    for (bi in billingInstructions) {
      chargeSlicingOverrides.add(_paymentPlan.getOverridesFor(bi))
    }

    return chargeSlicingOverrides
  }

}