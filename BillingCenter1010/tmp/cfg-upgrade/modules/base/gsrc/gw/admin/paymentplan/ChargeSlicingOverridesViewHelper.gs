package gw.admin.paymentplan

uses gw.api.locale.DisplayKey
uses typekey.BillingInstruction

uses java.math.BigDecimal

/**
 * Subclass of InstallmentViewHelper that wraps a ChargeSlicingOverrides entity
 *
 */
@Export
class ChargeSlicingOverridesViewHelper extends InstallmentViewHelper {
  private var _overrides : ChargeSlicingOverrides as readonly ChargeSlicingOverrides
  private var _billingInstruction : BillingInstruction as readonly BillingInstruction
  private var _paymentPlan : PaymentPlan
  //

  /* constructor and initialization code ******************************************************************************/

  construct(billingInstruction : BillingInstruction, overrides : ChargeSlicingOverrides, paymentPlan : PaymentPlan, isClone : boolean = false) {
    super()
    _billingInstruction = billingInstruction
    _overrides = overrides
    _paymentPlan = paymentPlan

    if (_overrides.PublicID == null && !isClone) {//brand new ChargeSlicingOverrides
      HasDownPayment = null  //for ChargeSlicingOverrides this value is ternary: null, false, or true
      FrequencyOfDownPayment = NOT_OVERRIDDEN
      SecondInstallmentChoice = NOT_OVERRIDDEN
    } else {// already existing ChargeSlicingOverrides

      switch (_overrides.DownPaymentSecondInstallment) {
        case null:
          FrequencyOfDownPayment = NOT_OVERRIDDEN
          SecondInstallmentChoice = NOT_OVERRIDDEN
          break

        case TC_DOWNPAYMENT:
          FrequencyOfDownPayment = YES
          SecondInstallmentChoice = NOT_OVERRIDDEN
          break

        case TC_SECONDINSTALLMENT:
          FrequencyOfDownPayment = _paymentPlan.HasDownPayment ? NO : NOT_OVERRIDDEN
          SecondInstallmentChoice = YES
          break

        case TC_NONE:
          if (_paymentPlan.HasDownPayment) {
            FrequencyOfDownPayment = NO
            SecondInstallmentChoice = NO
          } else {
            FrequencyOfDownPayment = NOT_OVERRIDDEN
            SecondInstallmentChoice = NO
          }

          break
      }
    }

    initWhenFields()
  }

  /* overridden getters and setters (public) *************************************************************************/

  //// Down Payment and Maximum Number of Installment fields

  override property set HasDownPayment(value: Boolean) {
    _overrides.HasDownPayment = value
  }

  override property get HasDownPayment(): Boolean {
    return _overrides.HasDownPayment
  }

  override property set DownPaymentPercent(value: BigDecimal) {
    _overrides.DownPaymentPercent = value
  }

  override property get DownPaymentPercent(): BigDecimal {
    return _overrides.DownPaymentPercent
  }

  override property set MaximumNumberOfInstallments(value: Integer) {
    _overrides.MaximumNumberOfInstallments = value
  }

  override property get MaximumNumberOfInstallments(): Integer {
    return _overrides.MaximumNumberOfInstallments
  }

  override property get RequiredFieldCalculator() : gw.admin.paymentplan.RequiredFieldCalculator {
    return new RequiredFieldCalculator(DownPaymentMode, gw.admin.paymentplan.RequiredFieldCalculator.ScreenLocation.OVERRIDES_PANEL, this)
  }

  //// PaymentScheduledAfter fields

  override property set DownPaymentAfter(value: PaymentScheduledAfter) {
    _overrides.DownPaymentAfter = value
  }

  override property get DownPaymentAfter(): PaymentScheduledAfter {
    return _overrides.DownPaymentAfter
  }

  override property set FirstInstallmentAfter(value: PaymentScheduledAfter) {
    _overrides.FirstInstallmentAfter = value
  }

  override property get FirstInstallmentAfter(): PaymentScheduledAfter {
    return _overrides.FirstInstallmentAfter
  }

  override property set SecondInstallmentAfter(value: PaymentScheduledAfter) {
    _overrides.SecondInstallmentAfter = value
  }

  override property get SecondInstallmentAfter(): PaymentScheduledAfter {
    return _overrides.SecondInstallmentAfter
  }

  override property set OneTimeChargeAfter(value: PaymentScheduledAfter) {
    _overrides.OneTimeChargeAfter = value
  }

  override property get OneTimeChargeAfter(): PaymentScheduledAfter {
    return _overrides.OneTimeChargeAfter
  }

  //// Align InvoiceItem to Installments fields

  override property set AlignInstallmentsToInvoices(value: Boolean) {
    // no op
  }

  override property get AlignInstallmentsToInvoices() : Boolean {
    return null
  }

  //// Off Sequence Installment fields

  override property set HasSecondInstallment(value: Boolean) {
    _overrides.HasSecondInstallment = value
  }

  override property get HasSecondInstallment() : Boolean {
    return _overrides.HasSecondInstallment
  }

  /* overridden getters and setters (protected) ***********************************************************************/

  //// callbacks for DaysFromReferenceDateToXXX fields

  override protected property get InternalDaysFromReferenceDateToDownPayment(): Integer {
    return _overrides.DaysFromReferenceDateToDownPayment
  }

  override protected property set InternalDaysFromReferenceDateToDownPayment(value: Integer)
  {
    _overrides.DaysFromReferenceDateToDownPayment = value
  }

  override protected property get InternalDaysFromReferenceDateToFirstInstallment(): Integer {
    return _overrides.DaysFromReferenceDateToFirstInstallment
  }

  override protected property set InternalDaysFromReferenceDateToFirstInstallment(value: Integer) {
    _overrides.DaysFromReferenceDateToFirstInstallment = value
  }

  override protected property get InternalDaysFromReferenceDateToSecondInstallment(): Integer {
    return _overrides.DaysFromReferenceDateToSecondInstallment
  }

  override protected property set InternalDaysFromReferenceDateToSecondInstallment(value: Integer) {
    _overrides.DaysFromReferenceDateToSecondInstallment = value
  }

  override protected property get InternalDaysFromReferenceDateToOneTimeCharge(): Integer {
    return _overrides.DaysFromReferenceDateToOneTimeCharge
  }

  override protected property set InternalDaysFromReferenceDateToOneTimeCharge(value: Integer) {
    _overrides.DaysFromReferenceDateToOneTimeCharge = value
  }

  /* overridden functions (public) **************************************************************************************/

  override public function onDownPaymentFrequencyChange() {
    switch (FrequencyOfDownPayment) {
      case YES:
          _overrides.HasDownPayment = true
          if (not BasePaymentPlanHasDownPayment and _overrides.HasDownPayment) {
            initDownPaymentFields()
          }
          break
      case NO:
          _overrides.HasDownPayment = !_paymentPlan.HasDownPayment ? null : false
          clearDownPaymentFields()
          //_overrides.HasSecondInstallment = null
          SecondInstallmentChoice = _paymentPlan.HasDownPayment ? NO : NOT_OVERRIDDEN;
          clearSecondInstallmentFields()
          break
      case NOT_OVERRIDDEN:
          _overrides.HasDownPayment = null
          _overrides.HasSecondInstallment = null
          SecondInstallmentChoice = NOT_OVERRIDDEN;
          clearSecondInstallmentFields()
          break
    }
  }

  override public function onHasSecondInstallmentChange() {
    switch(SecondInstallmentChoice) {
      case YES:
          _overrides.HasSecondInstallment = true
          if (secondInstallmentFieldsMustBeOverridden()) {
            initSecondInstallmentFields()
          }
          break
      case NOT_OVERRIDDEN:
          _overrides.HasSecondInstallment = null
          if (not BasePaymentPlanHasSecondInstallment) {
            clearSecondInstallmentFields()
          }
          break
      case NO:
          _overrides.HasSecondInstallment = false
          clearSecondInstallmentFields()
          break
    }
  }

  override public function validateDownPayment(): String {
    return _overrides.ChargeSlicingModifier.validateDownPayment()
  }

  override public function validateSecondInstallmentFields(): String {
    return _overrides.ChargeSlicingModifier.validateSecondInstallmentFields()
  }

  override final property get DownPaymentMode() : String {
    var modeName = ""
    switch (FrequencyOfDownPayment) {
      case DownPaymentFrequency.NOT_OVERRIDDEN:
          if (_paymentPlan.HasDownPayment) {
            modeName = "EveryTerm"
          } else {
            modeName = "None"
          }
          break
      case DownPaymentFrequency.YES:
          modeName = "EveryTerm"
          break
      case DownPaymentFrequency.NO:
          modeName = "None"
          break
    }
    return modeName
  }

  override final property get NoneSelectedLabel() : String {
    return "<" + DisplayKey.get('Web.PaymentPlanDetailScreen.NotOverridden') + ">"
  }

  override property get BasePaymentPlanHasDownPayment() : Boolean {
    return _paymentPlan.HasDownPayment
  }

  override property get BasePaymentPlanHasSecondInstallment() : Boolean {
    return _paymentPlan.HasSecondInstallment
  }

  private function secondInstallmentFieldsMustBeOverridden() : Boolean {
    return not BasePaymentPlanHasSecondInstallment and _overrides.HasSecondInstallment
  }
}