package gw.admin.paymentplan

uses java.util.Map
uses gw.util.Pair

/**
 * A class for determining whether a field is required in InstallmentTreatmentInputSet.pcf.
 *
 * In plain language, these are the rules for determining whether a field is required:
 * A field is normally required for the base payment plan and normally not required for an override. However, an override
 * field is required when the base payment plan does not have the corresponding field. For example, if the base payment
 * plan has no down payment while the override has a down payment then the down payment fields are required for the
 * override.
 *
 * There are two ways in which the content of the pcf can vary:
 *  1. The InstallmentTreatmentInputSet.pcf has three different modes: EveryTerm, None, FirstTermOnly.
 *  2. The InstallmentTreatmentInputSet may appear representing a base payment plan or representing an override.
 *
 * Both the pcf mode and the way in which the pcf appears are contributing factors to determining whether a field
 * is required.
 *
 * The _requiredFieldMap below puts all the combinations in one place.
 */
@Export
class RequiredFieldCalculator {
  private enum DownPaymentModeAndScreenLocation {
    EVERY_TERM_IN_PAYMENTPLAN_DETAIL,
    EVERY_TERM_IN_OVERRIDES_PANEL,
    NONE_IN_PAYMENTPLAN_DETAIL,
    NONE_IN_OVERRIDES_PANEL,
    FIRST_TERM_ONLY_ON_FIRST_TERM,
    FIRST_TERM_ONLY_ON_SUBSEQUENT_TERMS
  }

  private enum InputField {
    MAX_NUMBER_OF_INSTALLMENTS,
    DOWN_PAYMENT_FIELDS,
    FIRST_INSTALLMENT_FIELDS,
    HAS_SECOND_INSTALLMENT,
    SECOND_INSTALLMENT_FIELDS,
    ONE_TIME_CHARGES_FIELDS }

  private enum RequiredStatus {
    YES,
    NO,
    MAYBE // Yes if not in the base payment plan
  }

  final static var _requiredFieldMap : Map<DownPaymentModeAndScreenLocation, Map<InputField, RequiredStatus>> =
  {
  EVERY_TERM_IN_PAYMENTPLAN_DETAIL -> {   MAX_NUMBER_OF_INSTALLMENTS -> YES,   DOWN_PAYMENT_FIELDS -> YES,     FIRST_INSTALLMENT_FIELDS -> YES,   HAS_SECOND_INSTALLMENT -> NO,     SECOND_INSTALLMENT_FIELDS -> NO,      ONE_TIME_CHARGES_FIELDS -> YES },
  EVERY_TERM_IN_OVERRIDES_PANEL -> {      MAX_NUMBER_OF_INSTALLMENTS -> NO,    DOWN_PAYMENT_FIELDS -> MAYBE,   FIRST_INSTALLMENT_FIELDS -> NO,    HAS_SECOND_INSTALLMENT -> NO,     SECOND_INSTALLMENT_FIELDS -> NO,      ONE_TIME_CHARGES_FIELDS -> NO  },

  NONE_IN_PAYMENTPLAN_DETAIL -> {         MAX_NUMBER_OF_INSTALLMENTS -> YES,   DOWN_PAYMENT_FIELDS -> NO,      FIRST_INSTALLMENT_FIELDS -> YES,   HAS_SECOND_INSTALLMENT -> YES,    SECOND_INSTALLMENT_FIELDS -> YES,     ONE_TIME_CHARGES_FIELDS -> YES },
  NONE_IN_OVERRIDES_PANEL -> {            MAX_NUMBER_OF_INSTALLMENTS -> NO,    DOWN_PAYMENT_FIELDS -> NO,      FIRST_INSTALLMENT_FIELDS -> NO,    HAS_SECOND_INSTALLMENT -> MAYBE,  SECOND_INSTALLMENT_FIELDS -> MAYBE,   ONE_TIME_CHARGES_FIELDS -> NO  },

  FIRST_TERM_ONLY_ON_FIRST_TERM -> {      MAX_NUMBER_OF_INSTALLMENTS -> YES,   DOWN_PAYMENT_FIELDS -> YES,     FIRST_INSTALLMENT_FIELDS -> YES,   HAS_SECOND_INSTALLMENT -> NO,     SECOND_INSTALLMENT_FIELDS -> NO,      ONE_TIME_CHARGES_FIELDS -> YES },
  FIRST_TERM_ONLY_ON_SUBSEQUENT_TERMS -> {MAX_NUMBER_OF_INSTALLMENTS -> NO,    DOWN_PAYMENT_FIELDS -> NO,      FIRST_INSTALLMENT_FIELDS -> NO,    HAS_SECOND_INSTALLMENT -> YES,    SECOND_INSTALLMENT_FIELDS -> MAYBE,   ONE_TIME_CHARGES_FIELDS -> NO  }
  }

  final static var _requiredFieldKeys = {
      Pair.make<DownPaymentMode, ScreenLocation>(EveryTerm, PAYMENT_PLAN_DETAIL) ->  DownPaymentModeAndScreenLocation.EVERY_TERM_IN_PAYMENTPLAN_DETAIL,
      Pair.make<DownPaymentMode, ScreenLocation>(EveryTerm, OVERRIDES_PANEL) ->      DownPaymentModeAndScreenLocation.EVERY_TERM_IN_OVERRIDES_PANEL,
      Pair.make<DownPaymentMode, ScreenLocation>(None, PAYMENT_PLAN_DETAIL) ->       DownPaymentModeAndScreenLocation.NONE_IN_PAYMENTPLAN_DETAIL,
      Pair.make<DownPaymentMode, ScreenLocation>(None, OVERRIDES_PANEL) ->           DownPaymentModeAndScreenLocation.NONE_IN_OVERRIDES_PANEL,
      Pair.make<DownPaymentMode, ScreenLocation>(FirstTermOnly, FIRST_TERM) ->       DownPaymentModeAndScreenLocation.FIRST_TERM_ONLY_ON_FIRST_TERM,
      Pair.make<DownPaymentMode, ScreenLocation>(FirstTermOnly, SUBSEQUENT_TERMS) -> DownPaymentModeAndScreenLocation.FIRST_TERM_ONLY_ON_SUBSEQUENT_TERMS
  }

  var _helper : InstallmentViewHelper
  var _isRequiredMap : Map<InputField, RequiredStatus>

  public enum DownPaymentMode {EveryTerm, None, FirstTermOnly
    private construct() {}
    static function parse(value : String) : DownPaymentMode {
      if (value == "EveryTerm") {
        return EveryTerm
      } else if (value == "FirstTermOnly") {
        return FirstTermOnly
      } else {
        return None
      }
    }
  }

  public enum ScreenLocation {PAYMENT_PLAN_DETAIL, OVERRIDES_PANEL, FIRST_TERM, SUBSEQUENT_TERMS}

  construct(downPaymentMode : String, screenLocation : ScreenLocation, helper : InstallmentViewHelper) {
    _helper = helper
    initModeAndLocation(downPaymentMode, screenLocation)
  }

  final private function initModeAndLocation(downPaymentMode : String, screenLocation : ScreenLocation) {
    var mode = DownPaymentMode.parse(downPaymentMode)
    var modeAndLocationKey = _requiredFieldKeys.get(Pair.make(mode, screenLocation))
    _isRequiredMap = _requiredFieldMap.get(modeAndLocationKey)
  }

  // Properties for the UI

  property get MaximumNumberOfInstallmentsRequired() : Boolean {
    return _isRequiredMap.get(InputField.MAX_NUMBER_OF_INSTALLMENTS) == YES
  }

  property get DownPaymentFieldsRequired() : Boolean {
    return _isRequiredMap.get(InputField.DOWN_PAYMENT_FIELDS) == YES
        or
        (_isRequiredMap.get(InputField.DOWN_PAYMENT_FIELDS) == MAYBE and not _helper.BasePaymentPlanHasDownPayment)
  }

  property get FirstInstallmentFieldsRequired() : Boolean {
    return _isRequiredMap.get(InputField.FIRST_INSTALLMENT_FIELDS) == YES
  }

  property get SecondInstallmentFieldsRequired() : Boolean {
    return (_isRequiredMap.get(InputField.SECOND_INSTALLMENT_FIELDS) == YES and _helper.HasSecondInstallment)
        or
        (_isRequiredMap.get(InputField.SECOND_INSTALLMENT_FIELDS) == MAYBE and not _helper.BasePaymentPlanHasSecondInstallment and _helper.HasSecondInstallment)
  }

  property get HasSecondInstallmentRequired() : Boolean {
    return (_isRequiredMap.get(InputField.HAS_SECOND_INSTALLMENT) == YES)
        or
        (_isRequiredMap.get(InputField.HAS_SECOND_INSTALLMENT) == MAYBE and not _helper.BasePaymentPlanHasSecondInstallment)
  }

  property get OneTimeChargesFieldsRequired() : Boolean {
    return _isRequiredMap.get(InputField.ONE_TIME_CHARGES_FIELDS) == YES
  }

  // Display-related properties for the UI

  property get CanShowSecondInstallmentNotOverridden() : Boolean {

    // we don't want to show the "Not Overridden" option if the base payment plan has down payment and the override
    // has no down payment. This is to be consistent with the behavior when the user selects No DownPayment for the
    // base Payment Plan
    return _helper typeis ChargeSlicingOverridesViewHelper and !(_helper.BasePaymentPlanHasDownPayment &&
        _helper.FrequencyOfDownPayment == DownPaymentFrequency.NO)
  }

  property get CanShowSecondInstallmentFields() : Boolean {
    if (_helper.DownPaymentMode == "None" || _helper typeis SubsequentTermsViewHelper) {
      return _helper.HasSecondInstallment == Boolean.TRUE
    } else {
      return false
    }
  }

}