package gw.entity

uses gw.api.locale.DisplayKey

@Export
enhancement GWTAccountOwnerPatternEnhancement: TAccountOwnerPattern {

  /**
   * Utility for finding the correct display keys for a given TAccountOwnerPattern.
   */
  public static function getDisplayKeyFor(final s : String) : String {
    var displayKeyMap = new HashMap<String, String>()

    displayKeyMap.put("Account", DisplayKey.get("Java.TAccountOwnerType.Account"))
    displayKeyMap.put("ChargeCommission", DisplayKey.get("Java.TAccountOwnerType.ChargeCommission"))
    displayKeyMap.put("Collateral", DisplayKey.get("Java.TAccountOwnerType.Collateral"))
    displayKeyMap.put("CollateralRequirement", DisplayKey.get("Java.TAccountOwnerType.CollateralRequirement"))
    displayKeyMap.put("PolicyCommission", DisplayKey.get("Java.TAccountOwnerType.PolicyCommission"))
    displayKeyMap.put("PolicyPeriod", DisplayKey.get("Java.TAccountOwnerType.PolicyPeriod"))
    displayKeyMap.put("ProducerCode", DisplayKey.get("Java.TAccountOwnerType.ProducerCode"))
    displayKeyMap.put("SuspensePayment", DisplayKey.get("Java.TAccountOwnerType.SuspensePayment"))
    displayKeyMap.put("Producer", DisplayKey.get("Java.TAccountOwnerType.Producer"))

    return displayKeyMap.get(s)
  }
}
