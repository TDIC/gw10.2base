package gw.entity

@Export
enhancement GWUserEnhancement: entity.User {

  /**
   * Indicates whether or not a user can be editable.
   */
  function isEditable() : boolean {
    // A user can be editable if its Contact data has not been obfuscated or if the logged user has the
    // editobfuscatedusercontact permission
    return (not this.Contact.Obfuscated or perm.System.editobfuscatedusercontact)
  }

}
