package gw.agencybill

uses gw.api.database.Query
uses gw.pl.currency.MonetaryAmount
uses entity.AgencyBillCycle

@Export
enhancement AgencyBillCycleEnhancement: AgencyBillCycle {

  public property get PromisedAmount(): MonetaryAmount {
    var promiseItemsQuery = Query.make(AgencyPromiseItem)
        .compare(BaseDistItem#ExecutedDate, NotEquals, null)
        .compare(BaseDistItem#ReversedDate, Equals, null)
        .compare(AgencyPromiseItem#AppliedDate, Equals, null)
        .join(AgencyPromiseItem#InvoiceItem)
        .join(InvoiceItem#Invoice)
        .cast(StatementInvoice.Type)
        .compare(StatementInvoice#AgencyBillCycle, Equals, this)

    return promiseItemsQuery.select()
        .sum(this.Currency, \pi -> pi.GrossAmountToApply - pi.CommissionAmountToApply)
  }

  public property get UnpromisedAmount(): MonetaryAmount {
    return this.NetAmountDue - PromisedAmount
  }

}
