package gw.agencybill
uses gw.api.locale.DisplayKey
uses gw.api.filters.StandardBeanFilter

/**
 * Creates the filters used on the ProducerAgencyBillCycles page. Creating them in a separate class like this
 * ensures that the blocks do not inadvertantly "capture" page scoped variables - this was causing high memory
 * usage when the current filter values were stored in the session.
 * <p>
 * If new or altered filters are added at a customer site then create a new class, e.g. ProducerAgencyBillCycleFiltersExt,
 * and put the new/alrtered filters in that class.
 */
@Export
class ProducerAgencyBillCycleFilters {

  public static final var OPEN: StandardBeanFilter = new StandardBeanFilter(
     DisplayKey.get("Java.AgencyBillStatement.Status.Open"),
     \ x -> {
       var status = (x as AgencyBillCycle).StatementInvoice.DisplayStatus
       return status == DisplayKey.get("Java.AgencyBillStatement.Status.Open") || status == DisplayKey.get("Java.AgencyBillStatement.Status.PastDue")
     }
  )
  
  public static final var PLANNED: StandardBeanFilter = new StandardBeanFilter(
     DisplayKey.get("Java.AgencyBillStatement.Status.Planned"),
     \ x -> {
       var status = (x as AgencyBillCycle).StatementInvoice.DisplayStatus
       return status == DisplayKey.get("Java.AgencyBillStatement.Status.Planned")
     }
  )
  
  public static final var CLOSED: StandardBeanFilter = new StandardBeanFilter(
     DisplayKey.get("Java.AgencyBillStatement.Status.Closed"),
     \ x -> {
       var status = (x as AgencyBillCycle).StatementInvoice.DisplayStatus
       return status.contains(DisplayKey.get("Java.AgencyBillStatement.Status.Closed"))
     }
  )
  
  private construct() {}

}
