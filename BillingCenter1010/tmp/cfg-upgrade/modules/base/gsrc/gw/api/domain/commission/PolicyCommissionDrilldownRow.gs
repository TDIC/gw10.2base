package gw.api.domain.commission

uses gw.pl.currency.MonetaryAmount

uses java.util.ArrayList
uses java.util.List

@Export
public class PolicyCommissionDrilldownRow implements CommissionDrilldownRow {

  public var _policyCommission: PolicyCommission
  var _chargeCommissionDrilldownRows: List<ChargeCommissionDrilldownRow>

  var _reserve : MonetaryAmount
  var _earned : MonetaryAmount
  var _paid : MonetaryAmount
  var _writtenOff : MonetaryAmount
  var _total : MonetaryAmount

  var _commissionableGross : MonetaryAmount

  construct(policyCommission : PolicyCommission) {
    _policyCommission = policyCommission
    _chargeCommissionDrilldownRows = new ArrayList<ChargeCommissionDrilldownRow>()

    _reserve = 0bd.ofCurrency(policyCommission.Currency)
    _earned = 0bd.ofCurrency(policyCommission.Currency)
    _paid = 0bd.ofCurrency(policyCommission.Currency)
    _writtenOff = 0bd.ofCurrency(policyCommission.Currency)
    _total = 0bd.ofCurrency(policyCommission.Currency)
    _commissionableGross = 0bd.ofCurrency(policyCommission.Currency)
  }

  public function addCommissionAmounts(itemCommission : ItemCommission) {
    _reserve += itemCommission.CommissionReserve
    _earned += itemCommission.DirectBillEarned
    _paid += itemCommission.AgencyBillRetained
    _writtenOff += itemCommission.WrittenOffCommission
    _total += itemCommission.CommissionAmount
    _commissionableGross += itemCommission.InvoiceItem.Amount

    var row = getChargeCommissionRow(getChargeCommission(itemCommission))
    row.addCommissionAmounts(itemCommission)
  }

  override property get Name(): String {
    return "Producer Code " + _policyCommission.ProducerCode
  }

  /**
   * Get the total gross for all invoice items have any ItemCommission under this PolicyCommission.
  */
  override property get CommissionableGross(): String {
    return _commissionableGross.renderWithZeroDash()
  }

  override property get Rate(): String {
    return "-"
  }

  override property get Active(): Boolean {
    return _policyCommission.DefaultForPolicy
  }

  override property get Children(): List <CommissionDrilldownRow> {
    if (_chargeCommissionDrilldownRows == null) {
      _chargeCommissionDrilldownRows = _policyCommission.CommissionableCharges.sortBy(\cc -> cc.Charge.CreateTime).map(\cc -> new ChargeCommissionDrilldownRow(cc)).toList()
    }
    return _chargeCommissionDrilldownRows
  }

  override property get Reserve(): String {
    return _reserve.renderWithZeroDash()
  }

  override property get Earned(): String {
    return _earned.renderWithZeroDash()
  }

  override property get Paid(): String {
    return _paid.renderWithZeroDash()
  }

  override property get WrittenOff(): String {
    return _writtenOff.renderWithZeroDash()
  }

  override property get Total(): String {
    return _total.renderWithZeroDash()
  }

  override property get RowType(): String {
    return "ProducerCode"
  }


  private function getChargeCommissionRow(chargeCommission : ChargeCommission) : ChargeCommissionDrilldownRow {
    var row = _chargeCommissionDrilldownRows.firstWhere( \ row -> row._chargeCommission == chargeCommission)
    if (row == null) {
      row = new ChargeCommissionDrilldownRow(chargeCommission)
      _chargeCommissionDrilldownRows.add(row)
    }
    return row
  }

  private function getChargeCommission(itemCommission : ItemCommission) : ChargeCommission {
    return itemCommission.PolicyCommission.CommissionableCharges.singleWhere( \ cc -> cc.Charge == itemCommission.InvoiceItem.Charge)
  }

  override public function toString():String {
      return Name;
  }
}