package gw.api.domain.commission

uses gw.pl.currency.MonetaryAmount

uses java.util.ArrayList
uses java.util.List

@Export
public class ChargeCommissionDrilldownRow implements CommissionDrilldownRow {

  public var _chargeCommission: ChargeCommission
  var _itemCommissionDrilldownRows: List<ItemCommissionDrilldownRow>

  var _reserve : MonetaryAmount
  var _earned : MonetaryAmount
  var _paid : MonetaryAmount
  var _writtenOff : MonetaryAmount
  var _total : MonetaryAmount

  var _commissionableGross : MonetaryAmount

  construct(chargeCommission : ChargeCommission) {
    _chargeCommission = chargeCommission
    _itemCommissionDrilldownRows = new ArrayList<ItemCommissionDrilldownRow>()

    _reserve = 0bd.ofCurrency(chargeCommission.Currency)
    _earned = 0bd.ofCurrency(chargeCommission.Currency)
    _paid = 0bd.ofCurrency(chargeCommission.Currency)
    _writtenOff = 0bd.ofCurrency(chargeCommission.Currency)
    _total = 0bd.ofCurrency(chargeCommission.Currency)
    _commissionableGross = 0bd.ofCurrency(chargeCommission.Currency)
  }

  public function addCommissionAmounts(itemCommission : ItemCommission) {
    _reserve += itemCommission.CommissionReserve
    _earned += itemCommission.DirectBillEarned
    _paid += itemCommission.AgencyBillRetained
    _writtenOff += itemCommission.WrittenOffCommission
    _total += itemCommission.CommissionAmount
    _commissionableGross += itemCommission.InvoiceItem.Amount

    var row = new ItemCommissionDrilldownRow(itemCommission)
    _itemCommissionDrilldownRows.add(row)
    row.addCommissionAmounts(itemCommission)
  }


  override property get Name(): String {
    return _chargeCommission.Charge.ChargePattern + " - " + _chargeCommission.Charge.BillingInstruction
  }

  /**
   * Get the total gross for all invoice items have any ItemCommission under this ChargeCommission.
  */
  override property get CommissionableGross(): String {
    return _commissionableGross.renderWithZeroDash()
  }

  override property get Rate(): String {
    var rateAsPercentage = _chargeCommission.CommissionRate
    return rateAsPercentage == null ? "0.00%" : rateAsPercentage.toString()
  }

  override property get Active(): Boolean {
    return _chargeCommission.Charge.getDefaultChargeCommission(_chargeCommission.PolicyCommission.Role) == _chargeCommission
  }

  override property get Children(): List <CommissionDrilldownRow> {
    var sortBlock = \ii1: ItemCommissionDrilldownRow, ii2: ItemCommissionDrilldownRow -> {
      if (ii1._itemCommission.InvoiceItem.EventDate == ii2._itemCommission.InvoiceItem.EventDate) {
        return ii1._itemCommission.InvoiceItem.Amount > ii2._itemCommission.InvoiceItem.Amount
      } else {
        return ii1._itemCommission.InvoiceItem.EventDate < ii2._itemCommission.InvoiceItem.EventDate
      }
    }

    return _itemCommissionDrilldownRows.sort(sortBlock)
  }

  override property get Reserve(): String {
    return _reserve.renderWithZeroDash()
  }

  override property get Earned(): String {
    return _earned.renderWithZeroDash()
  }

  override property get Paid(): String {
    return _paid.renderWithZeroDash()
  }

  override property get WrittenOff(): String {
    return _writtenOff.renderWithZeroDash()
  }

  override property get Total(): String {
    return _total.renderWithZeroDash()
  }

  override property get RowType(): String {
    return "Charge"
  }

  override public function toString():String {
    return Name;
  }

}