package gw.api.invc

uses java.util.Date

@Export
enhancement InvoiceEnhancement : entity.Invoice {
  /**
   * Advance the clock to the day after the invoice's event date and make it billed
   */
  function makeBilledWithClockAdvanceToEventDate(){
    gw.transaction.Transaction.runWithNewBundle( \b -> {
      var invoice = b.add(this)
      if(invoice.EventDate.addDays(1).after(Date.CurrentDate) ){      
        (invoice.EventDate.addDays(1)).setClock()
      }
      invoice.makeBilled()
    })
  }
}
