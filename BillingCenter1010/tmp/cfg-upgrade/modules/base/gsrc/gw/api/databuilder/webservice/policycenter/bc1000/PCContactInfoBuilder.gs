package gw.api.databuilder.webservice.policycenter.bc1000

uses gw.api.databuilder.BCDataBuilder
uses gw.api.databuilder.UniqueKeyGenerator
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCContactInfo
uses typekey.Contact

@Export
class PCContactInfoBuilder {

  var _pcContactInfo : PCContactInfo

  construct() {
    _pcContactInfo = new PCContactInfo()
    withAddressBookUID("ABUID:" + UniqueKeyGenerator.get().nextID())
    withContactType(Contact.TC_PERSON)
    withFirstName(BCDataBuilder.getRandomWord())
    withLastName(BCDataBuilder.getRandomWord())
  }

  public function withAddressBookUID(addressBookUID : String) : PCContactInfoBuilder {
    _pcContactInfo.AddressBookUID = addressBookUID
    return this
  }

  public function withContactType (type : Contact) : PCContactInfoBuilder {
    if(type == Contact.TC_COMPANY) {
      _pcContactInfo.FirstName = null
      _pcContactInfo.LastName = null
    } else if (type == Contact.TC_PERSON) {
      _pcContactInfo.Name = null
    } else {
      throw "The BillingAPI only supports contacts of type person and company"
    }

    _pcContactInfo.ContactType = type.Code
    return this
  }

  public function withFirstName(firstName : String) {
    if(_pcContactInfo.ContactType == Contact.TC_COMPANY.Code){
      throw "FirstName can't be set in contacts of type Company - change the contact type to Company or use withName()"
    }
    _pcContactInfo.FirstName = firstName
  }

  public function withLastName(lastName : String) {
    if(_pcContactInfo.ContactType == Contact.TC_COMPANY.Code){
      throw "LastName can't be set in contacts of type Company - change the contact type to Company or use withName()"
    }
    _pcContactInfo.LastName = lastName
  }

  public function withName(name : String) {
    if(_pcContactInfo.ContactType == Contact.TC_PERSON.Code){
      throw "Name can't be set in contacts of type Person - change the contact type to Person first, or use withFirstName() and withLastName()"
    }
    _pcContactInfo.Name = name
  }

  public function create() : PCContactInfo {
    return _pcContactInfo
  }

}