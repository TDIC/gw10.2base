package gw.api.databuilder

@Export
enhancement CredentialBuilderEnhancement: CredentialBuilder {

  public function withFailedAttempts(failedAttemptsCount : int) : CredentialBuilder {
    this.set(Credential.FAILEDATTEMPTS_PROP.get(), failedAttemptsCount)
    return this
  }

  public function withLoadCommandID(loadCommandID : long) : CredentialBuilder {
    this.set(Credential.LOADCOMMANDID_PROP.get(), loadCommandID)
    return this
  }
}
