package gw.search

uses gw.api.database.IQueryBeanResult
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses gw.pl.currency.MonetaryAmount
uses gw.util.Pair

uses java.io.Serializable
uses java.util.Date

/**
 * Criteria used to perform a search in the PaymentSearchDV
 *
 * This class differs from most other *SearchCriteria.gs classes in that it encompasses searching for 3 different entities:
 * Direct Bill Payments, Agency Bill Payments, and Suspense Payments. It delegates to implementations of SearchStrategy.gs
 * when querying for each of these.
 *
 * The performSearch() method in most *SearchCriteria classes returns the collection that will be used to populate the
 * <SearchScreen> elements's "resultsName" property. In this class the return value of performSearch() is ignored by
 * PaymentSearchScreen.pcf and the results for the 3 kinds of payments are rather accessed through the properties
 * DirectBillMoneyRcvdSearchResults, AgencyBillMoneyRcvdSearchResults, and SuspensePaymentSearchResults.
 *
 * The PaymentType property determines which of the 3 kinds of searches will be performed on the db. A non-null result set
 * will always be returned for each of the 3 kinds of payments, but if the search criteria excluded a kind of payment then
 * as an optimization no search will be performed for that type of payment and an empty result set will be automatically returned.
 *
 */
@Export
class PaymentSearchCriteria implements Serializable  {

  //PaymentType = DIRECTBILLANDSUSPENSE by default because it is expected that in most cases payment searches will be done for DBMRs
  private var _paymentType: PaymentSearchType as PaymentType = PaymentSearchType.DIRECTBILL_AND_SUSPENSE
  private var _resultsTabsVisibleInUI: PaymentSearchType as ResultsTabsVisibleInUI = PaymentSearchType.DIRECTBILL_AND_SUSPENSE
  private var _earliestDate : Date as EarliestDate
  private var _latestDate : Date as LatestDate
  private var _minAmount : MonetaryAmount as MinAmount
  private var _maxAmount : MonetaryAmount as MaxAmount
  private var _producerName : String as ProducerName
  private var _producerNameKanji : String as ProducerNameKanji
  private var _producerCode : String as ProducerCode
  private var _accountNumber : String as AccountNumber
  private var _policyNumber : String as PolicyNumber
  private var _ownerOfActiveSuspenseItems : boolean as OwnerOfActiveSuspenseItems
  
  private var _method : PaymentMethod as Method
  private var _token : String as Token
  private var _checkNumber : String as CheckNumber
  private var _currency : Currency as CurrencyType
  
  private var _directBillMoneyReceivedCriteria : DirectBillMoneyRcvdSearchStrategy as DirectBillMoneyReceivedCriteria
  private var _agencyMoneyReceivedCriteria : AgencyBillMoneyRcvdSearchStrategy as AgencyMoneyReceivedCriteria
  private var _suspensePaymentCriteria : SuspensePaymentSearchStrategy as SuspensePaymentCriteria

  private var _contactCriteria : ContactCriteria as ContactCriteria = new ContactCriteria()
    
  private var _directBillMoneyRcvdSearchResults : IQueryBeanResult<DirectBillMoneyReceivedSearchView> as DirectBillMoneyRcvdSearchResults
  private var _agencyBillMoneyRcvdSearchResults : IQueryBeanResult<AgencyMoneyReceivedSearchView> as AgencyBillMoneyRcvdSearchResults
  private var _suspensePaymentSearchResults : IQueryBeanResult<SuspensePayment> as SuspensePaymentSearchResults
    
  private var _directBillMoneyRcvdSearchResultsCount : int as DirectBillMoneyRcvdSearchResultsCount
  private var _agencyBillMoneyRcvdSearchResultsCount : int as AgencyBillMoneyRcvdSearchResultsCount
  private var _suspensePaymentSearchResultsCount : int as SuspensePaymentSearchResultsCount

  function performSearch() : IQueryBeanResult {
    validateCriteria()
    
    DirectBillMoneyRcvdSearchResults = new DirectBillMoneyRcvdSearchStrategy(this).getSearchResults() as IQueryBeanResult<DirectBillMoneyReceivedSearchView>
    AgencyBillMoneyRcvdSearchResults = new AgencyBillMoneyRcvdSearchStrategy(this).getSearchResults() as IQueryBeanResult<AgencyMoneyReceivedSearchView>
    SuspensePaymentSearchResults = new SuspensePaymentSearchStrategy(this).getSearchResults() as IQueryBeanResult<SuspensePayment>

    DirectBillMoneyRcvdSearchResultsCount = DirectBillMoneyRcvdSearchResults.Count
    AgencyBillMoneyRcvdSearchResultsCount = AgencyBillMoneyRcvdSearchResults.Count
    SuspensePaymentSearchResultsCount = SuspensePaymentSearchResults.Count

    ResultsTabsVisibleInUI = PaymentType

    /* note that as described in this class's javadoc, this return value is ignored by PaymentSearchScreen.pcf and the
    properties DirectBillMoneyRcvdSearchResults, AgencyBillMoneyRcvdSearchResults, SuspensePaymentSearchResults are
    rather directly accessed by clients in order to access search results
     */
    if (DirectBillMoneyRcvdSearchResultsCount > 0) {
      return DirectBillMoneyRcvdSearchResults
    } else if (SuspensePaymentSearchResultsCount > 0) {
      return SuspensePaymentSearchResults
    } else if (AgencyBillMoneyRcvdSearchResultsCount > 0) {
      return AgencyBillMoneyRcvdSearchResults
    } else {
      return DirectBillMoneyRcvdSearchResults
    }
  }

  @Deprecated(:value="Use gw.search.PaymentSearchCriteria.performSearch() instead.", :version="10.0.0")
  function performAgencySearchOnly() : IQueryBeanResult {
    PaymentType = PaymentSearchType.AGENCYBILL
    validateCriteria()
    
    AgencyBillMoneyRcvdSearchResults = new AgencyBillMoneyRcvdSearchStrategy(this).getSearchResults() as IQueryBeanResult<AgencyMoneyReceivedSearchView>
    AgencyBillMoneyRcvdSearchResultsCount = AgencyBillMoneyRcvdSearchResults.Count
    ResultsTabsVisibleInUI = PaymentSearchType.AGENCYBILL

    return AgencyBillMoneyRcvdSearchResults
  }

  /*
  * Field dependency requirements for validation:
  * - if PaymentType=DirectBill or PaymentType=Suspense then the fields {ProducerName, ProducerNameKanji, ProducerCode} are disallowed
  * - if PaymentType=AgencyBill then the fields {Account #, Policy #} are disallowed
  * - if PaymentType=ALL then the fields {ProducerName, ProducerNameKanji, ProducerCode, Account #, Policy #} are disallowed
  */
  protected  function validateCriteria() {
    validateDirectBillAndSuspenseCriteria()
    validateAgencyBillCriteria()
    SearchHelper.checkForDateExceptions(EarliestDate, LatestDate);
    SearchHelper.checkForNumericExceptions(MinAmount, MaxAmount);
  }

  function validateDirectBillAndSuspenseCriteria() {
    if ({PaymentSearchType.DIRECTBILL_AND_SUSPENSE, PaymentSearchType.ALL}.contains(PaymentType)) {
      for (criterion in {
          new Pair(ProducerName, DisplayKey.get("Web.PaymentSearchDV.ProducerName")),
          new Pair(ProducerNameKanji, getDisplayKeyForProducerNameKanji()),
          new Pair(ProducerCode, DisplayKey.get("Web.PaymentSearchDV.ProducerCode"))}) {
        if (criterion.getFirst() != null) {
          throw new DisplayableException(getInvalidCriterionExceptionMessage(criterion.getSecond()))
        }
      }
    }
  }

  public function getDisplayKeyForProducerNameKanji() : String{
    return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP ? DisplayKey.get("Web.PaymentSearchDV.ProducerNamePhonetic") : DisplayKey.get("Web.PaymentSearchDV.ProducerName")
  }

  function validateAgencyBillCriteria() {
    if ({PaymentSearchType.AGENCYBILL, PaymentSearchType.ALL}.contains(PaymentType)) {
      for (criterion in {
          new Pair(AccountNumber, DisplayKey.get("Web.PaymentSearchDV.AccountNumber")),
          new Pair(PolicyNumber, DisplayKey.get("Web.PaymentSearchDV.PolicyNumber"))}) {
        if (criterion.getFirst() != null) {
          throw new DisplayableException(getInvalidCriterionExceptionMessage(criterion.getSecond()))
        }
      }
    }
  }

  private function getInvalidCriterionExceptionMessage(criterionLabel : String) : String {
    return DisplayKey.get("Web.PaymentSearchScreen.InvalidSearchCriterion", criterionLabel, DisplayKey.get("Web.PaymentSearchDV.PaymentType"), PaymentType.DisplayName)
  }
  
  protected function hasPaymentInstrumentCriteria() : boolean {
    return (Method != null || Token != null)
  }

}
