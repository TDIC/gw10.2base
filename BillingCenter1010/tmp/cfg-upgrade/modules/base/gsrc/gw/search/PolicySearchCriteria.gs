package gw.search

uses gw.api.database.BooleanExpression
uses gw.api.database.DBFunction
uses gw.api.database.IQueryBeanResult
uses gw.api.database.ISelectQueryBuilder
uses gw.api.database.Query
uses gw.api.database.Restriction
uses gw.api.database.Table
uses gw.api.system.PLConfigParameters
uses gw.util.concurrent.LockingLazyVar

uses java.io.Serializable
uses java.util.Arrays

/**
 * Criteria used to perform a {@link entity.PolicyPeriod policy} search in
 * the user interface using a {@link entity.PolicySearchView PolicySearchView}.
 */
@Export
class PolicySearchCriteria implements Serializable {
  
  final static var SHOULD_IGNORE_CASE = true

  /**
   * Whether to include archived {@link entity.PolicyPeriod PolicyPeriod}s in
   * the results. Note: If archiving is not enabled, this is ignored. Default is
   * {@code false}: Do not include archived {@code PolicyPeriod}s in the results.
   */
  var _includeArchived : boolean as IncludeArchived = false
  var _accountNumber : String as AccountNumber
  var _policyNumber : String as PolicyNumber
  var _closureStatus : PolicyClosureStatus as ClosureStatus
  var _lobCode : LOBCode as LOBCode
  var _billingMethod : PolicyPeriodBillingMethod as BillingMethod

  /**
   * A matching prefix for the {@link entity.ProducerCode#getCode()
   * Code} of the primary {@link entity.ProducerCode ProducerCode}.
   */
  var _producerCode : String as ProducerCode
  var _producer : Producer as Producer

  var _contactCriteria : ContactCriteria as ContactCriteria = new ContactCriteria()
  
  var _currencyCriterion : Currency as CurrencyCriterion

  static function searchPoliciesByNumber(policyNumber : String) : PolicySearchView {
    // no-op if no producer name specified (see CC-15050)
    policyNumber = policyNumber.trim();
    if (policyNumber.Empty) {
      return null;
    }
    final var criteria = new PolicySearchCriteria()
    criteria.PolicyNumber = policyNumber
    criteria.IncludeArchived = true // include archived here for quick search...
    return criteria.performSearch(TC_STARTSWITH).FirstResult
  }

  function performSearch(contactCriteriaSearchMode : StringCriterionMode) : IQueryBeanResult<PolicySearchView> {
    var accountQueryWithPolicyNumber = buildQuery(Query.make(PolicySearchView), contactCriteriaSearchMode)
    var accountQueryWithPolicyNumberLong = buildQuery(Query.make(PolicySearchView), contactCriteriaSearchMode)

    restrictByZoneAndPolicyNumber(accountQueryWithPolicyNumber, "PolicyNumber")
    restrictByZoneAndPolicyNumber(accountQueryWithPolicyNumberLong, "PolicyNumberLong")

    var accountQuery = accountQueryWithPolicyNumber.union(accountQueryWithPolicyNumberLong)

    return accountQuery.select()
  }

  private function buildQuery(query : Query<PolicySearchView>, contactCriteriaSearchMode : StringCriterionMode) : Query<PolicySearchView> {
    restrictSearchByPolicyPeriodFields(query)
    restrictSearchByPolicyFields(query)
    restrictSearchByProducer(query)
    restrictSearchByUserPermissions(query)
    restrictSearchByAccountNumber(query)
    restrictSearchByContact(query, contactCriteriaSearchMode)
    restrictSearchByCurrency(query)

    return query
  }

  private function restrictSearchByPolicyPeriodFields(query : Query<PolicySearchView>) {
    if (PLConfigParameters.ArchiveEnabled.getValue() && !IncludeArchived) {
      query.compare("ArchiveState", Equals, null)
    }
    if (ClosureStatus != null) {
      query.compare("ClosureStatus", Equals, ClosureStatus)
    }
    if (BillingMethod != null) {
      query.compare("BillingMethod", Equals, BillingMethod)
    }
  }

  private function restrictSearchByPolicyFields(query : Query<PolicySearchView>) {
    if (LOBCode != null) {
      query.join("Policy").compare("LOBCode", Equals, LOBCode)
    }
  }

  private function restrictSearchByProducer(query : Query<PolicySearchView>) {
    if (Producer != null || ProducerCode.NotBlank) {
      if (!PLConfigParameters.ArchiveEnabled.getValue() || !IncludeArchived) {
        // don't include archived results...
        restrictByProducer(policyProducerCodeRestriction(query))
      } else {
        // archived or not...
        query.or(\ restriction : Restriction<PolicySearchView> -> {
          restrictByProducer(policyProducerCodeRestriction(restriction))
          restrictByProducer(archivedPolicyProducerCodeRestriction(restriction))
        })
      }
    }
  }

  /**
   * @param restriction a {@code SelectQueryBuilder} on a {@link entity.PolicySearchView PolicySearchView}
   * to which to link (non-archived) the primary {@code ProducerCode} through a subselect.
   * @return A {@code ISelectQueryBuilder} on the primary
   *         {@link entity.ProducerCode ProducerCode} of a
   *         non-archived {@code PolicyPeriod}.
   */
  private function policyProducerCodeRestriction(restriction: ISelectQueryBuilder<PolicySearchView>)
          : ISelectQueryBuilder<PolicySearchView> {
    return restriction.subselect("ID", CompareIn, PolicyCommission, "PrimaryPolicyPeriod")
        .join(PolicyCommission#ProducerCode)
  }

  /**
   * @param restriction a {@code SelectQueryBuilder} on a
   * {@link entity.PolicySearchView PolicySearchView} to which
   * to link via archive the primary {@code ProducerCode} through a subselect.
   * @return A {@code ISelectQueryBuilder} on the primary
   *         {@link entity.ProducerCode ProducerCode} of an
   *         archived {@code PolicyPeriod}.
   */
  private function archivedPolicyProducerCodeRestriction(restriction : ISelectQueryBuilder<PolicySearchView>)
          : ISelectQueryBuilder {
    final var periodSummarySub = Query.make(PolicyPeriodArchiveSummary)
    restriction.subselect(PolicySearchView#ID, CompareIn, periodSummarySub, PolicyPeriodArchiveSummary#PolicyPeriod)

    final var archiveCommissionTable = periodSummarySub.join(PolicyCommissionArchiveSummary, "PolicyPeriodArchiveSummary")
    // is primary...
    archiveCommissionTable.compare("DefaultForPolicy", Equals, DBFunction.Constant(true))
    return archiveCommissionTable.join(PolicyCommissionArchiveSummary#ProducerCode)
  }

  /**
   * Restrict a {@code SelectQueryBuilder} on a
   * {@link entity.ProducerCode ProducerCode} by the
   * {@code Producer} and/or {@code ProducerCode} values.
   */
  private function restrictByProducer(restrict : ISelectQueryBuilder) {
    if (Producer != null) {
      restrict.compare(entity.ProducerCode#Producer, Equals, Producer)
    }
    if (ProducerCode.NotBlank) {
      restrict.startsWith(entity.ProducerCode#Code, ProducerCode, SHOULD_IGNORE_CASE)
    }
  }

  private function restrictSearchByUserPermissions(query : Query<PolicySearchView>) {
    if (perm.System.plcysearch) {
      // User is permitted to search all policies
      return
    }
    // User is only permitted to search for policies that they have created.
    query.compare("CreateUser", Equals, User.util.CurrentUser)
  }

  private function restrictSearchByAccountNumber(query : Query<PolicySearchView>) {
    if (AccountNumber.NotBlank) {
      query.join("Policy").join("Account").startsWith("AccountNumber", AccountNumber, SHOULD_IGNORE_CASE)
    }
  }

  private function restrictSearchByUserSecurityZone(rest : Restriction<PolicySearchView>) {
    if (perm.System.plcyignoresecurityzone) {
      return // no need to consider security zones
    }
    rest.or(new BooleanExpression<PolicySearchView>() {
      final var groupUsers = Arrays.asList(User.util.CurrentUser.GroupUsers)
      override function execute(restriction : Restriction<PolicySearchView>) {
        restriction.compare("SecurityZone", Equals, null) // Everyone can see null SecurityZone
        for (groupUser in groupUsers) {
          // And user can see their own security zones
          restriction.compare("SecurityZone", Equals, groupUser.Group.SecurityZone)
        }
      }
    })
  }

  private function restrictByZoneAndPolicyNumber(query: Query<PolicySearchView>, policyNumberField: String) {
    query.and(\ restriction : Restriction<PolicySearchView> -> {
      restrictSearchByUserSecurityZone(restriction)
      if (PolicyNumber.NotBlank) {
        restriction.startsWith(policyNumberField, PolicyNumber, SHOULD_IGNORE_CASE)
      }
    })
  }

  private function restrictSearchByContact(query : Query, contactCriteriaSearchMode : StringCriterionMode) {
    // Use a LockingLazyVar so that if no contact criteria was specified, we avoid doing the subselect entirely
    var lazyContactTable = LockingLazyVar.make(\ ->
        query.subselect("ID", CompareIn, PolicyPeriodContact, "PolicyPeriod").join("Contact") as Table<Contact>)
    ContactCriteria.restrictTable(lazyContactTable, contactCriteriaSearchMode)
  }
  
  private function restrictSearchByCurrency(query : Query<PolicySearchView>) {
    if (CurrencyCriterion != null) {
      query.compare("Currency", Equals, CurrencyCriterion)
    }
  }
}