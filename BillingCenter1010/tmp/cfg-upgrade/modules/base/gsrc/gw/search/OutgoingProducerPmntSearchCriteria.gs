package gw.search

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount

uses java.io.Serializable
uses java.util.Date

@Export
class OutgoingProducerPmntSearchCriteria implements Serializable {
  final static var SHOULD_IGNORE_CASE = true

  var _checkNumber : String as CheckNumber
  var _currency : Currency as Currency
  var _earliestIssueDate : Date as EarliestIssueDate
  var _latestIssueDate : Date as LatestIssueDate
  var _minAmount : MonetaryAmount as MinAmount
  var _maxAmount : MonetaryAmount as MaxAmount
  var _token : String as Token
  var _payee : String as Payee
  var _status : OutgoingPaymentStatus as Status
  var _method : PaymentMethod as Method

  function performSearch(): IQueryBeanResult<OutgoingProducerPmntSearchView> {
    SearchHelper.checkForDateExceptions(EarliestIssueDate, LatestIssueDate);
    SearchHelper.checkForNumericExceptions(MinAmount, MaxAmount);
    var query = buildQuery()
    return query.select()
  }

  function buildQuery() : Query<OutgoingProducerPmntSearchView> {
    var query = Query.make(entity.OutgoingProducerPmntSearchView)

    /*Outgoing Producer Payment*/
    restrictSearchByCheckNumber(query)
    restrictSearchByPayee(query)
    restrictSearchByStatus(query)
    restrictSearchByDate(query)
    restrictSearchByMinAndMaxAmount(query)
    restrictSearchByCurrency(query)

    restrictSearchByUserSecurityZone(query)

    /*Payment Instrument*/
    restrictSearchByPaymentInstrument(query)

    return query
  }

  function restrictSearchByCheckNumber(query : Query) {
    if (CheckNumber.NotBlank) {
      query.startsWith("RefNumber", CheckNumber, SHOULD_IGNORE_CASE)
    }
  }
  function restrictSearchByPayee(query : Query) {
    if (Payee.NotBlank) {
      query.startsWith("PayTo", Payee, SHOULD_IGNORE_CASE)
    }
  }
  function restrictSearchByStatus(query : Query) {
    if(Status != null) {
      query.compare("Status", Equals, Status)
    }
  }

  function restrictSearchByCurrency(query : Query) {
    if (Currency != null) {
      query.compare("Currency", Equals, Currency)
    }
  }

  function restrictSearchByDate(query : Query) {
    if (EarliestIssueDate != null || LatestIssueDate != null) {
      // If a latest date has been entered, make sure it is set to the end of the day so that the query
      // will return results after midnight that morning
      var endOfLatestIssueDate = LatestIssueDate != null ? DateUtil.endOfDay(LatestIssueDate) : LatestIssueDate
      query.between("IssueDate", EarliestIssueDate, endOfLatestIssueDate);
    }
  }

  function restrictSearchByMinAndMaxAmount(query : Query) {
    if (MinAmount != null || MaxAmount != null) {
      query.between("Amount", MinAmount, MaxAmount)
    }
  }

  private function hasPaymentInstrumentCriteria() : boolean {
    return (Method != null || Token != null)
  }

  function restrictSearchByPaymentInstrument(query : Query) {
    if(hasPaymentInstrumentCriteria()) {
      var paymentInstrumentQuery = query.join("PaymentInstrument")
      if(Token.NotBlank) {
        paymentInstrumentQuery.compare("Token", Equals, Token)
      }
      if(Method != null) {
        paymentInstrumentQuery.compare("PaymentMethod", Equals, Method)
      }
    }
  }


  private function restrictSearchByUserSecurityZone(query : Query) {
    if (perm.System.prodignoresecurityzone) {
      return // no need to consider security zones
    }
    var producerQuery = query.join("ProducerPayment").join("Producer")
    producerQuery.or(\restriction -> {
      restriction.compare("SecurityZone", Equals, null) // Everyone can see null SecurityZone
      restriction.compareIn("SecurityZone", User.util.CurrentUser.GroupUsers*.Group*.SecurityZone)
    })
  }

}
