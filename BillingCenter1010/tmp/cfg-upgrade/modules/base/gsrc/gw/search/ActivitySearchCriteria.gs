package gw.search

uses java.io.Serializable
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.pl.persistence.core.Bundle

@Export
class ActivitySearchCriteria implements Serializable {

  var _activityPattern: ActivityPattern as ActivityPattern
  var _assignedToUser: User as AssignedToUser
  var _activityStatus: ActivityStatus as Status

  function performSearch(): IQueryBeanResult<ActivitySearchView> {
    var query = Query.make(entity.ActivitySearchView)
    restrictSearchByActivityFields(query);
    return query.select()
  }

  function restrictSearchByActivityFields(query: Query) {
    if (ActivityPattern != null) {
      query.compare("ActivityPattern", Equals, ActivityPattern)
    }
    if (AssignedToUser != null) {
      query.compare("AssignedUser", Equals, AssignedToUser)
    }
    if (Status != null) {
      query.compare("Status", Equals, Status)
    }
  }

}