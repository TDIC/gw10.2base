package gw.search

uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException;
uses gw.api.database.IQueryBeanResult

/**
 * Search-related static utility functions.
 */
@Export
class SearchMethods {

  private static function isRestrictedUser() : boolean {
    return not User.util.CurrentUser.UnrestrictedUser
  }

  /**
   * Account search with validation.
   */
  public static function validateAndSearch(searchCriteria : AccountSearchCriteria,
                                           contactCriteriaSearchMode : StringCriterionMode) : IQueryBeanResult<AccountSearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.AccountNumber.NotBlank or
                   searchCriteria.AccountName.NotBlank or
                   searchCriteria.PolicyNumber.NotBlank or
                   searchCriteria.FEIN.NotBlank or
                   searchCriteria.ContactCriteria.isReasonablyConstrainedForSearch())
    }
    return searchCriteria.performSearch(contactCriteriaSearchMode);
  }

  /**
   * Activity search with validation.
   */
  public static function validateAndSearch(searchCriteria : gw.search.ActivitySearchCriteria) : IQueryBeanResult<ActivitySearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.AssignedToUser != null)
    }
    return searchCriteria.performSearch()
  }

  /**
   * AgencyMoneyReceived search with validation.
   * Because this search duplicates PaymentSearch functionality, it now plugs directly into PaymentSearchCriteria
   */
  @Deprecated(:value="Use gw.search.PaymentSearchCriteria.performSearch() instead.", :version="10.0.0")
  public static function validateAndSearch(searchCriteria : PaymentSearchCriteria,
                                           isAgencyReceived : Boolean) : IQueryBeanResult<AgencyMoneyReceivedSearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.ProducerName.NotBlank or
                   searchCriteria.ProducerCode.NotBlank)
    }
    return searchCriteria.performAgencySearchOnly() as IQueryBeanResult<AgencyMoneyReceivedSearchView>
  }

  /**
   * Charge search with validation.
   */
  public static function validateAndSearch(searchCriteria : gw.search.ReversibleChargeSearchCriteria) : IQueryBeanResult<Charge> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.Account != null or
          searchCriteria.PolicyPeriod != null)
    }
    return searchCriteria.performSearch()
  }

  /**
   * Delinquency search with validation.
   */
  public static function validateAndSearch(searchCriteria : DelinquencySearchCriteria) : IQueryBeanResult<DelinquencySearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.AccountNumber.NotBlank or
                   searchCriteria.PolicyNumber.NotBlank or
                   searchCriteria.ContactCriteria.isReasonablyConstrainedForSearch())
    }
    return searchCriteria.performSearch()
  }


  /**
   * Disbursement search with validation.
   */
  public static function validateAndSearch(searchCriteria : gw.search.AcctDisbSearchCriteria) : IQueryBeanResult<AcctDisbSearchView>{
    if (isRestrictedUser()) {
      validateThat(searchCriteria.CheckNumber.NotBlank or
                   searchCriteria.Payee != null or
                   searchCriteria.AccountNumber.NotBlank)
    }
    return searchCriteria.performSearch()
  }

  public static function validateAndSearch(searchCriteria : gw.search.AgcyDisbSearchCriteria) : IQueryBeanResult<AgcyDisbSearchView>{
    if (isRestrictedUser()) {
      validateThat(searchCriteria.CheckNumber.NotBlank or
                   searchCriteria.Payee.NotBlank)
    }
    return searchCriteria.performSearch()
  }

  public static function validateAndSearch(searchCriteria : gw.search.CollDisbSearchCriteria) : IQueryBeanResult<CollDisbSearchView>{
    if (isRestrictedUser()) {
      validateThat(searchCriteria.CheckNumber.NotBlank or
                   searchCriteria.Payee.NotBlank or
                   searchCriteria.AccountNumber.NotBlank)
    }
    return searchCriteria.performSearch()
  }

  public static function validateAndSearch(searchCriteria : gw.search.SuspDisbSearchCriteria) : IQueryBeanResult<SuspDisbSearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.CheckNumber.NotBlank or
                   searchCriteria.Payee.NotBlank)
    }
    return searchCriteria.performSearch()
  }

  /**
   * Invoice search with validation.
   */
  public static function validateAndSearch(searchCriteria : InvoiceSearchCriteria,
                                           contactCriteriaSearchMode : StringCriterionMode) : IQueryBeanResult<InvoiceSearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.AccountNumber.NotBlank or
                   searchCriteria.InvoiceNumber.NotBlank or
                   searchCriteria.ContactCriteria.isReasonablyConstrainedForSearch())
    }
    return searchCriteria.performSearch(contactCriteriaSearchMode);
  }

  /**
   * NegativeWriteoff transaction search with validation.
   */
  public static function validateAndSearch(searchCriteria : NegWriteoffSearchCrit) : IQueryBeanResult<AcctNegativeWriteoff> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.Account != null)
    }
    return searchCriteria.performSearch() as IQueryBeanResult<AcctNegativeWriteoff>
  }

  /**
   * Outgoing producer payment search with validation.
   */
  public static function validateAndSearch(searchCriteria : gw.search.OutgoingProducerPmntSearchCriteria) : IQueryBeanResult<OutgoingProducerPmntSearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.CheckNumber.NotBlank or
                   searchCriteria.Payee.NotBlank)
    }
    return searchCriteria.performSearch()
  }

  /**
   * Payment search with validation.
   */
  public static function validateAndSearch(searchCriteria : PaymentSearchCriteria) : IQueryBeanResult<DirectBillMoneyReceivedSearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.AccountNumber.NotBlank or
                   searchCriteria.PolicyNumber.NotBlank or
                   searchCriteria.CheckNumber.NotBlank or
                   searchCriteria.Token.NotBlank or 
                   searchCriteria.ContactCriteria.isReasonablyConstrainedForSearch())
    }
    return searchCriteria.performSearch() as IQueryBeanResult<DirectBillMoneyReceivedSearchView>
  }

  /**
   * PaymentRequest search with validation.
   */
  public static function validateAndSearch(searchCriteria : gw.search.PaymentRequestSearchCriteria) : IQueryBeanResult<PaymentRequestSearchView> {
    // Nothing to validate for PaymentRequestSearchCriteria
    return searchCriteria.performSearch()
  }

  /**
   * Policy search with validation.
   */
  public static function validateAndSearch(searchCriteria : PolicySearchCriteria,
                                           contactCriteriaSearchMode : StringCriterionMode) : IQueryBeanResult<PolicySearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.AccountNumber.NotBlank or
                   searchCriteria.PolicyNumber.NotBlank or
                   searchCriteria.ProducerCode.NotBlank or 
                   searchCriteria.ContactCriteria.isReasonablyConstrainedForSearch())
    }
    return searchCriteria.performSearch(contactCriteriaSearchMode);
  }

  /**
   * Producer search with validation.
   */
  public static function validateAndSearch(searchCriteria : ProducerSearchCriteria,
                                           contactCriteriaSearchMode : StringCriterionMode) : IQueryBeanResult<ProducerSearchView>{
    if (isRestrictedUser()) {
      validateThat(searchCriteria.ProducerName.NotBlank or
                   searchCriteria.ProducerCode.NotBlank or
                   searchCriteria.ContactCriteria.isReasonablyConstrainedForSearch())
    }
    return searchCriteria.performSearch(contactCriteriaSearchMode);
  }

  /**
   * Transaction search with validation.
   */
  public static function validateAndSearch(searchCriteria : gw.search.TransactionSearchCriteria) : IQueryBeanResult<Transaction> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.TransactionNumber.NotBlank)
    }
    return searchCriteria.performSearch()
  }

  public static function validateAndSearch(searchCriteria : TroubleTicketSearchCriteria) : IQueryBeanResult<TroubleTicket> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.TroubleTicketNumber.NotBlank or
                   searchCriteria.AccountNumber.NotBlank or
                   searchCriteria.PolicyNumber.NotBlank or
                   searchCriteria.Title.NotBlank or
                   searchCriteria.ContactCriteria.isReasonablyConstrainedForSearch() or
                   searchCriteria.AssignedToUser != null)
    }
    return searchCriteria.performSearch() as IQueryBeanResult<TroubleTicket>
  }

  /**
   * Writeoff search with validation.
   */
  public static function validateAndSearch(searchCriteria : gw.search.WriteoffSearchCriteria) : IQueryBeanResult<Writeoff> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.Account != null or
                   searchCriteria.PolicyPeriod != null)
    }
    return searchCriteria.performSearch()
  }

  /**
   * Credit search with validation.
   */
  public static function validateAndSearch(searchCriteria : CreditSearchCriteria) : IQueryBeanResult<Credit> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.Account != null or
                   searchCriteria.Reason != null)
    }
    return searchCriteria.performSearch() as IQueryBeanResult<Credit>
  }

  /**
   * Invoice Item search with validation.
   */
   public static function validateAndSearch(searchCriteria : InvoiceItemSearchCriteria, baseDist : BaseDist) : IQueryBeanResult<InvoiceItem> {
     if (isRestrictedUser()) {
       validateThat(searchCriteria.OwnerAccount.NotBlank or
                    searchCriteria.PayerAccountNumber.NotBlank or
                    searchCriteria.PayerProducerName.NotBlank or
                    searchCriteria.PayerProducerNameKanji.NotBlank or
                    searchCriteria.PolicyPeriod.NotBlank)
     }
     if (baseDist typeis AgencyCyclePromise) {
       searchCriteria.DistributionTypeIsPromise = true
     }
     return searchCriteria.performSearch(baseDist)
   }

  /**
   * Direct Bill Suspense Item search with validation.
   */
  public static function validateAndSearch(searchCriteria : gw.search.DirectSuspPmntItemSearchCriteria) : IQueryBeanResult<DirectSuspPmntItemSearchView> {
    if (isRestrictedUser()) {
      validateThat(searchCriteria.EarliestDate != null or
                   searchCriteria.LatestDate != null or
                   searchCriteria.MinAmount != null or
                   searchCriteria.MaxAmount != null)
    }
    return searchCriteria.performSearch()
  }

  /**
   * Statement Invoice search for Agency Payment Wizard with validation
   */
  public static function validateAndSearch(searchCriteria : StatementInvoiceSearchCriteria) : IQueryBeanResult<StatementInvoice> {
    if (isRestrictedUser()) {
      //nothing to validate
    }
    return searchCriteria.performSearch()
  }
  
  
  /**
   * Policy Period search for Agency Payment Wizard with validation
   */
  public static function validateAndSearch(searchCriteria : PolicyPeriodSearchCriteria) : IQueryBeanResult<PolicyPeriod> {
    if (isRestrictedUser()) {
      // nothing to validate
    }
    return searchCriteria.performSearch()
  }


  // Private helper methods

  /**
   * Confirm that the given condition is true.  If not, throw a DisplayableException
   * with the RequiredNotPresent message.
   */
  private static function validateThat(condition : boolean) {
    if (not condition) {
      throw new DisplayableException(DisplayKey.get("Java.Search.Error.RequiredNotPresent"))
    }
  }

}
