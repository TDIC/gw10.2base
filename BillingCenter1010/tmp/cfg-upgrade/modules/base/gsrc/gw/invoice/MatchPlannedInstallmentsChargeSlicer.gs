package gw.invoice

uses com.google.common.collect.ImmutableList
uses gw.api.domain.charge.ChargeInitializer
uses gw.api.domain.invoice.ChargeInstallmentChanger
uses gw.api.domain.invoice.ChargeSlicer
uses gw.api.domain.invoice.ChargeSlicers
uses gw.api.locale.DisplayKey
uses gw.api.util.Ratio
uses gw.pl.currency.MonetaryAmount
uses gw.util.Pair

uses java.lang.UnsupportedOperationException
uses java.util.ArrayList
uses java.util.Date
uses java.util.Map

/**
 * The MatchPlannedInstallmentsChargeSlicer will slice the new Charge's Amount by matching the planned installments
 * of the reference Charge(s) and, aggregating the installments by Invoice, distribute the amount proportionally among
 * the planned installments.
 */
@Export
class MatchPlannedInstallmentsChargeSlicer implements ChargeSlicer {

  override function createEntries(chargeInitializer: ChargeInitializer) {
    if (chargeInitializer.UnallocatedChargeAmount.IsZero) { return }
    var referenceCharges = referenceCharges(chargeInitializer)
    var datesAndAmounts = datesAndAmountsFromCharges(referenceCharges, chargeInitializer)
    addEntries(chargeInitializer, datesAndAmounts, TC_INSTALLMENT)
  }

  /**
   * This method is unsupported in the base configuration because the MatchPlannedInstallmentsChargeSlicer is not
   * designed for a PaymentPlan refactor.
   */
  override function recreateInvoiceItems(changer: ChargeInstallmentChanger, totalAmountForInvoiceItems: MonetaryAmount) {
    throw new UnsupportedOperationException(DisplayKey.get("Java.ChargeSlicerSelector.MatchPlannedInstallments.RecreateInvoiceItemsNotSupported"))
  }

  /**
   * Returns the Charges of the ChargeInitializer's PolicyPeriod which match the ChargeInitializer's ChargePattern.
   * @param chargeInitializer the ChargeInitializer which is using this MatchPlannedInstallmentsChargeSlicer as its
   *                          ChargeSlicer
   */
  function referenceCharges(chargeInitializer: ChargeInitializer) : ImmutableList<Charge> {
    return MatchPlannedInstallmentsMethods.referenceCharges(chargeInitializer)
  }

  /**
   * Returns the result of the slicing done by this MatchPlannedInstallmentsChargeSlicer in the form of a pair of lists
   * of ordered dates and amounts of the InvoiceItems to be created.
   * 1. The matchingInvoiceItems of the reference Charge(s) are found by filtering for planned InvoiceItems and the
   *    EventDate being on or after the EffectiveDate.
   * 2. Aggregate the amounts of the InvoiceItems by Invoice.
   * 3. Transform the aggregated amounts into a pair of ordered lists of dates and target amounts.
   * 4. Distribute the ChargeInitializer's amount proportionally among the target amounts.
   * Return a pair of ordered lists of dates and sliced amounts.
   */
  private function datesAndAmountsFromCharges(referenceCharges : ImmutableList<Charge>, chargeInitializer : ChargeInitializer) : Pair<List<Date>, List<MonetaryAmount>> {
    var amount = chargeInitializer.UnallocatedChargeAmount
    if (includeDownPayment(referenceCharges, chargeInitializer)) {
      amount = sliceDownPayment(referenceCharges, chargeInitializer)
    }
    var matchingInvoiceItems = matchingInvoiceItems(referenceCharges, chargeInitializer)
    var groupedAmounts = groupAmountsByInvoice(matchingInvoiceItems, amount.Currency)
    var datesAndAmounts = orderedListsOfDatesAndAmounts(groupedAmounts, referenceCharges)
    var orderedDates = datesAndAmounts.First
    var orderedAmounts = datesAndAmounts.Second
    var proportionalAmounts = distributeProportionally(amount, orderedAmounts)
    return Pair.make(orderedDates, proportionalAmounts)
  }

  /**
   * Slice the downpayments and return a remainder amount to be sliced among the installments.
   *
   * Compute downpayment fraction = sum(downpayments) / sum(charges which have down payments)
   * Compute amount of down payment for the new charge = ratio * ChargeInitizlizer's Amount
   * Distribute the new downpayment proportionally
   * Compute the adjusted ChargeInitializer's Amount = amount - new downpayment amount
   * Add down payment InvoiceItem entries to the ChargeInitializer
   * Return the adjusted amount
   */
  private function sliceDownPayment(referenceCharges : ImmutableList<Charge>, chargeInitializer : ChargeInitializer) : MonetaryAmount {
    var newChargeAmount = chargeInitializer.UnallocatedChargeAmount
    var currency = newChargeAmount.Currency
    var downPayments = referenceCharges*.InvoiceItems.where(\invoiceItem -> invoiceItem.Deposit).sortBy(\ii -> ii.EventDate)
    var downPaymentsSum = downPayments*.Amount.sum(currency)
    if (downPaymentsSum.IsZero) { return newChargeAmount }
    var chargesHavingDownPaymentSum = referenceCharges.where(\charge -> charge.InvoiceItems.hasMatch(\invoiceItem -> invoiceItem.Deposit))*.Amount.sum(currency)
    var downPaymentFraction = Ratio.valueOf(downPaymentsSum, chargesHavingDownPaymentSum)
    var amountOfDownPaymentForNewCharge = downPaymentFraction
        .multiply(newChargeAmount.Amount).toMonetaryAmount(currency)
    var adjustedAmount = newChargeAmount.subtract(amountOfDownPaymentForNewCharge)
    chargeInitializer.addEntry(amountOfDownPaymentForNewCharge, InvoiceItemType.TC_DEPOSIT, downPayments.first().EventDate)
    return adjustedAmount
  }

  /**
   * Returns the planned InvoiceItems of the reference Charge(s) of a ChargeInitializer where the EventDate is on or
   * after the ChargeInitializer's EffectiveDate
   */
  function matchingInvoiceItems(referenceCharges : ImmutableList<Charge>, chargeInitializer: ChargeInitializer) : InvoiceItem[] {
    return MatchPlannedInstallmentsMethods.matchingInvoiceItems(referenceCharges, chargeInitializer)
  }

  /**
   * Aggregates the amounts of the given InvoiceItems by Invoice.
   */
  private function groupAmountsByInvoice(matchingInvoiceItems : InvoiceItem[], currency : Currency) : Map<Invoice, MonetaryAmount> {
    return matchingInvoiceItems.partition(\invoiceItem -> invoiceItem.Invoice)
        .mapValues(\invoiceItems -> invoiceItems*.Amount.sum(currency))
  }

  /**
   * Transforms the map of Invoice -> Amount into a pair of ordered lists of dates and amounts respectively.
   */
  private function orderedListsOfDatesAndAmounts(groupedAmounts : Map<Invoice, MonetaryAmount>, referenceCharges : ImmutableList<Charge>) : Pair<List<Date>, List<MonetaryAmount>> {
    var dates = new ArrayList<Date>()
    var amounts = new ArrayList<MonetaryAmount>()
    var invoices = groupedAmounts.Keys.toList().sortBy(\invoice -> invoice.EventDate)
    for (invoice in invoices) {
      dates.add(MatchPlannedInstallmentsMethods.eventDateToUse(invoice, referenceCharges, TC_INSTALLMENT))
      amounts.add(groupedAmounts.get(invoice))
    }
    return Pair.make(dates, amounts)
  }

  /**
   * Returns a list of MonetaryAmounts representing a proportional distribution of the given amount based upon the
   * distribution of the list of target amounts.
   */
  private function distributeProportionally(totalToDistribute : MonetaryAmount, targets : List<MonetaryAmount>) : List<MonetaryAmount> {
    return ChargeSlicers.distributeProportionally(totalToDistribute, targets)
  }

  /**
   * Returns true when the effective date of the new charge is the same as the PolicyPeriod's effective date
   * and at least one down payment exists.
   */
  function includeDownPayment(referenceCharges : ImmutableList<Charge>, chargeInitializer : ChargeInitializer) : boolean{
    return MatchPlannedInstallmentsMethods.isEndorsementEffectiveSameAsPolicyPeriodEffective(chargeInitializer)
        and referenceCharges*.InvoiceItems.hasMatch(\invoiceItem -> invoiceItem.Deposit)
  }

  /**
   * Adds entries to the given ChargeInitializer for the given pair of ordered lists of Dates and MonetaryAmounts
   * @param chargeInitializer the ChargeInitializer
   * @param installmentDatesAndAmounts a pair of ordered lists of Dates and MonetaryAmounts
   * @param invoiceItemType Either Installment, Deposit or Onetime.
   */
  private function addEntries(chargeInitializer : ChargeInitializer, installmentDatesAndAmounts : Pair<List<Date>, List<MonetaryAmount>>, invoiceItemType : InvoiceItemType) {
    var dates = installmentDatesAndAmounts.First
    var amounts = installmentDatesAndAmounts.Second
    dates.eachWithIndex(\elt, index -> {
      if (amounts.get(index).IsNotZero) {
        chargeInitializer.addEntry(amounts.get(index), invoiceItemType, dates.get(index));
      }
    })
  }

}