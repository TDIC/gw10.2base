package gw.invoice

uses com.google.common.collect.ImmutableList
uses gw.api.domain.charge.ChargeInitializer
uses typekey.BillingInstruction

uses java.lang.Integer
uses java.util.Collections
uses java.util.Date

@Export
class MatchPlannedInstallmentsMethods {
  /**
   * Returns the Charges of this ChargeInitializer's PolicyPeriod which match the ChargeInitializer's ChargePattern.
   * @param chargeInitializer the given ChargeInitializer
   * @return the Charges of this ChargeInitializer's PolicyPeriod which match the ChargeInitializer's ChargePattern
   */
  static function referenceCharges(chargeInitializer: ChargeInitializer) : ImmutableList<Charge> {
    var charges = findReferenceCharges(chargeInitializer)
    return ImmutableList.copyOf(charges)
  }

  /**
   * Returns the Charges of this ChargeInitializer's PolicyPeriod which match the ChargeInitializer's ChargePattern
   * excluding all charges on last Cancellation BillingInstruction.
   * @param chargeInitializer the given ChargeInitializer
   * @return the Charges of this ChargeInitializer's PolicyPeriod which match the ChargeInitializer's ChargePattern
   * excluding all charges on last Cancellation BillingInstruction
   */
  static function referenceChargesForReinstatement(chargeInitializer: ChargeInitializer) : ImmutableList<Charge> {
    var charges = findReferenceCharges(chargeInitializer)
    return ImmutableList.copyOf(excludingLastCancellation(charges))
  }

  private static function findReferenceCharges(chargeInitializer: ChargeInitializer) : List<Charge> {
    return chargeInitializer.PolicyPeriod.Charges.where(\ charge ->
                                                            charge.ChargePattern == chargeInitializer.ChargePattern and
                                                            charge.BillingInstruction != chargeInitializer.BillingInstruction)
  }

  private static function excludingLastCancellation(charges : List<Charge>) : List<Charge> {

    var latestCancellationBI = charges.where(\c -> c.BillingInstruction typeis Cancellation)
                                  .sortBy(\c -> c.BillingInstruction.CreateTime)
                                  .last()
                                  .BillingInstruction
    return latestCancellationBI != null
              ? charges.where(\charge -> charge.BillingInstruction != latestCancellationBI)
              : charges
  }

  /**
   *
   * Returns the matching InvoiceItems of the reference Charges of a ChargeInitializer.
   * If the effective date of the PolicyChange is the same as the PolicyPeriod's effective date then return all
   * installments of the Charges. Otherwise return the planned InvoiceItems which are installments and where
   * the EventDate is on or after the ChargeInitializer's EffectiveDate.
   * @param referenceCharges the given reference Charges of a ChargeInitializer
   * @param chargeInitializer the ChargeInitializer
   * @return the matching InvoiceItems
   */
  static function matchingInvoiceItems(referenceCharges : ImmutableList<Charge>, chargeInitializer: ChargeInitializer) : InvoiceItem[] {
    var allReferenceInvoiceItems = referenceCharges*.InvoiceItems
    if (isEndorsementEffectiveSameAsPolicyPeriodEffective(chargeInitializer)) {
      return allReferenceInvoiceItems.where(\invoiceItem -> invoiceItem.Installment)
    } else {
      return plannedInstallmentsAfter(allReferenceInvoiceItems, chargeInitializer.EffectiveDate)
    }
  }

  /**
   *
   * Returns the matching InvoiceItems of the reference Charges of a ChargeInitializer.
   * Matching InvoiceItems are the planned InvoiceItems which are installments and where
   * the EventDate is on or after the ChargeInitializer's EffectiveDate.
   * @param referenceCharges the given reference Charges of a ChargeInitializer
   * @param chargeInitializer the ChargeInitializer
   * @return the matching InvoiceItems
   */
  static function matchingInvoiceItemsForReinstatement(referenceCharges : ImmutableList<Charge>, chargeInitializer: ChargeInitializer) : InvoiceItem[] {
    return plannedInstallmentsAfter(referenceCharges*.InvoiceItems, chargeInitializer.EffectiveDate)
  }

  private static function plannedInstallmentsAfter(invoiceItems: InvoiceItem[], effectiveDate: Date): InvoiceItem[] {
    return invoiceItems.where(\invoiceItem -> invoiceItem.Installment and
                                              invoiceItem.Invoice.Planned and
                                              invoiceItem.EventDate >= effectiveDate)
  }

  /**
   * Returns the appropriate EventDate for the Invoice. Among the Invoice's InvoiceItems, find the InvoiceItem which
   * belongs to the earliest reference Charge, giving preference to the Issuance Charge, and use the EventDate
   * of that InvoiceItem.
   * @param invoice the given Invoice
   * @param referenceCharges the reference charges
   * @return the appropriate EventDate for the Invoice given these reference Charges
   */
  static function eventDateToUse(invoice : Invoice, referenceCharges : ImmutableList<Charge>, invoiceItemType : InvoiceItemType) : Date {
    var relevantInvoiceItems = invoice.InvoiceItems
        .where(\invoiceItem -> invoiceItem.Type == invoiceItemType and referenceCharges.contains(invoiceItem.Charge))
        .toList()
    sortInvoiceItemsOfInvoice(relevantInvoiceItems)
    return relevantInvoiceItems.first().EventDate
  }

  private static function sortInvoiceItemsOfInvoice(invoiceItems: List<InvoiceItem>) {
    Collections.sort(invoiceItems, \ o1 : InvoiceItem, o2 : InvoiceItem -> {
      var charge1 = o1.Charge
      var charge2 = o2.Charge
      // Order by the BillingInstruction subtype: Issuance before PolicyChange
      var result = BILLING_INSTRUCTION_SUBTYPE_SORT_ORDER.parse(charge1.BillingInstruction.Subtype).Ordering
          .compareTo(BILLING_INSTRUCTION_SUBTYPE_SORT_ORDER.parse(charge2.BillingInstruction.Subtype).Ordering)
      if (result == 0) {
        // Then order by the Charge's ChargeDate
        result = charge1.ChargeDate.compareTo(charge2.ChargeDate)
      }
      if (result == 0) {
        // Then order by the InvoiceItem's EventDate
        result = o1.EventDate.compareTo(o2.EventDate)
      }
      return result
    })
  }

  /**
   * Create an order for BillingInstruction subtypes for sorting so that Issuance comes before PolicyChange
   * ISSUANCE_BILLINGINSTRUCTION     -> 1
   * POLICYCHANGE_BILLINGINSTRUCTION -> 2
   * OTHER_BILLINGINSTRUCTION        -> 3
   */
  private enum BILLING_INSTRUCTION_SUBTYPE_SORT_ORDER {
    ISSUANCE_BILLINGINSTRUCTION(1),
    POLICYCHANGE_BILLINGINSTRUCTION(2),
    OTHER_BILLINGINSTRUCTION(3)

    private var _value : Integer as readonly Ordering
    private construct(ordering : Integer) { _value = ordering }
    static function parse(billingInstruction : BillingInstruction) : BILLING_INSTRUCTION_SUBTYPE_SORT_ORDER {
      if (billingInstruction == TC_ISSUANCE) {
        return ISSUANCE_BILLINGINSTRUCTION
      }
      if (billingInstruction == TC_POLICYCHANGE) {
        return POLICYCHANGE_BILLINGINSTRUCTION
      }
      return OTHER_BILLINGINSTRUCTION
    }
  }

  /**
   * Returns true if the effective date of the new charge is the same as the PolicyPeriod's effective date.
   */
  static function isEndorsementEffectiveSameAsPolicyPeriodEffective(chargeInitializer: ChargeInitializer) : boolean {
    return chargeInitializer.EffectiveDate.isSameDayIgnoringTime(chargeInitializer.PolicyPeriod.PolicyPerEffDate)
  }

}