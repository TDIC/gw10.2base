package gw.paymentplanmodifier

uses gw.api.domain.invoice.PaymentPlanModifierMethods

@Export
class SuppressDownPaymentMethodsImpl implements PaymentPlanModifierMethods {

  construct (suppressDownPayment : SuppressDownPayment) {
  }

  override public function modify(paymentPlan : PaymentPlan) {
    paymentPlan.HasDownPayment = false;
  }
}
