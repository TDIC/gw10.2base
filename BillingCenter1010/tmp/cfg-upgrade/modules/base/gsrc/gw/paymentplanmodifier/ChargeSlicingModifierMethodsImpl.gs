package gw.paymentplanmodifier

uses gw.api.domain.invoice.PaymentPlanModifierMethods

@Export
class ChargeSlicingModifierMethodsImpl implements PaymentPlanModifierMethods {

  private var _chargeSlicingOverrides : ChargeSlicingOverrides;

  construct (chargeSlicingModifier: ChargeSlicingModifier) {
    _chargeSlicingOverrides = chargeSlicingModifier.ChargeSlicingOverrides
  }

  override function modify(paymentPlan : PaymentPlan) {
    paymentPlan.applyOverrides(_chargeSlicingOverrides)
    // Customization: If you have extension fields on PaymentPlan that you can override by billing instruction type,
    // then apply the overrides for those fields here. e.g.
    // if (_chargeSlicingOverrides.CustomField != null) {
    //      paymentPlan.CustomField = _chargeSlicingOverrides.CustomField
    // }
  }
}