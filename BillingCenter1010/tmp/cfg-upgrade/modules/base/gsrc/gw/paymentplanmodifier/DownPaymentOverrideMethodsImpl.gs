package gw.paymentplanmodifier

uses gw.api.domain.invoice.PaymentPlanModifierMethods

@Export
class DownPaymentOverrideMethodsImpl implements PaymentPlanModifierMethods {

  private var _downPaymentOverride : DownPaymentOverride;

  construct (downPaymentOverride : DownPaymentOverride) {
    _downPaymentOverride = downPaymentOverride
  }

  override function modify(paymentPlan : PaymentPlan) {
    paymentPlan.HasDownPayment = true;
    paymentPlan.DownPaymentPercent = _downPaymentOverride.DownPaymentPercent
  }
}

