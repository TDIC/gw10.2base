package gw.webservice.policycenter.bc900

uses gw.webservice.policycenter.bc900.entity.types.complex.CommissionPlanInfo
uses java.util.List

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement CommissionPlanInfoEnhancement : CommissionPlanInfo {
  function copyCommissionPlanInfo(plan : CommissionPlan) {
    this.copyPlanCurrencyInfo(plan)
    this.AllowedTiers = getAllowedTiers(plan)
  }
  
  private function getAllowedTiers(plan : CommissionPlan) : List<String> {
    return ProducerTier.getTypeKeys( false )
      .where( \ p -> plan.isAllowedTier( p ) )*.Code.toList()
  }
}
