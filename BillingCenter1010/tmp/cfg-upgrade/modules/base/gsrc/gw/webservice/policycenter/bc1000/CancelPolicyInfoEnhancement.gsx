package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.CancelPolicyInfo

/**
 * Defines behavior for the 910 API version of the WSDL entity that specifies
 * the data used to create a {@link Cancellation Cancellation} billing
 * instruction.
 */
@Export
enhancement CancelPolicyInfoEnhancement : CancelPolicyInfo {
  function executeCancellationBI(): BillingInstruction {
    return executedCancellationBISupplier()
  }

  /**
   * @deprecated Since 9.0.1. Use {@link #executeCancellationBI()} instead.
   */
  @java.lang.Deprecated
  function execute() : String {
    return executeCancellationBI().PublicID
  }

  private function createCancellationBI() : Cancellation {
    final var policyPeriod = this.findPolicyPeriod()
    final var cancellation = new Cancellation(policyPeriod.Currency)
    cancellation.AssociatedPolicyPeriod = policyPeriod
    cancellation.CancellationType = CancellationType.get(this.CancellationType)
    cancellation.CancellationReason = this.CancellationReason
    // cancellation.EffectiveDate this is auto set to the posted date
    cancellation.SpecialHandling = SpecialHandling.get(this.SpecialHandling.Code)
    return cancellation
  }

  function toCancellationForPreview() : Cancellation {
    final var bi = createCancellationBI()
    this.initializeBillingInstruction(bi)
    return bi
  }

  function executedCancellationBISupplier(): Cancellation {
    var bi = createCancellationBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
