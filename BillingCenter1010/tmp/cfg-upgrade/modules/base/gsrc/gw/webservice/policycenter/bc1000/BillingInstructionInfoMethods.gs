package gw.webservice.policycenter.bc1000

@Export
class BillingInstructionInfoMethods {
  static function assignOfferNumberToPolicyPeriodIfNotNull(policyPeriod : PolicyPeriod, offerNumber : String) {
    if (offerNumber != null) policyPeriod.OfferNumber = offerNumber
  }
}