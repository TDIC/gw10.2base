package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.PlanInfo
uses entity.Plan

@Export
enhancement PlanInfoEnhancement : PlanInfo {
  function copyPlanInfo(plan : Plan) {
    this.Name = plan.Name
    this.PublicID = plan.PublicID
  }
}
