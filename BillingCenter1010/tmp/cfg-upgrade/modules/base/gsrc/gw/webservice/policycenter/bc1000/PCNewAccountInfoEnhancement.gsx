package gw.webservice.policycenter.bc1000

uses gw.api.database.Query
uses gw.api.webservice.exception.BadIdentifierException
uses gw.pl.persistence.core.Bundle
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewAccountInfo

@Export
enhancement PCNewAccountInfoEnhancement: PCNewAccountInfo {
  function toNewAccount(currency: Currency, bundle: Bundle): Account {
    var account = new Account(bundle, currency)
    account.AccountNumber = this.AccountNumber
    account.AccountName = this.AccountName
    account.AccountNameKanji = this.AccountNameKanji
    account.BillingLevel = BillingLevel.get(this.BillingLevel)
    account.ServiceTier = CustomerServiceTier.get(this.CustomerServiceTier)
    PCAccountInfoEnhancement.initializeAccountDefaults(account)
    if (this.PaymentAllocationPlanPublicID != null) {
      final var planLookupResult = Query.make(PaymentAllocationPlan)
          .compare("PublicID", Equals, this.PaymentAllocationPlanPublicID).select()
      if (planLookupResult.Empty) {
        throw BadIdentifierException
            .badPublicId(PaymentAllocationPlan, this.PaymentAllocationPlanPublicID)
      }
      account.PaymentAllocationPlan = planLookupResult.AtMostOneRow
    }
    for (contact in this.BillingContacts) {
      var billingContact = contact.$TypeInstance.toAccountContact(account, {AccountRole.TC_ACCOUNTSPAYABLE})
      account.addToContacts(billingContact)
    }
    var insured = this.InsuredContact.$TypeInstance.toAccountContact(account, AccountHolderRoles)
    // init primary payer
    if (this.BillingContacts.Count > 0) {
      account.Contacts[0].PrimaryPayer = true
    } else {
      insured.PrimaryPayer = true
    }
    account.addToContacts(insured)
    return account
  }

  private property get AccountHolderRoles(): AccountRole[] {
    return this.InsuredIsBilling
        ? {AccountRole.TC_INSURED, AccountRole.TC_ACCOUNTSPAYABLE}
        : {AccountRole.TC_INSURED}
  }
}
