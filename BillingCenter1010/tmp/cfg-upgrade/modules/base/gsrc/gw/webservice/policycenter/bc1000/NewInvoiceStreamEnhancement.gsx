package gw.webservice.policycenter.bc1000

uses com.google.common.annotations.VisibleForTesting
uses gw.api.domain.invoice.AnchorDate
uses gw.api.domain.invoice.InvoiceStreamFactory
uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.SOAPSenderException
uses gw.pl.persistence.core.Bundle
uses gw.webservice.policycenter.bc1000.entity.types.complex.NewInvoiceStream
uses gw.webservice.util.WebserviceEntityLoader
uses entity.InvoiceStream

/**
 * Creates an InvoiceStream from the webservice DTO NewInvoiceStream.  
 * If NewInvoiceStream.PaymentInstrumentID is not null, then the new
 * stream uses the specified payment instrument as its overriding payment
 * instrument.  If the payment instrument is not specified but 
 * NewInvoiceStream.PaymentMethod is Responsive, then the new invoice stream
 * will use the Responsive payment instrument as the overriding
 * payment instrument.
 */
@Export
enhancement NewInvoiceStreamEnhancement : NewInvoiceStream {
  
  function createInvoiceStreamFor(account : Account, bundle : Bundle) : InvoiceStream {
    if (account.Bundle != bundle) {
      account = bundle.add(account)
    }

    var unappliedFund : UnappliedFund
    if (this.UnappliedFundID != null) {
      unappliedFund = bundle.add(UnappliedFund)
    } else if (this.UnappliedDescription != null) {
      unappliedFund = account.createDesignatedUnappliedFund(this.UnappliedDescription)
    } else {
      // out-of-the-box this only happens when the account did not exist
      // before issuance; if it can happen otherwise configure as desired...
      unappliedFund = account.DefaultUnappliedFund
    }

    var invoiceStream = InvoiceStreamFactory.createInvoiceStreamFor(unappliedFund, Periodicity.get(this.Interval))
    invoiceStream.setOverridingAnchorDates(OverridingAnchorDates)
    invoiceStream.OverridingPaymentInstrument = getOverridingPaymentInstrumentFrom(account)
    invoiceStream.OverridingBillDateOrDueDateBilling = OverridingBillDateOrDueDateBilling
    invoiceStream.Description = this.Description
    invoiceStream.OverridingLeadTimeDayCount = this.LeadTimeDayCount
    return invoiceStream
  }

  @VisibleForTesting
  property get OverridingAnchorDates() : List<AnchorDate> {
    // PC sends a NewInvoiceStream with DayOfWeek, FirstDayOfMonth,
    // and SecondDayOfMonth all non-null, so use the stream periodicity
    // to determine which are really the overriding anchor dates.
    var streamPeriodicity : Periodicity = Periodicity.get(this.Interval)
    return InvoiceStreamUtil.getOverridingAnchorDates(this, streamPeriodicity)
  }

  private property get OverridingBillDateOrDueDateBilling() : BillDateOrDueDateBilling {
    if (this.DueDateBilling == null) {
      throw new SOAPSenderException(DisplayKey.get("Webservice.Error.MissingRequiredField", "NewInvoiceStream.DueDateBilling"))
    }
    return this.DueDateBilling ? TC_DUEDATEBILLING : TC_BILLDATEBILLING
  }

  @VisibleForTesting
  function getOverridingPaymentInstrumentFrom(account : Account) : PaymentInstrument {
    // If NewInvoiceStream.PaymentInstrumentID is not null, then the new
    // stream uses the specified payment instrument as its overriding payment
    // instrument.  If the payment instrument is not specified but 
    // NewInvoiceStream.PaymentMethod is Responsive, then the new invoice stream
    // uses the Responsive payment instrument as the overriding
    // payment instrument.   
    if (this.PaymentInstrumentID != null) {
      return WebserviceEntityLoader
          .loadByPublicID<PaymentInstrument>(this.PaymentInstrumentID, "PaymentInstrumentID")
    }
    if (this.PaymentMethod == PaymentMethod.TC_RESPONSIVE.Code) {
      return account.PaymentInstruments.firstWhere(\ instrument -> instrument.PaymentMethod == TC_RESPONSIVE)
    }
    return null
  }

  private property get UnappliedFund() : UnappliedFund {
    return this.UnappliedFundID == null
        ? null : WebserviceEntityLoader
            .loadByPublicID<UnappliedFund>(this.UnappliedFundID, "UnappliedFundID")
  }
}
