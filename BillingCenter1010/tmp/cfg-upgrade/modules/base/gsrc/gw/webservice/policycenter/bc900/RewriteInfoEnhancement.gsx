package gw.webservice.policycenter.bc900

uses gw.api.domain.accounting.BillingInstructionUtil
uses gw.api.locale.DisplayKey
uses gw.api.web.policy.NewPolicyUtil
uses gw.api.webservice.exception.BadIdentifierException
uses gw.webservice.policycenter.bc900.entity.types.complex.RewriteInfo

/**
 * Defines behavior for the 900 API version of the WSDL entity that specifies
 * the data used to create a {@link entity.Rewrite Rewrite} billing instruction.
 */
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement RewriteInfoEnhancement : RewriteInfo {
  function executeRewriteBI() : BillingInstruction {
    return executedRewriteBISupplier()
  }

  function toRewriteForPreview() : Rewrite {
    final var bi = createRewriteBI(true)
    this.initializeBillingInstruction(bi)
    return bi
  }

  private function createRewriteBI(isForPreview : boolean = false) : Rewrite {
    final var priorPolicyPeriod = this.findPriorPolicyPeriod()
    if (priorPolicyPeriod == null) {
      throw new BadIdentifierException(
          DisplayKey.get("Webservice.Error.CannotFindMatchingPolicyPeriod",
              this.PriorPolicyNumber?:this.PolicyNumber,
              this.PriorTermNumber?:(this.TermNumber - 1)))
    }
    final var bi = (priorPolicyPeriod.Currency != this.CurrencyValue)
        ? NewPolicyUtil.createCurrencyChangeRewrite(this.findOwnerAccount(), priorPolicyPeriod)
        : NewPolicyUtil.createRewrite(priorPolicyPeriod)
    this.initPolicyPeriodBIInternal(bi) // must precede populate when prior period exists...
    this.populateIssuanceInfo(isForPreview, bi.NewPolicyPeriod)
    return bi
  }

  function executedRewriteBISupplier(): NewPlcyPeriodBI {
    var bi = createRewriteBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}