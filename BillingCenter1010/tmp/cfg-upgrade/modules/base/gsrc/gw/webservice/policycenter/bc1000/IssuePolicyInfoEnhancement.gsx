package gw.webservice.policycenter.bc1000

uses entity.InvoiceStream
uses gw.api.database.Query
uses gw.api.domain.invoice.InvoicePayer
uses gw.api.domain.invoice.InvoiceStreamFactory
uses gw.api.domain.invoice.InvoiceStreams
uses gw.api.locale.DisplayKey
uses gw.api.web.invoice.InvoicingOverrider
uses gw.api.web.policy.NewPolicyUtil
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.EntityStateException
uses gw.api.webservice.exception.SOAPSenderException
uses gw.pl.persistence.core.Bundle
uses gw.plugin.Plugins
uses gw.plugin.invoice.IPeriodicities
uses gw.webservice.policycenter.bc1000.entity.types.complex.InvoiceStreamOverrides
uses gw.webservice.policycenter.bc1000.entity.types.complex.IssuePolicyInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewAccountInfo
uses gw.webservice.util.WebserviceEntityLoader
uses typekey.Currency

@Export
enhancement IssuePolicyInfoEnhancement : IssuePolicyInfo {
  function executeIssuanceBI() : Issuance {
    return executedIssuanceBISupplier()
  }

  /**
   * Local {@link NewPlcyPeriodBI new PolicyPeriod Billing Instruction}
   *    initialization. Base {@link PlcyBillingInstruction} initialization
   *    occurs in {@link BillingInstructionInfo#execute}.
   * <p/>
   * Setting of the {@link NewPlcPeriodBI#setPolicyPaymentPlan PolicyPaymentPlan}
   * must be invoked <em>before</em> {@link #populateIssuanceInfo()} when prior
   * {@link PolicyPeriod} exists but not otherwise because of invoicing override
   * (ListBill) validation that occurs.
   */
  function initPolicyPeriodBIInternal(final bi: NewPlcyPeriodBI) {
    bi.PrimaryProducerCodeRoleEntry.ProducerCode = this.ProducerCode
    bi.PolicyPaymentPlan = this.PaymentPlan
  }

  private function createNewPolicyBIInternal() : Issuance {
    return NewPolicyUtil.createIssuance(
        findOwnerAccount(), createPolicyPeriod())
  }

  /**
   * Look-up existing owner {@link Account account} for this
   *    {@link IssuePolicyInfo issue}.
   *
   * If the currency of the {@code Account} is not the same as for the {@code
   * IssuePolicyInfo}, then find one or create a new one in the same {@link
   * MixedCurrencyAccountGroup} for the existing owner {@code account}.
   */
  function findOwnerAccount() : Account {
    var account = findExistingAccount(this.AccountNumber)
    if (account.Currency != CurrencyValue) {
      /* Get associated splinter account for different currency... */
      account = findOrCreateSplinterCurrencyAccount(account)
    }
    return account
  }

  private function findExistingAccount(accountNumber : String) : Account {
    var account = findAccount(accountNumber)
    if (account == null) {
      throw new BadIdentifierException(DisplayKey.get("BillingAPI.Error.AccountNotFound", accountNumber))
    }
    return account
  }

  private function findAccount(accountNumber : String) : Account {
    // The only time there's a new account in the bundle is when it was made by getOrCreateAccountForPreview()
    var tempAccountForPreview = gw.transaction.Transaction.getCurrent().getBeansByRootType(Account)
        .firstWhere(\ b -> b typeis Account && b.AccountNumber == accountNumber) as Account

    return tempAccountForPreview
        ?: Query.make(Account).compare("AccountNumber", Equals, accountNumber).select().AtMostOneRow
  }

  /**
   * Try to find a match using the publicID (assuming publicID is not null).
   * If a match is found, the Policy Period already exists. Otherwise, create the new Policy Period
   */
  function createPolicyPeriod() : PolicyPeriod {
    if (this.PCPolicyPublicID != null
        and this.findByPolicyPublicIDAndTerm(this.PCPolicyPublicID, this.TermNumber) != null) {
      throw new BadIdentifierException(
          DisplayKey.get("Webservice.Error.PolicyPeriodExists", this.PCPolicyPublicID))
    }
    var policy = new Policy(CurrencyValue)
    var period = new PolicyPeriod(policy.Currency)
    policy.addToPolicyPeriods( period )
    period = populateIssuanceInfo(period)
    return period
  }

  function setupAccountForPreview(accountNumber : String, newAccountInfo : PCNewAccountInfo) : Account {
    if(accountNumber == null){
      return null
    }
    var account = findAccount(accountNumber)
    validateNewAccountInfoForPreview(account, newAccountInfo)

    if(account != null and account.Currency != this.CurrencyValue){
      return findOrCreateSplinterCurrencyAccount(account)
    }

    if(account == null){
      if(newAccountInfo != null){
        account = newAccountInfo.toNewAccount(this.CurrencyValue, this.CurrentTransaction)
      } else { //Create account with system defaults (best guess at what the previews are going to look like)
        account = new Account(this.CurrentTransaction, this.CurrencyValue)
        account.AccountNumber = accountNumber
        account.BillingLevel = BillingLevel.TC_POLICYDESIGNATEDUNAPPLIED
        PCAccountInfoEnhancement.initializeAccountDefaults(account)
      }
    }

    return account
  }

  private function validateNewAccountInfoForPreview(account : Account, newAccountInfo : PCNewAccountInfo ) {
    if(account != null and newAccountInfo != null){
      throw new EntityStateException(DisplayKey.get("Webservice.Error.InvoicePreview.AccountInfo", newAccountInfo.AccountNumber))
    }
  }

  /**
   * WARNING: If the owner/payer accounts don't exist in BC yet, this method will attempt to create
   * invoice previews based off tmp accounts with the system's default settings. This might result
   * in invoices that are slightly different than the ones produced once the policy is actually
   * issued towards the real accounts.
   */
  function toIssuanceForPreview() : Issuance {
    return toIssuanceForPreview(null, null)
  }

  function toIssuanceForPreview(ownerAccount : PCNewAccountInfo, payerAccount : PCNewAccountInfo) : Issuance {
    //Preview Accounts need to be created prior to the creation of the policy period, if they doesn't exist yet
    setupAccountForPreview(this.AccountNumber, ownerAccount)
    setupAccountForPreview(this.AltBillingAccountNumber, payerAccount)
    return executedIssuanceBISupplier()
  }

  /**
   * The {@link ProducerCode} of record for the {@link PolicyPeriod} being
   * issued.
   */
  property get ProducerCode() : ProducerCode {
    if (this.ProducerCodeOfRecordId == null) {
      return null
    }
    var mainProducerCode = WebserviceEntityLoader
        .loadByPublicID<ProducerCode>(this.ProducerCodeOfRecordId, "ProducerCodeOfRecordId")
    if (mainProducerCode.Currency == this.CurrencyValue) {
      return mainProducerCode
    }
    final var producerCurrencyGroup = mainProducerCode.Producer.ProducerCurrencyGroup
    if (producerCurrencyGroup != null) {
      // look in splinter Producer for Currency ProducerCodes...
      final var splinterCode = Query.make(entity.ProducerCode)
          .compare("Code", Equals, mainProducerCode.Code)
          .subselect("Producer", CompareIn, Query.make(ProducerCurrencyGroup)
              .compare("CurrencyInGroup", Equals, this.CurrencyValue)
              .compare("ForeignEntity", Equals, producerCurrencyGroup),
              "Owner")
          .select().AtMostOneRow
      if (splinterCode != null) {
        return splinterCode
      }
    }
    throw new BadIdentifierException(
        DisplayKey.get("BillingAPI.Error.ProducerCodeForCurrencyDoesNotExist",
            this.ProducerCodeOfRecordId, this.CurrencyValue))
  }

  function populateIssuanceInfo(policyPeriod : PolicyPeriod) : PolicyPeriod {
    if (this.SpecialHandling != null) {
      throw new SOAPSenderException(DisplayKey.get("BillingAPI.Error.SpecialHandlingSetWhenCreatingNewPolicyPeriod"))
    }
    this.populateChangeInfo(policyPeriod)
    policyPeriod.BillingMethod = PolicyPeriodBillingMethod.get(this.BillingMethodCode)
    final var overridingPayerAccount = findOverridingPayerAccount()
    if (overridingPayerAccount != policyPeriod.OverridingPayerAccount) {
      policyPeriod.updateWith(
          new InvoicingOverrider().withOverridingPayerAccount(overridingPayerAccount))
    }

    updateInvoiceStreamOrOverrides(policyPeriod)

    policyPeriod.PolicyNumber = this.PolicyNumber
    policyPeriod.Policy.PCPublicID = this.PCPolicyPublicID
    policyPeriod.Policy.LOBCode = typekey.LOBCode.get(this.ProductCode)
    policyPeriod.BoundDate = this.ModelDate.toCalendar().Time
    policyPeriod.AssignedRisk = this.AssignedRisk
    policyPeriod.UWCompany = typekey.UWCompany.get(this.UWCompanyCode)
    policyPeriod.EligibleForFullPayDiscount = false
    policyPeriod.HoldInvoicingWhenDelinquent = false
    policyPeriod.ConfirmationNotificationState = TC_NOTIFYUPONSUFFICIENTPAYMENT
    policyPeriod.TermConfirmed = this.TermConfirmed

    if (this.HasScheduledFinalAudit) {
      policyPeriod.scheduleFinalAudit()
    }
    return policyPeriod
  }


  private function updateInvoiceStreamOrOverrides(policyPeriod: PolicyPeriod) {
    if (this.DefaultPolicyInvoiceStreamOverrides != null) {
      if (policyPeriod.BillingMethod != PolicyPeriodBillingMethod.TC_DIRECTBILL) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.DefaultPolicyInvoiceStreamOverridesApplicableOnlyToDirectBill"))
      }
      if (!policyPeriod.Policy.New) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.DefaultPolicyInvoiceStreamOverridesNotApplicableToExistingPolicy"))
      }
      var payer = policyPeriod.OverridingPayerAccount?: findOwnerAccount()
      if (!payerIsAccountAndUsesPolicyLevelBilling(payer)) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.DefaultPolicyInvoiceStreamOverridesNotApplicableToAccountLevelBilling"))
      }
      registerDefaultPolicyInvoiceStreamOverrides(policyPeriod.Policy, payer, this.DefaultPolicyInvoiceStreamOverrides.$TypeInstance)
    } else {
      final var overridingInvoiceStream =
          policyPeriod.AgencyBill
              ? null
              : getOverridingInvoiceStream(policyPeriod)
      if (overridingInvoiceStream != policyPeriod.OverridingInvoiceStream) {
        policyPeriod.updateWith(
            new InvoicingOverrider().withOverridingInvoiceStream(overridingInvoiceStream))
      }
    }
  }

  private function payerIsAccountAndUsesPolicyLevelBilling(payer : InvoicePayer) : boolean {
    return payer.isAccount() &&
        (payer.isPolicyLevelBillingWithDefaultUnapplied() or payer.isPolicyLevelBillingWithDesignatedUnapplied())
  }

  private function registerDefaultPolicyInvoiceStreamOverrides(policy : Policy, payer : InvoicePayer, defaultPolicyInvoiceStreamOverrides : InvoiceStreamOverrides) {
    if (defaultPolicyInvoiceStreamOverrides.PaymentInstrumentID != null) {
      var paymentInstrument = WebserviceEntityLoader.loadByPublicID<PaymentInstrument>(
          defaultPolicyInvoiceStreamOverrides.PaymentInstrumentID,
          "DefaultPolicyInvoiceStreamOverrides.PaymentInstrumentID")
      policy.updateOverridingPaymentInstrumentOnDefaultInvoiceStream(paymentInstrument)
    }
    if (defaultPolicyInvoiceStreamOverrides.Description != null and !defaultPolicyInvoiceStreamOverrides.Description.isEmpty()) {
      policy.updateDescriptionOnDefaultInvoiceStream(defaultPolicyInvoiceStreamOverrides.Description)
    }

    var invoiceStreamPeriodicity = InvoiceStreams.invoiceStreamPeriodicityFor(payer, this.PaymentPlan)
    if(InvoiceStreamUtil.isInvoiceDayOverridden(defaultPolicyInvoiceStreamOverrides, invoiceStreamPeriodicity)) {
      policy.updateOverridingAnchorDatesOnDefaultInvoiceStream(
          InvoiceStreamUtil.getOverridingAnchorDates(defaultPolicyInvoiceStreamOverrides, invoiceStreamPeriodicity))
    }
    if (defaultPolicyInvoiceStreamOverrides.LeadTimeDayCount != null) {
      if(defaultPolicyInvoiceStreamOverrides.LeadTimeDayCount < 0) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.IllegalFieldValue", "LeadTimeDayCount"))
      }
      policy.updateOverridingLeadTimeOnDefaultInvoiceStream(defaultPolicyInvoiceStreamOverrides.LeadTimeDayCount)
    }
    if (defaultPolicyInvoiceStreamOverrides.DueDateBilling != null) {
        policy.updateBillDateOrDueDateBillingOnDefaultInvoiceStream(
            defaultPolicyInvoiceStreamOverrides.DueDateBilling ?
                BillDateOrDueDateBilling.TC_DUEDATEBILLING :
                BillDateOrDueDateBilling.TC_BILLDATEBILLING
        );
    }
  }

  property get CurrencyValue() : Currency {
    return Currency.get(this.Currency)
  }

  /**
   * The {@link PaymentPlan} for the {@link PolicyPeriod} being issued.
   */
  property get PaymentPlan() : PaymentPlan {
    var paymentPlan = WebserviceEntityLoader
        .loadByPublicID<PaymentPlan>(this.PaymentPlanPublicId, "PaymentPlanPublicId")
    if (!paymentPlan.supportsCurrency(CurrencyValue)) {
      throw new BadIdentifierException(
          DisplayKey.get("Webservice.Error.PaymentPlanBadCurrency",
              this.PaymentPlanPublicId, paymentPlan.Currencies.join(", "), CurrencyValue))
    }
    return paymentPlan
  }

  private function findOverridingPayerAccount() : Account {
    if (this.AltBillingAccountNumber == null) {
      return null
    }

    //Invoice preview methods should make sure that the alt billing account exists
    //by the time the execution flow gets here. The find is bundle sensitive.
    var payerAccount = findExistingAccount(this.AltBillingAccountNumber)

    if (payerAccount.Currency != CurrencyValue) {
      /* Get associated splinter account for different currency... */
      payerAccount = findOrCreateSplinterCurrencyAccount(payerAccount)
    }
    return payerAccount
  }

  private function getOverridingInvoiceStream(policyPeriod : PolicyPeriod) : InvoiceStream {
    if (this.InvoiceStreamId != null) {
      return findInvoiceStreamWithPublicId(this.InvoiceStreamId)
    }
    if (this.NewInvoiceStream != null) {
      final var payer = policyPeriod.OverridingPayerAccount ?: findOwnerAccount()
      return this.NewInvoiceStream.$TypeInstance.createInvoiceStreamFor(payer, policyPeriod.Bundle)
    }
    if (policyPeriod.isListBill()) {
      var periodicity = this.PaymentPlan.Periodicity
      // If the PaymentPlan's periodicity is Quarterly, for example, the InvoiceStream that we find or create should be monthly
      var invoiceStreamPeriodicity = Plugins.get(IPeriodicities).isMultipleOfMonthly(periodicity) ? Periodicity.TC_MONTHLY : periodicity
      return policyPeriod.OverridingPayerAccount.InvoiceStreams.firstWhere(\ stream -> stream.Periodicity == invoiceStreamPeriodicity)
          ?: InvoiceStreamFactory.createInvoiceStreamFor(policyPeriod.OverridingPayerAccount, invoiceStreamPeriodicity)
    }
    return null
  }

  private function findInvoiceStreamWithPublicId(publicID : String) : InvoiceStream {
    return WebserviceEntityLoader.loadInvoiceStream(publicID)
  }

  /**
   * Look up or create a splinter currency account for the specified account.
   *
   * The currency for the account is that specified on this {@link IssuePolicyInfo}.
   *
   * @param account the {@link Account} whose {@link Currency} is different than that
   *                of the policy to be issued by this {@code IssuePolicyInfo}.
   * @return The splinter currency {@code Account}.
   */
  private function findOrCreateSplinterCurrencyAccount(final account : Account) : Account {
    var splinterAccount : Account
    if (account.AccountCurrencyGroup == null) {
      splinterAccount = BillingAPI.createAccountForCurrency(account, CurrencyValue)
    } else {
      splinterAccount = findExistingAccountForCurrency(account.AccountCurrencyGroup)
      if (splinterAccount == null) {
        splinterAccount = BillingAPI.createAccountForCurrency(account, CurrencyValue)
      }
    }
    return splinterAccount
  }

  /**
   * Find and return existing sibling account for the specified account group
   *    with the currency value for this info'.
   */
  private function findExistingAccountForCurrency(accountGroup : MixedCurrencyAccountGroup) : Account {
    return BillingAPI.findExistingAccountForCurrency(accountGroup, CurrencyValue)
  }

  function executedIssuanceBISupplier(): Issuance {
    var bi = createNewPolicyBIInternal()
    initPolicyPeriodBIInternal(bi)
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }

  protected property get CurrentTransaction() : Bundle {
    return gw.transaction.Transaction.Current
  }
}
