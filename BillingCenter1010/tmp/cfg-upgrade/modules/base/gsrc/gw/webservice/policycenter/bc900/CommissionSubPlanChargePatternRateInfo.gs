package gw.webservice.policycenter.bc900

uses gw.xml.ws.annotation.WsiExportable

uses java.math.BigDecimal

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc900/CommissionSubPlanChargePatternRateInfo" )
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
final class CommissionSubPlanChargePatternRateInfo {
  var _rate              : BigDecimal          as Rate
  var _role              : typekey.PolicyRole  as Role
  var _chargePatternCode : String              as ChargePatternCode

  construct() {}

  construct(commissionSubPlanChargePatternRate : CommissionSubPlanChargePatternRate) {
    this.Rate               = commissionSubPlanChargePatternRate.Rate
    this.Role               = commissionSubPlanChargePatternRate.Role
    this.ChargePatternCode  = commissionSubPlanChargePatternRate.ChargePattern.ChargeCode
  }


}