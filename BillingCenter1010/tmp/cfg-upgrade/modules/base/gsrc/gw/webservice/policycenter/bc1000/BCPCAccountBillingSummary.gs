package gw.webservice.policycenter.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.web.account.AccountSummaryFinancialsHelper
uses gw.web.account.AccountSummaryHelper
uses gw.xml.ws.annotation.WsiExportable
uses typekey.Currency

/**
 * A summary object that provides the billing summary for an account designed
 * explicitly for use with the PolicyCenter product.
 */
@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/BCPCAccountBillingSummary" )
@Export
final class BCPCAccountBillingSummary {
  var _accountNumber : String as AccountNumber
  var _accountCurrencyGroupOwner : String as AccountCurrencyGroupOwner
  var _accountName : String as AccountName
  var _accountNameKanji : String as AccountNameKanji
  var _currency : Currency as Currency
  var _billedOutstandingTotal : MonetaryAmount as BilledOutstandingTotal
  var _billedOutstandingCurrent : MonetaryAmount as BilledOutstandingCurrent
  var _billedOutstandingPastDue : MonetaryAmount as BilledOutstandingPastDue
  var _paid : MonetaryAmount as Paid
  var _writtenOff : MonetaryAmount as WrittenOff

  var _unbilledTotal : MonetaryAmount as UnbilledTotal
  var _unappliedFundsTotal : MonetaryAmount as UnappliedFundsTotal

  var _collateralRequirement : MonetaryAmount as CollateralRequirement
  var _collateralHeld : MonetaryAmount as CollateralHeld
  var _collateralChargesUnbilled : MonetaryAmount as CollateralChargesUnbilled
  var _collateralChargesBilled : MonetaryAmount as CollateralChargesBilled
  var _collateralChargesPastDue : MonetaryAmount as CollateralChargesPastDue

  var _delinquent : boolean as Delinquent

  var _billingLevel : String as BillingLevel
  var _billingSettings : AccountBillingSettings as BillingSettings
  var _primaryPayer : ContactSummary as PrimaryPayer

  var _nextInvoicesDueDate : Date as NextInvoicesDueDate
  var _nextInvoicesDue : BCPCAccountInvoice[] as NextInvoicesDue
  var _lastPaymentReceived : BCPCAccountPayment as LastPaymentReceived

  var _delinquenciesLast12Months : int as DelinquenciesLast12Months

  construct() {}

  internal construct(account : Account) {
    _currency = account.Currency

    _accountNumber = account.AccountNumber
    var currencyGroup = account.AccountCurrencyGroup
    _accountCurrencyGroupOwner = currencyGroup != null ? currencyGroup.MainAccount.AccountNumber : account.AccountNumber
    _accountName = account.AccountName
    _accountNameKanji = account.AccountNameKanji

    final var accountBalances = new AccountSummaryFinancialsHelper(account)
    _billedOutstandingTotal = accountBalances.OutstandingAmount
    _billedOutstandingCurrent = accountBalances.BilledAmount
    _billedOutstandingPastDue = accountBalances.DelinquentAmount
    _unbilledTotal = accountBalances.UnbilledAmount
    _paid = accountBalances.PaidAmount
    _writtenOff = accountBalances.WrittenOffAmount

    var allNextDueInvoicesWithSameDueDate = account.AllNextDueInvoicesWithSameDueDate
    _nextInvoicesDueDate = allNextDueInvoicesWithSameDueDate.first()?.DueDate
    _nextInvoicesDue = allNextDueInvoicesWithSameDueDate
        .map(\invoice ->
            new BCPCAccountInvoice() {
              :Amount = invoice.AmountDue,
              :InvoiceID = invoice.PublicID
            }
        )
    _lastPaymentReceived = new BCPCAccountPayment() {
      :Date = accountBalances.LastPaymentReceived.ReceivedDate,
      :Amount = accountBalances.LastPaymentReceived.Amount,
      :MoneyReceivedID = accountBalances.LastPaymentReceived.PublicID
    }

    _delinquenciesLast12Months = new AccountSummaryHelper(account).RecentDelinquenciesCount

    _unappliedFundsTotal = account.TotalUnappliedAmount

    _collateralRequirement = account.Collateral.TotalRequirementValue
    _collateralHeld = account.Collateral.TotalCollateralValue
    _collateralChargesUnbilled = account.Collateral.UnbilledAmount
    _collateralChargesBilled = account.Collateral.BilledAmount
    _collateralChargesPastDue = account.Collateral.DueAmount

    _delinquent = account.hasActiveDelinquenciesOutOfGracePeriod()
    _billingLevel = account.BillingLevel.Code
    final var primaryPayer = account.PrimaryPayer.Contact
    _primaryPayer = new ContactSummary() {
        :Name = primaryPayer.DisplayName,
        :Address = primaryPayer.PrimaryAddress.DisplayName,
        :Phone = primaryPayer.PrimaryPhoneValue
    }

    _billingSettings = new AccountBillingSettings(account)
  }
}



