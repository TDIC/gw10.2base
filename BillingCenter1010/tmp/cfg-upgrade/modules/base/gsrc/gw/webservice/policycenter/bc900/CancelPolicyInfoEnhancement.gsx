package gw.webservice.policycenter.bc900

uses gw.webservice.policycenter.bc900.entity.types.complex.CancelPolicyInfo

uses java.lang.*

/**
 * Defines behavior for the 900 API version of the WSDL entity that specifies
 * the data used to create a {@link entity.Cancellation Cancellation} billing
 * instruction.
 */
@Export
@gw.lang.Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement CancelPolicyInfoEnhancement : CancelPolicyInfo {
  function executeCancellationBI(): BillingInstruction {
    return executedCancellationBISupplier()
  }

  /**
   * @deprecated Since 9.0.1. Use {@link #executeCancellationBI()} instead.
   */
  @java.lang.Deprecated
  function execute() : String {
    return executeCancellationBI().PublicID
  }

  private function createCancellationBI() : Cancellation {
    final var policyPeriod = this.findPolicyPeriod()
    final var cancellation = new Cancellation(policyPeriod.Currency)
    cancellation.AssociatedPolicyPeriod = policyPeriod
    cancellation.CancellationType = typekey.CancellationType.get(this.CancellationType)
    cancellation.CancellationReason = this.CancellationReason
    // cancellation.EffectiveDate this is auto set to the posted date
    if (this.HasScheduledFinalAudit) {
      cancellation.SpecialHandling = TC_HOLDFORAUDITALL
    }
    return cancellation
  }

  function toCancellationForPreview() : Cancellation {
    final var bi = createCancellationBI()
    this.initializeBillingInstruction(bi)
    return bi
  }

  function executedCancellationBISupplier(): Cancellation {
    var bi = createCancellationBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}