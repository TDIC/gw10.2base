package gw.webservice.policycenter.bc1000

uses com.google.common.collect.Lists
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.BadIdentifierException
uses gw.pl.persistence.core.Bundle
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCAccountInfo
uses typekey.Currency

@Export
enhancement PCAccountInfoEnhancement: PCAccountInfo {
  function toAccount(bundle: Bundle): Account {
    var account = Query.make(entity.Account).compare("AccountNumber", Equals, this.AccountNumber).select().AtMostOneRow
    if (account == null){
      throw new BadIdentifierException(DisplayKey.get("BillingAPI.Error.AccountNotFound", this.AccountNumber))
    }
    var accounts = {account}
    //find siblings
    var group = account.AccountCurrencyGroup
    if (group != null) {
      accounts = Lists.newArrayList(group.findAccounts())
    }
    var parentAccount: Account
    for (sibling in accounts) {
      if (sibling.AccountNumber == account.AccountNumber) {
        parentAccount = sibling
      }
      sibling = bundle.add(sibling)
      sibling.AccountName = this.AccountName
      sibling.AccountNameKanji = this.AccountNameKanji
      sibling.ServiceTier = CustomerServiceTier.get(this.CustomerServiceTier)
      for (contact in this.BillingContacts) {
        var current = sibling.Contacts.firstWhere(\a -> a.Contact.AddressBookUID == contact.AddressBookUID)
        if (current == null) {
          var billingContact = contact.$TypeInstance.toAccountContact(sibling, {AccountRole.TC_ACCOUNTSPAYABLE})
          sibling.addToContacts(billingContact)
        } else {
          current = bundle.add(current)
          addContactRole(current, AccountRole.TC_ACCOUNTSPAYABLE)
        }
      }
      if (this.InsuredContact == null) {
        continue
      }
      var existingContact = sibling.Contacts.firstWhere(\a -> a.Contact.AddressBookUID == this.InsuredContact.AddressBookUID)
      if (existingContact == null) {
        var insured = this.InsuredContact.$TypeInstance.toAccountContact(sibling, AccountHolderRoles)
        sibling.addToContacts(insured)
      } else {
        existingContact = bundle.add(existingContact)
        addContactRole(existingContact, AccountRole.TC_INSURED)
        if (this.InsuredIsBilling) {
          addContactRole(existingContact, AccountRole.TC_ACCOUNTSPAYABLE)
        }
      }
    }
    return parentAccount
  }

  private function addContactRole(contact: AccountContact, accountRole: AccountRole) {
    if (not contact.Roles.hasMatch(\r -> r.Role == accountRole)) {
      var contactRole = new AccountContactRole(contact.Bundle)
      contactRole.Role = accountRole
      contactRole.AccountContact = contact
      contact.addToRoles(contactRole)
    }
  }

  /**
   * Set the default {@link Account} attributes on creation.
   */
  static internal function initializeAccountDefaults(newAccount: Account) {
    newAccount.InvoiceDayOfMonth = 15
    newAccount.InvoiceDeliveryType = TC_MAIL
    newAccount.BillingPlan =
        BillingPlan.finder.findFirstActivePlan(BillingPlan, newAccount.Currency)
    newAccount.DelinquencyPlan =
        DelinquencyPlan.finder.findFirstActivePlan(DelinquencyPlan, newAccount.Currency)
  }

  private property get AccountHolderRoles(): AccountRole[] {
    return this.InsuredIsBilling
        ? {AccountRole.TC_INSURED, AccountRole.TC_ACCOUNTSPAYABLE}
        : {AccountRole.TC_INSURED}
  }
}

