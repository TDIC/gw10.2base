package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.PremiumReportInfo

/**
 * Defines behavior for the 910 API version of the WSDL entity that specifies
 * the data used to create a {@link PremiumReportBI PremiumReport}
 * billing instruction.
 */
@Export
enhancement PremiumReportInfoEnhancement : PremiumReportInfo {
  function executePremiumReportBI(): BillingInstruction {
    return executedPremiumReportBISupplier()
  }

  /**
   * @deprecated Since 9.0.1. Use {@link #executePremiumReportBI()} instead.
   */
  @java.lang.Deprecated
  function execute() : String{
    return executePremiumReportBI().PublicID
  }

  private function createPremiumReportBI() : PremiumReportBI {
    final var policyPeriod = this.findPolicyPeriod()
    final var bi = new PremiumReportBI(policyPeriod.Currency)
    bi.AssociatedPolicyPeriod = policyPeriod
    bi.PeriodStartDate = this.AuditPeriodStartDate.toCalendar().Time
    bi.PeriodEndDate = this.AuditPeriodEndDate.toCalendar().Time
    bi.PaymentReceived = this.PaymentReceived
    return bi
  }

  function toPremiumReportForPreview() : PremiumReportBI {
    final var bi = createPremiumReportBI()
    this.initializeBillingInstruction(bi)
    return bi
  }

  function executedPremiumReportBISupplier(): PremiumReportBI {
    var bi = createPremiumReportBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
