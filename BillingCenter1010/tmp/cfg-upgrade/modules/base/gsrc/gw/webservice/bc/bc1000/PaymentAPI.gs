package gw.webservice.bc.bc1000

uses entity.Activity
uses entity.BaseDistItem
uses entity.Invoice
uses gw.api.locale.DisplayKey
uses gw.api.system.BCConfigParameters
uses gw.api.system.BCLoggerCategory
uses gw.api.web.accounting.UIWriteOffCreation
uses gw.api.web.accounting.WriteOffFactory
uses gw.api.web.admin.ActivityPatternsUtil
uses gw.api.web.payment.DirectBillPaymentFactory
uses gw.api.web.producer.agencybill.AgencyBillMoneySetupFactory
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.DataConversionException
uses gw.api.webservice.exception.EntityStateException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.api.webservice.exception.SOAPException
uses gw.pl.currency.MonetaryAmount
uses gw.pl.persistence.core.Bundle
uses gw.plugin.Plugins
uses gw.plugin.util.CurrentUserUtil
uses gw.transaction.Transaction
uses gw.webservice.util.WebserviceEntityLoader
uses gw.webservice.util.WebservicePreconditions
uses gw.xml.ws.annotation.WsiWebService

@WsiWebService("http://guidewire.com/bc/ws/gw/webservice/bc/bc1000/PaymentAPI")
@Export
class PaymentAPI {

  /**
   * API method that allows to edit existing batch payment entity.
   * Based on entity limitations, properties that can be changed are only amount or batch payment entries
   * @param batchPaymentDTO object from which changes will be applied to batchPayment
   */
  @Throws(RequiredFieldException, "if batch payment is null or batch number is null")
  @Throws(SOAPException, "if currency has been changed")
  @Throws(EntityStateException, "If a batch payment (that corresponds to a provided batchPaymentNumber) is in state 'Posted' or 'Reversed'.")
  function editBatchPayment(batchPaymentDTO: BatchPaymentDTO) {
    WebservicePreconditions.notNull(batchPaymentDTO, "batchPaymentDTO")
    WebservicePreconditions.notNull(batchPaymentDTO.BatchNumber, "batchNumber")

    var batchPayment = WebserviceEntityLoader.loadBatchByBatchNumber(batchPaymentDTO.BatchNumber)
    WebservicePreconditions.checkArgument(batchPayment.Currency == batchPaymentDTO.Amount.Currency, DisplayKey.get("PaymentAPI.Error.CurrencyDoNotMatch"))

    if (!batchPayment.isEditable()) {
      throw new EntityStateException(
          DisplayKey.get("PaymentAPI.Error.BatchPaymentHasNotAppropriateStateForEdition")
      )
    }

    gw.transaction.Transaction.runWithNewBundle(
        \bundle -> {
          batchPayment = bundle.add(batchPayment)
          mergeBatchPayment(batchPayment, batchPaymentDTO)
        }
    )
  }

  @Throws(RequiredFieldException, "If the batchNumber is null.")
  @Throws(BadIdentifierException, "If there are no batches with the given batchNumber.")
  @Throws(EntityStateException, "If a batch payment(that corresponds to a provided batchNumber) is in state 'Posted' or 'Reversed'.")
  function deleteBatchPayment(batchNumber: String) {
    var batchPayment = WebserviceEntityLoader.loadBatchByBatchNumber(batchNumber)

    if (!batchPayment.isEditable()) {
      throw new EntityStateException(
          DisplayKey.get("PaymentAPI.Error.BatchPaymentHasNotAppropriateStateForDeletion")
      )
    }

    Transaction.runWithNewBundle(\bundle -> {
      bundle.add(batchPayment)
      batchPayment.remove()
    })

  }

  @Throws(RequiredFieldException, "If the batchNumber is null.")
  @Throws(BadIdentifierException, "If there are no Batches with the given batchNumber.")
  function getBatchPayment(batchPaymentNumber: String): BatchPaymentDetailsDTO {
    return BatchPayments.toDataTransferObject(
        WebserviceEntityLoader.loadBatchByBatchNumber(batchPaymentNumber)
    )
  }

  @Throws(RequiredFieldException, "if Amount is null or Payments array is null")
  @Throws(DataConversionException, "if batchPayment is invalid or batch number is not null")
  @Throws(SOAPException, "if batchPayment is valid entity but is not permitted to be submit")
  function createBatchPayment(batchPaymentDTO: BatchPaymentDTO): String {
    WebservicePreconditions.isNull(batchPaymentDTO.BatchNumber, "BatchNumber")
    WebservicePreconditions.notNull(batchPaymentDTO.Amount, "Amount")
    WebservicePreconditions.notNull(batchPaymentDTO.Payments, "Payments")

    var batch: BatchPayment
    gw.transaction.Transaction.runWithNewBundle(
        \bundle -> {
          batch = createBatchPayment(bundle, batchPaymentDTO)
        }
    )
    return batch.BatchNumber
  }

  /**
   * Create a new {@link BatchPayment} with the values that's specified in the {@link BatchPaymentDTO}, and
   * post it asynchronously (BatchStatus is set to BatchPaymentsStatus.TC_POSTING).
   *
   * @param BatchPaymentDTO the new batch payment DTO.
   * @return The BatchNumber of {@link BatchPayment} that is created.
   *
   **/
  @Throws(RequiredFieldException, "if Amount is null or Payments array is null")
  @Throws(DataConversionException, "if batchPayment is invalid")
  @Throws(SOAPException, "if batchPayment is valid entity but is not permitted to be submit")
  function createAndPostBatchPayment(batchPaymentDTO: BatchPaymentDTO): String {
    WebservicePreconditions.isNull(batchPaymentDTO.BatchNumber, "BatchNumber")
    WebservicePreconditions.notNull(batchPaymentDTO.Amount, "Amount")
    WebservicePreconditions.notNull(batchPaymentDTO.Payments, "Payments")

    var batch: BatchPayment
    gw.transaction.Transaction.runWithNewBundle(
        \bundle -> {
          batch = createBatchPayment(bundle, batchPaymentDTO)
          batch.postAsync()
        }
    )
    return batch.BatchNumber
  }

  /**
   * Makes a payment to an Account
   * <p/>
   * @param directbillPaymentReceipt A PaymentReceiptRecord of subtype DirectBillMoneyDetails
   * @return payment The PublicID of the newly created DirectBillMoneyRcvd
   */
  @Throws(RequiredFieldException, "If the directbillPaymentReceipt fields AccountID, PaymentInstrumentRecord, or Amount is null")
  function makeDirectBillPayment(directbillPaymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord): String {
    validateDirectBillPaymentReceipt(directbillPaymentReceipt)
    return makeDirectBillPaymentToPolicyPeriod(directbillPaymentReceipt, null)
  }

  /**
   * Makes a payment to an Account or to a specific Policy on an Account. If no policyPeriodID is passed in, the payment will be distributed
   * to the children policies of the Account using standard payment distribution.
   * <p/>
   * If a non-null policyPeriodID parameter is passed in, the payment will be targeted at that specific PolicyPeriod.
   * <p/>
   * @param directbillPaymentReceipt A PaymentReceiptRecord of subtype DirectBillMoneyDetails
   * @param policyPeriodID The PolicyPeriod to make the payment to
   * @return directBillMoneyRcvdID The PublicID of the payment that is created
   */
  @Throws(RequiredFieldException, "If the directbillPaymentReceipt fields AccountID, PaymentInstrumentRecord, or Amount is null")
  function makeDirectBillPaymentToPolicyPeriod(directbillPaymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord, policyPeriodID: String): String {
    validateDirectBillPaymentReceipt(directbillPaymentReceipt)
    var directBillMoneyRcvd: DirectBillMoneyRcvd
    Transaction.runWithNewBundle(\ bundle -> {
      var account = bundle.add(WebserviceEntityLoader.loadAccount(directbillPaymentReceipt.AccountID))
      var unappliedFund = account.DefaultUnappliedFund
      var policyPeriod = null as PolicyPeriod
      var isTargetedToPolicy = false
      if (policyPeriodID != null and account.PolicyLevelBillingWithDesignatedUnapplied) {
        isTargetedToPolicy = true
        policyPeriod = bundle.add(WebserviceEntityLoader.loadPolicyPeriod(policyPeriodID))
        unappliedFund = policyPeriod.Policy.getDesignatedUnappliedFund(account)
        if (unappliedFund == null) {
          // The designated UnappliedFund for this policy with the given account owner does not exist
          isTargetedToPolicy = false
          unappliedFund = account.DefaultUnappliedFund
        }
      }
      var directBillMoneyDetails = PaymentReceipts.toEntity(directbillPaymentReceipt, bundle)
      directBillMoneyRcvd = DirectBillPaymentFactory.createAndExecuteMoneyReceivedFromPaymentReceipt(unappliedFund, directBillMoneyDetails as DirectBillMoneyDetails, directbillPaymentReceipt.ReceivedDate, directbillPaymentReceipt.Description)
      if (isTargetedToPolicy) {
        directBillMoneyRcvd.PolicyPeriod = policyPeriod
      }
    })
    return directBillMoneyRcvd.PublicID
  }

  /**
   * Makes a SuspensePayment to an Account or Policy. Exactly one of the AccountNumber, PolicyNumber, and OfferNumber fields on the passed-in SuspensePayment
   * must be non-null.
   *
   * @param suspensePayment The SuspensePayment being made (a PaymentReceiptRecord of subtype SuspensePayment)
   * @return SuspensePaymentID The PublicID of the payment that is created
   */
  @Throws(DataConversionException, "If more than one of AccountNumber, PolicyNumber, and OfferNumber are all non-null")
  function makeSuspensePayment(suspensePayment: gw.webservice.bc.bc1000.PaymentReceiptRecord): String {
    WebservicePreconditions.notNull(suspensePayment, "suspensePayment")
    validateSuspensePayment(suspensePayment)
    var suspensePaymentEntity: SuspensePayment
    Transaction.runWithNewBundle(\ bundle -> {
      suspensePaymentEntity = PaymentReceipts.toEntity(suspensePayment, bundle) as SuspensePayment
      suspensePaymentEntity.createSuspensePayment()
    })
    return suspensePaymentEntity.PublicID;
  }

  /**
   * Reverses a Suspense Payment
   *
   * @param suspensePaymentID The id of the Suspense Payment to be reversed
   */
  @Throws(BadIdentifierException, "If suspensePaymentID does not correspond to a valid payment")
  function reverseSuspensePayment(suspensePaymentID: String) {
    WebservicePreconditions.notNull(suspensePaymentID, "suspensePaymentID")
    var suspensePayment = WebserviceEntityLoader.loadByPublicID<SuspensePayment>(suspensePaymentID)

    Transaction.runWithNewBundle(\ bundle -> {
      suspensePayment = bundle.add(suspensePayment)

      var suspensePayments  = new ArrayList<SuspensePayment>()
      suspensePayments.add(suspensePayment)

      new gw.api.web.payment.SuspensePaymentUtil().reverse(suspensePayments)

    }
    )
  }

  private function validateSuspensePayment(suspensePayment: gw.webservice.bc.bc1000.PaymentReceiptRecord) {
    var numNonNull = 0
    if (suspensePayment.AccountNumber != null) {
      numNonNull++
    }
    if (suspensePayment.PolicyNumber != null) {
      numNonNull++
    }
    if (suspensePayment.OfferNumber != null) {
      numNonNull++
    }
    if (numNonNull > 1) {
      throw new DataConversionException(DisplayKey.get('PaymentAPI.Error.SuspensePaymentMoreThanOneNonNull'))
    }
  }

  /**
   * If an account has excess funds to eliminate in part or in whole, the funds are an account adjustment. BillingCenter always records the
   * account adjustment as a BillingCenter transaction, not a charge. If the adjustment amount is greater than the designated/default unapplied fund balance of the
   * given account, only the unapplied balance adjusts. An account adjustment functions like a Negative Writeoff.
   *<P>
   * An example of how this method is used: A dummy account has been set up to temporarily hold mis-allocated funds that are being
   * stored in BillingCenter for the sole purpose of recording them in the General Ledger system.  In this case, funds will be
   * transferred to the dummy accounts, integration code will post the information to the General Ledger system, and
   * finally the funds will be adjusted using this API method.
   *<P>
   * @param accountPublicID  A publicID of an existing account
   * @param adjustment Adjustment amount (must be less than zero)
   * @param policyPeriodPublicID Optional, if non-null, BC tries to adjust the designated unapplied fund for this policy on this account.
   *                             <P>Throws an exception if this doesn't exist
   *                             <P>If this is null, then BC tries to adjust the default unapplied fund of the given account.
   * @return Actual adjustment made, which will be the lesser of the adjustment amount and the designated/default unapplied fund balance on the account.
   */
  @Throws(RequiredFieldException, "If accountPublicID is null")
  @Throws(BadIdentifierException, "If there is no Account with PublicID matching accountPublicID")
  @Throws(BadIdentifierException, "If there is no PolicyPeriod with PublicID matching policyPeriodPublicID")
  @Throws(SOAPException, "If the adjustment amount is non-negative, if an unappliedfund can't be found for the policyperiod's policy on the account, or if the unappliedfund balance is not greater than zero ")
  function makeAccountAdjustment(accountPublicID: String, adjustmentAmount: MonetaryAmount, policyPeriodPublicID: String): MonetaryAmount {
    if (!adjustmentAmount.IsNegative) {
      throw new SOAPException(DisplayKey.get('PaymentAPI.Error.AdjustmentAmountMustBeNegative', adjustmentAmount))
    }
    var account = WebserviceEntityLoader.loadAccount(accountPublicID)
    var unappliedFund = account.DefaultUnappliedFund
    if (policyPeriodPublicID != null) {
      unappliedFund = getUnappliedFundFor(policyPeriodPublicID, account)
    }
    if (!unappliedFund.Balance.IsPositive) {
      throw new SOAPException(DisplayKey.get('Webservice.Error.UnappliedFundBalanceMustBePositive', unappliedFund, unappliedFund.Balance))
    }
    var accountAdjustment: AccountAdjustment
    Transaction.runWithNewBundle(\ bundle -> {
      unappliedFund = bundle.add(unappliedFund)
      accountAdjustment = unappliedFund.makeAdjustment(adjustmentAmount)
    })
    return accountAdjustment.Amount
  }

  /**
   * Reverses a Direct Bill payment
   *
   * @param directBillMoneyRcvdID The id of the DirectBillMoneyRcvd to be reversed
   * @param paymentReversalReason the reason for the reversal
   */
  @Throws(BadIdentifierException, "If directBillMoneyRcvdID does not correspond to a valid payment")
  @Throws(EntityStateException, "If the DirectBillMoneyRcvd is frozen or partially frozen")
  function reverseDirectBillPayment(directBillMoneyRcvdID: String, paymentReversalReason: PaymentReversalReason) {
    WebservicePreconditions.notNull(directBillMoneyRcvdID, "directBillMoneyRcvdID")
    var dbmr = WebserviceEntityLoader.loadByPublicID<DirectBillMoneyRcvd>(directBillMoneyRcvdID)
    if (dbmr.Frozen or dbmr.DirectBillPayment?.hasFrozenDistItem()) {
      throw new EntityStateException(
          DisplayKey.get('PaymentAPI.Error.ReversalNotPermittedOnFrozenDirectBillMoney')
      )
    }
    Transaction.runWithNewBundle(\ bundle -> {
        dbmr = bundle.add(dbmr)
        if (dbmr.BaseDist != null) {
          dbmr.BaseDist.reverse(paymentReversalReason)
        } else {
          dbmr.reverse(paymentReversalReason)
        }
      }
    )
  }

  /**
   * Make payment to producer unapplied
   * @param agencyPaymentReceipt: details of the payment
   * @return the new unapplied amount of the producer
   */
  @Throws(RequiredFieldException, "if the agencyPaymentReceipt fields ProducerID, PaymentInstrumentRecord, or Amount is null")
  @Throws(BadIdentifierException, "If there are no Producers with the given Producer ID.")
  @Throws(DataConversionException, "if agencyPaymentReceipt.Amount is null")
  function payToProducerUnapplied(agencyPaymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord): String {
    validateAgencyPaymentReceipt(agencyPaymentReceipt)
    Transaction.runWithNewBundle(\ bundle -> {
      var money = PaymentReceipts.toEntity(agencyPaymentReceipt, bundle) as AgencyBillMoneyRcvd
      money.execute()
    })
    var producer = WebserviceEntityLoader.loadProducer(agencyPaymentReceipt.ProducerID)
    return producer.UnappliedAmount as String
  }

  /**
   * Return the Unapplied Balance of the specified Producer
   * @param producerID PublicID of the Producer who's Unapplied balance will be returned
   */
  @Throws(RequiredFieldException, "if the producerID is null")
  @Throws(BadIdentifierException, "If there are no Producers with the given Producer ID.")
  function getProducerUnapplied(producerID: String): MonetaryAmount {
    var producer = WebserviceEntityLoader.loadProducer(producerID)
    return producer.UnappliedAmount
  }

  /**
   * Writeoff the given amount to the Producer's Unapplied balance.
   * @param producerID PublicID of the Producer whose Unapplied Balance will be written off
   * @param amount Amount to be written off
   * @return the producer's writeoff expense balance after the writeoff.
   */
  @Throws(RequiredFieldException, "if the producerID is null")
  @Throws(BadIdentifierException, "If there are no Producers with the given Producer ID.")
  function writeoffProducerUnapplied(producerID: String, writeoffAmount: MonetaryAmount): MonetaryAmount {
    var newWriteoffExpense: MonetaryAmount
    Transaction.runWithNewBundle(\bundle -> {
      var producer = bundle.add(WebserviceEntityLoader.loadProducer(producerID))
      var prodWriteoffContainer = new WriteOffFactory(bundle).createProducerWriteOff(producer)
      var uiWriteoffCreation = new UIWriteOffCreation(prodWriteoffContainer)
      uiWriteoffCreation.Amount = writeoffAmount
      uiWriteoffCreation.doWriteOff()
      newWriteoffExpense = producer.WriteoffExpenseBalance
    })
    return newWriteoffExpense
  }

  /**
   * Makes an Agency Payment and distributes it according to the DistributionItemRecords that are passed in. A DistributionItemRecord represents
   * the DistItem that will be created for a given InvoiceItem and how much gross and commission to allocate to it.  The Agency Payment will be
   * distributed and executed.
   * Optionally, a producer writeoff can be executed if the net amount of the payment does not match the amount distributed.  If the amount to write
   * off exceeds the producer's writeoff threshold, an activity is created to notify the account rep and the payment is allowed to go through without
   * the writeoff.
   *
   * @param agencyPaymentReceipt details of the payment
   * @param distributionItemRecord[] An array of DistributionItemRecords that represent the DistItems that are to be created, specifying how much
   * Gross and Commission to distribute to each InvoiceItem
   * @param attemptProducerWriteoff If this flag is true, the difference between the payment amount and the net distribution amount will attempt
   * to be written off.  If false, the difference will go into producer unapplied.
   */
  @Throws(RequiredFieldException, "If the agencyPaymentReceipt fields ProducerID, PaymentInstrumentRecord, or Amount is null")
  @Throws(BadIdentifierException, "If there are no Producers with the given Producer ID")
  @Throws(DataConversionException, "If agencyPaymentReceipt.Amount is null")
  function makeAgencyBillPayment(agencyPaymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord, distributionRecords: DistributionItemRecord[], attemptProducerWriteoff: boolean): String {
    validateAgencyPaymentReceipt(agencyPaymentReceipt)
    var money: AgencyBillMoneyRcvd
    Transaction.runWithNewBundle(\bundle -> {
      money = PaymentReceipts.toEntity(agencyPaymentReceipt, bundle) as AgencyBillMoneyRcvd
      var moneySetup = AgencyBillMoneySetupFactory.createEditingPaymentMoney(money, bundle)
      moneySetup.setPrefill(TC_ZERO)
      var distribution = moneySetup.prepareDistribution()
      distributeAccordingToDistItemRecords(distribution, distributionRecords)
      if (attemptProducerWriteoff) {
        var amountToWriteOff = getAmountToWriteOff(distribution)
        var producerWriteoffThreshold = distribution.Producer.AgencyBillPlan.getProducerWriteoffThreshold(distribution.getCurrency())
        if (amountToWriteOff.abs().compareTo(producerWriteoffThreshold) > 0) {
          createWriteoffActivity(distribution.Producer, amountToWriteOff, bundle)
        } else {
          distribution.WriteOffAmount = amountToWriteOff
        }
      }
      distribution.execute()
    })
    return money.PublicID
  }

  /**
   * Makes an Agency Payment and distributes to all Unsettled InvoiceItems belonging to the specified PolicyPeriod.  Money is distributed via the
   * IAgencyCycleDist plugin's implementation of distributeGrossAndCommission.
   * Optionally, a producer writeoff can be executed if the net amount of the payment does not match the amount distributed.  If the amount to write
   * off exceeds the producer's writeoff threshold, an activity is created to notify the account rep and the payment is allowed to go through without
   * the writeoff.
   *
   * @param agencyPaymentReceipt details of the payment
   * @param policyPeriodID PublicID of the PolicyPeriod for which we want to make the payment.
   * @param grossAmount the Gross Amount of money to be distributed among the PolicyPeriod's InvoiceItems
   * @param commissionAmount the CommissionAmount of money to be distributed among the PolicyPeriod's InvoiceItems
   * @param attemptProducerWriteoff If this flag is true, the difference between the payment amount and the net distribution amount will attempt
   * to be written off.  If false, the difference will go into producer unapplied.
   */
  function makeAgencyBillPaymentToPolicyPeriod(agencyPaymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord, policyPeriodID: String, netAmountToDistribute: MonetaryAmount, attemptProducerWriteoff: boolean): String {
    validateAgencyPaymentReceipt(agencyPaymentReceipt)
    var policyPeriod = WebserviceEntityLoader.loadPolicyPeriod(policyPeriodID)
    var invoiceItems = policyPeriod.InvoiceItemsWithoutOffsetsOrCommissionRemainderSortedByEventDate
        .where(\item -> item.GrossUnsettledAmount.IsNotZero || item.UnsettledCommission.IsNotZero)
    var money: AgencyBillMoneyRcvd
    Transaction.runWithNewBundle(\ bundle -> {
      money = PaymentReceipts.toEntity(agencyPaymentReceipt, bundle) as AgencyBillMoneyRcvd
      var moneySetup = AgencyBillMoneySetupFactory.createEditingPaymentMoney(money, bundle)
      moneySetup.setPrefill(TC_ZERO)
      var distribution = moneySetup.prepareDistribution()
      distribution.addInvoiceItems(invoiceItems)
      var agencyCycleDistPlugin = Plugins.get(gw.plugin.agencybill.IAgencyCycleDist)
      agencyCycleDistPlugin.distributeNetAmount(distribution.DistItemsList, netAmountToDistribute)
      if (attemptProducerWriteoff) {
        var amountToWriteOff = getAmountToWriteOff(distribution)
        var producerWriteoffThreshold = distribution.Producer.AgencyBillPlan.getProducerWriteoffThreshold(distribution.getCurrency())
        if (amountToWriteOff.abs().compareTo(producerWriteoffThreshold) > 0) {
          createWriteoffActivity(distribution.Producer, amountToWriteOff, bundle)
        } else {
          distribution.WriteOffAmount = amountToWriteOff
        }
      }
      distribution.execute()
    })
    return money.PublicID
  }

  /**
   * Makes a payment to an Account or to a specific Invoice on an Account. If no invoiceID is passed in, the payment
   * will be distributed to the invoices of the Account using standard payment distribution.
   * <p/>
   * If a non-null invoiceID parameter is passed in, the payment will be targeted at that specific Invoice.
   * <p/>
   * @param directbillPaymentReceipt A PaymentReceiptRecord of subtype DirectBillMoneyDetails
   * @param invoiceID The Invoice to make the payment to
   * @return directBillMoneyRcvdID The PublicID of the payment that is created
   */
  @Throws(RequiredFieldException, "If the directbillPaymentReceipt fields AccountID, PaymentInstrumentRecord, or Amount is null")
  function makeDirectBillPaymentToInvoice(directbillPaymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord, invoiceID: String): String {
    validateDirectBillPaymentReceipt(directbillPaymentReceipt)
    var directBillMoneyRcvd: DirectBillMoneyRcvd
   Transaction.runWithNewBundle(\ bundle -> {
      var account = bundle.add(WebserviceEntityLoader.loadAccount(directbillPaymentReceipt.AccountID))
      var unappliedFund = account.DefaultUnappliedFund
      var invoice = null as Invoice
      var isTargetedToInvoice = false
      if (invoiceID != null) {
        isTargetedToInvoice = true
        invoice = bundle.add(WebserviceEntityLoader.loadInvoice(invoiceID))
        unappliedFund = invoice.InvoiceStream.UnappliedFund
      }
      var directBillMoneyDetails = PaymentReceipts.toEntity(directbillPaymentReceipt, bundle)
      directBillMoneyRcvd =
          DirectBillPaymentFactory.createAndExecuteMoneyReceivedFromPaymentReceipt(unappliedFund,
              directBillMoneyDetails as DirectBillMoneyDetails,
              directbillPaymentReceipt.ReceivedDate,
              directbillPaymentReceipt.Description)
      if (isTargetedToInvoice) {
        directBillMoneyRcvd.Invoice = invoice
      }
    })
    return directBillMoneyRcvd.PublicID
  }

  //--------------------------- Private functions -----------------------

  private function createBatchPayment(bundle: Bundle, batchPaymentDTO: BatchPaymentDTO): BatchPayment {
    WebservicePreconditions.checkArgument(BCConfigParameters.EnableBatchPayments.Value == true,
        DisplayKey.get("Webservice.Error.BatchPaymentDisabled"))
    var batchPayment = new BatchPayment(bundle,batchPaymentDTO.Amount.Currency)
    mergeBatchPayment(batchPayment, batchPaymentDTO)
    return batchPayment
  }

  private function mergeBatchPayment(sourceBatchPayment: BatchPayment, batchPaymentDTO: BatchPaymentDTO) {
    BatchPayments.applyDTO(sourceBatchPayment, batchPaymentDTO)
    BatchPaymentWebValidator.Instance.validate(sourceBatchPayment)
    WebservicePreconditions.checkArgument(
        BatchPaymentsStatus.TC_READYTOPOST == sourceBatchPayment.BatchStatus,
        DisplayKey.get("Web.BatchPaymentWebAPI.NotReadyToPostBatchIsNotAccepted")
    )
  }

  private function distributeAccordingToDistItemRecords(distribution: AgencyCyclePayment, distributionRecords: DistributionItemRecord[]) {
    // InvoiceItems to which to distribute...
    final var invoiceItems = WebserviceEntityLoader
            .loadInvoiceItems(distributionRecords.map(\d -> d.InvoiceItemID))
        .where(\ item -> {
            if (item.Frozen) {
              BCLoggerCategory.API.error(
                  DisplayKey.get('PaymentAPI.Error.FrozenInvoiceItemIgnoredForDistribution',
                      {item, item.PublicID, distribution.Producer}
                  )
              )
            }
            return not item.Frozen
          }
        )
    distribution.addInvoiceItems(invoiceItems)
    // set distribution values in DistItems...
    final var distItemByInvoiceItemID = mapInvoiceItemPublicIDToDistItem(distribution.DistItems)
    distributionRecords.each(\ record -> {
      var distItem = distItemByInvoiceItemID.get(record.InvoiceItemID)
      // skipped distributions will not have a distItem...
      if (distItem != null) {
        distItem.GrossAmountToApply = record.GrossAmount
        distItem.CommissionAmountToApply = record.CommissionAmount
        distItem.Disposition = record.Disposition
      }
    })
  }

  private function mapInvoiceItemPublicIDToDistItem(distItems: BaseDistItem[]): Map <String, BaseDistItem> {
    return distItems.partitionUniquely(\distItem -> distItem.InvoiceItem.PublicID)
  }

  private function validateAgencyPaymentReceipt(agencyPaymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord) {
    WebservicePreconditions.notNull(agencyPaymentReceipt.ProducerID, "agencyPaymentReceipt.ProducerID")
    validatePaymentReceipt(agencyPaymentReceipt, "agencyPaymentReceipt")
  }

  private function validateDirectBillPaymentReceipt(directbillPaymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord) {
    WebservicePreconditions.notNull(directbillPaymentReceipt.AccountID, "directbillPaymentReceipt.AccountID")
    validatePaymentReceipt(directbillPaymentReceipt, "directbillPaymentReceipt")
  }

  private function validatePaymentReceipt(paymentReceipt: gw.webservice.bc.bc1000.PaymentReceiptRecord, fieldName: String) {
    WebservicePreconditions.notNull(paymentReceipt.PaymentInstrumentRecord, "${fieldName}.PaymentInstrumentRecord")
    WebservicePreconditions.notNull(paymentReceipt.MonetaryAmount, "${fieldName}.Amount")
  }

  private function createWriteoffActivity(producer: Producer, amountToWriteOff: MonetaryAmount, bundle: Bundle) {
    var activity = new Activity(bundle)
    activity.ActivityPattern = ActivityPatternsUtil.getActivityPattern("attemptedwriteoff")
    activity.Description = DisplayKey.get('Activity.AttemptedWriteoff.Description', producer.DisplayName, amountToWriteOff)
    if (producer.AccountRep != null) {
      activity.assignUserAndDefaultGroup(producer.AccountRep)
    } else {
      activity.assignUserByRoundRobin(true, CurrentUserUtil.getCurrentUser().User.RootGroup)
    }
  }

  // should be the same calculation as the UI uses

  private function getAmountToWriteOff(distribution: AgencyCyclePayment): MonetaryAmount {
    return distribution.Amount.subtract(distribution.DistributedAmountForUnexecutedDist.add(distribution.NetSuspenseAmountForSavedOrExecuted)).negate()
  }

  private function getUnappliedFundFor(policyPeriodPublicID: String, account: Account): UnappliedFund {
    var policyPeriod = WebserviceEntityLoader.loadPolicyPeriod(policyPeriodPublicID)
    var unappliedFund = policyPeriod.Policy.getDesignatedUnappliedFund(account)
    if (unappliedFund == null) {
      throw new SOAPException(DisplayKey.get('Webservice.Error.NoUnappliedFundOnPolicyForAccount', policyPeriod, account))
    }
    return unappliedFund
  }
}