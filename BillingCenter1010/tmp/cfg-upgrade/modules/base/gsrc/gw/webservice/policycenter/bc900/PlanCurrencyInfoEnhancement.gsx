package gw.webservice.policycenter.bc900

uses gw.api.locale.DisplayKey
uses gw.webservice.policycenter.bc900.entity.types.complex.PlanCurrencyInfo

/**
 * Defines the {@link PlanCurrencyInfo} copy currency info' method.
 */
@Deprecated("Preferably use classes in package gw.webservice.policycenter.bc1000 instead")
@Export
enhancement PlanCurrencyInfoEnhancement : PlanCurrencyInfo {
  function copyPlanCurrencyInfo(plan : Plan) {
    this.copyPlanInfo(plan)
    if (plan typeis MultiCurrencyPlan) {
      if (plan.isSingleCurrency) {
        this.Currency = plan.Currencies[0].Code
      } else {
        throw new IllegalStateException(DisplayKey.get("Webservice.Error.Plan.DoesNotSupportMultiCurrencies", null, plan))
      }
    }
  }
}
