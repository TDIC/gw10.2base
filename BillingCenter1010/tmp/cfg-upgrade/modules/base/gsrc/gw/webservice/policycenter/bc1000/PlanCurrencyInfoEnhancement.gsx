package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.PlanCurrencyInfo
uses entity.Plan

/**
 * Defines the {@link PlanCurrencyInfo} copy currency info' method.
 */
@Export
enhancement PlanCurrencyInfoEnhancement : PlanCurrencyInfo {
  function copyPlanCurrencyInfo(plan : Plan) {
    this.copyPlanInfo(plan)
    if (plan typeis MultiCurrencyPlan){
      this.Currencies = plan.Currencies*.Code.toList()
    }
  }
}
