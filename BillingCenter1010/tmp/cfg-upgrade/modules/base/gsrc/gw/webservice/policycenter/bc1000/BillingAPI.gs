package gw.webservice.policycenter.bc1000

uses com.google.common.base.Preconditions
uses com.google.common.collect.Lists
uses com.google.common.collect.Sets
uses com.guidewire.pl.system.exception.DBDuplicateKeyException
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.domain.account.AccountTransfer
uses gw.api.domain.invoice.InvoiceStreams
uses gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange
uses gw.api.domain.invoice.PaymentPlanChanger
uses gw.api.locale.DisplayKey
uses gw.api.util.BCUtils
uses gw.api.util.CurrencyUtil
uses gw.api.web.payment.PaymentInstrumentFactory
uses gw.api.webservice.exception.AlreadyExecutedException
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.DataConversionException
uses gw.api.webservice.exception.EntityStateException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.api.webservice.exception.SOAPException
uses gw.api.webservice.exception.SOAPSenderException
uses gw.api.webservice.exception.SOAPServerException
uses gw.api.webservice.exception.ServerStateException
uses gw.invoice.InvoiceItemFilter
uses gw.pl.persistence.core.Bundle
uses gw.pl.persistence.core.Key
uses gw.transaction.Transaction
uses gw.webservice.bc.bc1000.PaymentInstrumentRecord
uses gw.webservice.bc.bc1000.PaymentInstruments
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.ChargeInfo_ChargeCommissionRateOverrideInfos
uses gw.webservice.policycenter.bc1000.entity.types.complex.AccountGeneralInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.AgencyBillPlanInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.CancelPolicyInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.ChargeInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.CollateralInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.CommissionPlanInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.FinalAuditInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.IssuePolicyInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewAccountInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.NewProducerCodeInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCAccountInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCContactInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewProducerInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCPolicyPeriodInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCProducerInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PaymentAllocationPlanInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PaymentPlanInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PolicyChangeInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PolicyPeriodGeneralInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PremiumReportInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.ProducerCodeInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.ReinstatementInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.RenewalInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.RewriteInfo
uses gw.webservice.policycenter.BillingAPITransactionUtil
uses gw.webservice.util.WebserviceEntityLoader
uses typekey.Currency

/**
 * The custom API that supports integration with Policy Center 9, and which may
 * be maintained by Policy Center.
 */
@gw.xml.ws.annotation.WsiWebService("http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/BillingAPI")
@Export
class BillingAPI extends gw.webservice.policycenter.bc1000.AbstractBillingAPI {
  /**
   * Search for billing center account given the search criteria
   *
   * @param searchCriteria the search criteria
   * @return the list of account numbers
   */
  @Throws(BadIdentifierException, "If there are too many results")
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  function searchForAccounts(searchCriteria: BCAccountSearchCriteria,
                             limit: Integer): BCAccountSearchResult[] {
    final var results = searchCriteria.searchForAccountNumbers()
    if (limit == null){
      limit = 50
    }
    final var resultsCount = results.getCountLimitedBy(limit + 1)
    if (resultsCount > limit) {
      throw new BadIdentifierException(DisplayKey.get("Java.Search.TooManyResults", limit))
    } else if (resultsCount == 1 and searchCriteria.AccountNumber != null) {
      return {
          new BCAccountSearchResult(searchCriteria.matchAccountNumber())}
    }
    return results.map(\a -> new BCAccountSearchResult(a)).toTypedArray()
  }

  /**
   * Retrieves {@link InvoiceStreamInfo information} for all {@link
   *    entity.InvoiceStream}s associated with the identified {@link Account account}
   *    for the (optional in single currency mode) {@link typekey.Currency currency}
   *    which are compatible with the optional {@link PaymentPlan paymentPlanPublicID}
   *    or an empty list if the {@code account} does not exist.
   *
   * @param accountNumber the account number of the {@code account}
   * @param currency the (optional in single currency mode) currency for which the streams should apply;
   *                 if {@code null}, then this defaults to the identified
   *                 {@code account}
   * @param paymentPlanPublicID the (optional) PublicID of the PaymentPlan for which the streams should be compatible;
   *                 if {@code null}, then no filter is applied
   * @return A list of invoice streams for the {@code account}
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function getAccountInvoiceStreams(accountNumber : String, currency : Currency = null, paymentPlanPublicID: String = null)
      : InvoiceStreamInfo[] {
    final var account = findCurrencyAccount(accountNumber, currency)
    if (account == null) {
      return {} // this may be called for a newly created account in policy system
    }
    if (paymentPlanPublicID != null) {
      var paymentPlan = WebserviceEntityLoader.loadByPublicID<PaymentPlan>(paymentPlanPublicID)
      return (InvoiceStreams.getCompatibleInvoiceStreams(account, paymentPlan).map(\i -> new InvoiceStreamInfo(i)))?.toTypedArray()
    }
    return account.InvoiceStreams.map(\i -> new InvoiceStreamInfo(i))
  }

  /**
   * Looks up and returns {@link BCAccountSearchResult account search result}s
   *    for all sub-accounts of the {@link Account} identified by the specified
   *    account number.
   *
   * The search is recursive and will return all the sub-accounts under the tree.
   *
   * @param accountNumber the account number for the {@link Account}
   * @return an array of {@link BCAccountSearchResult account search results}s.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function getAllSubAccounts(accountNumber : String) : BCAccountSearchResult[] {
    return getAllSubAccountsForCurrency(accountNumber, null)
  }

  /**
   * Looks up and returns {@link BCAccountSearchResult account search result}s
   *    for all sub-accounts of the {@link Account} identified by the specified
   *    account number.
   *
   * The search is recursive and  will return all the sub-accounts under the tree.
   *
   * @param accountNumber the account number for the {@link Account}
   * @param currency the currency we want to filter the results to match, null returns all currencies
   * @return an array of {@link BCAccountSearchResult account search results}s.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function getAllSubAccountsForCurrency(accountNumber : String, currency : Currency) : BCAccountSearchResult[] {
    require(accountNumber, "accountNumber")

    final var account = BCAccountSearchCriteria.findByAccountNumber(accountNumber)
    if (account == null) {
      // this may be called before issuance for a policy system account
      // that will be created when the policy is issued...
      return {}
    }

    final var results: Map<Key, BCAccountSearchResult> = {}
    var parents = Sets.newHashSet({account.ID})
    while (not parents.isEmpty()) {
      parents.addAll(findAllSplinterIdsFor(parents))
      var query = BCAccountSearchCriteria.makePrimaryAccountQuery(
          \query ->
              query.compareIn(Account.PARENTACCOUNT_PROP.get(), parents)
      )
      var children = query.select().toTypedArray()

      // extract parents at this level...
      parents = Sets.newHashSet(children.map(\child -> child.ID))
      // remove circular references...
      parents.removeAll(results.Keys)

      children.each(\s -> results.put(s.ID, new BCAccountSearchResult(s)))
    }

    if (currency != null) {
      results.retainWhereValues(\v -> v.Currency == currency)
    }

    return results.Values.toTypedArray()
  }

  /**
   * Transfer policy period to another account.
   * @param policyPeriodInfo information to identify the policy period to be transferred
   * @param targetAccountNumber the target account number
   * @param transactionId the unique id to make this call idempotent
   */
  @Throws(BadIdentifierException, "If cannot find the policy period of account specified")
  @Throws(RequiredFieldException, "If required field is missing ")
  @Throws(AlreadyExecutedException, "If this call is already executed")
  @Throws(EntityStateException, "If any of the PolicyPeriods is either retrieved or has frozen InvoiceItems.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  function transferPolicyPeriods(policyPeriodInfos : PCPolicyPeriodInfo[],
                                 targetAccountNumber : String, transactionId : String) {
    require(policyPeriodInfos, "policyPeriodInfos")
    require(targetAccountNumber, "targetAccountNumber")

    if (policyPeriodInfos.IsEmpty) {
      throw new RequiredFieldException(DisplayKey.get("Webservice.Error.PolicyPeriodInfoIsRequired"))
    }
    var policyPeriods = policyPeriodInfos*.findPolicyPeriod()
    var retrievedPolicyPeriods = policyPeriods.where( \ elt -> elt.Retrieved)
    if (retrievedPolicyPeriods.HasElements) {
      throw new EntityStateException(DisplayKey.get("Webservice.Error.OperationNotPermittedOnRetrievedPolicyPeriod"))
    }
    var partiallyFrozenPolicyPeriods = policyPeriods.where( \ elt -> elt.hasFrozenInvoiceItems())
    if (partiallyFrozenPolicyPeriods.HasElements) {
      throw new EntityStateException(DisplayKey.get("Webservice.Error.OperationNotPermittedOnPartiallyFrozenPolicyPeriod"))
    }
    BillingAPITransactionUtil.executeTransaction( \bundle -> {
      policyPeriodInfos.map(\info -> info.findPolicyPeriodForUpdate())
          .partition(\policyPeriod -> policyPeriod.Currency)
          .eachKeyAndValue(\currency, policyPeriodList -> {
            transferPolicyPeriodsPerCurrency(currency, policyPeriodList, targetAccountNumber, bundle)
          })
      return null
    }, transactionId)
  }

  /**
   * Check if account with the given account number already exist in the system
   * @param accountNumber the account number to search for
   * @return true if account exists
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function isAccountExist(accountNumber: String): boolean {
    require(accountNumber, "accountNumber")
    var p = findAccountResult(accountNumber).Count
    if (p > 1) {
      throw new EntityStateException(DisplayKey.get("Webservice.Error.FoundMoreThanOneAccount", p, accountNumber))
    }
    return p > 0
  }

  private function findAccountResult(accountNumber: String): IQueryBeanResult<Account> {
    return Query.make(Account)
        .compare("AccountNumber", Relop.Equals, accountNumber)
        .select()
  }

  private function transferPolicyPeriodsPerCurrency(currency: Currency, policyPeriodList: List<PolicyPeriod>, targetAccountNumber: String, bundle: Bundle) {
    var transfer = new AccountTransfer(bundle){
        :ToAccount = findAccountOrSiblingForCurrency(targetAccountNumber, currency, bundle)
    }
    transfer.setPolicyPeriodsToTransfer(policyPeriodList.toTypedArray(), true)
    transfer.doTransfer()
  }

  private function findAccountOrSiblingForCurrency(
      accountNumber: String, currency: Currency, bundle: Bundle): Account {
    final var result = findAccountResult(accountNumber)
    if (result.Empty) {
      throwAccountNotFound(accountNumber)
    }
    final var account = bundle.add(result.AtMostOneRow)
    if (account.Currency == currency) {
      return account
    }
    if (account.AccountCurrencyGroup != null) {
      final var splinterAccount =
          findExistingAccountForCurrency(account.AccountCurrencyGroup, currency)
      if (splinterAccount != null) {
        return splinterAccount
      }
    }
    return createAccountForCurrency(account, currency)
  }

  /**
   * Return an information list of all available {@link PaymentAllocationPlan}s
   *    in BillingCenter.
   *
   * @return An {@code Array} of information records for all available
   *         {@link PaymentAllocationPlan}s.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  function findAllPaymentAllocationPlans() : PaymentAllocationPlanInfo[] {
    return PaymentAllocationPlan.finder
        .findAllAvailablePlans<PaymentAllocationPlan>(PaymentAllocationPlan)
        .map(\ plan -> {
          var planInfo = new PaymentAllocationPlanInfo()
          planInfo.copyPlanInfo(plan)
          return planInfo
        })
        .toTypedArray()
  }

  /**
   * Create a new account
   * @param accountInfo the account information
   * @return the new account's AccountNumber (useful if BC is being used to generate the AccountNumber instead of using a provided one)
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(DBDuplicateKeyException, "if the account number is duplicated")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function createAccount(accountInfo: PCNewAccountInfo, currency: Currency, transactionId: String): String {
    require(accountInfo, "accountInfo")
    require(currency, "currency")
    // no need to check for account number uniqueness and let the db check for it as well as
    // checking if the transaction id is the same which means this request is duplicated
    // (may be by a retry) and ignore the call.
    return BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var account = accountInfo.toNewAccount(currency, bundle)
      return account.AccountNumber
    }, transactionId)
  }

  /**
   * Update an existing account
   *
   * @param accountInfo the account information
   * @param transactionId the transaction id to make this call idempotent
   * @return the account public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no account exists with the given account number")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function updateAccount(accountInfo: PCAccountInfo, transactionId: String): String {
    require(accountInfo, "accountInfo")
    return BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var account: Account
      account = accountInfo.toAccount(bundle)
      return account.PublicID
    }, transactionId)
  }

  /**
   * Return all the billing method that the given producer support for the given currency
   *
   * @param producerCodeId the id of the producer
   * @param currency the currency we are looking at on the producer
   * @return the list of billing methods that the producer support
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no producer exists with the given producer code")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function getAvailableBillingMethods(producerCodeId: String, currency: Currency): String[] {
    require(producerCodeId, "producerCodeId")
    require(currency, "currency")
    var qp = Query.make(ProducerCode).compare("PublicID", Equals, producerCodeId).select()
    if (qp.Empty) {
      throw BadIdentifierException.badPublicId(ProducerCode, producerCodeId)
    } else if (qp.Count > 1) {
      throw new ServerStateException(DisplayKey.get("Webservice.Error.FoundMoreThanOneProducerCode", qp.Count, producerCodeId))
    }
    if (qp.AtMostOneRow.Currency == currency){
      return (qp.AtMostOneRow.Producer.AgencyBillPlan == null)
          ? new String[] {PolicyPeriodBillingMethod.TC_DIRECTBILL.Code}
          : new String[] {PolicyPeriodBillingMethod.TC_DIRECTBILL.Code, PolicyPeriodBillingMethod.TC_AGENCYBILL.Code}
    }
    var producerCurrencyGroup = qp.AtMostOneRow.Producer.ProducerCurrencyGroup
    if (producerCurrencyGroup != null) {
      var producers = producerCurrencyGroup.findProducers()
      if (producers != null) {
        for (producer in producers) {
          if (producer.Currency == currency) {
            return (producer.AgencyBillPlan == null)
                ? new String[]{PolicyPeriodBillingMethod.TC_DIRECTBILL.Code}
                : new String[]{PolicyPeriodBillingMethod.TC_DIRECTBILL.Code, PolicyPeriodBillingMethod.TC_AGENCYBILL.Code}
          }
        }
      }
    }
    return null
  }

  /**
   * Issue a policy period.
   *
   * @param information necessary for issue a policy period
   * @return the new policy period public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(SOAPSenderException, "If caller has provided info. not applicable to the context.")
  @Throws(SOAPSenderException, "If SpecialHandling is specified (it can not be specified when creating a new PolicyPeriod)")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If a policy already exists with the given number")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function issuePolicyPeriod(issuePolicyInfo: IssuePolicyInfo, transactionId: String): String {
    require(issuePolicyInfo, "issuePolicyInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> issuePolicyInfo.executeIssuanceBI(), transactionId)
  }

  /**
   * Generates a preview of the installment schedule that would be in place for
   * a policy after execution of a {@link PolicyChange} as specified by the
   * {@link PolicyChangeInfo}.<br/><b>Note:</b> The returned {@link InvoiceSummaryItem} array will not include entry for retired Invoices
   * even if they have non-retired InvoiceItems
   * @param policyChangeInfo the information from which to execute a policy
   *                         change on a policy period
   * @return The invoices preview summary items.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function previewPolicyChangeInvoices(policyChangeInfo : PolicyChangeInfo)
      : InvoiceSummaryItem[] {
    require(policyChangeInfo, "policyChangeInfo")
    var policyChange : PolicyChange
    runWithNonPersistentBundle(\ bundle -> {
      policyChange = policyChangeInfo.toPolicyChangeForPreview()
      policyChange.execute()
      // Installment fees, if any, are added when an invoice is billed. Preview
      // needs to behave as if all invoices have been billed, so add the fees
      // explicitly for planned ones since billed ones have already done so
      addFeesForPlannedInvoices(policyChange.PolicyPeriod.Invoices)
    })
    return policyChangeInfo.createInvoicesSummary(policyChange)
  }

  /**
   * Generates a preview of the installment schedule that would be created for a new policy after execution of an
   * {@link Issuance} as specified by the {@link IssuePolicyInfo}. If the account owner or overriding payer
   * (AltBillingAccountNumber) not already exist in billing center, PCNewAccountInfos may optionally be provided.
   * If the info objects are not provided, BC will create temporary accounts with system default settings.
   *
   * @param issuePolicyInfo the information from which to issue a policy period
   * @param ownerAccount the object to create a temporary account with, in order to create the invoice previews.
   *                     Expected to be null if the account is already known by BillingCenter
   * @param overridingPayer the object to create a temporary overriding payer account with, in order to create the invoice
   *                        previews. Expected to be null if the account is already known by BillingCenter.
   * @return The invoices preview summary items.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(SOAPSenderException, "If caller has provided info. not applicable to the context.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If ownerAccount or overridingPayer represents an account that already exists in BillingCenter")
  function previewIssuanceInvoices(issuePolicyInfo : IssuePolicyInfo, ownerAccount : PCNewAccountInfo, overridingPayer : PCNewAccountInfo)
  : InvoiceSummaryItem[] {
    require(issuePolicyInfo, "issuePolicyInfo")
    var issuance : Issuance
    runWithNonPersistentBundle(\ bundle -> {
      issuance = issuePolicyInfo.toIssuanceForPreview(ownerAccount, overridingPayer)
      // Installment fees, if any, are added when an invoice is billed. Preview
      // needs to behave as if all invoices have been billed, so add the fees
      // explicitly
      issuance.PolicyPeriod.Invoices.each(\ invoice -> invoice.addFees())
    })
    return issuePolicyInfo.createInvoicesSummary(issuance)
  }

  /**
   * Generates a preview of the installment schedule that would be created for a policy after execution of an
   * {@link Renewal} as specified by the {@link RenewalInfo}. If the account owner or overriding payer
   * (AltBillingAccountNumber) not already exist in billing center, PCNewAccountInfos may optionally be provided.
   * If the info objects are not provided, BC will create temporary accounts with system default settings.
   *
   * @param ownerAccount the object to create a temporary account with, in order to create the invoice previews.
   *                     Expected to be null if the account is already known by BillingCenter
   * @param overridingPayer the object to create a temporary overriding payer account, in order to create the invoice
   *                        previews. Expected to be null if the account is already known by BillingCenter.
   * @param renewalInfo the information from which to renew a policy period
   * @return The invoices preview summary items.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(SOAPSenderException, "If caller has provided info. not applicable to the context.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(EntityStateException, "If ownerAccount or overridingPayer represents an account that already exists in BillingCenter")
  function previewRenewalInvoices(renewalInfo : RenewalInfo, ownerAccount : PCNewAccountInfo, overridingPayer : PCNewAccountInfo)
  : InvoiceSummaryItem[] {
    require(renewalInfo, "renewalInfo")
    var renewal : NewPlcyPeriodBI
    runWithNonPersistentBundle(\ bundle -> {
      renewal = renewalInfo.toRenewalForPreview(ownerAccount, overridingPayer)
      // Installment fees, if any, are added when an invoice is billed. Preview
      // needs to behave as if all invoices have been billed, so add the fees
      // explicitly
      renewal.PolicyPeriod.Invoices.each(\ invoice -> {invoice.addFees()})
    })
    return renewalInfo.createInvoicesSummary(renewal)
  }

  /**
   * Generates a preview of the installment schedule that would be created for a policy after execution of an
   * {@link Rewrite} as specified by the {@link RewriteInfo}. If the account owner or overriding payer
   * (AltBillingAccountNumber) not already exist in billing center, PCNewAccountInfos may optionally be provided.
   * If the info objects are not provided, BC will create temporary accounts with system default settings.
   *
   * @param ownerAccount the object to create a temporary account with, in order to create the invoice previews.
   *                     Expected to be null if the account is already known by BillingCenter
   * @param overridingPayer the object to create a temporary overriding payer account, in order to create the invoice
   *                        previews. Expected to be null if the account is already known by BillingCenter.
   * @param rewriteInfo the information from which to rewrite a policy period
   * @return The invoices preview summary items.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(SOAPSenderException, "If caller has provided info. not applicable to the context.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(EntityStateException, "If ownerAccount or overridingPayer represents an account that already exists in BillingCenter")
  function previewRewriteInvoices(rewriteInfo : RewriteInfo, ownerAccount : PCNewAccountInfo, overridingPayer : PCNewAccountInfo)
  : InvoiceSummaryItem[] {
    require(rewriteInfo, "rewriteInfo")
    var rewrite : Rewrite
    runWithNonPersistentBundle(\ bundle -> {
      rewrite = rewriteInfo.toRewriteForPreview(ownerAccount, overridingPayer)
      // Installment fees, if any, are added when an invoice is billed. Preview
      // needs to behave as if all invoices have been billed, so add the fees
      // explicitly
      addFeesForPlannedInvoices(rewrite.PolicyPeriod.Invoices)
    })
    return rewriteInfo.createInvoicesSummary(rewrite)
  }

  /**
   * Generates a preview of the installment schedule that would be in place for
   * a policy after execution of a {@link Reinstatement} as specified by the
   * {@link ReinstatementInfo}.<br/><b>Note:</b> The returned {@link InvoiceSummaryItem} array will not include entry for retired Invoices
   * even if they have non-retired InvoiceItems
   * @param reinstatementInfo the information from which to reinstate a policy
   *                          period
   * @return The invoices preview summary items.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function previewReinstatementInvoices(reinstatementInfo : ReinstatementInfo)
      : InvoiceSummaryItem[] {
    require(reinstatementInfo, "reinstatementInfo")
    var reinstatement : Reinstatement
    BCUtils.executeInPreviewBundle(\ bundle -> {
      reinstatement = reinstatementInfo.toReinstatementForPreview()
      reinstatement.execute()
      // Installment fees, if any, are added when an invoice is billed. Preview
      // needs to behave as if all invoices have been billed, so add the fees
      // explicitly for planned ones since billed ones have already done so
      addFeesForPlannedInvoices(reinstatement.PolicyPeriod.Invoices)
    })
    return reinstatementInfo.createInvoicesSummary(reinstatement)
  }

  /**
   * Generates a preview of the installment schedule that would be in place for
   * a policy after execution of a {@link PremiumReport} as specified by the
   * {@link PremiumReportInfo}.<br/><b>Note:</b> The returned {@link InvoiceSummaryItem} array will not include entry for retired Invoices
   * even if they have non-retired InvoiceItems
   * @param reportInfo the information from which to execute a premium report
   *                   on a policy period
   * @return The invoices preview summary items.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function previewPremiumReportInvoices(reportInfo : PremiumReportInfo)
      : InvoiceSummaryItem[] {
    require(reportInfo, "reportInfo")
    var premiumReport : PremiumReportBI
    BCUtils.executeInPreviewBundle(\ bundle -> {
      premiumReport = reportInfo.toPremiumReportForPreview()
      premiumReport.execute()
      // Installment fees, if any, are added when an invoice is billed. Preview
      // needs to behave as if all invoices have been billed, so add the fees
      // explicitly for planned ones since billed ones have already done so
      addFeesForPlannedInvoices(premiumReport.PolicyPeriod.Invoices)
    })
    return reportInfo.createInvoicesSummary(premiumReport)
  }

  /**
   * Generates a preview of the installment schedule that would be in place for
   * a policy after execution of a final {@link Audit} as specified by the
   * {@link FinalAuditInfo}.<br/><b>Note:</b> The returned {@link InvoiceSummaryItem} array will not include entry for retired Invoices
   * even if they have non-retired InvoiceItems
   * @param finalAuditInfo the information from which to execute an audit of a
   *                       policy period
   * @return The invoices preview summary items.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function previewFinalAuditInvoices(finalAuditInfo : FinalAuditInfo)
      : InvoiceSummaryItem[] {
    require(finalAuditInfo, "finalAuditInfo")
    var audit : Audit
    BCUtils.executeInPreviewBundle(\ bundle -> {
      audit = finalAuditInfo.toFinalAuditForPreview()
      audit.execute()
      // Installment fees, if any, are added when an invoice is billed. Preview
      // needs to behave as if all invoices have been billed, so add the fees
      // explicitly for planned ones since billed ones have already done so
      addFeesForPlannedInvoices(audit.PolicyPeriod.Invoices)
    })
    return finalAuditInfo.createInvoicesSummary(audit)
  }

  /**
   * Generates a preview of the installment schedule that would be in place for
   * a policy after execution of a {@link Cancellation} as specified by the
   * {@link CancelPolicyInfo}.<br/><b>Note:</b> The returned {@link InvoiceSummaryItem} array will not include entry for retired Invoices
   * even if they have non-retired InvoiceItems
   * @param cancelInfo the information from which to execute a cancellation of a
   *                   policy period
   * @return The invoices preview summary items.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function previewCancellationInvoices(cancelInfo : CancelPolicyInfo)
      : InvoiceSummaryItem[] {
    require(cancelInfo, "cancelInfo")
    var cancellation : Cancellation
    BCUtils.executeInPreviewBundle(\ bundle -> {
      cancellation = cancelInfo.toCancellationForPreview()
      cancellation.execute()
      // Installment fees, if any, are added when an invoice is billed. Preview
      // needs to behave as if all invoices have been billed, so add the fees
      // explicitly for planned ones since billed ones have already done so
      addFeesForPlannedInvoices(cancellation.PolicyPeriod.Invoices)
    })
    return cancelInfo.createInvoicesSummary(cancellation)
  }

  private function addFeesForPlannedInvoices(invoices : Collection<Invoice>) {
    invoices.each(\ invoice -> {
      if (invoice.Planned) {
        invoice.addFees()
      }
    })
  }

  /**
   * Updates the written date on the given charge
   *
   * @param charge              The charge for which the written date should be updated
   * @param writtenDate         The new written date
   */
  @Throws(RequiredFieldException, "if the charge or writtenDate variable is null")
  @Throws(BadIdentifierException, "If no charge exists with the given PublicID")
  @Throws(EntityStateException, "If the Charge has frozen InvoiceItems.")
  function updateChargeWrittenDate(chargeID: String, writtenDate: Date) {
    require(chargeID, "charge")
    require(writtenDate, "writtenDate")
    var charge = WebserviceEntityLoader.loadByPublicID<Charge>(chargeID)
    if (charge.hasFrozenInvoiceItems()) {
      throw new EntityStateException(DisplayKey.get("Webservice.Error.OperationNotPermittedOnPartiallyFrozenCharge"))
    }
    Transaction.runWithNewBundle(\ bundle -> {
      charge = bundle.add(charge)
      charge.WrittenDate = writtenDate
    })
  }

  /**
   * Updates the TermConfirmed field on the given policy period
   *
   * @param policyNumber      The policy number for which the term confirmed field should be updated
   * @param termNumber        The term number for this policy
   * @param isConfirmed       The new value
   */
  @Throws(RequiredFieldException, "if the policyNumber or termNumber or isConfirmed variable is null")
  @Throws(EntityStateException, "If the PolicyPeriod is closed")
  function updatePolicyPeriodTermConfirmed(policyNumber: String, termNumber: int,
                                           isConfirmed: Boolean) {
    require(policyNumber, "policy number")
    require(termNumber, "policy period term number")
    require(isConfirmed, "isConfirmed")
    var policyPeriod = PolicyPeriod.finder.findByPolicyNumberAndTerm(policyNumber, termNumber)
    if (policyPeriod.Closed) {
      throw new EntityStateException(DisplayKey.get("Webservice.Error.OperationNotPermittedOnClosedPolicyPeriod"))
    }
    Transaction.runWithNewBundle(\ bundle -> {
      policyPeriod = bundle.add(policyPeriod)
      policyPeriod.TermConfirmed = isConfirmed
    })
  }


  /**
   * Cancel a policy period.
   *
   * @param cancelInfo information necessary for cancellation
   * @return the cancellation billing instruction public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no policy exists with the given number")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function cancelPolicyPeriod(cancelInfo: CancelPolicyInfo, transactionId: String): String {
    require(cancelInfo, "cancelInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> cancelInfo.executeCancellationBI(), transactionId)
  }

  /**
   * Change a policy period.
   *
   * @param changeInfo information necessary for policy change
   * @return the policy change billing instruction public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no policy exists with the given number")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If PolicyPeriod is closed and the change is not permitted.")
  function changePolicyPeriod(changeInfo : PolicyChangeInfo, transactionId : String) : String {
    require(changeInfo, "changeInfo")
    var policyPeriod = changeInfo.findPolicyPeriod() // throws if the PolicyPeriod is archived.
    if (policyPeriod.Closed and changeInfo.TermConfirmSpecified) {
      throw new EntityStateException(DisplayKey.get("Webservice.Error.OperationNotPermittedOnClosedPolicyPeriod"))
    }
    return BillingAPITransactionUtil.executeBillingInstruction(\ bundle -> changeInfo.executePolicyChangeBI(), transactionId)
  }

  /**
   * Reinstate a policy period.
   *
   * @param reinstatementInfo information necessary for reinstatement of the policy period
   * @return the cancellation billing instruction public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no policy exists with the given number")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function reinstatePolicyPeriod(reinstatementInfo : ReinstatementInfo, transactionId : String) : String {
    require(reinstatementInfo, "reinstatementInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> reinstatementInfo.executeReinstatementBI(), transactionId)
  }

  /**
   * Issue Final Audit for a policy period.
   *
   * @param finalAuditInfo information necessary for Final Audit
   * @return the Audit billing instruction public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no policy exists with the given number")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function issueFinalAudit(finalAuditInfo : FinalAuditInfo, transactionId : String) : String {
    require(finalAuditInfo, "finalAuditInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> finalAuditInfo.executeFinalAuditBI(), transactionId)
  }

  /**
   * Issue Premium Report for a policy period.
   *
   * @param premiumReportInfo information necessary for Premium Report
   * @return the Premium Reporting billing instruction public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no policy exists with the given number")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function issuePremiumReport(premiumReportInfo : PremiumReportInfo, transactionId : String) : String {
    require(premiumReportInfo, "premiumReportInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> premiumReportInfo.executePremiumReportBI(), transactionId)
  }

  /**
   * Renew a policy period if it is already exist or issue a NewRenewal BI if the period
   * does not exist in BC yet.
   *
   * @param renewalInfo information necessary for renewing a policy period
   * @return the Renewal or NewRenewal billing instruction's public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(SOAPSenderException, "If caller has provided info. not applicable to the context.")
  @Throws(SOAPSenderException, "If SpecialHandling is specified (it can not be specified when creating a new PolicyPeriod)")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no policy exists with the given number")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function renewPolicyPeriod(renewalInfo: RenewalInfo, transactionId: String): String {
    require(renewalInfo, "renewalInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> renewalInfo.executeRenewalBI(), transactionId)
  }

  /**
   * Rewrite an existing policy period.
   *
   * @param rewriteInfo information to rewrite the policy period
   * @return the Rewrite billing instruction's public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(SOAPSenderException, "If caller has provided info. not applicable to the context.")
  @Throws(SOAPSenderException, "If SpecialHandling is specified (it can not be specified when creating a new PolicyPeriod)")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no policy exists with the given number")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function rewritePolicyPeriod(rewriteInfo : RewriteInfo, transactionId : String): String {
    require(rewriteInfo, "rewriteInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> rewriteInfo.executeRewriteBI(), transactionId)
  }

  /**
   * Add charges like fees to a PolicyPeriod.
   *
   * @param policyPeriodGeneralInfo information for creating the General BI on an existing policy period
   * @param transactionId the unique id to make this call idempotent
   * @return the General billing instruction's public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(SOAPException, "If the charge category is not Fee or General.")
  @Throws(AlreadyExecutedException, "If the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no policy period exists with the given PCPolicyPublicID and TermNUmber or PolicyNumber and TermNumber")
  @Throws(RequiredFieldException, "If policyPeriodGeneralInfo is null")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  function addPolicyPeriodGeneralCharges(policyPeriodGeneralInfo : PolicyPeriodGeneralInfo, transactionId : String) : String {
    require(policyPeriodGeneralInfo, "policyPeriodGeneralInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> policyPeriodGeneralInfo.executeGeneralBI(), transactionId)
  }

  /**
   * Add charges like fees/subrogation to an Account.
   *
   * @param accountGeneralInfo information for creating the General BI on an existing account
   * @param transactionId the unique id to make this call idempotent
   * @return the AccountGeneral billing instruction's public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(SOAPException, "If the charge category is not Fee or General.")
  @Throws(AlreadyExecutedException, "If the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no account exists with the given number")
  @Throws(RequiredFieldException, "If accountGeneralInfo is null")
  function addAccountGeneralCharges(accountGeneralInfo: AccountGeneralInfo, transactionId: String): String {
    require(accountGeneralInfo, "accountGeneralInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> accountGeneralInfo.executeGeneralBI(), transactionId)
  }

  /**
   * Add Collateral charges to a Collateral or unsegregated CollateralRequirement
   *
   * @param collateralInfo information for creating Collateral BI on existing Collateral or CollateralRequirement
   * @param transactionId the unique id to make this call idempotent
   * @return the CollateralBI public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no account exists with the given number")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function addCollateralCharges(collateralInfo: CollateralInfo, transactionId: String): String {
    require(collateralInfo, "collateralInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> collateralInfo.executeCollateralBI(), transactionId)
  }

  /**
   * Add Collateral charges to a segregated CollateralRequirement
   *
   * @param collateralInfo information for creating SegregatedCollReqBI on existing CollateralRequirement
   * @param transactionId the unique id to make this call idempotent
   * @return the SegregatedCollReqBI public id
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no account exists with the given number")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function addSegregatedCollateralCharges(collateralInfo: CollateralInfo, transactionId: String): String {
    require(collateralInfo, "collateralInfo")
    return BillingAPITransactionUtil.executeBillingInstruction(\bundle -> collateralInfo.executeSegregatedCollateralBI(), transactionId)
  }

  /**
   * Return an array of all agency bill plans in BC
   *
   * @return array of agency bill plan info objects
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  function getAllAgencyBillPlans() : AgencyBillPlanInfo[] {
    return AgencyBillPlan.finder
        .findAllAvailablePlans<AgencyBillPlan>(AgencyBillPlan)
        .map(\ plan -> {
          var planInfo = new AgencyBillPlanInfo()
          planInfo.copyPlanCurrencyInfo(plan)
          return planInfo
        })
        .toTypedArray()
  }

  /**
   * Return an array of all Commission Plans in BC
   * @return array of commission plan info objects
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  function getAllCommissionPlans() : CommissionPlanInfo[] {
    return CommissionPlan.finder
        .findAllAvailablePlans<CommissionPlan>(CommissionPlan)
        .map(\ plan -> {
          var planInfo = new CommissionPlanInfo()
          planInfo.copyCommissionPlanInfo(plan)
          return planInfo
        })
        .toTypedArray()
  }

  /**
   * Returns the CommissionSubPlan that is the best match for the given (new) policy period using the given producer
   * code.  Does not persist anything - this method should be used to preview what the eventual commission a producer
   * could expect to get.
   *
   * @param policyPeriod   a new policy period.  If the policy period already exists, then a BadIdentifierException is
   *                       thrown.
   * @param producerCodeID the publicID of a producer code
   * @return CommissionSubPlanInfo  that has info. about the about the best match
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If issuePolicyInfo or producerCodePublicID is null.")
  @Throws(BadIdentifierException, "If a policy period  with the given PCPolicyPublicID and TermNumber already exists, or producerCodePublicID is invalid.")
  function previewApplicableCommissionSubPlan(issuePolicyInfo: IssuePolicyInfo, producerCodePublicID: String): CommissionSubPlanInfo {
    require(issuePolicyInfo, "issuePolicyInfo")
    var producerCode = WebserviceEntityLoader.loadByPublicID<ProducerCode>(producerCodePublicID, "producerCodePublicID")
    var commissionSubPlanInfo: CommissionSubPlanInfo
    runWithNonPersistentBundle(\ bundle -> {
      var commissionSubPlan = producerCode.CommissionPlan.getApplicableSubPlan(issuePolicyInfo.toIssuanceForPreview().PolicyPeriod)
      commissionSubPlanInfo = new CommissionSubPlanInfo(commissionSubPlan)
    })
    return commissionSubPlanInfo
  }

  /**
   * Populates CommissionRateOverrides on an array of {@link ChargeInfos} by getting the commission rate for each charge
   * on the given {@link IssuePolicyInfo}. This method, combined with {@link #previewApplicableCommissionSubPlan(IssuePolicyInfo, String)}
   * can be used to preview the expected commission rates that a new policy will generate.
   * <em>Note: In the default configuration, only {@link PolicyRole.TC_PRIMARY} is supported.</em>
   *
   * @param issuePolicyInfo     The policy period issuance info to be previewed
   * @param policyRoleCode      The policy role to be previewed
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(BadIdentifierException, "If there is no existing CommissionSubPlan with the given PublicID, or policy role exists with the given code.")
  function previewCommissionRates(issuePolicyInfo: IssuePolicyInfo, policyRoleCode: String): ChargeInfo[] {
    require(issuePolicyInfo, "issuePolicyInfo")
    require(policyRoleCode, "policyRoleCode")

    var chargeInfos = Lists.newArrayList<ChargeInfo>()
    var policyRole = PolicyRole.get(policyRoleCode)
    if (policyRole == null) {
      throw new BadIdentifierException(DisplayKey.get("BillingAPI.Error.PolicyRoleNotFound", policyRoleCode))
    }
    runWithNonPersistentBundle(\ bundle -> {
      var issuance = issuePolicyInfo.toIssuanceForPreview()
      var policyPeriod = issuance.PolicyPeriod
      var policyCommission = policyPeriod.getDefaultPolicyCommissionForRole(policyRole)

      for (var charge in policyPeriod.Charges) {
        var chargeInfo = new ChargeInfo()
        chargeInfo.copyChargeInfo(charge)
        if (policyCommission != null) {
          var commissionRate = policyCommission.getCommissionRateAsBigDecimal(charge)
          if (commissionRate != null) {
            chargeInfo.ChargeCommissionRateOverrideInfos.add(new ChargeInfo_ChargeCommissionRateOverrideInfos() {
                : Role = policyRoleCode,
                : Rate = commissionRate
            })
          }
        }
        chargeInfos.add(chargeInfo)
      }
    })
    return chargeInfos.toTypedArray()
  }

  /**
   * Create a new {@link Producer producer} as specified in the
   *    {@link PCNewProducerInfo}.
   *
   * The {@code Producer} attributes that will be set are the {@link
   * Producer#PublicID PublicID}, {@link Producer#Name Name}, {@link
   * Producer#Tier Tier} (defaulted if not specified), {@link
   * Producer#PrimaryContact PrimaryContact}, and {@link Producer#AgencyBillPlan
   * AgencyBillPlan} (<code>null</code> if not specified).
   *<p/>
   * In a multi-currency installation, this will also create corresponding
   * splinter {@code Producer}s for the specified {@code Currencies}. {@link
   * AgencyBillPlan}s are optionally specified.
   *
   * @param newProducerInfo The information with which to create the {@link
   *                        Producer}:
   *                        <blockquote><dl>
   *                            <dt>PreferredCurrency <dd>The {@link typekey.Currency}
   *                                 in which the {@code Producer} prefers to do
   *                                 business. (This will be the {@code
   *                                 Currency} of the main {@code Producer} in a
   *                                 multi-currency producer group.)
   *                            <dt>PublicID <dd>The unique public identifier
   *                                 that identifies the {@code Producer} (which
   *                                 will be the main one when multiple
   *                                 currencies are specified).
   *                             <dt>ProducerName <dd>The name string to be
   *                                 associated with the {@code Producer} (and
   *                                 any multi-currency splinters}.
   *                             <dt>Tier <dd>The {@link ProducerTier} for the
   *                                 {@code Producer} (and any multi-currency
   *                                 splinters).
   *                             <dt>PrimaryContact <dd>The primary {@link
   *                                 ProducerContact} for the {@code Producer}
   *                                 (and any multi-currency splinters).
   *                             <dt>AgencyBillPlanIDs <dd>(Optional) A list of
   *                                 {@link AgencyBillPlan} {@code PublicID}s,
   *                                 one (or none) per each {@link typekey.Currency} to
   *                                 be supported.
   *                             <dt>Currencies<dd>(Single currency optional) A
   *                                 list of {@code Currency} codes that the
   *                                 {@code Producer} should support.
   *                          </dl>
   *                        (The AgencyBillPlanInfos is not needed and will be
   *                        ignored.)
   *                        </blockquote>
   * @param transactionId the transaction id to make this call idempotent
   * @return The {@code PublicID} identifying string of the {@code Producer}
   *         that was created.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(BadIdentifierException, "If no AgencyBill Plan exists with a given PublicID")
  @Throws(DataConversionException, "If more than one AgencyBill Plan is specified with a given Currency")
  function createProducer(newProducerInfo: PCNewProducerInfo, transactionId: String): String {
    require(newProducerInfo, "newProducerInfo")
    var producer = BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var producer = newProducerInfo.toNewProducer(bundle)
      return producer
    }, transactionId)
    return producer.PublicID
  }

  /**
   * Update an existing {@link Producer producer} as specified in the
   *    {@link PCProducerInfo}.
   *
   * The {@code Producer} attributes that will be updated are the {@link
   * Producer#Name Name}, {@link Producer#Tier Tier} (defaulted if not
   * specified), {@link Producer#PrimaryContact PrimaryContact}, and {@link
   * Producer#AgencyBillPlan AgencyBillPlan} (<code>null</code> if not
   * specified).
   *<p/>
   * In a multi-currency installation, this will also update the corresponding
   * splinter {@code Producer}s belonging to the main {@code Producer}'s
   * currency group. {@link AgencyBillPlan}s are optionally specified.
   *
   * @param producerInfo The information with which to update the {@link
   *                     Producer}:
   *                     <blockquote><dl>
   *                         <dt>PublicID <dd>The public identifier that
   *                             identifies the {@code Producer} (or the main
   *                             one when multiple currencies are specified).
   *                         <dt>ProducerName <dd>The name string to be
   *                             associated with the {@code Producer} (and any
   *                             multi-currency splinters}.
   *                         <dt>Tier <dd>The {@link ProducerTier} for the
   *                             {@code Producer} (and any multi-currency
   *                             splinters).
   *                         <dt>PrimaryContact <dd>The primary {@link
   *                             ProducerContact} for the {@code Producer}
   *                             (and any multi-currency splinters).
   *                         <dt>AgencyBillPlanIDs <dd>(Optional) A list of
   *                             {@link AgencyBillPlan} {@code PublicID}s, one
   *                             per each {@link typekey.Currency} to be updated on the
   *                             existing {@code Producer} for that {@code
   *                             Currency}, or specified on any new {@code
   *                             Producer}s to be added for new currencies to be
   *                             supported.
   *                         <dt>Currencies <dd>(Optional) A list of {@code
   *                             Currency} codes that the {@code Producer}
   *                             should now support.
   *                       </dl>
   *                     (The AgencyBillPlanInfos is not needed and will be
   *                     ignored.)
   *                     </blockquote>
   * @param transactionId the transaction id to make this call idempotent
   * @return The {@code PublicID} identifying string of the {@code Producer}
   *         that was updated.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(BadIdentifierException, "If no producer exists with the given public id")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function updateProducer(producerInfo: PCProducerInfo, transactionId: String): String {
    require(producerInfo, "producerInfo")
    return BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var producer: Producer
      producer = producerInfo.toProducer(bundle)
      return producer.PublicID
    }, transactionId)
  }

  /**
   * Return true if the producer with the same name exist
   * @param producerId the public id of the producer to check
   * @return true if the producer exists, otherwise false
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function isProducerExist(producerId: String): boolean {
    require(producerId, "producerId")
    return !Query.make(Producer).compare("PublicID", Equals, producerId).select().Empty
  }

  /**
   * @param producerId the {@code PublicID} of the {@link ProducerCode} for which
   *                   information is to be returned
   * @return The {@link PCProducerInfo} containing values set from the
   *         identified {@link Producer}.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(BadIdentifierException, "If no producer exists with the given public identifier")
  function getProducerInfo(producerId : String) : PCProducerInfo {
    require(producerId, "producerId")
    final var producerInfo = new PCProducerInfo() {
        : PublicID = producerId
    }
    producerInfo.loadInformation()
    return producerInfo
  }

  /**
   * Create a new {@link ProducerCode producer code} with the values and parent
   *    {@link Producer} as specified in the {@link NewProducerCodeInfo}.
   *
   * This will do nothing if the identified {@code ProducerCode} already exists.
   *
   * The {@code ProducerCode} attributes that will be set are the {@link
   * ProducerCode#PublicID PublicID}, {@link ProducerCode#Code Code}, {@link
   * Producer#Active Active} flag, and {@link ProducerCode#CommissionPlan
   * CommissionPlan} (defaulted if not specified).
   *<p/>
   * In a multi-currency installation, this will also create corresponding
   * {@code ProducerCode}s for the specified {@code Currencies} on the owning
   * {@link ProducerCode#Producer Producer} parent's currency splinter
   * producers. {@link CommissionPlan}s are optionally specified but will be
   * assigned by default if not (which will be an error if it does not exist).
   *
   * @param producerCodeInfo The information with which to create the {@link
   *                         ProducerCode}:
   *                         <blockquote><dl>
   *                             <dt>ProducerPublicID <dd>The unique public
   *                                 identifier that identifies the parent
   *                                 {@link Producer} of the code.
   *                             <dt>PublicID <dd>The unique public identifier
   *                                 that identifies the {@code ProducerCode}
   *                                 (which will be the main one when multiple
   *                                 currencies are specified).
   *                             <dt>Code <dd>The code string to be associated
   *                                 with the {@code ProducerCode} (and any
   *                                 multi-currency splinters}.
   *                             <dt>Active <dd>Whether the {@code
   *                                 ProducerCode} (and any multi-currency
   *                                splinters) is to be active.
   *                             <dt>CommissionPlanIDs <dd>(Optional) A list of
   *                                 {@link CommissionPlan} {@code PublicID}s,
   *                                 one per each {@link typekey.Currency} to be
   *                                 supported.
   *                             <dt>Currencies<dd>(Optional, single currency) A
   *                                 list of {@code Currency} codes that the
   *                                 {@code ProducerCode} should support.
   *                           </dl>
   *                       (The CommissionPlanInfos is not needed and will be
   *                       ignored.)
   *                       </blockquote>
   * @param transactionId the transaction id to make this call idempotent
   * @return The {@code PublicID} identifying string of the {@code ProducerCode}
   *         that was created.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function createProducerCode(producerCodeInfo: NewProducerCodeInfo, transactionId: String): String {
    require(producerCodeInfo, "producerCodeInfo")
    if (!Query.make(ProducerCode).compare("PublicID", Equals, producerCodeInfo.PublicID).select().Empty) {
      return producerCodeInfo.PublicID
    }
    var producerCode = BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var producerCode = producerCodeInfo.toNewProducerCode(bundle)
      return producerCode
    }, transactionId)
    return producerCode.PublicID
  }

  /**
   * Update an existing {@link ProducerCode producer code} as specified in the
   *    {@link ProducerCodeInfo}.
   *
   * The {@code ProducerCode} attributes that can be updated are the {@link
   * ProducerCode#Code Code}, {@link Producer#Active Active} flag, and {@link
   * ProducerCode#CommissionPlan CommissionPlan} (optional if not specified).
   *<p/>
   * In a multi-currency installation, this will also update the attributes of
   * the corresponding {@code ProducerCode}s found on the owning {@link
   * ProducerCode#Producer Producer} parent's currency splinter producers.
   * {@link CommissionPlan}s are optionally specified but will be assigned by
   * default if not (which will be an error if it does not exist).
   *
   * @param producerCodeInfo The information with which to update the {@link
   *                         ProducerCode} as well as which identifies it:
   *                         <blockquote><dl>
   *                             <dt>PublicID <dd>The public identifier that
   *                                 identifies the {@code ProducerCode} (or
   *                                 main one when multiple currencies are
   *                                 supported).
   *                             <dt>Code <dd>The code string to be associated
   *                                 with the {@code ProducerCode} (and any
   *                                 multi-currency splinters}.
   *                             <dt>Active <dd>Whether the {@code
   *                                 ProducerCode} (and any multi-currency
   *                                splinters) is to be active.
   *                             <dt>CommissionPlanIDs <dd>(Optional) A list of
   *                                 {@link CommissionPlan} {@code PublicID}s,
   *                                 one per each {@link typekey.Currency} to be updated
   *                                 on the existing {@code ProducerCode} for
   *                                 that {@code Currency}, or specified on any
   *                                 new {@code ProducerCode}s to be added for
   *                                 new currencies to be supported.
   *                             <dt>Currencies <dd>(Optional) A list of {@code
   *                                 Currency} codes that the {@code
   *                                 ProducerCode} should now support.
   *                         </dl>
   *                       (The CommissionPlanInfos is not needed and will be
   *                       ignored.)
   *                       </blockquote>
   * @param transactionId the transaction id to make this call idempotent
   * @return The {@code PublicID} identifying string of the {@code ProducerCode}
   *         that was updated.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(BadIdentifierException, "If no producer code exists with the given public identifier")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function updateProducerCode(producerCodeInfo: ProducerCodeInfo, transactionId: String): String {
    require(producerCodeInfo, "producerCodeInfo")
    return BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var producerCode = producerCodeInfo.toProducerCode(bundle)
      return producerCode.PublicID
    }, transactionId)
  }

  /**
   * Return true if the producer code with the given code exists.
   *
   * @param code the code of the producer code
   * @return true if the producer code exist
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function isProducerCodeExist(producerId: String, code: String): boolean {
    require(producerId, "producerId")
    var q = Query.make(Producer)
    q.compare("PublicID", Equals, producerId)
    var producerCodeTable = q.join(ProducerCode, "Producer")
    producerCodeTable.compare("Code", Equals, code)
    return !q.select().Empty
  }

  /**
   * @param publicID the {@code PublicID} of the {@link ProducerCode} for which
   *                 information is to be returned
   * @return The {@link ProducerCodeInfo} containing values set from the
   *         identified {@link ProducerCode}.
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(BadIdentifierException, "If no producer code exists with the given public identifier")
  function getProducerCodeInfo(publicID : String) : ProducerCodeInfo {
    require(publicID, "publicID")
    final var producerCodeInfo = new ProducerCodeInfo() {
      : PublicID = publicID
    }
    producerCodeInfo.loadInformation()
    return producerCodeInfo
  }

  /**
   * Update a contact with the given contact's public id
   * @param contactInfo information necessary for updating the contact
   * @@param transactionId the transaction id to make this call idempotent
   * @return an array of public ids of all the contacts that were updated
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function updateContact(contactInfo: PCContactInfo, transactionId: String): String[] {
    require(contactInfo, "contactInfo")
    return BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var contact = contactInfo.toContact(bundle)
      setAccountNamesOfContactFrom(contactInfo, bundle)
      return new String[]{contact.PublicID}
    }, transactionId)
  }

  private function setAccountNamesOfContactFrom(contactInfo: PCContactInfo, bundle: Bundle) {
    for (accountNumber in contactInfo.AccountNumbers) {
      var account = findAccountResult(accountNumber).AtMostOneRow
      if (account != null) {
        var currencyGroup = account.AccountCurrencyGroup
        if (currencyGroup != null) {
          for (splinterAccount in currencyGroup.findAccounts()) {
            splinterAccount = bundle.add(splinterAccount)
            splinterAccount.AccountName = contactInfo.AccountName
            splinterAccount.AccountNameKanji = contactInfo.AccountNameKanji
          }
        } else {
          account = bundle.add(account)
          account.AccountName = contactInfo.AccountName
          account.AccountNameKanji = contactInfo.AccountNameKanji
        }
      }
    }
  }

  /**
   * Gets all of the available payment plans in the system.  Only plans which are effective (ie effectiveDate &lt=
   * current date &lt= expiration date) are returned.
   *
   * @return the available payment plans
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  function getAllPaymentPlans() : PaymentPlanInfo[] {
    return findPaymentPlansFor(null)
  }

  /**
   * Gets all of the available payment plans for an account filtered by the given currency
   * If currency is null, all payment plans for the account will be returned
   *
   * @param accountNumber information to identify the account
   * @param currency the currency to filter on
   * @return the payment plans associated with an account
   */
  @Throws(BadIdentifierException, "If no account exists with the given account number")
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  function getPaymentPlansForAccount(accountNumber: String, currency: Currency): PaymentPlanInfo[] {
    final var acct = findAccountIdentifiedBy(accountNumber)

    if (acct.ListBill && acct.Currency != currency) {
      throw new ServerStateException(
          DisplayKey.get("Webservice.Error.CurrencyMustMatchListBillAccount", currency, accountNumber))
    }

    // For all non-list bill accounts, return all available pay plans with Currency
    final var currentDate = Date.CurrentDate
    return not acct.ListBill
        ? findPaymentPlansFor(currency)
        : convertPaymentPlansToDTO(acct.PaymentPlans
            .where(\plan -> plan.EffectiveDate <= currentDate
                and (plan.ExpirationDate == null
                    or plan.ExpirationDate > currentDate)
                and plan.UserVisible))
  }

  /**
   * Updates the given policy period with the new payment plan.
   * The invoiceItem filter predicate can be customized in Gosu to filter invoice items based on any criteria.
   * @param policyPeriodPublicID information to identify the policy period
   * @param paymentPlanInfo the new payment plan
   * @param itemFilterType filter to select what current items on the policy period to reverse or remove before building new invoice items; if {@code null}, defaults to all items
   * @param includeDownPaymentItems whether to remove or reverse downpament items before building new invoice items. This is ignored when itemFilterType is AllItems
   * @param reverseAndRedistributePayments Whether to reverse and redistribute or just reverse affected payment items
   *
   * @return whether or not the change was successful
   */
  @Throws(BadIdentifierException, "If no payment plan exists to match the given info")
  @Throws(BadIdentifierException, "Policy period public id is not valid")
  @Throws(RequiredFieldException, "Policy period public id cannot be null")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  function changePaymentPlan(policyPeriodPublicID: String,
                             paymentPlanInfo : PaymentPlanInfo,
                             itemFilterType : InvoiceItemFilterType,
                             includeDownPaymentItems : boolean,
                             reverseAndRedistributePayments : boolean) : boolean  {
    var policyPeriod = WebserviceEntityLoader.loadPolicyPeriod(policyPeriodPublicID)
    if (policyPeriod.Archived) {
      throw new EntityStateException(DisplayKey.get("Webservice.Error.OperationNotPermittedOnArchivedPolicyPeriod"))
    }
    itemFilterType = itemFilterType ?: TC_ALLITEMS

    Transaction.runWithNewBundle(\ bundle -> {
      bundle.add(policyPeriod)
      var paymentPlan = WebserviceEntityLoader.loadPaymentPlan(paymentPlanInfo.PublicID)
      var paymentHandling = reverseAndRedistributePayments ?
          PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute :
          PaymentHandlingDuringPaymentPlanChange.ReverseOnly
      var changer = new PaymentPlanChanger(policyPeriod, paymentPlan, paymentHandling,
          InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()
    })
    return policyPeriod.PaymentPlan.PublicID == paymentPlanInfo.PublicID
  }


  /**
   * Waive a final audit in BC. This function will set the policy period's closure status from
   * OPEN_LOCK to OPEN and make the policy period do not require final audit anymore.
   *
   * @param policyPeriodInfo information to identify the policy period
   * @return the public id of the policy period
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function waiveFinalAudit(policyPeriodInfo : PCPolicyPeriodInfo, transactionId : String) : String {
    require(policyPeriodInfo, "policyPeriodInfo")
    return BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var period = policyPeriodInfo.findPolicyPeriodForUpdate()
      period = bundle.add(period)
      period.waiveFinalAudit()
      return period.PublicID
    }, transactionId)
  }

  /**
   * Schedule a final audit in BC. This function will set the policy period's closure status from
   * OPEN or CLOSE to OPEN_LOCK and make the policy period require final audit.
   *
   * @param policyPeriodInfo information to identify the policy period
   * @return the public id of the policy period
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(AlreadyExecutedException, "if the SOAP request is already executed")
  @Throws(EntityStateException, "If the PolicyPeriod has been archived")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function scheduleFinalAudit(policyPeriodInfo : PCPolicyPeriodInfo, transactionId : String): String {
    require(policyPeriodInfo, "policyPeriodInfo")
    return BillingAPITransactionUtil.executeTransaction(\bundle -> {
      var period = policyPeriodInfo.findPolicyPeriodForUpdate()
      period = bundle.add(period)
      period.scheduleFinalAudit()
      return period.PublicID
    }, transactionId)
  }

  /**
   * Retrieve the information about the policy period including the current payment plan and
   * billing method
   *
   * @param policyPeriodInfo information to identify the policy period
   * @return the information about the policy period
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function getPolicyPeriod(policyPeriodInfo: PCPolicyPeriodInfo): IssuePolicyInfo {
    require(policyPeriodInfo, "policyPeriodInfo")
    var period = policyPeriodInfo.findByPolicyPublicIDOrPolicyNumber(policyPeriodInfo.PCPolicyPublicID, policyPeriodInfo.TermNumber, policyPeriodInfo.PolicyNumber)
    if (period == null) {
      return null
    }
    var accountNumber: String
    if (period.OverridingPayerAccount != null) {
      accountNumber = period.OverridingPayerAccount.AccountCurrencyGroup != null
          ? period.OverridingPayerAccount.AccountCurrencyGroup.MainAccount.AccountNumber
          : period.OverridingPayerAccount.AccountNumber
    }
    return new IssuePolicyInfo() {
        : PaymentPlanPublicId = period.PaymentPlan.PublicID,
        : BillingMethodCode = period.BillingMethod.Code,
        : AltBillingAccountNumber = accountNumber,
        : InvoiceStreamId = period.OverridingInvoiceStream.PublicID,
        : Currency = period.Currency.Code
    }
  }

  /**
   * Retrieves {@link UnappliedFund}s {@link PCUnappliedInfo information} for
   *    those associated with the identified {@link Account account}.
   *
   * Delete this method if you are running Multi-Currency mode!
   *
   * @param accountNumber the account number of the {@code account}
   * @return An array of information on the unapplied funds for the
   *         {@code account}
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  function getUnapplieds(accountNumber: String): PCUnappliedInfo[] {
    return getUnappliedFunds(accountNumber, null)
  }

  /**
   * Retrieves {@link UnappliedFund}s {@link PCUnappliedInfo information} for
   *    those associated with the identified {@link Account account} for the
   *    {@link typekey.Currency currency} or an empty list if the {@code account} does
   *    not exist.
   *
   * @param accountNumber the account number of the {@code account}
   * @param currency the currency for which the {@code UnappliedFunds} should
   *                 apply; if {@code null}, then this defaults to the
   *                 identified {@code account}
   * @return An array of information on the unapplied funds for the
   *         {@code account} and {@code currency}
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(BadIdentifierException, "If no account exists with the given account number")
  function getUnappliedFunds(
      accountNumber : String, currency : Currency = null) : PCUnappliedInfo[] {
    final var acct = findCurrencyAccount(accountNumber, currency)
    if (acct == null) {
      return {} // no account or splinter so no unapplieds...
    }
    return acct.UnappliedFunds
        .map(\ unappliedFund -> new PCUnappliedInfo(unappliedFund))
  }

  /**
   * Retrieves {@link PaymentInstrumentRecord} data for all {@link
   *    PaymentInstrument}s associated with the identified {@link Account
   *    account} for the {@link typekey.Currency currency} or a list of the default
   *    immutable instruments if the {@code account} does not exist.
   *
   * @param accountNumber the account number of the {@code account}
   * @param currency the currency for which the instruments should apply;
   *                 if {@code null}, then this defaults to the identified
   *                 {@code account}
   * @return An array of information on payment instruments for the
   *         {@code account}
   */
  @Throws(SOAPServerException, "If communication error or any other problem occurs.")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(BadIdentifierException, "If no account exists with the given account number")
  function getAccountPaymentInstruments(accountNumber : String,
      currency : Currency = null) : PaymentInstrumentRecord[] {
    final var account = findCurrencyAccount(accountNumber, currency)
    // if account or splinter does not (yet) exist, return default...
    final var instruments = (account == null)
        // of the immutables, responsive only...
        ? {PaymentInstrumentFactory.ResponsivePaymentInstrument}
        // only owned or of the immutables, responsive...
        : account.PaymentInstruments.where(\ instr -> instr.Account == account
            or instr == PaymentInstrumentFactory.ResponsivePaymentInstrument)
    return instruments
        .map(\ instrument -> PaymentInstruments.toPCRecord(instrument))
        .toTypedArray()
  }

  /**
   * Creates a {@link PaymentInstrument} as described by the {@link
   *    PaymentInstrumentRecord} for the {@link Account} identified by the
   *    {@code AccountNumber} and {@link typekey.Currency}.
   *
   * @param accountNumber the account number of the {@code account}
   * @param currency the currency for which the instrument applies relative to
   *                 the identified {@code account}
   * @param paymentInstrumentRecord the data that describes the {@link
   *                                PaymentInstrument} to be created
   */
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")
  @Throws(BadIdentifierException, "If no account exists with the given account number")
  @Throws(RequiredFieldException, "If required parameter is missing.")
  @Throws(DataConversionException, "The PaymentInstrument identified by the non-null PublicID already exists in the system)")
  @Throws(DataConversionException, "If paymentInstrumentRecord.OneTime is true")
  function createPaymentInstrumentOnAccount(accountNumber : String, currency : Currency,
      paymentInstrumentRecord : PaymentInstrumentRecord) : PaymentInstrumentRecord {
    if (CurrencyUtil.isMultiCurrencyMode()) {
      require(currency, "Currency")
    }
    PaymentInstruments.validateForCreation(paymentInstrumentRecord)

    var newInstrument : PaymentInstrument
    Transaction.runWithNewBundle(\ bundle -> {
        final var account =
            findAccountOrSiblingForCurrency(accountNumber, currency, bundle)
        newInstrument = PaymentInstruments.toEntity(paymentInstrumentRecord)
        newInstrument.Account = account
      })
    return PaymentInstruments.toPCRecord(newInstrument)
  }

  /**
   * Look-up and return an {@link Account} identified by its account number
   *    and the specified currency if any.
   *
   * This looks up an account identified by the account number. If it matches
   * the specified currency (or the currency is not specified [{@code null}],
   * then it is returned. Otherwise, the splinter account for the currency
   * is looked up and returned.
   *
   * @param accountNumber the account number of the {@code account}
   * @param currency the (optional) currency that identifies the specific
   *                 {@code account}
   * @return The main or splinter {@link Account} for the {@code currency} and
   *         identified by the account number, if it exists.
   */
  private function findCurrencyAccount(
      accountNumber : String, currency : Currency = null) : Account {
    require(accountNumber, "accountNumber")
    if (CurrencyUtil.isMultiCurrencyMode()) {
      require(currency, "Currency")
    }
    var account = findAccountResult(accountNumber).AtMostOneRow
    if (account != null && currency != null && account.Currency != currency) {
      /* get associated splinter account for different currency... */
      account = (account.AccountCurrencyGroup == null)
          ? null : findExistingAccountForCurrency(
              account.AccountCurrencyGroup, currency)
    }
    return account
  }

  internal static function findExistingAccountForCurrency(
      accountGroup : MixedCurrencyAccountGroup, currency : Currency) : Account {
    // might've already splintered but not yet committed...
    final var currentBundle = Transaction.Current
    final var createdSplinter = (currentBundle == null)
        ? null
        : currentBundle.getBeansByRootType(Account)
            .firstWhere(\ b -> b typeis Account
                && b.AccountCurrencyGroup == accountGroup && b.Currency == currency) as Account

    return createdSplinter ?: Query.make(Account)
        .compare("Currency", Equals, currency)
        .subselect("ID", CompareIn,
            Query.make(AccountCurrencyGroup)
                .compare("ForeignEntity", Equals, accountGroup), "Owner")
        .select().AtMostOneRow
  }

  private function convertPaymentPlansToDTO(plans: Iterable<PaymentPlan>)
      : PaymentPlanInfo[] {
    return plans
        .map(\paymentPlan -> {
          var paymentPlanInfo = new PaymentPlanInfo()
          paymentPlanInfo.copyPaymentPlanInfo(paymentPlan)
          return paymentPlanInfo
        })
        .toTypedArray()
  }

  private function findPaymentPlansFor(currency : Currency) : PaymentPlanInfo[] {
    return convertPaymentPlansToDTO(PaymentPlan.finder
        .findAllAvailablePlans<PaymentPlan>(PaymentPlan, currency,
            \ query -> query.compare("UserVisible", Equals, true)))
  }

  private function findAllSplinterIdsFor(ids: Set<Key>): Set<Key> {
    final var groupsQuery = Query.make(AccountCurrencyGroup)
    groupsQuery.compareIn("Owner", ids.toArray())
    final var resultsQuery = Query.make(AccountCurrencyGroup).withDistinct(true)
    resultsQuery.subselect("ForeignEntity", CompareIn, groupsQuery, "ForeignEntity")

    return resultsQuery.select().map(\currencyGroup -> currencyGroup.Owner.ID).toSet()
  }

  static internal function createAccountForCurrency(parentAccount: Account, currency: Currency): Account {
    Preconditions.checkState(!parentAccount.ListBill,
        DisplayKey.get("Java.Error.Account.ListBill.SplinterAccount"))

    parentAccount = Transaction.Current.add(parentAccount)// ensure writable...

    var account = parentAccount.createSplinterAccountForCurrency(currency)
    account.AccountNameKanji = parentAccount.AccountNameKanji
    account.InvoiceDayOfMonth = parentAccount.InvoiceDayOfMonth
    account.InvoiceDeliveryType = parentAccount.InvoiceDeliveryType
    account.BillingPlan =
        BillingPlan.finder.findFirstActivePlan(BillingPlan, account.Currency)
    account.DelinquencyPlan =
        DelinquencyPlan.finder.findFirstActivePlan(DelinquencyPlan, account.Currency)
    account.BillingLevel = parentAccount.BillingLevel
    account.ServiceTier = parentAccount.ServiceTier

    for (var contact in parentAccount.Contacts) {
      var newContact = new AccountContact(account.Bundle)
      newContact.Account = account
      newContact.Contact = contact.Contact
      newContact.PrimaryPayer = contact.PrimaryPayer
      for (var contactRole in contact.Roles) {
        var copiedContactRole = new AccountContactRole(account.Bundle)
        copiedContactRole.Role = contactRole.Role
        newContact.addToRoles(copiedContactRole)
      }
      account.addToContacts(newContact)
    }
    return account
  }
}
