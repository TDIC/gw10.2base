package gw.webservice.policycenter.bc1000

uses gw.xml.ws.annotation.WsiExportable

uses java.math.BigDecimal

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/CommissionSubPlanRateInfo" )
@Export
final class CommissionSubPlanRateInfo {
  var _rate : BigDecimal         as Rate
  var _role : PolicyRole as Role

  construct() {}

  construct(policyRole : PolicyRole, rate : BigDecimal) {
    this.Role = policyRole
    this.Rate = rate
  }

}