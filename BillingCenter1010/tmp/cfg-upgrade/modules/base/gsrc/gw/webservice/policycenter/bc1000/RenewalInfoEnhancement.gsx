package gw.webservice.policycenter.bc1000

uses gw.api.locale.DisplayKey
uses gw.api.system.BCLoggerCategory
uses gw.api.web.policy.NewPolicyUtil
uses gw.api.webservice.exception.EntityStateException
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewAccountInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.RenewalInfo

/**
 * Defines behavior for the 910 API version of the WSDL entity that specifies
 * the data used to create a {@link Renewal Renewal} billing instruction.
 */
@Export
enhancement RenewalInfoEnhancement : RenewalInfo {
  function executeRenewalBI() : BillingInstruction {
    return executedRenewalBISupplier()
  }

  function findPriorPolicyPeriod() : PolicyPeriod {
    final var priorPeriod = this.findByPolicyPublicIDOrPolicyNumber(
        this.PCPolicyPublicID, TermNumberToFind, PolicyNumberToFind)
    if (priorPeriod != null && priorPeriod.Archived) {
      throw new EntityStateException(
          DisplayKey.get('Webservice.Error.OperationNotPermittedOnArchivedPolicyPeriod')
      )
    }
    return gw.transaction.Transaction.Current.add(priorPeriod) // ensure writable...
  }

  property get PolicyNumberToFind() : String {
    return this.PriorPolicyNumber == null ? this.PolicyNumber : this.PriorPolicyNumber
  }

  property get TermNumberToFind() : int {
    // For the case when the prior policy period does not exist in PC but exist in BC.
    // As the result, PC cannot tell BC about the prior period but BC has to guess base on
    // the current policy period.
    return this.PriorTermNumber == null ? this.TermNumber - 1 : this.PriorTermNumber
  }

  /**
   * WARNING: If the owner/payer accounts don't exist in BC yet, this method will attempt to create
   * invoice previews based off tmp accounts with the system's default settings. This might result
   * in invoices that are slightly different than the ones produced once the policy is actually
   * issued towards the real accounts.
   */
  function toRenewalForPreview() : NewPlcyPeriodBI {
    return toRenewalForPreview(null, null)
  }

  function toRenewalForPreview(ownerAccountInfo : PCNewAccountInfo, overridingPayerInfo : PCNewAccountInfo) : NewPlcyPeriodBI {
    //Must create accounts prior to initialization code, if they don't exist yet
    this.setupAccountForPreview(this.AccountNumber, ownerAccountInfo)
    this.setupAccountForPreview(this.AltBillingAccountNumber, overridingPayerInfo)

    return executedRenewalBISupplier()
  }

  private function createRenewalBI() : NewPlcyPeriodBI {
    var bi : NewPlcyPeriodBI
    final var priorPeriod = findPriorPolicyPeriod()
    if (priorPeriod == null) { // new renewal
      BCLoggerCategory.BILLING_API.info("Could not find prior policy period ${PolicyNumberToFind}-${TermNumberToFind}. Creating New Renewal.")
      bi = createNewPolicyBIInternal()
      this.initPolicyPeriodBIInternal(bi) // can occur after populate when no prior period...
    } else {
      BCLoggerCategory.BILLING_API.info("Renewing policy period ${PolicyNumberToFind}-${TermNumberToFind}.")
      bi = makeRenewalBIInternal(priorPeriod)
    }
    return bi
  }

  private function createNewPolicyBIInternal() : NewRenewal {
    final var owningAccount = this.findOwnerAccount()
    return NewPolicyUtil.createNewRenewal(
        owningAccount, this.createPolicyPeriod(), this.TermNumber)
  }

  private function makeRenewalBIInternal(final priorPeriod : PolicyPeriod) : Renewal {
    final var renewalBI = (priorPeriod.Currency != this.CurrencyValue)
        ? NewPolicyUtil.createCurrencyChangeRenewal(this.findOwnerAccount(), priorPeriod)
        : NewPolicyUtil.createRenewal(priorPeriod)
    this.initPolicyPeriodBIInternal(renewalBI) // must precede populate when prior period exists...
    this.populateIssuanceInfo(renewalBI.NewPolicyPeriod)
    return renewalBI
  }

  function executedRenewalBISupplier(): NewPlcyPeriodBI {
    var bi = createRenewalBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}