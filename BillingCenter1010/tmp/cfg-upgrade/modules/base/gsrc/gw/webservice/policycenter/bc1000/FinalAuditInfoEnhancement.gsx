package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.FinalAuditInfo

/**
 * Defines behavior for the 910 API version of the WSDL entity that specifies
 * the data used to create a final {@link Audit Audit} billing
 * instruction.
 */
@Export
enhancement FinalAuditInfoEnhancement : FinalAuditInfo {
  function executeFinalAuditBI(): BillingInstruction {
    return executedFinalAuditBISupplier()
  }

  /**
   * @deprecated Since 9.0.1. Use {@link #executeFinalAuditBI()} instead.
   */
  @java.lang.Deprecated
  function execute() : String {
    return executeFinalAuditBI().PublicID
  }

  private function createFinalAuditBI() : Audit {
    final var policyPeriod = this.findPolicyPeriod()

    final var bi = new Audit(policyPeriod.Currency)
    bi.AssociatedPolicyPeriod = policyPeriod
    return bi
  }

  function toFinalAuditForPreview() : Audit {
    final var bi = createFinalAuditBI()
    this.initializeBillingInstruction(bi)
    return bi
  }

  function executedFinalAuditBISupplier(): Audit {
    var bi = createFinalAuditBI()
    bi.FinalAudit = true
    bi.TotalPremium = false
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
