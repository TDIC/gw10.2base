package gw.webservice.policycenter.bc1000

uses gw.api.database.Query
uses gw.pl.currency.MonetaryAmount
uses gw.web.policy.PolicySummaryFinancialsHelper
uses gw.xml.ws.annotation.WsiExportable

/**
 * Defines a summary of billing information for a {@link PolicyPeriod} sent to a
 * Policy administration system such as PolicyCenter for display.
 */
@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/PolicyBillingSummary" )
@Export
final class PolicyBillingSummary {
  var _policyTermInfos: PolicyTermInfo[] as PolicyTermInfos
  var _currentOutstanding : MonetaryAmount as CurrentOutstanding
  var _paid : MonetaryAmount as Paid
  var _depositRequirement : MonetaryAmount as DepositRequirement
  var _totalCharges : MonetaryAmount as TotalCharges
  var _writtenOff : MonetaryAmount as WrittenOff
  var _paymentPlanName : String as PaymentPlanName
  var _altBillingAccount : String as AltBillingAccount
  var _invoiceStream : String as InvoiceStream
  var _invoices : PCInvoiceInfo[] as Invoices
  var _billingStatus : DisplayableBillingStatus as BillingStatus
  var _archived : boolean as Archived
  var _retrieved : boolean as Retrieved

  construct() {}

  construct(policyPeriod : PolicyPeriod) {
    _billingStatus = new DisplayableBillingStatus(policyPeriod)

    _policyTermInfos = findAllPolicyPeriods(policyPeriod.Policy)
        .sortBy(\ p -> p.PolicyPerEffDate)
        .map(\ p -> new PolicyTermInfo(p))
    var policyPeriodBalances = new PolicySummaryFinancialsHelper(policyPeriod)
    _currentOutstanding = policyPeriodBalances.OutstandingAmount
    _paid = policyPeriodBalances.PaidAmount
    _totalCharges = policyPeriodBalances.TotalValue
    _writtenOff = policyPeriod.Archived
        ? policyPeriod.PolicyPeriodArchiveSummary.WrittenOff
        : policyPeriodBalances.WrittenOffAmount
    _paymentPlanName = policyPeriod.PaymentPlan.Name

    var overridingPayerAccount : Account
    overridingPayerAccount = policyPeriod.OverridingPayerAccount
    if (policyPeriod.OverridingPayerAccount != null){
      overridingPayerAccount = policyPeriod.OverridingPayerAccount.AccountCurrencyGroup != null
          ? policyPeriod.OverridingPayerAccount.AccountCurrencyGroup.MainAccount
          : policyPeriod.OverridingPayerAccount
    }
    _altBillingAccount = overridingPayerAccount.AccountNumber
    if (overridingPayerAccount.AccountName != null) {
      _altBillingAccount += " (${overridingPayerAccount.AccountName})"
    }

    _invoiceStream = policyPeriod.OverridingInvoiceStream.PCDisplayName
    // TODO: _depositRequirement = policyPeriod.DepositRequirement
    _invoices = PCInvoiceInfo.invoicesFor(policyPeriod)

    _archived = policyPeriod.Archived
    _retrieved = policyPeriod.Retrieved
  }

  private function findAllPolicyPeriods(policy : Policy) : PolicyPeriod[] {
    if (policy.Account.AccountCurrencyGroup == null) {
      // single currency...
      return policy.PolicyPeriods
    }
    // retrieve all "splinter" PolicyPeriods across currencies...
    final var policyQuery = Query.make(Policy)
    policyQuery.compare("PCPublicID", Equals, policy.PCPublicID)
    return Query.make(PolicyPeriod)
      .subselect("Policy", CompareIn, policyQuery, "ID")
      .select()
      .toTypedArray()
  }
}