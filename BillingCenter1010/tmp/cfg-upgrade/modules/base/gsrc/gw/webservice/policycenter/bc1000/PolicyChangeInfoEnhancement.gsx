package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.PolicyChangeInfo

/**
 * Defines behavior for the 910 API version of the WSDL entity that specifies
 * the data used to create a {@link PolicyChange PolicyChange} billing
 * instruction.
 */
@Export
enhancement PolicyChangeInfoEnhancement : PolicyChangeInfo {
  function executePolicyChangeBI() : BillingInstruction {
    return executedChangeBISupplier()
  }

  private function createChangeBI(forPreview : boolean = false) : PolicyChange {
    var policyPeriod = forPreview
        ? this.findPolicyPeriod()
        : this.findPolicyPeriodForUpdate()
    var bi = new PolicyChange(policyPeriod.Currency)
    policyPeriod = bi.Bundle.add( policyPeriod )
    populateChangeInfo(policyPeriod)
    return bi;
  }

  function populateChangeInfo(period : PolicyPeriod) : PolicyPeriod {
    period.RiskJurisdiction = Jurisdiction.get(this.JurisdictionCode)
    period.PolicyPerEffDate = this.PeriodStart == null ? period.PolicyPerEffDate : this.PeriodStart.toCalendar().Time
    period.PolicyPerExpirDate = this.PeriodEnd == null ? period.PolicyPerExpirDate : this.PeriodEnd.toCalendar().Time
    BillingInstructionInfoMethods.assignOfferNumberToPolicyPeriodIfNotNull(period, this.OfferNumber)
    initializeContact(period)
    return period
  }

  private function initializeContact(period : PolicyPeriod) {
    if (this.PrimaryNamedInsuredContact == null) return;

    var alreadyHasSpecifiedContact = period.Contacts
      .hasMatch(\policyPeriodContact -> policyPeriodContact.Contact.AddressBookUID == this.PrimaryNamedInsuredContact.AddressBookUID)

    if (!alreadyHasSpecifiedContact){
      period.Contacts.firstWhere(\ c -> c.Roles*.Role.contains(TC_PRIMARYINSURED))?.remove()  // period.PrimaryInsured denorm is not synced yet
      var primaryInsured = this.PrimaryNamedInsuredContact.$TypeInstance.toPolicyPeriodContact(period)
      period.addToContacts(primaryInsured)
    }
  }

  function toPolicyChangeForPreview() : PolicyChange {
    var bi = createChangeBI(true)
    this.initializeBillingInstruction(bi)
    return bi
  }

  property get TermConfirmSpecified() : boolean {
    return this.TermConfirmed != null
  }

  function executedChangeBISupplier(): PolicyChange {
    var bi = createChangeBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}