package gw.webservice.policycenter.bc900

uses gw.xml.ws.annotation.WsiExportable

uses java.math.BigDecimal

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc900/CommissionSubPlanRateInfo" )
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
final class CommissionSubPlanRateInfo {
  var _rate : BigDecimal         as Rate
  var _role : typekey.PolicyRole as Role

  construct() {}

  construct(policyRole : PolicyRole, rate : BigDecimal) {
    this.Role = policyRole
    this.Rate = rate
  }

}