package gw.webservice.policycenter.bc900

uses gw.api.locale.DisplayKey
uses gw.api.util.BCUtils
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.pl.persistence.core.Bundle

/**
 * Base abstract BillingAPI class. Defines common, shared utility methods
 * for {@link BillingAPI} and {@link BillingSummaryAPI}.
 */
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
internal abstract class AbstractBillingAPI {

  protected final function require(element : Object, parameterName : String) {
    if (element == null) {
      throw new RequiredFieldException(
          DisplayKey.get("Webservice.Error.MissingRequiredField", parameterName))
    }
  }

  /**
   * Look-up an {@link Account} by its (main) {@code AccountNumber}.
   *
   * If the {@code AccountNumber} identifies a currency splinter {@code Account},
   * this will fail by throwing a {@link BadIdentifierException}.
   *
   * @param accountNumber the (main) account number of the {@code account}
   * @return A main {@code Account} identified by the {@code AccountNumber}.
   */
  @Throws(BadIdentifierException, "If no account exists with the given account number")
  protected final function findAccountIdentifiedBy(accountNumber : String) : Account {
    require(accountNumber, "accountNumber")

    final var account = BCAccountSearchCriteria.findByAccountNumber(accountNumber)
    if (account == null) {
      throwAccountNotFound(accountNumber)
    }
    return account
  }

  protected final function throwAccountNotFound(accountNumber : String) {
    throw new BadIdentifierException(
        DisplayKey.get("BillingAPI.Error.AccountNotFound", accountNumber))
  }

  protected final function runWithNonPersistentBundle(call: block(bundle: Bundle)) {
    BCUtils.executeInPreviewBundle(call)
  }
}
