package gw.webservice.policycenter.bc900

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

uses java.math.BigDecimal

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc900/PremiumIncentiveInfo" )
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
final class PremiumIncentiveInfo {
  var _bonusPercentage : BigDecimal         as BonusPercentage
  var _threshold       : MonetaryAmount     as Threshold

  construct() {}

  construct(premiumIncentive : PremiumIncentive) {
    this.BonusPercentage = premiumIncentive.BonusPercentage
    this.Threshold       = premiumIncentive.Threshold
  }

}