package gw.webservice.policycenter.bc1000

uses gw.api.domain.invoice.AnchorDate
uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.SOAPSenderException
uses gw.webservice.policycenter.bc1000.entity.types.complex.InvoiceStreamOverrides
uses org.joda.time.IllegalFieldValueException

@Export
class InvoiceStreamUtil {
  public static function getOverridingAnchorDates(invoiceStreamOverrides : InvoiceStreamOverrides, periodicity: Periodicity): List<AnchorDate> {
    // PC sends a invoiceStreamOverrides (NewInvoiceStream or DefaultPolicyInvoiceStreamOverrides)
    // with DayOfWeek, FirstDayOfMonth, SecondDayOfMonth, AnchorDate,
    // so use the stream periodicity to determine the overriding anchor dates.
    //
    // TODO: Need to add validation for overriding field to match the periodicity for NewInvoiceStream
    // Currently the validation is done only for DefaultPolicyInvoiceStreamOverrides. If the validation logic is the same for both
    // NewInvoiceStream and DefaultPolicyInvoiceStreamOverrides, the validation could be moved to this method.
    // If the validation can be done here, we dont need to return empty list
    if (periodicity == Periodicity.TC_MONTHLY) {
      try {
        return {AnchorDate.fromDayOfMonth(invoiceStreamOverrides.FirstDayOfMonth)}
      } catch (e : IllegalFieldValueException) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.IllegalFieldValue", "FirstDayOfMonth"))
      }
    }
    if (periodicity == Periodicity.TC_TWICEPERMONTH) {
      try {
        return { AnchorDate.fromDayOfMonth(invoiceStreamOverrides.FirstDayOfMonth), AnchorDate.fromDayOfMonth(invoiceStreamOverrides.SecondDayOfMonth) }
      } catch (e : IllegalFieldValueException) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.IllegalFieldValue", "FirstDayOfMonth/SecondDayOfMonth"))
      }
    }
    if (periodicity == Periodicity.TC_EVERYWEEK) {
      try {
        return { AnchorDate.fromDayOfWeek(DayOfWeek.get(invoiceStreamOverrides.DayOfWeek)) }
      } catch (e : IllegalFieldValueException) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.IllegalFieldValue", "DayOfWeek"))
      }
    }
    if (invoiceStreamOverrides.AnchorDate != null) {
      try {
        return { AnchorDate.fromDate(invoiceStreamOverrides.AnchorDate.toCalendar().Time) }
      } catch (e : IllegalFieldValueException) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.IllegalFieldValue", "AnchorDate"))
      }
    }
    return {}
  }

  static function isInvoiceDayOverridden(invoiceStreamOverrides: InvoiceStreamOverrides, periodicity: Periodicity) : boolean {
    // If at least one of the four fields were overridden,
    // we assume that the caller of this method intends to override invoice day
    // and if it does not match the periodicity, we throw exception

    if (invoiceStreamOverrides.AnchorDate == null &&
        invoiceStreamOverrides.FirstDayOfMonth == null &&
        invoiceStreamOverrides.SecondDayOfMonth == null &&
        invoiceStreamOverrides.DayOfWeek == null) {
      return false
    }

    if (periodicity == Periodicity.TC_MONTHLY) {
      if(invoiceStreamOverrides.FirstDayOfMonth != null &&
          invoiceStreamOverrides.SecondDayOfMonth == null &&
          invoiceStreamOverrides.DayOfWeek == null &&
          invoiceStreamOverrides.AnchorDate == null) {
        return true;
      } else {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.InvoiceDayNotMatchingPeriodicity"))
      }
    }

    if (periodicity == Periodicity.TC_TWICEPERMONTH) {
      if(invoiceStreamOverrides.FirstDayOfMonth != null &&
          invoiceStreamOverrides.SecondDayOfMonth != null &&
          invoiceStreamOverrides.DayOfWeek == null &&
          invoiceStreamOverrides.AnchorDate == null) {
        return true;
      } else {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.InvoiceDayNotMatchingPeriodicity"))
      }
    }

    if (periodicity == Periodicity.TC_EVERYWEEK) {
      if (invoiceStreamOverrides.FirstDayOfMonth == null &&
          invoiceStreamOverrides.SecondDayOfMonth == null &&
          invoiceStreamOverrides.DayOfWeek != null &&
          invoiceStreamOverrides.AnchorDate == null) {
        return true;
      } else {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.InvoiceDayNotMatchingPeriodicity"))
      }
    }

    if(invoiceStreamOverrides.FirstDayOfMonth == null &&
        invoiceStreamOverrides.SecondDayOfMonth == null &&
        invoiceStreamOverrides.DayOfWeek == null &&
        invoiceStreamOverrides.AnchorDate != null) {
      return true;
    } else {
      throw new SOAPSenderException(DisplayKey.get("Webservice.Error.InvoiceStreamOverrides.InvoiceDayNotMatchingPeriodicity"))
    }
  }
}