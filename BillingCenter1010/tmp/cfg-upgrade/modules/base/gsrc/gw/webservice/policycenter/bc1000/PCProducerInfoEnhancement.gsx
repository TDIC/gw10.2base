package gw.webservice.policycenter.bc1000

uses com.google.common.base.Joiner
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.DataConversionException
uses gw.pl.persistence.core.Bundle
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PCProducerInfo_AgencyBillPlanInfos
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PCProducerInfo_CurrencyPlanPairs
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PCProducerInfo_PrimaryContact
uses gw.webservice.policycenter.bc1000.entity.types.complex.AgencyBillPlanInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.CurrencyPlanPair
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCContactInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCProducerInfo
uses gw.webservice.util.WebserviceEntityLoader
uses typekey.Currency
uses entity.Contact

@Export
enhancement PCProducerInfoEnhancement : PCProducerInfo {
  /**
   * Load the information from the {@link Producer} identified
   *    by the {@code PublicID} value of this info'.
   */
  function loadInformation() {
    toRecord(Producer)
  }

  /**
   * Set the information from the values of the specified {@link Producer}.
   */
  function toRecord(producer : Producer) {
    this.PublicID = producer.PublicID
    this.ProducerName = producer.Name
    this.ProducerNameKanji = producer.NameKanji
    this.Tier = producer.Tier.Code

    this.PrimaryContact = toContactInfo(producer.PrimaryContact.Contact)

    final var producersInCurrencyGroup = (producer.ProducerCurrencyGroup != null)
        ? producer.ProducerCurrencyGroup.findProducers()
        : { producer }
    final var producersWithAgencyBillPlan = producersInCurrencyGroup.where(\prod -> prod.AgencyBillPlan != null)
    final var agencyBillPlans = producersWithAgencyBillPlan*.AgencyBillPlan
    this.AgencyBillPlanInfos = Arrays.asList(
        agencyBillPlans.map(\ plan -> {
          final var planInfo = new AgencyBillPlanInfo()
          planInfo.copyPlanCurrencyInfo(plan)
          return new PCProducerInfo_AgencyBillPlanInfos(planInfo)
        })
    )
    this.CurrencyPlanPairs = Arrays.asList(
        producersWithAgencyBillPlan.toTypedArray().map(\ prod -> {
          final var currencyPlanPair = new CurrencyPlanPair()
          currencyPlanPair.Currency = prod.Currency.Code
          currencyPlanPair.PlanID = prod.AgencyBillPlan.PublicID
          return new PCProducerInfo_CurrencyPlanPairs(currencyPlanPair)
        })
    )
  }

  function toProducer(bundle : Bundle) : Producer {
    final var mainProducer = bundle.add(Producer)

    final var agencyBillPlansByCurrency = this.AgencyBillPlans
    final var producersInCurrencyGroup: Iterable<Producer> = (mainProducer.ProducerCurrencyGroup != null)
        ? mainProducer.ProducerCurrencyGroup.findProducers()
        : { mainProducer }

    for (producer in producersInCurrencyGroup) {
      producer = bundle.add( producer )
      producer.Name = this.ProducerName
      producer.NameKanji = this.ProducerNameKanji
      producer.Tier = this.Tier == null
          ? ProducerTier.TC_BRONZE : ProducerTier.get(this.Tier)

      if (producer.AgencyBillPlan == null
          && agencyBillPlansByCurrency.containsKey(producer.Currency)) {
        producer.AgencyBillPlan = agencyBillPlansByCurrency.get(producer.Currency)
      }
      if (producer.PrimaryContact.Contact.AddressBookUID != this.PrimaryContact.AddressBookUID) {
        if (producer.PrimaryContact != null) {
          producer.PrimaryContact.Roles[0].Role = TC_SECONDARY
        }
        // sending a new contact
        var existing = producer.Contacts
            .firstWhere(\ p -> p.Contact.AddressBookUID == this.PrimaryContact.AddressBookUID )
        if (existing == null) {
          producer.addToContacts(
              this.PrimaryContact.$TypeInstance.toProducerContact( producer ) )
        } else {
          existing.Roles[0].Role = TC_PRIMARY
        }
      }
    }

    if (!this.CurrencyPlanPairs.Empty) {
      final var existingCurrencies =
          producersInCurrencyGroup.map(\ producer -> producer.Currency)
      for (var currency in this.CurrenciesSet) {   //we only care about unique currencies
        if (!existingCurrencies.contains(currency)) {
          // new currency, splinter for it...
          createProducerForCurrency(mainProducer, currency,
              agencyBillPlansByCurrency.get(currency))
        }
      }
    }
    return mainProducer
  }

  private property get Producer() : Producer {
    return WebserviceEntityLoader
        .loadByPublicID<Producer>(this.PublicID, "PublicID")
  }

  /**
   * A map of the {@link AgencyBillPlan}s by {@link Currency} as identified
   * by the {@link #AgencyBillPlanIDs} for this {@link PCProducerInfo}.
   */
  property get AgencyBillPlans(): Map<Currency, AgencyBillPlan> {
    if (not AgencyBillPlanIDs.IsEmpty) {
      var results = new HashMap<Currency, AgencyBillPlan>()
      final var planQuery = Query.make(AgencyBillPlan)
      planQuery.compareIn("PublicID", this.AgencyBillPlanIDs)
      var agencyBillPlans = planQuery.select()

      validateAgencyBillPlanIDs(agencyBillPlans.toList())

      var currencyPlanPairs = mapCurrencyPlanPair().filterByValues(\planID -> planID != null)
      for (entry in currencyPlanPairs.entrySet()) {
        var currency = entry.getKey()
        var planID = entry.getValue()
        var agencyBillPlan = agencyBillPlans.firstWhere(\plan -> plan.PublicID == planID)
        if (!agencyBillPlan.Currencies.contains(currency)) {
          // agency bill plan doesnt support currency
          throw new IllegalStateException(DisplayKey.get("Webservice.Error.Plan.DoesNotSupportCurrency", AgencyBillPlan.Type.RelativeName, agencyBillPlan, currency))
        }
        results.put(currency, agencyBillPlan)
      }
      return results
    }
    return {} // none specified
  }

  property get AgencyBillPlanIDs() : String[] {
    var agencyBillPlanIDs = new HashSet<String>()
    agencyBillPlanIDs.addAll(mapCurrencyPlanPair().Values)
    return agencyBillPlanIDs.where(\elt -> elt != null).toTypedArray()
  }

  /**
   * A set of the {@link Currency}s as identified by the codes in the
   * {@link #Currencies} list for this {@link PCProducerInfo}.
   */
  property get CurrenciesSet() : Set<Currency> {
    var currencies = new HashSet<Currency>()
    currencies.addAll(mapCurrencyPlanPair().keySet())
    currencies.addAll(getExistingCurrencies())
    return currencies
  }

  private function toContactInfo(contact : Contact) : PCProducerInfo_PrimaryContact {
    if (contact == null) {
      return null
    }
    var contactInfo = new PCContactInfo()
    contactInfo.ContactType = contact.Subtype.Code
    contactInfo.PublicID = contact.ExternalID
    contactInfo.AddressBookUID = contact.AddressBookUID
    return new PCProducerInfo_PrimaryContact(contactInfo)
  }

  private function
  createProducerForCurrency(mainProducer : Producer, currency : Currency,
      agencyBillPlan : AgencyBillPlan) {
    final var newProducer = createProducerForCurrency(mainProducer, currency)
    newProducer.AgencyBillPlan = agencyBillPlan
  }

  /**
   * Create a new {@link Producer} for the specified {@link Currency} belonging
   * to a multi-currency producer group with the specified main
   * {@code Producer}.
   *<p/>
   * Note that a {@code Producer} in a {@link MixedCurrencyProducerGroup} must
   * be unique per {@code Currency}.
   *
   * @param mainProducer the main {@link Producer} of a producer group
   * @param currency the {@link Currency} for which to create the
   *                 {@code Producer}
   * @return The new {@link Producer}.
   */
  static function createProducerForCurrency(
      mainProducer : Producer, currency : Currency) : Producer {
    if (mainProducer.Bundle.ReadOnly) {
      mainProducer = gw.transaction.Transaction.Current.add(mainProducer) // ensure writable...
    }

    var newProducer = mainProducer.createSplinterProducerForCurrency(currency)
    newProducer.NameKanji = mainProducer.NameKanji
    setProducerAttributes(newProducer,
        createPrimaryContactFor(newProducer.Bundle, mainProducer.PrimaryContact.Contact))
    return newProducer
  }

  static function
  setProducerAttributes(newProducer : Producer, primaryContact : ProducerContact) {
    if (primaryContact != null) {
      newProducer.addToContacts(primaryContact)
    }
    // Default these fields because PC does not care about them
    initializeDefaultsForFieldsPCDoesNotCareAbout(newProducer)
  }

  private static
  function initializeDefaultsForFieldsPCDoesNotCareAbout(producer: Producer) {
    producer.InitialRecurDate = Date.CurrentDate.nextDayOfMonth( 1 )
    producer.RecurDayOfMonth = 1
    producer.RecurPeriodicity = TC_MONTHLY
  }

  private static function
  createPrimaryContactFor(bundle : Bundle, primaryContact : Contact) : ProducerContact {
    return primaryContact == null
        ? null : PCContactInfoEnhancement
            .createPrimaryProducerContactFor(bundle, primaryContact)
  }

  protected function mapCurrencyPlanPair(): Map<Currency, String> {
    var results = new HashMap<Currency, String>()
    this.CurrencyPlanPairs.each(\pair -> {
      final var currency = Currency.get(pair.Currency)
      if (currency == null) {
        throw new BadIdentifierException(
            DisplayKey.get("Webservice.Error.Currency.Unknown", pair.Currency))
      }
      if (results.containsKey(currency)) {
        if (results.get(currency) != pair.PlanID) {
          throw new DataConversionException(DisplayKey.get("Webservice.Error.DuplicateCurrencies",
              PCProducerInfo.Type.RelativeName, currency.Code))
        }
      } else {
        results.put(currency, pair.PlanID)
      }
    })
    return results
  }

  private function getExistingCurrencies() : Set<Currency> {
    var currencies = new HashSet<Currency>()
    if (Producer != null){
      currencies.addAll(Producer.ProducerCurrencyGroup == null ? {Producer.Currency} : Producer.ProducerCurrencyGroup.findProducers()*.Currency.toSet())
    }
    return currencies
  }

  private function validateAgencyBillPlanIDs(agencyBillPlans : List<AgencyBillPlan>) {
    var unfoundPlans = this.AgencyBillPlanIDs.where(\planID -> !agencyBillPlans.hasMatch(\plan -> plan.PublicID == planID))
    if (!unfoundPlans.IsEmpty) {
      throw new BadIdentifierException(DisplayKey.get("Java.Object.PublicId.NotFound", AgencyBillPlan.Type.RelativeName,
          Joiner.on(", ").join(unfoundPlans)))
    }
  }
}
