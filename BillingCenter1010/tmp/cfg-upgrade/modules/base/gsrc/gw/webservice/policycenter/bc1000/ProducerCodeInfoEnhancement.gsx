package gw.webservice.policycenter.bc1000

uses com.google.common.base.Joiner
uses com.google.common.base.Preconditions
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.DataConversionException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.pl.persistence.core.Bundle
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.ProducerCodeInfo_CommissionPlanInfos
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.ProducerCodeInfo_CurrencyPlanPairs
uses gw.webservice.policycenter.bc1000.entity.types.complex.CommissionPlanInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.CurrencyPlanPair
uses gw.webservice.policycenter.bc1000.entity.types.complex.ProducerCodeInfo
uses gw.webservice.util.WebserviceEntityLoader
uses typekey.Currency

@Export
enhancement ProducerCodeInfoEnhancement : ProducerCodeInfo {
  /**
   * Load the information from the {@link ProducerCode} identified
   *    by the {@code PublicID} value of this info'.
   */
  function loadInformation() {
    toRecord(ProducerCode)
  }

  /**
   * Set the information from the values of the specified {@link ProducerCode}.
   */
  function toRecord(producerCode : ProducerCode) {
    this.PublicID = producerCode.PublicID
    this.Code = producerCode.Code
    this.Active = producerCode.Active

    final var codes = findProducerCodesInCurrencyGroup(producerCode)
    this.CurrencyPlanPairs = Arrays.asList(
        codes.toTypedArray().map(\elt -> {
          final var currencyPlanPair = new CurrencyPlanPair()
          currencyPlanPair.Currency = elt.Currency.Code
          currencyPlanPair.PlanID = elt.CommissionPlan.PublicID
          return new ProducerCodeInfo_CurrencyPlanPairs(currencyPlanPair)
        })
    )
    this.CommissionPlanInfos = Arrays.asList(
        codes*.CommissionPlan.map(\ plan -> {
            final var planInfo = new CommissionPlanInfo()
            planInfo.copyCommissionPlanInfo(plan)
            return new ProducerCodeInfo_CommissionPlanInfos(planInfo)
        })
      )
  }

  function toProducerCode(bundle : Bundle) : ProducerCode {
    final var mainProducerCode = bundle.add(ProducerCode)
    final var commissionPlansMap = this.CommissionPlans

    final var producerCodesInCurrencyGroup =
        findProducerCodesInCurrencyGroup(mainProducerCode)
    for (producerCode in producerCodesInCurrencyGroup) {
      producerCode = bundle.add(producerCode)
      producerCode.Code = this.Code
      producerCode.Active = this.Active
      final var updateCommissionPlan =
          commissionPlansMap.get(producerCode.Currency)
      if (updateCommissionPlan != null
          && producerCode.CommissionPlan != updateCommissionPlan) {
        // update CommissionPlan...
        producerCode.CommissionPlan = updateCommissionPlan
      }
    }

    if (!this.CurrencyPlanPairs.Empty) {
      // look for new currencies...
      final var existingCurrencies = producerCodesInCurrencyGroup
          .map(\ producerCode -> producerCode.Currency)
      for (currency in this.CurrenciesSet) {
        if (!existingCurrencies.contains(currency)) {
          // new currency
          createNewProducerCode(mainProducerCode.Producer, currency,
              commissionPlansMap, bundle)
        }
      }
    }
    return mainProducerCode
  }

  /**
   * A map of the {@link CommissionPlan}s by {@link typekey.Currency} as identified
   * by the {@link #CommissionPlanIDs} for this {@link ProducerCodeInfo}.
   */
  property get CommissionPlans() : Map<Currency, CommissionPlan> {
    if (not CommissionPlanIDs.IsEmpty) {
      var results = new HashMap<Currency, CommissionPlan>()
      final var planQuery = Query.make(CommissionPlan)
      planQuery.compareIn("PublicID", this.CommissionPlanIDs)
      var commissionPlans = planQuery.select()

      validateCommissionPlanIDs(commissionPlans.toList())

      var currencyPlanPairs = mapCurrencyPlanPair().filterByValues(\planID -> planID != null)
      for (entry in currencyPlanPairs.entrySet()) {
        var currency = entry.getKey()
        var planID = entry.getValue()
        var commissionPlan = commissionPlans.firstWhere(\plan -> plan.PublicID == planID)
        if (!commissionPlan.Currencies.contains(currency)) {
          // agency bill plan doesnt support currency
          throw new IllegalStateException(DisplayKey.get("Webservice.Error.Plan.DoesNotSupportCurrency",
              CommissionPlan.Type.RelativeName, commissionPlan, currency))
        }
        results.put(currency, commissionPlan)
      }
      return results
    }
    return {} // none specified
  }

  property get CommissionPlanIDs() : String[] {
    var commissionPlanIDs = new HashSet<String>()
    commissionPlanIDs.addAll(mapCurrencyPlanPair().Values)
    return commissionPlanIDs.where(\elt -> elt != null).toTypedArray()
  }

  /**
   * A set of the {@link typekey.Currency}s as identified by the codes in the
   * {@link #Currencies} list for this {@link ProducerCodeInfo}.
   */
  property get CurrenciesSet() : Set<Currency> {
    return mapCurrencyPlanPair().keySet()
  }

  /**
   * Create a new {@link ProducerCode} for the specified {@link typekey.Currency} belonging
   * to the associated {@link Producer} in a multi-currency producer group with
   * attributes corresponding to those on this {@link ProducerCodeInfo}.
   *
   * @param mainProducer the main {@link Producer} of a producer group
   * @param currency the {@link typekey.Currency} for which to create the
   *                 {@code ProducerCode}
   * @param commissionPlansMap a map of optional {@link CommissionPlan}s by
   *                           {@code Currency} from which to assign the
   *                           @{code CommissionPlan}
   * @param bundle the {@link Bundle} in which to create the new
    *              {@code ProducerCode}
   * @return The new {@link ProducerCode}
   */
  function createNewProducerCode(mainProducer : Producer, currency : Currency,
      commissionPlansMap : Map<Currency, CommissionPlan>, bundle : Bundle)
          : ProducerCode {
    final var targetProducer = (currency == mainProducer.Currency)
        ? mainProducer
        : findOrCreateSplinterCurrencyProducer(mainProducer, currency)
    return makeProducerCode(targetProducer,
        getCommissionPlanForCurrency(currency, commissionPlansMap),
        bundle)
  }

  /**
   * Returns the {@link ProducerCode} identified by the {@link
   *    ProducerCodeInfo#PublicID PublicID} of this {@link ProducerCodeInfo}.
   *    This is a single {@code ProducerCode} or the main one within a multi-
   *    currency {@link MixedCurrencyProducerGroup}.
   */
  private property get ProducerCode() : ProducerCode {
    return WebserviceEntityLoader
        .loadByPublicID<ProducerCode>(this.PublicID, "PublicID")
  }

  private function findProducerCodesInCurrencyGroup(
      mainProducerCode : ProducerCode) : Iterable<ProducerCode> {
    final var producerCurrencyGroup =
        mainProducerCode.Producer.ProducerCurrencyGroup
    if (producerCurrencyGroup == null) {
      return {mainProducerCode}
    }

    // look in splinter Producers for all Currency ProducerCodes...
    return Query.make(entity.ProducerCode)
        .compare("Code", Equals, mainProducerCode.Code)
        .subselect("Producer", CompareIn, Query.make(ProducerCurrencyGroup)
              .compare("ForeignEntity", Equals, producerCurrencyGroup),
            "Owner")
        .select()
  }

  private function makeProducerCode(
      producer : Producer, commissionPlan : CommissionPlan, bundle : Bundle)
          : ProducerCode {
    final var producerCode = new ProducerCode(bundle, producer.Currency)
    producerCode.Code = this.Code
    producerCode.Active = this.Active
    producerCode.CommissionPlan = commissionPlan
    producer.addToProducerCodes( producerCode )
    return producerCode
  }

  /**
   * @param currency the {@link typekey.Currency}
   * @param commissionPlansMap the map of specified {@link CommissionPlan}s
   *                           by {@link typekey.Currency}
   * @return The {@link CommissionPlan} for the {@link typekey.Currency} either from the
   *         map or the first active one for the {@link typekey.Currency}, if any.
   * @throws RequiredFieldException if no {@link CommissionPlan} found
   */
  private function getCommissionPlanForCurrency(
      currency: Currency, commissionPlansMap: Map<Currency, CommissionPlan>)
      : CommissionPlan {
    Preconditions.checkNotNull(currency)
    var commissionPlan = commissionPlansMap.get(currency)
    if (commissionPlan == null) {
      commissionPlan = CommissionPlan.finder
          .findFirstActivePlanByPlanOrder(CommissionPlan, currency)
      if (commissionPlan == null) {
        throw new RequiredFieldException(
            DisplayKey.get("Webservice.Error.ProducerCode.MissingCommissionPlan", currency))
      }
    }
    return commissionPlan
  }

  /**
   * Look up or create a splinter currency producer  for the specified
   *    main producer.
   *
   * The currency for the producer is that specified on this {@link
   * NewProducerCodeInfo}.
   *
   * @param mainProducer the {@link Producer} whose {@link typekey.Currency} is
   *                     different than that of the ProducerCode to be created
   *                     by this {@code NewProducerCodeInfo}.
   * @return The splinter currency {@code Producer}.
   */
  private static function findOrCreateSplinterCurrencyProducer(
      mainProducer : Producer, currency : Currency) : Producer {
    var splinterProducer : Producer
    if (mainProducer.ProducerCurrencyGroup == null) {
      splinterProducer = createProducerForCurrency(mainProducer, currency)
    } else {
      splinterProducer = findExistingProducerForCurrency(
          mainProducer.ProducerCurrencyGroup, currency)
      if (splinterProducer == null) {
        splinterProducer = createProducerForCurrency(mainProducer, currency)
      }
    }
    return splinterProducer
  }

  private static function createProducerForCurrency(
      mainProducer : Producer, currency : Currency) : Producer {
    return PCProducerInfoEnhancement
        .createProducerForCurrency(mainProducer, currency)
  }

  private static function findExistingProducerForCurrency(
      producerGroup : MixedCurrencyProducerGroup, currency : Currency)
      : Producer {
    return Query.make(Producer)
        .compare("Currency", Equals, currency)
        .subselect("ID", CompareIn,
            Query.make(ProducerCurrencyGroup)
                .compare("ForeignEntity", Equals, producerGroup), "Owner")
        .select().AtMostOneRow
  }

  private function mapCurrencyPlanPair() : Map<Currency, String> {
    var results = new HashMap<Currency, String>()
    this.CurrencyPlanPairs.each(\pair -> {
      final var currency = Currency.get(pair.Currency)
      if (currency == null) {
        throw new BadIdentifierException(
            DisplayKey.get("Webservice.Error.Currency.Unknown", pair.Currency))
      }
      if (results.containsKey(currency)) {
        if (results.get(currency) != pair.PlanID) {
          throw new DataConversionException(DisplayKey.get("Webservice.Error.DuplicateCurrencies",
              ProducerCodeInfo.Type.RelativeName, currency.Code))
        }
      } else {
        results.put(currency, pair.PlanID)
      }
    })
    return results
  }

  private function validateCommissionPlanIDs(agencyBillPlans : List<CommissionPlan>) {
    var unfoundPlans = this.CommissionPlanIDs.where(\planID -> !agencyBillPlans.hasMatch(\plan -> plan.PublicID == planID))
    if (!unfoundPlans.IsEmpty) {
      throw new BadIdentifierException(DisplayKey.get("Java.Object.PublicId.NotFound", CommissionPlan.Type.RelativeName,
          Joiner.on(", ").join(unfoundPlans)))
    }
  }

}
