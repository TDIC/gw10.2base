package gw.archiving

@Export
class DistArchiveReferenceForCollateralImpl implements ArchiveReferenceByChargeOwner {
  var _distArchiveReferenceForCollateral: DistArchiveReferenceForCollateral

  construct(distArchiveReferenceForCollateral: DistArchiveReferenceForCollateral) {
      _distArchiveReferenceForCollateral = distArchiveReferenceForCollateral
  }

  override property get ChargeTAccountOwner() : TAccountOwner {
    return _distArchiveReferenceForCollateral.Collateral
  }
}