package gw.archiving

uses java.lang.IllegalArgumentException

/**
 * Factory class for creating archive references on
 * {@link BaseDist dists} that are frozen during archiving.
 */
@Export
class DistArchiveReferenceFactory {

  function createArchiveReferences(policyPeriod : PolicyPeriod) {
    for (distID in policyPeriod.FreezeSet.distsToBeFrozen()) {
      var dist = policyPeriod.Bundle.loadBean(distID) as BaseDist
      if (dist.FreezingNow) {
        createArchiveReference(dist)
      }
    }
  }

  private function createArchiveReference(dist : BaseDist) {
    var distArchiveReference = new DistArchiveReference(dist.Currency)
    distArchiveReference.BaseDist = dist

    var distItemsByTAccountOwner = dist.DistItems.partition(\ distItem -> distItem.InvoiceItem.Charge.TAccountOwner)
    for (taccountOwner in distItemsByTAccountOwner.keySet()) {
      var distArchiveReferenceByChargeOwner = createTypedDistArchiveReference(dist, taccountOwner)
      distArchiveReferenceByChargeOwner.DistArchiveReference = distArchiveReference
      setSumsFromDistItems(distArchiveReferenceByChargeOwner, distItemsByTAccountOwner.get(taccountOwner))
    }
  }

  private function createTypedDistArchiveReference(dist : BaseDist, taccountOwner : TAccountOwner) : DistArchiveReferenceByChargeOwner {
    switch (taccountOwner.IntrinsicType) {
      case Account.Type :
          var distArchiveReferenceForAccount = new DistArchiveReferenceForAccount(dist, dist.Currency)
          distArchiveReferenceForAccount.Account = taccountOwner as Account
          return distArchiveReferenceForAccount

      case Collateral.Type :
          var distArchiveReferenceForCollateral = new DistArchiveReferenceForCollateral(dist, dist.Currency)
          distArchiveReferenceForCollateral.Collateral = taccountOwner as Collateral
          return distArchiveReferenceForCollateral

      case CollateralRequirement.Type :
          var distArchiveReferenceForCollateralRequirement = new DistArchiveReferenceForCollateralRequirement(dist, dist.Currency)
          distArchiveReferenceForCollateralRequirement.CollateralRequirement = taccountOwner as CollateralRequirement
          return distArchiveReferenceForCollateralRequirement

      case PolicyPeriod.Type :
          var distArchiveReferenceForPolicyPeriod = new DistArchiveReferenceForPolicyPeriod(dist, dist.Currency)
          distArchiveReferenceForPolicyPeriod.PolicyPeriod = taccountOwner as PolicyPeriod
          return distArchiveReferenceForPolicyPeriod
    }
    throw new IllegalArgumentException(
        "T-account owner of type " +
            taccountOwner.IntrinsicType + " with name " + taccountOwner.DisplayName +
            " is not a recognized charge t-account owner")
  }


  private function setSumsFromDistItems(distArchiveReferenceByChargeOwner: DistArchiveReferenceByChargeOwner, distItems: List<BaseDistItem>) {
    var distCurrency = distArchiveReferenceByChargeOwner.Currency
    distArchiveReferenceByChargeOwner.GrossApplied = distItems.sum(distCurrency, \ distItem -> distItem.GrossAmountToApply)
    distArchiveReferenceByChargeOwner.CommissionApplied = distItems.sum(distCurrency, \ distItem -> distItem.CommissionAmountToApply)
  }

}