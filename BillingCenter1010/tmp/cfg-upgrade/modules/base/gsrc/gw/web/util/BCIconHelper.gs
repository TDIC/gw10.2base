package gw.web.util

uses gw.api.locale.DisplayKey
uses gw.api.web.color.GWColor

/**
 * Helper class used to resolve conditional icon names. Used mainly to return different icon names depending on typecodes,
 * but could also be used to resolve any icon name dynamically generated, etc
 */
@Export
class BCIconHelper {

  public static function getIcon(billingMethod: PolicyPeriodBillingMethod): String {
    switch(billingMethod) {
      case TC_AGENCYBILL: return "agency_bill"
      case TC_DIRECTBILL: return "direct_bill"
      case TC_LISTBILL: return "list_bill"
      default: return ""
    }
  }

  public static function getIcon(product: LOBCode): String {
    switch (product) {
      case TC_BUSINESSAUTO: return "commercial_auto"
      case TC_BUSINESSOWNERS: return "businessowner"
      case TC_COMMERCIALPACKAGE: return "commercial_package"
      case TC_COMMERCIALPROPERTY: return "commercial_building"
      case TC_GENERALLIABILITY: return "general_liability"
      case TC_HOPHOMEOWNERS: return "homeowners"
      case TC_INLANDMARINE: return "inland_marine"
      case TC_PERSONALAUTO: return "personal_auto"
      case TC_WORKERSCOMP: return "workers_comp"
      default: return "policy"
    }
  }

  public static function getIcon(serviceTier: CustomerServiceTier): String {
    switch (serviceTier) {
      case TC_GOLD: return "tier2"
      case TC_PLATINUM: return "tier1"
      case TC_SILVER: return "tier3"
      default: return ""
    }
  }

  public static function getIconColor(serviceTier: CustomerServiceTier): GWColor {
    switch (serviceTier) {
      case TC_GOLD: return GWColor.THEME_TIER_2
      case TC_PLATINUM: return GWColor.THEME_TIER_1
      case TC_SILVER: return GWColor.THEME_TIER_3
      default: return null
    }
  }

  public static function getPolicyPeriodInForceIcon(policyPeriod: PolicyPeriod): String {
    if (policyPeriod.IsInForce) {
      return "inforce";
    } else {
      return policyPeriod.Archived ? "not_inforce_archived" : "not_inforce";
    }
  }

  public static function getPolicyPeriodPastDueIcon(policyPeriod: PolicyPeriod): String {
    return policyPeriod.DelinquentAmount.IsPositive ? "past_due" : ""
  }

  public static function getAgencyBillPaymentExceptionIcon(invoiceItem: InvoiceItem): String {
    if (!invoiceItem.HasBeenPaymentException) {
      return null
    }

    return invoiceItem.PaymentException ? "flag" : "info"
  }

  public static function getAgencyBillPaymentExceptionIconColor(invoiceItem: InvoiceItem): GWColor {
    if (!invoiceItem.HasBeenPaymentException) {
      return null
    }

    return invoiceItem.PaymentException ? gw.api.web.color.GWColor.THEME_ALERT_ERROR : gw.api.web.color.GWColor.THEME_APP_FONT
  }

  public static function getAgencyBillPaymentExceptionIconLabel(invoiceItem: InvoiceItem): String {
    if (!invoiceItem.HasBeenPaymentException) {
      return null
    }

    return invoiceItem.PaymentException ?
        DisplayKey.get("Java.AgencyBillStatementItem.ExceptionStatus.CurrentException") :
        DisplayKey.get("Java.AgencyBillStatementItem.ExceptionStatus.PastException")
  }

  public static property get DelinquencyIcon(): String {
    return "delinquent"
  }

  public static property get TimeSinceInceptionIcon(): String {
    return "longtime_customer"
  }
}