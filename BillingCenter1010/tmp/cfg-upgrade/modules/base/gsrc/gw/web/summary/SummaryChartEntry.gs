package gw.web.summary

uses gw.api.locale.DisplayKey
uses gw.api.web.color.GWColor

uses java.math.BigDecimal

/**
 * Defines an entry for a summary chart, which specifies a display key
 * to be used for the label, a value, and a color for the entry in a
 * chart.
 */
@Export
class SummaryChartEntry {
  /**
   * The display key identifier (full path) for the label.
   */
  var _labelDisplayKeyId : String as readonly LabelDisplayKeyID
  /**
   *  The entry value.
   */
  var _value : BigDecimal
  /**
   * The chart color to be used for the entry in the chart.
   */
  var _color : GWColor as Color

  construct(displayKeyID : String, value : BigDecimal, color : GWColor) {
    _labelDisplayKeyId = displayKeyID
    _value = value
    _color = color
  }

  /**
   * The value that will be used to render the pie chart portion.
   */
  property get ChartValue() : BigDecimal {
    return _value
  }

  /**
   * The value to be used in formatting the label.
   */
  property get DisplayValue() : Object {
    return ChartValue
  }


  /**
   * The label in the legend for this chart entry.
   */
  property get Label() : String {
    return DisplayKey.get(LabelDisplayKeyID, DisplayValue?:"")
  }
}