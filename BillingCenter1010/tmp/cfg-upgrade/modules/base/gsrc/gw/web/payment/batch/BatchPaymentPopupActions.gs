package gw.web.payment.batch

uses gw.api.locale.DisplayKey

@Export
public enum BatchPaymentPopupActions {

  POST(null,
      \batches -> BatchPaymentsActionsService.Instance.postAsync(batches),
      DisplayKey.get("Web.DesktopBatchPaymentsScreen.PostConfirmationMessage")),

  REVERSE(
      \batches -> atLeastOnePaymentWasReversedWarning(batches),
      \batches -> BatchPaymentsActionsService.Instance.reverseAsync(batches),
      DisplayKey.get("Web.DesktopBatchPaymentsScreen.ReverseConfirmationMessage")),

  DELETE(null,
      \batches -> BatchPaymentsActionsService.Instance.delete(batches),
      DisplayKey.get("Web.DesktopBatchPaymentsScreen.DeleteConfirmationMessage"));

  private construct(warningMessage: block(batches: BatchPayment[]): String,
                    actionToConfirm: block(batches: BatchPayment[]),
                    titleLabel: String
      ){
    _warningMessage = warningMessage
    _actionToConfirm = actionToConfirm
    _titleLabel = titleLabel
  }

  private final var _warningMessage: block(batches: BatchPayment[]): String as readonly WarningFor
  public final var _actionToConfirm: block(batches: BatchPayment[])as readonly ToConfirmFor
  private final var _titleLabel: String as readonly TitleLabel

  private static function atLeastOnePaymentWasReversedWarning(batches: BatchPayment[]): String {

    if (BatchPaymentsActionsService.Instance.isAtLeastOnePaymentWasReversedWithin(batches)) {
      return DisplayKey.get("Web.DesktopBatchPaymentsScreen.ReversingBatchesWithReversedPayments")
    }
    return null
  }

}
