package gw.web.account

uses com.google.common.collect.Maps
uses com.google.common.collect.Sets
uses gw.api.database.DBFunction
uses gw.api.database.Query
uses gw.api.filters.StandardBeanFilter
uses gw.api.locale.DisplayKey
uses java.util.List

/**
 * Encapsulates the value and display logic used to display the information
 * for an {@link entity.Account Account}'s {@link entity.PolicyPeriod policies}
 * on a summary display.
 */
@Export
class AccountSummaryPolicyHelper {
  private var _account: Account
  public construct(account: Account) {
    _account = account
  }

  /**
      Retrieves a list of policy summary views for the {@link entity.PolicyPeriod
      PolicyPeriod}s that are interesting for customer service representatives
      to see on the Account Summary Screen because they are either:
      <ul type="square">
      <li>open (even if they have already expired because the PolicyClosure
        batch process hasn't yet run)
      <li>cancelled (regardless of whether or not they have expired or are
        closed)
      <li>reinstated (regardless of whether or not they have expired or are
        closed)
      </ul>
      The {@code PolicyPeriod}s are wrapped in an {@link
      gw.web.account.AccountPolicySummaryView AccountPolicySummaryView} that
      enables the cancellation date and modification date to be associated with
      the {@code PolicyPeriod} for purposes of the {@code ListView} on the
      {@link pcf.AccountSummaryScreen}.
   */
  public function listPolicySummaryViews(): List<AccountPolicySummaryView> {
    var policySummaryViews = Maps.newHashMap<PolicyPeriod, AccountPolicySummaryView>()

    //Retrieve any PolicyPeriods that were cancelled, even if they are expired /////////////////////////////////////////
    var cancellationQuery = Query.make(Cancellation)
    var cancellationPolicyPeriodTable = cancellationQuery.join("AssociatedPolicyPeriod")
    var cancellationPolicyTable = cancellationPolicyPeriodTable.join("Policy")
    cancellationPolicyTable.compare("Account", Equals, _account)

    //we only want the latest cancellation on a PolicyPeriod
    var maxModDateCancelQuery = Query.make(Cancellation)
    maxModDateCancelQuery.join("AssociatedPolicyPeriod")
        .compare("ID", Equals,
            cancellationPolicyPeriodTable.getColumnRef("ID"))
        .join("Policy")
        .compare("Account", Equals, _account)
    cancellationQuery.subselect("ModificationDate", CompareIn,
        maxModDateCancelQuery, DBFunction.Max(maxModDateCancelQuery.getColumnRef("ModificationDate")))

    var cancellations = cancellationQuery.select().toSet()
    for (cancellation in cancellations) {
      policySummaryViews.put(cancellation.AssociatedPolicyPeriod,
          new AccountPolicySummaryView(
            cancellation.AssociatedPolicyPeriod, cancellation.ModificationDate))
    }

    //Retrieve any PolicyPeriods that were reinstated, even if they are expired ////////////////////////////////////////
    var reinstatementQuery = Query.make(Reinstatement)
    var reinstatementPolicyPeriodTable = reinstatementQuery.join("AssociatedPolicyPeriod")
    var reinstatementPolicyTable = reinstatementPolicyPeriodTable.join("Policy")
    reinstatementPolicyTable.compare("Account", Equals, _account)

    //we only want the latest reinstatement on a PolicyPeriod
    var maxModDateReinstQuery = Query.make(Reinstatement)
    maxModDateReinstQuery.join("AssociatedPolicyPeriod")
        .compare("ID", Equals, reinstatementPolicyPeriodTable.getColumnRef("ID"))
        .join("Policy")
        .compare("Account", Equals, _account)
    reinstatementQuery.subselect("ModificationDate", CompareIn,
        maxModDateReinstQuery, DBFunction.Max(maxModDateReinstQuery.getColumnRef("ModificationDate")))

    var reinstatements = reinstatementQuery.select().toSet()
    for (reinstatement in reinstatements) {
      var policyPeriod = reinstatement.AssociatedPolicyPeriod
      var policySummaryView = policySummaryViews.get(policyPeriod)
      //should not be null. we should have found a cancellation if it has been reinstated
      policySummaryView.ReinstatementDate = reinstatement.ModificationDate
      policySummaryViews.put(policyPeriod, policySummaryView)
    }

    //Retrieve all open PolicyPeriods /////////////////////////////////////////////////////////////////////////////////
    var openPolicyPeriodQuery = Query.make(PolicyPeriod)
    openPolicyPeriodQuery.compare("ClosureStatus", NotEquals, PolicyClosureStatus.TC_CLOSED)
    var openPolicyTable = openPolicyPeriodQuery.join("Policy").compare("Account", Equals, _account)
    openPolicyTable.compareNotIn("ID", cancellations.map(\elt -> elt.AssociatedPolicyPeriod.ID).toArray())
    //exclude ones we already found when querying for cancellations
    openPolicyPeriodQuery.withLogSQL(false)

    //Add open PolicyPeriods to return value collection
    var openPolicyPeriods = Sets.newHashSet(openPolicyPeriodQuery.select().iterator())
    for (openPolicyPeriod in openPolicyPeriods) {
      policySummaryViews.put(openPolicyPeriod, new AccountPolicySummaryView(openPolicyPeriod))
    }

    return policySummaryViews.values().toList()
  }

  // ListView filters //////////////////////////////////////////////////////////////////////////////////////////////////
  private var _filter_all =
      new StandardBeanFilter(
          DisplayKey.get('Web.AccountSummary.Policies.Filter.All'),
              \elt -> true)
  private var _filter_active =
      new StandardBeanFilter(
          DisplayKey.get('Web.AccountSummary.Policies.Filter.Open'),
              \elt -> (((elt as AccountPolicySummaryView ).PolicyPeriod.ClosureStatus == TC_OPEN
                  || (elt as AccountPolicySummaryView ).PolicyPeriod.ClosureStatus == TC_OPENLOCKED )
                  && (elt as AccountPolicySummaryView ).PolicyPeriod.Cancellation == null))
  private var _filter_cancelled =
      new StandardBeanFilter(
          DisplayKey.get('Web.AccountSummary.Policies.Filter.Canceled'),
              \elt -> ((elt as AccountPolicySummaryView ).ReinstatementDate == null && (elt as AccountPolicySummaryView ).CancellationDate != null))
  private var _filter_reinstated =
      new StandardBeanFilter(
          DisplayKey.get('Web.AccountSummary.Policies.Filter.Reinstated'),
              \elt -> (elt as AccountPolicySummaryView ).ReinstatementDate != null)
  property get FILTER_ALL(): StandardBeanFilter {
    return _filter_all
  }

  property get FILTER_ACTIVE(): StandardBeanFilter {
    return _filter_active
  }

  property get FILTER_CANCELLED(): StandardBeanFilter {
    return _filter_cancelled
  }

  property get FILTER_REINSTATED(): StandardBeanFilter {
    return _filter_reinstated
  }
}