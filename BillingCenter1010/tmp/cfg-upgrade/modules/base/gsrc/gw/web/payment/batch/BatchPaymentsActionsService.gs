package gw.web.payment.batch

uses gw.api.web.UnsavedWorkHelper
uses gw.pl.persistence.core.Bundle
uses gw.transaction.Transaction
uses pcf.BatchPaymentDetailsPage

@Export
class BatchPaymentsActionsService {

  private construct() {
  }

  private static final var _instance = new BatchPaymentsActionsService()

  public static property get Instance(): BatchPaymentsActionsService {
    return _instance
  }

  function postAsync(batchPayments : BatchPayment[]) {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> postAsync(bundle, batchPayments))
  }

  function reverseAsync(batchPayments : BatchPayment[]) {
    Transaction.runWithNewBundle(\bundle -> reverseAsync(bundle, batchPayments))
  }

  function delete(batchPayments: BatchPayment[]) {
    Transaction.runWithNewBundle(\bundle -> delete(bundle, batchPayments))
  }

  private function delete(bundle: Bundle, batchPayments: BatchPayment[]) {
    for (var batchPayment in batchPayments) {
      batchPayment = bundle.add(batchPayment)
      batchPayment.remove()
    }
  }

  private function postAsync(bundle : Bundle, batchPayments : BatchPayment[]) {
    for (var batchPayment in batchPayments) {
      batchPayment = bundle.add(batchPayment)
      batchPayment.postAsync()
      UnsavedWorkHelper.clearUnsavedWorkEntryForDestination(BatchPaymentDetailsPage.createDestination(batchPayment))
    }
  }

  function isAtLeastOnePaymentWasReversedWithin(selectedBatches: BatchPayment[]): boolean {
    return selectedBatches.hasMatch(\batchPayment -> batchPayment.AtLeastOnePaymentReversed)
  }

  private function reverseAsync(bundle : Bundle, batchPayments : BatchPayment[]) {
    for (var batchPayment in batchPayments) {
      batchPayment = bundle.add(batchPayment)
      batchPayment.reverseAsync()
    }
  }

}
