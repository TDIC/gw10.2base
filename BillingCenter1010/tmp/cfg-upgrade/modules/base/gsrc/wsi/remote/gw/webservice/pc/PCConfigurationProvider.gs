package wsi.remote.gw.webservice.pc

uses gw.xml.ws.IWsiWebserviceConfigurationProvider
uses gw.xml.ws.WsdlConfig

uses javax.xml.namespace.QName

@Export
class PCConfigurationProvider implements IWsiWebserviceConfigurationProvider {

  override function configure(serviceName : QName, portName : QName, config : WsdlConfig)  {
    config.Guidewire.Authentication.Username = "su"
    config.Guidewire.Authentication.Password = "gw"
  }

}
