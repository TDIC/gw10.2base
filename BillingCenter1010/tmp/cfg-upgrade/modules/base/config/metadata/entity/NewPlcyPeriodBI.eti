<?xml version="1.0"?>
<subtype
  xmlns="http://guidewire.com/datamodel"
  abstract="true"
  desc="Abstract base type for policy related billing instructions which create new Policy Period"
  entity="NewPlcyPeriodBI"
  supertype="PlcyBillingInstruction">
  <implementsInterface
    iface="com.guidewire.bc.domain.accounting.billinginstruction.NewPlcyPeriodBIPublicMethods"
    impl="com.guidewire.bc.domain.accounting.billinginstruction.impl.NewPlcyPeriodBIImpl"/>
  <array
    arrayentity="ProducerCodeRoleEntry"
    desc="The new policy period's producer codes"
    exportable="false"
    name="ProducerCodes"/>
  <foreignkey
    columnName="PriorPolicyPeriodID"
    desc="The previous policy period"
    exportasid="true"
    fkentity="PolicyPeriod"
    name="PriorPolicyPeriod"
    nullok="true"
    setterScriptability="hidden"/>
  <foreignkey
    columnName="PolicyPaymentPlanID"
    desc="The base payment plan to use for the new Policy Period. Any appropriate modifiers will be added and attached to new payment plan in the PlcyBillingInstruction."
    exportasid="true"
    fkentity="PaymentPlan"
    name="PolicyPaymentPlan"
    nullok="true"/>
  <foreignkey
    columnName="NewPolicyPeriodID"
    desc="The policy period created by this BI"
    fkentity="PolicyPeriod"
    name="NewPolicyPeriod"
    nullok="false"
    setterScriptability="hidden"/>
  <dbcheckbuilder
    className="com.guidewire.bc.system.database.dbchecks.accounting.billinginstruction.NewPlcyPeriodBIDBCheckBuilder"/>
</subtype>
