<?xml version="1.0"?>
<delegate
  xmlns="http://guidewire.com/datamodel"
  name="ItemState">
  <implementsInterface
    iface="com.guidewire.bc.domain.invoice.ItemStatePublicMethods"
    impl="com.guidewire.bc.domain.invoice.impl.ItemStateImpl"/>
  <fulldescription><![CDATA[
        This delegate contains a set of dynamic state information about an invoice item,
        including the commission amount of the invoice item for the active primary producer (which can change when there
        is
        a commission override) and the current amount of commission paid on the invoice item for the active primary
        producer
        (which changes whenever there is a payment against the invoice item)
    ]]></fulldescription>
  <monetaryamount
    amountColumnName="PrimaryAgencyBillRetained"
    desc="The amount of this InvoiceItem's primary active commission that the producer has retained from agency bill payments."
    loadable="false"
    loadedByCallback="true"
    name="PrimaryAgencyBillRetained"
    nullok="false"
    scaleToCurrency="true"
    setterScriptability="hidden"
    soapnullok="true">
    <tag
      name="DefaultValueZero"/>
  </monetaryamount>
  <monetaryamount
    amountColumnName="PrimaryDirectBillEarned"
    desc="the amount of this InvoiceItems's primary active commission that has been earned by the producer via direct bill.  (It includes the amount that has been earned and not yet sent to the producer as well as the amount that has been earned and already sent.)"
    loadable="false"
    loadedByCallback="true"
    name="PrimaryDirectBillEarned"
    nullok="false"
    scaleToCurrency="true"
    setterScriptability="hidden"
    soapnullok="true">
    <tag
      name="DefaultValueZero"/>
  </monetaryamount>
  <monetaryamount
    amountColumnName="PrimaryCommissionAmount"
    desc="Denormalization of the commission amount for the invoice item for the active primary producer"
    loadable="false"
    loadedByCallback="true"
    name="PrimaryCommissionAmount"
    nullok="false"
    scaleToCurrency="true"
    setterScriptability="doesNotExist"
    soapnullok="true">
    <tag
      name="DefaultValueZero"/>
  </monetaryamount>
  <monetaryamount
    amountColumnName="PrimaryWrittenOffCommission"
    currencyColumnName="PrimaryWrittenOffCmsn_cur"
    desc="Denormalization of the amount of invoice item's commission for the active primary producer that was written off"
    loadable="false"
    loadedByCallback="true"
    name="PrimaryWrittenOffCommission"
    nullok="false"
    scaleToCurrency="true"
    setterScriptability="doesNotExist"
    soapnullok="true">
    <tag
      name="DefaultValueZero"/>
  </monetaryamount>
  <monetaryamount
    amountColumnName="PromisedCommission"
    desc="Denormalization of the total primary commission promised on this invoice item"
    loadable="false"
    loadedByCallback="true"
    name="PromisedCommission"
    nullok="false"
    scaleToCurrency="true"
    setterScriptability="hidden"
    soapnullok="true">
    <tag
      name="DefaultValueZero"/>
  </monetaryamount>
  <monetaryamount
    amountColumnName="PromisedAndPaidAmount"
    desc="Denormalization of the total gross amount promised and paid on this invoice item"
    loadable="false"
    loadedByCallback="true"
    name="PromisedAndPaidAmount"
    nullok="false"
    scaleToCurrency="true"
    setterScriptability="hidden"
    soapnullok="true">
    <tag
      name="DefaultValueZero"/>
  </monetaryamount>
  <column
    desc="The number describing where the associated invoice item falls on the invoice.  This should be a unique and unchanging number."
    name="LineItemNumber"
    nullok="true"
    type="integer"/>
</delegate>
