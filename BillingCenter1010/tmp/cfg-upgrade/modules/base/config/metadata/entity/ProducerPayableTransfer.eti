<?xml version="1.0"?>
<entity
  xmlns="http://guidewire.com/datamodel"
  desc="Wrapper entity for ProducerPayableTransferTxn transaction that debits CommissionsPayable of one Producer and credits Cash of another producer"
  entity="ProducerPayableTransfer"
  table="producerpayabletransfer"
  type="retireable">
  <implementsInterface
    iface="com.guidewire.bc.domain.producer.impl.ProducerPayableTransferInternalMethods"
    impl="com.guidewire.bc.domain.producer.impl.ProducerPayableTransferImpl"/>
  <implementsInterface
    iface="gw.bc.approval.ActivityApprovable"
    impl="com.guidewire.bc.domain.producer.impl.ProducerPayableTransferImpl"/>
  <implementsInterface
    iface="com.guidewire.bc.domain.producer.ProducerPayableTransferPublicMethods"
    impl="com.guidewire.bc.domain.producer.impl.ProducerPayableTransferImpl"/>
  <implementsInterface
    iface="com.guidewire.pl.domain.approval.impl.ApprovableInternalMethods"
    impl="com.guidewire.bc.domain.producer.impl.ProducerPayableTransferImpl"/>
  <implementsEntity
    name="Approvable"/>
  <implementsEntity
    name="InCurrencySilo"/>
  <monetaryamount
    amountColumnName="Amount"
    amountRange="nonnegative"
    desc="The amount to debit from CommissionsPayable of one producer and credit to Cash of another producer"
    name="Amount"
    nullok="false"
    scaleToCurrency="true"/>
  <foreignkey
    columnName="DebitsPayableOfID"
    desc="The producer whose CommissionsPayable t-account will be debited"
    fkentity="Producer"
    name="DebitsPayableOf"
    nullok="false"/>
  <foreignkey
    columnName="CreditsPayableOfID"
    desc="The producer whose CommissionsPayable t-account will be credited"
    fkentity="Producer"
    name="CreditsPayableOf"
    nullok="false"/>
  <array
    arrayentity="ProducerPayableTransferApprovalActivity"
    name="ActivitiesCreatedByApproval"
    setterScriptability="doesNotExist">
    <link-association>
      <typelist-map
        field="Status"/>
    </link-association>
  </array>
  <dbcheckbuilder
    className="com.guidewire.bc.system.database.dbchecks.accounting.ProducerPayableTransferDBCheckBuilder"/>
</entity>
