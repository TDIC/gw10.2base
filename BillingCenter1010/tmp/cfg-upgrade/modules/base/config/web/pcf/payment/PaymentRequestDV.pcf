<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <DetailViewPanel
    id="PaymentRequestDV">
    <Require
      name="paymentRequest"
      type="PaymentRequest"/>
    <Variable
      initialValue="paymentRequest.Account"
      name="account"
      type="entity.Account"/>
    <Variable
      initialValue="new gw.payment.PaymentInstrumentRange(account.PaymentInstruments)"
      name="paymentInstrumentRange"
      type="gw.payment.PaymentInstrumentRange"/>
    <Variable
      initialValue="new gw.web.account.AccountSummaryFinancialsHelper(account)"
      name="financialsHelper"
      type="gw.web.account.AccountSummaryFinancialsHelper"/>
    <InputColumn>
      <Label
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.AccountInfo&quot;)"/>
      <TextInput
        action="AccountSummary.push(account)"
        id="accountNumber"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.AccountNumber&quot;)"
        value="account.AccountNumber"/>
      <TextInput
        id="accountName"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.AccountName&quot;)"
        value="account.AccountNameLocalized"/>
      <Label
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.PaymentAmount&quot;)"/>
      <MonetaryAmountInput
        currency="paymentRequest.Currency"
        editable="true"
        id="amount"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.Amount&quot;)"
        value="paymentRequest.Amount"/>
      <DateInput
        editable="true"
        id="paymentDate"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.PaymentDate&quot;)"
        validationExpression="((paymentRequest.DraftDate == null) or gw.api.util.DateUtil.verifyDateOnOrAfterToday(paymentRequest.DraftDate)) ? null : DisplayKey.get(&quot;Web.NewPaymentRequestScreen.DraftDateInPast&quot;)"
        value="paymentRequest.DraftDate"/>
      <DateInput
        editable="true"
        id="paymentDueDate"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.PaymentDueDate&quot;)"
        validationExpression="validateDueDate()"
        value="paymentRequest.DueDate"
        visible="paymentRequest.Invoice == null"/>
      <RangeInput
        editable="true"
        id="PaymentInstrument"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.PaymentInstrument&quot;)"
        required="true"
        value="paymentRequest.PaymentInstrument"
        valueRange="gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.paymentRequestPaymentInstrumentFilter)   "
        valueType="entity.PaymentInstrument">
        <MenuItem
          action="NewPaymentInstrumentPopup.push(gw.payment.PaymentInstrumentFilters.paymentRequestPaymentInstrumentOptions,account,true)"
          id="CreateNewPaymentInstrument"
          label="DisplayKey.get(&quot;Web.NewPaymentInstrument.CreateNewPaymentInstrument&quot;)"/>
      </RangeInput>
    </InputColumn>
    <InputColumn>
      <Label
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.AccountStatus&quot;)"/>
      <MonetaryAmountInput
        currency="account.Currency"
        formatType="currency"
        id="accountUnbilled"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.TotalUnbilled&quot;)"
        value="financialsHelper.UnbilledAmount"/>
      <MonetaryAmountInput
        currency="account.Currency"
        formatType="currency"
        id="totalCurrentBilled"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.TotalCurrentBilled&quot;)"
        value="financialsHelper.BilledAmount"/>
      <MonetaryAmountInput
        currency="account.Currency"
        formatType="currency"
        id="totalPastDue"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.TotalPastDue&quot;)"
        value="financialsHelper.DelinquentAmount"/>
      <MonetaryAmountInput
        boldLabel="true"
        currency="account.Currency"
        formatType="currency"
        id="totalOutstanding"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.TotalOutstanding&quot;)"
        value="financialsHelper.OutstandingAmount"/>
      <MonetaryAmountInput
        boldLabel="true"
        currency="account.Currency"
        formatType="currency"
        id="totalUnderContract"
        label="DisplayKey.get(&quot;Web.NewPaymentRequestScreen.TotalUnderContract&quot;)"
        value="financialsHelper.TotalValue"/>
    </InputColumn>
    <Code><![CDATA[uses gw.api.util.DateUtil

function validateDueDate() : String {
  if(paymentRequest.DueDate == null)
    return null

  if(!DateUtil.verifyDateOnOrAfterToday(paymentRequest.DueDate)){
    return DisplayKey.get("Web.NewPaymentRequestScreen.Validation.DueDateInThePast")
  }

  if(paymentRequest.DraftDate != null and DateUtil.compareIgnoreTime(paymentRequest.DraftDate, paymentRequest.DueDate) > 0) {
    return DisplayKey.get("Web.NewPaymentRequestScreen.Validation.DueDateBeforeDraftDate")
  }

  return null
}]]></Code>
  </DetailViewPanel>
</PCF>