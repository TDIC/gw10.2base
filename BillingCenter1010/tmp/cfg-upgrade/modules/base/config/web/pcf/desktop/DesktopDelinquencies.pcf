<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Page
    canEdit="true"
    canVisit="perm.System.mydelinquenciesview and perm.System.viewdesktop"
    id="DesktopDelinquencies"
    title="DisplayKey.get(&quot;Web.DesktopDelinquencies&quot;)">
    <LocationEntryPoint
      signature="DesktopDelinquencies()"/>
    <Variable
      initialValue="gw.api.util.DateUtil.currentDate()"
      name="today"
      type="java.util.Date"/>
    <!-- EDF: Note that the query here is filtered by the ToolbarFilter below,
    so despite first appearances, this shouldn't be a performance problem. -->
    <Variable
      initialValue="gw.api.database.Query.make(DelinquencyProcess).select()"
      name="delinquencyProcesses"
      type="gw.api.database.IQueryBeanResult&lt;DelinquencyProcess&gt;"/>
    <Screen
      id="DesktopDelinquenciesScreen">
      <TitleBar
        title="DisplayKey.get(&quot;Web.DesktopDelinquencies&quot;)"/>
      <Toolbar/>
      <ListViewPanel
        id="DesktopDelinquenciesLV"
        stretch="true">
        <RowIterator
          appendPageInfo="true"
          checkBoxVisible="false"
          editable="false"
          elementName="delinquencyProcess"
          numEntriesRequired="0"
          value="delinquencyProcesses"
          valueType="gw.api.database.IQueryBeanResult&lt;entity.DelinquencyProcess&gt;">
          <Variable
            initialValue="new gw.web.account.AccountSummaryFinancialsHelper(delinquencyProcess.Account)"
            name="financialsHelper"
            type="gw.web.account.AccountSummaryFinancialsHelper"/>
          <ToolbarFilter
            name="Filter">
            <ToolbarFilterOption
              filter="new gw.api.web.delinquency.DelinquencyProcessFilterSet.OpenedThisWeek()"
              selectOnEnter="true"/>
            <ToolbarFilterOption
              filter="new gw.api.web.delinquency.DelinquencyProcessFilterSet.AllOpen()"/>
          </ToolbarFilter>
          <Row>
            <DateCell
              action="AccountDetailDelinquencies.push(delinquencyProcess.Account)"
              id="delinquencyDate"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.DelinquencyDate&quot;)"
              sortDirection="descending"
              sortOrder="1"
              value="delinquencyProcess.StartDate"/>
            <TextCell
              action="AccountSummary.push(delinquencyProcess.Account)"
              enableSort="false"
              grow="true"
              id="account"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.Account&quot;)"
              value="delinquencyProcess.Account.AccountNumber"
              wrap="false"/>
            <TextCell
              enableSort="false"
              grow="true"
              id="insured"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.Insured&quot;)"
              value="delinquencyProcess.Account.AccountNameLocalized"/>
            <TextCell
              enableSort="false"
              id="age"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.Age&quot;)"
              outputConversion="DisplayKey.get(&quot;Web.DesktopDelinquencies.AgeInDays&quot;, VALUE)"
              value="delinquencyProcess.Age"
              valueType="java.math.BigDecimal"
              wrap="false"/>
            <TextCell
              enableSort="false"
              id="currentEvent"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.CurrentEvent&quot;)"
              value="delinquencyProcess.PreviousEvent"
              valueType="entity.DelinquencyProcessEvent"
              wrap="false"/>
            <MonetaryAmountCell
              align="right"
              currency="delinquencyProcess.Currency"
              enableSort="false"
              formatType="currency"
              id="totalUnbilled"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.TotalUnbilled&quot;)"
              value="financialsHelper.UnbilledAmount"/>
            <MonetaryAmountCell
              align="right"
              currency="delinquencyProcess.Currency"
              enableSort="false"
              formatType="currency"
              id="billedCurrent"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.BilledCurrent&quot;)"
              value="financialsHelper.BilledAmount"/>
            <MonetaryAmountCell
              align="right"
              currency="delinquencyProcess.Currency"
              enableSort="false"
              formatType="currency"
              id="pastDue"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.PastDue&quot;)"
              value="financialsHelper.DelinquentAmount"/>
            <MonetaryAmountCell
              align="right"
              currency="delinquencyProcess.Currency"
              enableSort="false"
              formatType="currency"
              id="total"
              label="DisplayKey.get(&quot;Web.DesktopDelinquencies.Total&quot;)"
              value="financialsHelper.TotalValue"/>
          </Row>
        </RowIterator>
      </ListViewPanel>
    </Screen>
  </Page>
</PCF>