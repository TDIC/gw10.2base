<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <DetailViewPanel
    id="PaymentSearchDV">
    <Require
      name="searchCriteria"
      type="gw.search.PaymentSearchCriteria"/>
    <InputColumn>
      <Label
        id="SearchCriteria"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.SearchCriteria&quot;)"/>
      <RangeInput
        editable="true"
        id="PaymentTypeCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.PaymentType&quot;)"
        required="true"
        value="searchCriteria.PaymentType"
        valueRange="gw.search.PaymentSearchType.AllValues"
        valueType="gw.search.PaymentSearchType">
        <PostOnChange/>
      </RangeInput>
      <PickerInput
        conversionExpression="PickedValue.DisplayName"
        editable="true"
        id="AccountNumberCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.AccountNumber&quot;)"
        pickLocation="AccountSearchPopup.push()"
        value="searchCriteria.AccountNumber"
        valueType="java.lang.String"
        visible="searchCriteria.PaymentType == DIRECTBILL_AND_SUSPENSE"/>
      <PickerInput
        conversionExpression="PickedValue.DisplayName"
        editable="true"
        id="PolicyNumberCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.PolicyNumber&quot;)"
        pickLocation="PolicySearchPopup.push(true)"
        value="searchCriteria.PolicyNumber"
        valueType="java.lang.String"
        visible="searchCriteria.PaymentType == DIRECTBILL_AND_SUSPENSE"/>
      <PickerInput
        conversionExpression="PickedValue.Name"
        editable="true"
        id="ProducerNameCriterion"
        label="searchCriteria.getDisplayKeyForProducerNameKanji()"
        pickLocation="ProducerSearchPopup.push()"
        value="searchCriteria.ProducerName"
        valueType="java.lang.String"
        visible="searchCriteria.PaymentType == gw.search.PaymentSearchType.AGENCYBILL"/>
      <PickerInput
        conversionExpression="PickedValue.NameKanji"
        editable="true"
        id="ProducerNameKanjiCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.ProducerName&quot;)"
        pickLocation="ProducerSearchPopup.push()"
        value="searchCriteria.ProducerNameKanji"
        valueType="java.lang.String"
        visible="(searchCriteria.PaymentType == gw.search.PaymentSearchType.AGENCYBILL) &amp;&amp; (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP)"/>
      <TextInput
        editable="true"
        id="ProducerCodeCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.ProducerCode&quot;)"
        value="searchCriteria.ProducerCode"
        visible="searchCriteria.PaymentType == gw.search.PaymentSearchType.AGENCYBILL"/>
      <TypeKeyInput
        editable="true"
        id="CurrencyCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.Currency&quot;)"
        value="searchCriteria.CurrencyType"
        valueType="typekey.Currency"
        visible="gw.api.util.CurrencyUtil.isMultiCurrencyMode()">
        <PostOnChange
          onChange="blankMinimumAndMaximumFields()"/>
      </TypeKeyInput>
      <RangeInput
        editable="true"
        id="MethodCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.Method&quot;)"
        value="searchCriteria.Method"
        valueRange="getPaymentMethodRange()"
        valueType="typekey.PaymentMethod"/>
      <TextInput
        editable="true"
        id="TokenCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.Token&quot;)"
        value="searchCriteria.Token"/>
      <TextInput
        editable="true"
        id="CheckNumberCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.CheckNumber&quot;)"
        value="searchCriteria.CheckNumber"/>
      <DateInput
        editable="true"
        id="EarliestDateCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.EarliestDate&quot;)"
        value="searchCriteria.EarliestDate"/>
      <DateInput
        editable="true"
        id="LatestDateCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.LatestDate&quot;)"
        value="searchCriteria.LatestDate"/>
      <MonetaryAmountInput
        available="(searchCriteria.CurrencyType != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())"
        currency="gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.CurrencyType)"
        editable="true"
        id="MinAmountCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.MinAmount&quot;)"
        value="searchCriteria.MinAmount"/>
      <MonetaryAmountInput
        available="(searchCriteria.CurrencyType != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())"
        currency="gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.CurrencyType)"
        editable="true"
        id="MaxAmountCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.MaxAmount&quot;)"
        value="searchCriteria.MaxAmount"/>
      <CheckBoxInput
        editable="true"
        id="HasSuspenseItemsCriterion"
        label="DisplayKey.get(&quot;Web.PaymentSearchDV.HasActiveSuspenseItems&quot;)"
        value="searchCriteria.OwnerOfActiveSuspenseItems"/>
    </InputColumn>
    <InputColumn>
      <InputSetRef
        def="ContactCriteriaInputSet(searchCriteria.ContactCriteria)"/>
    </InputColumn>
    <InputFooterSection>
      <InputSetRef
        def="SearchAndResetInputSet()"/>
    </InputFooterSection>
    <Code><![CDATA[// The AgencyMoneyReceivedDV page uses the PaymentSearchCriteria class
// as well, since the functionality is identical. Keep that in mind when
// modifying this page and the PaymentSearchCriteria class.

function getPaymentMethodRange() : java.util.List<PaymentMethod> {
  var paymentMethodRange = new java.util.ArrayList<PaymentMethod>();
  paymentMethodRange.add( PaymentMethod.TC_ACH );
  paymentMethodRange.add( PaymentMethod.TC_CASH );
  paymentMethodRange.add( PaymentMethod.TC_CHECK );
  paymentMethodRange.add( PaymentMethod.TC_CREDITCARD );
  paymentMethodRange.add( PaymentMethod.TC_WIRE );
  return paymentMethodRange;
}

function blankMinimumAndMaximumFields() {
  searchCriteria.MinAmount = null
  searchCriteria.MaxAmount = null
}]]></Code>
  </DetailViewPanel>
</PCF>