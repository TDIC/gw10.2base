<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Page
    beforeCommit="invoices = account.InvoicesSortedByDate; canEditNextInvoiceDate = (account.NextInvoice != null);"
    canEdit="perm.System.invcdateedit"
    canVisit="perm.System.accttabview and perm.System.acctinvcview"
    id="AccountDetailInvoices"
    showUpLink="true"
    title="DisplayKey.get(&quot;Web.AccountDetailInvoices.Title&quot;)">
    <LocationEntryPoint
      signature="AccountDetailInvoices(account : Account)"/>
    <LocationEntryPoint
      signature="AccountDetailInvoices(account : Account, selectedInvoice : AccountInvoice)"/>
    <Variable
      initialValue="gw.api.util.DateUtil.currentDate()"
      name="today"
      type="java.util.Date"/>
    <Variable
      name="account"
      type="Account"/>
    <Variable
      initialValue="account.InvoicesSortedByDate"
      name="invoices"
      type="entity.AccountInvoice[]"/>
    <Variable
      name="selectedInvoice"
      type="AccountInvoice"/>
    <Variable
      initialValue="null"
      name="invoiceResent"
      type="String"/>
    <Variable
      initialValue="account.BillingPlan.Aggregation"
      name="aggregation"
      type="AggregationType"/>
    <Variable
      initialValue="account.NextInvoice != null"
      name="canEditNextInvoiceDate"
      type="boolean"/>
    <Variable
      initialValue="null"
      name="selectedInvoiceStream"
      type="InvoiceStream"/>
    <Screen
      id="AccountDetailInvoicesScreen">
      <AlertBar
        action="invoiceResent = null"
        id="AccountDetailInvoices_InvoiceSentAlertBar"
        label="DisplayKey.get(&quot;Web.AccountDetailInvoices.InvoiceSent&quot;, invoiceResent)"
        visible="invoiceResent != null"/>
      <ListDetailPanel
        id="DetailPanel"
        selectionName="invoice"
        selectionOnEnter="selectedInvoice"
        selectionType="AccountInvoice">
        <PanelRef>
          <Toolbar>
            <ToolbarRangeInput
              id="InvoiceStreamFilter"
              label="DisplayKey.get(&quot;Web.AccountDetailInvoices.InvoiceStreamFilter&quot;)"
              noneSelectedLabel="DisplayKey.get(&quot;Web.AccountDetailInvoices.ShowAllInvoices&quot;)"
              value="selectedInvoiceStream"
              valueRange="account.InvoiceStreams.sort(\ invoiceStream1, invoiceStream2 -&gt; invoiceStream1.DisplayName &lt;= invoiceStream2.DisplayName)"
              valueType="entity.InvoiceStream">
              <PostOnChange
                onChange="updateInvoices()"/>
            </ToolbarRangeInput>
            <ToolbarButton
              action="NewInvoice.go(account, selectedInvoiceStream)"
              available="perm.Account.invccreate and  selectedInvoiceStream != null"
              id="AccountDetailInvoices_NewInvoiceButton"
              label="DisplayKey.get(&quot;Web.AccountDetailInvoices.NewInvoice&quot;)"
              visible="true"/>
            <CheckedValuesToolbarButton
              allCheckedRowsAction="gw.api.web.invoice.InvoiceUtil.deleteInvoices(CheckedValues); invoices = account.InvoicesSortedByDate; canEditNextInvoiceDate = (account.NextInvoice != null);"
              available="perm.Account.invcremove"
              confirmMessage="DisplayKey.get(&quot;Web.AccountDetailInvoices.DeleteInvoices.Confirm&quot;)"
              flags="all DeleteableInvoice"
              id="AccountDetailInvoices_RemoveInvoicesButton"
              iterator="AccountInvoicesLV"
              label="DisplayKey.get(&quot;Web.AccountDetailInvoices.DeleteInvoices&quot;)"
              visible="true"/>
          </Toolbar>
          <ListViewPanel
            id="AccountInvoicesLV"
            stretch="true">
            <RowIterator
              editable="false"
              elementName="invoiceRow"
              hasCheckBoxes="true"
              pageSize="0"
              value="invoices"
              valueType="entity.AccountInvoice[]">
              <ToolbarFlag
                condition="!invoiceRow.isSent() and invoiceRow.InvoiceItems.length == 0"
                name="DeleteableInvoice"/>
              <Row>
                <DateCell
                  footerLabel="DisplayKey.get(&quot;Web.AccountInvoicesLV.Total&quot;)"
                  id="InvoiceDate"
                  label="DisplayKey.get(&quot;Web.AccountInvoicesLV.InvoiceDate&quot;)"
                  value="invoiceRow.EventDate"/>
                <DateCell
                  id="InvoiceDueDate"
                  label="DisplayKey.get(&quot;Web.AccountInvoicesLV.PaymentDueDate&quot;)"
                  value="invoiceRow.PaymentDueDate"/>
                <TextCell
                  id="InvoiceNumber"
                  label="DisplayKey.get(&quot;Web.AccountInvoicesLV.InvoiceNumber&quot;)"
                  value="invoiceRow.InvoiceNumber"/>
                <BooleanRadioCell
                  id="InvoiceAdHoc"
                  label="DisplayKey.get(&quot;Web.AccountInvoicesLV.IsAdHoc&quot;)"
                  value="invoiceRow.isAdHoc()"/>
                <TextCell
                  id="Status"
                  label="DisplayKey.get(&quot;Web.AccountInvoicesLV.Status&quot;)"
                  sortBy="invoiceRow.Status"
                  value="invoiceRow.StatusForUI"/>
                <MonetaryAmountCell
                  currency="account.Currency"
                  footerSumValue="invoiceRow.Amount"
                  formatType="currency"
                  id="Amount"
                  label="DisplayKey.get(&quot;Web.AccountInvoicesLV.Amount&quot;)"
                  value="invoiceRow.Amount"/>
                <MonetaryAmountCell
                  currency="account.Currency"
                  footerSumValue="invoiceRow.AmountDue"
                  formatType="currency"
                  id="CurrentAmountDue"
                  label="DisplayKey.get(&quot;Web.AccountInvoicesLV.CurrentAmountDue&quot;)"
                  value="invoiceRow.AmountDue"/>
                <TextCell
                  grow="true"
                  id="InvoiceStream"
                  label="DisplayKey.get(&quot;Web.AccountInvoicesLV.InvoiceStream&quot;)"
                  value="invoiceRow.InvoiceStream"
                  valueType="entity.InvoiceStream"/>
              </Row>
            </RowIterator>
          </ListViewPanel>
        </PanelRef>
        <CardViewPanel>
          <Variable
            initialValue="invoice.PreviousInvoice"
            name="previousInvoice"
            type="entity.AccountInvoice"/>
          <Card
            id="InvoiceDetailCard"
            title="DisplayKey.get(&quot;Web.AccountDetailInvoices.InvoiceDetail.Title&quot;, gw.api.util.StringUtil.formatDate(invoice.EventDate,&quot;short&quot;))">
            <PanelRef>
              <Toolbar>
                <EditButtons
                  editLabel="DisplayKey.get(&quot;Web.AccountDetailInvoices.EditInvoiceDates&quot;)"
                  editVisible="invoice.Status == TC_PLANNED and perm.Account.invcdateedit"
                  updateConfirmMessage="gw.api.util.EquityValidationHelper.isEquityViolatedCheckAllPolicies(account)"/>
                <ToolbarButton
                  action="gw.api.web.invoice.InvoiceUtil.resendInvoice(invoice);invoiceResent = invoice.InvoiceNumber"
                  available="invoice.Status != TC_PLANNED and perm.Account.invcresend and not CurrentLocation.InEditMode and not invoice.Frozen"
                  id="InvoiceDetailDV_ResendInvoice"
                  label="DisplayKey.get(&quot;Web.AccountDetailInvoices.ResendInvoice&quot;)"/>
              </Toolbar>
              <DetailViewPanel
                id="InvoiceDetailDV">
                <InputColumn>
                  <Label
                    label="DisplayKey.get(&quot;Web.InvoiceDetailDV.InvoiceInfo&quot;)"/>
                  <InputSetRef
                    def="AccountInvoiceInformationInputSet(invoice)"
                    mode="gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()"/>
                </InputColumn>
                <InputColumn>
                  <Label
                    label="DisplayKey.get(&quot;Web.InvoiceDetailDV.Statement&quot;)"/>
                  <MonetaryAmountInput
                    align="right"
                    currency="account.Currency"
                    formatType="currency"
                    id="PriorAmountDue"
                    label="DisplayKey.get(&quot;Web.InvoiceDetailDV.PriorAmountDue&quot;)"
                    value="previousInvoice.OutstandingAmount"/>
                  <MonetaryAmountInput
                    align="right"
                    currency="account.Currency"
                    formatType="currency"
                    id="TotalCharges"
                    label="DisplayKey.get(&quot;Web.InvoiceDetailDV.TotalCharges&quot;)"
                    value="invoice.Status != TC_PLANNED ? invoice.Amount : null"/>
                  <MonetaryAmountInput
                    align="right"
                    currency="account.Currency"
                    formatType="currency"
                    id="TotalAmountDue"
                    label="DisplayKey.get(&quot;Web.InvoiceDetailDV.TotalAmountDue&quot;)"
                    value="invoice.OutstandingAmount"/>
                </InputColumn>
              </DetailViewPanel>
            </PanelRef>
            <PanelRef
              def="AccountInvoiceItemsLV(invoice, aggregation)"
              mode="gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()">
              <TitleBar
                title="DisplayKey.get(&quot;Web.InvoiceDetailDV.InvoiceCharges&quot;)"/>
              <Toolbar>
                <ToolbarRangeInput
                  id="AggregationTypeSelector"
                  label="DisplayKey.get(&quot;Web.InvoiceItemsLV.AggregationType&quot;)"
                  required="true"
                  value="aggregation"
                  valueRange="typekey.AggregationType.getTypeKeys(false)"
                  valueType="typekey.AggregationType">
                  <PostOnChange/>
                </ToolbarRangeInput>
              </Toolbar>
            </PanelRef>
          </Card>
        </CardViewPanel>
      </ListDetailPanel>
    </Screen>
    <Code><![CDATA[function updateInvoices() {
      invoices = selectedInvoiceStream != null
        ? account.InvoicesSortedByDate.where(\ i -> i.InvoiceStream == selectedInvoiceStream)
        : account.InvoicesSortedByDate
    }]]></Code>
  </Page>
</PCF>