<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Screen
    id="NewProducerWizardBasicStepScreen">
    <Require
      name="producer"
      type="Producer"/>
    <Variable
      initialValue="producer.AccountRep.DisplayName"
      name="accountRepDisplayName"
      type="String"/>
    <Variable
      initialValue="gw.api.database.Query.make(SecurityZone).select()"
      name="allSecurityZones"
      type="gw.api.database.IQueryBeanResult&lt;SecurityZone&gt;"/>
    <Variable
      initialValue="new gw.payment.PaymentInstrumentRange(producer.PaymentInstruments)"
      name="paymentInstrumentRange"
      type="gw.payment.PaymentInstrumentRange"/>
    <Toolbar>
      <WizardButtons/>
    </Toolbar>
    <DetailViewPanel
      id="NewProducerDV">
      <InputColumn>
        <Label
          label="DisplayKey.get(&quot;Web.NewProducerDV.Basics&quot;)"/>
        <TextInput
          editable="true"
          id="Name"
          label="gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP ? DisplayKey.get(&quot;Web.NewProducerDV.Basics.NamePhonetic&quot;) : DisplayKey.get(&quot;Web.NewProducerDV.Basics.Name&quot;)"
          value="producer.Name"/>
        <TextInput
          editable="true"
          id="NameKanji"
          label="DisplayKey.get(&quot;Web.NewProducerDV.Basics.Name&quot;)"
          value="producer.NameKanji"
          visible="gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP"/>
        <TypeKeyInput
          editable="true"
          id="Tier"
          label="DisplayKey.get(&quot;Web.NewProducerDV.Basics.Tier&quot;)"
          value="producer.Tier"
          valueType="typekey.ProducerTier">
          <PostOnChange/>
        </TypeKeyInput>
        <TextInput
          id="Currency"
          label="DisplayKey.get(&quot;Web.NewProducerDV.Basics.Currency&quot;)"
          value="producer.Currency"
          valueType="typekey.Currency"/>
        <RangeInput
          editable="true"
          id="SecurityZone"
          label="DisplayKey.get(&quot;Web.AccountDetailDV.SecurityZone&quot;)"
          value="producer.SecurityZone"
          valueRange="allSecurityZones"
          valueType="entity.SecurityZone"/>
        <Label
          label="DisplayKey.get(&quot;Web.NewProducerDV.DirectBill&quot;)"/>
        <TypeKeyInput
          editable="true"
          id="Periodicity"
          label="DisplayKey.get(&quot;Web.NewProducerDV.DirectBill.PaymentPeriodicity&quot;)"
          value="producer.RecurPeriodicity"
          valueType="typekey.Periodicity"/>
        <InputDivider/>
        <InputSetRef
          def="ProducerHoldStatementInputSet(producer)"/>
        <InputDivider/>
        <RangeInput
          editable="true"
          id="PaymentInstrument"
          label="DisplayKey.get(&quot;Web.NewProducerDV.DirectBill.PaymentInstrument&quot;)"
          required="true"
          value="producer.DefaultPaymentInstrument"
          valueRange="gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentFilter)"
          valueType="entity.PaymentInstrument">
          <MenuItem
            action="NewPaymentInstrumentPopup.push(gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentOptions,producer,false)"
            id="CreateNewPaymentInstrument"
            label="DisplayKey.get(&quot;Web.NewPaymentInstrument.CreateNewPaymentInstrument&quot;)"/>
        </RangeInput>
        <TextInput
          editable="true"
          id="CommissionDayOfMonth"
          label="DisplayKey.get(&quot;Web.NewProducerDV.CommissionDayOfMonth&quot;)"
          requestValidationExpression="VALUE != null and VALUE &gt; 0 and VALUE &lt;= 31 ? null : DisplayKey.get(&quot;Java.Account.InvoiceDayOfMonth.ValidationError&quot;)"
          value="producer.RecurDayOfMonth"
          valueType="java.lang.Integer"/>
        <Label
          label="DisplayKey.get(&quot;Web.NewProducerDV.AgencyBill&quot;)"/>
        <RangeInput
          editable="true"
          id="AgencyBillPlan"
          label="DisplayKey.get(&quot;Web.NewProducerDV.AgencyBill.Plan&quot;)"
          required="false"
          value="producer.AgencyBillPlan"
          valueRange="Plan.finder.findAllAvailablePlans&lt;AgencyBillPlan&gt;(AgencyBillPlan, producer.Currency)"
          valueType="entity.AgencyBillPlan"/>
        <PickerInput
          conversionExpression="PickedValue.DisplayName"
          editable="true"
          id="AccountRep"
          label="DisplayKey.get(&quot;Web.ProducerDetail.AccountRep&quot;)"
          pickLocation="UserSearchPopup.push()"
          validationExpression="validateAndSetAccountRep()"
          value="accountRepDisplayName"
          valueType="java.lang.String"/>
        <Label
          label="DisplayKey.get(&quot;Web.NewProducerDV.PrimaryContact&quot;)"/>
        <TextInput
          id="PrimaryContactName"
          label="DisplayKey.get(&quot;Web.NewProducerDV.PrimaryContact.Name&quot;)"
          value="producer.PrimaryContact.DisplayName"/>
        <TextInput
          id="Address"
          label="DisplayKey.get(&quot;Web.NewProducerDV.PrimaryContact.Address&quot;)"
          value="new gw.api.address.AddressFormatter().format(producer.PrimaryContact.Contact.PrimaryAddress, &quot;\n&quot;)"/>
        <InputSetRef
          def="GlobalPhoneInputSet(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(producer.PrimaryContact.Contact, Contact#WorkPhone), DisplayKey.get(&quot;Web.NewProducerDV.PrimaryContact.Phone&quot;)))"
          editable="false"
          id="PrimaryContactPhone"/>
        <TextInput
          id="PrimaryContactEmail"
          label="DisplayKey.get(&quot;Web.NewProducerDV.PrimaryContact.Email&quot;)"
          value="producer.PrimaryContact.Contact.EmailAddress1"/>
      </InputColumn>
      <InputFooterSection>
        <ListViewInput
          boldLabel="true"
          label="DisplayKey.get(&quot;Web.NewProducerDV.Contacts&quot;)"
          labelAbove="true"
          validationExpression="producer.checkPrimaryContact()">
          <Toolbar>
            <ToolbarButton
              hideIfReadOnly="true"
              id="addNewContact"
              label="DisplayKey.get(&quot;Button.Add&quot;)">
              <MenuItem
                action="NewProducerContactPopup.push(producer, Company)"
                id="addNewCompany"
                label="DisplayKey.get(&quot;Web.PolicyDetailContacts.AddNewCompany&quot;)"/>
              <MenuItem
                action="NewProducerContactPopup.push(producer, Person)"
                id="addNewPerson"
                label="DisplayKey.get(&quot;Web.PolicyDetailContacts.AddNewPerson&quot;)"/>
            </ToolbarButton>
            <IteratorButtons
              addVisible="false"
              iterator="NewProducerContactsLV"/>
            <PickerToolbarButton
              action="ContactSearchPopup.push(true)"
              available="true"
              hideIfReadOnly="true"
              id="addExistingContact"
              label="DisplayKey.get(&quot;Web.ProducerContacts.AddExistingContact&quot;)"
              onPick="gw.contact.ContactConnection.connectContactToProducer(PickedValue, producer)"/>
          </Toolbar>
          <ListViewPanel
            id="NewProducerContactsLV">
            <RowIterator
              editable="true"
              elementName="producerContact"
              id="producerContactIterator"
              toRemove="producer.removeFromContacts(producerContact)"
              value="producer.Contacts"
              valueType="entity.ProducerContact[]">
              <Row>
                <LinkCell
                  id="Edit">
                  <Link
                    action="ProducerContactDetailPopup.push(producerContact)"
                    hideIfReadOnly="true"
                    id="EditLink"
                    label="DisplayKey.get(&quot;Web.NewProducerContactsLV.Edit&quot;)"
                    styleClass="miniButton"/>
                </LinkCell>
                <TextCell
                  id="ContactName"
                  label="DisplayKey.get(&quot;Web.NewProducerContactsLV.Name&quot;)"
                  value="producerContact.DisplayName"/>
                <TextCell
                  id="ContactAddress"
                  label="DisplayKey.get(&quot;Web.NewProducerContactsLV.Address&quot;)"
                  value="producerContact.Contact.PrimaryAddress"
                  valueType="entity.Address"/>
                <TextCell
                  id="ContactRoles"
                  label="DisplayKey.get(&quot;Web.NewProducerContactsLV.Roles&quot;)"
                  value="gw.api.web.producer.ProducerUtil.getRolesForDisplay(producerContact)"/>
              </Row>
            </RowIterator>
          </ListViewPanel>
        </ListViewInput>
      </InputFooterSection>
    </DetailViewPanel>
    <Code><![CDATA[function validateAndSetAccountRep() : String {
  if (accountRepDisplayName == null) {
    producer.AccountRep = null
    return null
  }
  
  var allUsers = gw.api.database.Query.make(User).select().toList()
  var user = allUsers.firstWhere( \ user -> user.DisplayName == accountRepDisplayName )
  if (user == null) {
    return DisplayKey.get("Web.ProducerDetail.InvalidAccountRep")  
  } else {
    producer.AccountRep = user
    return null
  }
}]]></Code>
  </Screen>
</PCF>