<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Screen
    editable="editing"
    id="NewMultiPaymentScreen">
    <Require
      name="newMultiPayment"
      type="NewMultiPayment"/>
    <Require
      name="currency"
      type="Currency"/>
    <Require
      name="editing"
      type="boolean"/>
    <Variable
      initialValue="new gw.payment.PaymentInstrumentRange( getMultiPaymentInstruments() )"
      name="paymentInstrumentRange"
      type="gw.payment.PaymentInstrumentRange"/>
    <Toolbar>
      <AddButton
        id="AddEmptyPayments"
        iterator="NewMultiPaymentLV"
        label="DisplayKey.get(&quot;Button.Add&quot;)"
        toCreateAndAdd="newMultiPayment.addEmptyPaymentRows()"/>
      <IteratorButtons
        iterator="NewMultiPaymentLV"/>
      <ToolbarDivider/>
      <CheckedValuesToolbarButton
        allCheckedRowsAction="validateRowOkToSplit(CheckedValues[0]);AgencyMultiPaymentSplitConfirmationPopup.push(CheckedValues[0])"
        flags="one Row"
        id="SplitMultiPayment"
        iterator="NewMultiPaymentLV"
        label="DisplayKey.get(&quot;Web.NewMultiPaymentScreen.Split&quot;)"
        visible="editing"/>
      <ToolbarDivider/>
      <WizardButtons/>
    </Toolbar>
    <ListViewPanel
      editable="true"
      id="NewMultiPaymentLV">
      <RowIterator
        checkBoxVisible="editing"
        editable="true"
        elementName="newPayment"
        hasCheckBoxes="true"
        toRemove="newMultiPayment.removeFromPayments(newPayment)"
        value="editing ? newMultiPayment.Payments : newMultiPayment.NonBlankPaymentEntries"
        valueType="entity.PaymentEntry[]">
        <ToolbarFlag
          name="Row"/>
        <Row>
          <PickerCell
            editable="newPayment.PolicyNumber == null and newPayment.Invoice == null and newPayment.Producer == null"
            footerLabel="DisplayKey.get(&quot;Web.NewMultiPaymentLV.Total&quot;)"
            id="Account"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.AccountNumber&quot;)"
            pickLocation="AccountSearchPopup.push()"
            validationExpression="newPayment.UnusedEntry ? null : newPayment.verifyAccount()"
            value="newPayment.AccountNumber"
            valueType="java.lang.String">
            <PostOnChange/>
          </PickerCell>
          <PickerCell
            editable="newPayment.AccountNumber == null and newPayment.PolicyNumber == null and newPayment.Producer == null and newPayment.SuspensePayment == MultiPaymentType.TC_PAYMENT"
            id="Invoice"
            inputConversion="PaymentEntry.findInvoiceFromInvoiceNumber(VALUE)"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.InvoiceNumber&quot;)"
            outputConversion="newPayment.Invoice.InvoiceNumber"
            pickLocation="InvoiceSearchPopup.push()"
            validationExpression="newPayment.UnusedEntry ? null : newPayment.verifyInvoice()"
            value="newPayment.Invoice"
            valueType="entity.Invoice">
            <PostOnChange/>
          </PickerCell>
          <PickerCell
            editable="newPayment.AccountNumber == null and newPayment.Invoice == null and newPayment.Producer == null"
            id="PolicyPeriod"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.PolicyNumber&quot;)"
            pickLocation="PolicySearchPopup.push(true)"
            validationExpression="newPayment.UnusedEntry ? null : newPayment.verifyPolicy()"
            value="newPayment.PolicyNumber"
            valueType="java.lang.String">
            <PostOnChange/>
          </PickerCell>
          <PickerCell
            editable="newPayment.AccountNumber == null and newPayment.PolicyNumber == null and newPayment.Invoice == null and newPayment.SuspensePayment == MultiPaymentType.TC_PAYMENT"
            id="Producer"
            inputConversion="PaymentEntry.findProducerFromName(VALUE)"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.Producer&quot;)"
            pickLocation="ProducerSearchPopup.push()"
            validationExpression="newPayment.UnusedEntry ? null : newPayment.verifyProducer()"
            value="newPayment.Producer"
            valueType="entity.Producer">
            <PostOnChange/>
          </PickerCell>
          <TypeKeyCell
            editable="true"
            id="PaymentType"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.SuspensePayment&quot;)"
            value="newPayment.SuspensePayment"
            valueType="typekey.MultiPaymentType">
            <PostOnChange
              deferUpdate="false"
              onChange="if (newPayment.SuspensePayment == MultiPaymentType.TC_SUSPENSE) { newPayment.Invoice = null; newPayment.Producer = null }"/>
          </TypeKeyCell>
          <DateCell
            editable="true"
            id="PaymentDate"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.PaymentDate&quot;)"
            required="true"
            value="newPayment.PaymentDate"/>
          <RangeCell
            editable="true"
            id="PaymentInstrument"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.PaymentInstrument&quot;)"
            required="true"
            sortValueRange="false"
            value="newPayment.PaymentInstrument"
            valueRange="paymentInstrumentRange.AvailableInstruments"
            valueType="entity.PaymentInstrument">
            <PostOnChange
              deferUpdate="false"/>
          </RangeCell>
          <TextCell
            available="newPayment.PaymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_CHECK"
            editable="true"
            id="RefNumber"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.RefNumber&quot;)"
            value="newPayment.RefNumber"/>
          <MonetaryAmountCell
            currency="currency"
            editable="true"
            footerSumValue="newPayment.Amount"
            id="Amount"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.Amount&quot;)"
            required="false"
            validationExpression="newPayment.isUnusedEntry() ? null : newPayment.verifyAmount()"
            value="newPayment.Amount"/>
          <TextCell
            editable="true"
            id="Description"
            label="DisplayKey.get(&quot;Web.NewMultiPaymentLV.Description&quot;)"
            value="newPayment.Description"/>
        </Row>
      </RowIterator>
    </ListViewPanel>
    <Code><![CDATA[uses gw.api.util.DisplayableException

function validateRowOkToSplit(rowToBeValidated : PaymentEntry) {
  if (!String.isEmpty(rowToBeValidated.AccountNumber)) {
    throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowWithAccountNumber"))
  }
  else if (rowToBeValidated.IsSuspensePayment) {
    throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowWithSuspensePayment"))
  }
  else if (rowToBeValidated.Producer == null) {
      throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowNotForProducer"))
  }
  else if (rowToBeValidated.Amount == null || rowToBeValidated.Amount.signum() <=0) {
    throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowWithNonPositiveAmount"))    
  }
  else if (!newMultiPayment.Payments.hasMatch( \ paymentEntry -> paymentEntry.isUnusedEntry())) {
    throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowIfNoEmptyRowsAvailable"))
  }
}

function getMultiPaymentInstruments() : java.util.List<PaymentInstrument> {
  var instruments = new java.util.ArrayList<PaymentInstrument>() { 
    gw.api.web.payment.PaymentInstrumentFactory.getCashPaymentInstrument(),
    gw.api.web.payment.PaymentInstrumentFactory.getCheckPaymentInstrument()
  }
  return instruments
}]]></Code>
  </Screen>
</PCF>