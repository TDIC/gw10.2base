package gw.servertest.enhancements

uses gw.api.domain.invoice.InvoicePayer
uses gw.api.test.BCServerTestClassBase
uses gw.api.util.MonetaryAmountAssertion
uses gw.pl.currency.MonetaryAmount
uses gw.testharness.ChangesCurrentTimeUtil
uses gw.testharness.v3.PLAssertions
uses org.fest.assertions.*

uses java.math.BigDecimal

@Export
enhancement BCServerTestClassBaseBaseEnhancement : BCServerTestClassBase {

  function setCurrentTime(currentTime : Date) {
    ChangesCurrentTimeUtil.setCurrentTime(this, currentTime.getTime())
  }

  function makeBilledWithClockAdvanceToEventDate(invoice : Invoice) : List<ChargeInvoicingTxn> {
    if (invoice.EventDate.addDays(1).after(Date.Today)) {
      setCurrentTime(invoice.EventDate.addDays(1))
    }
    return invoice.makeBilled()
  }

  function firstInvoiceOf(invoicePayer : InvoicePayer) : Invoice {
      return invoicePayer.getInvoicesSortedByEventDate().get(0)
  }

  function assertThat(actual : BigDecimal) : BigDecimalAssert {
    return PLAssertions.assertThat(actual)
  }

  protected function assertThat(monetaryAmount : MonetaryAmount) : MonetaryAmountAssertion {
    return new MonetaryAmountAssertion(monetaryAmount)
  }


}
