package tdic.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses gw.api.system.PLLoggerCategory
uses org.slf4j.LoggerFactory

// BrianS - Class that can be used to copy data when a column has been renamed.
class RenameColumn  extends BeforeUpgradeVersionTrigger {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade");
  private static final var _LOG_TAG = "${RenameColumn.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber;
  private var _tableName : String as readonly TableName;
  private var _oldColumnName : String as readonly OldColumnName;
  private var _newColumnName : String as readonly NewColumnName;

  construct(versionNumber : int, tableName : String, oldColumnName : String, newColumnName : String) {
    super(versionNumber);
    _versionNumber = versionNumber;
    _tableName = tableName;
    _oldColumnName = oldColumnName;
    _newColumnName = newColumnName;
  }

  override public property get Description() : String {
    return "Move data from " + _tableName + "." + _oldColumnName + " to " + _newColumnName;
  }

  // BrianS - No version checks are needed for this upgrade.
  override function hasVersionCheck() : boolean {
    return false;
  }

  override function execute() : void {
    var table = getTable(_tableName);
    var oldColumn = table.getColumn(_oldColumnName);
    var newColumn = table.getColumn(_newColumnName);

    // Step 1:  Create new column.
    _logger.info (_LOG_TAG + "Creating column: " + _tableName + "." + _newColumnName);
    newColumn.create();

    // Step 2:  Copy data to the new column.
    _logger.info (_LOG_TAG + "Copying data from " + _tableName + "." + _oldColumnName
                                + " to " + _tableName + "." + _newColumnName);
    var builder = table.update();
    builder.set (newColumn, builder.getColumnRef(_oldColumnName));
    builder.execute();

    // Step 3:  Delete old column.
    _logger.info (_LOG_TAG + "Removing column " + _tableName + "." + _oldColumnName);
    oldColumn.drop();

    _logger.info (_LOG_TAG + "Completed " + Description);
  }
}