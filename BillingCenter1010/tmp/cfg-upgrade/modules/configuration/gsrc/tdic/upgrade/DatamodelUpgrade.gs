package tdic.upgrade

uses gw.plugin.upgrade.IDatamodelUpgrade
uses gw.api.datamodel.upgrade.IDatamodelChange
uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses gw.api.database.upgrade.after.AfterUpgradeVersionTrigger
uses java.util.ArrayList
uses gw.api.system.PLLoggerCategory
uses gw.api.database.upgrade.DatamodelChangeWithoutArchivedDocumentChange
uses java.util.List
uses org.slf4j.LoggerFactory

class DatamodelUpgrade implements IDatamodelUpgrade {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade");
  private static final var _LOG_TAG = "${DatamodelUpgrade.Type.RelativeName} - "

  override property get MajorVersion() : int {
    // BrianS - TDIC's major version is currently 0.  Not sure if this can be changed.
    return 0;
  }

  override property get BeforeUpgradeDatamodelChanges() :List<IDatamodelChange<BeforeUpgradeVersionTrigger>> {
    _logger.info (_LOG_TAG + "BeforeUpgradeDataModelChanges");
    var list = new ArrayList<IDatamodelChange<BeforeUpgradeVersionTrigger>>()

    // BrianS - BillingInstruction.Description was modified from a datatype of shorttext to mediumtext.
    //          BillingCenter fails to auto upgrade the column.
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new UpgradeColumnUsingTemp (33, "bc_billinginstruction", "Description")));
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new AddColumn(44, "bc_activitypattern", "AssignableQueue_Ext")))
    list.add(DatamodelChangeWithoutArchivedDocumentChange.make(new DeleteColumn(44, "bcx_glinttransfilter_tdic", "TransactionType")))
    return list
  }

  override property get AfterUpgradeDatamodelChanges() : List<IDatamodelChange<AfterUpgradeVersionTrigger>> {
    _logger.info (_LOG_TAG + "AfterUpgradeDataModelChanges");
    var list = new ArrayList<IDatamodelChange<AfterUpgradeVersionTrigger>>()

    return list
  }
}