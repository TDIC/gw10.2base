package tdic.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses gw.api.system.PLLoggerCategory
uses org.slf4j.LoggerFactory

// BrianS - Class that can be used to upgrade a column using a temporary column.  This upgrade script
//          will only work when data can be copied from a column with the old datatype to a column with
//          the new datatype.
class UpgradeColumnUsingTemp extends BeforeUpgradeVersionTrigger {
  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade");
  private static final var _LOG_TAG = "${UpgradeColumnUsingTemp.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber;
  private var _tableName : String as readonly TableName;
  private var _columnName : String as readonly ColumnName;

  construct(versionNumber : int, tableName : String, columnName : String) {
    super(versionNumber);
    _versionNumber = versionNumber;
    _tableName = tableName;
    _columnName = columnName;
  }

  override public property get Description() : String {
    return "Upgrade " + _tableName + "." + _columnName + " using temporary column";
  }

  // BrianS - Temporary column name will append "_TMP" to the end of the column name.  This might be an issue
  //          when the column name is long.
  public property get TmpColumnName() : String {
    return _columnName + "_TMP";
  }

  // BrianS - No version checks are needed for this upgrade.
  override function hasVersionCheck() : boolean {
    return false;
  }

  override function execute() : void {
    var table = getTable(_tableName);
    var column = table.getColumn(_columnName);
    var tmpColumnName = TmpColumnName;

    // Step 1:  Create temporary column.
    _logger.info (_LOG_TAG + "Creating temporary column: " + _tableName + "." + tmpColumnName);
    var tempColumn = table.createTempColumn (tmpColumnName, column.DataType);

    // Step 2:  Copy data to the temporary column.
    _logger.info (_LOG_TAG + "Copying data from " + _tableName + "." + _columnName
                                + " to " + _tableName + "." + tmpColumnName);
    var builder = table.update();
    builder.set (tempColumn, builder.getColumnRef(_columnName));
    builder.execute();

    // Step 3:  Delete data model column.
    _logger.info (_LOG_TAG + "Removing column " + _tableName + "." + _columnName);
    column.drop();

    // Step 4:  Create column.
    _logger.info (_LOG_TAG + "Creating column " + _tableName + "." + _columnName);
    column.create();

    // Step 5:  Copy data from the temporary column.
    _logger.info (_LOG_TAG + "Copying data from " + _tableName + "." + tmpColumnName
                               + " to " + _tableName + "." + _columnName);
    builder = table.update();
    builder.set (column, builder.getColumnRef(tmpColumnName));
    builder.execute();

    // Step 6:  Delete the temporary column.
    _logger.info (_LOG_TAG + "Removing column " + _tableName + "." + tmpColumnName);
    tempColumn.drop();

    _logger.info (_LOG_TAG + "Completed " + Description);
  }
}