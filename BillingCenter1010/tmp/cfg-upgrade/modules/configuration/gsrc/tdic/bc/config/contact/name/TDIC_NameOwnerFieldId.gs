package tdic.bc.config.contact.name

uses gw.api.name.NameOwnerFieldId

/**
 * US1129,
 * 01/21/2015 Vicente
 *
 * Extension to NameOwnerFieldId for additional available fields for person name at TDIC.
 */
class TDIC_NameOwnerFieldId extends NameOwnerFieldId {

  protected construct(aName : String) {
    super(aName)
  }

  public static final var CREDENTIAL : NameOwnerFieldId = new TDIC_NameOwnerFieldId("Credential_TDIC")
}