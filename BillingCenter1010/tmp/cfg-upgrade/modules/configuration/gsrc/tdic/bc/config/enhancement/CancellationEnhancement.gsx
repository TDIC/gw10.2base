package tdic.bc.config.enhancement

enhancement CancellationEnhancement : entity.Cancellation {

  public property get IsDelinquencyCancel() : boolean {
    return this.CancellationReason == "Not Taken - Non-Payment of Premium" or this.CancellationReason == "Non-payment";
  }
}
