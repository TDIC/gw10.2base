package tdic.bc.integ.plugins.bankeft.dto.BankPrenote

uses java.util.ArrayList
uses java.math.BigInteger
uses java.lang.Math
uses java.util.List

/**
 * A Gosu class to contain the fields needed for the GX model when generating the flat file.
 */
class TDIC_BankEFTPrenoteFile {

  /**
   * The List of payment instrument line items to write to the flat file.
   */
  var _paymentInstruments : List<TDIC_PaymentInstrumentWritableBean> as PaymentInstruments

  /**
   * Adds to the list of payment instruments.
   */
  @Param("bean", "The PaymentInstrumentWritableBean to add to the list of payment instruments")
  public function addToPaymentInstruments(bean : TDIC_PaymentInstrumentWritableBean) {
    if (_paymentInstruments == null) {
      _paymentInstruments = new ArrayList<TDIC_PaymentInstrumentWritableBean>()
    }
    _paymentInstruments.add(bean)
  }


  /**
   * Gets the total number payment instruments to be written to the flat file.
   */
  @Returns("An int for the total number payment instruments")
  property get TotalNumPaymentInstruments() : int {
    return _paymentInstruments?.Count
  }

  /**
   * Gets the total number of blocks needed to write the flat file, based on the total number of lines, which amounts
   * to the total number payment instruments plus 2 header lines and 2 trailer lines.  One block is needed per 10 lines,
   * and any additional portion of 10 lines needs a block as well.
   */
  @Returns("An int for the total number of blocks needed to write the flat file")
  property get TotalNumBlocks() : int {
    if (_paymentInstruments == null) {
      // Need at least one block for the header lines
      return 1
    }
    // Add 4 to the total number of payment instruments, for the 2 header lines and the 2 trailer lines
    return Math.ceil((TotalNumPaymentInstruments+4) / 10.0) as int
  }

  /**
   * Gets the total payment amount of all payment instruments to be written to the flat file.  Note that all payment
   * amounts in the flat file are already multiplied by 100 to avoid any decimals.
   */
  @Returns("A BigInteger for the total payment amount of all payment instruments")
  property get TotalAmount() : BigInteger {
      return BigInteger.ZERO
  }

  /**
   * Gets the sum of all routing number values.  Note that the routing number value does not include the 9th digit,
   * which is the check digit.  This is used to calculate the entry hash in the trailer lines of the flat file.
   *
   */
  @Returns("A BigInteger for the sum of all routing number values")
  property get SumOfRoutingNumbers() : BigInteger {
    var sum = BigInteger.ZERO
    if (_paymentInstruments == null) {
      return sum
    }
    for (bean in _paymentInstruments) {
      var routingWithoutCheckDigit = new BigInteger(bean.BankRoutingNumber.substring(0, 8))
      sum = sum.add(routingWithoutCheckDigit)
    }
    return sum
  }
}