package tdic.bc.common.batch.invoices

uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses gw.api.database.Query
uses gw.api.system.PLLoggerCategory
uses java.lang.Exception
uses org.slf4j.LoggerFactory
uses tdic.bc.config.invoice.InvoiceUtil

class ResendInvoicesBatchProcess extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${ResendInvoicesBatchProcess.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_RESENDINVOICES_TDIC);
  }

  /*
    Used to determine if conditions are met to begin processing
      Return true: doWork() will be called
      Return false: only a history event will be written
      Avoids lengthy processing in doWork() if there is nothing to do
   */
  override function checkInitialConditions() : boolean {
    return true;
  }

  override function doWork() {
    var invoice : Invoice;

    // Get all the invoices from the resend table.
    _logger.info (_LOG_TAG + "Querying for ResendInvoice_TDIC entities.");
    var query = Query.make(ResendInvoice_TDIC);
    var results = query.select();
    _logger.debug (_LOG_TAG + "Found " + results.Count + " records.");

    OperationsExpected = results.Count;

    for (resendInvoice in results) {
      if (this.TerminateRequested) {
        break;
      }

      Transaction.runWithNewBundle (\bundle -> {
        resendInvoice = bundle.add(resendInvoice);
        invoice = resendInvoice.Invoice;
        if (shouldResendInvoice(invoice)) {
          // Raise event to produce document
          invoice.addEvent ("InvoiceResent");
        }
        resendInvoice.remove();
      }, "iu");
      incrementOperationsCompleted();
    }
  }

  protected function shouldResendInvoice (invoice : Invoice) : boolean {
    var retval = invoice.AmountDue > InvoiceUtil.InvoiceThreshold;

    if (retval == false) {
      _logger.trace (_LOG_TAG + "Invoice " + invoice.InvoiceNumber + " will NOT be resent because " + invoice.AmountDue
                        + " amount due is below the " + InvoiceUtil.InvoiceThreshold + " invoicing threshold.");
    }

    return retval;
  }

  /*
  Called by Guidewire to request that process self-terminate
    Return true if request will be honored
    Return false if request will not be honored
  Handled by setting monitor flag in code and exiting any loops
  */
  override function requestTermination() : boolean {
    return true;
  }
}