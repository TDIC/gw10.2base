package tdic.bc.config.enhancement
/**
 * US718
 * 11/19/2014 Vicente
 *
 * FundsTracker Enhancement for TDIC.
 */
enhancement FundsTrackerEnhancement : entity.FundsTracker {
  /**
   * Returns the reversal reason if this FundsTracker was reversal or a empty string if not
   */
  @Returns("The reversal reason if this FundsTracker was reversal or a empty string if not")
  property get PaymentReversalReason() : String {
    return this.Trackable typeis DirectBillMoneyRcvd ? this.Trackable.ReversalReason.DisplayName : null
  }
}
