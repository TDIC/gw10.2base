package tdic.bc.common.batch.returncreditallocaton

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount
uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicemodel.anonymous.elements.Invoice_Amount
uses typekey.BillingInstruction

class RetunCreditAllocationToAccountDefault_TDIC extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${RetunCreditAllocationToAccountDefault_TDIC.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_RETURNCREDITALLOCATIONTOACCOUNTDEFAULT_TDIC);
  }
    /*  Batch Process to allocate the funds to Account default unapplied. We are doing this to avoid the credit charges allocation to the
    future terms. This is implemented for Audit credit transactions.
* */
  override function checkInitialConditions() : boolean {
    return true;
  }

  override function requestTermination() : boolean {
    return true;
  }

  protected override function doWork() {
  // Identify the invoices which are having audit credit charges.


    //var chargeQuery = Query.make(Charge)
  var invoices = Query.make(Invoice).compare(Invoice#Status, Equals,InvoiceStatus.TC_PLANNED).compare(Invoice#AmountDue, LessThan,0bd.ofDefaultCurrency())
  var invoiceResults = invoices.select()
  var auditInvoices = invoiceResults.where(\elt -> elt.InvoiceItems.allMatch(\elt1 -> elt1.Charge.BillingInstruction.Subtype == BillingInstruction.TC_AUDIT))

    _logger.info("Planned Invoices Count With Negative AmountDue and Audit Charge:  " + auditInvoices.Count)
    auditInvoices.each(\invoice -> {
    var policyPeriod : PolicyPeriod
      var policyBalance = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
      // Validating policy Level Billing or not.
      if (invoice.InvoiceStream.PolicyLevelStream) {
        policyPeriod = invoice.InvoiceItems.first().PolicyPeriod
        policyBalance = invoice.InvoiceItems.first().PolicyPeriod.AmountBilledOrDue_TDIC
        _logger.info("Policy Level Stream - policyPeriod " + policyPeriod + " policyBalance: " + policyBalance)
      } else {
        policyPeriod = invoice.InvoiceStream.PolicyPeriod_TDIC
        policyBalance = invoice.InvoiceItems.first().PolicyPeriod.AmountBilledOrDue_TDIC
        _logger.info("PolicyPeriod " + policyPeriod + " policyBalance: " + policyBalance)
      }
      _logger.info("Invoice AmountDue : " + invoice.AmountDue)
      // Creating Funds Transfer for Positive Balances of specific Invoice Stream.
      var disbAmount = (invoice.AmountDue.negate() - policyBalance)
     var disbursementExists = new gw.api.web.disbursement.DisbursementUtil().getAccountAndCollateralDisbursements(policyPeriod.Account)
                                .where(\disb -> (disb as AccountDisbursement).UnappliedFund.DefaultForAccount and
                                disb.Amount == disbAmount and disb.CreateTime.trimToMidnight().afterOrEqual(invoice.CreateTime.trimToMidnight()))
     if(disbursementExists.HasElements)
      _logger.info("Disbursement Already Exists for this Invoice " + invoice.InvoiceNumber + " Skipping Funds transfer and Disbursement Creation")

      if (!disbursementExists.HasElements and disbAmount.IsPositive) {
        _logger.info("Invoice " + invoice.InvoiceNumber + "is eligible for funds transfer of Amount : " + disbAmount)
        gw.transaction.Transaction.runWithNewBundle(\elt -> {
          var fundsTransferUtil = new gw.api.web.transaction.FundsTransferUtil(); 
          var target = new FundsTransfer(policyPeriod.Account.Currency)
          policyPeriod = elt.add(policyPeriod)
          target = elt.add(target)
          var defaultUnapplied = policyPeriod.Account.DefaultUnappliedFund
          target.TargetAccount = policyPeriod.Account
          target.setTargetUnapplied(defaultUnapplied)
          target.Amount = invoice.AmountDue.negate() - policyBalance
          fundsTransferUtil.addToTransfers(target)
          fundsTransferUtil.SourceOwner = policyPeriod.Account;
          fundsTransferUtil.TargetType = TAccountOwnerType.TC_ACCOUNT
          // Set the Source Unapplied Funds seperately for Policy Level and Policy Period Level.
          if (invoice.InvoiceStream.PolicyLevelStream) {
            fundsTransferUtil.SourceUnappliedFunds = policyPeriod.Account.UnappliedFunds.firstWhere(\elt1 -> elt1.Policy == policyPeriod.Policy)
          } else {
            fundsTransferUtil.SourceUnappliedFunds = policyPeriod.Account.UnappliedFunds.firstWhere(\elt1 -> elt1.PolicyPeriod_TDIC == invoice.InvoiceItems.first().PolicyPeriod)
      }
      fundsTransferUtil.setSources()
      fundsTransferUtil.createTransfers()
          _logger.info("Funds transfer complete and start Create Disbursement")
      var disb = new AccountDisbursement(policyPeriod.Account.Currency)
      createDisbursementAtAccount(disb, invoice)
       _logger.info("disbursement Created : " + disb?.DisbursementNumber)
   }, "su")
   }
  })

  }
  function createDisbursementAtAccount(disb : AccountDisbursement, invoice : Invoice) {
    var policyPeriod = invoice.InvoiceItems.first().PolicyPeriod
    var policyBalance = invoice.InvoiceItems.first().PolicyPeriod.AmountBilledOrDue_TDIC
    disb.setUnappliedFundsAndFields(policyPeriod.Account.DefaultUnappliedFund)
    disb.Amount = invoice.AmountDue.negate() - policyBalance
    disb.DueDate = gw.api.util.DateUtil.currentDate()
    disb.Status = DisbursementStatus.TC_AWAITINGAPPROVAL
    disb.Reason = Reason.TC_AUTOMATIC
    disb.Address = policyPeriod.Account.PrimaryPayer.Contact.PrimaryAddress.toString()
    disb.MailTo = policyPeriod.Account.PrimaryPayer.Contact.toString()
    disb.PayTo = policyPeriod.Account.PrimaryPayer.Contact.toString()
    disb.PaymentInstrument = policyPeriod.Account.DefaultPaymentInstrument
    var activity = disb.getOpenApprovalActivity()
    //Creating Disbursement Approval acitivty.
    if (activity == null) {
      disb.getApprovalHandler()?.approvalRequired(null)
      activity = disb.getOpenApprovalActivity()
    }
    //Assigning activity to designated group and queue.
    var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Approve Disbursements")
    if (activity?.assignGroup(groupName)) {
      var queueName = activity.AssignedGroup.getQueue("Approve Disbursements")
      if (queueName != null) {
        activity.assignActivityToQueue(queueName, groupName)
      }
    }
  }


  }