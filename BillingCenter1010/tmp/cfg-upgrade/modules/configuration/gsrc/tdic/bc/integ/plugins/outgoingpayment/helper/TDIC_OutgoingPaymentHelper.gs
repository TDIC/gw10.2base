package tdic.bc.integ.plugins.outgoingpayment.helper

uses com.tdic.util.delimitedfile.DelimitedFileImpl
uses gw.api.gx.GXOptions
uses gw.xml.XmlElement
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentFile
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentWritableBean

uses java.text.SimpleDateFormat

/**
 * US570 - Lawson Outgoing Payments Integration
 * 11/24/2014 Alvin Lee
 *
 * Class with helper methods related to Lawson integration for outgoing payments
 */
class TDIC_OutgoingPaymentHelper {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("OUTGOING_PAYMENT")

  /**
   * A String containing the Event Name for OutgoingPaymentAdded.
   */
  public static final var OUTGOING_PAYMENT_ADDED : String = "OutgoingPaymentAdded"

  /**
   * Helper function to map an outgoing payment entity to the GX model for the writable bean so it can be saved to the database.
   */
  @Param("disbPayment", "The OutgoingDisbPmnt entity to map to the GX model")
  @Returns("The GX Model representation of the outgoing payment writable bean")
  public static function mapOutgoingPaymentToWritableBeanModel(disbPayment : OutgoingDisbPmnt) : tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean {
    _logger.debug("TDIC_OutgoingPaymentHelper#mapOutgoingPaymentToWritableBeanModel - Entering")

    // Build XML Model options object
    var options = new GXOptions()
    options.Incremental = false // Send all values, not just changed ones
    options.Verbose = true // Send all fields, even nulls

    var sdfTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    // Build outgoing payment writable bean object
    var gxOutgoingPayment = new OutgoingPaymentWritableBean()
    gxOutgoingPayment.PublicID = disbPayment.PublicID

    // Populate fields for AccountDisbursement (all disbursements are Account Disbursements for Release A)
    if (disbPayment.Disbursement typeis AccountDisbursement) {
      _logger.debug("TDIC_OutgoingPaymentHelper#mapOutgoingPaymentToWritableBeanModel - AccountDisbursement to be written to integration database")
      var disbAccount = disbPayment.Disbursement.Account
      gxOutgoingPayment.AccountName = disbAccount.AccountName
      gxOutgoingPayment.AccountNumber = disbAccount.AccountNumber

      // Check for disbursement being connected to a policy unapplied fund
      var unapplied = disbPayment.Disbursement.UnappliedFund
      if (unapplied.Policy != null) {
        if(unapplied.Policy.LatestPolicyPeriod.ERE_TDIC){
          gxOutgoingPayment.LOB = LOBAccountCode_TDIC.TC_PLCLAIMSMADEERE_TDIC.Code
        }else {
          gxOutgoingPayment.LOB = unapplied.Policy.LatestPolicyPeriod.Offering_TDIC.Code
        }
        gxOutgoingPayment.PolicyNumber = unapplied.Policy.LatestPolicyPeriod.PolicyNumber
        gxOutgoingPayment.StateCode = unapplied.Policy.LatestPolicyPeriod.RiskJurisdiction.Code
      }
    }
    else if (disbPayment.Disbursement typeis SuspenseDisbursement) {
      _logger.debug("TDIC_OutgoingPaymentHelper#mapOutgoingPaymentToWritableBeanModel - SuspenseDisbursement to be written to integration database")
      // Note:  Will need to populate address fields if Suspense Disbursements are to be used in the future.
    }
    gxOutgoingPayment.Amount = disbPayment.Amount
    gxOutgoingPayment.DueDate = sdfTimestamp.format(disbPayment.Disbursement.DueDate)

    return new tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean(gxOutgoingPayment, options)
  }

  /**
   * Converts data to the lines that can be written to the destination flat file.
   */
  @Param("vendorSpecName", "A String containing the name of the vendor spec spreadsheet to use when encoding the file")
  @Param("flatFileData", "The object containing the data to write to the destination flat file")
  @Returns("A List<String> containing the liens to be written to the destination flat file")
  @Throws(Exception, "If there are any unexpected errors encoding the data")
  public static function convertDataToFlatFileLines(vendorSpecName : String, flatFileData : OutgoingPaymentFile) : List<String> {
    var gxModel = new tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentfilemodel.OutgoingPaymentFile(flatFileData)
    _logger.debug("TDIC_OutgoingPaymentHelper#convertDataToFlatFileLines - Outgoing Payment Invoice GX Model Generated: ")
    _logger.debug(gxModel.asUTFString())
    var xml = XmlElement.parse(gxModel.asUTFString())
    var df = new DelimitedFileImpl()
    return df.encode(vendorSpecName, xml)
  }

}