package tdic.bc.common.batch.plan

uses gw.api.database.Query
uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory

class ChangeDefaultReturnPremiumPlan extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${ChangeDefaultReturnPremiumPlan.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_UPDATERETURNPREMIUMPLAN_TDIC);
  }

  override function checkInitialConditions() : boolean {
    return true;
  }

  override function requestTermination() : boolean {
    return true;
  }

  protected override function doWork() {

    // find the plan to be updated
    var wcRetPremPlan = Query.make(ReturnPremiumPlan).compare(ReturnPremiumPlan#Name, Equals, "TDIC Return Premium Plan (WC)").select().FirstResult
    // Get all the policy periods
    var query = Query.make(PolicyPeriod)
    var results = query.select()
    _logger.info(_LOG_TAG + "Found " + results.Count + " policy periods.")

    for (policyPeriod in results) {
      if (this.TerminateRequested) {
        break;
      }
      // update the return premium plan for the policyperiods.
      if (policyPeriod.ReturnPremiumPlan != null){
        changeReturnPremiumPlan(policyPeriod, wcRetPremPlan);
        incrementOperationsCompleted();
      }
    }
  }

  protected function changeReturnPremiumPlan(policyPeriod : PolicyPeriod, retPremPlan : ReturnPremiumPlan) : void {
    _logger.info(_LOG_TAG + policyPeriod.PolicyNumberLong
        + " - Changing return premium plan from Default to WC")
    Transaction.runWithNewBundle(\bundle -> {
      policyPeriod = bundle.add(policyPeriod);
      var returnPremiumPlan = bundle.add(retPremPlan)
      policyPeriod.ReturnPremiumPlan = returnPremiumPlan
    }, "iu");
  }
}