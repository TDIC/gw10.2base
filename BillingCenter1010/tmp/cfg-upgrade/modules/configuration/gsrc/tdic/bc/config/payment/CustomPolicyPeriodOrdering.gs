package tdic.bc.config.payment

uses gw.payment.DirectInvoiceItemAllocationOrdering

/**
 * Order invoice items based on the order of the policy term number.
 */

class CustomPolicyPeriodOrdering extends DirectInvoiceItemAllocationOrdering {

  override property get TypeKey() : InvoiceItemOrderingType {
    return TC_POLICYPERIOD_TDIC
  }

  override function compare(invoiceItem1 : InvoiceItem, invoiceItem2 : InvoiceItem) : int {
    var policyTerm1 = invoiceItem1.PolicyPeriod.TermNumber
    var policyTerm2 = invoiceItem2.PolicyPeriod.TermNumber
    if (policyTerm1 == null) {
      return - 1
    } else if (policyTerm2 == null) {
      return 1
    } else {
      return policyTerm1.compareTo(policyTerm2)
    }
  }
}