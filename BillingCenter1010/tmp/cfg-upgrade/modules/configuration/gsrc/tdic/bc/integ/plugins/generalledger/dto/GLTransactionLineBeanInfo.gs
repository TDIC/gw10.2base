package tdic.bc.integ.plugins.generalledger.dto

uses com.tdic.util.database.IWritableBeanInfo

uses java.util.Map
uses java.sql.Connection
uses java.sql.PreparedStatement

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Concrete implementation of Metadata class describing the GL transaction persistence options we want
 */
class GLTransactionLineBeanInfo<T> extends IWritableBeanInfo<T> {
  
  // Map to bind fields we want exported to the SQL database, fields not listed here will not be exported
  public static var _fields:Map<String, Map<String, Object>> = {
    "AccountName" -> {FIELD_SQL_NAME -> "AccountName",
        FIELD_BINDER -> stringFieldBinder
    },
    "AccountNumber" -> {FIELD_SQL_NAME -> "AccountNumber",
        FIELD_BINDER -> stringFieldBinder
    },
    "Amount" -> {FIELD_SQL_NAME -> "Amount",
        FIELD_BINDER -> bigDecimalFieldBinder
    },
    "ChargeName" -> {FIELD_SQL_NAME -> "ChargeName",
        FIELD_BINDER -> stringFieldBinder
    },
    "CreatedDate" -> {FIELD_SQL_NAME -> "CreatedDate",
        FIELD_BINDER -> stringFieldBinder
    },
    "CreatedUser" -> {FIELD_SQL_NAME -> "CreatedUser",
        FIELD_BINDER -> stringFieldBinder
    },
    "CreditCardType" -> {FIELD_SQL_NAME -> "CreditCardType",
        FIELD_BINDER -> stringFieldBinder
    },
    "Description" -> {FIELD_SQL_NAME -> "Description",
        FIELD_BINDER -> stringFieldBinder
    },
    "DirectBillMoneyRcvdPublicID" -> {FIELD_SQL_NAME -> "DirectBillMoneyRcvdPublicID",
        FIELD_BINDER -> stringFieldBinder
    },
    "GWTAccountName" -> {FIELD_SQL_NAME -> "GWTAccountName",
        FIELD_BINDER -> stringFieldBinder
    },
    "GWTransactionName" -> {FIELD_SQL_NAME -> "GWTransactionName",
        FIELD_BINDER -> stringFieldBinder
    },
    "InvoiceStatus"-> {FIELD_SQL_NAME -> "InvoiceStatus",
        FIELD_BINDER -> stringFieldBinder
    },
    "LineItemType" -> {FIELD_SQL_NAME -> "LineItemType",
        FIELD_BINDER -> stringFieldBinder
    },
    "LOB" -> {FIELD_SQL_NAME -> "LOB",
        FIELD_BINDER -> stringFieldBinder
    },
    "MappedTAccountName" -> {FIELD_SQL_NAME -> "MappedTAccountName",
        FIELD_BINDER -> stringFieldBinder
    },
    "MappedTransactionName" -> {FIELD_SQL_NAME -> "MappedTransactionName",
        FIELD_BINDER -> stringFieldBinder
    },
    "PaymentMethod"-> {FIELD_SQL_NAME -> "PaymentMethod",
        FIELD_BINDER -> stringFieldBinder
    },
    "PaymentReversalReason"-> {FIELD_SQL_NAME -> "PaymentReversalReason",
        FIELD_BINDER -> stringFieldBinder
    },
    "PolicyEffectiveDate" -> {FIELD_SQL_NAME -> "PolicyEffectiveDate",
        FIELD_BINDER -> stringFieldBinder
    },
    "PolicyNumber" -> {FIELD_SQL_NAME -> "PolicyNumber",
        FIELD_BINDER -> stringFieldBinder
    },
    "PolicyPeriodStatus" -> {FIELD_SQL_NAME -> "PolicyPeriodStatus",
        FIELD_BINDER -> stringFieldBinder
    },
    "StateCode" -> {FIELD_SQL_NAME -> "StateCode",
        FIELD_BINDER -> stringFieldBinder
    },
    "TermNumber" -> {FIELD_SQL_NAME -> "TermNumber",
        FIELD_BINDER -> integerFieldBinder
    },
    "TransactionDate" -> {FIELD_SQL_NAME -> "TransactionDate",
        FIELD_BINDER -> stringFieldBinder
    },
    "TransactionNumber" -> {FIELD_SQL_NAME -> "TransactionNumber",
        FIELD_BINDER -> stringFieldBinder
    },
    "TransactionSubtype" -> {FIELD_SQL_NAME -> "TransactionSubtype",
        FIELD_BINDER -> stringFieldBinder
    },
    "UWCompany"-> {FIELD_SQL_NAME -> "UWCompany",
        FIELD_BINDER -> stringFieldBinder
    }
  }
  
  /**
   * Default constructor with no parameters.
   */
  construct() {
  }
  
  /**
   * Abstract method implementation, provides the SQL name of the table for this object
   */
  override property get TableName() : String {
    return "GLIntegration"
  }

  /**
   * Abstract method implementation, provides the Gosu name of the object's Public ID property for use with reflection calls
   */  
  override property get IDColumnName() : String {
    return "PublicID"
  }

  /**
   * Abstract method implementation, provides the SQL name of the Public ID property in the database table
   */  
  override property get IDColumnSQLName() : String {
    return "TransactionLinePublicID"
  }

  /**
   * Abstract method implementation, provides the SQL name of the Processed field in the database table
   */
  override property get ProcessedColumnSQLName(): String {
    return "Processed"
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will insert a new object when it is executed
   * 
   * @param conn - database connection
   * @param entity - Entity that is to be inserted into the database
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function createAndBindCreateSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindCreateSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to paramaterize a call in order to create, bind, and return a JDBC
   * prepared statement that will retrieve an object when it is executed
   * 
   * @param conn - database connection
   * @param entity - Entity with populated primary key to be used to retrieve values from the database
   * @return PreparedStatement that will return values based on the provided entity object
   */
  override function createAndBindRetrieveSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindRetrieveSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will update an object when it is executed
   * 
   * @param conn - database connection
   * @param entity - Entity that is to be updated in the database
   * @return PreparedStatement that will update values based on the provided entity object
   */
  override function createAndBindUpdateSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindUpdateSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to populate the fields of an entity from a GX model representation.
   */
  @Param("entity", "The object to populate")
  @Param("model", "The GX model containing the data to populate into the entity")
  override function loadFromModel(entity : Object, model : Object) {
    loadFromModel(entity, model, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will update an object when it is executed
   * 
   * @param conn - database connection
   * @param entity - Entity that is to be updated in the database
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function markProcessed(conn : Connection, entity : T) : PreparedStatement {
    return markProcessed(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will retrieve all objects of our entity type that have not yet been written to an export file
   * 
   * @param conn - database connection
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function createRetrieveAllSQL(conn : Connection) : PreparedStatement {
    return createRetrieveAllSQL(conn, _fields)
  }

}