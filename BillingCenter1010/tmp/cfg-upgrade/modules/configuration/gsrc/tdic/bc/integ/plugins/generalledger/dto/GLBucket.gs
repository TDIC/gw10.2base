package tdic.bc.integ.plugins.generalledger.dto

uses java.math.BigDecimal
uses java.util.Date
uses java.text.SimpleDateFormat

/**
 * US570 - General Ledger Integration
 * 1/14/2015 Alvin Lee
 *
 * A class representing a single bucket for GL.
 */
class GLBucket {

  /**
   * Description for this bucket, used in the flat file.
   */
  private var _description : String as Description

  /**
   * The T-Account code mapped to this bucket.
   */
  private var _mappedTAccountCode : String as MappedTAccountCode

  /**
   * The total amount in this bucket.
   */
  private var _totalBucketAmount : BigDecimal as TotalBucketAmount

  /**
   * The identifier for the bucket, which is a sequential number for the flat file.
   */
  private var _uid : int as UID

  /**
   * The account unit for the bucket.
   */
  private var _accountUnit : String as AccountUnit

  /**
   * The date for the transactions in this bucket.
   */
  private var _date : Date as Date

  construct() {
    // Start the total amount at $0
    _totalBucketAmount = BigDecimal.ZERO
  }

  property get FullDate() : String {
    var sdf = new SimpleDateFormat("MM/dd/yyyy")
    return sdf.format(_date)
  }

  property get ShortDate() : String {
    var sdf = new SimpleDateFormat("MMddyy")
    return sdf.format(_date)
  }

  property get MonthDay() : String {
    var sdf = new SimpleDateFormat("MMdd")
    return sdf.format(_date)
  }


}