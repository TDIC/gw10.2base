package tdic.bc.config.unappliedfund

uses gw.api.domain.invoice.InvoiceStreamFactory
uses java.lang.UnsupportedOperationException
uses gw.api.web.invoice.InvoiceStreamView;
uses java.util.Date
uses org.slf4j.LoggerFactory

class UnappliedFundUtil {
  static var _logger = LoggerFactory.getLogger("Application.Invoice");

  // Mapping from periodicity to a periodicity that is allowed on an invoice stream.
  private static var _periodicityMap = {
      Periodicity.TC_EVERYWEEK       -> Periodicity.TC_EVERYWEEK,
      Periodicity.TC_EVERYOTHERWEEK  -> Periodicity.TC_EVERYOTHERWEEK,
      Periodicity.TC_TWICEPERMONTH   -> Periodicity.TC_TWICEPERMONTH,
      Periodicity.TC_MONTHLY         -> Periodicity.TC_MONTHLY,
      Periodicity.TC_EVERYOTHERMONTH -> Periodicity.TC_MONTHLY,
      Periodicity.TC_QUARTERLY       -> Periodicity.TC_MONTHLY,
      Periodicity.TC_EVERYFOURMONTHS -> Periodicity.TC_MONTHLY,
      Periodicity.TC_EVERYSIXMONTHS  -> Periodicity.TC_MONTHLY,
      Periodicity.TC_EVERYYEAR       -> Periodicity.TC_MONTHLY,
      Periodicity.TC_EVERYOTHERYEAR  -> Periodicity.TC_MONTHLY
  };

  /**
   * Create a term level unapplied fund (and invoice stream).
  */
  public static function createTermInvoiceStream (account : Account, policyPeriod : PolicyPeriod,
                                                  paymentPlan : PaymentPlan) : InvoiceStream {
    var unappliedFundDescription = policyPeriod.PolicyNumber + "-" + policyPeriod.TermNumber;
    _logger.info ("UnappliedFundUtil.createTermInvoiceStream - Creating invoice stream and unapplied fund for "
                      + unappliedFundDescription);

    // Add account to bundle
    var bundle = policyPeriod.Bundle;
    account = bundle.add(account);

    var unappliedFund = account.createDesignatedUnappliedFund(unappliedFundDescription);

    var periodicity = getInvoiceStreamPeriodicity(paymentPlan.Periodicity);
    var invoiceStream = InvoiceStreamFactory.createInvoiceStreamFor(account, periodicity);

    // BrianS - Set override for invoice day of month.
    var invoiceStreamView = new InvoiceStreamView (invoiceStream);
    invoiceStreamView.UnappliedFund = unappliedFund;
    invoiceStreamView.OverrideAnchorDates = true;
    // TODO: uncomment when PC is up.
    if (periodicity != null) /* == Periodicity.TC_MONTHLY)*/ {
      var invoiceDayOfMonth = policyPeriod.Policy.InvoiceDayOfMonth_TDIC;
      if (invoiceDayOfMonth == 0) {
        invoiceDayOfMonth = Date.CurrentDate.DayOfMonth;
      }
      invoiceStreamView.OverridingAnchorDateViews[0].DayOfMonth = invoiceDayOfMonth;
    } else {
      throw new UnsupportedOperationException ("Invoice stream overrides not defined for " + periodicity);
    }

    return invoiceStream;
  }

  /**
   * Get periodicity for term level invoice stream.
  */
  protected static function getInvoiceStreamPeriodicity (periodicity : Periodicity) : Periodicity {
    var retval = _periodicityMap.get(periodicity);

    if (retval == null) {
      throw new UnsupportedOperationException ("Invoice stream periodicity not defined for " + periodicity);
    }

    return retval;
  }
}