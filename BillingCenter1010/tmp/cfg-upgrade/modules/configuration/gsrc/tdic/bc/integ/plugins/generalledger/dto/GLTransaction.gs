package tdic.bc.integ.plugins.generalledger.dto

uses java.util.ArrayList
uses java.util.List

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Class holding the line items of a BillingCenter GL transaction.
 */
class GLTransaction {

  /**
   * The line items in a BillingCenter GL transaction.
   */
  private var _lineItems : List<GLTransactionLineWritableBean> as LineItems

  /**
   * Adds a line item to the GL transaction.
   */
  public function addToLineItems(lineItem : GLTransactionLineWritableBean) {
    if (_lineItems == null) {
      _lineItems = new ArrayList<GLTransactionLineWritableBean>()
    }
    _lineItems.add(lineItem)
  }

}