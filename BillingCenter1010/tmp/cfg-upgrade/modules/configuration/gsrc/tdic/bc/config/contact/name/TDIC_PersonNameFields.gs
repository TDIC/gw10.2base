package tdic.bc.config.contact.name

uses gw.api.name.PersonNameFields

/**
 * US1129,
 * 01/21/2015 Vicente
 *
 * Additional name fields for Persons at TDIC.
 */
interface TDIC_PersonNameFields extends PersonNameFields {

  property get Credential_TDIC() : Credential_TDIC
  property set Credential_TDIC(value : Credential_TDIC)
}