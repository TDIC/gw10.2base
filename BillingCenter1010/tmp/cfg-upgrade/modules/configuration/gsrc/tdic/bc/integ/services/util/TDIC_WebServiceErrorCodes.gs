package tdic.bc.integ.services.util

class TDIC_WebServiceErrorCodes {

  //Check Status API Error Codes
  public static final var CHECK_STATUS_UPDATE_PROCESS_FAILED : int = 10001
  public static final var CHECK_TRANSACTION_RECORD_ID_EMPTY : int = 10002
  public static final var OUTGIONG_PAYMENT_RECORD_NOT_FOUND : int = 10003
  public static final var INVALID_CHECK_STATUS_VALUE : int = 10004
  public static final var CHECK_STATUS_UPDATE_FAILED : int = 10005


}