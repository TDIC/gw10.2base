package tdic.bc.common.batch.paymentplan

uses gw.processes.BatchProcessBase
uses gw.api.system.PLLoggerCategory
uses gw.api.database.Query
uses java.util.Date
uses gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange
uses gw.api.domain.invoice.PaymentPlanChanger
uses gw.transaction.Transaction
uses gw.invoice.InvoiceItemFilter
uses org.slf4j.LoggerFactory

class ChangeQuarterlyPaymentPlan extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${ChangeQuarterlyPaymentPlan.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_CHANGEQUARTERLYV2TOV3_TDIC);
  }

  /*
    Used to determine if conditions are met to begin processing
      Return true: doWork() will be called
      Return false: only a history event will be written
      Avoids lengthy processing in doWork() if there is nothing to do
   */
  override function checkInitialConditions() : boolean {
    return true;
  }

  override function doWork() {
    var invoice : Invoice;

    // Get all the policy periods with the Quarterly v2 payment plan.
    _logger.debug (_LOG_TAG + "Querying for Quarterly v2 policy periods.");
    var query = Query.make(PolicyPeriod)
                     .compare(PolicyPeriod#PaymentPlan, Equals, PaymentPlan.QuarterlyV2PaymentPlan);
    var results = query.select();
    _logger.debug (_LOG_TAG + "Found " + results.Count + " policy periods.");

    // BrianS - Operations Expected not set because we don't know how many will be able to be processed.
    //OperationsExpected = results.Count;

    for (policyPeriod in results) {
      if (this.TerminateRequested) {
        break;
      }

      if (shouldPaymentPlanChange(policyPeriod)) {
        changePaymentPlan(policyPeriod);
        incrementOperationsCompleted();
      }
    }
  }

  protected function shouldPaymentPlanChange (policyPeriod : PolicyPeriod) : boolean {
    var lastEvent : DelinquencyProcessEvent;
    var now = Date.Now;
    var retval = true;

    var delinquencies = policyPeriod.Policy.ActiveDelinquencyProcesses_TDIC;
    if (delinquencies.HasElements) {
      _logger.trace (_LOG_TAG + policyPeriod.PolicyNumberLong
                        + " - Payment plan will not be changed to Quarterly v3 because there are open delinquencies.");
      retval = false;

      for (delinquency in delinquencies) {
        lastEvent = delinquency.PreviousEvent;
        // Log a warning if delinquency hasn't had any updates in the last 2 weeks.
        if (lastEvent.CompletionTime < now.addDays(-14)) {
          _logger.warn (_LOG_TAG + policyPeriod.PolicyNumberLong + " - Delinquency " + delinquency
                          + " has a status of " + delinquency.Status.DisplayName
                          + " and hasn't had any activity since " + lastEvent.CompletionTime.formatDate(SHORT));
        }
      }
    }

    var amountDue = policyPeriod.Invoices.where(\i -> i.BilledOrDue).sum(\i -> i.AmountDue);
    var delinquencyThreshold = policyPeriod.PolicyPeriodDelinquencyPlan.PolEnterDelinquencyThreshold;
    if (amountDue > delinquencyThreshold) {
      _logger.trace (_LOG_TAG + policyPeriod.PolicyNumberLong
                        + " - Payment plan will not be changed to Quarterly v3 because amount due on billed or due invoices is "
                        + amountDue + ", which is above the delinqunecy threshold of " + delinquencyThreshold + ".");
      retval = false;
    }

    amountDue = policyPeriod.Invoices.sum(\i -> i.AmountDue);
    if (amountDue.IsNotZero and policyPeriod.Charges*.BillingInstruction.hasMatch(\bi -> bi typeis PolicyChange)) {
      _logger.trace (_LOG_TAG + policyPeriod.PolicyNumberLong
                        + " - Payment plan will not be changed to Quarterly v3 because there is a policy change charge and there is still a balance due of "
                        + amountDue + " on the policy.");
      retval = false;
    }

    if (policyPeriod.Canceled) {
      _logger.trace (_LOG_TAG + policyPeriod.PolicyNumberLong
                        + " - Payment plan will not be changed to Quarterly v3 because policy is canceled..");
      retval = false;
    }

    return retval;
  }

  protected function changePaymentPlan (policyPeriod : PolicyPeriod) : void {
    _logger.info (_LOG_TAG + policyPeriod.PolicyNumberLong
                    + " - Changing payment plan from Quarterly v2 to Quarterly v3.");

    // Logic from OOTB ChangePaymentPlanPopup.pcf

    // Redistribute Payments - Method for handling existing payments.
    //    ReverseAndRedistribute - reverses the payments and redistributes them
    //    ReverseOnly - reverses the payments, but does not redistribute them
    var redistributePayments = PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute;

    // Invoice Item Filter Type - Type of invoice items to process
    //    allitems - processes all the invoice items associated with the policy
    //    planneditems - processes only invoice items that have not yet been invoiced
    //    notfullypaiditems - processes only invoice items that have not yet been paid in full
    var itemFilterType = InvoiceItemFilterType.TC_ALLITEMS;

    // Include Down Payment Items - If an invoice item type other than All Items is selected, the Include Down Payment
    // Items checkbox can be checked to include down payment invoice items in the recalculation process. If the down
    // payment was previously billed under the original payment plan, that charge is reversed and a new invoice item is
    // created according to the new payment plan.
    // BrianS - Doesn't make a difference since Quarterly v2 didn't use down payments.
    var includeDownPaymentItems = true;

    Transaction.runWithNewBundle(\bundle -> {
      policyPeriod = bundle.add(policyPeriod);
      var paymentPlan = bundle.add(PaymentPlan.QuarterlyV3PaymentPlan);
      policyPeriod.NewPaymentPlan_TDIC = paymentPlan;
      var changer = new PaymentPlanChanger (policyPeriod, paymentPlan, redistributePayments,
                                            InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems));
      changer.execute();
    }, "iu");
  }

  /*
  Called by Guidewire to request that process self-terminate
    Return true if request will be honored
    Return false if request will not be honored
  Handled by setting monitor flag in code and exiting any loops
  */
  override function requestTermination() : boolean {
    return true;
  }
}