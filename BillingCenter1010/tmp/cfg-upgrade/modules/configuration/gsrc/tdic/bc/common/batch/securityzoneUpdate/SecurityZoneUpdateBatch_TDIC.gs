package tdic.bc.common.batch.securityzoneUpdate

uses gw.api.database.Query
uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory

class SecurityZoneUpdateBatch_TDIC extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${SecurityZoneUpdateBatch_TDIC.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_SECURITYZONEUPATE_TDIC);
  }

  override function checkInitialConditions() : boolean {
    return true;
  }

  override function requestTermination() : boolean {
    return true;
  }

  protected override function doWork() {
  if(ScriptParameters.SecurityZoneUpdated) {
    // find the Producers whose SecurityZOne is null and update with CATDIC Security Zone.
    var catdicSecurityZone = Query.make(SecurityZone).compare(SecurityZone#Name, Equals, "CATDIC Security Zone").select().AtMostOneRow
    var producers = Query.make(Producer).compare(Producer#SecurityZone, Equals, null).select()
    _logger.info(_LOG_TAG + " Found " + producers.Count + " Producers.")
    for (producer in producers) {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        producer = bundle.add(producer)
        var producerCode = producer.ProducerCodes.first().Code
        if (producerCode != null) {
          var securityZone = Query.make(SecurityZone).select().where(\zone -> zone.Name.contains(producerCode)).first()
          //Update SecurityZone for all the producers.
          producer.SecurityZone = securityZone
        } else {
          producer.SecurityZone = catdicSecurityZone
        }
      }, "iu")
    }
  }
  }
}