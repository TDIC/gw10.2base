package tdic.bc.config.invoice

uses java.text.DecimalFormat
uses com.tdic.util.misc.LuhnAlgorithm
uses java.lang.Exception
uses gw.api.util.DisplayableException
uses gw.api.system.BCLoggerCategory
uses java.math.BigDecimal
uses org.apache.commons.lang.StringUtils
uses org.slf4j.LoggerFactory

/**
 * US1277
 * 03/23/2015 Kesava Tavva
 *
 * Helper class for invoice pre-update rule. It has implementation of generating OCR Barcode for a given invoice.
 */
class TDIC_InvoicePURHelper {
  private static final var _logger = LoggerFactory.getLogger("Rules")
  private static final var INT_PATTERN_2 = "%02d"
  private static final var AMOUNT_FORMAT = new DecimalFormat("000000.00")
  private static final var POLICY_NUM_DIGITS = 10
  private static final var POLICY_NUM_PAD_CHAR = '0'

  /**
   * US1277
   * 03/23/2015 Kesava Tavva
   *
   * Static function generates OCRBar code by using LuhnAlgorithm util. Algorithm uses following values for generating code
   * PolicyNumber
   * PolicyTermNumber
   * InvoiceNumber
   * InvoiceAmount
   * InvoiceAmountDue
   */
  @Param("invoice", "Invoice which is being updated")
  @Returns("String Object which reprsents OCR Barcode")
  static function generateOCRBarCode(invoice : entity.Invoice, currentBalance: BigDecimal=null, invoiceamountDue: BigDecimal=null ) : String {
    var barCode : String
    _logger.debug("generateOCRBarCode started for invoice: ${invoice.InvoiceNumber}-${invoice}")
    try{
      if( !invoice.Policies.HasElements
          || invoice.Policies[0] == null
          || invoice.Policies[0].RiskJurisdiction == null
          || invoice.Policies[0].Policy.LOBCode == null
          ) return barCode

      var amount = currentBalance == null ? getAmount(invoice) : currentBalance
      var amountDue = invoiceamountDue == null ? getAmountDue(invoice) : invoiceamountDue
      _logger.info("PolicyNumber: ${invoice.Policies[0]?.PolicyNumber} | TermNumber: ${invoice.Policies[0]?.TermNumber} | InvoiceNumber: ${invoice.InvoiceNumber} | "
          +"RiskJurisdiction: ${invoice.Policies[0]?.RiskJurisdiction} | LOBCode: ${invoice.Policies[0]?.Policy.LOBCode} | "
          +"Amount: ${amount} | AmountDue: ${amountDue}")
      var policyPrefix = getConvertedPolicyPrefix(invoice.Policies[0]?.RiskJurisdiction, invoice.Policies[0]?.Policy.LOBCode)
      barCode = policyPrefix
          + StringUtils.leftPad(invoice.Policies[0]?.PolicyNumber,POLICY_NUM_DIGITS,POLICY_NUM_PAD_CHAR)
          + String.format(INT_PATTERN_2,{invoice.Policies[0]?.TermNumber})
          + invoice.InvoiceNumber.replaceAll("P","5")
          + formatAmount(amount)
          + formatAmount(amountDue)
      barCode = barCode+LuhnAlgorithm.calcChecksum(barCode)
    }catch(e : Exception){
      _logger.error("OCRBarCode generation failed for invoice : ${invoice.InvoiceNumber}-${invoice}", e)
      throw new DisplayableException("OCRBarCode generation Failed for invoice : ${invoice.InvoiceNumber}-${invoice}. Please review log file for more details.")
    }
    _logger.info("generateOCRBarCode completed for invoice: ${invoice.InvoiceNumber}-${invoice}")
    return barCode
  }
  /**
   * US1277
   * 03/23/2015 Kesava Tavva
   *
   * Generates 4 digit prefix number for bar code based on Policy's jurisdiction and Line of business code.
   */
  @Param("jurisdiction", "Policy's RiskJurisdiction value")
  @Param("lobCode", "Policy's line of business value")
  @Returns("String Object which reprsents Jurisdiction and LOB values for OCR barcode")
  private static function getConvertedPolicyPrefix(jurisdiction : typekey.Jurisdiction, lobCode : typekey.LOBCode) : String {
    var policyPrefix : String
    if(jurisdiction != null && lobCode != null){
      var lob = gw.api.util.TypecodeMapperUtil.getTypecodeMapper().getAliasByInternalCode( "LOBCode", "tdic:bc", lobCode as String)
      policyPrefix = LuhnAlgorithm.convertPolicyNum("${jurisdiction}${lob}")
    }
    return policyPrefix
  }

  /**
   * US1277
   * 04/09/2015 Kesava Tavva
   *
   * Convert BigDecimal to String by using decimal format. Converts -ve value and/or decimal value to +ve 8 digit whole number
   */
  @Param("amount", "Input value for formatting to 8 digit string object")
  @Returns("String Object which reprsents 8 digit formatted value")
  private static function formatAmount(amount : BigDecimal) : String {
    var formattedAmount : String
    if(amount < 0) amount = amount * -1
    formattedAmount = AMOUNT_FORMAT.format(amount)
    if(formattedAmount.contains(".")) formattedAmount = formattedAmount.remove(".")

    // BrianS - Limit length to 8
    if (formattedAmount.length > 8) {
      formattedAmount = "99999999";
    }

    return formattedAmount
  }

  /**
   * GW767
   *
   * Return invoice amount due when invoice's charge type is Audit to match with doc requirements
   * else return Policy's termc remaining balance.
   */
  @Param("invoice", "Invoice which is being updated")
  @Returns("BigDecimal, Amount value for OCR Barcode generation. ")
  private static function getAmount(invoice: Invoice): BigDecimal {
    return invoice.Policies[0]?.RemainingBalance.Amount
  }

  /**
   * GW1117
   *
   * Return Policy term unpaid amount. If calculated value is <=0 then return amount due as zero.
   */
  @Param("invoice", "Invoice which is being updated")
  @Returns("BigDecimal, Amount value for OCR Barcode generation. ")
  private static function getAmountDue(invoice: Invoice): BigDecimal {
    var amountDue = new BigDecimal(0.0)
    amountDue = (invoice.Status == typekey.InvoiceStatus.TC_BILLED) ? invoice.InvoiceStream.AmountBilledOrDue_TDIC?.Amount : invoice.InvoiceStream.AmountDue_TDIC?.Amount
    if(amountDue <= 0 or amountDue == null){
      amountDue = 0
    }
    return amountDue
  }
}