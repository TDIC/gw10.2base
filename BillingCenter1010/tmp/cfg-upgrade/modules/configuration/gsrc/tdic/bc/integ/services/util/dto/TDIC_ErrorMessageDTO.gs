package tdic.bc.integ.services.util.dto

uses gw.xml.ws.annotation.WsiExportable

@WsiExportable
final class TDIC_ErrorMessageDTO {
  var _errorCode : int as ErrorCode

  override function toString() : String{
    return "${ErrorCode}"
  }

}