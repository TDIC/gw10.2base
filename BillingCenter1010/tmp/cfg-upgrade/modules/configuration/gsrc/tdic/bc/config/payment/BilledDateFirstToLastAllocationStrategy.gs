package tdic.bc.config.payment

uses com.google.common.collect.Ordering
uses gw.bc.payment.InvoiceItemAllocationOrdering
uses gw.payment.AbstractAllocationStrategy

/**
 * Allocates credits first to last, using Billed Date as the first criteria.
 */
class BilledDateFirstToLastAllocationStrategy extends AbstractAllocationStrategy {
  override property get PositiveInvoiceItemOrdering() : Ordering<InvoiceItem> {
    return InvoiceItemAllocationOrdering.Util.getInvoiceItemOrderingsFromTypes ( {
                                                InvoiceItemOrderingType.TC_BILLEDDATE,
                                                InvoiceItemOrderingType.TC_EVENTDATE,
                                                InvoiceItemOrderingType.TC_CHARGEPATTERN } );
  }
}