package tdic.bc.config.enhancement

uses gw.api.database.Query

/**
 * US715
 * 01/06/2015 Vicente
 *
 * SuspensePayment Enhancement for TDIC.
 */
enhancement DirectSuspPmntItemSearchViewEnhancement : entity.DirectSuspPmntItemSearchView {
  /**
   * US715
   * 01/06/2015 Vicente
   *
   * Policy if the Policy Number is populated or null
   */
  @Returns("Policy if the Policy Number is populated")
  property get Policy_TDIC() : Policy {
    var policy = Query.make(PolicyPeriod).compare(PolicyPeriod#PolicyNumber, Equals, this.PolicyNumber).select().FirstResult
    return policy?.Policy
  }
}
