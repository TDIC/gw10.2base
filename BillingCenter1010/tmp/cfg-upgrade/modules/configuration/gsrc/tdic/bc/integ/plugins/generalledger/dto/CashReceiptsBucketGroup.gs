package tdic.bc.integ.plugins.generalledger.dto

uses java.util.Map
uses java.util.HashMap

/**
 * US570 - General Ledger Integration
 * 12/17/2014 Alvin Lee
 *
 * Class to hold the different buckets for cash receipts.  There must be one instance of this class per LOB and state
 * combination.
 */
class CashReceiptsBucketGroup {

  /**
   * A Map<String, CashReceiptsBucket> to connect the name of the cash receipts bucket to the bucket itself.
   */
  private var _nameBucketMap : Map<String, CashReceiptsBucket> as NameBucketMap

  construct() {
    _nameBucketMap = new HashMap<String, CashReceiptsBucket>()

    // Set up Payment Buckets (DEP)
    var ihdUnappliedBucket = new CashReceiptsBucket()
    ihdUnappliedBucket.Description = "WC IHD (MMDDYY) In-Process"
    ihdUnappliedBucket.SequenceNumber = 4
    ihdUnappliedBucket.DepOrNsf = "DEP"
    _nameBucketMap.put("IHDUnapplied", ihdUnappliedBucket)
    var lockboxUnappliedBucket = new CashReceiptsBucket()
    lockboxUnappliedBucket.Description = "WC LBX (MMDDYY) In-Process"
    lockboxUnappliedBucket.SequenceNumber = 4
    lockboxUnappliedBucket.DepOrNsf = "DEP"
    _nameBucketMap.put("LockboxUnapplied", lockboxUnappliedBucket)
    var visaMCUnappliedBucket = new CashReceiptsBucket()
    visaMCUnappliedBucket.Description = "WC CC V/MC (MMDDYY) In-Process"
    visaMCUnappliedBucket.SequenceNumber = 4
    visaMCUnappliedBucket.DepOrNsf = "DEP"
    _nameBucketMap.put("VisaMCUnapplied", visaMCUnappliedBucket)
    var amexUnappliedBucket = new CashReceiptsBucket()
    amexUnappliedBucket.Description = "WC CC AMEX (MMDDYY) In-Process"
    amexUnappliedBucket.SequenceNumber = 4
    amexUnappliedBucket.DepOrNsf = "DEP"
    _nameBucketMap.put("AmexUnapplied", amexUnappliedBucket)
    var eftUnappliedBucket = new CashReceiptsBucket()
    eftUnappliedBucket.Description = "WC EFT/ACH (MMDDYY) In-Process"
    eftUnappliedBucket.SequenceNumber = 4
    eftUnappliedBucket.DepOrNsf = "DEP"
    _nameBucketMap.put("EFTUnapplied", eftUnappliedBucket)

    // Set up Reversal Buckets (NSF)
    var ihdUnappliedBucketReversed = new CashReceiptsBucket()
    ihdUnappliedBucketReversed.Description = "WC IHD (MMDDYY) In-Process"
    ihdUnappliedBucketReversed.SequenceNumber = 0
    ihdUnappliedBucketReversed.DepOrNsf = "NSF"
    _nameBucketMap.put("IHDUnappliedReversed", ihdUnappliedBucketReversed)
    var lockboxUnappliedBucketReversed = new CashReceiptsBucket()
    lockboxUnappliedBucketReversed.Description = "WC LBX (MMDDYY) In-Process"
    lockboxUnappliedBucketReversed.SequenceNumber = 0
    lockboxUnappliedBucketReversed.DepOrNsf = "NSF"
    _nameBucketMap.put("LockboxUnappliedReversed", lockboxUnappliedBucketReversed)
    var visaMCUnappliedBucketReversed = new CashReceiptsBucket()
    visaMCUnappliedBucketReversed.Description = "WC CC V/MC (MMDDYY) In-Process"
    visaMCUnappliedBucketReversed.SequenceNumber = 0
    visaMCUnappliedBucketReversed.DepOrNsf = "NSF"
    _nameBucketMap.put("VisaMCUnappliedReversed", visaMCUnappliedBucketReversed)
    var amexUnappliedBucketReversed = new CashReceiptsBucket()
    amexUnappliedBucketReversed.Description = "WC CC AMEX (MMDDYY) In-Process"
    amexUnappliedBucketReversed.SequenceNumber = 0
    amexUnappliedBucketReversed.DepOrNsf = "NSF"
    _nameBucketMap.put("AmexUnappliedReversed", amexUnappliedBucketReversed)
    var eftUnappliedBucketReversed = new CashReceiptsBucket()
    eftUnappliedBucketReversed.Description = "WC EFT/ACH (MMDDYY) In-Process"
    eftUnappliedBucketReversed.SequenceNumber = 0
    eftUnappliedBucketReversed.DepOrNsf = "NSF"
    _nameBucketMap.put("EFTUnappliedReversed", eftUnappliedBucketReversed)
  }

}