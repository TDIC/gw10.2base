package tdic.bc.config.enhancement

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount
uses org.slf4j.LoggerFactory

enhancement InvoiceStreamEnhancement : entity.InvoiceStream {
  /**
   * Return the policy period used for invoicing.
   */
  public property get PolicyPeriod_TDIC() : PolicyPeriod {
    var retval : PolicyPeriod;
    var policyPeriods : PolicyPeriod[];

    if (this.Policy != null) {
      policyPeriods = this.Policy.PolicyPeriods;
    } else {
      var account = this.getFieldValue("Account") as Account;
      policyPeriods = account.Policies*.PolicyPeriods;
    }

    var logger = LoggerFactory.getLogger("Application.Invoice")
    if(policyPeriods.where(\pp -> pp.OverridingInvoiceStream == this)?.HasElements){
      retval = policyPeriods.firstWhere(\elt -> elt.OverridingInvoiceStream == this)
    } else {
      retval = policyPeriods.where(\pp -> pp.InvoiceStream == this)?.first()
      // Ideally the else block value doesn't matter much for the policy level billing.
      // In case the else block value doesn't satisfy the business needs then replace it with below code.
      // May be needed for the renewal of policyperiods which are on policy level billing.
      /*var policyPeriodList = policyPeriods.where(\pp -> pp.InvoiceStream == this)?.toList()
      if(policyPeriodList.HasElements and policyPeriodList != null){
        Collections.sort(policyPeriodList, Collections.reverseOrder())
        retval = policyPeriods.first()
      }*/
    }
    logger.trace("PolicyPeriod returned from InvoiceStreamEnhancement PolicyPeriod_TDIC property : ${retval.PolicyNumberLong}")
    return retval;
  }

  protected function getChargeTotal (chargePatternCode : String) : MonetaryAmount {
    var total = new MonetaryAmount (0, this.Currency);

    var policyPeriod = this.PolicyPeriod_TDIC;
    var charges = policyPeriod.Charges;
    var billingInstructions = charges.partition(\c -> c.BillingInstruction).Keys;

    // Remove delinquency cancels that haven't been reinstated for in-progress delinqencies.
    billingInstructions.removeWhere(\bi -> bi typeis Cancellation and ignoreCancellation(bi));

    // Remove reversed audits without a following final audit.
    var lastNonReversedAudit = billingInstructions.whereTypeIs(Audit)
                                                  .where(\a -> a.RevisionType_TDIC != AuditRevisionType_TDIC.TC_REVERSAL)
                                                  .maxBy(\a -> a.CreateTime);
    billingInstructions.removeWhere(\bi -> bi typeis Audit and
                                           bi.RevisionType_TDIC == AuditRevisionType_TDIC.TC_REVERSAL and
                                           (lastNonReversedAudit == null or bi.CreateTime > lastNonReversedAudit.CreateTime));

    for (billingInstruction in billingInstructions) {
      for (charge in billingInstruction.Charges) {
        if (chargePatternCode == null or charge.ChargePattern.ChargeCode == chargePatternCode) {
          total += charge.Amount;
        }
      }
    }

    return total;
  }

  private function ignoreCancellation (cancellation : Cancellation) : boolean {
    var retval = false;

    // Ignore cancellation if there are Cancellation contexts.
    for (charge in cancellation.Charges) {
      for (invoiceItem in charge.InvoiceItems) {
        for (paymentItem in invoiceItem.PaymentItems) {
          if (paymentItem typeis DirectBillPaymentItem and
              paymentItem.DBPmntDistributionContext_TDIC == DBPmntDistributionContext.TC_CANCELLATION) {
            return true;
          }
        }
      }
    }

    return retval;
  }

  public property get EstimatedAnnualPremium_TDIC() : MonetaryAmount {
    return this.getChargeTotal ("Premium");
  }

  public property get StateAssessments_TDIC() : MonetaryAmount {
    return this.getChargeTotal ("StateAssessmentFee");
  }

  public property get CIGA_Surcharge_TDIC() : MonetaryAmount {
    return this.getChargeTotal ("CIGASurcharge");
  }

  public property get TotalPremium_TDIC() : MonetaryAmount {
    return this.getChargeTotal (null);
  }

  public property get PaymentsReceived_TDIC() : MonetaryAmount {
    var policyPeriod = this.PolicyPeriod_TDIC;
    var total : MonetaryAmount = null;

    var amountZero = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    var unappliedFundBalance = (this.UnappliedFund.Balance == null) ? amountZero : this.UnappliedFund.Balance

    if (policyPeriod != null) {
      total = this.PolicyPeriod_TDIC.PaidAmount + unappliedFundBalance
    }

    return total
  }

  public property get CurrentBalance_TDIC() : MonetaryAmount {
    var balance = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    var policyPeriod = this.PolicyPeriod_TDIC
    if(policyPeriod != null){

      var totalPremium = policyPeriod.OverridingInvoiceStream != null ? this.TotalPremium_TDIC :
          policyPeriod.Policy.OrderedPolicyPeriods.where(\elt -> elt.OverridingInvoiceStream == null)?.sum(\s -> s.TotalPremium_TDIC)
      var paymentsReceived = policyPeriod.OverridingInvoiceStream != null ? this.PaymentsReceived_TDIC :
          policyPeriod.Policy.OrderedPolicyPeriods.where(\elt -> elt.OverridingInvoiceStream == null)?.sum(\s -> s.PaymentsReceived_TDIC)

      if (totalPremium != null and paymentsReceived != null) {
        balance = totalPremium - paymentsReceived;
      }
    }

    return balance;
  }

  public property get AmountBilledOrDue_TDIC() : MonetaryAmount {
    var amountBilledOrDue = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    var policyperiod = this.PolicyPeriod_TDIC
    if(policyperiod != null){
      amountBilledOrDue = policyperiod.OverridingInvoiceStream != null ? policyperiod.AmountBilledOrDue_TDIC :
          policyperiod.Policy.OrderedPolicyPeriods.where(\elt -> elt.OverridingInvoiceStream == null)?.sum(\s -> s.AmountBilledOrDue_TDIC)
    }
    if(amountBilledOrDue != null){
      return amountBilledOrDue
    } else {
      return new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    }
  }

  public property get AmountDue_TDIC() : MonetaryAmount {
    var amountDue = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    var policyperiod = this.PolicyPeriod_TDIC
    if(policyperiod != null) {
      amountDue = policyperiod.OverridingInvoiceStream != null ? policyperiod.AmountDue_TDIC :
          policyperiod.Policy.OrderedPolicyPeriods.where(\elt -> elt.OverridingInvoiceStream == null)?.sum(\s -> s.AmountDue_TDIC)
    }
    if(amountDue != null){
      return amountDue
    } else {
      return new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    }
  }

  public property get NonBlankInvoices_TDIC() : Invoice[]{
    return this.Invoices.where( \ elt -> (elt.Amount_amt!=0 or elt.AmountDue_amt != 0))
  }

  public function debugInvoiceStream() : String {
    var message : String;

    message = "Invoice Stream " + this.DisplayName;
    message += "\nUnapplied Fund " + this.UnappliedFund + " - Balance = " + this.UnappliedFund.Balance
                  + ", DelinquencyCancelAmount_TDIC = " + this.PolicyPeriod_TDIC.DelinquencyCancelAmount_TDIC;
    for (invoice in this.Invoices.orderBy(\i -> i.Date)) {
      message += "\n   Invoice " + invoice.InvoiceNumber + " (" + invoice.Status.DisplayName
          + ") - EventDate = " + invoice.EventDate + ", Due " + invoice.DueDate
          + ", Amount = " + invoice.Amount_amt + ", TotalPaid = " + invoice.TotalPaid
          + ", AmountDue = " + invoice.AmountDue_amt;
    }

    if (this.PolicyPeriod_TDIC != null) {
      message += "\n   Estimated Annual Premium = " + this.EstimatedAnnualPremium_TDIC;
      message += "\n   State Assessments = " + this.StateAssessments_TDIC;
      message += "\n   CIGA Surcharge = " + this.CIGA_Surcharge_TDIC;
      message += "\n   Total Premium = " + this.TotalPremium_TDIC;
      message += "\n   Payment Received = " + this.PaymentsReceived_TDIC;
      message += "\n   Current Balance = " + this.CurrentBalance_TDIC;
      message += "\n   Amount Billed Or Due = " + this.AmountBilledOrDue_TDIC;
      message += "\n   Amount Due = " + this.AmountDue_TDIC;
    }

    return message;
  }

  /*
   * This method returns a Planned existing Invoice with the same dates
   */
  function getPlannedExistingInvoiceWithDates_Ext(billDate: Date, dueDate: Date) : Invoice {
    var existingInvoice = this.Invoices.firstWhere(\elt1 -> elt1?.Date.isSameDayIgnoringTime(billDate)
        and elt1?.DueDate.isSameDayIgnoringTime(dueDate)
        and elt1?.Planned)
    return  existingInvoice
  }
/*
 * This method returns the actual Payment Instrument for the InvoiceStream.
 */
  function getOriginalPaymentInstrument() : PaymentInstrument{
    if(this.OverridingPaymentInstrument != null)
      return this.OverridingPaymentInstrument
    else
      return this.PaymentInstrument
  }
}
