package tdic.bc.integ.plugins.generalledger.dto

uses java.math.BigDecimal
uses java.util.Date
uses java.text.SimpleDateFormat

/**
 * US570 - General Ledger Integration
 * 12/17/2014 Alvin Lee
 *
 * A class representing a single bucket for cash receipts.
 */
class CashReceiptsBucket extends GLBucket {

  /**
   * The sequence number corresponding to this cash receipts bucket, used in the Cash Receipts flat file.
   */
  private var _sequenceNumber : int as SequenceNumber

  /**
   * Will either be "DEP" or "NSF", depending if it is a payment line or reversal line.
   */
  private var _depOrNsf : String as DepOrNsf

  construct() {
    super()
  }

}