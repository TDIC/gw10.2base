package tdic.bc.config.enhancement

uses java.util.Date
uses gw.api.database.Query
uses java.util.List

/**
 * US372
 * 11/17/2014 Vicente
 *
 * PaymentPlan Enhancement for TDIC.
 */
enhancement PaymentPlanEnhancement : PaymentPlan {
  static public property get QuarterlyV2PaymentPlan() : PaymentPlan {
    return getPaymentPlanByPublicID ("bc-loc:3007");
  }

  static public property get QuarterlyV3PaymentPlan() : PaymentPlan {
    return getPaymentPlanByPublicID ("bc-loc:2003");
  }

  static public function getPaymentPlanByPublicID (publicID : String) : PaymentPlan {
    var query = Query.make(PaymentPlan).compare(PaymentPlan#PublicID, Equals, publicID);
    return query.select().AtMostOneRow;
  }

  /**
   * Gets all of the available payment plans in the system. (overlap the period between
   * effective date and expiration date)
   *
   * The content of this method has moved to policyperiod enhancement.
   * The query in the Enhancement is written better.
   *
   * This method is obsolete, please consider using the policyperiod enhancement
   * property AvailablePaymentPlans.
   **/
  /**
   *  GW287 - The code allows payment plan in the future (effective date of the policy < effective date of the payment
   *          plan when it should not. Modify the logic to fix the scenario.
   *  01/22/2016 Hermia Kho
   */
  @Param("effectiveDate", "Start date of the period")
  @Param("expirationDate", "End date of the period")
  @Returns("Returns the payment plans that overlap the period between the input effective date and expiration date")
  public static function AvailablePaymentPlans_TDIC(effectiveDate: Date, expirationDate: Date) : List<PaymentPlan> {
    //Check if all Payment Plans overlap with this policy period
    var paymentPlanQuery = gw.api.database.Query.make(entity.PaymentPlan)
    //paymentPlanQuery.compare(entity.PaymentPlan#Currency, Equals, Currency.TC_USD)
    paymentPlanQuery.compare(entity.PaymentPlan#UserVisible, Equals, true)
    var allPaymentPlans = paymentPlanQuery.select().where(\elt -> elt.Currencies.hasMatch(\elt1 -> elt1.Value == Currency.TC_USD))

    //var filteredPlans = allPaymentPlans.where( \ paymentP -> expirationDate >= paymentP.EffectiveDate
    //    && (paymentP.ExpirationDate == null || effectiveDate <= paymentP.ExpirationDate))
    var filteredPlans = allPaymentPlans.where( \ paymentP -> (effectiveDate >= paymentP.EffectiveDate and effectiveDate <= paymentP.ExpirationDate) or
        (effectiveDate >= paymentP.EffectiveDate and paymentP.ExpirationDate == null) )
    return filteredPlans
  }

  /**
   * Determines if payment plan is annual by checking
   * MaximumNumberOfInstallments which will be 12 if plan is Monthly
   */
  property get IsMonthly() : boolean {
    return this.MaximumNumberOfInstallments == 12
  }
}