package tdic.bc.common.batch.banklockbox.dto

uses java.math.BigDecimal

/**
 * US567 - Bank/Lockbox Integration
 * 01/09/2015 Alvin Lee
 *
 * Fields to keep track of the totals for the entire lockbox flat file.
 */
class LockboxFileTotal {

  /**
   * Total number of transactions in the lockbox file.
   */
  private var _totalNumberTransactions : int as TotalNumberTransactions

  /**
   * Total amount of the transactions in the lockbox file.
   */
  private var _totalAmount : BigDecimal as TotalAmount

}