package tdic.bc.common.batch.securityzoneUpdate

uses gw.api.database.Query
uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory

class AccountSecurityZoneUpdateBatch_TDIC extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${AccountSecurityZoneUpdateBatch_TDIC.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_ACCOUNTSECURITYZONEUPATE_TDIC);
  }

  override function checkInitialConditions() : boolean {
    return true;
  }

  override function requestTermination() : boolean {
    return true;
  }

  protected override function doWork() {
    if(ScriptParameters.SecurityZoneUpdated) {
      // find the Accounts whose SecurityZOne is null and update with CATDIC Security Zone.
      var catdicSecurityZone = Query.make(SecurityZone).compare(SecurityZone#Name, Equals, "CATDIC Security Zone").select().AtMostOneRow
      var accounts = Query.make(Account).compare(Account#SecurityZone, Equals, null).select()
      _logger.info(_LOG_TAG + "Found " + accounts.Count + " Accounts.")
      for (account in accounts) {
        if (this.TerminateRequested) {
          break;
        }
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          account = bundle.add(account)
          account.SecurityZone = catdicSecurityZone
        }, "iu")
      }
    }
  }
}