package tdic.bc.config.enhancement

uses gw.api.database.Query

/**
 * US123
 * 01/05/2015 Vicente
 *
 * SuspensePayment Enhancement for TDIC.
 */
enhancement SuspensePaymentEnhancement: entity.SuspensePayment {
  /**
   * US123
   * 01/05/2015 Vicente
   *
   * Account Name if the Account Number is populated or null
   */
  @Returns("Account Name if the Account Number is populated")
  property get AccountName_TDIC() : String {
    var account = Query.make(Account).compare(Account#AccountNumber,Equals ,this.AccountNumber).select().FirstResult
    return account?.AccountName
  }

  /**
   * US123
   * 01/05/2015 Vicente
   *
   * Policy if the Policy Number is populated or null
   */
  @Returns("Policy if Policy Number is populated")
  property get Policy_TDIC() : Policy {
    var policy = Query.make(PolicyPeriod).compare(PolicyPeriod#PolicyNumber,Equals ,this.PolicyNumber).select().FirstResult
    return policy?.Policy
  }
}
