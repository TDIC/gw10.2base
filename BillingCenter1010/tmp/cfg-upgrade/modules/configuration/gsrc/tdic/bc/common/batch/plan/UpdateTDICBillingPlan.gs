package tdic.bc.common.batch.plan

uses gw.api.database.Query
uses gw.pl.currency.MonetaryAmount
uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory

class UpdateTDICBillingPlan extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${UpdateTDICBillingPlan.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_UPDATEBILLINGPLAN_TDIC)
  }

  override function checkInitialConditions() : boolean {
    return true;
  }

  override function requestTermination() : boolean {
    return true;
  }

  protected override function doWork() {

    // find the plan to be updated
    var billingPlan = Query.make(BillingPlan).compare(BillingPlan#Name, Equals, "TDIC Billing Plan").select().FirstResult
      // update the billing plan field values.
      if (billingPlan != null){
        updateBillingPlan(billingPlan)
        incrementOperationsCompleted()
      }
    _logger.info(_LOG_TAG + billingPlan
        + " - updating billing plan for TDIC has been completed")
  }

  protected function updateBillingPlan(billingPlan : BillingPlan) : void {
    _logger.info(_LOG_TAG + billingPlan
        + " - updating billing plan for TDIC")
    Transaction.runWithNewBundle(\bundle -> {
      billingPlan = bundle.add(billingPlan)
      // update the following fields
      // NonResponsivePmntDueInterval = 19, PaymentReversalFee = 25$, LowBalanceFeethreshold - 5.01$
      // RequestIntervalDayCount = 0, ChangeDeadlineIntervalDayCount = 1, DraftIntervalDayCount = 3
      billingPlan.NonResponsivePmntDueInterval = 19
      billingPlan.RequestIntervalDayCount = 0
      billingPlan.ChangeDeadlineIntervalDayCount = 1
      billingPlan.DraftIntervalDayCount = 3
      billingPlan.SuppressLowBalInvoices = true
      billingPlan.LowBalanceThreshold = new MonetaryAmount(5.01bd, Currency.TC_USD)
      billingPlan.PaymentReversalFee = new MonetaryAmount(0.00bd, Currency.TC_USD)
    }, "iu");
  }
}