package tdic.bc.integ.plugins.generalledger

uses gw.plugin.messaging.MessageRequest
uses gw.api.system.BCLoggerCategory
uses org.slf4j.LoggerFactory

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Implementation of a Messaging Request plugin to send GL transaction messages.  This is mainly for setting the
 * SenderRefID field on the message.
 */
class TDIC_GLIntegrationMessageRequest implements MessageRequest {

  /**
   * Standard logger for General Ledger
   */
  private static final var _logger = LoggerFactory.getLogger("GENERAL_LEDGER")

  /**
   * Sets the SenderRefID on the message.
   */
  @Param("msg","the original message")
  @Returns("A String for the payload, which will be unchanged in this case")
  override function beforeSend(msg: entity.Message): String {
    _logger.debug("TDIC_GLIntegrationMessageRequest#beforeSend - Entering")
    if (msg.SenderRefID == null) {
      msg.SenderRefID = msg.PublicID
    }
    _logger.debug("TDIC_GLIntegrationMessageRequest#beforeSend - SenderRefID: " + msg.SenderRefID)
    return msg.Payload
  }

  override function afterSend(p0: entity.Message) {
  }

  override function shutdown() {
  }

  override function suspend() {
  }

  override function resume() {
  }

  override property  set DestinationID(p0: int) {
  }
}