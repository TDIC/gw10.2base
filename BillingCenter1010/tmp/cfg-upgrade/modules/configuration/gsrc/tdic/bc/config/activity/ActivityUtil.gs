package tdic.bc.config.activity

uses gw.api.database.Query
uses gw.api.util.DisplayableException
uses gw.api.database.IQueryBeanResult
uses gw.api.locale.DisplayKey

/**
 * US1186 - Attach Policy Periods to Manually Created Activities
 * 2/19/2015 Alvin Lee
 *
 * Helper class when creating activities.
 */
class ActivityUtil {

  /**
   * Finds a Policy Period by the Policy Number Long.
   */
  @Param("policyNumber", "A String containing the Policy Period Number")
  @Returns("The matching Policy Period entity")
  public static function findPolicyPeriodFromNumber(policyNumber: String) : PolicyPeriod {
    if (policyNumber == null) {
      return null
    }
    var policyPeriodQuery = Query.make(PolicyPeriod.Type)
    policyPeriodQuery.compare(PolicyPeriod#PolicyNumberLong, Equals, policyNumber)
    var result = policyPeriodQuery.select()
    if (result.Count == 0) {
      throw new DisplayableException(DisplayKey.get("TDIC.Web.ActivityDetail.Error.PolicyNotFound", policyNumber))
    }
    var foundPolicyPeriod = result.getAtMostOneRow()
    return foundPolicyPeriod
  }

  /**
   * Finds all activities that were created by the specified user.
   */
  @Param("user", "The User entity to search for who created the activity")
  @Returns("An IQueryBeanResult<Activity> to return the collection of activities created by the specified user")
  public static function findActivitiesCreatedBy(user : User) : IQueryBeanResult<Activity> {
    if (user == null) {
      return null
    }
    var activityQuery = Query.make(Activity)
    activityQuery.compare(Activity#CreateUser, Equals, user)
    return activityQuery.select()
  }

}