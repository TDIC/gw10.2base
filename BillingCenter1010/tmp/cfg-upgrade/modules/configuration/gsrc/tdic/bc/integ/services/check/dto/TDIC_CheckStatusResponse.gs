package tdic.bc.integ.services.check.dto

uses gw.xml.ws.annotation.WsiExportable

@WsiExportable
final class TDIC_CheckStatusResponse {

  var checkStatusResponseDTOList: List<TDIC_CheckStatusResponseDTO> as CheckStatusResponseDTOList

  override function toString() : String{
    var response = new StringBuffer("Response: ")
    for(record in CheckStatusResponseDTOList){
      response.append(record.toString())
    }
    return response.toString()
  }

}