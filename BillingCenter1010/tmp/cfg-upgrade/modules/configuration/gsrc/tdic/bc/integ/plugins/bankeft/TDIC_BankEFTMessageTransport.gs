package tdic.bc.integ.plugins.bankeft

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestWritableBean
uses tdic.bc.integ.plugins.message.TDIC_MessagePlugin

uses java.sql.Connection
uses java.sql.SQLException
uses java.lang.Exception
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_PaymentInstrumentWritableBean

/**
 * US568 - Bank/EFT Integration
 * 09/15/2014 Alvin Lee
 *
 * Implementation of a Messaging Transport plugin to process Payment Requests/ Prenote validations.  This will write a
 * message to the integration database with the payment request details/payment instruments details, to be later retrieved
 * and written to the NACHA flat file to be sent to Bank of America.
 */
class TDIC_BankEFTMessageTransport extends TDIC_MessagePlugin {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_EFT")

  /**
   * Default constructor
   */
  construct() {
    _logger.debug("TDIC_BankEFTMessageTransport#construct - Creating Bank/EFT Message Transport plugin")
  }

  override function send(msg: entity.Message, payload: String) {
    _logger.debug("TDIC_BankEFTMessageTransport#send - Payload: " + payload)

    // Check for duplicates
    var history = gw.api.database.Query.make(MessageHistory).compare(MessageHistory#SenderRefID, Equals, msg.SenderRefID).select().AtMostOneRow
    if (history != null) {
      _logger.warn("TDIC_BankEFTMessageTransport#send - Duplicate message found with SenderRefID: " + msg.SenderRefID)
      history = gw.transaction.Transaction.getCurrent().add(history)
      history.reportDuplicate()
      return
    }


    var dbConnection : Connection = null
    try {
      // Get a connection from DatabaseManager using the GWINT DB URL
      var dbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      dbConnection = DatabaseManager.getConnection(dbUrl)

      if (msg.EventName.equals(Account.ACCOUNTCHANGED_EVENT)) {
        var paymentInstrumentModel = tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.TDIC_PaymentInstrumentWritableBean.parse(payload)
        // Get the Payment Instrument writable bean from the model
        var paymentInstrumentBean = new TDIC_PaymentInstrumentWritableBean()
        paymentInstrumentBean.loadFromModel(paymentInstrumentModel)
        paymentInstrumentBean.executeCreate(dbConnection)
      } else {
        var paymentRequestModel = tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.PaymentRequestWritableBean.parse(payload)
        // Get the Payment Request writable bean from the model
        var paymentRequestBean = new PaymentRequestWritableBean()
        paymentRequestBean.loadFromModel(paymentRequestModel)
        paymentRequestBean.executeCreate(dbConnection)
      }
        // Commit DB Connection
        if (dbConnection != null) {
          dbConnection.commit()
        } else {
          _logger.error("TDIC_BankEFTMessageTransport#send - Unable to get DB connection using JDBC URL: " + dbUrl)
          throw new SQLException("Unable to get DB connection using JDBC URL: " + dbUrl)
        }

        //Acknowledge message
        msg.reportAck()
      }catch(sqle:SQLException){
        // Handle database error; no need to call reportError here since we are re-throwing the exception
        _logger.error("Database connectivity/execution error persisting entity", sqle)
        if (dbConnection != null) {
          // Attempt to rollback
          try {
            dbConnection.rollback()
          } catch (rbe : Exception) {
            _logger.error("Exception while rolling back DB", rbe)
          }
        }
        throw sqle
      }catch(e:Exception){
        // Handle other errors; no need to call reportError here since we are re-throwing the exception
        _logger.error("Unexpected error persisting entity", e)
        if (dbConnection != null) {
          // Attempt to rollback
          try {
            dbConnection.rollback()
          } catch (rbe : Exception) {
            _logger.error("Exception while rolling back DB", rbe)
          }
        }
        throw e
      }finally {
      // Close database connection
      if (dbConnection != null) {
        _logger.debug("Closing DB connection")
        try {
          DatabaseManager.closeConnection(dbConnection)
        } catch (e : Exception) {
          _logger.error("Exception on closing DB (continuing)", e)
        }
      }
    }
  }

  override function shutdown() {
    _logger.info("TDIC_BankEFTMessageTransport#shutdown - Entering")
  }

  override function resume() {
    _logger.info("TDIC_BankEFTMessageTransport#resume - Entering")
  }

  override property  set DestinationID(p0: int) {
  }

}