package tdic.bc.common.batch.eftreversal.helper

uses gw.pl.currency.MonetaryAmount
/**
 * GINTEG-99
 * 10/10/2019 Vipul Kumar
 *
 * Object used to store fields for EFT/ACH payment reversal records.
 */
class EFTPaymentDTO {

  private var _transactionNumber : String  as TxnNumber
  private var _reversalDate : Date as ReversalDate
  private var _amount : MonetaryAmount as Amount
  private var _payeeName : String as PayeeName
  private var _policyNumber : String as PolicyNumber
  private var _reasonCode : String as ReasonCode
  private var _description : String as Description
  private var _routingNumber : String as RoutingNumber
  private var _accountNumber : String as AccountNumber
  private var _paymentReversered : Boolean as PaymentReversed
  private var _isPrenoteReturn : Boolean as IsPrenoteReturn = Boolean.FALSE
  private var _invoiceNumber : String as InvoiceNumber

}