package tdic.bc.config.invoice

uses gw.api.database.Query
uses gw.api.system.BCLoggerCategory
uses gw.pl.currency.MonetaryAmount
uses org.slf4j.LoggerFactory

class InvoiceUtil {

  /**
   * Determine the document type which should be generated for the invoice.
   */
  static public function getDocumentCreationEventType (invoice : Invoice) : TDIC_DocCreationEventType {
    var retval : TDIC_DocCreationEventType;
    var policyPeriod = invoice.InvoiceStream.PolicyPeriod_TDIC;
    var firstInvoice = policyPeriod.Invoices.minBy(\i -> i.EventDate);
    var apw = (policyPeriod.PaymentPlan.Name == "TDIC Monthly APW");

    if (invoice == firstInvoice) {
      if (policyPeriod.IsPolicyPeriodCreatedByIssuance_TDIC) {
        retval = TDIC_DocCreationEventType.TC_INITIALBILLING;
      } else {
        retval = apw ? TDIC_DocCreationEventType.TC_APWRENEWAL : TDIC_DocCreationEventType.TC_NONAPWRENEWAL;
      }
    } else {
      if (invoice.IsFinalAuditInvoice_TDIC) {
        retval = apw ? TDIC_DocCreationEventType.TC_FINALAUDITAPW : TDIC_DocCreationEventType.TC_FINALAUDIT;
      } else {
        retval = TDIC_DocCreationEventType.TC_BILLINGINSTALLMENT;
      }
    }

    return retval;
  }

  /**
   * Add invoice to the resend list.
   */
  static public function addToResendList (invoice : Invoice) : void {
    var query = Query.make(ResendInvoice_TDIC).compare(ResendInvoice_TDIC#Invoice, Equals, invoice);
    var results = query.select();

    if (results.Empty) {
      var resend = new ResendInvoice_TDIC(invoice.Bundle);
      resend.Invoice = invoice;
    } else {
      var logger = LoggerFactory.getLogger("Application.Invoice");
      logger.trace ("Invoice " + invoice.InvoiceNumber + " is already on the resend list.");
    }
  }

  /**
   * Invoice threshold - invoicing will occur when over this amount.
   */
  static public property get InvoiceThreshold() : MonetaryAmount {
    return new MonetaryAmount (5, Currency.TC_USD);
  }

  /**
   * Clear the payment item distribution context
  */
  static public function clearDistributionContext (policyPeriod : PolicyPeriod) : void {
    var logger = LoggerFactory.getLogger("Application.Invoice");
    logger.trace ("Clearing distribution context on Policy " + policyPeriod.PolicyNumberLong);

    for (invoiceStream in policyPeriod.Policy.Account.InvoiceStreams) {
      for (invoice in invoiceStream.Invoices) {
        for (invoiceItem in invoice.InvoiceItems.where (\ii -> ii.PolicyPeriod == policyPeriod)) {
          for (paymentItem in invoiceItem.PaymentItems) {
            if (paymentItem typeis DirectBillPaymentItem) {
              paymentItem.DBPmntDistributionContext_TDIC = null;
            }
          }
        }
      }
    }
  }
}