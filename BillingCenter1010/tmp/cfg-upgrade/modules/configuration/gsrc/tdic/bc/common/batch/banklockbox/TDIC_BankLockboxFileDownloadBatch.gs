package tdic.bc.common.batch.banklockbox

uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses gw.pl.exception.GWConfigurationException
uses java.lang.RuntimeException
uses gw.api.system.server.ServerUtil
uses java.io.File
uses com.tdic.util.misc.FtpUtil
uses java.util.HashSet
uses com.tdic.util.misc.EmailUtil
uses java.lang.Exception
uses java.io.IOException
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/22/15
 * US567 - Bank/Lockbox Integration
 * Implementation of a custom batch process for the Bank/Lockbox Integration to download Bank/Lockbox file.
 */
class TDIC_BankLockboxFileDownloadBatch extends BatchProcessBase {

  private static final var _logger = LoggerFactory.getLogger("BANK_LOCKBOX")

  /**
   * Key for looking up the notification email addresses from the integration database (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"

  /**
   * Notification email addresses.
   */
  private var _notificationEmail : String = null

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Notification email addresses in case of any failure in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Key for looking up the incoming file directory from the integration database (value: lockbox.incomingdir).
   */
  public static final var INCOMING_DIRECTORY_KEY : String = "lockbox.incomingdir"

  /**
   * Absolute directory path/string for incoming files.
   */
  private var _incomingPath : String = null

  /**
   * Name of the vendor for purposes of connecting to the FTP (value: BofaIncoming).
   */
  public static final var VENDOR_NAME : String = "BofaIncoming"

  /**
   * A Set<File> indicating the files that need to be processed, as more than one lockbox file can be processed at once.
   */
  private var _incomingFilesToProcess = new HashSet<File>()

  /**
   * Notification email's subject for Bank/Lockbox batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Bank/Lockbox Batch Job Failure on server" + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Bank/Lockbox batch job failures.
   */
  public static final var EMAIL_SUBJECT: String = "Bank/Lockbox Batch Job Completed" + gw.api.system.server.ServerUtil.ServerId

  /**
   * Key for looking up the done directory from the integration database (value: lockbox.donedir).
   */
  public static final var DONE_DIRECTORY_KEY : String = "lockbox.donedir"

  /**
   * Key for looking up the Integration DB URL from properties file
   */
  public static final var INTEGRATION_DB_URL_KEY : String = "IntDBURL"

  /**
   * Absolute directory path/string for completed files.
   */
  private var _donePath : String = null

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_BANKLOCKBOXFILEDOWNLOAD)
  }

  /**
   * Runs the actual batch process.
   */
  override function doWork() {
    // GINTEG-1135 : Skip if we don't have any data to processthere are no transactions that need to have flat files written.
    if (checkInitialConditions_TDIC()) {
      try {
        var fileList = FtpUtil.getFileList(VENDOR_NAME)
        var filesToDownload = new HashSet<String>()
        if (fileList == null) {
          throw new RuntimeException("Error retrieving list of files from the FTP server. Check integration log for stack trace.")
        }
        for (fileName in fileList) {
          _logger.debug("TDIC_BankLockboxFileDownloadBatch#doWork() - Checking file from FTP server: " + fileName)
          // Regex match for Bank/Lockbox flat files
          if (fileName.matches("WC[0-9]{8}\\.txt") && !isPreviouslyCompletedFile(fileName)) {
            _logger.debug("TDIC_BankLockboxFileDownloadBatch#doWork() - Adding file for processing: " + fileName)
            filesToDownload.add(fileName)
          }
        }
        for (fileToDownload in filesToDownload) {
          var incomingFileToProcess = new File(_incomingPath + "/" + fileToDownload)
          // Download file from server
          if (!FtpUtil.downloadFile(VENDOR_NAME, incomingFileToProcess)) {
            throw new RuntimeException("Unable to download lockbox file with name '" + fileToDownload
                + "' from the FTP server. Check integration log for stack trace.")
          }
          _incomingFilesToProcess.add(incomingFileToProcess)
        }

        // If there is no file from the bank, send an email notification
        if (_incomingFilesToProcess.Empty) {
          _logger.warn("TDIC_BankLockboxFileDownloadBatch#doWork() - No lockbox file found from the bank. Sending email notification and exiting.")
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "The Bank/Lockbox batch job has found no files from the bank to process.")
        } else {
          OperationsExpected = _incomingFilesToProcess.Count
          _logger.info("TDIC_BankLockboxFileDownloadBatch#doWork() - Exiting with " + _incomingFilesToProcess.Count
              + " files downloaded to incoming directory in TDIC network.")
          EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT, "The Bank/Lockbox batch completed with" + _incomingFilesToProcess.Count
              + " files downloaded to incoming directory in TDIC network.")
        }

      } catch (io : IOException) {
        _logger.error("TDIC_BankLockboxFileDownloadBatch#doWork() - IOException= " + io)
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "BankLockboxFileDownload batch job failed due to error:" + io.LocalizedMessage)
        throw io
      } catch (ex : Exception) {
        _logger.error("TDIC_BankLockboxFileDownloadBatch#doWork() - Exception= " + ex)
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "BankLockboxFileDownload batch job failed due to error:" + ex.LocalizedMessage)
        throw ex
      }
    }
  }

  /**
   * Determines if the process should run the doWork() method.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  @Throws(RuntimeException, "If there are connection issues with the bank's FTP server")
  function checkInitialConditions_TDIC() : boolean {
    _logger.debug("TDIC_BankLockboxBatch#checkInitialConditions() - Entering")
    var dbUrl : String
    try {
      // Get required values from integration database
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty(INTEGRATION_DB_URL_KEY)
      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }
      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
      if(_failureNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _incomingPath = PropertyUtil.getInstance().getProperty(INCOMING_DIRECTORY_KEY)
      if (_incomingPath == null) {
        throw new GWConfigurationException("Cannot retrieve incoming file path with the key '" + INCOMING_DIRECTORY_KEY
            + "' from integration database.")
      }
      var incomingDirectory = new File(_incomingPath)
      if (!incomingDirectory.exists()) {
        if (!incomingDirectory.mkdirs()) {
          _logger.error("TDIC_BankLockboxBatch#checkInitialConditions() - Failed to create incoming directory: " + _incomingPath)
          throw new GWConfigurationException("Failed to create incoming directory: " + _incomingPath)
        }
      }

      _donePath = PropertyUtil.getInstance().getProperty(DONE_DIRECTORY_KEY)
      if (_donePath == null) {
        throw new GWConfigurationException("Cannot retrieve completed file path with the key '" + DONE_DIRECTORY_KEY
            + "' from integration database.")
      }

      var doneDirectory = new File(_donePath)
      if (!doneDirectory.exists()) {
        if (!doneDirectory.mkdirs()) {
          _logger.error("TDIC_BankLockboxFileDownloadBatch#checkInitialConditions() - Failed to create done directory: " + _donePath)
          throw new GWConfigurationException("Failed to create done directory: " + _donePath)
        }
      }

      return true

    } catch (gwce : GWConfigurationException) {
      // Log and send email notification of failure
      _logger.error("TDIC_BankLockboxFileDownloadBatch#checkInitialConditions - Integration database either not property set up or missing required properties. Error: " + gwce.LocalizedMessage)
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
          "The Bank/Lockbox batch job has failed due to the integration database either being not property set up or missing required properties. Error: " + gwce.LocalizedMessage)
      throw gwce
    } catch (ex:Exception) {
      _logger.error("TDIC_BankLockboxFileDownloadBatch#checkInitialConditions() - Exception= " + ex.LocalizedMessage)
      EmailUtil.sendEmail(_failureNotificationEmail,EMAIL_SUBJECT_FAILURE,"BankLockboxFileDownload batch job failed due to error:"+ex.LocalizedMessage)
      throw ex
    }
  }

  /**
   * Checks done directory for matching file names. This ensures that no duplicate lockbox files are processed.
   */
  @Param("fileName", "A String containing the file name to check")
  @Returns("A boolean indicating if the file name shows up in the done directory as a previously completed file")
  @Throws(GWConfigurationException, "If there are setup issues with the local server, such as a non-existent directory path")
  private function isPreviouslyCompletedFile(fileName : String) : boolean {
    _logger.debug("TDIC_BankLockboxBatch#isPreviouslyCompletedFile - Entering")
    var doneDirectory = new File(_donePath)
    if (!doneDirectory.exists()) {
      _logger.info("TDIC_BankLockboxBatch#isPreviouslyCompletedFile - Completed files directory does not exist: " + _donePath)
    }
    else {
      for (completedFile in doneDirectory.listFiles()) {
        if (completedFile.Name.equalsIgnoreCase(fileName)) {
          _logger.info("TDIC_BankLockboxBatch#isPreviouslyCompletedFile - Existing file: " + fileName + ". Skipping file.")
          return true
        }
      }
    }
    _logger.debug("TDIC_BankLockboxBatch#isPreviouslyCompletedFile - File does not exist: " + fileName)
    return false
  }

}

