package tdic.bc.integ.plugins.bankeft.helper.pymntrequest

uses com.tdic.util.properties.PropertyUtil
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.BankEFTFile
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestWritableBean
uses tdic.bc.integ.plugins.bankeft.helper.BankEFTDelimitedFileImpl
uses gw.plugin.Plugins
uses gw.plugin.util.IEncryption
uses gw.xml.XmlElement
uses java.lang.Exception
uses gw.api.gx.GXOptions
uses java.util.List
uses org.slf4j.LoggerFactory

/**
 * US568 - Bank/EFT Integration
 * 01/05/2015 Alvin Lee
 *
 * Helper class with methods to add lines to Bank/EFT flat file.
 */
class TDIC_BankEFTHelper {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_EFT")

  public static final var _vendorSpec : String = PropertyUtil.getInstance().getProperty("EFTVendorSpec")

  /**
   * Helper function to map a payment request entity to the GX model for the writable bean so it can be saved to the database.
   */
  @Param("paymentRequest", "The PaymentRequest entity to map to the GX model")
  @Returns("The GX Model representation of the payment request writable bean")
  public static function mapPaymentRequestToWritableBeanModel(paymentRequest : PaymentRequest) : tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.PaymentRequestWritableBean {
    _logger.debug("TDIC_PaymentRequestHelper#mapPaymentRequestToWritableBeanModel - Entering")

    //Build XML Model options object
    var options = new GXOptions()
    options.Incremental = false // Send all values, not just changed ones
    options.Verbose = true // Send all fields, even nulls

    //Build payment request writable bean object
    var gxPaymentRequest = new PaymentRequestWritableBean()
    gxPaymentRequest.PublicID = paymentRequest.PublicID
    gxPaymentRequest.AccountNumber = paymentRequest.Account.AccountNumber
    gxPaymentRequest.InvoiceNumber = paymentRequest.Invoice.InvoiceNumber
    gxPaymentRequest.Amount = paymentRequest.Amount
    gxPaymentRequest.DraftDate = paymentRequest.DraftDate as String
    gxPaymentRequest.DueDate = paymentRequest.DueDate as String
    gxPaymentRequest.BankName = paymentRequest.PaymentInstrument.BankName_TDIC
    gxPaymentRequest.BankRoutingNumber = paymentRequest.PaymentInstrument.BankABARoutingNumber_TDIC
    var encryptionPlugin = Plugins.get("EncryptionByAESPlugin") as IEncryption
    gxPaymentRequest.BankAccountNumber = encryptionPlugin.encrypt(paymentRequest.PaymentInstrument.BankAccountNumber_TDIC)
    var primaryContact = paymentRequest.Account.PrimaryPayer.Contact
    if (primaryContact typeis Company) {
      gxPaymentRequest.ContactName = primaryContact.Name
    }
    else if (primaryContact typeis Person) {
      gxPaymentRequest.ContactName = primaryContact.LastName
    }

    return new tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.PaymentRequestWritableBean(gxPaymentRequest, options)
  }

  /**
   * Converts data to the lines that can be written to the destination flat file.
   */
  @Param("flatFileData", "The BankEFTFile object containing the data to write to the destination flat file")
  @Returns("A List<String> containing the liens to be written to the destination flat file")
  @Throws(Exception, "If there are any unexpected errors encoding the data")
  public static function convertDataToFlatFileLines(flatFileData : BankEFTFile) : List<String> {
    var gxModel = new tdic.bc.integ.plugins.bankeft.gxmodel.bankeftfilemodel.BankEFTFile(flatFileData)
    _logger.debug("TDIC_PaymentRequestHelper#convertDataToFlatFileLines - Bank/EFT GX Model Generated: ")
    _logger.debug(gxModel.asUTFString())
    var xml = XmlElement.parse(gxModel.asUTFString())
    var df = new BankEFTDelimitedFileImpl()
    return df.encode(_vendorSpec, xml)
  }

}