package tdic.bc.config.payment

uses gw.api.web.payment.ReturnPremiumAllocationStrategy

class BilledDateFirstToLastReturnPremiumAllocationStrategy extends BilledDateFirstToLastAllocationStrategy
    implements ReturnPremiumAllocationStrategy {

  override property get TypeKey() : ReturnPremiumAllocateMethod {
    return ReturnPremiumAllocateMethod.TC_BILLEDDATEFIRSTTOLAST_TDIC;
  }
}
