package tdic.bc.common.batch.bankeft

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses java.sql.Connection
uses java.sql.ResultSet
uses java.lang.Exception
uses gw.api.system.server.ServerUtil
uses gw.pl.exception.GWConfigurationException
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.BankEFTFile
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestBeanInfo
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestWritableBean
uses tdic.bc.integ.plugins.bankeft.helper.pymntrequest.TDIC_BankEFTHelper
uses java.sql.SQLException
uses java.io.File
uses org.apache.commons.io.FileUtils
uses java.io.IOException
uses com.tdic.util.misc.FtpUtil
uses com.tdic.util.misc.EmailUtil
uses gw.plugin.Plugins
uses gw.plugin.util.IEncryption
uses java.math.BigDecimal
uses org.slf4j.LoggerFactory

/**
 * Custom batch job for the Bank/EFT integration to upload the flat files to be sent to the bank.
 */

class TDIC_BankEFTUploadBatchJob extends BatchProcessBase {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_EFT")

  /**
   * Name of the Bank EFT flat file, hardcoded as this is not likely to change, unless the entire system changes (value: INPUT.DAT).
   */
  public static final var FILENAME : String = "INPUT.DAT"

  /**
   * Key for looking up the file output directory from the integration database (value: eft.outputdir).
   */
  public static final var OUTPUT_DIR_PATH : String = "eft.outputdir"

  /**
   * Key for looking up the file output directory from the integration database (value: eft.outputdir).
   */
  public static final var ARCHIVEOUTPUT_DIR_PATH : String = "eft.archiveoutputdir"

  /**
   * Key for looking up the eft file created email addresses from the properties file (value: PaymentNotificationEmail).
   */
  public static final var FILE_NOTIFICATION_EMAIL_KEY : String = "EFTFileNotificationEmail"

  /**
   * Key for looking up the notification email addresses from the integration database (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Name of the vendor for purposes of connecting to the FTP (value: BofaOutgoingACH).
   */
  public static final var VENDOR_NAME : String = "BofaOutgoingACH"

  /**
   * Absolute directory path/string to use to write files.
   */
  private var _destinationDirectoryPath : String = null

  /**
   * Absolute directory path/string to use to save processed files.
   */
  private var _archiveDestinationDirectoryPath : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Notification email addresses in case of eft outbound file succesfully created.
   */
  private var _fileNotificationEmail : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for Bank/EFT batch job completion.
   */
  public static final var EMAIL_SUBJECT_COMPLETED: String = "Bank/EFT Upload Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Bank/EFT batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Bank/EFT Upload Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * The Source File to be uploaded.
   */
  private var _sourceFile : File = null

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_BANKEFTUPLOADBATCHPROCESS)
  }

  /**
   * Runs the actual batch process.
   */
  override function doWork() {
    _logger.info("TDIC_BankEFTUploadBatchJob#doWork() - Entering")
    if (checkInitialConditions_TDIC()) {
      try {
        // Check TerminateRequested flag before proceeding with each entry
        if (TerminateRequested) {
          _logger.info("TDIC_BankEFTUploadBatchJob#doWork() - Terminate requested during doWork() method.")
          return
        }

        _logger.debug("TDIC_BankEFTUploadBatchJob#doWork() - Source file retrieved: ${_sourceFile.AbsolutePath}")

        // Upload file to the Bank of America FTP server
        _logger.debug("TDIC_BankEFTUploadBatchJob#doWork() - Uploading file to Bank of America FTP server")
        if (ScriptParameters.EnableEFTFileFTPUpload && !FtpUtil.uploadFile(VENDOR_NAME, _sourceFile)) {
          throw new Exception("Error uploading file to FTP.  Check integration log for stack trace.")
        }

        var archiveFile = new File(_archiveDestinationDirectoryPath + "/" + FILENAME + "-" + Date.CurrentDate.Time)
        FileUtils.copyFile(_sourceFile, archiveFile)
        FileUtils.deleteQuietly(_sourceFile)

        EmailUtil.sendEmail(_fileNotificationEmail, EMAIL_SUBJECT_COMPLETED, "The Bank/EFT Upload batch process has completed successfully with no errors.")
      } catch (sqle : SQLException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_BankEFTUploadBatchJob#doWork() - Database connection error while attempting to access integration database table", sqle)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, "Bank/EFT batch job failed on Server: " + gw.api.system.server.ServerUtil.ServerId, "The Bank/EFT batch job has failed due to a database connection error while attempting to access the integration database: " + sqle.LocalizedMessage)
        }
      } catch (e : Exception) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_BankEFTUploadBatchJob#doWork() - Error processing output file: " + e.LocalizedMessage, e)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, "Bank/EFT batch job failed on Server: " + gw.api.system.server.ServerUtil.ServerId, "The Bank/EFT batch job has failed due to an unexpected error while processing the output file: " + e.LocalizedMessage)
        }
      }
    }
    _logger.info("TDIC_BankEFTUploadBatchJob#doWork() - Finished processing")
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are payment requests to process into a flat file.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  function checkInitialConditions_TDIC() : boolean {
    _logger.debug("TDIC_BankEFTUploadBatchJob#checkInitialConditions() - Entering")

    var dbUrl : String
    try {
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }

      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _fileNotificationEmail = PropertyUtil.getInstance().getProperty(FILE_NOTIFICATION_EMAIL_KEY)
      if (_fileNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FILE_NOTIFICATION_EMAIL_KEY + "' from properties file.")
      }

      _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
      if (_failureNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _destinationDirectoryPath = PropertyUtil.getInstance().getProperty(OUTPUT_DIR_PATH)
      if (_destinationDirectoryPath == null) {
        throw new GWConfigurationException("Cannot retrieve file output path with the key '" + OUTPUT_DIR_PATH
            + "' from integration database.")
      }

      //Retrieve the Archive Location
      _archiveDestinationDirectoryPath = PropertyUtil.getInstance().getProperty(ARCHIVEOUTPUT_DIR_PATH)
      if (_archiveDestinationDirectoryPath == null) {
        throw new GWConfigurationException("Cannot locate the output path with the key '" + ARCHIVEOUTPUT_DIR_PATH)
      }

      // Retrieve the file to be uploaded
      _sourceFile = new File(_destinationDirectoryPath + "/" + FILENAME)
      if (_sourceFile == null) {
        throw new GWConfigurationException("Cannot retrieve the File: " + FILENAME
            + " from the Directory: " + _destinationDirectoryPath)
      }
    } catch (gwce : GWConfigurationException) {
      // Send email notification of failure
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
          "The Bank/EFT Upload batch job has failed due to the integration database either being not property set up or missing required properties.")
      throw gwce
    }
    return true
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   *
   * @return A boolean to indicate that the process can be stopped.
   */
  override function requestTermination() : boolean {
    _logger.info("TDIC_BankEFTBatchJob#requestTermination() - Terminate requested for batch process.")
    // Set TerminateRequested to true by calling super method
    super.requestTermination()
    return true
  }
}