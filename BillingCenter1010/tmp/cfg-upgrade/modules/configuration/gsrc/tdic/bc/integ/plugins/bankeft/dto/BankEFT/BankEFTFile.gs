package tdic.bc.integ.plugins.bankeft.dto.BankEFT

uses java.util.ArrayList
uses java.text.SimpleDateFormat
uses java.math.BigInteger
uses java.lang.Math
uses java.util.List

/**
 * A Gosu class to contain the fields needed for the GX model when generating the flat file.
 */
class BankEFTFile {

  /**
   * The List of payment requests to write to the flat file.
   */
  var _paymentRequests : List<PaymentRequestWritableBean> as PaymentRequests

  /**
   * Adds to the list of payment requests.
   */
  @Param("bean", "The PaymentRequestWritableBean to add to the list of payment requests")
  public function addToPaymentRequests(bean : PaymentRequestWritableBean) {
    if (_paymentRequests == null) {
      _paymentRequests = new ArrayList<PaymentRequestWritableBean>()
    }
    _paymentRequests.add(bean)
  }

  /**
   * Gets the Due Date from the very first payment request, as long as a single payment request exists.
   * This should be the same across all payment requests in a single flat file.
   */
  @Returns("A formatted String containing the Due Date of the very first payment request")
  property get DueDate() : String {
    if (_paymentRequests == null || _paymentRequests.Empty) {
      return null
    }
    var dueDate = new SimpleDateFormat("yyyy-MM-dd").parse(_paymentRequests.first().DueDate)
    return new SimpleDateFormat("yyMMdd").format(dueDate)
  }

  /**
   * Gets the total number payment requests to be written to the flat file.
   */
  @Returns("An int for the total number payment requests")
  property get TotalNumPaymentRequests() : int {
    return _paymentRequests?.Count
  }

  /**
   * Gets the total number of blocks needed to write the flat file, based on the total number of lines, which amounts
   * to the total number payment requests plus 2 header lines and 2 trailer lines.  One block is needed per 10 lines,
   * and any additional portion of 10 lines needs a block as well.
   */
  @Returns("An int for the total number of blocks needed to write the flat file")
  property get TotalNumBlocks() : int {
    if (_paymentRequests == null) {
      // Need at least one block for the header lines
      return 1
    }
    // Add 4 to the total number of payment requests, for the 2 header lines and the 2 trailer lines
    return Math.ceil((TotalNumPaymentRequests+4) / 10.0) as int
  }

  /**
   * Gets the total payment amount of all payment requests to be written to the flat file.  Note that all payment
   * amounts in the flat file are already multiplied by 100 to avoid any decimals.
   */
  @Returns("A BigInteger for the total payment amount of all payment requests")
  property get TotalAmount() : BigInteger {
    if (_paymentRequests == null) {
      return BigInteger.ZERO
    }
    return _paymentRequests.sum( \ bean -> bean.Amount).longValue()
  }

  /**
   * Gets the sum of all routing number values.  Note that the routing number value does not include the 9th digit,
   * which is the check digit.  This is used to calculate the entry hash in the trailer lines of the flat file.
   *
   */
  @Returns("A BigInteger for the sum of all routing number values")
  property get SumOfRoutingNumbers() : BigInteger {
    var sum = BigInteger.ZERO
    if (_paymentRequests == null) {
      return sum
    }
    for (bean in _paymentRequests) {
      var routingWithoutCheckDigit = new BigInteger(bean.BankRoutingNumber.substring(0, 8))
      sum = sum.add(routingWithoutCheckDigit)
    }
    return sum
  }
}