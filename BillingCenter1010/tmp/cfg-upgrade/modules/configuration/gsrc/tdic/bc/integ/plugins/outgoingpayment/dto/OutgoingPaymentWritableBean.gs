package tdic.bc.integ.plugins.outgoingpayment.dto

uses com.tdic.util.database.WritableBeanBaseImpl
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper

uses java.math.BigDecimal

/**
 * US570 - Lawson Outgoing Payments Integration
 * 11/24/2014 Alvin Lee
 *
 * Class holding values that will be written to the external table to be forwarded to the Lawson system for outgoing payments.
 */
class OutgoingPaymentWritableBean extends WritableBeanBaseImpl {

  /**
   * The name of the account associated with the transaction.
   */
  private var _accountName : String as AccountName

  /**
   * The account number associated with the outgoing payment.
   */
  private var _accountNumber: String as AccountNumber

  /**
   * The amount of the outgoing payment.
   */
  private var _amount : BigDecimal as Amount

  /**
   * The due date of the disbursement.  This is not necessarily the same date as the issue date since disbursements at
   * TDIC only get issued once per week.
   */
  private var _dueDate : String as DueDate

  /**
   * Line of business.
   */
  private var _lob : String as LOB

  /**
   * The policy number.
   */
  private var _policyNumber : String as PolicyNumber

  /**
   * The state code of the policy.
   */
  private var _stateCode : String as StateCode

  /**
   * The vendor number of the contact to which the payment is made.
   */
  private var _vendorNumber : String as VendorNumber

  /**
   * Default constructor.
   */
  construct() {
    super(new OutgoingPaymentBeanInfo())
  }

  /**
   * A unique number for each line in the outgoing payment flat files.
   */
  private var _sequenceNumber : int as SequenceNumber

  /**
   * The mapped T-Account code, which should be the same for all entries within the flat file.
   */
  private var _mappedTAccountCode : String as MappedTAccountCode

  private static final var ACCOUNT_UNIT = "000.00.00"

  /**
   * Gets the Account Unit based on the line of business and jurisdiction.
   */
  property get AccountUnit() : String {
    //GWPS-942 i.e., for Premium Refunds the Acount Unit Key should not be BaLSheet,No-State,No-LOB but should be 000.00.00
    return ACCOUNT_UNIT//TDIC_GLIntegrationHelper.getMappedAccountUnit(CompanyAccountCode_TDIC.TC_BALSHEET, _lob, _stateCode)
  }

  /*
   * Formats the policy number if it is not null; otherwise, return account number if it is not null; otherwise, return
   * a blank string.
   */
  property get AccountOrFormattedPolicyNumber() : String {
    if (_policyNumber != null) {
      return _stateCode + this.LOBCode + _policyNumber
    }
    else if (_accountNumber != null) {
      return _accountNumber
    }
    else {
      return ""
    }
  }

  property get LOBCode(): String {
    var lobCode = ""
    if(_lob.HasContent){
      if(_lob.startsWithIgnoreCase("PL")){
        lobCode = lobCode + "PL"
      }else if (_lob.startsWithIgnoreCase("BOP")){
        lobCode = lobCode + "CP"
      }else{
        lobCode = lobCode + "WC"
      }
    }else{
      lobCode = lobCode + "WC"
    }
    return lobCode
  }

  property get PremiumRefund(): String {
    return this.LOBCode + "_PREMIUM_REFUND"
  }

  property get InvPremiumRefund(): String {
    return this.LOBCode + " PREMIUM REFUND"
  }

}