package tdic.bc.config.enhancement

uses java.math.BigDecimal
uses gw.api.system.BCLoggerCategory
uses tdic.bc.config.invoice.InvoiceUtil
uses java.util.ArrayList
uses java.util.List
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.hpexstream.util.TDIC_BCExstreamHelper

enhancement TDIC_InvoiceEnhancement: entity.Invoice {
  /**
   * US669
   * 02/03/2015 Shane Murphy
   *
   * Total the individual invoice items paid amount for the invoice
   */
  property get TotalPaid(): BigDecimal {
    var total: BigDecimal = 0.0
    this.InvoiceItems.each(\elt -> {
      total += elt.PaidAmount_amt
    })
    return total
  }

  /**
   *
   * Return OCR Barcode with current invoice amounts
   */
  property get OCRBarCode_TDIC(): String {
    return tdic.bc.config.invoice.TDIC_InvoicePURHelper.generateOCRBarCode(this)
  }

  /**
   *
   * Return Invoice's context
   */
  property get InvoiceContext_TDIC(): String {
    return this.InvoiceItems?.first()?.Charge.BillingInstruction.Subtype.Code
  }

  /**
   *  Check if status changed to "Billed" if not return FALSE
   *  If TRUE check for invoice due threshold amount is > 5 after payments distributed from Unapplied fund bucket
   *  If threshold amount satisfied then this invoice needs document to be requested
   *  GWPS-2660 : Suppress invoice when the current balance - CurrentBalance_TDIC of the period is negative
   */
  // GBC-3160 - BC Docs - Invoice - Line item details are showing negative and balance incorect
  property get ShouldRequestInvoiceDocument(): Boolean {
    var requestDoc: Boolean
    var logger = LoggerFactory.getLogger("Application.Invoice")
    var baseDistItems = gw.transaction.Transaction.Current.InsertedBeans.whereTypeIs(entity.BaseDistItem)
        ?.where( \ elt -> elt.InvoiceItem.Invoice.InvoiceNumber == this.InvoiceNumber)
    var invoiceItemsPaidAmount = baseDistItems*.GrossAmountToApply_amt.sum()
    logger.info("Invoice: ${this.InvoiceNumber}; AmountBilledOrDue_TDIC:${this.InvoiceStream.AmountBilledOrDue_TDIC}; Invoice Amount: ${this.Amount_amt}; Invoice Amount Due: ${this.AmountDue_amt}; Amount Distributed from unapplied amount bucket: ${invoiceItemsPaidAmount}")
    requestDoc = ((this?.AmountDue != null) and (this?.AmountDue > InvoiceUtil.InvoiceThreshold)
                and (this.Amount_amt!=0 or this.AmountDue_amt != 0) and this.InvoiceStream.CurrentBalance_TDIC.IsPositive)
    return requestDoc
  }

  property get ShouldRequestInvoiceResendDocument(): Boolean {
    var requestDoc: Boolean
    var logger = LoggerFactory.getLogger("Application.Invoice")
    logger.info("Invoice: ${this.InvoiceNumber}; AmountBilledOrDue_TDIC:${this.InvoiceStream.AmountBilledOrDue_TDIC}; AmountDue_TDIC: ${this.InvoiceStream.AmountDue_TDIC}")
    if(this.Status == InvoiceStatus.TC_BILLED){
      requestDoc = (this.InvoiceStream.AmountBilledOrDue_TDIC  > InvoiceUtil.InvoiceThreshold)
    }else if(this.Status == InvoiceStatus.TC_DUE){
      requestDoc = (this.InvoiceStream.AmountDue_TDIC > InvoiceUtil.InvoiceThreshold)
    }else{
      requestDoc = false
    }
    return requestDoc
  }

  /**
   * Is the invoice a final audit invoice.  A final audit invoice is only used when the only positive
   * charge(s) are for audits.
   */
  property get IsFinalAuditInvoice_TDIC() : boolean {
    var retval : Boolean;
    var auditInvoiceItems = this.InvoiceItems.where( \ elt -> elt.Charge.BillingInstruction typeis Audit)
    if(auditInvoiceItems.HasElements){
      if(auditInvoiceItems.sum( \ elt -> elt.Amount_amt) > 0){
        retval = true
      }else{
        retval = false
      }
    }else{
      retval = false
    }
    return retval;
  }


  //20180529 TJT GW-3116: Generating Payment Schedule Previews that appear on printed invoices.
  // This file was derived from the ChangePaymentPlanPopup.pcf
  // Family of files:
  //  TDIC_InvoiceItemPreview.gs
  //  TDIC_PaymentPlanPreview.gs
  //  TDIC_InvoiceEnhancement.gsx (this file)
  //  TDIC_BCInvoiceModel.gx

  public property get InstallmentPreview_TDIC() : tdic.bc.config.invoice.TDIC_InvoiceItemPreview[] {
    var tmpPolicyPeriod : PolicyPeriod = this.InvoiceStream.PolicyPeriod_TDIC //FIXME TJT this is a horrible way to derive the PolicyPeriod
    var redistributePayments = tmpPolicyPeriod.AgencyBill ? gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseOnly : gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
    var itemFilterType : typekey.InvoiceItemFilterType = InvoiceItemFilterType.TC_ALLITEMS
    var includeDownPaymentItems : boolean = true
    var tmpTDIC_InstallmentInvoiceItemPreviews = new ArrayList<tdic.bc.config.invoice.TDIC_InvoiceItemPreview>()

    // OOTB: for (eachEligiblePlan in gw.api.web.plan.PaymentPlans.findEligibleVisiblePaymentPlansForPolicyPeriod(this)){
    for (eachEligiblePlan in tmpPolicyPeriod.AvailablePaymentPlans){
      // is this the first invoice in the term then preview all plans; if this isn't the first invoice in the term then preview only the currently selected plan
      if ((this == tmpPolicyPeriod.Invoices.minBy(\i -> i.EventDate)) or ((this != tmpPolicyPeriod.Invoices.minBy(\i -> i.EventDate)) and (tmpPolicyPeriod.PaymentPlan == eachEligiblePlan)) ){
        // reset the Invoices List for the next iteration of Payment Plan
        var tmpTDIC_InvoiceItemPreviews : List<tdic.bc.config.invoice.TDIC_InvoiceItemPreview> = new ArrayList<tdic.bc.config.invoice.TDIC_InvoiceItemPreview>()
        // simulate the Invoices for this Payment Plan
        var changer = new gw.api.domain.invoice.PaymentPlanChanger(tmpPolicyPeriod, eachEligiblePlan, redistributePayments, gw.invoice.InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
        var entries : java.util.List<gw.api.domain.invoice.ChargeInstallmentChanger.Entry> = changer.InstallmentPreview
        // transform the simulated Entries (Invoice Items) into a List of Invoices
        for (eachEntry in changer.InstallmentPreview){
          if (eachEntry.DisplayName == "Added"){
            var tmpTDIC_InvoiceItemPreview_toUpdate = tmpTDIC_InvoiceItemPreviews.firstWhere( \ elt -> elt.EventDate == eachEntry.EventDate )// .InvoiceItem.EventDate)
            if (tmpTDIC_InvoiceItemPreview_toUpdate == null){
              tmpTDIC_InvoiceItemPreviews.add(new tdic.bc.config.invoice.TDIC_InvoiceItemPreview(eachEntry,eachEligiblePlan.Name, tmpPolicyPeriod.PaymentPlan == eachEligiblePlan ? true : false))
            } else {
              // if there is already an invoice in the List for this Event Date, then combine the amount values
              tmpTDIC_InvoiceItemPreview_toUpdate.updateInstallment(eachEntry)
            }
          }
        }
        // create the Payment Plan Preview, and load its Invoices
        var tmpTDIC_PaymentPlanPreview : tdic.bc.config.invoice.TDIC_PaymentPlanPreview = new tdic.bc.config.invoice.TDIC_PaymentPlanPreview(eachEligiblePlan.Name, tmpPolicyPeriod.PaymentPlan == eachEligiblePlan ? true : false, tmpTDIC_InvoiceItemPreviews?.toTypedArray())
        // set the installment numbers on the Invoices
        tmpTDIC_PaymentPlanPreview.setInstallmentNumbers()
        // load the List of Invoices onto the Installment invoice item previews list
        tmpTDIC_InstallmentInvoiceItemPreviews.addAll(tmpTDIC_PaymentPlanPreview.TDIC_InvoiceItemPreviews?.toList())
      }
    }
    // return the collection of Payments Plans (each has a collection of Invoices)
    return tmpTDIC_InstallmentInvoiceItemPreviews?.toTypedArray()
  }

  /**
   * US644
   * 11/28/2014 shanem
   *
   * Get template Ids for event name and create appropriate documents on the PolicyPeriod
   */
  @Param("eventName", "Event Name to create documents for")
  function createDocumentsForEvent(eventName: String, aPolicyPeriod:PolicyPeriod=null) {
    var _exstreamLogger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Entering.")
    var bcHelper = new TDIC_BCExstreamHelper()

    var acct = this.InvoiceStream.PolicyPeriod_TDIC.Account
    acct = gw.transaction.Transaction.getCurrent().add(acct)

    //bcHelper.createDocumentStubsForInvoice(eventName, acct, this)
    bcHelper.createDocumentStubs(eventName, acct, this.InvoiceStream.PolicyPeriod_TDIC)
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Notifying Exstream Message Queue.")
    this.addEvent(eventName)
    //acct.addEvent(eventName)
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Exiting.")
  }

}
