package tdic.bc.common.batch.eftreversal.helper

uses gw.api.database.Query
uses gw.webservice.bc.bc1000.PaymentAPI
uses java.math.BigDecimal
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory
uses tdic.payment.DBPaymentReversalHelper

/**
 * US1126 - CR - EFT/ACH Reversal Automation
 * 02/05/2015 Vicente
 *
 * Class that extends NACHAOperation. Store the invoice number and return code for a payment reverse requests.
 */
class PaymentReverseOperation extends NACHAOperation{

  /**
   * Return Code
   */
  private var _returnCode : PaymentReversalReason as ReturnCode

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("EFT_REVERSAL")

  /**
   * Reference to PaymentAPI
   */
  private static final var _paymentAPI = new PaymentAPI()

  /**
   * Reference to PaymentAPI
   */
  private static final var _prenotePrefix = "bc:"

  /**
   * Constructor
   */
  @Param("amount", "Amount related to the payment.")
  @Param("invoiceNumber", "Inovice number related with the payment.")
  @Param("returnCode", "Return code that indicates the reversal reason")
  construct(amount : BigDecimal, invoiceNumber: String, returnCode: PaymentReversalReason) {
    _amount = amount
    _invoiceNumber = invoiceNumber
    _returnCode = returnCode
  }

  /**
   * Process payment reverse operations. Payment is reversed using the return code as reversal reason. If return code
   * is R02, R03, R04, R08, R10, R13, or R16, an activity to is created to indicate that the client has to be
   * removed from APW.
   */
  @Param("paymentReverseOperation", "The PaymentReverseOperation object containing the data to reverse the payment.")
  @Throws(EFTReversalValidationException, "If the payment has been already reversed")
  function process()  : EFTPaymentDTO {
    _logger.debug("PaymentReverseOperation#process - Entering")
    var directBillMoneyRcvd : DirectBillMoneyRcvd
    var eftPaymentDTO = new EFTPaymentDTO()
    if(this.InvoiceNumber.startsWith(_prenotePrefix)){
      try {

        var paymentInstrumentQuery : Query
        paymentInstrumentQuery = gw.api.database.Query.make(PaymentInstrument)
        paymentInstrumentQuery.compare(PaymentInstrument#PublicID, Equals, this.InvoiceNumber)
        var results = paymentInstrumentQuery.select()
        if (results != null && results?.HasElements) {
          eftPaymentDTO.IsPrenoteReturn = Boolean.TRUE
          eftPaymentDTO.InvoiceNumber = this.InvoiceNumber
          eftPaymentDTO.Amount = this.Amount.ofDefaultCurrency()
          eftPaymentDTO.ReasonCode = this.ReturnCode.Description
          eftPaymentDTO.AccountNumber = (results.first() as PaymentInstrument)?.Account?.AccountNumber
          eftPaymentDTO.PayeeName = (results.first() as PaymentInstrument)?.Account?.AccountName
          eftPaymentDTO.PolicyNumber = "N/A"
        } else {
          eftPaymentDTO.IsPrenoteReturn = Boolean.TRUE
          eftPaymentDTO.InvoiceNumber = this.InvoiceNumber
          eftPaymentDTO.Amount = this.Amount.ofDefaultCurrency()
          eftPaymentDTO.ReasonCode = this.ReturnCode.Description
          eftPaymentDTO.AccountNumber = "Not Found"
          eftPaymentDTO.PayeeName = "N/A"
          eftPaymentDTO.PolicyNumber = "N/A"
        }
      } catch (ex: Exception){
        eftPaymentDTO.IsPrenoteReturn = Boolean.TRUE
        eftPaymentDTO.InvoiceNumber = this.InvoiceNumber
        eftPaymentDTO.Amount = this.Amount.ofDefaultCurrency()
        eftPaymentDTO.ReasonCode = this.ReturnCode.Description
        eftPaymentDTO.AccountNumber = "Not Found"
        eftPaymentDTO.PayeeName = "N/A"
        eftPaymentDTO.PolicyNumber = "N/A"
        eftPaymentDTO.Description = ex.Message
      }
      return eftPaymentDTO
    }

    try {
      directBillMoneyRcvd = TDIC_EFTReversalAndChangeNotificationsBatchJobHelper.getDirectBillMoneyRcvdWithInvoiceNumber(this.Amount, this.InvoiceNumber)
    }catch(ex:EFTReversalValidationException){
      _logger.error("PaymentReverseOperation#process - Payment reversal failed for Invoice : " + this.InvoiceNumber + " Amount :" + this.Amount + " ReasonCode :" + this.ReturnCode)
      _logger.error("Exception reversing payment : " + ex.StackTraceAsString)
      eftPaymentDTO.PaymentReversed = false
      eftPaymentDTO.ReversalDate = Date.CurrentDate
      eftPaymentDTO.Description = ex.Message
      eftPaymentDTO.Amount = this.Amount.ofDefaultCurrency()
      eftPaymentDTO.ReasonCode = this.ReturnCode.Description
      return eftPaymentDTO
    }
      // Reverse the payment if it has not already been reversed.
      if(!directBillMoneyRcvd.Reversed){
        _logger.debug("PaymentReverseOperation#process - Payment to be reversed for Invoice number " + this.InvoiceNumber
            + " with the Return Code " + this.ReturnCode)
        try{
          _paymentAPI.reverseDirectBillPayment(directBillMoneyRcvd.PublicID, this.ReturnCode)
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            // SN: GBC-3013 - Need to turn off NSF/Payment Reversal Fee.
            /*var dbPaymentReversalHelper = new DBPaymentReversalHelper()
            if (not dbPaymentReversalHelper.isPaymentReversalFeeCreatedForToday(directBillMoneyRcvd)) {
              dbPaymentReversalHelper.createReversalFees(directBillMoneyRcvd, this.ReturnCode)
            }*/
            eftPaymentDTO.PaymentReversed = true
            eftPaymentDTO.TxnNumber = directBillMoneyRcvd.MoneyReceivedTransaction?.TransactionNumber
            eftPaymentDTO.PayeeName = directBillMoneyRcvd.UnappliedFund?.PolicyPeriod_TDIC?.Contacts?.firstWhere(\c -> c.Roles*.Role.contains(TC_PRIMARYINSURED))?.Contact?.Name
            eftPaymentDTO.AccountNumber = directBillMoneyRcvd.Account.AccountNumber
            eftPaymentDTO.ReversalDate = Date.CurrentDate
            eftPaymentDTO.Description = this.ReturnCode.Description
            eftPaymentDTO.Amount = directBillMoneyRcvd.Amount
            eftPaymentDTO.ReasonCode = this.ReturnCode.Code
            eftPaymentDTO.RoutingNumber = directBillMoneyRcvd.PaymentInstrument.BankABARoutingNumber_TDIC
            eftPaymentDTO.PolicyNumber = directBillMoneyRcvd.UnappliedFund?.PolicyPeriod_TDIC?.PolicyNumber
          })
        }catch(ex:Exception) {
          _logger.error("PaymentReverseOperation#process - Payment reversal failed for Invoice : " + this.InvoiceNumber + " Amount :" + this.Amount + " ReasonCode :" + this.ReturnCode)
          _logger.error("Exception reversing payment : " + ex.StackTraceAsString)
          eftPaymentDTO.PaymentReversed = false
          eftPaymentDTO.TxnNumber = directBillMoneyRcvd.MoneyReceivedTransaction?.TransactionNumber
          eftPaymentDTO.PayeeName = directBillMoneyRcvd.UnappliedFund?.PolicyPeriod_TDIC?.Contacts?.firstWhere(\c -> c.Roles*.Role.contains(TC_PRIMARYINSURED))?.Contact?.Name
          eftPaymentDTO.AccountNumber = directBillMoneyRcvd.Account?.AccountNumber
          eftPaymentDTO.ReversalDate = Date.CurrentDate
          eftPaymentDTO.Description = ex.Message
          eftPaymentDTO.Amount = directBillMoneyRcvd.Amount
          eftPaymentDTO.ReasonCode = this.ReturnCode.Description
          eftPaymentDTO.RoutingNumber = directBillMoneyRcvd.PaymentInstrument?.BankABARoutingNumber_TDIC
          eftPaymentDTO.PolicyNumber = directBillMoneyRcvd.UnappliedFund?.PolicyPeriod_TDIC?.PolicyNumber
        }
      }else{
        _logger.warn("PaymentReverseOperation#process - "
            + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.PaymentAlreadyReversed", this.InvoiceNumber))
        //throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.PaymentAlreadyReversed", this.InvoiceNumber))
        eftPaymentDTO.PaymentReversed = false
        eftPaymentDTO.TxnNumber = directBillMoneyRcvd.MoneyReceivedTransaction?.TransactionNumber
        eftPaymentDTO.PayeeName = directBillMoneyRcvd.UnappliedFund?.PolicyPeriod_TDIC?.Contacts?.firstWhere(\c -> c.Roles*.Role.contains(TC_PRIMARYINSURED))?.Contact?.Name
        eftPaymentDTO.AccountNumber = directBillMoneyRcvd.Account?.AccountNumber
        eftPaymentDTO.ReversalDate = Date.CurrentDate
        eftPaymentDTO.Description = DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.PaymentAlreadyReversed", this.InvoiceNumber)
        eftPaymentDTO.Amount = directBillMoneyRcvd.Amount
        eftPaymentDTO.ReasonCode = this.ReturnCode.Description
        eftPaymentDTO.RoutingNumber = directBillMoneyRcvd.PaymentInstrument?.BankABARoutingNumber_TDIC
        eftPaymentDTO.PolicyNumber = directBillMoneyRcvd.UnappliedFund?.PolicyPeriod_TDIC?.PolicyNumber
      }


    // If it is the first (i.e. non-consecutive) NSF Reversal (Return Code R01), automatically create a second payment request for the same invoice
    /*  09/15/2020 GWPS2510 - APW Payments Requests reversed for Insufficient Funds for prior month requested Invoice Bill Date should not be scheduling again another APW.*/
    /*if (this.ReturnCode == PaymentReversalReason.TC_R01) {
      _logger.debug("PaymentReverseOperation#process - This payment reversal is due to NSF. Checking if a new payment request should be generated.")
      if (!directBillMoneyRcvd.IsConsecutiveAPWPaymentAlsoNSF_TDIC) {
        _logger.debug("PaymentReverseOperation#process - This payment is the first APW NSF reversal. Create new payment request for the same invoice.")
        TDIC_EFTReversalAndChangeNotificationsBatchJobHelper.createPaymentRequestForInvoice(this.InvoiceNumber, directBillMoneyRcvd.Account)
      }
      else {
        _logger.debug("PaymentReverseOperation#process - This payment is a consecutive APW NSF reversal. No new payment request created, as the account will be removed from APW.")
      }
    }*/

    _logger.debug("PaymentReverseOperation#process - Exiting")
    return eftPaymentDTO
  }

}