package tdic.bc.integ.services.check

uses gw.xml.ws.annotation.WsiWebService
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.services.check.dto.TDIC_CheckStatusRequest
uses tdic.bc.integ.services.check.dto.TDIC_CheckStatusResponse
uses tdic.bc.integ.services.check.dto.TDIC_CheckStatusDTO
uses tdic.bc.integ.services.check.dto.TDIC_CheckStatusResponseDTO
uses tdic.bc.integ.services.util.TDIC_WebServiceErrorCodes
uses tdic.bc.integ.services.util.dto.TDIC_ErrorMessageDTO
uses entity.Contact

uses java.math.BigDecimal

@WsiWebService
class TDIC_CheckStatusAPI {

  private static final var _logger = LoggerFactory.getLogger("CHECK_STATUS_UPDATE")
  private static var CLASS_NAME = TDIC_CheckStatusAPI
  private static final var API_USER = "iu"

  //Email
  private static final var EMAIL_TO = PropertyUtil.getInstance().getProperty("CheckStatusBatchNotificationEmail")
  private static final var FAILURE_EMAIL_TO = PropertyUtil.getInstance().getProperty("BCInfraIssueNotificationEmail")
  private static final var FAILURE_SUB = "Failure::${gw.api.system.server.ServerUtil.Env}-Check Status Update Batch Job Failed"
  private static final var SUCCESS_SUB = "Success::${gw.api.system.server.ServerUtil.Env}-Check Status Update Batch Job completed successfully"
  private static final var PARTIAL_SUCCESS_SUB = "PartialSuccess::${gw.api.system.server.ServerUtil.Env}-Check Status Update Batch Job completed successfully with errors and/or warnings"

  //Check Status Typecode mappings
  private static final var MAPPER = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  private static final var TYPELIST = "OutgoingPaymentStatus"
  private static final var NAMESPACE = "tdic:lawson"

  //Check Status Records
  private var _notMatchedRecordsList : List<TDIC_CheckStatusResponseDTO>
  private var _errorCheckStatusRecordsList : List<TDIC_CheckStatusResponseDTO>

  function updateCheckStatus(request : TDIC_CheckStatusRequest) : TDIC_CheckStatusResponse {
    var logPrefix = "${CLASS_NAME}#updateCheckStatus(request)"
    _logger.info("${logPrefix} - start")
    _logger.info("${logPrefix} - ${request}")

    var response = new TDIC_CheckStatusResponse()
    try {
      _errorCheckStatusRecordsList = new ArrayList<TDIC_CheckStatusResponseDTO>()
      //validate Check Status request details
      var validDataCheckStatusRecordsList = validateCheckStatusRequestDetails(request.CheckStatusDTOList)

      //process valid check status records
      processCheckStatusUpdates(validDataCheckStatusRecordsList)

      //update response and send email
      if (_errorCheckStatusRecordsList.HasElements || _notMatchedRecordsList.HasElements) {
        if(_errorCheckStatusRecordsList.HasElements){
          response.CheckStatusResponseDTOList = _errorCheckStatusRecordsList
        }
        EmailUtil.sendEmail(EMAIL_TO, PARTIAL_SUCCESS_SUB, "${buildEmailDescription()}")
      }
      else {
        EmailUtil.sendEmail(EMAIL_TO, SUCCESS_SUB, "Check status update batch process successfully completed.")
      }
    }catch(e : Exception){
      EmailUtil.sendEmail(FAILURE_EMAIL_TO, FAILURE_SUB, "Check status update batch process failed with errors. Please review below error details. \n ${e.StackTraceAsString}")
      _logger.error("${logPrefix} - Check status update process failed with erorrs.", e)
      throw e
    }
    _logger.info("${logPrefix} - ${response}")
    _logger.info("${logPrefix} - end")
    return response
  }

  @Param("paymentRecords","List of OutgoingDisbPmnt records retrtieved for status updates")
  @Param("checkStatusRecords","Check status update records retrieved from Lawson system")
  private function processCheckStatusUpdates(checkStatusRecordsList : List<TDIC_CheckStatusDTO>) {
    var logPrefix = "${CLASS_NAME}#processCheckStatusUpdates(List, Map)"
    _logger.debug("${logPrefix} - start")

    _notMatchedRecordsList = new ArrayList<TDIC_CheckStatusResponseDTO>()

    if(checkStatusRecordsList.HasElements) {
      var paymentRecords = retrieveOutgoingPayments(checkStatusRecordsList*.TransactionRecordID)
      var paymentRecord: OutgoingDisbPmnt
      var errorMessagesList: List<TDIC_ErrorMessageDTO>
      for (checkStatusRecord in checkStatusRecordsList) {
        paymentRecord = paymentRecords.firstWhere(\pr -> pr.PublicID?.equalsIgnoreCase(checkStatusRecord.TransactionRecordID))
        errorMessagesList = new  ArrayList<TDIC_ErrorMessageDTO>()
        if (paymentRecord != null) {
          try {
            var status = MAPPER.getInternalCodeByAlias(TYPELIST, NAMESPACE, checkStatusRecord.CheckStatus?.toUpperCase())
            if (status == null) {
              errorMessagesList.add(new TDIC_ErrorMessageDTO(){:ErrorCode = TDIC_WebServiceErrorCodes.INVALID_CHECK_STATUS_VALUE})
            } else {
              _logger.info("${logPrefix} - Check status update starting for '${paymentRecord.PublicID}'")
              gw.transaction.Transaction.runWithNewBundle(\bundle -> {
                paymentRecord = bundle.add(paymentRecord)
                paymentRecord.Status = typekey.OutgoingPaymentStatus.get(status)
                paymentRecord.RefNumber = checkStatusRecord.CheckNumber
                if (status == OutgoingPaymentStatus.TC_ISSUED as String) {
                  paymentRecord.IssueDate = checkStatusRecord.CheckIssueDate
                } else {
                  paymentRecord.StatusUpdateDate_TDIC = checkStatusRecord.CheckStatusDate
                }
              }, API_USER)
              _logger.info("${logPrefix} - Check status update completed for '${paymentRecord.PublicID}'")
            }
          } catch (e : Exception) {
            errorMessagesList.add(new TDIC_ErrorMessageDTO(){:ErrorCode = TDIC_WebServiceErrorCodes.CHECK_STATUS_UPDATE_FAILED})
          }
        } else {
          //Check not found with TransactionReferenceID in BC
          errorMessagesList.add(new TDIC_ErrorMessageDTO(){:ErrorCode = TDIC_WebServiceErrorCodes.OUTGIONG_PAYMENT_RECORD_NOT_FOUND})
        }

        if (errorMessagesList.HasElements) {
          addToErrorCheckStatusRecords(checkStatusRecord, errorMessagesList)
        }
      }
      //GWPS-184 : Incorrect warning messages are corrected.
      for (checkStatusRecord in checkStatusRecordsList) {
        paymentRecord = paymentRecords.firstWhere(\pr -> pr.PublicID?.equalsIgnoreCase(checkStatusRecord.TransactionRecordID))
        matchRecord(paymentRecord, checkStatusRecord, paymentRecords)
      }
    }
    _logger.debug("${logPrefix} - end")
  }

  private function validateCheckStatusRequestDetails(checkStatusRecordsList : List<TDIC_CheckStatusDTO>): List<TDIC_CheckStatusDTO>{
    var validDataCheckStatusRecordsList = new ArrayList<TDIC_CheckStatusDTO>()
    var errorMessagesList = new ArrayList<TDIC_ErrorMessageDTO>()
    var errorRecord:TDIC_ErrorMessageDTO
    for(checkStatusRecord in checkStatusRecordsList){
      if(isRequiredDataAvailable(checkStatusRecord)){
        validDataCheckStatusRecordsList.add(checkStatusRecord)
      }else {
        //TransactionID is null
        if (!checkStatusRecord.TransactionRecordID.HasContent) {
          errorRecord = new TDIC_ErrorMessageDTO()
          errorRecord.ErrorCode = TDIC_WebServiceErrorCodes.CHECK_TRANSACTION_RECORD_ID_EMPTY
          errorMessagesList.add(errorRecord)
        }

        if (errorMessagesList.HasElements) {
          addToErrorCheckStatusRecords(checkStatusRecord, errorMessagesList)
        }
      }
    }
    return validDataCheckStatusRecordsList
  }

  private function isRequiredDataAvailable(checkStatusRecord: TDIC_CheckStatusDTO):Boolean{
    return (checkStatusRecord!=null && checkStatusRecord.TransactionRecordID.HasContent)
  }

  private function addToErrorCheckStatusRecords(checkStatusRecord: TDIC_CheckStatusDTO, errors: List<TDIC_ErrorMessageDTO>){
    var checkStatusResponseRecord = new TDIC_CheckStatusResponseDTO()
    checkStatusResponseRecord.CheckStatusDTO = checkStatusRecord
    checkStatusResponseRecord.Errors = errors
    _errorCheckStatusRecordsList.add(checkStatusResponseRecord)
  }

  protected function retrieveOutgoingPayments(publicIDs: String[]) : IQueryBeanResult<OutgoingDisbPmnt> {
    var logPrefix = "${CLASS_NAME}#retrieveOutgoingPayments()"
    _logger.debug("${logPrefix}- start")
    var pQuery = Query.make(OutgoingDisbPmnt).compareIn(OutgoingDisbPmnt#PublicID,publicIDs).select()
    _logger.debug("${logPrefix} - end")
    return pQuery
  }

  /*matchRecord method is to validate incomming lawson Payment record against GW Payment.*/
  @Param("payment","OutgoingDisbPmnt Object for status updates")
  @Param("paymentRecords","List of OutgoingDisbPmnt records retrtieved for validation")
  @Param("checkStatusRecord","Check status update records retrieved from Lawson system")
  private function matchRecord(payment: OutgoingDisbPmnt, checkStatusRecord: TDIC_CheckStatusDTO,
                               paymentRecords: IQueryBeanResult<OutgoingDisbPmnt>) {

    var comments = new StringBuilder()
    if(payment.RefNumber != null && payment.RefNumber != checkStatusRecord.CheckNumber)
      comments.append("CheckNumber: ${payment.RefNumber} vs ${checkStatusRecord.CheckNumber} not matching | ")
    //GWPS-184 : Validation is updated to handle scenario where multiple GW payments are merged into single check.
    if(payment.Disbursement.Amount.Amount != checkStatusRecord.CheckAmount) {
      var allPaymentsWithSameCheckNumber = paymentRecords.where(\ outgoingDisbPmntObj ->
          outgoingDisbPmntObj.RefNumber == checkStatusRecord.CheckNumber)
      var amount: BigDecimal = allPaymentsWithSameCheckNumber.sum(\ outGoingPayment -> outGoingPayment.Disbursement.Amount.Amount)
      if(amount != checkStatusRecord.CheckAmount) {
        comments.append("Amount: ${payment.Disbursement.Amount.Amount} vs ${checkStatusRecord.CheckAmount} not matching | ")
      }
    }

    var vendorNumber =  getVendorNumber(payment)
    if(vendorNumber != checkStatusRecord.VendorID)
      comments.append("VendorNumber: ${vendorNumber} vs ${checkStatusRecord.VendorID} not matching | ")

    if(comments.toString().HasContent){
      var noMatchRecord = new TDIC_CheckStatusResponseDTO()
      noMatchRecord.CheckStatusDTO = checkStatusRecord
      noMatchRecord.Comments = comments.toString()
      _notMatchedRecordsList.add(noMatchRecord)
    }
  }

  private function buildEmailDescription() : String {
    var description = new StringBuilder()
    description.append("Check status update batch process completed with Errors and/or Warnings.\n")
    if(_notMatchedRecordsList.HasElements){
      description.append("Check datails (BC OutgoingPayment record vs Lawson check status record) that are not matching are :-\n")
      _notMatchedRecordsList.each( \ record -> {
        description.append("CheckNumber: '${record.CheckStatusDTO.CheckNumber}' -  ${record.Comments}\n")
      })
    }
    if(_errorCheckStatusRecordsList.HasElements){
      description.append("Error Records are:-\n")
      _errorCheckStatusRecordsList.each( \ record -> {
        description.append("Error in processing check status update for the check number: '${record.CheckStatusDTO.CheckNumber}'. Details are: ${record.Comments}\n")
      })
    }
    return description?.toString()
  }

  private function getVendorNumber(disbPayment : OutgoingDisbPmnt) : String {
    var logPrefix = "${CLASS_NAME}#getVendorNumber(OutgoingDisbPmnt)"
    _logger.trace("${logPrefix}- start")
    var vendorNumber : String = null
    // Populate fields for AccountDisbursement (all disbursements are Account Disbursements for Release A)
    if (disbPayment.Disbursement typeis AccountDisbursement) {

      var disbAccount = disbPayment.Disbursement.Account
      var unapplied = disbPayment.Disbursement.UnappliedFund
      var paymentContact : Contact = null
      if (unapplied.Policy != null) {
        vendorNumber = unapplied.Policy.LatestPolicyPeriod.PrimaryInsured.Contact.VendorNumber
      }
      else {
        vendorNumber = disbAccount.PrimaryPayer.Contact.VendorNumber
      }
    }
    _logger.debug("${logPrefix}- Vendor number found for DisbursementPayment ${disbPayment} is ${vendorNumber} ")
    _logger.trace("${logPrefix}- end")
    return vendorNumber
  }
}