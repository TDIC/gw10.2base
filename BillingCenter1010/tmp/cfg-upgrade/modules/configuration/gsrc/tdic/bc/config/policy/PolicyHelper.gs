package tdic.bc.config.policy

uses java.lang.Integer
uses java.util.List
uses gw.api.upgrade.Coercions

class PolicyHelper {
  /**
   * US83
   * 01/07/2015 Vicente
   *
   * Returns the list of available lines of business in TDIC. In Release A is just Workers Compensation
   */
  @Returns("Returns the list of available lines of business in TDIC. In Release A is just Workers Compensation")
  public static function tdicProductLines() : List<typekey.LOBCode> {
    return {LOBCode.TC_WC7WORKERSCOMP, LOBCode.TC_BUSINESSOWNERS, LOBCode.TC_GENERALLIABILITY}
  }

  /**
   * US83
   * 01/13/2015 Vicente
   *
   * Complete the input number with leading zeros if the format correspond with a number of return the same string if not
   */
  @Returns("Complete the input number with leading zeros if the format correspond with a number of return the same string if not")
  public static function completePolicyNumberWithLeadingZeros(number : String) : String {

    var returnPolicyNumber : String
    try {
      returnPolicyNumber  = String.format("%010d", new Integer[]{Coercions.makeIntFrom(number)})
    } catch(e : java.lang.NumberFormatException) {
      returnPolicyNumber = number
    }
    return returnPolicyNumber
  }
}