package tdic.bc.common.logger

uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

/**
 * Logger Enhancement, developers should add their own BillingCenter-specific Logger Categories in this Enhancement.
 */
enhancement BCLoggerCategoryEnhancement : gw.api.system.BCLoggerCategory {

  /**
   * Logger for Bank/EFT plugin integration.
   */
  static property get BANK_EFT() : Logger {
    return LoggerFactory.getLogger("Plugin.BankEFT")
  }

  /**
   * Logger for Bank/Lockbox plugin integration.
   */
  static property get BANK_LOCKBOX() : Logger {
    return LoggerFactory.getLogger("Plugin.BankLockbox")
  }

  /**
   * Logger for Invoicing plugins.
   */
  static property get INVOICING_PLUGINS() : Logger {
    return LoggerFactory.getLogger("Plugin.Invoicing")
  }

  /**
   * Logger for Credit Card Handlers.
   */
  static property get CREDIT_CARD_HANDLER() : Logger {
    return LoggerFactory.getLogger("Api.CreditCardHandler")
  }

  /**
   * Logger for General Ledger integration.
   */
  static property get GENERAL_LEDGER() : Logger {
    return LoggerFactory.getLogger("Plugin.GeneralLedger")
  }

  /**
   * Logger for Outgoing Payment integration.
   */
  static property get OUTGOING_PAYMENT() : Logger {
    return LoggerFactory.getLogger("Plugin.OutgoingPayment")
  }

  /**
   * Logger for Direct Bill Payment plugins.
   */
  static property get DIRECT_BILL_PAYMENT_PLUGIN() : Logger {
    return LoggerFactory.getLogger("Plugin.DirectBillPayment")
  }

  /**
   * Logger for EFT Reversal and Change Notifications plugin integration.
   */
  static property get EFT_REVERSAL() : Logger {
    return LoggerFactory.getLogger("Plugin.EFTReversalChangeNotifications")
  }

  /**
   * Logger for Direct Bill Payment plugins.
   */
  static property get CHECK_STATUS_UPDATE() : Logger {
    return LoggerFactory.getLogger("Batch.TDIC_CheckStatusUpdate")
  }

  /**
   * Logger for PolicyBillingAPI webservice
   */
  static property get POLICY_BILLING_API() : Logger {
    return LoggerFactory.getLogger("Api.TDIC_PolicyBillingAPI")
  }

  /**
   * Logger for the Quarterly Plan Changer Batch
   */
  static property get QUARTERLY_PLAN_CHANGER() : Logger {
    return LoggerFactory.getLogger("Batch.TDIC_QuarterlyPlanChanger")
  }

}
