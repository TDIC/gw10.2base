package tdic.bc.integ.plugins.generalledger.dto

uses java.util.HashMap
uses java.util.Map

/**
 * US570 - General Ledger Integration
 * 1/14/2015 Alvin Lee
 *
 * A class to represent a GL account.  Each unique account (LOB and state combination) will have a group of GL buckets.
 */
class GLAccount {

  /**
   * A Map<String, GLBucketGroup> to connect the unique account unit to a group of GL buckets.
   */
  private var _accountUnitBucketGroupMap : Map<String, GLBucketGroup> as AccountUnitBucketGroupMap

  construct() {
    _accountUnitBucketGroupMap = new HashMap<String, GLBucketGroup>()
  }

}