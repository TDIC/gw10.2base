package tdic.bc.common.batch.checkstatus

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.bc.common.batch.checkstatus.dto.TDIC_CheckStatusRecord
uses com.tdic.util.database.DatabaseParam
uses com.tdic.util.misc.EmailUtil

uses java.io.IOException
uses java.sql.Connection
uses java.sql.ResultSet
uses java.sql.SQLException

uses entity.Contact

/**
 * US566
 * 01/28/2015 Kesava Tavva
 *
 * Implementation of custom batch process for updating Outgoing payment status. Checks initial conditions to run batch process,
 * retrieving outgoing disbursement records, reads check status updates from Lawson datatbase.
 */
/**
 * 01/19/2016 Hermia Kho
 * Replace Batch_User "su" with "iu"
 */
class TDIC_CheckStatusUpdateBatch extends BatchProcessBase {

  private static final var _logger = LoggerFactory.getLogger("CHECK_STATUS_UPDATE")
  private static var CLASS_NAME = "TDIC_CheckStatusUpdateBatch"
  private var _notMatchedRecords : List<TDIC_CheckStatusRecord>
  private var _errorRecords : List<TDIC_CheckStatusRecord>
  private static final var BATCH_USER = "iu"
  private var _emailTo : String as readonly EmailTo
  private var _failureEmailTo : String as readonly FailureEmailTo
  private var _lawsonDBParam : DatabaseParam as readonly ExternalAppDBParam
  private static final var EMAIL_TO_PROP_FIELD = "CheckStatusBatchNotificationEmail"
  private static final var FAILURE_EMAIL_RECIPIENT = "BCInfraIssueNotificationEmail"
  private static final var EXTERNAL_APP_NAME = "Lawson"
  private static final var FAILURE_SUB = "Failure::${gw.api.system.server.ServerUtil.ServerId}-Check Status Update Batch Job Failed"
  private static final var SUCCESS_SUB = "Success::${gw.api.system.server.ServerUtil.ServerId}-Check Status Update Batch Job completed successfully"
  private static final var TERMINATE_SUB = "Terminate::${gw.api.system.server.ServerUtil.ServerId}-Check Status Update Batch Job Terminated"
  private static final var PARTIAL_SUCCESS_SUB = "PartialSuccess::${gw.api.system.server.ServerUtil.ServerId}-Check Status Update Batch Job completed successfully with errors and/or warnings"
  private static final var MAPPER = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  private static final var TYPELIST = "OutgoingPaymentStatus"
  private static final var NAMESPACE = "tdic:lawson"

  construct(){
    super(BatchProcessType.TC_CHECKSTATUSUPDATE_TDIC)
  }

  /**
   * US566
   * 01/28/2015 Kesava Tavva
   *
   * This function override doWork() function of BatchProcessBase and does actual process
   * work of updating outgoingpayment status.
   */
  override function doWork() {
    var logPrefix = "${CLASS_NAME}#doWork()"
    _logger.debug("${logPrefix} - Entering")
    try {
      if (TerminateRequested) {
        _logger.warn("TDIC_CheckStatusUpdateBatch#doWork() - Terminate requested during doWork() method(before retrieveOutgoingPayments()).")
        EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Check status update batch terminated. Number of Records processed are ${Progress}.")
        return
      }
      var paymentRecordsForStatusUpdate = retrieveOutgoingPayments()
      _logger.debug("${logPrefix} - OutgoingPayment records are: ${paymentRecordsForStatusUpdate}")
      if(!paymentRecordsForStatusUpdate.HasElements){
        EmailUtil.sendEmail(EmailTo, SUCCESS_SUB, "No Payment records available in BillingCenter for processing check status updates.")
        _logger.debug("${logPrefix} - Exiting")
        return
      }

      if (TerminateRequested) {
        _logger.warn("TDIC_CheckStatusUpdateBatch#doWork() - Terminate requested during doWork() method(before retrieveCheckStatusTransactions()).")
        EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Check status update batch terminated. Number of Records processed are ${Progress}.")
        return
      }
      var checkStatusRecords = retrieveCheckStatusTransactions(getPublicIDsList(paymentRecordsForStatusUpdate))
      _logger.debug("${logPrefix} - CheckStatus records retrieved are: ${checkStatusRecords}")
      if(checkStatusRecords?.size() == 0){
        EmailUtil.sendEmail(EmailTo, SUCCESS_SUB, "No check status updates available in Lawson.")
        _logger.debug("${logPrefix} - Exiting")
        return
      }
      OperationsExpected = checkStatusRecords.size()
      processCheckStatusUpdates(paymentRecordsForStatusUpdate, checkStatusRecords)
      if(_errorRecords.HasElements || _notMatchedRecords.HasElements)
        EmailUtil.sendEmail(EmailTo, PARTIAL_SUCCESS_SUB, "${buildEmailDescription()}")
      else
        EmailUtil.sendEmail(EmailTo, SUCCESS_SUB, "Check status update batch process successfully completed.")
      _logger.debug("${logPrefix} - Exiting")

    }catch(e : Exception){
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "Check status update batch process failed with errors. Please review below error details. \n ${e.StackTraceAsString}")
      _logger.error("${logPrefix} - Batch process failed with erorrs.", e)
    }
  }

  /**
   * US566
   * 02/02/2015 Kesava Tavva
   *
   * Extract public ids from all outgoing payment records that are available for status updates
   * and build a list of those public ids.
   */
  @Param("paymentRecords", "List of OutgoingPayment records that are available for status updates")
  @Returns("List<String>, list of public ids from payment records")
  private function getPublicIDsList(paymentRecords : List<OutgoingDisbPmnt>) : List<String> {
    var publicIDsList = new ArrayList<String>()
    paymentRecords.each( \ rec -> {
      publicIDsList.add(rec.PublicID)
    })
    return publicIDsList
  }

  /**
   * US566
   * 02/03/2015 Kesava Tavva
   *
   * Match IssueDate, CheckNumber, Amount and VendorNumber from Billing center outgoing payment record with
   * check status update details retrieved from, Lawson. If there is a mismatch, record will be added
   * to list of unmatched records.
   */
  @Param("payment","OutgoingDisbPmnt record for status update")
  @Param("checkStatusRecord","Check status update details received from Lawson")
  private function matchRecord(payment: OutgoingDisbPmnt, checkStatusRecord : TDIC_CheckStatusRecord) {
    var comments = new StringBuilder()
    if(payment.Disbursement.getDateIssued() != checkStatusRecord.CheckIssueDate)
      comments.append("CheckIssueDate: ${payment.Disbursement.getDateIssued()} vs ${checkStatusRecord.CheckIssueDate} not matching | ")

    if(payment.RefNumber != null && payment.RefNumber != checkStatusRecord.CheckNumber)
      comments.append("CheckNumber: ${payment.RefNumber} vs ${checkStatusRecord.CheckNumber} not matching | ")

    if(payment.Disbursement.Amount.Amount != checkStatusRecord.CheckAmount)
      comments.append("Amount: ${payment.Disbursement.Amount.Amount} vs ${checkStatusRecord.CheckAmount} not matching | ")

    var vendorNumber =  getVendorNumber(payment)
    if(vendorNumber != checkStatusRecord.VendorID)
      comments.append("VendorNumber: ${vendorNumber} vs ${checkStatusRecord.VendorID} not matching | ")

    if(comments.toString().HasContent){
      checkStatusRecord.Comments = comments.toString()
      _notMatchedRecords.add(checkStatusRecord)
    }
  }

  /**
   * US566
   * 02/03/2015 Kesava Tavva
   *
   * Updates check status with the details retrieved from Lawson system. If the check status retrieved is not
   * a valid status as per BC typelist values then record will be added to error records.
   */
  @Param("paymentRecords","List of OutgoingDisbPmnt records retrtieved for status updates")
  @Param("checkStatusRecords","Check status update records retrieved from Lawson system")
  private function processCheckStatusUpdates(paymentRecords : List<OutgoingDisbPmnt>, checkStatusRecords : Map<String, TDIC_CheckStatusRecord>) {
    var logPrefix = "${CLASS_NAME}#processCheckStatusUpdates(List, Map)"
    _logger.debug("${logPrefix} - Entering")
    _errorRecords = new ArrayList<TDIC_CheckStatusRecord>()
    _notMatchedRecords = new ArrayList<TDIC_CheckStatusRecord>()
    for(paymentRecord in paymentRecords){
      if (TerminateRequested) {
        _logger.warn("TDIC_CheckStatusUpdateBatch#doWork() - Terminate requested during doWork() method(within processCheckStatusUpdates()).")
        EmailUtil.sendEmail(FailureEmailTo, TERMINATE_SUB, "Check status update batch terminated. Number of Records processed are ${Progress}.")
        return
      }
      incrementOperationsCompleted()
      var checkStatusRecord = checkStatusRecords.get(paymentRecord.PublicID)
      if(checkStatusRecord != null) {
        try{
          var status = MAPPER.getInternalCodeByAlias( TYPELIST, NAMESPACE, checkStatusRecord.CheckStatus?.toUpperCase())
          if(status == null){
            checkStatusRecord.Comments = "'${checkStatusRecord.CheckStatus}' is not a valid CheckStatus value."
            _errorRecords.add(checkStatusRecord)
            incrementOperationsFailed()
          }
          else {
            matchRecord(paymentRecord, checkStatusRecord)
            _logger.debug("${logPrefix} - Check status update starting for '${paymentRecord.PublicID}'")
            gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
              paymentRecord = bundle.add(paymentRecord)
              paymentRecord.Status = typekey.OutgoingPaymentStatus.get(status)
              paymentRecord.RefNumber = checkStatusRecord.CheckNumber
              if(status == OutgoingPaymentStatus.TC_ISSUED as String){
                paymentRecord.IssueDate = checkStatusRecord.CheckIssueDate
              }else{
                paymentRecord.StatusUpdateDate_TDIC = checkStatusRecord.CheckStatusDate
              }
            }, BATCH_USER)
            _logger.debug("${logPrefix} - Check status update completed for '${paymentRecord.PublicID}'")
          }
        }catch(e : Exception){
          checkStatusRecord.Comments = e.StackTraceAsString
          _errorRecords.add(checkStatusRecord)
          incrementOperationsFailed()
        }
      }
    }
    _logger.debug("${logPrefix} - Exiting")
  }

  /**
   * US566
   * 02/04/2015 Kesava Tavva
   *
   * Builds Email description with details from Unmatched and/or Error check status update records
   */
  @Returns("String, Description of Unmatched records and/or error records")
  private function buildEmailDescription() : String {
    var description = new StringBuilder()
    description.append("Check status update batch process completed with Errors and/or Warnings.\n")
    if(_notMatchedRecords.HasElements){
      description.append("Check datails (BC OutgoingPayment record vs Lawson check status record) that are not matching are :-\n")
      _notMatchedRecords.each( \ record -> {
        description.append("CheckNumber: '${record.CheckNumber}' -  ${record.Comments}\n")
      })
    }
    if(_errorRecords.HasElements){
      description.append("Error Records are:-\n")
      _errorRecords.each( \ record -> {
        description.append("Error in processing check status update for the check number: '${record.CheckNumber}'. Details are: ${record.Comments}\n")
      })
    }
    return description?.toString()
  }
  /**
   * US566
   * 02/02/2015 Kesava Tavva
   *
   * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    _logger.debug("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(EMAIL_TO_PROP_FIELD)
    _failureEmailTo = PropertyUtil.getInstance().getProperty(FAILURE_EMAIL_RECIPIENT)
    _logger.trace("${logPrefix} - ${EMAIL_TO_PROP_FIELD}-${_emailTo}")
    _lawsonDBParam = PropertyUtil.getInstance().getAppDBProperty(EXTERNAL_APP_NAME)

    var initFailureMsg:StringBuilder
    if(_emailTo == null){
      initFailureMsg = new StringBuilder()
      initFailureMsg.append("Failed to retrieve notification email addresses with the key '${EMAIL_TO_PROP_FIELD}' from integration database.")
    }
    if(_failureEmailTo == null){
      initFailureMsg = new StringBuilder()
      initFailureMsg.append("Failed to retrieve notification email addresses with the key '${FAILURE_EMAIL_RECIPIENT}' from integration database.")
    }
    if(_lawsonDBParam == null){
      if(initFailureMsg == null)
        initFailureMsg = new StringBuilder()
      else
        initFailureMsg.append(" | ")

      initFailureMsg.append("Failed to retrieve External App database connection details with the key '${EXTERNAL_APP_NAME}' from integration database.")
    }

    if (initFailureMsg?.toString().HasContent) {
      if(_failureEmailTo != null)
        EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "Unable to initiate batch process. Please review log file for more details.")
      throw new GWConfigurationException(initFailureMsg.toString())
    }
    _logger.debug("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  /**
   * US566
   * 02/02/2015 Kesava Tavva
   *
   * Retrieve Outgoing disbursement Payment details for status updates.
   */
  @Returns("List, OutgoingDisbPmnt records list")
  protected function retrieveOutgoingPayments() : List<OutgoingDisbPmnt> {
    var logPrefix = "${CLASS_NAME}#retrieveOutgoingPayments()"
    _logger.debug("${logPrefix}- Entering")
    var pQuery = Query.make(OutgoingDisbPmnt)
    pQuery.or(\or1->{
      or1.compare("Status",Equals,OutgoingPaymentStatus.TC_REQUESTED)
      or1.compare("Status",Equals,OutgoingPaymentStatus.TC_ISSUED)
      or1.compare("Status",Equals,OutgoingPaymentStatus.TC_PENDINGSTOP)
      or1.compare("Status",Equals,OutgoingPaymentStatus.TC_PENDINGVOID)
    })
    _logger.debug("${logPrefix} - Exiting")
    return pQuery.select()?.toList()
  }

  /**
   * US566
   * 02/02/2015 Kesava Tavva
   *
   * Retrieve Check status update records from Lawson using JDBC connection.
   */
  @Returns("List, list of Check status update records")
  @Throws(IOException, "If there are any IO errors")
  @Throws(SQLException, "If there are JDBC issues such as connectivity, reading data")
  @Throws(Exception, "If there are any other errors")
  private function retrieveCheckStatusTransactions(transRecordIds : List<String>): Map<String,TDIC_CheckStatusRecord> {
    var logPrefix = "${CLASS_NAME}#retrieveCheckStatusTransactions(List<String>)"
    _logger.debug("${logPrefix}- Entering")

    var checkStatusRecords : Map<String, TDIC_CheckStatusRecord>
    if(!transRecordIds.HasElements){
      _logger.debug("${logPrefix} - Exiting")
      return checkStatusRecords
    }
    //buildTransactionIDsList
    var transactionIDs = buildTransactionIDsList(transRecordIds)
    var con: Connection
    var resultSet : ResultSet
    try {
      con = DatabaseManager.getConnection(ExternalAppDBParam.Host,
          ExternalAppDBParam.Instance,
          ExternalAppDBParam.Port,
          ExternalAppDBParam.DbName,
          ExternalAppDBParam.UserName,
          ExternalAppDBParam.Password)

      var sqlStmt = "select * from AP_CHECKDETAILS where LOWER(TransactionRecordID) in (${transactionIDs})"
      _logger.debug("${logPrefix} - sqlstmt: ${sqlStmt}")
      resultSet = DatabaseManager.executeQuery(con, sqlStmt)
      checkStatusRecords = new HashMap<String,TDIC_CheckStatusRecord>()
      while(resultSet.next()){
        var record = new TDIC_CheckStatusRecord()
        record.TransactionRecordID = resultSet.getString("TransactionRecordID")?.toLowerCase()?.trim()
        record.CheckIssueDate = resultSet.getDate("CheckDate")
        record.CheckStatus = resultSet.getString("CheckStatus")?.trim()
        record.CheckStatusDate = resultSet.getDate("CheckStatusDate")
        record.CheckNumber = resultSet.getString("CheckNumber")?.trim()
        record.CheckAmount = resultSet.getBigDecimal("CheckAmount")
        record.VendorID = resultSet.getString("VendorID")?.trim()
        record.CheckPayee = resultSet.getString("CheckPayee")?.trim()
        checkStatusRecords.put(record.TransactionRecordID, record)
      }
      _logger.debug("${logPrefix} - Exiting")
      return checkStatusRecords
    } catch (io:IOException) {
      _logger.error("${logPrefix} - IOException= " + io)
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "TDIC_CheckStatusUpdateBatch#retrieveCheckStatusTransactions()- Check status batch failed due to :"+io.LocalizedMessage)
      throw(io)
    } catch (sql:SQLException) {
      _logger.error("${logPrefix} - SQLException= " + sql)
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "TDIC_CheckStatusUpdateBatch#retrieveCheckStatusTransactions()- Check status batch failed due to :"+sql.LocalizedMessage)
      throw(sql)
    } catch (e:Exception) {
      _logger.error("${logPrefix} - Exception= " + e)
      EmailUtil.sendEmail(FailureEmailTo, FAILURE_SUB, "TDIC_CheckStatusUpdateBatch#retrieveCheckStatusTransactions()- Check status batch failed due to :"+e.LocalizedMessage)
      throw(e)
    } finally {
      try {
        if (resultSet != null) resultSet.close()
        if (con != null) con.close()
      } catch (fe:Exception) {
        _logger.error("${logPrefix} - finally Exception= " + fe)
      }
    }
  }

  /**
   * US566
   * 02/02/2015 Kesava Tavva
   *
   * Build String object based on list of PublicIDs of OutgoingDisbPmnt records retrieved for check status updates.
   */
  @Returns("List, list of Check status update records")
  private function buildTransactionIDsList(transRecordIds : List<String>) : String {
    var transIDs : StringBuilder
    if(transRecordIds.HasElements){
      transIDs = new StringBuilder()
      transRecordIds.eachWithIndex( \ id, index -> {
        if(index != transRecordIds.size()-1)
          transIDs.append("'${id}',")
        else
          transIDs.append("'${id}'")
      })
    }
    return transIDs?.toString()
  }

  /**
   * Helper function to retrieve vendor number of Contact associated with OutgoingDisbPmnt
   */
  @Param("disbPayment", "OutgoingDisbPmnt retrieved for check status update")
  @Returns("Vendor number of the contact associated with disbpayment")
  private function getVendorNumber(disbPayment : OutgoingDisbPmnt) : String {
    var logPrefix = "${CLASS_NAME}#getVendorNumber(OutgoingDisbPmnt)"
    _logger.trace("${logPrefix}- Entering")
    var vendorNumber : String = null
    // Populate fields for AccountDisbursement (all disbursements are Account Disbursements for Release A)
    if (disbPayment.Disbursement typeis AccountDisbursement) {

      var disbAccount = disbPayment.Disbursement.Account
      var unapplied = disbPayment.Disbursement.UnappliedFund
      var paymentContact : Contact = null
      if (unapplied.Policy != null) {
        vendorNumber = unapplied.Policy.LatestPolicyPeriod.PrimaryInsured.Contact.VendorNumber
      }
      else {
        vendorNumber = disbAccount.PrimaryPayer.Contact.VendorNumber
      }
    }
    _logger.debug("${logPrefix}- Vendor number found for DisbursementPayment ${disbPayment} is ${vendorNumber} ")
    _logger.trace("${logPrefix}- Exiting")
    return vendorNumber
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   */
  @Returns("A boolean to indicate that the process can be stopped.")
  override function requestTermination() : boolean {
    _logger.info("${CLASS_NAME}#requestTermination() - Terminate requested for batch process.")
    // Set TerminateRequested to true by calling super method
    super.requestTermination()
    return true
  }
}