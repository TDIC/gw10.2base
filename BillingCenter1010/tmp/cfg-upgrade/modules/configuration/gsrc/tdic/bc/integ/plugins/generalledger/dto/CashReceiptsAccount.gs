package tdic.bc.integ.plugins.generalledger.dto

uses java.util.Map
uses java.util.HashMap

/**
 * US570 - General Ledger Integration
 * 12/17/2014 Alvin Lee
 *
 * A class to represent a cash receipts account.  Each unique account (LOB and state combination) will have a group of
 * cash receipts buckets.
 */
class CashReceiptsAccount {

  /**
   * A Map<String, CashReceiptsBucketGroup> to connect the unique account unit to a group of cash receipts buckets.
   */
  private var _accountUnitBucketGroupMap : Map<String, CashReceiptsBucketGroup> as AccountUnitBucketGroupMap

  construct() {
    _accountUnitBucketGroupMap = new HashMap<String, CashReceiptsBucketGroup>()
  }

}