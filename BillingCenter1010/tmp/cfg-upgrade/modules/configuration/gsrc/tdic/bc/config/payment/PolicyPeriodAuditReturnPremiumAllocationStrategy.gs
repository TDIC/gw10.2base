package tdic.bc.config.payment

uses gw.api.web.payment.ReturnPremiumAllocationStrategy
uses com.google.common.base.Preconditions
uses gw.api.web.payment.AllocationPool
uses gw.payment.FirstToLastReturnPremiumAllocationStrategy
uses org.slf4j.LoggerFactory

class PolicyPeriodAuditReturnPremiumAllocationStrategy implements ReturnPremiumAllocationStrategy {

  private static final var _logger = LoggerFactory.getLogger("Application.Invoice")

  override property get TypeKey() : ReturnPremiumAllocateMethod {
    return TC_PPFIRSTTOLAST_TDIC
  }

  override function allocate(distItems : List<BaseDistItem>, amountToAllocate : AllocationPool) {

    // Validate that the required negative credit and positive invoice items exist.
    if (distItems.Empty) {
      return
    }

    var targetDist = distItems[0].BaseDist
    Preconditions.checkArgument(distItems.where(\elt -> elt.BaseDist != targetDist).size() == 0,
        "All dist items must belong to the same base dist.")

    var negativeDistItems = distItems.where(\distItem -> distItem.InvoiceItem.Amount.IsNegative)
    if (negativeDistItems.Empty) {
      return
    }

    var positiveDistItems = distItems.where(\distItem -> distItem.InvoiceItem.Amount.IsPositive)
    if (positiveDistItems.Empty) {
      // for fully paid policyperiod no item available for allocation hence directly disburse the funds before return
      var policyPeriod = negativeDistItems[0].PolicyPeriod

      _logger.info("PolicyPeriodAuditReturnPremiumAllocationStrategy ::Credit Allocation target " + policyPeriod)
      _logger.info("PolicyPeriodAuditReturnPremiumAllocationStrategy ::Amount To Disburse " + amountToAllocate.GrossAmount)
      // GWPS-612 : Code blocked to create disbursement.
      //gw.account.CreateDisbursementWizardHelper.createAndProcessDisbursement_TDIC(policyPeriod, amountToAllocate.GrossAmount)
      return
    }

    // Get the policy period of the first credit item
    var policyPeriod = negativeDistItems[0].PolicyPeriod
    _logger.info("PolicyPeriodAuditReturnPremiumAllocationStrategy ::Credit Allocation target " + policyPeriod)

    // Get all the positive invoice items that have the same policy period as the credit item.
    var policyPeriodDistItems = positiveDistItems.where(
        \ elt -> elt.PolicyPeriod == policyPeriod)

    // Calculate the amount to allocate to policyPeriod items
    var currency = distItems[0].Currency
    var amountNeededForPolicyPeriod = policyPeriodDistItems.sum(currency,
        \ elt -> elt.GrossAmountOwed - elt.GrossAmountToApply)
    _logger.trace("PolicyPeriodAuditReturnPremiumAllocationStrategy ::Amount Needed for Allocation " + amountNeededForPolicyPeriod)

    var amountAvailableForPolicyPeriod = amountNeededForPolicyPeriod.min(amountToAllocate.GrossAmount)
    _logger.trace("PolicyPeriodAuditReturnPremiumAllocationStrategy ::Amount Available for Allocation " + amountAvailableForPolicyPeriod)
    _logger.trace("PolicyPeriodAuditReturnPremiumAllocationStrategy ::Amount To Allocate " + amountToAllocate.GrossAmount)

    // Allocate credit to the preferred invoice items based on a First-to-Last allocation method
    new FirstToLastReturnPremiumAllocationStrategy().allocate(
        policyPeriodDistItems, AllocationPool.withGross(amountAvailableForPolicyPeriod))
    _logger.debug("PolicyPeriodAuditReturnPremiumAllocationStrategy :: first to last allocation done.")

    var excessTreatment = typekey.ReturnPremiumExcessTreatment.TC_UNAPPLIED // default setting
    excessTreatment = policyPeriod.ReturnPremiumPlan.ReturnPremiumHandlingSchemes.firstWhere(\elt -> elt.HandlingCondition
        == ReturnPremiumHandlingCondition.TC_AUDIT).ExcessTreatment

    // If there is remaining credit then it should be disbursed...
    var amountToBeDisbursed = amountToAllocate.GrossAmount - amountAvailableForPolicyPeriod
    if (amountToBeDisbursed.IsNotZero &&
        amountToBeDisbursed.IsPositive &&
        excessTreatment == ReturnPremiumExcessTreatment.TC_DISBURSEMENT_TDIC) {

      // Perform the disbursement
      _logger.info("PolicyPeriodAuditReturnPremiumAllocationStrategy :: Amount to disburse " + amountToBeDisbursed)
      gw.account.CreateDisbursementWizardHelper.createAndProcessDisbursement_TDIC(policyPeriod, amountToAllocate.GrossAmount)
    }
    _logger.debug("PolicyPeriodAuditReturnPremiumAllocationStrategy :: allocate - Exiting")
  }

}