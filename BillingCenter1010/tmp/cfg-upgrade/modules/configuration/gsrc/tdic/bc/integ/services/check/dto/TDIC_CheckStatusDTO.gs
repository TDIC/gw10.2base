package tdic.bc.integ.services.check.dto

uses gw.xml.ws.annotation.WsiExportable
uses tdic.bc.integ.services.util.dto.TDIC_ErrorMessageDTO

uses java.math.BigDecimal


@WsiExportable
final class TDIC_CheckStatusDTO {
  var _transactionRecordID : String as TransactionRecordID //Required
  var _checkDate : java.util.Date as  CheckIssueDate
  var _checkNumber : String as CheckNumber //Required
  var _checkPayee : String as CheckPayee
  var _vendorID : String as VendorID
  var _checkStatus : String as CheckStatus
  var _checkStatusDate : java.util.Date as CheckStatusDate
  var _checkAmount : BigDecimal as CheckAmount

  construct() {
  }

  override function toString() : String{
    return "${TransactionRecordID} | ${CheckIssueDate} | ${CheckNumber} | ${CheckPayee} | ${VendorID} | ${CheckStatus} | ${CheckStatusDate} | ${CheckAmount}"
  }


}