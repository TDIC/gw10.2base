package tdic.bc.config.contact

uses gw.webservice.contactapi.abcontactapihelpers.core.BeanPopulator
uses gw.webservice.contactapi.mapping.FieldMappingImpl

/**
 * US1130
 * 02/13/2015 Vicente
 *
 * Extension of BCTaxIDFieldMapping for the mapping for the TaxID received from ContactManager
 */
class TDIC_BCTaxIDFieldMapping extends FieldMappingImpl<Contact> {

  construct() {
    super(Contact#TaxID)
  }

  private function populateOfficialIDFieldOnContactEntityWithValueFromXML(bp : BeanPopulator<Contact>) {
    var contactToPopulate = bp.Bean
    var xmlBackedInstance = bp.XmlBackedInstance
    contactToPopulate.TaxID = xmlBackedInstance.fieldValue("TaxID")
  }
}