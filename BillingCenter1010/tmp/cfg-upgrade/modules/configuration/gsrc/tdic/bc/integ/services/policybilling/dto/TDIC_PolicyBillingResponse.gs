package tdic.bc.integ.services.policybilling.dto

uses gw.xml.ws.annotation.WsiExportable
uses java.util.List

/**
 * US1137
 * 03/02/2015 Kesava Tavva
 *
 * WebService response object which contains list of DTOs with PolicyNumber and PaidThroughDate
 */
@WsiExportable
final class TDIC_PolicyBillingResponse {
  var pbDTOList : List<TDIC_PolicyBillingDTO> as PolicyBillingDTOList

  /**
   * US1137
   * 03/02/2015 Kesava Tavva
   *
   * Override function to return field values in a String object.
   */
  override function toString() : String {
    return "PolicyBillingDTOList: ${PolicyBillingDTOList}"
  }
}