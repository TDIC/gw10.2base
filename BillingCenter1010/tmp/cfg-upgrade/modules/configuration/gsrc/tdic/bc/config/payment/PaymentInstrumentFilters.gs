package tdic.bc.config.payment

uses com.google.common.collect.ImmutableList

/**
 * US65 - Remove unused payment methods
 * 08/18/2014 Alvin Lee
 *
 * Payment Instrument Filters to be used at TDIC.
 */
class PaymentInstrumentFilters extends gw.payment.PaymentInstrumentFilters{

  /**
   * TDIC's payment methods that are available when creating a new Payment Instrument from "Account Details Summary",
   * "New Account", "New Direct Bill Payment", "New Payment Request", and "New Suspense Payment" pages.
   */
  public static final var newPaymentInstrumentFilterCreditCardOnly : ImmutableList<PaymentMethod> = ImmutableList.of(
     PaymentMethod.TC_CREDITCARD
  )

  /**
   * US63: Maintain account/unapplied fund payment Instruments -ACH/EFT
   * 10/26/2014 Vicente
   *
   * TDIC's payment methods that are available when creating a new ACH/EFT Payment Instrument from "Account Details
   * Summary"
   */
  public static final var newPaymentInstrumentFilterACHOnly : ImmutableList<PaymentMethod> = ImmutableList.of(
    PaymentMethod.TC_ACH
  )
}