package tdic.bc.integ.plugins.bankeft.dto.BankPrenote

uses com.tdic.util.database.IWritableBeanInfo

uses java.util.Map
uses java.sql.Connection
uses java.sql.PreparedStatement

/**
 * US568 - Bank/EFT Integration
 * 09/24/2014 Alvin Lee
 *
 * Concrete implementation of class describing the Payment Instrument fields to persist to the integration database.
 */
class TDIC_PaymentInstrumentBeanInfo<T> extends IWritableBeanInfo<T> {

  // Map to bind fields to export to the SQL database. Fields not listed here will not be exported
  public static var _fields:Map<String, Map<String, Object>> = {
      "ContactName" -> {FIELD_SQL_NAME -> "ContactName",
          FIELD_BINDER -> stringFieldBinder
      },
      "BankName" -> {FIELD_SQL_NAME -> "BankName",
          FIELD_BINDER -> stringFieldBinder
      },
      "BankRoutingNumber" -> {FIELD_SQL_NAME -> "BankRoutingNumber",
          FIELD_BINDER -> stringFieldBinder
      },
      "BankAccountNumber" -> {FIELD_SQL_NAME -> "BankAccountNumber",
          FIELD_BINDER -> stringFieldBinder
      }
  }

  /**
   * Default constructor with no parameters.
   */
  construct() {
  }

  /**
   * Abstract method implementation, provides the SQL name of the table for this object
   */
  override property get TableName():String {
    return "EFTPrenoteIntegration"
  }

  /**
   * Abstract method implementation, provides the Gosu name of the object's Public ID property for use with reflection calls
   */
  override property get IDColumnName(): String {
    return "PublicID"
  }

  /**
   * Abstract method implementation, provides the SQL name of the Public ID property in the database table
   */
  override property get IDColumnSQLName(): String {
    return "PaymentInstrumentPublicID"
  }

  /**
   * Abstract method implementation, provides the SQL name of the Processed field in the database table
   */
  override property get ProcessedColumnSQLName(): String {
    return "Processed"
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will insert a new object when it is executed
   *
   * @param conn - database connection
   * @param entity - Entity that is to be inserted into the database
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function createAndBindCreateSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindCreateSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will retrieve an object when it is executed
   *
   * @param conn - database connection
   * @param entity - Entity with populated primary key to be used to retrieve values from the database
   * @return PreparedStatement that will return values based on the provided entity object
   */
  override function createAndBindRetrieveSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindRetrieveSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will update an object when it is executed
   *
   * @param conn - database connection
   * @param entity - Entity that is to be updated in the database
   * @return PreparedStatement that will update values based on the provided entity object
   */
  override function createAndBindUpdateSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindUpdateSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to populate the fields of an entity from a GX model representation.
   */
  @Param("entity", "The object to populate")
  @Param("model", "The GX model containing the data to populate into the entity")
  override function loadFromModel(entity : Object, model : Object) {
    loadFromModel(entity, model, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will update an object when it is executed
   *
   * @param conn - database connection
   * @param entity - Entity that is to be updated in the database
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function markProcessed(conn : Connection, entity : T) : PreparedStatement {
    return markProcessed(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will retrieve all objects of our entity type that have not yet been written to an export file
   *
   * @param conn - database connection
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function createRetrieveAllSQL(conn: Connection): PreparedStatement {
    return createRetrieveAllSQL(conn, _fields)
  }

}