package tdic.bc.integ.plugins.generalledger.dto

uses java.util.ArrayList
uses java.util.List

/**
 * US570 - Lawson Cash Receipts Integration
 * 4/28/2015 Alvin Lee
 *
 * A Gosu class to contain the fields needed for the GX model when generating the flat file.
 */
class CashReceiptsFile {

  /**
   * The List of cash receipts buckets to write to the flat file.
   */
  private var _cashReceiptsBuckets : List<CashReceiptsBucket> as CashReceiptsBuckets

  /**
   * Counter for the DEP lines; must be unique for each DEP line in the cash receipts file
   */
  private var _depCounter = 0

  /**
   * Counter for the NSF lines; must be unique for each NSF line in the cash receipts file
   */
  private var _nsfCounter = 500

  /**
   * Adds to the list of cash receipts buckets.
   */
  @Param("bean", "The CashReceiptsBucket to add to the list of cash receipts buckets")
  public function addToCashReceiptsBuckets(bean : CashReceiptsBucket) {
    if (_cashReceiptsBuckets == null) {
      _cashReceiptsBuckets = new ArrayList<CashReceiptsBucket>()
    }
    if (bean.DepOrNsf == "NSF") {
      bean.UID = _nsfCounter
      _nsfCounter++
    }
    else {
      bean.UID = _depCounter
      _depCounter++
    }
    _cashReceiptsBuckets.add(bean)
  }

}