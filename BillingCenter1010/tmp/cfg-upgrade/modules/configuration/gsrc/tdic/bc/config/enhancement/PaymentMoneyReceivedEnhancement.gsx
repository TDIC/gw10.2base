package tdic.bc.config.enhancement

enhancement PaymentMoneyReceivedEnhancement : PaymentMoneyReceived {
  /**
   * Returns true if the payment reversal is one of atFault reasons
   */
  @Returns("A boolean to indicate if the APW payment prior to this one is an APW NSF Payment Reversal")
  property get IsReversalAtFault_TDIC() : boolean {
    if(this.Reversed and PaymentReversalReason.TF_ATFAULTREVERSALS_TDIC.TypeKeys?.hasMatch(\elt1 -> elt1==this.ReversalReason)) {
      return true
    } else {
      return false
    }
  }
}
