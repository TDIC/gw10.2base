package tdic.bc.common.batch.troubleticket

uses gw.api.database.DBFunction
uses gw.api.database.Query
uses gw.api.system.PLLoggerCategory
uses gw.api.util.DateUtil
uses gw.command.demo.GeneralUtil
uses gw.processes.BatchProcessBase

uses java.text.SimpleDateFormat

class TDIC_TroubleTicketsDailyBatch extends BatchProcessBase {

  private static final var CLASS_NAME = TDIC_TroubleTicketsDailyBatch.Type.RelativeName
  var _logger = PLLoggerCategory.TDIC_INTEGRATION

  construct(){
    super(BatchProcessType.TC_TROUBLETICKETSDAILY_TDIC)
  }

  protected override function doWork() {

    var user = GeneralUtil.findUserByUserName("DoraE")
    var group = GeneralUtil.findGroupByUserName("Urgent Trouble Tickets")

    _logger.info("${CLASS_NAME}#doWork(): Trouble ticket Hold on Delinquency start...)")
    //3181 - query to fetch the open delinquencies on policy level
    var polDelProcess  = Query.make(PolicyDlnqProcess).compare("Status", Equals, DelinquencyProcessStatus.TC_OPEN)
        .compare("ExitDate", Equals, null).select()

    _logger.info("${CLASS_NAME}#doWork(): Total delinquencies with Initial Reminder Event ${DateUtil.currentDate()}:"+polDelProcess.Count)


    var ttHelper:CreateTroubleTicketHelper
    var tt:TroubleTicket

    for(del in polDelProcess){
      // Handling only CA Policies.
     if(del.PolicyPeriod.RiskJurisdiction == Jurisdiction.TC_CA){
      if (TerminateRequested) {
        _logger.warn("${CLASS_NAME}#doWork():  - Terminate requested during doWork() method(before retrieveCheckStatusTransactions()).")
        return
      }
      //3181 - Loop through the delinquencies that are active and having the previous event as Initial Reminder/DunningLetter1.TT will be created in only this scenario

      if(del.Active and del.PreviousEvent.EventName == DelinquencyEventName.TC_DUNNINGLETTER1){
        incrementOperationsCompleted()

        var pp = del.PolicyPeriod

        if(pp.Policy.ActiveTroubleTickets.where(\elt -> elt.Title == "COVID Q120").HasElements) {
          _logger.info("${CLASS_NAME}#doWork(): COVID related Trouble ticket already exists for ${pp.Policy}")
        }else {
          try {
            _logger.info("${CLASS_NAME}#doWork(): Create COVID related Trouble ticket for ${pp.Policy}")
            gw.transaction.Transaction.runWithNewBundle(\bundle -> {
              tt = new TroubleTicket(bundle)
              tt.Priority = Priority.TC_URGENT
              tt.TicketType = TroubleTicketType.TC_DISASTERHOLD
              tt.Title = "COVID Q120"
              tt.assign(group, user)
              tt.DetailedDescription = "Suspend delinquency process for 60 days per CDI request"
              tt.Hold.setAppliedToHoldType(HoldType.TC_DELINQUENCY, true)
              tt.Hold.setReleaseDate(HoldType.TC_DELINQUENCY, new SimpleDateFormat("MM/dd/yyyy").parse("07/14/2020"))
              tt.AssignedUser = user
              ttHelper = new CreateTroubleTicketHelper(bundle)
              //ttHelper.linkTroubleTicketWithAccounts(tt, {pp.Account})
              // GWPS-1692 - Trouble Ticket are creaing at Policy Level.
              ttHelper.linkTroubleTicketWithPolicy(tt, pp.Policy)
              _logger.info("${CLASS_NAME}#doWork(): Created trouble ticket info --> Ticket#${tt.TroubleTicketNumber}; AccountNumber: ${pp.PolicyNumber} AssignedUser: ${user.DisplayName}", false)
            }, "iu")
            tt = null
          }catch(e: Exception){
            incrementOperationsFailed()
            _logger.error("${CLASS_NAME}#doWork(): Create COVID related Trouble ticket for ${pp.PolicyNumber}")
          }
        }
      }
     }
    }
    _logger.info("${CLASS_NAME}#doWork(): Trouble ticket Hold on Delinquency completed.)")
  }
}