package tdic.bc.config.enhancement

uses tdic.bc.integ.plugins.hpexstream.util.TDIC_BCExstreamHelper
uses org.slf4j.LoggerFactory

/**
 * US718 - US718: Direct Bill Unapplied Funds Tracking Information Rules
 * 21/11/2014 Vicente
 *
 * DirectBillMoneyRcvd for TDIC.
 */
enhancement DirectBillMoneyRcvdEnhancement : DirectBillMoneyRcvd {
 /**
  * Returns the destiny account if this direct bill money was moved to another account or null if not
  */
  @Returns("Returns the destiny account if this direct bill money was moved to another account or null if not")
  property get DestinyAccount_TDIC() : Account {
    var query = gw.api.database.Query.make(DirectBillMoneyRcvd)
    query.compare(DirectBillMoneyRcvd#MovedFromPMR, Equals, this)
    var movedPayment = query.select().FirstResult
    return movedPayment?.Account
  }

  /**
   * US1158 - EFT Reversals / US1012 - Create Activity
   * 3/17/2015 Hermia Kho / Alvin Lee
   *
   * Check previous APW payment to see if it is reversed due to NSF.  This method is useful for detecting consecutive
   * NSF for APW payments.  Note that if the current payment is not an ACH payment, it will return false, as it
   * will not be able to detect where the current payment is in the list of ACH payments.  This method will not check if
   * the current ACH payment is an APW NSF Reversal, as it assumes the code outside this method will perform that check.
   */
  @Returns("A boolean to indicate if the APW payment prior to this one is an APW NSF Payment Reversal")
  property get IsConsecutiveAPWPaymentAlsoNSF_TDIC() : boolean {
    var _logger = LoggerFactory.getLogger("Rules")
    _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Reversal with Payment Instrument ACH encountered. Next, identify all ACH payments under the account, sorted by date. Account: " + this.Account)
    // subset the list to just ACH payment and sort the ACH payment list by Received date
    var payACHResults = this.Account.findReceivedPaymentMoneysSortedByReceivedDate().where( \ pmr -> pmr.PaymentInstrument.PaymentMethod == PaymentMethod.TC_ACH)
    // list the result to confirm sort
    if (_logger.DebugEnabled) {
      _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Total number of ACH payments = " + payACHResults.size())
      for (payACHResult in payACHResults) {
        _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - ACH received date listed = "
            + payACHResult.ReceivedDate + " | Amount = " + payACHResult.Amount + " | Reversed = " + payACHResult.Reversed)
      }
    }
    // Get the index if the current payment in relation to all the ACH payments
    var indexOfCurrentPayment = payACHResults.indexOf(this)
    if (indexOfCurrentPayment < 0) {
      // Return false since current payment not found as an ACH payment
      _logger.warn("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Current payment not found as an ACH payment. Returning false.")
      return false
    }

    // Check for a prior ACH payment
    if (indexOfCurrentPayment > 0) {
      // If the prior payment was also reversed due to NSF, return true
      _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Prior ACH payment exists. Checking prior ACH for an NSF reversal...")
      var priorACHPayment = payACHResults.get(indexOfCurrentPayment-1)
      _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Prior ACH Received date = "
          + priorACHPayment.ReceivedDate + " | Reversal date = " + priorACHPayment.ReversalDate + " | Amount = "
          + priorACHPayment.Amount)
      if (priorACHPayment.Reversed && priorACHPayment.ReversalReason == PaymentReversalReason.TC_R01) {
        _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Prior ACH was also reversed for NSF. Returning true.")
        return true
      }
      else {
        _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Prior ACH is not an NSF reversal.")
      }
    }

    // Check for a next ACH payment
    if (this != payACHResults.last()) {
      // If the next payment was also reversed due to NSF, return true
      _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Next ACH payment exists. Checking next ACH for an NSF reversal...")
      var nextACHPayment = payACHResults.get(indexOfCurrentPayment+1)
      _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF : Next ACH Received date = "
          + nextACHPayment.ReceivedDate + " | Reversal date = " + nextACHPayment.ReversalDate + " | Amount = "
          + nextACHPayment.Amount)
      if (nextACHPayment.Reversed && nextACHPayment.ReversalReason == PaymentReversalReason.TC_R01) {
        _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Next ACH was also reversed for NSF. Returning true.")
        return true
      }
      else {
        _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - Next ACH is not an NSF reversal.")
      }
    }

    _logger.debug("DirectBillMoneyRcvdEnhancement#IsConsecutiveNSF - No consecutive ACH NSF found. Returning false.")
    return false
  }

  function createDocumentsForEvent_TDIC (eventName: String) {
    var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _logger.trace("TDIC_AccountEnhancement#createDocumentsForEvent() - Entering.")
    (new TDIC_BCExstreamHelper()).createDocumentStubs(eventName, this.Account, this.DirectBillPayment.ReversedDistItems?[0].PolicyPeriod)
    _logger.trace("TDIC_AccountEnhancement#createDocumentsForEvent() - Notifying Exstream Message Queue.")
  }
}