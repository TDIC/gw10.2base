package tdic.bc.config.enhancement

uses java.lang.Exception
uses gw.api.system.server.ServerUtil;

enhancement UserEnhancement : entity.User {

  /**
   * Create default user settings
   */
  public function createDefaultUserSettings_TDIC() : void {
    if (this.UserSettings != null) {
      throw new Exception ("Unable to create user settings, since they are already created.");
    }

    var productCode = ServerUtil.Product.ProductCode;

    var userSettings = new UserSettings();

    // Default User Settings
    if (productCode == "bc") {
      userSettings.setFieldValue ("StartupPage", StartupPage.TC_DESKTOPGROUP);
    } else if (productCode == "pc") {
      userSettings.setFieldValue ("EmailOnActAssign", true);
      userSettings.PrintPageNums = true;
      userSettings.RotateTables = true;
      userSettings.ShowPrintPreview = true;
      userSettings.setFieldValue ("StartupPage", StartupPage.get("DesktopActivities"));
    }

    this.UserSettings = userSettings;
  }
}
