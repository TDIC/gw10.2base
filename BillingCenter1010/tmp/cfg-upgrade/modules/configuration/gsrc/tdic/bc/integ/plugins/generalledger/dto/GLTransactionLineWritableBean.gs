package tdic.bc.integ.plugins.generalledger.dto

uses com.tdic.util.database.IWritableBeanInfo
uses com.tdic.util.database.WritableBeanBaseImpl

uses java.math.BigDecimal

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Class holding values that will be written to the external table to be forwarded to the GL system
 */
class GLTransactionLineWritableBean extends WritableBeanBaseImpl {

  /**
   * The name of the account associated with the transaction.
   */
  private var _accountName : String as AccountName

  /**
   * The account number associated with the transaction.
   */
  private var _accountNumber: String as AccountNumber

  /**
   * The transaction line item amount.
   */
  private var _amount : BigDecimal as Amount

  /**
   * Charge Name.
   */
  private var _chargeName : String as ChargeName

  /**
   * The date the transaction entity was created.
   */
  private var _createdDate : String as CreatedDate

  /**
   * The user who created the transaction entity.
   */
  private var _createdUser : String as CreatedUser

  /**
   * The type of credit card used for the payment (only for credit card payment transactions).
   */
  private var _creditCardType : String as CreditCardType

  /**
   * The description of the transaction.
   */
  private var _description : String as Description

  /**
   * The PublicID of the DirectBillMoneyRcvd entity, used to tie payment transactions together.
   */
  private var _directBillMoneyRcvdPublicID : String as DirectBillMoneyRcvdPublicID

  /**
   * The Guidewire name of the T-account associated with the line item.
   */
  private var _gwTAccountName : String as GWTAccountName

  /**
   * The Guidewire name for the type of transaction.
   */
  private var _gwTransactionName : String as GWTransactionName

  /**
   * The invoice status of the invoice to which a payment is applied, for Charge Paid From Account transactions
   */
  private var _invoiceStatus : String as InvoiceStatus

  /**
   * The type of line item: Credit or Debit.
   */
  private var _lineItemType : String as LineItemType

  /**
   * Line of business.
   */
  private var _lob : String as LOB

  /**
   * The mapped name of the T-account associated with the line item.
   */
  private var _mappedTAccountName : String as MappedTAccountName

  /**
   * The mapped name for the type of transaction.
   */
  private var _mappedTransactionName : String as MappedTransactionName

  /**
   * The payment method, populated for Direct Bill Money Received transactions
   */
  private var _paymentMethod : String as PaymentMethod

  /**
   * The payment reversal reason, populated for Direct Bill Money Received transactions
   */
  private var _paymentReversalReason : String as PaymentReversalReason

  /**
   * The policy effective date.
   */
  private var _policyEffectiveDate:String as PolicyEffectiveDate

  /**
   * The policy number.
   */
  private var _policyNumber : String as PolicyNumber

  /**
   * The status of the policy period.
   */
  private var _policyPeriodStatus : String as PolicyPeriodStatus

  /**
   * The state code of the policy.
   */
  private var _stateCode : String as StateCode

  /**
   * The term number of the policy period.
   */
  private var _termNumber : int as TermNumber

  /**
   * The date of the transaction.
   */
  private var _transactionDate : String as TransactionDate

  /**
   * The transaction number.
   */
  private var _transactionNumber : String as TransactionNumber

  /**
   * The type of transaction.
   */
  private var _transactionSubtype:String as TransactionSubtype

  /**
   * Underwriting company
   */
  private var _uwCompany : String as UWCompany

  /**
   * Default constructor.
   */
  construct() {
    super(new GLTransactionLineBeanInfo())
  }

  /**
   * Constructor for WritableBean requiring an BeanInfo object
   *
   * @param type - BeanInfo object for this WritableBean
   */
  construct(type: IWritableBeanInfo) {
    super(type)
  }

}