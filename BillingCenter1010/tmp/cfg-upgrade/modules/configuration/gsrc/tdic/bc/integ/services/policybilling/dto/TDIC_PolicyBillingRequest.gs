package tdic.bc.integ.services.policybilling.dto

uses gw.xml.ws.annotation.WsiExportable
uses java.util.List

/**
 * US1137
 * 03/02/2015 Kesava Tavva
 *
 * WebService request object which contains list of PolicyNumbers
 */
@WsiExportable
final class TDIC_PolicyBillingRequest {
  var policyNumbersList : List<String> as PolicyNumbersList

  /**
   * US1137
   * 03/02/2015 Kesava Tavva
   *
   * Override function to return field values in a String object.
   */
  override function toString() : String {
    return "PolicyNumbersList: ${PolicyNumbersList}"
  }
}