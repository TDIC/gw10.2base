package tdic.bc.integ.services.creditcard

uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.api.util.DisplayableException
uses gw.api.web.payment.DBPaymentDistItemGroup
uses gw.api.web.payment.DirectBillPaymentView
uses gw.pl.currency.MonetaryAmount
uses gw.transaction.ChargePatternHelper
uses net.authorize.ResponseField
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses java.math.BigDecimal

/**
 * US680, DE67
 * 01/14/2015 Rob Kelly
 *
 * A helper class for managing credit card payments via the UI.
 */
class TDIC_PCFHelper {
  public static final var LOGGER : Logger = LoggerFactory.getLogger("CREDIT_CARD_HANDLER")
  private static final var LOG_TAG = "TDIC_PCFHelper#"
  private static final var MAPPER = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  private static final var TYPELIST = "CreditCardType_TDIC"
  private static final var NAMESPACE = "tdic:authnet"
  /**
   * Creates a new DirectBillPaymentView for the specified account and credit card handler.
   */
  static function createNewPaymentView(anAccount : Account, aCreditCardHelper : TDIC_CreditCardHandler) : DirectBillPaymentView {

    var pView = gw.api.web.payment.DirectBillPaymentView.createView(anAccount, false)
    pView.MoneyReceived.PaymentInstrument = null

    // BrianS - Do not default to Default Unapplied Fund (GW-2832)
    pView.TargetUnappliedFund = getUnappliedFunds(anAccount).first();

    if (aCreditCardHelper != null) {
      var paymentCutoffTime = aCreditCardHelper.getPaymentCutOffTime()
      var rightNow = new Date()
      if (rightNow.Time >= paymentCutoffTime.Time) {
        pView.MoneyReceived.ReceivedDate = rightNow.addBusinessDays(1)
      }
    }
    // Fix for GBC -887 General Ledger; Getting Null pointer exception in New Direct Bill Payment transaction
    /*if (anAccount.ListBill) {*/
      pView.IncludeOnlyCriteria = DistributionLimitType.TC_OUTSTANDING
   /* } else {
      pView.IncludeOnlyCriteria = anAccount.DistributionLimitTypeFromPlan;
    }*/

    return pView
  }

  /**
   * Creates a new Credit Card PaymentInstrument for the specified Account.
   */
  static function createNewCreditCardPaymentInstrument(anAccount : Account) : PaymentInstrument {

    var newPaymentInstrument = new PaymentInstrument()
    newPaymentInstrument.PaymentMethod = typekey.PaymentMethod.TC_CREDITCARD
    newPaymentInstrument.CreditCardPayeeEmail_TDIC = anAccount.PrimaryPayer.Contact.EmailAddress1
    return newPaymentInstrument
  }

  /**
   * Makes a credit card payment using the details contained in the specified payment view using the specified credit card handler.
   */
  @Throws(DisplayableException, "if the payment cannot be made")
  static function makeCreditCardPayment(aPaymentView : DirectBillPaymentView, aCreditCardHandler : TDIC_CreditCardHandler) {
    var logTag =  LOG_TAG + "makeCreditCardPayment(DirectBillPaymentView, TDIC_CreditCardHandler) - "
    LOGGER.info(logTag + "Credit card Payment process started for account:  Account: ${aPaymentView.TargetAccount}; AccontName: ${aPaymentView.TargetAccount.AccountName}")
    LOGGER.info(logTag + "Credit card Payment money received details:  ReceivedDate: ${aPaymentView.MoneyReceived.ReceivedDate}; Amount: ${aPaymentView.MoneyReceived.Amount}; UnappliedFund: ${aPaymentView.TargetUnappliedFund}")

    if (aCreditCardHandler != null and aPaymentView != null /*and not aPaymentView.ExecuteWithoutDistribution*/) {

      var paymentAmount = aPaymentView.MoneyReceived.Amount
      var paymentInstrument = aPaymentView.MoneyReceived.PaymentInstrument
      LOGGER.info(logTag + "Credit card Payment process started for account:  Account: ${aPaymentView.TargetAccount}; AccontName: ${aPaymentView.TargetAccount.AccountName}")
      var paymentDescription = buildCreditCardPaymentDescription(aPaymentView.Groups, aCreditCardHandler.getMaximumPaymentDescriptionLength())
      var paymentCutoffTime = aCreditCardHandler.getPaymentCutOffTime()
      var rightNow = new Date()
      if (rightNow.Time >= paymentCutoffTime.Time) {

        aPaymentView.MoneyReceived.ReceivedDate = rightNow.addBusinessDays(1)
      }
      var authNetResponses = aCreditCardHandler.submitPaymentAndFee(paymentAmount, paymentInstrument, paymentDescription, aPaymentView.MoneyReceived)
      if(authNetResponses != null and not authNetResponses.Empty){
        //Invoice Payment Fee
        var invoicePaymentResponse = authNetResponses.get(aCreditCardHandler.INVOICE_PAYMENT_RESPONSE)
        if(invoicePaymentResponse != null and not invoicePaymentResponse.Empty){
          aPaymentView.MoneyReceived.RefNumber = invoicePaymentResponse.get(ResponseField.AUTHORIZATION_CODE)
          aPaymentView.TargetAccount.addHistoryFromGosu(Date.CurrentDate, HistoryEventType.TC_CREDITCARDPAYMENTPROCESSED_TDIC,
              DisplayKey.get("TDIC.AccountHistory.CreditCard.Payment", "Invoice payment", paymentAmount), null, aPaymentView.MoneyReceived.RefNumber, false)
          LOGGER.info(logTag + "User entered credit card type is : "+aPaymentView.MoneyReceived.PaymentInstrument.CreditCardType_TDIC+"; AuthNet Credit Card Type received is: "+invoicePaymentResponse.get(ResponseField.CARD_TYPE))
          var cardType = MAPPER.getInternalCodeByAlias( TYPELIST, NAMESPACE, invoicePaymentResponse.get(ResponseField.CARD_TYPE))
          if(cardType != null){
            aPaymentView.MoneyReceived.PaymentInstrument.CreditCardType_TDIC = typekey.CreditCardType_TDIC.get(cardType)
          }else{
            if(invoicePaymentResponse.containsValue(CreditCardType_TDIC.TC_V.DisplayName)){
              aPaymentView.MoneyReceived.PaymentInstrument.CreditCardType_TDIC = CreditCardType_TDIC.TC_V
            }else if(invoicePaymentResponse.containsValue(CreditCardType_TDIC.TC_M.DisplayName)){
              aPaymentView.MoneyReceived.PaymentInstrument.CreditCardType_TDIC = CreditCardType_TDIC.TC_M
            }else if(invoicePaymentResponse.containsValue(CreditCardType_TDIC.TC_A.DisplayName)){
              aPaymentView.MoneyReceived.PaymentInstrument.CreditCardType_TDIC = CreditCardType_TDIC.TC_A
            }else{
              LOGGER.warn(logTag +"Credit card type received from authNet is not mapped: "+invoicePaymentResponse.get(ResponseField.CARD_TYPE))
              aPaymentView.MoneyReceived.PaymentInstrument.Description = invoicePaymentResponse.get(ResponseField.CARD_TYPE)
            }
          }
        }
        //Convenience Fee
       /* GWPS2822 - Disabling the Credit Card Convenience Fee as part of new business process.
       var feePaymentResponse = authNetResponses.get(aCreditCardHandler.FEE_PAYMENT_RESPONSE)
        if(feePaymentResponse != null and not feePaymentResponse.Empty){
          aPaymentView.MoneyReceived.CCFeeRefNumber_TDIC = feePaymentResponse.get(ResponseField.AUTHORIZATION_CODE)
          aPaymentView.MoneyReceived.CCFeeAmount_TDIC = new MonetaryAmount(aCreditCardHandler.convenienceFeeAmount, Currency.TC_USD)
          aPaymentView.TargetAccount.addHistoryFromGosu(Date.CurrentDate, HistoryEventType.TC_CREDITCARDFEEPAYMENTPROCESSED_TDIC,
              DisplayKey.get("TDIC.AccountHistory.CreditCard.Payment", "Convenience Fee",aPaymentView.MoneyReceived.CCFeeAmount_TDIC), null, aPaymentView.MoneyReceived.CCFeeRefNumber_TDIC, false)
        }*/
      }
      aCreditCardHandler.finishCreditCardProcess()
    }

    // Create the credit card usage fee for the credit card payment irrespective of the distribution.
    if(aPaymentView.MoneyReceived.CCFeeWaiverReason_TDIC == null and not isCreditCardFeeCreatedForToday(aPaymentView.MoneyReceived.Account)){
      var policyPeriod = aPaymentView.TargetUnappliedFund.PolicyPeriod_TDIC
      if(policyPeriod != null){
        var bundle = gw.transaction.Transaction.Current
        var bi = new General(bundle, Currency.TC_USD)
        bi.AssociatedPolicyPeriod = policyPeriod
        var creditCardFeeAmount = ScriptParameters.getParameterValue("TDIC_CreditCardFeeAmount") as BigDecimal
        bi.ModificationDate = gw.api.util.DateUtil.currentDate()
        bi.buildCharge(new MonetaryAmount(creditCardFeeAmount, policyPeriod.Currency), ChargePatternHelper.getChargePattern("CreditCardUsageFee"))
        bi.execute()
      }

    }
  }

  /**
   * Builds a description string for a credit card payment from the specified groups of distribution items.
   */
  private static function buildCreditCardPaymentDescription(itemsGroup : java.util.List<DBPaymentDistItemGroup>, maxDescriptionLength : int) : String {

    var description = new java.lang.StringBuilder()
    var itemDescription : String
    var itemDescriptions = new ArrayList<String>()
    if (itemsGroup != null) {

      for (aGroup in itemsGroup) {

        for (anItem in aGroup.DistItems) {

          if (anItem.GrossAmountToApply != null and anItem.GrossAmountToApply.IsNotZero) {

            itemDescription = DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.PaymentDescription", buildCreditCardPaymentDescriptionPolicyNumber(anItem.InvoiceItem.PolicyPeriod), anItem.InvoiceItem.Invoice.InvoiceNumber)
            if (description.length() + itemDescription.length() <= maxDescriptionLength and not itemDescriptionPresent(itemDescription, itemDescriptions)) {

              if (description.length() > 0) {

                description.append(",")
              }
              description.append(itemDescription)
              itemDescriptions.add(itemDescription)
            }
          }
        }
      }
    }
    return description.toString()
  }

  /**
   * Builds a policy number for use in a description of a credit card payment for an item on the specified PolicyPeriod.
   */
  private static function buildCreditCardPaymentDescriptionPolicyNumber(aPolicyPeriod : PolicyPeriod) : String {

    var policyNumberPrefix : String
    if (aPolicyPeriod.Policy.LOBCode == typekey.LOBCode.TC_WC7WORKERSCOMP) {

      policyNumberPrefix = "WC"
    }
    return policyNumberPrefix + aPolicyPeriod.RiskJurisdiction.Code + aPolicyPeriod
  }

  /**
   * Returns true if the specified description is an element in the specified list of descriptions; false otherwise
   */
  private static function itemDescriptionPresent(itemDescription : String, itemDescriptions : List<String>) : boolean {

    for (aDescription in itemDescriptions) {

      if (aDescription == itemDescription) {

        return true
      }
    }

    return false
  }

  /**
   * Get a filtered list of unapplied funds.
   */
  public static function getUnappliedFunds (account : Account) : List<UnappliedFund> {
    var unappliedFunds = account.UnappliedFundsOrdered;

    // If permission is missing, filter out non term-level unapplied funds.
    /*if (perm.System.TransferNonTermLevelUnapplied_TDIC == false) {
      unappliedFunds = unappliedFunds.where(\uaf -> uaf.IsTermLevelUnapplied);
    }*/

    return unappliedFunds;
  }

  public static function isCreditCardFeeCreatedForToday (account : Account) : boolean {
    var currentDateCharges : List<Charge>
    account.AllPolicyPeriods.each(\pp -> {
      currentDateCharges = pp.Charges?.where(\elt -> elt.ChargePattern == ChargePatternHelper.getChargePattern("CreditCardUsageFee") and elt.CreateTime?.trimToMidnight() == DateUtil.currentDate().trimToMidnight())
    })
    if(!currentDateCharges.Empty){
      return true
    }
    return false
  }
}
