package tdic.bc.integ.plugins.bankeft.helper.prenote

uses com.tdic.util.properties.PropertyUtil
uses gw.api.gx.GXOptions
uses gw.plugin.Plugins
uses gw.plugin.util.IEncryption
uses gw.xml.XmlElement
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_BankEFTPrenoteFile
uses tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_PaymentInstrumentWritableBean
uses tdic.bc.integ.plugins.bankeft.helper.BankEFTDelimitedFileImpl

/**
 * US568 - Bank/EFT Integration
 * 10/01/2019 Vipul Kumar
 *
 * Helper class with methods to add lines to Bank/EFT Prenote flat file.
 */
class TDIC_BankEFTPrenoteHelper {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_EFT")
  public static final var _vendorSpec : String = PropertyUtil.getInstance().getProperty("EFTPrenoteVendorSpec")

  /**
   * Converts data to the lines that can be written to the destination flat file.
   */
  @Param("flatFileData", "The BankEFTPrenoteFile object containing the data to write to the destination flat file")
  @Returns("A List<String> containing the liens to be written to the destination flat file")
  @Throws(Exception, "If there are any unexpected errors encoding the data")
  public static function convertDataToFlatFileLines(flatFileData : TDIC_BankEFTPrenoteFile) : List<String> {
    var gxModel = new tdic.bc.integ.plugins.bankeft.gxmodel.tdic_bankeftprenotefilemodel.TDIC_BankEFTPrenoteFile(flatFileData)
    _logger.info("TDIC_BankEFTPrenoteHelper#convertDataToFlatFileLines - Bank/EFT GX Model Generated: ")
    _logger.info(gxModel.asUTFString())
    var xml = XmlElement.parse(gxModel.asUTFString())
    var df = new BankEFTDelimitedFileImpl()
    return df.encode(_vendorSpec, xml)
  }

  /**
   * Helper function to map a payment instrument entity to the GX model for the writable bean so it can be saved to the database.
   */
  @Param("paymentInstrument", "The PaymentInstrument entity to map to the GX model")
  @Returns("The GX Model representation of the payment instrument writable bean")
  public static function mapPaymentInstrumentToWritableBeanModel(paymentInstrument : PaymentInstrument) : tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.TDIC_PaymentInstrumentWritableBean {
    _logger.info("TDIC_BankEFTPrenoteHelper#mapPaymentInstrumentToWritableBeanModel - Entering")

    //Build XML Model options object
    var options = new GXOptions()
    options.Incremental = false // Send all values, not just changed ones
    options.Verbose = true // Send all fields, even nulls

    //Build payment instrument writable bean object
    var gxPaymentInstrument = new TDIC_PaymentInstrumentWritableBean()
    gxPaymentInstrument.PublicID = paymentInstrument.PublicID
    gxPaymentInstrument.BankName = paymentInstrument.BankName_TDIC
    gxPaymentInstrument.BankRoutingNumber = paymentInstrument.BankABARoutingNumber_TDIC
    var encryptionPlugin = Plugins.get("EncryptionByAESPlugin") as IEncryption
    gxPaymentInstrument.BankAccountNumber = encryptionPlugin.encrypt(paymentInstrument.BankAccountNumber_TDIC)
    var primaryContact = paymentInstrument.Account.PrimaryPayer.Contact
    if (primaryContact typeis Company) {
      gxPaymentInstrument.ContactName = primaryContact.Name
    }
    else if (primaryContact typeis Person) {
      gxPaymentInstrument.ContactName = primaryContact.LastName
    }

    return new tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.TDIC_PaymentInstrumentWritableBean(gxPaymentInstrument, options)
  }

}