package tdic.bc.config.enhancement

uses gw.api.domain.account.AccountTransfer  // entity.AccountTransfer

enhancement AccountTransferEnhancement : AccountTransfer {
  /**
   * Move delinquency from old account to new account.
  */
  public function updateDelinquencies() : void {
    for (policyPeriodTransfer in this.PolicyPeriods) {
      for (delinquencyProcess in policyPeriodTransfer.PolicyPeriod.DelinquencyProcesses) {
        delinquencyProcess = this.Bundle.add (delinquencyProcess);
        delinquencyProcess.setFieldValue ("Account", this.ToAccount);
      }
    }
  }

/*
    var currentBundle = gw.transaction.Transaction.Current
    var paymentReceiveds = this.FromAccount.findReceivedPaymentMoneysSortedByReceivedDateDescending()
    for(paymentReceived in paymentReceiveds) {
      paymentReceived = currentBundle.add(paymentReceived)
      paymentReceived.setFieldValue("Account", this.ToAccount)
    }
*/
}
