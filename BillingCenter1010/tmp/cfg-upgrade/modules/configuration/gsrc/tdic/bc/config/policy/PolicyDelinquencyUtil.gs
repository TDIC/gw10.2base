package tdic.bc.config.policy

uses gw.api.system.BCLoggerCategory
uses gw.pl.currency.MonetaryAmount
uses java.util.HashMap
uses java.util.Map
uses java.util.ArrayList
uses java.util.List
uses org.slf4j.LoggerFactory

class PolicyDelinquencyUtil {
  static private var _logger  = LoggerFactory.getLogger("Application.DelinquencyProcess");
  static private var _LOG_TAG = "PolicyDelinquencyUtil - ";

  protected var _policy : Policy;

  public construct (policy : Policy) {
    _policy = policy;
  }

  /**
   * Determine the delinquencies on these policies that can exit and call exitDelinquency on each of them.
  */
  public function exitDelinquencies() : void {
    var amountDue : MonetaryAmount;
    var amountDueMap : HashMap<PolicyPeriod,MonetaryAmount>;
    var cancelPeriodMap : Map<PolicyPeriod,List<PolicyDlnqProcess>>;
    var delinquencies : List<PolicyDlnqProcess>;
    var delinquencyThreshold = _policy.Account.DelinquencyPlan.AcctEnterDelinquencyThreshold;
    var delinquencyUnapplied : MonetaryAmount;
    var satisfiedPeriods : List<PolicyPeriod>;
    var unappliedAmount : MonetaryAmount;

    _logger.info (_LOG_TAG + "Checking policy " + _policy + " for delinquencies that should exit.");
    delinquencies = _policy.ActiveDelinquencyProcesses_TDIC;

    if (delinquencies.HasElements) {
      amountDueMap = new HashMap<PolicyPeriod,MonetaryAmount>();

      // Add policy periods involved with delinquencies to the accountDueMap.
      for (delinquency in delinquencies) {
        amountDueMap.put (delinquency.PolicyPeriod, null);
        if (delinquency.CancelInfo_TDIC.PolicyPeriod != null) {
          amountDueMap.put (delinquency.CancelInfo_TDIC.PolicyPeriod, null);
        }
      }

      // Get amount due for each PolicyPeriod
      for (policyPeriod in amountDueMap.Keys) {
        amountDueMap.put (policyPeriod, policyPeriod.AmountDue_TDIC);
      }

      if (_logger.DebugEnabled) {
        for (policyPeriod in amountDueMap.Keys.orderBy(\pp -> pp.TermNumber)) {
          _logger.debug (_LOG_TAG + "Amount due on " + policyPeriod.PolicyNumberLong
                            + " is " + amountDueMap.get(policyPeriod));
        }
      }

      // Construct the cancel period map.
      cancelPeriodMap = delinquencies.where(\d -> d.CancelInfo_TDIC.PolicyPeriod != null)
                                     .partition(\d -> d.CancelInfo_TDIC.PolicyPeriod);

      // Find periods that are satisfied.
      satisfiedPeriods = new ArrayList<PolicyPeriod>();
      for (policyPeriod in amountDueMap.Keys) {
        amountDue = amountDueMap.get (policyPeriod);

        unappliedAmount = policyPeriod.OverridingInvoiceStream.UnappliedFund.Balance;
        _logger.debug (_LOG_TAG + "Unapplied balance for " + policyPeriod.PolicyNumberLong + " is " + unappliedAmount);

        delinquencyUnapplied = policyPeriod.DelinquencyCancelAmount_TDIC;
        _logger.debug (_LOG_TAG + "Amount of unapplied corresponding to delinquency cancel is " + delinquencyUnapplied);
        unappliedAmount = unappliedAmount - delinquencyUnapplied;

        if ((amountDue - unappliedAmount) <= delinquencyThreshold) {
          _logger.info (_LOG_TAG + "PolicyPeriod " + policyPeriod + " has been satisfied.");
          satisfiedPeriods.add (policyPeriod);
        } else {
          _logger.info (_LOG_TAG + "PolicyPeriod " + policyPeriod + " has NOT been satisfied.");
        }
      }

      // Exit delinquencies if policy periods are satisfied.
      for (delinquency in delinquencies) {
        if (isDelinquencySatisfied (delinquency, satisfiedPeriods, cancelPeriodMap)) {
          _logger.info (_LOG_TAG + "Exiting delinquency " + delinquency);
          delinquency.exitDelinquency();
        } else {
          _logger.info (_LOG_TAG + "Delinquency " + delinquency + " will NOT be exited.");
        }
      }
    }
  }

  /**
   * Determine if the delinquency can be exited.
  */
  protected function isDelinquencySatisfied (delinquency : PolicyDlnqProcess, satisfiedPeriods : List<PolicyPeriod>,
                                             cancelPeriodMap : Map<PolicyPeriod,List<PolicyDlnqProcess>>) : boolean {
    var retval : boolean;

    // Check if the delinquency's policy period is satisfied.
    retval = satisfiedPeriods.contains(delinquency.PolicyPeriod);

    if (retval == true and delinquency.Reason != DelinquencyReason.TC_PASTDUE) {
      var cancelPeriod = delinquency.CancelInfo_TDIC.PolicyPeriod;
      var jobNumber = delinquency.CancelInfo_TDIC.JobNumber;

      if (cancelPeriod == null) {
        // Cancellation has not been requested yet.  This delinquency can exit.
      } else if (jobNumber == null) {
        // Cancellation was started by another delinquency process.  This delinquency can exit.
      } else {
        // This delinquency started the cancellation.  Need to make sure that all other delinquencies that
        // would have canceled this term have been satisfied.
        var associatedDelinquencies = cancelPeriodMap.get(cancelPeriod).where(\d -> d != delinquency);
        for (associatedDelinquency in associatedDelinquencies) {
          if (satisfiedPeriods.contains(associatedDelinquency.PolicyPeriod) == false) {
            retval = false;
            break;
          }
        }
      }
    }

    return retval;
  }
}