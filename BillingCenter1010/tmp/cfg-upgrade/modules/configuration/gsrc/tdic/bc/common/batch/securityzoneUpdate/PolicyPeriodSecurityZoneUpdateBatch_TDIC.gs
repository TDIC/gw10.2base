package tdic.bc.common.batch.securityzoneUpdate

uses gw.api.database.Query
uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory

class PolicyPeriodSecurityZoneUpdateBatch_TDIC extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${PolicyPeriodSecurityZoneUpdateBatch_TDIC.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_POLICYPERIODSECURITYZONEUPATE_TDIC);
  }

  override function checkInitialConditions() : boolean {
    return true;
  }

  override function requestTermination() : boolean {
    return true;
  }

  protected override function doWork() {
    if(ScriptParameters.SecurityZoneUpdated) {
      // find the PolicyPeriods whose SecurityZOne is null and update with CATDIC Security Zone.
      var catdicSecurityZone = Query.make(SecurityZone).compare(SecurityZone#Name, Equals, "CATDIC Security Zone").select().AtMostOneRow
      var policyPeriods = Query.make(PolicyPeriod).compare(PolicyPeriod#SecurityZone, Equals, null).select()
      _logger.info(_LOG_TAG + "Found " + policyPeriods.Count + " PolicyPeriods.")
      for (policyPeriod in policyPeriods) {
        if (this.TerminateRequested) {
          break;
        }
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          policyPeriod = bundle.add(policyPeriod)
          policyPeriod.SecurityZone = catdicSecurityZone
        }, "iu")
      }
    }
  }
}