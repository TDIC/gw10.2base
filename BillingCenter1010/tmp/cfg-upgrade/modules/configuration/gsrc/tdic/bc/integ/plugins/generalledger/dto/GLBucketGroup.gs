package tdic.bc.integ.plugins.generalledger.dto

uses java.util.HashMap
uses java.util.Map

/**
 * US570 - General Ledger Integration
 * 1/14/2015 Alvin Lee
 *
 * Class to hold the different buckets for GL.  There must be one instance of this class per LOB and state combination.
 */
class GLBucketGroup {

  /**
   * A Map<String, GLBucket> to connect the name of the cash receipts bucket to the bucket itself.
   */
  private var _nameBucketMap : Map<String, GLBucket> as NameBucketMap

  construct() {
    _nameBucketMap = new HashMap<String, GLBucket>()

    // Set up Disbursement Paid Buckets
    var disbPaid = new GLBucket()
    disbPaid.Description = "DISBURSEMENT PAID"
    _nameBucketMap.put("DisbPaid", disbPaid)
    var disbPaidOffset = new GLBucket()
    disbPaidOffset.Description = "DISBURSEMENT PAID"
    _nameBucketMap.put("DisbPaidOffset", disbPaidOffset)
    var disbPaidRev = new GLBucket()
    disbPaidRev.Description = "DISBURSEMENT PAID REV"
    _nameBucketMap.put("DisbPaidRev", disbPaidRev)
    var disbPaidRevOffset = new GLBucket()
    disbPaidRevOffset.Description = "DISBURSEMENT PAID REV"
    _nameBucketMap.put("DisbPaidRevOffset", disbPaidRevOffset)

    // Set up Charge Paid From Account Buckets
    var chargePaidFromAccountBilled = new GLBucket()
    chargePaidFromAccountBilled.Description = "CHARGE PAID FROM ACCOUNT"
    _nameBucketMap.put("ChargePaidFromAccountBilled", chargePaidFromAccountBilled)
    var chargePaidFromAccountUnbilled = new GLBucket()
    chargePaidFromAccountUnbilled.Description = "CHARGE PAID FROM ACCOUNT"
    _nameBucketMap.put("ChargePaidFromAccountUnbilled", chargePaidFromAccountUnbilled)
    var chargePaidFromAccountFuture = new GLBucket()
    chargePaidFromAccountFuture.Description = "CHARGE PAID FROM ACCOUNT"
    _nameBucketMap.put("ChargePaidFromAccountFuture", chargePaidFromAccountFuture)
    var chargePaidFromAccountOffset = new GLBucket()
    chargePaidFromAccountOffset.Description = "CHARGE PAID FROM ACCOUNT"
    _nameBucketMap.put("ChargePaidFromAccountOffset", chargePaidFromAccountOffset)
    var chargePaidFromAccountRevBilled = new GLBucket()
    chargePaidFromAccountRevBilled.Description = "CHARGE PAID FROM ACCOUNT REV"
    _nameBucketMap.put("ChargePaidFromAccountRevBilled", chargePaidFromAccountRevBilled)
    var chargePaidFromAccountRevUnbilled = new GLBucket()
    chargePaidFromAccountRevUnbilled.Description = "CHARGE PAID FROM ACCOUNT REV"
    _nameBucketMap.put("ChargePaidFromAccountRevUnbilled", chargePaidFromAccountRevUnbilled)
    var chargePaidFromAccountRevFuture = new GLBucket()
    chargePaidFromAccountRevFuture.Description = "CHARGE PAID FROM ACCOUNT"
    _nameBucketMap.put("ChargePaidFromAccountRevFuture", chargePaidFromAccountRevFuture)
    var chargePaidFromAccountRevOffset = new GLBucket()
    chargePaidFromAccountRevOffset.Description = "CHARGE PAID FROM ACCOUNT REV"
    _nameBucketMap.put("ChargePaidFromAccountRevOffset", chargePaidFromAccountRevOffset)

    // Set up Charge Written Off Buckets
    var writeOffBilled = new GLBucket()
    writeOffBilled.Description = "WRITE-OFF"
    _nameBucketMap.put("WriteOffBilled", writeOffBilled)
    var writeOffUnbilled = new GLBucket()
    writeOffUnbilled.Description = "WRITE-OFF"
    _nameBucketMap.put("WriteOffUnbilled", writeOffUnbilled)
    var writeOffOffset = new GLBucket()
    writeOffOffset.Description = "WRITE-OFF"
    _nameBucketMap.put("WriteOffOffset", writeOffOffset)
    var writeOffRevBilled = new GLBucket()
    writeOffRevBilled.Description = "WRITE-OFF REV"
    _nameBucketMap.put("WriteOffRevBilled", writeOffRevBilled)
    var writeOffRevUnbilled = new GLBucket()
    writeOffRevUnbilled.Description = "WRITE-OFF REV"
    _nameBucketMap.put("WriteOffRevUnbilled", writeOffRevUnbilled)
    var writeOffRevOffset = new GLBucket()
    writeOffRevOffset.Description = "WRITE-OFF REV"
    _nameBucketMap.put("WriteOffRevOffset", writeOffRevOffset)

    // Set up Negative Write-off Buckets
    var negWriteOff = new GLBucket()
    negWriteOff.Description = "NEG WRITE-OFF"
    _nameBucketMap.put("NegWriteOff", negWriteOff)
    var negWriteOffOffset = new GLBucket()
    negWriteOffOffset.Description = "NEG WRITE-OFF"
    _nameBucketMap.put("NegWriteOffOffset", negWriteOffOffset)
    var negWriteOffRev = new GLBucket()
    negWriteOffRev.Description = "NEG WRITE-OFF REV"
    _nameBucketMap.put("NegWriteOffRev", negWriteOffRev)
    var negWriteOffRevOffset = new GLBucket()
    negWriteOffRevOffset.Description = "NEG WRITE-OFF REV"
    _nameBucketMap.put("NegWriteOffRevOffset", negWriteOffRevOffset)

    // Set up Charge Billed Buckets
    var chargeBilledBilled = new GLBucket()
    chargeBilledBilled.Description = "CHARGE BILLED"
    _nameBucketMap.put("ChargeBilledBilled", chargeBilledBilled)
    var chargeBilledUnbilled = new GLBucket()
    chargeBilledUnbilled.Description = "CHARGE BILLED"
    _nameBucketMap.put("ChargeBilledUnbilled", chargeBilledUnbilled)

    // Set up Policy Period Became Effective (Advance Premium)
    var policyPeriodBecameEffBilled = new GLBucket()
    policyPeriodBecameEffBilled.Description = "ADVANCE PREMIUM"
    _nameBucketMap.put("PolicyPeriodBecameEffBilled", policyPeriodBecameEffBilled)
    var policyPeriodBecameEffUnbilled = new GLBucket()
    policyPeriodBecameEffUnbilled.Description = "ADVANCE PREMIUM"
    _nameBucketMap.put("PolicyPeriodBecameEffUnbilled", policyPeriodBecameEffUnbilled)
    var policyPeriodBecameEffOffset = new GLBucket()
    policyPeriodBecameEffOffset.Description = "ADVANCE PREMIUM"
    _nameBucketMap.put("PolicyPeriodBecameEffOffset", policyPeriodBecameEffOffset)

    // Set up Account Collection Transaction Buckets
    var accountCollectionTxn = new GLBucket()
    accountCollectionTxn.Description = "ACCOUNT COLLECTION TRANSACTION"
    _nameBucketMap.put("AccountCollectionTxn", accountCollectionTxn)
    var accountCollectionTxnOffset = new GLBucket()
    accountCollectionTxnOffset.Description = "ACCOUNT COLLECTION TRANSACTION"
    _nameBucketMap.put("AccountCollectionTxnOffset", accountCollectionTxnOffset)
    var accountCollectionTxnRev = new GLBucket()
    accountCollectionTxnRev.Description = "ACCOUNT COLLECTION TRANSACTION REV"
    _nameBucketMap.put("AccountCollectionTxnRev", accountCollectionTxnRev)
    var accountCollectionTxnRevOffset = new GLBucket()
    accountCollectionTxnRevOffset.Description = "ACCOUNT COLLECTION TRANSACTION REV"
    _nameBucketMap.put("AccountCollectionTxnRevOffset", accountCollectionTxnRevOffset)

    // Set up Account Goodwill Transaction Buckets
    var accountGoodwillTxn = new GLBucket()
    accountGoodwillTxn.Description = "ACCOUNT GOODWILL TRANSACTION"
    _nameBucketMap.put("AccountGoodwillTxn", accountGoodwillTxn)
    var accountGoodwillTxnOffset = new GLBucket()
    accountGoodwillTxnOffset.Description = "ACCOUNT GOODWILL TRANSACTION"
    _nameBucketMap.put("AccountGoodwillTxnOffset", accountGoodwillTxnOffset)
    var accountGoodwillTxnRev = new GLBucket()
    accountGoodwillTxnRev.Description = "ACCOUNT GOODWILL TRANSACTION REV"
    _nameBucketMap.put("AccountGoodwillTxnRev", accountGoodwillTxnRev)
    var accountGoodwillTxnRevOffset = new GLBucket()
    accountGoodwillTxnRevOffset.Description = "ACCOUNT GOODWILL TRANSACTION REV"
    _nameBucketMap.put("AccountGoodwillTxnRevOffset", accountGoodwillTxnRevOffset)

    // Set up Account Interest Transaction Buckets
    var accountInterestTxn = new GLBucket()
    accountInterestTxn.Description = "ACCOUNT INTEREST TRANSACTION"
    _nameBucketMap.put("AccountInterestTxn", accountInterestTxn)
    var accountInterestTxnOffset = new GLBucket()
    accountInterestTxnOffset.Description = "ACCOUNT INTEREST TRANSACTION"
    _nameBucketMap.put("AccountInterestTxnOffset", accountInterestTxnOffset)
    var accountInterestTxnRev = new GLBucket()
    accountInterestTxnRev.Description = "ACCOUNT INTEREST TRANSACTION REV"
    _nameBucketMap.put("AccountInterestTxnRev", accountInterestTxnRev)
    var accountInterestTxnRevOffset = new GLBucket()
    accountInterestTxnRevOffset.Description = "ACCOUNT INTEREST TRANSACTION REV"
    _nameBucketMap.put("AccountInterestTxnRevOffset", accountInterestTxnRevOffset)

    // Set up Account Other Transaction Buckets
    var accountOtherTxn = new GLBucket()
    accountOtherTxn.Description = "ACCOUNT OTHER TRANSACTION"
    _nameBucketMap.put("AccountOtherTxn", accountOtherTxn)
    var accountOtherTxnOffset = new GLBucket()
    accountOtherTxnOffset.Description = "ACCOUNT OTHER TRANSACTION"
    _nameBucketMap.put("AccountOtherTxnOffset", accountOtherTxnOffset)
    var accountOtherTxnRev = new GLBucket()
    accountCollectionTxnRev.Description = "ACCOUNT OTHER TRANSACTION REV"
    _nameBucketMap.put("AccountOtherTxnRev", accountCollectionTxnRev)
    var accountOtherTxnRevOffset = new GLBucket()
    accountOtherTxnRevOffset.Description = "ACCOUNT OTHER TRANSACTION REV"
    _nameBucketMap.put("AccountOtherTxnRevOffset", accountOtherTxnRevOffset)
  }

}