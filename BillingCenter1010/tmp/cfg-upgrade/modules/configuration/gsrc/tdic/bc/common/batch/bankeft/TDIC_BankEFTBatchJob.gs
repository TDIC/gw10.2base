package tdic.bc.common.batch.bankeft

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.util.DateUtil
uses gw.processes.BatchProcessBase
uses java.sql.Connection
uses java.sql.ResultSet
uses java.lang.Exception
uses gw.api.system.server.ServerUtil
uses gw.pl.exception.GWConfigurationException
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.BankEFTFile
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestBeanInfo
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestWritableBean
uses tdic.bc.integ.plugins.bankeft.helper.pymntrequest.TDIC_BankEFTHelper
uses java.sql.SQLException
uses java.io.File
uses org.apache.commons.io.FileUtils
uses java.io.IOException
uses com.tdic.util.misc.FtpUtil
uses com.tdic.util.misc.EmailUtil
uses gw.plugin.Plugins
uses gw.plugin.util.IEncryption
uses java.math.BigDecimal
uses org.slf4j.LoggerFactory

/**
 * US568 - Bank/EFT Integration
 * 09/25/2014 Alvin Lee
 *
 * Custom batch job for the Bank/EFT integration to generate payment request flat files to be sent to the bank.
 */
class TDIC_BankEFTBatchJob extends BatchProcessBase {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_EFT")

  /**
   * Name of the Bank EFT flat file, hardcoded as this is not likely to change, unless the entire system changes (value: INPUT.DAT).
   */
  public static final var FILENAME : String = "INPUT.DAT"

  /**
   * Key for looking up the file output directory from the integration database (value: eft.outputdir).
   */
  public static final var OUTPUT_DIR_PATH : String = "eft.outputdir"

  /**
   * Key for looking up the eft file created email addresses from the properties file (value: PaymentNotificationEmail).
   */
  public static final var FILE_NOTIFICATION_EMAIL_KEY : String = "EFTFileNotificationEmail"

  /**
   * Key for looking up the notification email addresses from the integration database (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Name of the vendor for purposes of connecting to the FTP (value: BofaOutgoingACH).
   */
  public static final var VENDOR_NAME : String = "BofaOutgoingACH"

  /**
   * Absolute directory path/string to use to write files.
   */
  private var _destinationDirectoryPath : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Notification email addresses in case of eft outbound file succesfully created.
   */
  private var _fileNotificationEmail : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for Bank/EFT batch job completion.
   */
  public static final var EMAIL_SUBJECT_COMPLETED: String = "Bank/EFT Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Bank/EFT batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Bank/EFT Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId
  public static final var EMAIL_SUBJECT_INVOICE: String = "Bank/EFT Batch Job - Invoices with BillDate not on 1st of the Month  in " + gw.api.system.server.ServerUtil.getEnv()

  /**
   * The connection to the external database.
   */
  private var _dbConnection:Connection = null

  /**
   * Payment requests to process as a part of a single batch run.
   */
  private var _paymentRequestResults : ResultSet = null

  /**
   * The PaymentRequestBeanInfo to interact with the external database.
   */
  private var _beanInfo : PaymentRequestBeanInfo = null

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_BANKEFTBATCHPROCESS)
  }

  /**
   * Runs the actual batch process.
   */
  override function doWork() {
    _logger.info("TDIC_BankEFTBatchJob#doWork() - Entering")
    // GINTEG-1135 : Check whether we have any data to process.
    _logger.info("TDIC_CashReceiptsBatchJob#doWork() - Check whether we have any data to process")
    if (checkInitialConditions_TDIC()) {
      // Set up file to write data
      var destFile = new File(_destinationDirectoryPath + "/" + FILENAME)

      _logger.debug("TDIC_BankEFTBatchJob#doWork() - Destination file constructed: ${destFile.AbsolutePath}")

      var flatFileData = new BankEFTFile()
      try {
        while (_paymentRequestResults.next()) {
          // Check TerminateRequested flag before proceeding with each entry
          if (TerminateRequested) {
            _logger.info("TDIC_BankEFTBatchJob#doWork() - Terminate requested during doWork() method. Progress: " + Progress + " rows completed.")
            // Write lines and then commit transaction
            if (flatFileData.PaymentRequests != null && !flatFileData.PaymentRequests.Empty) {
              FileUtils.writeLines(destFile, TDIC_BankEFTHelper.convertDataToFlatFileLines(flatFileData))
            }
            _dbConnection.commit()
            return
          }

          // Map fields to object
          var paymentRequestBean = new PaymentRequestWritableBean()
          paymentRequestBean.DueDate = _paymentRequestResults.getString("DueDate")
          paymentRequestBean.InvoiceNumber = _paymentRequestResults.getString("InvoiceNumber")
          paymentRequestBean.Amount = _paymentRequestResults.getBigDecimal("Amount").multiply(new BigDecimal(100)).longValue()
          paymentRequestBean.ContactName = _paymentRequestResults.getString("ContactName").toUpperCase()
          paymentRequestBean.BankRoutingNumber = _paymentRequestResults.getString("BankRoutingNumber")
          var encryptionPlugin = Plugins.get("EncryptionByAESPlugin") as IEncryption
          paymentRequestBean.BankAccountNumber = encryptionPlugin.decrypt(_paymentRequestResults.getString("BankAccountNumber"))


          //Increment operations completed for the purpose of keeping progress.  Errors are not incremented since a single failure terminates all.
          flatFileData.addToPaymentRequests(paymentRequestBean)
          incrementOperationsCompleted()

          // Mark the line as processed
          paymentRequestBean.PublicID = _paymentRequestResults.getString(_beanInfo.IDColumnSQLName)
          paymentRequestBean.markAsProcessed(_dbConnection)
        }

        // Write lines and then commit transaction
        if (flatFileData.PaymentRequests != null && !flatFileData.PaymentRequests.Empty) {
          FileUtils.writeLines(destFile, TDIC_BankEFTHelper.convertDataToFlatFileLines(flatFileData))
        }

        _dbConnection.commit()
        var totalAmout =  flatFileData.TotalAmount?.toString()
        if(totalAmout.length() >= 2){
          totalAmout = totalAmout.substring(0,totalAmout.length()-2)+"."+totalAmout.substring(totalAmout.length()-2)
        }
        EmailUtil.sendEmail(_fileNotificationEmail, EMAIL_SUBJECT_COMPLETED, "The Bank/EFT batch process has completed successfully with no errors. Total Amount sent is :- $$ " + totalAmout)


      } catch (ioe : IOException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_BankEFTBatchJob#doWork() - Error while writing to output file " + destFile.AbsolutePath, ioe)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, "Bank/EFT batch job failed on Server: " + gw.api.system.server.ServerUtil.ServerId, "The Bank/EFT batch job has failed due to an error while writing to output file: " + ioe.LocalizedMessage)
        }
        _dbConnection.rollback()
      } catch (sqle : SQLException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_BankEFTBatchJob#doWork() - Database connection error while attempting to access integration database table", sqle)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, "Bank/EFT batch job failed on Server: " + gw.api.system.server.ServerUtil.ServerId, "The Bank/EFT batch job has failed due to a database connection error while attempting to access the integration database: " + sqle.LocalizedMessage)
        }
        _dbConnection.rollback()
      } catch (e : Exception) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_BankEFTBatchJob#doWork() - Error processing output file: " + e.LocalizedMessage, e)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, "Bank/EFT batch job failed on Server: " + gw.api.system.server.ServerUtil.ServerId, "The Bank/EFT batch job has failed due to an unexpected error while processing the output file: " + e.LocalizedMessage)
        }
        _dbConnection.rollback()
      } finally {
        try {
          if (_dbConnection != null) {
            DatabaseManager.closeConnection(_dbConnection)
          }
        } catch (e : Exception) {
          _logger.error("TDIC_BankEFTBatchJob#doWork() - Exception on closing DB (continuing)", e)
        }
      }
    }
    _logger.info("TDIC_BankEFTBatchJob#doWork() - Finished processing")
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are payment requests to process into a flat file.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  function checkInitialConditions_TDIC() : boolean {
    _logger.debug("TDIC_BankEFTBatchJob#checkInitialConditions() - Entering")

    var dbUrl : String
    try {
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }

      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _fileNotificationEmail = PropertyUtil.getInstance().getProperty(FILE_NOTIFICATION_EMAIL_KEY)
      if (_fileNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FILE_NOTIFICATION_EMAIL_KEY + "' from properties file.")
      }

      _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
      if (_failureNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _destinationDirectoryPath = PropertyUtil.getInstance().getProperty(OUTPUT_DIR_PATH)
      if (_destinationDirectoryPath == null) {
        throw new GWConfigurationException("Cannot retrieve file output path with the key '" + OUTPUT_DIR_PATH
            + "' from integration database.")
      }
    } catch (gwce : GWConfigurationException) {
      // Send email notification of failure
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
          "The Bank/EFT batch job has failed due to the integration database either being not property set up or missing required properties.")
      throw gwce
    }

    try {
      // Get a connection from DatabaseManager using the GWINT DB URL
      _dbConnection = DatabaseManager.getConnection(dbUrl)
      _beanInfo = new PaymentRequestBeanInfo()
      var stmt = _beanInfo.createRetrieveAllSQL(_dbConnection)
      _paymentRequestResults = stmt.executeQuery()
      _paymentRequestResults.last()
      // If the last payment request row is row 0, there are no payment requests to process
      if (_paymentRequestResults.Row != 0) {

        var totalPR = Query.make(PaymentRequest).compare(PaymentRequest#Status, Relop.NotEquals, PaymentRequestStatus.TC_CANCELED)
            .compare(PaymentRequest#Status, Relop.NotEquals, PaymentRequestStatus.TC_CLOSED).select()

        _logger.info("TDIC_BankEFTBatchJob#checkInitialConditions() - Results returned. Number of payment requests to be processed in GWINT: " + _paymentRequestResults.Row)
        OperationsExpected = _paymentRequestResults.Row
        _paymentRequestResults.beforeFirst()
       /*If there are any invoices whose bill Date doesnt fall on 1st of the month, send an notification email. this may cause an additional file to be sent for these invoices to bank. */
        var invoices = Query.make(Invoice).compare(Invoice#Status , Equals, InvoiceStatus.TC_PLANNED).select()
        var invResults = invoices.where(\elt ->  elt.EventDate.MonthOfYear == DateUtil.currentDate().MonthOfYear and elt.EventDate.DayOfMonth != 1 and
              elt.EventDate.DayOfMonth > DateUtil.currentDate().DayOfMonth and elt.AmountDue.Amount >0 and elt.Status == InvoiceStatus.TC_PLANNED
              and elt.InvoiceStream.getOriginalPaymentInstrument().PaymentMethod == PaymentMethod.TC_ACH)

        var invList = new StringBuilder()
        if(invResults.Count > 0 ) {
          for (inv in invResults) {
            if (!totalPR.hasMatch(\elt -> elt.Invoice == inv)) {
              invList.append(inv.InvoiceNumber)
              invList.append("\n")
            }
          }
          _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
          if (_notificationEmail != null) {
            EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_INVOICE,
                "The Bank/EFT batch found the below invoices with bill date not on 1st of the month. \n " + invList)
            _logger.info("TDIC_BankEFTBatchJob#checkInitialConditions() - The Bank/EFT batch found the below invoices with bill date not on 1st of the month. \n " + invList)
          }
        }
        return true


      }
      else {
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
              "The Bank/EFT batch job did not run, due to having no payment requests to be processed.")
        }
        _logger.warn("TDIC_BankEFTBatchJob#checkInitialConditions() - No payment requests to be processed. Exiting batch process.")
        // Attempt to close connection
        try {
          if (_dbConnection != null) {
            DatabaseManager.closeConnection(_dbConnection)
          }
        } catch (e : Exception) {
          _logger.error("TDIC_BankEFTBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
        }
        return false
      }
    } catch (sqle : SQLException) {
      _logger.error("TDIC_BankEFTBatchJob#checkInitialConditions() - Database connection error while attempting to read integration database", sqle)
      // Send email notification of failure
      if(_failureNotificationEmail != null){
        EmailUtil.sendEmail(_failureNotificationEmail, "Database connection error on Server:"+gw.api.system.server.ServerUtil.ServerId, "Database connection error while attempting to read integration database" + sqle.LocalizedMessage)
      }
      // Attempt to close connection
      try {
        if (_dbConnection != null) {
          DatabaseManager.closeConnection(_dbConnection)
        }
      } catch (e : Exception) {
        _logger.error("TDIC_BankEFTBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
      }
      throw sqle
    } catch (e : Exception) {
      // Handle other errors
      _logger.error("TDIC_BankEFTBatchJob#checkInitialConditions() - Unexpected error while attempting to read integration database", e)
      // Send email notification of failure
      if(_failureNotificationEmail != null){
        EmailUtil.sendEmail(_failureNotificationEmail, "Unexpected error on server: " +gw.api.system.server.ServerUtil.ServerId,"Unexpected error while attempting to read integration database" + e.LocalizedMessage)
      }
      // Attempt to close connection
      try {
        if (_dbConnection != null) {
          DatabaseManager.closeConnection(_dbConnection)
        }
      } catch (e2 : Exception) {
        _logger.error("TDIC_BankEFTBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e2)
      }
      throw e
    }
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   *
   * @return A boolean to indicate that the process can be stopped.
   */
  override function requestTermination() : boolean {
    _logger.info("TDIC_BankEFTBatchJob#requestTermination() - Terminate requested for batch process.")
    // Set TerminateRequested to true by calling super method
    super.requestTermination()
    return true
  }
}