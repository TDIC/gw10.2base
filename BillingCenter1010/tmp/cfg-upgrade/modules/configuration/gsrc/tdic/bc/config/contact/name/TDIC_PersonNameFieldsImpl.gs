package tdic.bc.config.contact.name

uses gw.api.name.PersonNameFieldsImpl

/**
 * US1129,
 * 01/21/2015 Vicente
 *
 * Name fields used for Persons at TDIC.
 */
class TDIC_PersonNameFieldsImpl implements TDIC_PersonNameFields {

  var _firstName : String as FirstName
  var _lastName : String as LastName
  var _firstNameKanji : String as FirstNameKanji
  var _lastNameKanji : String as LastNameKanji
  var _middleName : String as MiddleName
  var _particle : String as Particle
  var _prefix : NamePrefix as Prefix
  var _suffix : NameSuffix as Suffix
  var _credential : Credential_TDIC as Credential_TDIC

  // not used for Person entities:
  var _name : String as Name
  var _nameKanji : String as NameKanji
}