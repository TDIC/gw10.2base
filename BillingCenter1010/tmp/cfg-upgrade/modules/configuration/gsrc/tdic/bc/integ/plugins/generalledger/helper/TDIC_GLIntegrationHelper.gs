package tdic.bc.integ.plugins.generalledger.helper

uses com.tdic.util.delimitedfile.DelimitedFileImpl
uses com.tdic.util.properties.PropertyUtil
uses entity.Transaction
uses gw.api.database.Query
uses gw.api.gx.GXOptions
uses gw.api.system.database.SequenceUtil
uses gw.api.util.DateUtil
uses gw.pl.persistence.core.Bundle
uses gw.xml.XmlElement
uses org.apache.commons.lang3.StringUtils
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.generalledger.dto.GLBucket
uses tdic.bc.integ.plugins.generalledger.dto.GLBucketGroup
uses tdic.bc.integ.plugins.generalledger.dto.GLFile
uses tdic.bc.integ.plugins.generalledger.dto.GLRuntimeException
uses tdic.bc.integ.plugins.generalledger.dto.GLTransaction
uses tdic.bc.integ.plugins.generalledger.dto.GLTransactionLineWritableBean

uses java.math.BigDecimal
uses java.text.SimpleDateFormat

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 * <p>
 * Class with helper methods related to GL Integration for transactions
 */
class TDIC_GLIntegrationHelper {

  /**
   * A String containing the Event Name for TransactionAdded.
   */
  public static final var TRANSACTION_ADDED : String = "TransactionAdded"
  /**
   * A string containing the email id.
   */

  public static final var EMAIL_RECIPIENT : String = PropertyUtil.getInstance().getProperty("InfraIssueNotificationEmail")
  /**
   * Standard logger for General Ledger
   */
  private static final var _logger = LoggerFactory.getLogger("GENERAL_LEDGER")
  /**
   * A map for Guidewire's transaction names to the general ledger names/codes.
   */
  private static var _glTransactionNamesMap : Map<String, String> = null

  private static final var surchargePatterens = {"NJIGASurcharge_TDIC", "FireFighterReliefSurcharge_TDIC", "FireSafetySurcharge_TDIC", "CIGASurcharge"}

  /**
   * Default constructor with no parameters.
   */
  construct() {
  }

  /**
   * Helper function to add an entity to the Guidewire database from a UI screen
   *
   * @param entity - keyable bean to add to the current bundle/commit if it's not already in a bundle
   */
  static function addEntity<T extends KeyableBean>(entity : T) {
    _logger.debug("TDIC_GLIntegrationHelper#addEntity")
    if (entity.getBundle() == null) {
      var currentBundle = gw.transaction.Transaction.getCurrent()
      currentBundle.add(entity)
      currentBundle.commit()
    }
  }

  /**
   * Helper function to delete an entity to the Guidewire database from a UI screen
   *
   * @param entity - keyable bean to delete from the current bundle/commit if it's not already in a bundle
   */
  static function deleteEntity<T extends KeyableBean>(entity : T) {
    _logger.debug("TDIC_GLIntegrationHelper#deleteEntity")
    var currentBundle : Bundle = null

    if (entity.getBundle() == null) {
      currentBundle = gw.transaction.Transaction.getCurrent()
    } else {
      currentBundle = entity.getBundle()
    }
    currentBundle.remove(entity)
    currentBundle.commit()
  }

  /**
   * Helper function to take line items from a transaction and map them to a GX model to be written to the database
   *
   * @param trans - The Transaction entity containing the line items to be written to the database
   * @return XML model object with transaction data for the line items to be recorded in the GL system
   */
  static function mapToGLTransactionModel(trans : Transaction) : tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.GLTransaction {
    _logger.debug("TDIC_GLIntegrationHelper#mapToGLTransactionModel - Entering for Transaction: " + trans.Subtype)

    //Define GL Names Map each time, just in case the mapping has been changed since the last time this has run.
    mapGLCodes()

    //Build XML Model options object
    var options = new GXOptions()
    options.Incremental = false // Send all values, not just changed ones
    options.Verbose = true // Send all fields, even nulls

    var sdfTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    //Build our transaction object
    var gxTransaction = new GLTransaction()

    //Build the line items
    for (line in trans.LineItems) {
      gxTransaction.addToLineItems(mapToGLTransModelProperties(line, trans, false))
      //GWPS-926 i.e., to send Final Audit interest as IN both DEP and WTH records in Cash Receipts Report
      if (trans typeis DirectBillMoneyReceivedTxn and trans.DirectBillMoneyRcvd.PaymentInstrument.PaymentMethod == PaymentMethod.TC_CHECK) {
        if (trans.DirectBillMoneyRcvd.RefNumber?.equalsIgnoreCase("FA INT V4")) {
          gxTransaction.addToLineItems(mapToGLTransModelProperties(line, trans, true))
        }
      }
    }
    //Create our model
    return new tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.GLTransaction(gxTransaction, options)
  }

  //GWPS-926 -- Moved mapping related to transaction model properties from mapToGLTransactionModel to the below method
  static function mapToGLTransModelProperties(line : LineItem, trans : Transaction, faInterestRecInd : boolean) : GLTransactionLineWritableBean {
    var sdfTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var gxLineItem = new GLTransactionLineWritableBean()
    //added fa to PublicID to differntiate the record between DEP and WTH while updating the Processed Column to 1
    gxLineItem.PublicID = faInterestRecInd ? line.PublicID.concat("-fa") : line.PublicID
    gxLineItem.Description = trans.DisplayName // using display name of transaction as the description
    gxLineItem.Amount = line.Amount // This is a signed value
    gxLineItem.LineItemType = line.Type as String // typecode mapper can be used here
    gxLineItem.TransactionDate = sdfTimestamp.format(trans.TransactionDate) // date of the transaction
    gxLineItem.TransactionSubtype = trans.Subtype as String // Transaction subtype
    gxLineItem.CreatedDate = sdfTimestamp.format(line.CreateTime)  // date the entry was created
    gxLineItem.CreatedUser = line.CreateUser.DisplayName
    gxLineItem.TransactionNumber = trans.TransactionNumber

    //Use the mapped transaction name, if available
    var transactionName = _glTransactionNamesMap.get(trans.Subtype as String)
    gxLineItem.GWTransactionName = trans.Subtype.DisplayName // Original Transaction name
    gxLineItem.MappedTransactionName = transactionName != null ? transactionName : trans.Subtype.DisplayName // Mapped Transaction name

    //Use the mapped T-account name, if available
    gxLineItem.GWTAccountName = line.TAccount.TAccountPattern.DisplayName
    gxLineItem.MappedTAccountName = faInterestRecInd ? getTAccNameForFAInterest(line) : getMappedTAccountName(line)

    var container = line.TAccount.TAccountContainer

    //Check where we can get policy period data
    var pp : PolicyPeriod = null
    if (container typeis PolTAcctContainer) {
      pp = container.PolicyPeriod
    } else if (trans typeis ProducerTransaction && trans.PolicyPeriod != null) {
      pp = trans.PolicyPeriod
    }
    if (pp != null) {
      gxLineItem.AccountNumber = pp.Account.AccountNumber
      gxLineItem.AccountName = pp.Account.AccountName
      gxLineItem.UWCompany = pp.UWCompany.DisplayName
      gxLineItem.LOB = getMappedLOBAccountCode(pp)
      gxLineItem.StateCode = pp.RiskJurisdiction.Code
      gxLineItem.PolicyEffectiveDate = pp.EffectiveDate as String
      gxLineItem.PolicyNumber = pp.PolicyNumber
      gxLineItem.PolicyPeriodStatus = pp.Status.Code
      gxLineItem.TermNumber = pp.TermNumber
    }
    //Otherwise, for account-level transactions, just populate the account data
    else if (container typeis AcctTAcctContainer) {
      gxLineItem.AccountNumber = container.Account.AccountNumber
      gxLineItem.AccountName = container.Account.AccountName
      //If a Designated Unapplied T-Account, populate information from the Policy entity since the UnappliedFund is accessible
      if (line.TAccount.TAccountPattern.TAccountName == "Designated Unapplied") {
        var unappliedQuery = gw.api.database.Query.make(UnappliedFund).compare(UnappliedFund#TAccount, Equals, line.TAccount)
        var unapplied = unappliedQuery.select().FirstResult
        if (unapplied.Policy != null) {
          gxLineItem.PolicyNumber = unapplied.Policy.DisplayName
          gxLineItem.LOB = getMappedLOBAccountCode(unapplied.PolicyPeriod_TDIC)
          //Assumption is made that the RiskJurisdiction must be the same across all policy periods under a single Policy entity
          gxLineItem.StateCode = unapplied.Policy.LatestPolicyPeriod.RiskJurisdiction.Code
        }
      }
    }

    //Transaction specific mappings - add more project specific transaction mappings here
    if (trans typeis DirectBillMoneyReceivedTxn) {
      gxLineItem.DirectBillMoneyRcvdPublicID = trans.DirectBillMoneyRcvd.PublicID
      gxLineItem.PaymentMethod = trans.DirectBillMoneyRcvd.PaymentInstrument.PaymentMethod.Code
      if (trans.DirectBillMoneyRcvd.PaymentInstrument.PaymentMethod == PaymentMethod.TC_CREDITCARD) {
        gxLineItem.CreditCardType = trans.DirectBillMoneyRcvd.PaymentInstrument.CreditCardType_TDIC.Code
      }
      gxLineItem.PaymentReversalReason = trans.DirectBillMoneyRcvd.ReversalReason.Code
    }
    if (trans typeis SuspPymtTransaction && container typeis SuspPymtTAcctContainer) {
      gxLineItem.AccountNumber = container.SuspensePayment.AccountNumber
      gxLineItem.PolicyNumber = container.SuspensePayment.PolicyNumber
      gxLineItem.DirectBillMoneyRcvdPublicID = container.SuspensePayment.PaymentMoneyReceived.PublicID
      if (_logger.DebugEnabled && gxLineItem.DirectBillMoneyRcvdPublicID != null) {
        _logger.debug("TDIC_GLIntegrationHelper#mapToGLTransactionModel - Suspense Payment Applied. Storing PaymentMoneyReceived.PublicID '"
            + gxLineItem.DirectBillMoneyRcvdPublicID + "' for the applied payment.")
      }
      gxLineItem.PaymentMethod = container.SuspensePayment.PaymentInstrument.PaymentMethod.Code
    }
    if (trans typeis ChargeInstanceTxn) {
      gxLineItem.ChargeName = trans.Charge.DisplayName
    }
    if (trans typeis ChargeInvoicingTxn) {
      gxLineItem.InvoiceStatus = trans.InvoiceItem.Invoice.Status.Code
    }
    return gxLineItem
  }

  /**
   * Method to get TAccount Name for DirectBillRecvd Transaction for Final Audit Interest
   * @param line : LineItem
   * @return MAppedTAccountName for FA Interest i.e, 6790-0000
   */
  static function getTAccNameForFAInterest(line : LineItem) : String {
    _logger.debug("TDIC_GLIntegrationHelper#getTAccNameForFAInterest - Entering")
    var query = Query.make(GLIntegrationTAccountMapping_TDIC)
    query.compare(GLIntegrationTAccountMapping_TDIC#TranasctionType, Equals, line.Transaction.Subtype)
    query.compare(GLIntegrationTAccountMapping_TDIC#LineItemType, Equals, line.Type)
    query.compare(GLIntegrationTAccountMapping_TDIC#OriginalTAccountName, Equals, GLOriginalTAccount_TDIC.TC_FAINTERESTUNAPPLIED)
    var qr = query.select()?.first()
    if (line.TAccount.TAccountPattern.DisplayName.endsWithIgnoreCase("Unapplied"))
      return qr.MappedTAccountName
    else
      return line.TAccount.TAccountPattern.DisplayName
  }

  /**
   * Helper function to take a policy period and map it to writable beans to be written to the database
   *
   * @param policyPeriod The PolicyPeriod object with the status change
   * @return A GLTransaction object containing writable beans for the policy period status change to be recorded in the database
   */
  static function mapToPolicyPeriodEffectiveTransaction(policyPeriod : PolicyPeriod) : GLTransaction {
    _logger.debug("TDIC_GLIntegrationHelper#mapToPolicyPeriodEffectiveTransaction - Entering for policy period " + policyPeriod)

    //Define GL Names Map each time, just in case the mapping has been changed since the last time this has run.
    mapGLCodes()

    var sdfTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var currentTimestamp = sdfTimestamp.format(DateUtil.currentDate())

    //Build our transaction object
    var glTransaction = new GLTransaction()

    // Suppressing GL transaction on Reinstatement GWPS-169
    if (policyPeriod.Charges.sortBy(\charge -> charge.ChargeDate).last()?.BillingInstruction typeis Reinstatement) {
      _logger.info("Skipping GL Transaction on Reinstatement for Policy : " + policyPeriod.PolicyNumber)
      return null
    }

    // Check for total amount paid (for billed, due, and unbilled invoices)
    if (policyPeriod.PaymentsReceived_TDIC.Amount.compareTo(BigDecimal.ZERO) > 0) {
      _logger.debug("TDIC_GLIntegrationHelper#mapToPolicyPeriodEffectiveTransaction - Billed Amount: "
          + policyPeriod.BilledAmount + " | Due Amount: " + policyPeriod.DueAmount + " | Unbilled Amount: "
          + policyPeriod.UnbilledAmount)
      _logger.debug("TDIC_GLIntegrationHelper#mapToPolicyPeriodEffectiveTransaction - Total Amount Paid: "
          + policyPeriod.PaymentsReceived_TDIC.Amount + " | Paid Amount Toward Billed: " + policyPeriod.PaidAmountTowardBilledInvoices
          + " | Paid Amount Toward Unbilled: " + policyPeriod.PaidAmountTowardUnbilledInvoices)
      // Create Line Item for debiting Future T-Account
      var futureLineItem = createDefaultPolicyPeriodEffectiveLineItem(policyPeriod, currentTimestamp)
      _logger.debug("TDIC_GLIntegrationHelper#mapToPolicyPeriodEffectiveTransaction - Total Paid Amount being moved from future T-Account: " + policyPeriod.PaidAmount)
     /*GWPS-694 - Use the field PaymentsReceived_TDIC instead of PaidAmount on PoicyPeriod as this is fetching the system adjusted value instead os actual payments*/
      // futureLineItem.Amount = policyPeriod.PaidAmount
      futureLineItem.Amount = policyPeriod.PaymentsReceived_TDIC.Amount
      futureLineItem.LineItemType = LedgerSide.TC_DEBIT.Code
      futureLineItem.GWTAccountName = GLOriginalTAccount_TDIC.TC_FUTURE.Code
      futureLineItem.MappedTAccountName = getMappedTAccountName(typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE, LedgerSide.TC_DEBIT, true, null, futureLineItem.GWTAccountName)
      glTransaction.addToLineItems(futureLineItem)
      if (policyPeriod.PaidAmountTowardBilledInvoices.compareTo(BigDecimal.ZERO) > 0) {
        // Create Line Item for crediting Billed
        _logger.debug("TDIC_GLIntegrationHelper#mapToPolicyPeriodEffectiveTransaction - Non-zero Billed/Due Amount - Creating transaction line items for Billed Amount")
        var billedLineItem = createDefaultPolicyPeriodEffectiveLineItem(policyPeriod, currentTimestamp)
        billedLineItem.Amount = policyPeriod.PaidAmountTowardBilledInvoices
        billedLineItem.LineItemType = LedgerSide.TC_CREDIT.Code
        billedLineItem.GWTAccountName = GLOriginalTAccount_TDIC.TC_BILLED.Code
        billedLineItem.MappedTAccountName = getMappedTAccountName(typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE, LedgerSide.TC_CREDIT, false, InvoiceStatus.TC_BILLED, billedLineItem.GWTAccountName)
        glTransaction.addToLineItems(billedLineItem)
      }
      if (policyPeriod.PaidAmountTowardUnbilledInvoices.compareTo(BigDecimal.ZERO) > 0) {
        // Create Line Item for crediting Unbilled
        _logger.debug("TDIC_GLIntegrationHelper#mapToPolicyPeriodEffectiveTransaction - Non-zero Planned Amount - Creating transaction line items for Unbilled Amount")
        var unbilledLineItem = createDefaultPolicyPeriodEffectiveLineItem(policyPeriod, currentTimestamp)
        unbilledLineItem.Amount = policyPeriod.PaidAmountTowardUnbilledInvoices
        unbilledLineItem.LineItemType = LedgerSide.TC_CREDIT.Code
        unbilledLineItem.GWTAccountName = GLOriginalTAccount_TDIC.TC_UNBILLED.Code
        unbilledLineItem.MappedTAccountName = getMappedTAccountName(typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE, LedgerSide.TC_CREDIT, false, InvoiceStatus.TC_PLANNED, unbilledLineItem.GWTAccountName)
        glTransaction.addToLineItems(unbilledLineItem)
      }
      //GWPS-2105 : Sending the undistributed amount to GL in unapplied bucket as a credit item
      if(policyPeriod.undistributedAmount_TDIC > 0) {
        var unappliedLineItem = createDefaultPolicyPeriodEffectiveLineItem(policyPeriod, currentTimestamp)
        _logger.debug("TDIC_GLIntegrationHelper#mapToPolicyPeriodEffectiveTransaction - Total undistributed amount : " + policyPeriod.undistributedAmount_TDIC)
        unappliedLineItem.Amount = policyPeriod.undistributedAmount_TDIC
        unappliedLineItem.LineItemType = LedgerSide.TC_CREDIT.Code
        unappliedLineItem.GWTAccountName = GLOriginalTAccount_TDIC.TC_UNAPPLIED.Code
        unappliedLineItem.MappedTAccountName = getMappedTAccountName(typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE, LedgerSide.TC_CREDIT, false, null, unappliedLineItem.GWTAccountName)
        glTransaction.addToLineItems(unappliedLineItem)
      }
    } else {
      _logger.debug("TDIC_GLIntegrationHelper#mapToPolicyPeriodEffectiveTransaction - No amount paid on policy period "
          + policyPeriod + ", and therefore no amounts to move for the policy period becoming effective. Exiting.")
    }
    return glTransaction
  }

  private static function createDefaultPolicyPeriodEffectiveLineItem(policyPeriod : PolicyPeriod, currentTimestamp : String) : GLTransactionLineWritableBean {
    var glLineItem = new GLTransactionLineWritableBean()
    glLineItem = new GLTransactionLineWritableBean()
    glLineItem.PublicID = "ppeff:" + SequenceUtil.next(0, "PolicyPeriodEffective")
    glLineItem.Description = "Policy Period Effective Date Status Change"
    glLineItem.GWTransactionName = "Policy Period Effective Date Status Change"
    var transactionName = _glTransactionNamesMap.get(typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE.Code)
    glLineItem.MappedTransactionName = transactionName != null ? transactionName : typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE.Code
    glLineItem.TransactionDate = currentTimestamp
    glLineItem.TransactionSubtype = typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE.Code
    glLineItem.CreatedDate = currentTimestamp
    glLineItem.CreatedUser = "System User"
    glLineItem.AccountNumber = policyPeriod.Account.AccountNumber
    glLineItem.AccountName = policyPeriod.Account.AccountName
    glLineItem.UWCompany = policyPeriod.UWCompany.DisplayName
    glLineItem.LOB = policyPeriod.Policy.LOBCode.Code
    glLineItem.StateCode = policyPeriod.RiskJurisdiction.Code
    glLineItem.PolicyEffectiveDate = policyPeriod.EffectiveDate as String
    glLineItem.PolicyNumber = policyPeriod.PolicyNumber
    glLineItem.PolicyPeriodStatus = policyPeriod.Status.Code
    glLineItem.TermNumber = policyPeriod.TermNumber
    return glLineItem
  }

  /**
   * Maps the GL account codes from the GLIntegrationTransactionMapping entity.
   */
  private static function mapGLCodes() {
    _glTransactionNamesMap = new HashMap<String, String>()
    var mapQuery = Query.make(GLIntegrationTransactionMapping_TDIC)
    var mapResults = mapQuery.select()
    for (currentMapping in mapResults) {
      _glTransactionNamesMap.put(currentMapping.OriginalTransactionName.Code, currentMapping.MappedTransactionName)
    }
  }

  /*
   * This method returns the mapped T-account name using the line item from the Transaction.
   */
  @Param("line", "The LineItem from which to retrieve the T-account name")
  @Returns("A String containing the mapped T-account name")
  public static function getMappedTAccountName(line : LineItem) : String {
    _logger.debug("TDIC_GLIntegrationHelper#getMappedTAccountName - Entering")
    var query = Query.make(GLIntegrationTAccountMapping_TDIC)
    query.compare(GLIntegrationTAccountMapping_TDIC#TranasctionType, Equals, line.Transaction.Subtype)
    query.compare(GLIntegrationTAccountMapping_TDIC#LineItemType, Equals, line.Type)

    for (qr in query.select()) {
      //Note: GINTEG-1187: New credit transactions AccountCollectionTxn ,AccountGoodwillTxn ,AccountInterestTxn ,AccountOtherTxn
      // are added as part of R2 BC GL.Since Original T-Account Name is Ad hoc credit and unapplied ,New condition to march
      // Original TAccount Name with FundsSourceType for the above credit transactons.
      if ((line.TAccount.TAccountPattern.DisplayName.endsWith(qr.OriginalTAccountName.DisplayName)) ||
          (line.Transaction typeis CreditTransaction && line.Transaction.FundsSourceType.Code.equalsIgnoreCase(qr.OriginalTAccountName.Code))) {
        //Override mapped account name handled for the charge patterens related to
        // Group 2:Policy Payment Reversal Fee ,Credit Card Usage Fee. Group 3:Deductible
        var overrideMappedAccountName = getOverrideMappedTAccountName(line, qr.OriginalTAccountName.DisplayName, qr.InvoiceStatus.Code)
        if (overrideMappedAccountName != null)
          return overrideMappedAccountName

        if (line.Transaction typeis ChargeInvoicingTxn && line.TAccount.TAccountContainer typeis PolTAcctContainer) {
          // If future term, it does not matter what the Invoice status is; only applies to ChargePaidFromAccount transactions
          if (line.Transaction typeis ChargePaidFromAccount) {
            if (line.TAccount.TAccountContainer.PolicyPeriod.Status == PolicyPeriodStatus.TC_FUTURE && qr.FutureTerm) {
              _logger.debug("TDIC_GLIntegrationHelper#getMappedTAccountName - Found T-Account mapping: " + qr.MappedTAccountName)
              return qr.MappedTAccountName
            } else if (line.Transaction.InvoiceItem.Invoice.Status == qr.InvoiceStatus
                && line.TAccount.TAccountContainer.PolicyPeriod.Status != PolicyPeriodStatus.TC_FUTURE && !qr.FutureTerm) {
              _logger.debug("TDIC_GLIntegrationHelper#getMappedTAccountName - Found T-Account mapping: " + qr.MappedTAccountName)
              return qr.MappedTAccountName
            }
          } else if (line.Transaction.InvoiceItem.Invoice.Status == qr.InvoiceStatus) {
            _logger.debug("TDIC_GLIntegrationHelper#getMappedTAccountName - Found T-Account mapping: " + qr.MappedTAccountName)
            return qr.MappedTAccountName
          }
        } else {
          _logger.debug("TDIC_GLIntegrationHelper#getMappedTAccountName - Found T-Account mapping: " + qr.MappedTAccountName)
          return qr.MappedTAccountName
        }
      }
    }

    // If no match is found, just return the Guidewire default T-account name.
    _logger.debug("TDIC_GLIntegrationHelper#getMappedTAccountName - No T-Account mapping found.  Returning Guidewire's T-Account Name: " + line.TAccount.TAccountPattern.DisplayName)
    return line.TAccount.TAccountPattern.DisplayName
  }

  /*
   * This method returns the mapped T-account name using the line item from the Transaction for R2.
   * Note:
   * In Release 2, there are additional charge patterns to be added to the Release 1 requirements and associated mappings.
   * The Charge Patterns included in Release 2 fall into 3 groups as follows:
   * Group 1:Premium,Recapture,Policy Recapture,Extended Reporting Period Endorsement,IL Mine Subsidence,
   * New Jersey Insurance Guarantee Surcharge,Fire Safety Surcharge,Firefighter Relief Surcharge
   * Group 2:Policy Payment Reversal Fee ,Credit Card Usage Fee
   * Group 3:Deductible
   * The Group 1 mapped accounts that will be viewable in the BillingCenter UI.
   * Group 2 and 3 accounts that differ are highlighted in yellow and will be adjusted in the integration layer.
   * Note that there are changes from R1 to R2 for the account mappings.
   */
  @Param("line", "The LineItem from which to retrieve the T-account name")
  @Returns("A String containing the mapped T-account name")
  public static function getOverrideMappedTAccountName(line : LineItem, originalTAccountName : String, invoiceStatus : String) : String {
    _logger.debug("TDIC_GLIntegrationHelper#getOverrideMappedTAccountName - Entering")
    var chargeName : String
    var mappedAccount : String
    var transactionSubtype = line.Transaction.Subtype
    var txnInvoiceStatus : String
    if (line.Transaction typeis ChargeInstanceTxn) {
      chargeName = line.Transaction.Charge.ChargePattern.ChargeCode
      if (chargeName == null)
        return null
    }
    if (line.Transaction typeis ChargeInvoicingTxn) {
      txnInvoiceStatus = line.Transaction.InvoiceItem.Invoice.Status.Code
      if (!txnInvoiceStatus.equalsIgnoreCase(invoiceStatus))
        return null
    }

    if (surchargePatterens.contains(chargeName)) {
      if ((transactionSubtype == typekey.Transaction.TC_CHARGEWRITTENOFF &&
          originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_WRITEOFFEXPENSE.DisplayName)) ||
          (transactionSubtype == typekey.Transaction.TC_ACCOUNTNEGATIVEWRITEOFFTXN &&
              originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_NEGATIVEWRITEOFF.DisplayName))) {
        mappedAccount = "2660-1200"
      }
    } else if (chargeName?.containsIgnoreCase("PolicyPaymentReversalFee") ||
        chargeName?.containsIgnoreCase("CreditCardUsageFee")) {
      if ((transactionSubtype == typekey.Transaction.TC_CHARGEWRITTENOFF &&
          originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_WRITEOFFEXPENSE.DisplayName)) ||
          transactionSubtype == typekey.Transaction.TC_CHARGEBILLED && line.Type == LedgerSide.TC_CREDIT) {
        mappedAccount = "4780-1200"
      }
    } else if (chargeName?.containsIgnoreCase("Deductible")) {
      if ((transactionSubtype == typekey.Transaction.TC_CHARGEBILLED && originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_UNBILLED.DisplayName)) ||
          (transactionSubtype == typekey.Transaction.TC_CHARGEWRITTENOFF && originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_WRITEOFFEXPENSE.DisplayName)) ||
          (transactionSubtype == typekey.Transaction.TC_ACCOUNTNEGATIVEWRITEOFFTXN && originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_NEGATIVEWRITEOFF.DisplayName)) ||
          ((transactionSubtype == typekey.Transaction.TC_ACCOUNTCOLLECTIONTXN || transactionSubtype == typekey.Transaction.TC_ACCOUNTGOODWILLTXN
              || transactionSubtype == typekey.Transaction.TC_ACCOUNTINTERESTTXN || transactionSubtype == typekey.Transaction.TC_ACCOUNTOTHERTXN) &&
              originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_ADHOCCREDIT.DisplayName))) {
        mappedAccount = "5536-0000"
      } else if (((transactionSubtype == typekey.Transaction.TC_CHARGEPAIDFROMACCOUNT || transactionSubtype == typekey.Transaction.TC_CHARGEWRITTENOFF) &&
          originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_DUE.DisplayName)) ||
          transactionSubtype == typekey.Transaction.TC_CHARGEBILLED && originalTAccountName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_BILLED.DisplayName)) {
        mappedAccount = "1261-0000"
      }
    }
    return mappedAccount
  }


  /*
   * This method returns the mapped T-account name using the individual fields from the GLIntegrationTAccountMapping_TDIC
   * entity.  This is mainly used for the T-Account mapping for the manually-created transaction PolicyPeriodBecameEffective.
   */
  @Param("transSubtype", "A Transaction typekey for the subtype from which to retrieve the T-account name")
  @Param("lineItemType", "The LedgerSide (credit or debit of the line item) from which to retrieve the T-account name")
  @Param("futureTerm", "A boolean indicating a future policy period line item from which to retrieve the T-account name")
  @Param("invoiceStatus", "The InvoiceStatus for the line item from which to retrieve the T-account name")
  @Param("originalName", "A String containing the original T-Account name, used to retrieve mapped the T-account name")
  @Returns("A String containing the mapped T-account name")
  public static function getMappedTAccountName(transSubtype : typekey.Transaction, lineItemType : LedgerSide, futureTerm : boolean, invoiceStatus : InvoiceStatus, originalName : String) : String {
    _logger.debug("TDIC_GLIntegrationHelper#getMappedTAccountName - Entering")
    var query = Query.make(GLIntegrationTAccountMapping_TDIC)
    query.compare(GLIntegrationTAccountMapping_TDIC#TranasctionType, Equals, transSubtype)
    query.compare(GLIntegrationTAccountMapping_TDIC#LineItemType, Equals, lineItemType)
    query.compare(GLIntegrationTAccountMapping_TDIC#FutureTerm, Equals, futureTerm)
    if (!futureTerm) {
      query.compare(GLIntegrationTAccountMapping_TDIC#InvoiceStatus, Equals, invoiceStatus)
    }
    query.compare(GLIntegrationTAccountMapping_TDIC#OriginalTAccountName, Equals, typekey.GLOriginalTAccount_TDIC.get(originalName))
    var results = query.select()
    if (results.Count == 0) {
      return originalName
    } else {
      return results.FirstResult.MappedTAccountName
    }
  }

  /**
   * Gets the account unit based on the line of business and the risk jurisdiction.
   */
  @Param("company", "The Company typelist for the Account, specific to each type of Lawson GL file")
  @Param("lobCode", "A String representing the line of business")
  @Param("jurisdictionCode", "A String containing the state abbreviation for the policy's risk jurisdiction")
  @Returns("A String containing the mapped account unit")
  public static function getMappedAccountUnit(company : CompanyAccountCode_TDIC, lobCode : String, jurisdictionCode : String) : String {
    _logger.debug("TDIC_GLIntegrationHelper#getMappedAccountUnit - Getting account unit for LOB '" + lobCode + "' and state '" + jurisdictionCode + "'")
    var accountUnit = new StringBuilder()
    accountUnit.append(company.DisplayName).append(".")
    accountUnit.append(getMappedJurisdictionAccountCode(jurisdictionCode)).append(".")
    accountUnit.append(getMappedLOBAccountCode(lobCode))
    return accountUnit.toString()
  }

  /**
   * Gets the code mapped to the line of business.
   */
  @Param("lobCode", "A String representing the line of business")
  @Returns("A String containing the mapped LOB code")
  @Throws(RuntimeException, "If a mapping cannot be found for the LOB code")
  private static function getMappedLOBAccountCode(lobCode : String) : String {
    if (lobCode == null) {
      _logger.warn("TDIC_GLIntegrationHelper#getMappedLOBAccountCode - No LOB code provided.  Returning '00'")
      //return "00" TODO Always return "40" for Worker's Comp in Release A, even for Unapplied (no policy period, so no LOB). This will change in Release B. For example, ChargePaidFromAccount transactions will need to copy the LOB from the applied line item.
      return LOBAccountCode_TDIC.TC_WC7WORKERSCOMP.DisplayName
    } else {
      var mappedCode = LOBAccountCode_TDIC.get(lobCode)
      if (mappedCode != null) {
        _logger.debug("TDIC_GLIntegrationHelper#getMappedLOBAccountCode - Found account mapping for LOB: " + mappedCode)
        return mappedCode.DisplayName
      } else {
        _logger.error("TDIC_GLIntegrationHelper#getMappedLOBAccountCode - Incorrect setup:  Cannot find mapping for LOB code '" + lobCode + "'")
        throw new RuntimeException("Incorrect setup:  Cannot find mapping for LOBCode '" + lobCode + "'")
      }
    }
  }

  /**
   * Gets the code mapped to the risk jurisdiction.
   */
  @Param("jurisdictionCode", "A String containing the state abbreviation for the policy's risk jurisdiction")
  @Returns("A String containing the mapped jurisdiction code")
  @Throws(RuntimeException, "If a mapping cannot be found for the jurisdiction")
  private static function getMappedJurisdictionAccountCode(jurisdictionCode : String) : String {
    if (jurisdictionCode == null) {
      _logger.warn("TDIC_GLIntegrationHelper#getMappedJurisdictionAccountCode - No Jurisdiction code provided.  Returning '00'")
      //return "00" TODO Always return "06" for California in Release A, even for Unapplied (no policy period, so no Jurisdiction). This will change in Release B. For example, ChargePaidFromAccount transactions will need to copy the State from the applied line item.
      return JurisdictionAcctCode_TDIC.TC_CA.DisplayName
    } else {
      var mappedCode = JurisdictionAcctCode_TDIC.get(jurisdictionCode)
      if (mappedCode != null) {
        _logger.debug("TDIC_GLIntegrationHelper#getMappedJurisdictionAccountCode - Found account mapping for Jurisdiction: " + mappedCode)
        return mappedCode.DisplayName
      } else {
        _logger.error("TDIC_GLIntegrationHelper#getMappedJurisdictionAccountCode - Incorrect setup:  Cannot find mapping for Jurisdiction '" + jurisdictionCode + "'")
        throw new RuntimeException("Incorrect setup:  Cannot find mapping for Jurisdiction '" + jurisdictionCode + "'")
      }
    }
  }

  /**
   * Process the DisbursementPaid transaction.  This will look for the correct GL bucket and either add or subtract the
   * amount from the bucket, depending on if it is a credit line or debit line (reversed for GL).
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processDisbursementTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#processDisbursementTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName.endsWith("Unapplied")) {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("DisbPaidRev")
      } else {
        bucket = bucketGroup.NameBucketMap.get("DisbPaid")
      }
    } else if (transLine.GWTAccountName == "Cash") {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("DisbPaidRevOffset")
      } else {
        bucket = bucketGroup.NameBucketMap.get("DisbPaidOffset")
      }
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the ChargeWrittenOff transaction.  This will look for the correct GL bucket and either add or subtract the
   * amount from the bucket, depending on if it is a credit line or debit line (reversed for GL).
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processWriteOffTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#processWriteOffTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName.endsWith("Due")) {
      if (transLine.InvoiceStatus == InvoiceStatus.TC_BILLED.Code || transLine.InvoiceStatus == InvoiceStatus.TC_DUE.Code) {
        if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
          bucket = bucketGroup.NameBucketMap.get("WriteOffRevBilled")
        } else {
          bucket = bucketGroup.NameBucketMap.get("WriteOffBilled")
        }
      } else if (transLine.InvoiceStatus == InvoiceStatus.TC_PLANNED.Code) {
        if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
          bucket = bucketGroup.NameBucketMap.get("WriteOffRevUnbilled")
        } else {
          bucket = bucketGroup.NameBucketMap.get("WriteOffUnbilled")
        }
      }
    } else if (transLine.GWTAccountName.endsWith("Write-Off Expense")) {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("WriteOffRevOffset")
      } else {
        bucket = bucketGroup.NameBucketMap.get("WriteOffOffset")
      }
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the AccountNegativeWriteoffTxn transaction.  This will look for the correct GL bucket and either add or
   * subtract the amount from the bucket, depending on if it is a credit line or debit line (reversed for GL).
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processNegativeWriteOffTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#processNegativeWriteOffTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName.endsWith("Unapplied")) {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("NegWriteOffRevOffset")
      } else {
        bucket = bucketGroup.NameBucketMap.get("NegWriteOffOffset")
      }
    } else if (transLine.GWTAccountName == "Negative Write-Off") {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("NegWriteOffRev")
      } else {
        bucket = bucketGroup.NameBucketMap.get("NegWriteOff")
      }
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the ChargeBilled transaction.  This will look for the correct GL bucket and either add or subtract the
   * amount from the bucket, depending on if it is a credit line or debit line.
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processChargeBilledTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#procoessChargeBilledTransaction - Entering")
    var bucket : GLBucket
    if (transLine.PolicyPeriodStatus == PolicyPeriodStatus.TC_FUTURE.Code) {
      // This transaction does not matter if the policy period is in the future, since TDIC does not consider the amount
      // moved until the policy period is effective.  Therefore, return without making changes to the buckets.
      _logger.debug("TDIC_GLIntegrationHelper#procoessChargeBilledTransaction - Policy Period " + transLine.PolicyNumber
          + "-" + transLine.TermNumber + "is in the future. Skipping ChargeBilled transaction.")
      return
    }
    if (transLine.GWTAccountName.endsWith("Unbilled")) {
      bucket = bucketGroup.NameBucketMap.get("ChargeBilledUnbilled")
    } else if (transLine.GWTAccountName.endsWith("Billed")) {
      bucket = bucketGroup.NameBucketMap.get("ChargeBilledBilled")
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the ChargePaidFromAccount transaction.  This will look for the correct GL bucket and either add or
   * subtract the amount from the bucket, depending on if it is a credit line or debit line.
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processChargePaidFromAccountTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#processChargePaidFromAccountTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName.endsWith("Unapplied")) {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("ChargePaidFromAccountRevOffset")
      } else {
        bucket = bucketGroup.NameBucketMap.get("ChargePaidFromAccountOffset")
      }
    } else if (transLine.GWTAccountName.endsWith("Due")) {
      if (transLine.PolicyPeriodStatus == PolicyPeriodStatus.TC_FUTURE.Code) {
        // This invoice status does not matter if the policy period is in the future
        if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
          bucket = bucketGroup.NameBucketMap.get("ChargePaidFromAccountRevFuture")
        } else {
          bucket = bucketGroup.NameBucketMap.get("ChargePaidFromAccountFuture")
        }
      } else {
        if (transLine.InvoiceStatus == InvoiceStatus.TC_BILLED.Code || transLine.InvoiceStatus == InvoiceStatus.TC_DUE.Code) {
          if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
            bucket = bucketGroup.NameBucketMap.get("ChargePaidFromAccountRevBilled")
          } else {
            bucket = bucketGroup.NameBucketMap.get("ChargePaidFromAccountBilled")
          }
        } else if (transLine.InvoiceStatus == InvoiceStatus.TC_PLANNED.Code) {
          if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
            bucket = bucketGroup.NameBucketMap.get("ChargePaidFromAccountRevUnbilled")
          } else {
            bucket = bucketGroup.NameBucketMap.get("ChargePaidFromAccountUnbilled")
          }
        }
      }
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the PolicyPeriodBecameEffective transaction.  This will look for the correct GL bucket and either add or subtract the
   * amount from the bucket, depending on if it is a credit line or debit line.
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processPolicyPeriodBecameEffectiveTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#procoessPolicyPeriodBecameEffectiveTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName == GLOriginalTAccount_TDIC.TC_BILLED.Code) {
      bucket = bucketGroup.NameBucketMap.get("PolicyPeriodBecameEffBilled")
    } else if (transLine.GWTAccountName == GLOriginalTAccount_TDIC.TC_UNBILLED.Code) {
      bucket = bucketGroup.NameBucketMap.get("PolicyPeriodBecameEffUnbilled")
    } else if (transLine.GWTAccountName == GLOriginalTAccount_TDIC.TC_FUTURE.Code) {
      bucket = bucketGroup.NameBucketMap.get("PolicyPeriodBecameEffOffset")
    } else if(transLine.GWTAccountName == GLOriginalTAccount_TDIC.TC_UNAPPLIED.Code) {
      //GWPS-2105: Mapping to PolicyPeriodBecameEffOffset for the unapplied amount
      bucket = bucketGroup.NameBucketMap.get("PolicyPeriodBecameEffOffset")
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the Account Collection transaction.  This will look for the correct GL bucket and either add or
   * subtract the amount from the bucket, depending on if it is a credit line or debit line (reversed for GL).
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processAccountCollectionTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#processAccountCollectionTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName.endsWith("Unapplied")) {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("AccountCollectionTxnRevOffset")
      } else {
        bucket = bucketGroup.NameBucketMap.get("AccountCollectionTxnOffset")
      }
    } else if (transLine.GWTAccountName == "Collections Credit") {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("AccountCollectionTxnRev")
      } else {
        bucket = bucketGroup.NameBucketMap.get("AccountCollectionTxn")
      }
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the Account Goodwill transaction.  This will look for the correct GL bucket and either add or
   * subtract the amount from the bucket, depending on if it is a credit line or debit line (reversed for GL).
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processAccountGoodwillTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#processAccountGoodwillTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName.endsWith("Unapplied")) {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("AccountGoodwillTxnRevOffset")
      } else {
        bucket = bucketGroup.NameBucketMap.get("AccountGoodwillTxnOffset")
      }
    } else if (transLine.GWTAccountName == "Goodwill Credit") {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("AccountGoodwillTxnRev")
      } else {
        bucket = bucketGroup.NameBucketMap.get("AccountGoodwillTxn")
      }
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the Account Interest transaction.  This will look for the correct GL bucket and either add or
   * subtract the amount from the bucket, depending on if it is a credit line or debit line (reversed for GL).
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processAccountInterestTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#processAccountInterestTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName.endsWith("Unapplied")) {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("AccountInterestTxnRevOffset")
      } else {
        bucket = bucketGroup.NameBucketMap.get("AccountInterestTxnOffset")
      }
    } else if (transLine.GWTAccountName == "Interest Credit") {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("AccountInterestTxnRev")
      } else {
        bucket = bucketGroup.NameBucketMap.get("AccountInterestTxn")
      }
    }
    processBucketFields(transLine, bucket)
  }

  /**
   * Process the Account Other transaction.  This will look for the correct GL bucket and either add or
   * subtract the amount from the bucket, depending on if it is a credit line or debit line (reversed for GL).
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of GL buckets, holding the bucket that will be updated")
  public static function processAccountOtherTransaction(transLine : GLTransactionLineWritableBean, bucketGroup : GLBucketGroup) {
    _logger.debug("TDIC_GLIntegrationHelper#processAccountOtherTransaction - Entering")
    var bucket : GLBucket
    if (transLine.GWTAccountName.endsWith("Unapplied")) {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("AccountOtherTxnRevOffset")
      } else {
        bucket = bucketGroup.NameBucketMap.get("AccountOtherTxnOffset")
      }
    } else if (transLine.GWTAccountName == "Other Credit") {
      if (transLine.Description.endsWithIgnoreCase("(Reversed)")) {
        bucket = bucketGroup.NameBucketMap.get("AccountOtherTxnRev")
      } else {
        bucket = bucketGroup.NameBucketMap.get("AccountOtherTxn")
      }
    }
    processBucketFields(transLine, bucket)
  }


  /**
   * Processes the bucket fields, including setting the Mapped T-Account Code on the bucket, as well as adding or
   * subtracting to the total amount of the bucket, depending if the transaction line is credit or debit.
   * In GL terms, credit means negative (subtract); debit means positive (add).
   */
  @Param("transLine", "The GLTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucket", "The GL bucket whose fields will be updated")
  @Throws(GLTransactionLineWritableBean, "If a proper GL bucket was not been passed into the method")
  private static function processBucketFields(transLine : GLTransactionLineWritableBean, bucket : GLBucket) {
    _logger.debug("TDIC_GLIntegrationHelper#processBucketFields - Entering")
    /*GWPS-114 - Ignore CarriedForward invoices from sending to Lawson */
    if (transLine.InvoiceStatus != InvoiceStatus.TC_CARRIEDFORWARD.Code) {
      if (bucket == null) {
        _logger.warn("TDIC_GLIntegrationHelper#processBucketFields - Unable to determine bucket for the "
            + transLine.GWTAccountName + " T-Account in the " + transLine.TransactionSubtype + " transaction")
        throw new GLRuntimeException("Unable to determine bucket for the " + transLine.GWTAccountName
            + " T-Account in the " + transLine.TransactionSubtype + " transaction" + " with InvoiceStatus:" + transLine.InvoiceStatus)
      }
      if (bucket.MappedTAccountCode == null) {
        bucket.MappedTAccountCode = transLine.MappedTAccountName
      }
      if (transLine.LineItemType == LedgerSide.TC_CREDIT.Code) {
        _logger.debug("TDIC_GLIntegrationHelper#processBucketFields - Crediting (Subtracting) " + transLine.Amount
            + " from the GL bucket: " + bucket.Description)
        bucket.TotalBucketAmount = bucket.TotalBucketAmount.subtract(transLine.Amount)
      } else if (transLine.LineItemType == LedgerSide.TC_DEBIT.Code) {
        _logger.debug("TDIC_GLIntegrationHelper#processBucketFields - Debiting (Adding) " + transLine.Amount
            + " to the GL bucket: " + bucket.Description)
        bucket.TotalBucketAmount = bucket.TotalBucketAmount.add(transLine.Amount)
      }
    }
  }

  /**
   * Converts data to the lines that can be written to the destination flat file.
   */
  @Param("flatFileData", "The object containing the data to write to the destination flat file")
  @Returns("A List<String> containing the liens to be written to the destination flat file")
  @Throws(Exception, "If there are any unexpected errors encoding the data")
  public static function convertDataToFlatFileLines(flatFileData : GLFile) : List<String> {
    var gxModel = new tdic.bc.integ.plugins.generalledger.gxmodel.glfilemodel.GLFile(flatFileData)
    _logger.debug("TDIC_GLIntegrationHelper#convertDataToFlatFileLines - GL GX Model Generated: ")
    _logger.debug(gxModel.asUTFString())
    var xml = XmlElement.parse(gxModel.asUTFString())
    var df = new DelimitedFileImpl()
    return df.encode("LawsonBCGLSpec.xlsx", xml)
  }

  /**
   * Gets the LOBAccountCode based on the line of business and offering.
   */

  @Param("policyPeriod", "A PolicyPeriod representing the Policy transaction.")
  @Returns("A String representing the LOBAccountCode")
  public static function getMappedLOBAccountCode(policyPeriod : PolicyPeriod) : String {
    //_logger.debug("TDIC_GLIntegrationHelper#getMappedAccountUnit - Getting account unit for LOB '" + lobCode + "' and state '" + jurisdictionCode + "'")
    var lobCode = policyPeriod.Policy.LOBCode.Code

    if (lobCode == null) return null

    if (lobCode.equalsIgnoreCase(LOBCode.TC_WC7WORKERSCOMP.Code)) {
      return LOBCode.TC_WC7WORKERSCOMP.Code
    } else {
      return (policyPeriod.Offering_TDIC == Offering_TDIC.TC_PLCLAIMSMADE_TDIC && policyPeriod.ERE_TDIC) ?
          LOBAccountCode_TDIC.TC_PLCLAIMSMADEERE_TDIC.Code : LOBAccountCode_TDIC.get(policyPeriod.Offering_TDIC.Code).Code
    }
  }

  public static function isValidTransactionForAccUnit(transLine : GLTransactionLineWritableBean) : Boolean {
    return ((transLine.TransactionSubtype == typekey.Transaction.TC_CHARGEWRITTENOFF.Code && transLine.GWTAccountName.endsWith(GLOriginalTAccount_TDIC.TC_WRITEOFFEXPENSE.DisplayName)) ||
        (transLine.TransactionSubtype == typekey.Transaction.TC_ACCOUNTNEGATIVEWRITEOFFTXN.Code && transLine.GWTAccountName.endsWith(GLOriginalTAccount_TDIC.TC_NEGATIVEWRITEOFF.DisplayName)) ||
        ((transLine.TransactionSubtype == typekey.Transaction.TC_ACCOUNTCOLLECTIONTXN.Code || transLine.TransactionSubtype == typekey.Transaction.TC_ACCOUNTGOODWILLTXN.Code ||
            transLine.TransactionSubtype == typekey.Transaction.TC_ACCOUNTINTERESTTXN.Code || transLine.TransactionSubtype == typekey.Transaction.TC_ACCOUNTOTHERTXN.Code) &&
            transLine.GWTAccountName.endsWith("Credit")))
  }
}