package tdic.bc.config.invoice

uses java.math.BigDecimal

class InvoiceDocumentHelper {

  static function PaymentTransaction (account : Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : Transaction[] {
    var paymentsReceived = account.Transactions.where(\elt -> elt typeis DirectBillMoneyReceivedTxn and !elt.Reversal
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in paymentsReceived){
      var dbillmrec = trx as DirectBillMoneyReceivedTxn
      var transaction = dbillmrec as Transaction
      if(transaction.LongDisplayName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray()
  }

  static function PaymentReversalTransaction (account: Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : Transaction[] {
    var paymentsReversed = account.Transactions.where(\elt -> elt typeis DirectBillMoneyReceivedTxn && elt.Reversal
        and elt.DirectBillMoneyRcvd.ReversalReason != PaymentReversalReason.TC_MOVED
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in paymentsReversed){
      var dbillmrec = trx as DirectBillMoneyReceivedTxn
      var transaction = dbillmrec as Transaction
      if(transaction.LongDisplayName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray()
  }

  static function PaymentMoveFromTransaction (account : Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : Transaction[] {
    var paymentsMoved = account.Transactions.where(\elt -> elt typeis DirectBillMoneyReceivedTxn and elt.Reversed
        and elt.DirectBillMoneyRcvd.ReversalReason == PaymentReversalReason.TC_MOVED
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in paymentsMoved){
      var dbillmrec = trx as DirectBillMoneyReceivedTxn
      var transaction = dbillmrec as Transaction
      if(transaction.LongDisplayName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray()
  }

  static function FundsTransferOutgoingTransaction (account : Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : Transaction[] {
    var transferTrnx = account.Transactions.where(\elt -> elt typeis TransferTransaction
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in transferTrnx){
      var transfer = trx as TransferTransaction
      var transaction = transfer as Transaction
      if(transaction.LineItems.firstWhere(\elt -> elt.Type == LedgerSide.TC_DEBIT).TAccount.TAccountOwnerTypeNameTAccountName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray()
  }

  static function FundsTransferIncomingTransaction (account : Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : Transaction[] {
    var transferTrnx = account.Transactions.where(\elt -> elt typeis TransferTransaction
    and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in transferTrnx){
      var transfer = trx as TransferTransaction
      var transaction = transfer as Transaction
      if(transaction.LineItems.firstWhere(\elt -> elt.Type == LedgerSide.TC_CREDIT).TAccount.TAccountOwnerTypeNameTAccountName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray()
  }

  static function DisbursementSentTransaction (account: Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : BigDecimal {
    var disbursementsPaid = account.Transactions.where(\elt -> elt typeis DisbursementPaid && elt.Reversal == false
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in disbursementsPaid){
      var disbPaid = trx as DisbursementPaid
      var transaction = disbPaid as Transaction
      if(transaction.LineItems.firstWhere(\elt -> elt.Type == LedgerSide.TC_DEBIT).TAccount.TAccountOwnerTypeNameTAccountName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray().sum(\s -> s.Amount)
  }

  static function DisbursementVoidedTransaction (account: Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : BigDecimal {
    var disbursementReversal = account.Transactions.where(\elt -> elt typeis DisbursementPaid && elt.Reversal
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in disbursementReversal){
      var disbVoided = trx as DisbursementPaid
      var transaction = disbVoided as Transaction
      if(transaction.LineItems.firstWhere(\elt -> elt.Type == LedgerSide.TC_CREDIT).TAccount.TAccountOwnerTypeNameTAccountName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray().sum(\s -> s.Amount)
  }

  static function WriteOffTransaction (pp: PolicyPeriod, fromDate: Date, toDate: Date) : BigDecimal {
    var writeOffs = pp.Transactions.where(\elt -> elt typeis ChargeWrittenOff and elt.Reversal == false
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in writeOffs){
      var wOffs = trx as ChargeWrittenOff
      var transaction = wOffs as Transaction
      transactionList.add(transaction)
    }
    return transactionList.toTypedArray().sum(\s -> s.Amount)
  }

  static function WriteOffReversalTransaction (pp: PolicyPeriod, fromDate: Date, toDate: Date) : BigDecimal {
    var writeOffReversals = pp.Transactions.where(\elt -> elt typeis ChargeWrittenOff && elt.Reversal
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in writeOffReversals){
      var wOffRevs = trx as ChargeWrittenOff
      var transaction = wOffRevs as Transaction
      transactionList.add(transaction)
    }
    return transactionList.toTypedArray().sum(\s -> s.Amount)
  }

  static function NegativeWriteOffTransaction (account: Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : BigDecimal {
    var negativeWriteOffs = account.Transactions.where(\elt -> elt typeis AccountNegativeWriteoffTxn and elt.Reversal == false
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in negativeWriteOffs){
      var negwOffs = trx as AccountNegativeWriteoffTxn
      var transaction = negwOffs as Transaction
      if(transaction.LongDisplayName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray().sum(\s -> s.Amount)
  }

  static function NegativeWriteOffReversalTransaction (account: Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : BigDecimal {
    var negativeWriteOffsRev = account.Transactions.where(\elt -> elt typeis AccountNegativeWriteoffTxn and elt.Reversal
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var transactionList = new ArrayList<Transaction>()
    for(trx in negativeWriteOffsRev){
      var negwOffRevs = trx as AccountNegativeWriteoffTxn
      var transaction = negwOffRevs as Transaction
      if(transaction.LongDisplayName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        transactionList.add(transaction)
      }
    }
    return transactionList.toTypedArray().sum(\s -> s.Amount)
  }

  static function CreditTransaction (account: Account, fromDate: Date, toDate: Date, invoice : AccountInvoice) : BigDecimal {
    // Good Will Transaction
    var goodwillTxns = account.Transactions.where(\elt -> elt typeis AccountGoodwillTxn
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var goodWillTransactionList = new ArrayList<Transaction>()
    for(trx in goodwillTxns){
      var goodWill = trx as AccountGoodwillTxn
      var transaction = goodWill as Transaction
      if(transaction.LineItems.firstWhere(\elt -> elt.Type == LedgerSide.TC_CREDIT).TAccount.TAccountOwnerTypeNameTAccountName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        goodWillTransactionList.add(transaction)
      }
    }
    // Collection Transaction
    var collectionTxns = account.Transactions.where(\elt -> elt typeis AccountCollectionTxn
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var collectionTransactionList = new ArrayList<Transaction>()
    for(trx in collectionTxns){
      var collection = trx as AccountCollectionTxn
      var transaction = collection as Transaction
      if(transaction.LineItems.firstWhere(\elt -> elt.Type == LedgerSide.TC_CREDIT).TAccount.TAccountOwnerTypeNameTAccountName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        collectionTransactionList.add(transaction)
      }
    }
    // Interest Transactions
    var interestTxns = account.Transactions.where(\elt -> elt typeis AccountInterestTxn
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var interestTransactionList = new ArrayList<Transaction>()
    for(trx in interestTxns){
      var interest = trx as AccountInterestTxn
      var transaction = interest as Transaction
      if(transaction.LineItems.firstWhere(\elt -> elt.Type == LedgerSide.TC_CREDIT).TAccount.TAccountOwnerTypeNameTAccountName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        interestTransactionList.add(transaction)
      }
    }
    // Other Transactions
    var otherTrnxs = account.Transactions.where(\elt -> elt typeis AccountOtherTxn
        and elt.TransactionDate.afterOrEqual(fromDate.addDays(1)) and elt.TransactionDate.beforeOrEqual(toDate.addDays(1)))
    var otherTransactionList = new ArrayList<Transaction>()
    for(trx in otherTrnxs){
      var other = trx as AccountOtherTxn
      var transaction = other as Transaction
      if(transaction.LineItems.firstWhere(\elt -> elt.Type == LedgerSide.TC_CREDIT).TAccount.TAccountOwnerTypeNameTAccountName.contains(invoice.InvoiceStream.UnappliedFund.DisplayName)){
        otherTransactionList.add(transaction)
      }
    }

    var goodwillAmount = goodWillTransactionList.toTypedArray().sum(\s -> s.Amount)
    var collectionAmount = collectionTransactionList.toTypedArray().sum(\s -> s.Amount)
    var interestAmount = interestTransactionList.toTypedArray().sum(\s -> s.Amount)
    var OtherAmount = otherTransactionList.toTypedArray().sum(\s -> s.Amount)
    return goodwillAmount + collectionAmount + interestAmount + OtherAmount
  }

}