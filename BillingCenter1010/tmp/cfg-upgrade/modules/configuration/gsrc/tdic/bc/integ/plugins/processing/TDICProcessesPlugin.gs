package tdic.bc.integ.plugins.processing

uses gw.plugin.processing.IProcessesPlugin
uses gw.processes.BatchProcess
uses tdic.bc.common.batch.bankeft.TDIC_BankEFTBatchJob
uses tdic.bc.common.batch.bankeft.TDIC_BankEFTPrenoteBatchJob
uses tdic.bc.common.batch.bankeft.TDIC_BankEFTUploadBatchJob
uses tdic.bc.common.batch.banklockbox.TDIC_BankLockboxFileDownloadBatch
uses tdic.bc.common.batch.banklockbox.TDIC_BankLockboxFileProcessBatch
uses tdic.bc.common.batch.checkstatus.TDIC_CheckStatusUpdateBatch
uses tdic.bc.common.batch.eftreversal.TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob
uses tdic.bc.common.batch.eftreversal.TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob
uses tdic.bc.common.batch.generalledger.TDIC_CashReceiptsBatchJob
uses tdic.bc.common.batch.generalledger.TDIC_GLIntegrationBatchJob
uses tdic.bc.common.batch.generalledger.TDIC_PolicyPeriodEffectiveStatusBatchJob
uses tdic.bc.common.batch.invoices.RemoveGhostinvoices
uses tdic.bc.common.batch.invoices.ResendInvoicesBatchProcess
uses tdic.bc.common.batch.invoices.UnappliedFundsTargetedToInvoices
uses tdic.bc.common.batch.outgoingpayment.TDIC_OutgoingPaymentBatchJob
uses tdic.bc.common.batch.paymentplan.ChangeQuarterlyPaymentPlan
uses tdic.bc.common.batch.plan.ChangeDefaultReturnPremiumPlan
uses tdic.bc.common.batch.plan.ChangeTDICDelinquencyPlan
uses tdic.bc.common.batch.plan.UpdateTDICBillingPlan
uses tdic.bc.common.batch.securityzoneUpdate.AccountSecurityZoneUpdateBatch_TDIC
uses tdic.bc.common.batch.securityzoneUpdate.PolicyPeriodSecurityZoneUpdateBatch_TDIC
uses tdic.bc.common.batch.securityzoneUpdate.SecurityZoneUpdateBatch_TDIC
uses tdic.bc.common.batch.troubleticket.TDIC_TroubleTicketsDailyBatch
uses tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetCreditCardHandlerCleanupBatch
uses tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetOpenHandlerCleanupBatch
uses tdic.util.cache.CacheManagerBatchProcess
uses tdic.bc.integ.plugins.hpexstream.batch.TDIC_DocumentCreationBatch
uses tdic.bc.common.batch.returncreditallocaton.RetunCreditAllocationToAccountDefault_TDIC
uses org.slf4j.LoggerFactory

/**
 * US568 - Bank/EFT Integration
 * 09/25/2014 Alvin Lee
 *
 * IProcessesPlugin implementation for all custom batch processes.
 */
@Export
class TDICProcessesPlugin implements IProcessesPlugin {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")

  override function createBatchProcess(type : BatchProcessType, arguments : Object[]) : BatchProcess {
    switch(type) {
      case BatchProcessType.TC_BANKLOCKBOXFILEPROCESS:
        return new TDIC_BankLockboxFileProcessBatch()
      case BatchProcessType.TC_BANKLOCKBOXFILEDOWNLOAD:
        return new TDIC_BankLockboxFileDownloadBatch()
      case BatchProcessType.TC_BANKEFTBATCHPROCESS:
        return new TDIC_BankEFTBatchJob()
      case BatchProcessType.TC_BANKEFTUPLOADBATCHPROCESS:
        return new TDIC_BankEFTUploadBatchJob()
      case BatchProcessType.TC_BANKEFTPRENOTEBATCHPROCESS_TDIC:
        return new TDIC_BankEFTPrenoteBatchJob()
      case BatchProcessType.TC_CACHEMANAGER:
        return new CacheManagerBatchProcess()
      case BatchProcessType.TC_DOCUMENTCREATIONBATCH:
        return new TDIC_DocumentCreationBatch()
      case BatchProcessType.TC_GENERALLEDGER:
        return new TDIC_GLIntegrationBatchJob()
      case BatchProcessType.TC_CASHRECEIPTS:
        return new TDIC_CashReceiptsBatchJob()
      case BatchProcessType.TC_AUTHORIZENETCREDITCARDHANDLERCLEANUP_TDIC:
        return new TDIC_AuthorizeNetCreditCardHandlerCleanupBatch()
      case BatchProcessType.TC_OUTGOINGPAYMENT:
        return new TDIC_OutgoingPaymentBatchJob()
      case BatchProcessType.TC_EFTREVERSALFILEDOWNLOADBATCHPROCESS:
        return new TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob()
      case BatchProcessType.TC_EFTREVERSALFILEPROCESSBATCHPROCESS:
        return new TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob()
      case BatchProcessType.TC_POLICYPERIODEFFECTIVESTATUS:
        return new TDIC_PolicyPeriodEffectiveStatusBatchJob()
      case BatchProcessType.TC_CHECKSTATUSUPDATE_TDIC:
        return new TDIC_CheckStatusUpdateBatch()
      case BatchProcessType.TC_AUTHORIZENETOPENHANDLERCLEANUPBATCH_TDIC:
        return new TDIC_AuthorizeNetOpenHandlerCleanupBatch()
      case BatchProcessType.TC_RESENDINVOICES_TDIC:
        return new ResendInvoicesBatchProcess()
      case BatchProcessType.TC_CHANGEQUARTERLYV2TOV3_TDIC:
        return new ChangeQuarterlyPaymentPlan()
      case BatchProcessType.TC_UNAPPLIEDFUNDINVOICE_TDIC:
        return new UnappliedFundsTargetedToInvoices()
      case BatchProcessType.TC_UPDATERETURNPREMIUMPLAN_TDIC:
        return new ChangeDefaultReturnPremiumPlan()
      case BatchProcessType.TC_REMOVEGHOSTINVOICE_TDIC:
        return new RemoveGhostinvoices()
      case BatchProcessType.TC_UPDATEBILLINGPLAN_TDIC:
        return new UpdateTDICBillingPlan()
      case BatchProcessType.TC_UPDATEDELINQUENCYPLAN_TDIC:
        return new ChangeTDICDelinquencyPlan()
      case BatchProcessType.TC_SECURITYZONEUPATE_TDIC:
        return new SecurityZoneUpdateBatch_TDIC()
      case BatchProcessType.TC_ACCOUNTSECURITYZONEUPATE_TDIC:
        return new AccountSecurityZoneUpdateBatch_TDIC()
      case BatchProcessType.TC_POLICYPERIODSECURITYZONEUPATE_TDIC:
        return new PolicyPeriodSecurityZoneUpdateBatch_TDIC()
      case BatchProcessType.TC_TROUBLETICKETSDAILY_TDIC:
        return new TDIC_TroubleTicketsDailyBatch()
      case BatchProcessType.TC_RETURNCREDITALLOCATIONTOACCOUNTDEFAULT_TDIC:
        return new RetunCreditAllocationToAccountDefault_TDIC()
      default:
        return null
    }
  }
}