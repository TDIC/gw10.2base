package tdic.bc.integ.plugins.hpexstream.bo

/**
 * GW 526
 * 12/07/2015 Kesava Tavva
 *
 * This object is to source document request source details which will be used
 * to populate on document payload request to HP-Exstream
 */

class TDIC_DocumentRequestSource {
  private var _policyPeriod : PolicyPeriod as PolicyPeriodSource
  private var _directBillMoneyRcvd : DirectBillMoneyRcvd as PaymentReversalInfo
}