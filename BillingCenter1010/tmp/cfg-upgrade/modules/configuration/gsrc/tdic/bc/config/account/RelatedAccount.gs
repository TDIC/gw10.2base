package tdic.bc.config.account
/**
 * US62 - Related Accounts
 * 09/18/2014 Alvin Lee
 *
 * POGO class to hold not just the related Account entity, but the Contact/ADA Number linking the accounts.
 */
class RelatedAccount {

  /**
   * The Contact entity with the ADA Number used to link the accounts.
   */
  private var _contactLink : Contact as Contact

  /**
   * The related Account entity.
   */
  private var _relatedAccount : Account as Account

}