package tdic.bc.config.enhancement
uses java.lang.IllegalArgumentException

/**
 * US62
 * 09/09/2014 Vicente Ortega
 *
 * Contact Enhancement for TDIC.
 */
enhancement ContactEnhancement: entity.Contact {

  /**
   * Return the ADA Number from the Official ID table
   */
  property get ADANumberOfficialID_TDIC() : String {
    return this.OfficialIDs.firstWhere( \ id -> id.OfficialIDType == OfficialIDType.TC_ADANUMBER_TDIC).OfficialIDValue
  }

  /**
   * Set the ADA Number in the Official ID and Contact entity
   */
  property set ADANumberOfficialID_TDIC(value : String) {
    if(value != null) {
      // Grab the number of official IDs with the Official ID type of ADA Number
      var adaNumberCount = this.OfficialIDs.countWhere( \ id -> id.OfficialIDType == OfficialIDType.TC_ADANUMBER_TDIC )

      // Check if there is more than one official IDs with the official ID Type of ADA Number
      if (adaNumberCount > 0) {
        if (adaNumberCount > 1) {
          throw new IllegalArgumentException("Can not have more than one ADA number in the Person Contact")
        }
        this.OfficialIDs.firstWhere( \ id -> id.OfficialIDType == OfficialIDType.TC_ADANUMBER_TDIC).OfficialIDValue = value
      }
      else {
        // Create a new Official ID object
        var anOfficialID = new OfficialID(this.Bundle)
        anOfficialID.OfficialIDType = OfficialIDType.TC_ADANUMBER_TDIC
        anOfficialID.OfficialIDValue = value
        // Add the new Official ID to the OfficialIDs Array
        this.addToOfficialIDs(anOfficialID)
      }
    }
  }
}
