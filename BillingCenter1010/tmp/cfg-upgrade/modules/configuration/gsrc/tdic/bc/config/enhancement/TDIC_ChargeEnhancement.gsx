package tdic.bc.config.enhancement

enhancement TDIC_ChargeEnhancement : Charge {

  property get ChargeName_TDIC(): String {
    if (this.BillingInstruction.DisplayName?.equalsIgnoreCase("Audit")) {
      return this.BillingInstruction.DisplayName + " " + this.DisplayName
    } else if (this.ChargePattern.ChargeCode?.equalsIgnoreCase("Deductible")){
      if (this.ClaimID_TDIC != null)
        return  this.ChargePattern.ChargeCode + " - Claim # " + this.ClaimID_TDIC
      else
        return this.DisplayName
    } else if(this.ChargePattern.ChargeCode?.equalsIgnoreCase("Premium")){
       return this.ChargePattern.ChargeCode + " Installment"
    } else if(this.ChargePattern.ChargeCode?.equalsIgnoreCase("PolicyRecapture")){
      return "Refund Reversal"
    } else {
      return this.DisplayName
    }
  }
}
