package tdic.bc.common.batch.banklockbox.dto

uses java.math.BigDecimal

/**
 * US567 - Bank/Lockbox Integration
 * 08/19/2014 Alvin Lee
 *
 * Object used to store fields for a transaction line in the bank/lockbox file.
 */
class InvoiceLine {

  /**
   * The policy number on the invoice, to which the payment is being applied.
   */
  var _policyNumber : String as PolicyNumber

  /**
   * The term number for the policy, to which the payment is being applied.
   */
  var _termNumber : int as TermNumber

  /**
   * The number of the invoice, to which the payment is being applied.
   */
  var _invoiceNumber : String as InvoiceNumber
  /**
      * The amount of the invoice, to which the payment is being applied.
      */
  var _invoiceAmount : BigDecimal as InvoiceAmount

}