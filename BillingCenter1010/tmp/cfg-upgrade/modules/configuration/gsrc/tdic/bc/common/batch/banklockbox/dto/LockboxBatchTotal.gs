package tdic.bc.common.batch.banklockbox.dto

uses java.math.BigDecimal

/**
 * US567 - Bank/Lockbox Integration
 * 01/09/2015 Alvin Lee
 *
 * Fields to keep track of the totals for a payment batch within the lockbox flat file.
 */
class LockboxBatchTotal {

  /**
   * The identifying number of the payment batch.
   */
  private var _batchNumber : String as BatchNumber

  /**
   * Total number of transactions in the payment batch.
   */
  private var _totalNumberTransactions : int as TotalNumberTransactions

  /**
   * Total amount of the transactions in the payment batch.
   */
  private var _totalAmount : BigDecimal as TotalAmount
}