package tdic.bc.config.invoice

uses gw.pl.currency.MonetaryAmount
uses java.util.Date
uses gw.i18n.DateTimeFormat
uses gw.api.upgrade.Coercions

//20180529 TJT GW-3116: Generating Payment Schedule Previews that appear on printed invoices
// Family of files:
//  TDIC_InvoiceItemPreview.gs (this file)
//  TDIC_PaymentPlanPreview.gs
//  TDIC_InvoiceEnhancement.gsx
//  TDIC_BCInvoiceModel.gx

class TDIC_InvoiceItemPreview {
  // Formatting performed on the GW side, instead of the Reporting side.
  // Only  following fields are exposed on the TDIC_BCInvoiceModel.gx:
  //  InstallmentNumber (int), EventDateString (String), DueDateString (String), AmountString (String)

  private var _installmentNumber : int as InstallmentNumber
  private var _eventDate : Date as EventDate
  private var _eventDateString : String as EventDateString
  private var _dueDate : Date as DueDate
  private var _dueDateString : String as DueDateString
  private var _chargeNameString : String as ChargeNameString // the OOTB class that generates this is always returnning null
  private var _amount : MonetaryAmount as Amount
  private var _amountString : String as AmountString
  private var _invoiceItemTypeString : String as InvoiceItemTypeString // the OOTB that generates provides two values (deposit, installment)
  //KesavaT - These two fields added for HP-Exstream purpose to provide values in one dimensional array
  private var _paymentPlanName: String as PaymentPlanName
  private var _currentlySelected: boolean as CurrentlySelected

  construct(){}

  construct(passedInEntry : gw.api.domain.invoice.ChargeInstallmentChanger.Entry,passedInPaymentPlanName : String, passedinCurrentlySelected : boolean) {
    _installmentNumber = Coercions.makePIntFrom(passedInEntry.Invoice.InvoiceNumber) // always null: passedInEntry.InvoiceItem.InstallmentNumber
    _eventDate = passedInEntry.EventDate
    _eventDateString = _eventDate.formatDate(DateTimeFormat.SHORT)
    _dueDate = passedInEntry.Invoice.DueDate // always null: passedInEntry.InvoiceItem.InvoiceDueDate
    _dueDateString = _dueDate.formatDate(DateTimeFormat.SHORT)
    //_chargeName = passedInEntry.Charge.getDisplayName()  //e.g. CIGAe
    _amount = passedInEntry.Amount
    _amountString = gw.api.util.StringUtil.formatNumber(_amount.Amount.doubleValue(), "$######.00")
    _invoiceItemTypeString = passedInEntry.InvoiceItemType.Description
    _paymentPlanName = passedInPaymentPlanName
    _currentlySelected = passedinCurrentlySelected
  }

  // The system loops through Invoice Items, and we need to combine Invoice Items with identical Event/Due dates.  Specifically, we combine the amount values.
  function updateInstallment (passedInEntry : gw.api.domain.invoice.ChargeInstallmentChanger.Entry){
    _amount = (_amount == null ? passedInEntry.Amount : _amount + passedInEntry.Amount)
    _amountString = gw.api.util.StringUtil.formatNumber(_amount.Amount.doubleValue(), "$######.00")     // formatting performed on the GW side, instead of the Reporting side
  }
}