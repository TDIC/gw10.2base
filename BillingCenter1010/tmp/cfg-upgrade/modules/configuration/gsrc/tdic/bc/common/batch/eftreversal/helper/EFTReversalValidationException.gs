package tdic.bc.common.batch.eftreversal.helper

uses java.lang.Exception

/**
 * US1126 - CR - EFT/ACH Reversal Automation
 * 02/05/2015 Vicente
 *
 * Extension of Exception for EFT/ACH Reversal and Change Notifications batch validations
 */
class EFTReversalValidationException extends Exception {

  construct(message : String) {
    super(message)
  }
}