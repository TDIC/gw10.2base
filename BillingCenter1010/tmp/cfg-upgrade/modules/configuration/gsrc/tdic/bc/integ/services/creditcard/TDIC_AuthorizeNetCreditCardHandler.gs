package tdic.bc.integ.services.creditcard

uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.DisplayableException
uses gw.api.web.SessionVar
uses java.lang.Exception
uses java.lang.Integer
uses java.lang.Long
uses java.lang.NumberFormatException
uses java.lang.System
uses java.math.BigDecimal
uses java.util.ArrayList
uses java.util.Calendar
uses java.util.Date
uses java.util.Map
uses java.util.concurrent.ConcurrentHashMap
uses java.util.HashMap
uses java.util.List
uses net.authorize.Environment
uses net.authorize.Merchant
uses net.authorize.ResponseField
uses net.authorize.cim.Result
uses net.authorize.cim.TransactionType
uses net.authorize.cim.ValidationModeType
uses net.authorize.data.Order
uses net.authorize.data.cim.CustomerProfile
uses net.authorize.data.cim.PaymentProfile
uses net.authorize.data.cim.PaymentTransaction
uses gw.api.locale.DisplayKey

/**
 * US679, US680, DE67
 * 01/14/2015 Rob Kelly
 *
 * A Credit Card Handler for making credit card payments via Authorize.Net.
 */
public class TDIC_AuthorizeNetCreditCardHandler implements TDIC_CreditCardHandler {

  /**
   * The logger tag for this Credit Card Handler.
   */
  private static final var LOG_TAG = "TDIC_AuthorizeNetCreditCardHandler#"

  /**
   * The name of the Authorize.Net Login ID property in cached properties.
   */
  private static final var API_LOGIN_ID_PROPERTY = "auth.net.api.login"

  /**
   * The name of the Authorize.Net Transaction Key property in cached properties.
   */
  private static final var TRANSACTION_KEY_PROPERTY = "auth.net.trans.key"

  /**
   * The name of the Authorize.Net Environment property in cached properties.
   */
  private static final var ENVIRONMENT_PROPERTY = "auth.net.env"

  /**
   * The name of the Authorize.Net Time-Out property in cached properties.
   */
  private static final var TIME_OUT_PROPERTY = "auth.net.timeout"

  /**
   * The name of the Authorize.Net Payment Cutoff Time property in cached properties.
   */
  private static final var PAYMENT_CUTOFF_TIME_PROPERTY = "auth.net.payment.cutoff"

  /**
   * The root name of the Authorize.Net Open Handler properties in cached properties.
   *
   * @see TDIC_AuthorizeNetCreditCardHandler#OPEN_HANDLER_PROPERTY_ROOT_SERVERID
   */
  private static final var OPEN_HANDLER_PROPERTY_ROOT = "auth.net.openhandler."

  /**
   * The root name of the Authorize.Net Open Handler properties in cached properties for Open Credit Card Handlers on this instance.
   * IMPORTANT: These cached properties should be updated by this class only.
   *
   * //@see startCreditCardProcess(Account,PaymentInstrument)
   * //@see finishCreditCardProcess()
   * //@see TDIC_AuthorizeNetCreditCardHandler#load()
   */
  private static final var OPEN_HANDLER_PROPERTY_ROOT_SERVERID = OPEN_HANDLER_PROPERTY_ROOT + gw.api.system.server.ServerUtil.getServerId() + "."

  /**
   * The value of the Authorize.Net Environment property indicating the sandbox environment.
   */
  private static final var SANDBOX_ENVIRONMENT = "sandbox"

  /**
   * The value of the Authorize.Net Environment property indicating the production environment.
   */
  private static final var PRODUCTION_ENVIRONMENT = "prod"

  /**
   * The default payment cutoff hour for Authorize.Net Credit Card Handlers.
   */
  private static final var DEFAULT_PAYMENT_CUTOFF_HOUR = 17

  /**
   * The default payment cutoff minute for Authorize.Net Credit Card Handlers.
   */
  private static final var DEFAULT_PAYMENT_CUTOFF_MINUTE = 29

  /**
   * The maximum number of characters allowed in Authorize.Net payment descriptions.
   */
  private static final var MAX_PAYMENT_DESCRIPTION_LENGTH = 255

  /**
   * All "open" Authorize.Net Credit Card Handlers on this server instance.
   *
   * When the credit card process starts for a handler, that handler is open and is added to this map.
   * When the credit card process finishes for a handler, that handler is no longer open and is removed from this map.
   *
   * There may be any number of open credit card handlers on a server instance, but there will be at most one open credit card handler for each user session.
   * The ID of the Authorize.Net customer profile associated with the currently open credit card handler for the current user session is stored in the session variable _openProfileIDSessionVar.
   *
   * //@see startCreditCardProcess(Account,PaymentInstrument)
   * //@see finishCreditCardProcess()
   * @see TDIC_AuthorizeNetCreditCardHandler#_openProfileIDSessionVar
   * //@see TDIC_AuthorizeNetCreditCardHandler#cleanupOnLogout()
   */
  private static var _openCreditCardHandlers = new ConcurrentHashMap<String, TDIC_AuthorizeNetCreditCardHandler>()

  /**
   * The ID of the Authorize.Net customer profile associated with the currently open Authorize.Net Credit Card Handler for the current user session.
   *
   * //@see startCreditCardProcess(Account,PaymentInstrument)
   * //@see finishCreditCardProcess()
   * @see TDIC_AuthorizeNetCreditCardHandler#_openProfileIDSessionVar
   * //@see TDIC_AuthorizeNetCreditCardHandler#cleanupOnLogout()
   */
  private static var _openProfileIDSessionVar = new SessionVar()

  /**
   * The Authorize.Net API Login ID for this Credit Card Handler.
   */
  private var apiLoginID : String

  /**
   * The Authorize.Net Transaction Key for this Credit Card Handler.
   */
  private var transactionKey : String

  /**
   * The Authorize.Net Environment for this Credit Card Handler, either SANDBOX_ENVIRONMENT or PRODUCTION_ENVIRONMENT.
   */
  private var environment : String

  /**
   * The Authorize.Net Merchant for this Credit Card Handler.
   */
  private var merchant : Merchant

  /**
   * The maximum time allowed (in ms) to complete the credit card payment process using this Credit Card Handler.
   */
  private var processTimeOut : long

  /**
   * The time of day at which settlement occurs for payments made using this Credit Card Handler.
   */
  private var paymentCutOffTime : Date

  /**
   * The account number with which this Credit Card Handler is associated.
   */
  private var accountNumber : String

  /**
   * The ID of the Authorize.Net Customer Profile for this Credit Card Handler.
   */
  private var customerProfileID : String

  /**
   * The ID of the Authorize.Net Payment Profile for this Credit Card Handler.
   */
  private var paymentProfileID : String

  /**
   * The time at which the credit card payment process started for this Credit Card Handler.
   */
  private var processStartTime : long = -1


  private static final var CONVENIENCE_FEE_PAYMENT_DESC = "Convenience Fee Charge"

  /**
   * Deletes any "orphan" Authorize.Net customer profiles created by Credit Card Handlers on this server instance.
   * This method should be added to config\startup\preload.txt and is used to recover from sessions that crash before an open Credit Card Handler finishes.
   */
  public static function load() {

    var logTag =  LOG_TAG + "load() - "
    LOGGER.info(logTag + "Searching for cached properties identifying open credit card handlers")
    var openHandlerKeys = PropertyUtil.getInstance().getIntProperties().keySet().where(\k -> (k as String).contains(OPEN_HANDLER_PROPERTY_ROOT_SERVERID))
    if (openHandlerKeys == null or openHandlerKeys.size() == 0) {

      LOGGER.info(logTag + "No open credit card handlers found")
      return
    }

    var apiLoginID = PropertyUtil.getInstance().getProperty(API_LOGIN_ID_PROPERTY)
    var transactionKey = PropertyUtil.getInstance().getProperty(TRANSACTION_KEY_PROPERTY)
    var environment = PropertyUtil.getInstance().getProperty(ENVIRONMENT_PROPERTY)
    var merchant = createMerchant(environment, apiLoginID, transactionKey)
    var openHandlerCustomerProfileID : String
    for (anOpenHandlerKey in openHandlerKeys) {

      openHandlerCustomerProfileID = PropertyUtil.getInstance().getProperty(anOpenHandlerKey as String)
      var accountNumber = extractAccountNumberFromPropertyName(anOpenHandlerKey as String)
      LOGGER.info(logTag + "Deleting customer profile " + openHandlerCustomerProfileID + " for open credit card handler on account " + accountNumber)
      deleteCustomerProfile(merchant, openHandlerCustomerProfileID)
      PropertyUtil.getInstance().getIntProperties().remove(anOpenHandlerKey)
    }
  }

  /**
   * Returns the account number contained in the specified property name.
   */
  @Param("aPropertyName", "a property name containing an account number")
  @Returns("the account number contained in aPropertyName as a String")
  public static function extractAccountNumberFromPropertyName(aPropertyName : String) : String {

    if (aPropertyName == null or not aPropertyName.contains(OPEN_HANDLER_PROPERTY_ROOT)) {

      return null
    }

    var propertyNameElements = aPropertyName.split("\\.")
    return propertyNameElements[propertyNameElements.length - 1]
  }

  /**
   * Returns the timed out Credit Card Handlers on this instance.
   */
  @Returns("the timed out Credit Card Handlers on this instance as a List")
  public static function getTimedOutHandlers() : List<TDIC_AuthorizeNetCreditCardHandler> {

    var timedOutHandlers = new ArrayList<TDIC_AuthorizeNetCreditCardHandler>()
    var aHandler : TDIC_AuthorizeNetCreditCardHandler
    for (aKey in _openCreditCardHandlers.keySet()) {

      aHandler = _openCreditCardHandlers.get(aKey)
      if (aHandler.isTimedOut()) {

        timedOutHandlers.add(aHandler)
      }
    }

    return timedOutHandlers
  }

  /**
   * Returns true if open handler is found and cleared, false otherwise.
   */
  @Returns("the timed out Credit Card Handlers on this instance as a List")
  public static function openHandlerExists(handlerProfileId : String) : boolean {

    var aHandler : TDIC_AuthorizeNetCreditCardHandler
    if(_openCreditCardHandlers.size() > 0){
      for (aKey in _openCreditCardHandlers.keySet()) {
        if(aKey == handlerProfileId){
          aHandler = _openCreditCardHandlers.get(aKey)
          aHandler.finishCreditCardProcess()
          return true
        }
      }
    }
    return false
  }

  /**
   * Determines whether there is open Authorize.Net Credit Card Handler for the current user session.
   */
  @Returns("true if cleanup is required; false otherwise")
  public static function isSessionCleanupRequired() : boolean {

    if (_openProfileIDSessionVar.RequestAvailable) {

      var openProfileID = _openProfileIDSessionVar.get() as String
      return openProfileID != null
    }

    return false
  }

  /**
   * Finishes the open Authorize.Net Credit Card Handler for the current user session.
   * Does nothing if there is no open Credit Card Handler.
   */
  public static function cleanupSession() {

    var logTag =  LOG_TAG + "cleanupSession() - "
    LOGGER.info(logTag + "Looking for an open credit card handler")
    if (_openProfileIDSessionVar.RequestAvailable) {

      var openProfileID = _openProfileIDSessionVar.get() as String
      if (openProfileID != null) {

        var openHandler = _openCreditCardHandlers.get(openProfileID)
        // robk, 11/21/2014: The Open Credit Card Handler may have already been cleaned up by an instance of the batch process TDIC_AuthorizeNetCreditCardHandlerCleanupBatch
        if (openHandler != null) {

          LOGGER.info(logTag + "Found open credit card handler for customer profile with ID " + openProfileID)
          openHandler.finishCreditCardProcess()
        }
      }
    }
  }

  /**
   * Creates a new Merchant for the Authorize.Net merchant account identified by the specified environment, API Login ID and Transaction Key.
   */
  @Param("anEnvironment", "the environment for the required merchant")
  @Param("anAPILoginID", "the API Login ID for the required merchant")
  @Param("aTransactionKey", "the Transaction Key for the required merchant")
  @Returns("the new Merchant")
  @Throws(DisplayableException, "if the environment is invalid")
  public static function createMerchant(anEnvironment : String, anAPILoginID : String, aTransactionKey : String) : Merchant {

    var logTag = LOG_TAG + "createMerchant(String,String,String) - "
    var newMerchant : Merchant
    if (anEnvironment == SANDBOX_ENVIRONMENT) {

      newMerchant = Merchant.createMerchant(Environment.SANDBOX, anAPILoginID, aTransactionKey)
    }
    else if (anEnvironment == PRODUCTION_ENVIRONMENT) {

      newMerchant = Merchant.createMerchant(Environment.PRODUCTION, anAPILoginID, aTransactionKey)
    }
    else {

      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.UnsupportedPropertyValue", anEnvironment, ENVIRONMENT_PROPERTY))
    }

    LOGGER.info(logTag + "New Authorize.Net Merchant created with API Login ID " + newMerchant.getLogin())
    return newMerchant
  }

  /**
   * Deletes the Authorize.Net customer profile with the specified ID from the specified merchant account.
   */
  @Param("aMerchant", "the merchant account from which to delete the customer profile")
  @Param("aCustomerProfileID", "the id of the customer profile to delete")
  @Returns("the response code returned by Authorize.Net for the delete profile transaction")
  public static function deleteCustomerProfile(aMerchant : Merchant, aCustomerProfileID : String) : String {

    var logTag = LOG_TAG + "deleteCustomerProfile(Merchant,String) - "
    var resultCode : String
    if (aCustomerProfileID != null) {

      LOGGER.info(logTag + "Deleting customer profile " + aCustomerProfileID)
      var transaction = aMerchant.createCIMTransaction(TransactionType.DELETE_CUSTOMER_PROFILE)
      transaction.setRefId(Long.toString(System.currentTimeMillis()))
      transaction.setCustomerProfileId(aCustomerProfileID)
      var result = aMerchant.postTransaction(transaction) as Result<Transaction>
      logDebugMessages(result.getMessages(), logTag)
      if (result != null) {

        resultCode = result.getResultCode()
        if (resultCode == Result.OK) {

          LOGGER.info(logTag + "Customer profile " + aCustomerProfileID + " deleted")
        }
        else {

          LOGGER.warn(logTag + resultCode + " returned from Authorize.Net; customer profile " + aCustomerProfileID + " may need to be deleted manually from merchant account " + aMerchant.getLogin())
        }
      }
      else {

        LOGGER.warn(logTag + "null returned from Authorize.Net; customer profile " + aCustomerProfileID + " may need to be deleted manually from merchant account " + aMerchant.getLogin())
      }
    }
    return resultCode
  }

  /**
   * Creates an Authorize.Net customer profile with the specified description, account number and e-mail address.
   */
  @Param("aMerchant", "the merchant account on which to create the customer profile")
  @Param("aDescription", "a description of the new customer profile")
  @Param("anAccountNumber", "the id of the new customer profile")
  @Param("anEmail", "the e-mail address of the new customer profile")
  @Returns("the id of the newly created customer profile")
  @Throws(DisplayableException, "if no customer profile id for the new customer profile is returned from Authorize.Net")
  private static function createCustomerProfile(aMerchant : Merchant, aDescription : String, anAccountNumber : String, anEmail : String) : String {

    var logTag = LOG_TAG + "createCustomerProfile(Merchant,String,String,String) - "
    LOGGER.info(logTag + "Creating new customer profile id for Account " + anAccountNumber)

    var customerProfile = CustomerProfile.createCustomerProfile()
    customerProfile.setDescription(aDescription);
    customerProfile.setMerchantCustomerId(anAccountNumber)
    customerProfile.setEmail(anEmail)

    var transaction = aMerchant.createCIMTransaction(TransactionType.CREATE_CUSTOMER_PROFILE)
    transaction.setRefId(Long.toString(System.currentTimeMillis()))
    transaction.setCustomerProfile(customerProfile)
    transaction.setValidationMode(ValidationModeType.NONE)
    var result = aMerchant.postTransaction(transaction) as Result<Transaction>
    var newCustomerProfileID : String
    if (result != null) {

      if (result.getResultCode() == Result.OK) {

        logDebugMessages(result.getMessages(), logTag)
        newCustomerProfileID = result.getCustomerProfileId()
        if (newCustomerProfileID != null) {

          LOGGER.info(logTag + "New customer profile id " + newCustomerProfileID + " created for Account " + anAccountNumber)
        }
        else {

          LOGGER.error(logTag + "No customer profile id returned from Authorize.Net for new customer profile for Account " + anAccountNumber)
          throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.NoCustomerProfileIDReturned"))
        }
      }
      else {

        logErrorMessages(result.getMessages(), logTag)
        throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.ErrorReturned", extractFirstMessageText(result.getMessages())))
      }
    }
    else {

      LOGGER.error(logTag + "null result returned from Authorize.Net")
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.NoCustomerProfileIDReturned"))
    }

    return newCustomerProfileID
  }

  /**
   * Gets a token for the secure hosted page for the specified Authorize.net customer profile.
   */
  @Param("aMerchant", "the merchant account on which the customer profile exists")
  @Param("aCustomerProfileID", "The ID of the customer profile for which the token is required")
  @Returns("A token for accessing the hosted profile page")
  @Throws(DisplayableException, "if no token is returned from Authorize.net")
  private static function getHostedProfilePageToken(aMerchant : Merchant, aCustomerProfileID : String) : String {

    var logTag = LOG_TAG + "getHostedProfilePageToken(Merchant,String) - "
    var token : String
    if (aCustomerProfileID != null) {

      LOGGER.info(logTag + "Getting hosted token for customer profile " + aCustomerProfileID)
      var transaction = aMerchant.createCIMTransaction(TransactionType.GET_HOSTED_PROFILE_PAGE)
      transaction.setRefId(Long.toString(System.currentTimeMillis()))
      transaction.setCustomerProfileId(aCustomerProfileID)
      var result = aMerchant.postTransaction(transaction) as Result<Transaction>
      if (result != null) {

        if (result.getResultCode() == Result.OK) {

          logDebugMessages(result.getMessages(), logTag)
          token = result.getToken()
          if (token != null) {

            LOGGER.info(logTag + "Got Token " + result.Token)
          }
          else {

            LOGGER.error(logTag + "No token returned for customer profile " + aCustomerProfileID)
            throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.NoTokenReturned", aCustomerProfileID))
          }
        }
        else {

          logErrorMessages(result.getMessages(), logTag)
          throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.ErrorReturned", extractFirstMessageText(result.getMessages())))
        }
      }
      else {

        LOGGER.error(logTag + "null result returned from Authorize.Net")
        throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.NoTokenReturned", aCustomerProfileID))
      }
    }

    return token
  }

  /**
   * Retrieves the payment profile for the Authorize.Net customer profile identified by the specified ID.
   */
  @Param("aMerchant", "the merchant account on which the customer profile exists")
  @Param("aCustomerProfileID", "the ID of the customer profile for which the payment profile is required")
  @Returns("the retrieved customer profile")
  private static function retrievePaymentProfile(aMerchant : Merchant, aCustomerProfileID : String) : PaymentProfile {

    var logTag = LOG_TAG + "retrievePaymentProfile(Merchant,String) - "
    var paymentProfile : PaymentProfile
    var transaction = aMerchant.createCIMTransaction(TransactionType.GET_CUSTOMER_PROFILE)
    transaction.setRefId(Long.toString(System.currentTimeMillis()))
    transaction.setCustomerProfileId(aCustomerProfileID)
    var result = aMerchant.postTransaction(transaction) as Result<Transaction>
    if (result != null) {

      if (result.getResultCode() == Result.OK) {

        logDebugMessages(result.getMessages(), logTag)
        var paymentProfiles = result.getCustomerPaymentProfileList()
        if (paymentProfiles == null or paymentProfiles.size() == 0) {

          LOGGER.error(logTag + "No payment profiles found for customer profile " + aCustomerProfileID)
          throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.NoPaymentProfileReturned", aCustomerProfileID))
        }
        else {

          if (paymentProfiles.size() > 1) {

            LOGGER.warn(logTag + paymentProfiles.size() + " payment profiles found for customer profile " + aCustomerProfileID + "; using first payment profile")
          }

          paymentProfile = paymentProfiles.first()
        }
      }
      else {

        logErrorMessages(result.getMessages(), logTag)
        throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.ErrorReturned", extractFirstMessageText(result.getMessages())))
      }
    }
    else {

      LOGGER.error(logTag + "null result returned from Authorize.Net; payment profile could not be retrieved for the customer profile " + aCustomerProfileID)
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.NoPaymentProfileReturned", aCustomerProfileID))
    }

    return paymentProfile
  }

  /**
   * Creates an Authorize.Net payment transaction for the specified amount and with the specified description.
   */
  @Param("aMerchant", "the merchant account on which the customer profile exists")
  @Param("aCustomerProfileId", "the ID of the customer profile for which the payment is to be made")
  @Param("aPaymentProfileID", "the ID of the payment profile with which the payment is to be made")
  @Param("anAmount", "the amount of the created transaction")
  @Param("aDescription", "the description of the created transaction")
  @Returns("the authorization code for the payment transaction; null if the payment transaction could not be created")
  private static function createPaymentTransaction(aMerchant : Merchant, aCustomerProfileId : String, aPaymentProfileID : String, anAmount : BigDecimal, aDescription : String) : Map<ResponseField, String> {

    var logTag = LOG_TAG + "createPaymentTransaction(Merchant,BigDecimal,String) - "
    if (anAmount == null or anAmount.doubleValue() == 0 or anAmount.signum() < 0) {

      LOGGER.error(logTag + "The amount " + anAmount + " is invalid for a payment transaction")
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.InvalidAmount", anAmount.toString()))
    }

    if (aDescription != null and aDescription.length > MAX_PAYMENT_DESCRIPTION_LENGTH) {

      LOGGER.error(logTag + "The description \"" + aDescription + "\" is invalid for a payment transaction; the length of the payment descripion is longer than the maximum permitted of " + MAX_PAYMENT_DESCRIPTION_LENGTH + " characters")
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.DescriptionLengthExceedsMax", aDescription, MAX_PAYMENT_DESCRIPTION_LENGTH))
    }

    var paymentTransaction = PaymentTransaction.createPaymentTransaction()
    paymentTransaction.setTransactionType(net.authorize.TransactionType.AUTH_CAPTURE)
    paymentTransaction.setCustomerPaymentProfileId(aPaymentProfileID)
    var order = Order.createOrder()
    order.setTotalAmount(anAmount)
    order.setDescription(aDescription)
    paymentTransaction.setOrder(order)

    var transaction = aMerchant.createCIMTransaction(TransactionType.CREATE_CUSTOMER_PROFILE_TRANSACTION)
    var refID = Long.toString(System.currentTimeMillis())
    transaction.setRefId(refID)
    transaction.setCustomerProfileId(aCustomerProfileId)
    transaction.setPaymentTransaction(paymentTransaction)

    LOGGER.info(logTag + "Posting payment transaction with refID " + refID)
    var result = aMerchant.postTransaction(transaction) as Result<Transaction>
    var directResponseMap: Map<ResponseField, String>
    if (result != null) {

      logDebugMessages(result.getMessages(), logTag)
      var directResponseList = result.getDirectResponseList()
      for (aDirectResponse in directResponseList) {

        LOGGER.info(logTag + "Direct Response: " + aDirectResponse.getDirectResponseString())

      }

      if (result.getResultCode() == Result.OK) {

        var paymentTransactionID : String
        if (directResponseList == null or directResponseList.size() == 0) {

          LOGGER.warn(logTag + "No direct responses received for payment transaction with refID " + refID)
        }
        else {

          if (directResponseList.size() > 1) {

            LOGGER.warn(logTag + directResponseList.size() + " direct responses received for payment transaction with refID " + refID + "; using first direct response")
          }

          directResponseMap = result.getDirectResponseList().first().getDirectResponseMap()
        }

        LOGGER.info(logTag + "Payment of " + anAmount + " made to payment profile " + aPaymentProfileID + ", Response from A.net: " + directResponseMap + ")")
      }
      else {
        logErrorMessages(result.getMessages(), logTag + "Error Message for Payment with refID " + refID + ": ")
        throw new DisplayableException(DisplayKey.get("TDIC.PaymentFailed", extractFirstMessageText(result.getMessages())))
      }
    }
    else {

      LOGGER.warn(logTag + "null returned from Authorize.Net; the  payment transaction with refID " + refID + " may not have been processed")
    }

    return directResponseMap
  }


  /**
   * Logs the specified messages at DEBUG level using the specified log tag if DEBUG is enabled.
   */
  private static function logDebugMessages(debugMessages : List<net.authorize.xml.Message>, aLogTag : String) {
    for (aMsg in debugMessages) {
      LOGGER.info(aLogTag + aMsg.getCode() + ", " + aMsg.getText())
    }
  }

  /**
   * Logs the specified messages at ERROR level using the specified log tag.
   */
  private static function logErrorMessages(errorMessages : List<net.authorize.xml.Message>, aLogTag : String) {

    for (aMsg in errorMessages) {

      LOGGER.error(aLogTag + aMsg.getCode() + ", " + aMsg.getText())
    }
  }

  /**
   * Returns the text of first Authorize.Net message in the specified list of messages; null if there is no first message in the list.
   */
  private static function extractFirstMessageText(messages : List<net.authorize.xml.Message>) : String {

    if (messages != null) {

      var firstMessage = messages.first()
      if (firstMessage != null) {

        return firstMessage.getText()
      }
    }

    return null
  }

  /**
   * Returns true if there is an Open Credit Handler for the specified account on any instance; false otherwise.
   */
  private static function isCreditCardHandlerOpenForAccount(anAccountNumber : String) : boolean {

    if (PropertyUtil.getInstance().getIntProperties() != null) {

      var openHandlerKeys = PropertyUtil.getInstance().getIntProperties().keySet().where(\k -> (k as String).contains(OPEN_HANDLER_PROPERTY_ROOT))
      for (aKey in openHandlerKeys) {

        if (extractAccountNumberFromPropertyName(aKey as String) == anAccountNumber) {

          return true
        }
      }
    }

    return false
  }

  /**
   * Constructs a new Authorize.Net Credit Card Handler using cached Authorize.Net properties.
   */
  construct() {

    this(PropertyUtil.getInstance().getProperty(API_LOGIN_ID_PROPERTY),
        PropertyUtil.getInstance().getProperty(TRANSACTION_KEY_PROPERTY),
        PropertyUtil.getInstance().getProperty(ENVIRONMENT_PROPERTY),
        PropertyUtil.getInstance().getProperty(TIME_OUT_PROPERTY),
        PropertyUtil.getInstance().getProperty(PAYMENT_CUTOFF_TIME_PROPERTY))
  }

  /**
   * Constructs a new Authorize.Net Credit Card Handler using the specified Authorize.Net properties.
   */
  construct(authNetApiLoginID : String, authNetTransactionKey : String, authNetEnvironment : String, authNetProcessTimeOutAsString : String, authNetPaymentCutoffTimeAsString : String) {

    this.apiLoginID = authNetApiLoginID
    this.transactionKey = authNetTransactionKey
    this.environment = authNetEnvironment
    setProcessTimeOut(authNetProcessTimeOutAsString)
    setPaymentCutoffTime(authNetPaymentCutoffTimeAsString)
  }

  /**
   * Sets the process time out for this Credit Card Handler using the specified String.
   */
  @Param("processTimeOutAsString", "the process time out for this Credit Card Handler as a String")
  private function setProcessTimeOut(processTimeOutAsString : String) {

    try {

      this.processTimeOut = Long.parseLong(processTimeOutAsString)
    }
    catch (nfe : NumberFormatException) {

      var logTag = LOG_TAG + "setProcessTimeOut(String) - "
      LOGGER.warn(logTag + DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.UnsupportedPropertyValue", processTimeOutAsString, TIME_OUT_PROPERTY))
      this.processTimeOut = -1
    }
  }

  /**
   * Sets the payment cutoff time for this Credit Card Handler using the specified String.
   */
  @Param("processTimeOutAsString", "the payment cut off time for this Credit Card Handler as a String")
  private function setPaymentCutoffTime(paymentCutoffTimeAsString : String) {

    var rightNow = Calendar.getInstance()
    rightNow.setLenient(false)
    var paymentCutoffHour = DEFAULT_PAYMENT_CUTOFF_HOUR
    var paymentCutoffMinute = DEFAULT_PAYMENT_CUTOFF_MINUTE
    try {

      if (paymentCutoffTimeAsString != null) {

        var paymentCutoffTimeAsTokens = paymentCutoffTimeAsString.split(":")
        if (paymentCutoffTimeAsTokens.length != 2) {

          throw paymentCutoffTimeAsString
        }

        paymentCutoffHour = Integer.parseInt(paymentCutoffTimeAsTokens[0])
        paymentCutoffMinute = Integer.parseInt(paymentCutoffTimeAsTokens[1])
        if (paymentCutoffHour < 0 or paymentCutoffHour > 23 or paymentCutoffMinute < 0 or paymentCutoffMinute > 59) {

          paymentCutoffHour = DEFAULT_PAYMENT_CUTOFF_HOUR
          paymentCutoffMinute = DEFAULT_PAYMENT_CUTOFF_MINUTE
          throw paymentCutoffTimeAsString
        }
      }
    }
    catch (e : Exception) {

      var logTag = LOG_TAG + "setPaymentCutoffTime(String) - "
      LOGGER.warn(logTag + DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.UnsupportedPropertyValue", paymentCutoffTimeAsString, PAYMENT_CUTOFF_TIME_PROPERTY))
    }
    finally {

      rightNow.set(Calendar.HOUR_OF_DAY, paymentCutoffHour)
      rightNow.set(Calendar.MINUTE, paymentCutoffMinute)
      this.paymentCutOffTime = rightNow.getTime()
    }
  }

  /**
   * Returns the time of day at which payments will be picked up for settlement by Authorize.Net.
   */
  @Returns("the time of day at which payments will be picked up for settlement as a Date")
  override function getPaymentCutOffTime() : Date {

    return this.paymentCutOffTime
  }

  /**
   * Returns the maximum number of characters permitted in the description of a credit card payment.
   *
   * @see #submitPayment(BigDecimal,PaymentInstrument,String)
   */
  @Returns("the the maximum length in characters of the description of a credit card payment as an integer")
  override public function getMaximumPaymentDescriptionLength() : int {

    return MAX_PAYMENT_DESCRIPTION_LENGTH
  }

  /**
   * Creates a new Authorize.Net customer profile for the specified Account and gets a token for the secure hosted profile page for the specified PaymentInstrument.
   */
  @Param("anAccount", "the account that the credit card payment pays")
  @Param("aPaymentInstrument", "the credit card payment instrument that pays the account")
  @Throws(DisplayableException, "if no customer profile id or no secure token is returned from Authorize.Net for the new customer profile ")
  override function startCreditCardProcess(anAccount : Account, aPaymentInstrument : PaymentInstrument) {
    var logTag = LOG_TAG + "startCreditCardProcess(Account,PaymentInstrument) - "
    LOGGER.info(logTag + "Credit Card Process started for Account: ${anAccount.AccountNumber}; PaymentInstrument: ${aPaymentInstrument}")
    if (anAccount != null and aPaymentInstrument != null and aPaymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_CREDITCARD) {

      this.merchant = createMerchant(this.environment, this.apiLoginID, this.transactionKey)
      if (this.environment == SANDBOX_ENVIRONMENT) {

        LOGGER.warn(logTag + "This Authorize.Net Credit Card Handler is using the sandbox environment")
      }

      LOGGER.info(logTag + "Process Time Out set to " + this.processTimeOut + "ms")
      if (this.processTimeOut <= 0) {

        throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.UnsupportedPropertyValue", this.processTimeOut, TIME_OUT_PROPERTY))
      }

      if (isCreditCardHandlerOpenForAccount(anAccount.AccountNumber)) {

        LOGGER.info(logTag + DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.PaymentAlreadyInProgress", anAccount.AccountNumber))
        new TDIC_AuthorizeNetOpenHandlerCleanup().removeOpenHandlers({OPEN_HANDLER_PROPERTY_ROOT_SERVERID + anAccount.AccountNumber})
        LOGGER.info(logTag + "Existing open handler for account: ${anAccount.AccountNumber} has been removed.")
      }

      this.accountNumber = anAccount.AccountNumber
      this.processStartTime = System.currentTimeMillis()

      var recipientEmail = aPaymentInstrument.CreditCardPayeeEmail_TDIC != null ? aPaymentInstrument.CreditCardPayeeEmail_TDIC : anAccount.PrimaryPayer.Contact.EmailAddress1
      this.customerProfileID = createCustomerProfile(this.merchant, anAccount.AccountName, anAccount.AccountNumber, recipientEmail)
      // robk, 11/17/2014: store the ID of the created profile in the HttpSession that is currently in scope and add this handler instance to the map of all open handler instances on this server
      if (_openProfileIDSessionVar.RequestAvailable) {

        _openProfileIDSessionVar.set(this.customerProfileID)
      }
      _openCreditCardHandlers.put(this.customerProfileID, this)
      // robk, 11/18/2014: store the ID of the created profile as a cached property to enable recovery following a session crash
      if (PropertyUtil.getInstance().getIntProperties() != null) {

        PropertyUtil.getInstance().getIntProperties().setProperty(OPEN_HANDLER_PROPERTY_ROOT_SERVERID + this.accountNumber, this.customerProfileID)
      }
      aPaymentInstrument.Token = getHostedProfilePageToken(this.merchant, this.customerProfileID)
    }
  }

  /**
   * Populates the specified PaymentInstrument with the masked card number associated with the Authorize.Net payment profile of this Credit Card Handler.
   */
  @Param("aPaymentInstrument", "a credit card payment instrument")
  @Throws(DisplayableException, "if the Credit Card Process has not been started, if the credit card process has timed out, or if no payment profile is returned from Authorize.Net")
  override function populateCreditCardFields(aPaymentInstrument : PaymentInstrument) {

    checkProcessTimeOutNotExpired()
    var logTag = LOG_TAG + "populateCreditCardFields(PaymentInstrument) - "
    if (this.customerProfileID == null) {

      LOGGER.error(logTag + "The Credit Card Process has not been started for this Credit Card Handler")
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.ProcessNotStarted"))
    }

    if (aPaymentInstrument != null and aPaymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_CREDITCARD) {

      LOGGER.info(logTag + "Getting payment profile for customer profile " + this.customerProfileID)
      var paymentProfile = retrievePaymentProfile(this.merchant, this.customerProfileID)
      if (paymentProfile != null) {

        this.paymentProfileID = paymentProfile.getCustomerPaymentProfileId()
        LOGGER.info(logTag + "Payment profile for this Credit Card Handler set to " + this.paymentProfileID)
        var paymentMethodList = paymentProfile.getPaymentList()
        if (paymentMethodList == null or paymentMethodList.size() == 0) {

          LOGGER.error(logTag + "No payment methods found for payment profile " + this.paymentProfileID)
          throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.NoPaymentMethodReturned", this.paymentProfileID))
        }
        else {

          if (paymentMethodList.size() > 1) {

            LOGGER.warn(logTag + paymentMethodList.size() + " payment methods found for payment profile " + this.paymentProfileID + "; using first payment method")
          }
          var paymentMethod = paymentMethodList.first()
          var creditCard = paymentMethod.getCreditCard()
          aPaymentInstrument.CreditCardNumber_TDIC = creditCard.getCreditCardNumber()
        }
        LOGGER.info(logTag + "Card Number on payment instrument set to " + aPaymentInstrument.CreditCardNumber_TDIC)
      }
      else {

        LOGGER.error(logTag + "null payment profile returned from Authorize.Net; payments cannot be submitted for the payment instrument " + aPaymentInstrument)
        throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.AuthorizeNet.NoPaymentProfileReturned", this.customerProfileID))
      }
    }
  }

  /**
   * Submits a payment for the specified amount on the specified credit card payment instrument.
   */
  @Param("anAmount", "the amount of the credit card payment")
  @Param("aPaymentInstrument", "the credit card payment instrument with which to make the payment")
  @Param("aDescription", "a description of the credit card payment")
  @Returns("an authorization code for the payment")
  @Throws(DisplayableException, "if the Credit Card Process has not been started, if the payment instrument does not have credit card fields set, if the credit card process has timed out, if the amount is invalid, or if the payment submission fails")
  function submitPayment(anAmount : BigDecimal, aPaymentInstrument : PaymentInstrument, aDescription : String) : Map<ResponseField, String> {

    checkProcessTimeOutNotExpired()
    var logTag = LOG_TAG + "submitPayment(BigDecimal,PaymentInstrument,String) - "
    if (this.customerProfileID == null) {

      LOGGER.error(logTag + "The Credit Card Process has not been started for this Credit Card Handler")
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.ProcessNotStarted"))
    }

    if (aPaymentInstrument.CreditCardNumber_TDIC == null) {

      LOGGER.error(logTag + "The Credit Card Number has not been set on the Payment Instrument " + aPaymentInstrument)
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.NoCreditCardNumber"))
    }

    var authNetResponse : Map<ResponseField, String>
    if (aPaymentInstrument != null and aPaymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_CREDITCARD) {

      if (LOGGER.isDebugEnabled()) LOGGER.info(logTag + "Making payment of " + anAmount + " to payment profile " + this.paymentProfileID + " with description \"" + aDescription + "\"")
      authNetResponse = createPaymentTransaction(this.merchant, this.customerProfileID, this.paymentProfileID, anAmount, (aDescription.HasContent?aDescription:"Invoice Payment"))
    }

    return authNetResponse
  }

  /**
   * Submits a payment for the specified amount on the specified credit card payment instrument.
   */
  @Param("anAmount", "the amount of the credit card payment")
  @Param("aPaymentInstrument", "the credit card payment instrument with which to make the payment")
  @Param("aDescription", "a description of the credit card payment")
  @Returns("an authorization code for the payment")
  @Throws(DisplayableException, "if the Credit Card Process has not been started, if the payment instrument does not have credit card fields set, if the credit card process has timed out, if the amount is invalid, or if the payment submission fails")
  function submitPaymentAndFee(anAmount : BigDecimal, aPaymentInstrument : PaymentInstrument, aDescription : String, moneyReceived: DirectBillMoneyRcvd) : Map<String,Map<ResponseField, String>> {

    checkProcessTimeOutNotExpired()
    var logTag = LOG_TAG + "submitPayment(BigDecimal,PaymentInstrument,String) - "
    if (this.customerProfileID == null) {

      LOGGER.error(logTag + "The Credit Card Process has not been started for this Credit Card Handler")
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.ProcessNotStarted"))
    }

    if (aPaymentInstrument.CreditCardNumber_TDIC == null) {

      LOGGER.error(logTag + "The Credit Card Number has not been set on the Payment Instrument " + aPaymentInstrument)
      throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.NoCreditCardNumber"))
    }
    var authNetResponses = new HashMap<String,Map<ResponseField, String>>()
    //var authNetResponse : Map<ResponseField, String>
    if (aPaymentInstrument != null and aPaymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_CREDITCARD) {

      //if (LOGGER.isDebugEnabled())
      LOGGER.info(logTag + "Making payment of " + anAmount + " to payment profile " + this.paymentProfileID + " with description \"" + aDescription + "\"")
      var invPaymentResponse = createPaymentTransaction(this.merchant, this.customerProfileID, this.paymentProfileID, anAmount, (aDescription.HasContent?aDescription:"Invoice Payment"))
      authNetResponses.put(INVOICE_PAYMENT_RESPONSE,invPaymentResponse)
      /* GWPS-2822 - Disable the Credit card Convenience fee from sending as a seperate transaction to Authorize.net.
      if(moneyReceived.CCFeeWaiverReason_TDIC == null){

        var feePaymentResponse = chargeConvenienceFee(aPaymentInstrument)
        authNetResponses.put(FEE_PAYMENT_RESPONSE,feePaymentResponse)
      }*/
    }
    return authNetResponses
  }

  /* GWPS-2822 - Disable the Credit card Convenience fee from sending as a seperate transaction to Authorize.net
 //TODO: Fee hard coded - use script param??
  private function chargeConvenienceFee(aPaymentInstrument : PaymentInstrument): Map<ResponseField, String>{
    return createPaymentTransaction(this.merchant, this.customerProfileID, this.paymentProfileID, new BigDecimal(convenienceFeeAmount), CONVENIENCE_FEE_PAYMENT_DESC)
  }*/

  /**
   * Deletes the Authorize.Net customer profile for this Credit Card Handler.
   */
  override function finishCreditCardProcess() {

    if (this.customerProfileID != null) {

      deleteCustomerProfile(this.merchant, this.customerProfileID)
      if (_openProfileIDSessionVar.RequestAvailable) {

        _openProfileIDSessionVar.set(null)
      }
      _openCreditCardHandlers.remove(this.customerProfileID, this)
      if (PropertyUtil.getInstance().getIntProperties() != null) {

        PropertyUtil.getInstance().getIntProperties().remove(OPEN_HANDLER_PROPERTY_ROOT_SERVERID + this.accountNumber)
      }
      this.accountNumber = null
      this.paymentProfileID = null
      this.customerProfileID = null
      this.processStartTime = 0
    }
  }

  /**
   * Returns true if this Credit Card Handler has timed out; false otherwise.
   */
  public function isTimedOut() : boolean {

    // robk, 11/24/2014: if the credit card process hasn't been started, then it hasn't timed out
    if (this.processStartTime < 0) {

      return false
    }

    var currentTime = System.currentTimeMillis()
    if (currentTime - this.processStartTime < this.processTimeOut) {

      return false
    }

    return true
  }

  /**
   * Throws a DisplayableException if the process time out for this Credit Card Handler has expired; otherwise does nothing.
   */
  private function checkProcessTimeOutNotExpired() {

    if (not isTimedOut()) {

      return
    }
    throw new DisplayableException(DisplayKey.get("TDIC.CreditCardHandler.ProcessTimeOut"))
  }

  /**
   * Returns the Account Number of this Credit Card Handler.
   */
  internal function getAccountNumber() : String {

    return this.accountNumber
  }

  /**
   * Returns the Authorize.Net customer profile of this Credit Card Handler.
   */
  internal function getCustomerProfileID() : String {

    return this.customerProfileID
  }

  /**
   * Returns the Authorize.Net payment profile of this Credit Card Handler.
   */
  internal function getPaymentProfileID() : String {

    return this.paymentProfileID
  }
}