package tdic.payment

uses gw.accounting.NewGeneralSingleChargeHelper
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount
uses gw.transaction.UserTransactionType
uses org.slf4j.LoggerFactory

uses java.math.BigDecimal

class DBPaymentReversalHelper {

  construct() {}

  function createReversalFees(moneyReceived : DirectBillMoneyRcvd, reason : PaymentReversalReason) {
    if(PaymentReversalReason.TF_ATFAULTREVERSALS_TDIC.getTypeKeys().hasMatch(\elt1 -> elt1 == reason)) {
      var originalDBMR = moneyReceived.OriginalVersion as DirectBillMoneyRcvd

      var chargesInDist = originalDBMR.BaseDist.DistItems*.InvoiceItem*.Charge
      if(chargesInDist.length == 0) {
        var chargeList = chargesInDist.toList()
        originalDBMR.Account.AllPolicyPeriods.each(\elt -> {
          elt.LatestPolicyPeriod.Charges.each(\c -> {
            chargeList.add(c)
          })
        })
        chargesInDist = chargeList.toTypedArray()
      }

      // Build a set of policy periods associated with the distribution
      var policyPeriods = new HashSet<PolicyPeriod>()
      for(charge in chargesInDist) {
        switch(typeof charge.TAccountOwner) {
          case PolicyPeriod:
            policyPeriods.add(charge.PolicyPeriod)
            break
          default:
        }
      }

        var policyPeriod = policyPeriods.where(\pp -> pp.IsInForce).orderByDescending(\ pp  -> pp.EffectiveDate).first()
        if(policyPeriod == null) {
          policyPeriod = policyPeriods.orderByDescending(\ pp  -> pp.EffectiveDate).first()
        }
        // SN: GBC-3013 - Need to turn off NSF/Payment Reversal Fee.
        /*var helper = new NewGeneralSingleChargeHelper(policyPeriod.Account, UserTransactionType.FEE_OR_GENERAL)
        helper.TAccountOwner = policyPeriod
        helper.ChargeInitializer.ChargePattern = ChargePatternKey.POLICYPAYMENTREVERSALFEE.get()
        var paymentReversalFeeAmount = ScriptParameters.getParameterValue("TDIC_PaymentReversalFeeAmount") as BigDecimal
        helper.ChargeInitializer.Amount = new MonetaryAmount(paymentReversalFeeAmount, policyPeriod.Currency)
        helper.BillingInstruction.execute()*/

      // create this activity only if the policyperiod has the prodcucercode "CATDIC"
      if(policyPeriod.HasCATDICProducerCode) {
        createAtFaultReversalActivity(policyPeriod)
      }
    }
  }

  function createAtFaultReversalActivity(plcyPeriod : PolicyPeriod) {
    // create an activity for Payment Reversal for At-Fault reason
    if(plcyPeriod.Account.DefaultPaymentInstrument.PaymentMethod == PaymentMethod.TC_ACH) {
      activityAPW(plcyPeriod.Account)
    } else {
      activityNonAPW(plcyPeriod.Account)
    }
  }

  // Activity for APW PI.
  function activityAPW (account : Account) {
    var actvty = new Activity()
    actvty.ActivityPattern = gw.api.web.admin.ActivityPatternsUtil.getActivityPattern("apw_payment_reversal")
    actvty.Priority = Priority.TC_HIGH
    actvty.Account = account
    var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Service - Billing")
    var queueName = groupName.getQueue("Service - Billing")
    if (actvty?.assignGroup(groupName) and queueName != null) {
      actvty.assignActivityToQueue(queueName, groupName)
    }
    var _logger = LoggerFactory.getLogger("Rules")
    if (actvty.assignActivityToQueue(queueName, groupName)) {
      _logger.debug("DBPaymentReversalHelper - addAtfaultReversalActivity for APW : assignment successful. Assigned Queue : " + actvty.AssignedQueue.DisplayName)
    }
    else {
      _logger.warn("DBPaymentReversalHelper - addAtfaultReversalActivity for APW : " + actvty.ActivityPattern.DisplayName)
    }
  }

  // Activity for Non APW PI.
  function activityNonAPW (account : Account) {
    var actvty = new Activity()
    actvty.ActivityPattern = gw.api.web.admin.ActivityPatternsUtil.getActivityPattern("at_fault_payment_reversal")
    actvty.Priority = Priority.TC_HIGH
    actvty.Account = account
    var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Service - Billing")
    var queueName = groupName.getQueue("Service - Billing")
    if (actvty?.assignGroup(groupName) and queueName != null) {
      actvty.assignActivityToQueue(queueName, groupName)
    }
    var _logger = LoggerFactory.getLogger("Rules")
    if (actvty.assignActivityToQueue(queueName, groupName)) {
      _logger.debug("DBPaymentReversalHelper - addAtfaultReversalActivity for responsive : assignment successful. Assigned Queue : " + actvty.AssignedQueue.DisplayName)
    }
    else {
      _logger.warn("DBPaymentReversalHelper - addAtfaultReversalActivity for resposive : " + actvty.ActivityPattern.DisplayName)
    }
  }

  // From integration layer reversals, we want to charge one NSF Fee per account per day.
  // Since the reversal fee is a policy level charge we need to associate the fee to either one of policies in order PL, CP, WC respectively.
  function isPaymentReversalFeeCreatedForToday (moneyReceived : DirectBillMoneyRcvd) : boolean {
    var account = moneyReceived.Account
    var currentDateCharges : List<Charge>
    account.AllPolicyPeriods.each(\pp -> {
      currentDateCharges = pp.Charges.where(\elt -> elt.ChargePattern == ChargePatternKey.POLICYPAYMENTREVERSALFEE.get() and elt.CreateTime.trimToMidnight() == DateUtil.currentDate().trimToMidnight())
    })
    if(!currentDateCharges.Empty){
      return true
    }
    return false
  }
}