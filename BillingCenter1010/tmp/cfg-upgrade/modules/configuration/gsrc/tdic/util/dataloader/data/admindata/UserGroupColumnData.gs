package tdic.util.dataloader.data.admindata

uses java.lang.Integer

class UserGroupColumnData {

  construct() {

  }
  private var _column               : Integer               as Column
  private var _group                : String                as Group
}
