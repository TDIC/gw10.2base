package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.AttributeData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses java.lang.Exception

class BCAttributeLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aAttributeData = applicationData as AttributeData
    try {
      var currAttribute = BCAdminDataLoaderUtil.findAttributeByPublicId(aAttributeData.PublicID)
      // add or update if Exclude is false
      if (aAttributeData.Exclude != true) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var aAttribute: Attribute
          // If the public id is not found then create a new attribute
          if (currAttribute == null) {
            aAttribute = new Attribute()
          } else {
            // If the public id is found then update the existing attribute
            aAttribute = bundle.add(currAttribute)
          }
          aAttribute.PublicID = aAttributeData.PublicID
          aAttribute.Name = aAttributeData.Name
          aAttribute.Description = aAttributeData.Description
          aAttribute.Type = aAttributeData.UserAttributeType
          aAttribute.Active = aAttributeData.Active
          aAttributeData.Skipped = false
          if (!aAttribute.New) {
            if (aAttribute.getChangedFields().Count > 0) {
              aAttributeData.Updated = true
            } else {
              aAttributeData.Unchanged = true
            }
          }
          bundle.commit()
        },User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Attribute Load: " + aAttributeData.Name + " " + e.toString())
      }
      aAttributeData.Error = true
    }
  }
}