package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.admindata.RegionData
uses java.util.Collection

class BCRegionExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return RegionData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getRegionEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(RegionData.CD), queryRegions(), \e -> getRowFromRegion(e))
  }

  // query data

  public static function queryRegions(): Collection <Region> {
    var result = Query.make(Region).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromRegion(region: Region): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(RegionData.COL_NAME), region.Name)
    colMap.put(colIndexString(RegionData.COL_PUBLIC_ID), region.PublicID)
    colMap.put(colIndexString(RegionData.COL_ZONES), region.Zones)
    colMap.put(colIndexString(RegionData.COL_EXCLUDE), "false")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getRegionColumnTypes(): HashMap {
    return getColumnTypes(RegionData.CD)
  }
}
