package tdic.util.dataloader.data.sampledata

uses java.util.Date
uses java.math.BigDecimal
uses tdic.util.dataloader.data.ApplicationData


class PaymentReversalData extends ApplicationData{

  construct() {

  }
  private var _reversalDate         : Date                   as ReversalDate
  private var _referencenumber      : String                 as ReferenceNumber
  private var _transactionnumber    : String                 as TransactionNumber
  private var _reversalReason       : PaymentReversalReason  as ReversalReason
  private var _amount               : BigDecimal             as Amount
  
}
