package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses java.lang.Integer
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.AuthorityLimitTypeData
uses java.lang.Exception

class AuthorityLimitTypeWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var firstRowDone = false
  var currRowNum: int
  var authorityLimitTypeArray = new ArrayList <AuthorityLimitTypeData>()
  override function parseSheet(sheet: Sheet): ArrayList <AuthorityLimitTypeData> {
    // Check that sheet exists         
    if (sheet == null)
    {
      LOG.warn("No Authority Limit Type Sheet")
      return null
    }
    LOG.info("Authority Limit Type Sheet: Parsing started")
    try
    {
      // Iterate through each row in the AuthorityLimit sheet 
      for (var row in sheet.rowIterator())
      {
        currRowNum = row.RowNum
        // Get the Auth Limit Types from the first row (it has headers)
        if (!firstRowDone)
        {
          // each cell beginning with column 5 (E) contains a limit type
          var x = 0 as Integer
          var cells = row.cellIterator()
          for (myCell in cells) {
            x = x + 1
            if (x >= 5) {
              var limit = myCell.toString().trim()
              if (limit != null and limit != "") {
                // add to auth limit type array
                var authorityLimitTypeData = new AuthorityLimitTypeData()
                authorityLimitTypeData.Column = x
                authorityLimitTypeData.LimitType = limit
                authorityLimitTypeArray.add(authorityLimitTypeData)
              }
            }
          }
          firstRowDone = true
          continue
        }
      }
      LOG.info("Authority Limit Type Sheet: Parsing complete")
      return authorityLimitTypeArray
    }
        catch (e: Exception)
        {
          throw "Error parsing Authority Limit Type sheet at line: " + currRowNum + " Exception : " + e.toString()
        }
  }
}
