package tdic.util.dataloader.export.planexport

uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.GLTransactionFilterData
uses java.util.HashMap
uses java.util.Collection
uses gw.api.database.Query

/**
 * US570 - General Ledger Integration
 * 11/17/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.  Export for Transaction filters.
 */
class BCGLTransactionFilterExport extends WorksheetExportUtil {

  /**
   * Standard constructor.
   */
  construct() {
  }
  
  /**
   * Gets the Excel sheet name for the GL Filters.
   * 
   * @return A String containing the Excel sheet name for the GL Filters
   */
  public static function getSheetName() : String {
    return GLTransactionFilterData.SHEET
  }
  
  /**
   * A method to fetch the entity values for the GL Filters.
   * 
   * @return A HashMap containing the header name and values of the entity
   */
  public static function getGLFilterEntityValues () : HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(GLTransactionFilterData.CD), queryGLFilters(), \e -> getRowFromGLFilter(e))
  }
  
  /**
   * Queries the existing GL Filters in the database.
   * 
   * @return A Collection<GLIntegrationTransactionFilter_TDIC> of the GL Filters retrieved from the database
   */
  public static function queryGLFilters() : Collection<GLIntegrationTransactionFilter_TDIC> {
    var result = Query.make(GLIntegrationTransactionFilter_TDIC).select()
    return result.toCollection()
  }
  
  /**
   * Method to build column map from entity.
   * 
   * @param glFilter The GLIntegrationTransactionFilter_TDIC with the values to map
   * @return A HashMap containing the column name mapped to the entity's values
   */
  public static function getRowFromGLFilter(glFilter : GLIntegrationTransactionFilter_TDIC) : HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(GLTransactionFilterData.COL_TRANS_TYPE), glFilter.TransactionType)
    return colMap
  }
  
  /**
   * Method to fetch the type of each column in the entity.  Used to create cell type during excel export.
   * 
   * @return A HashMap containing the type of each column in the entity
   */
  static function getGLFilterColumnTypes() : HashMap {
    return getColumnTypes(GLTransactionFilterData.CD)
  }

}
