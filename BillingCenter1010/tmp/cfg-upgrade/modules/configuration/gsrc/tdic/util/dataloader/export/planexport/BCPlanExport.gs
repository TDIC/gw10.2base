package tdic.util.dataloader.export.planexport

uses tdic.util.dataloader.export.ExcelExport
uses java.util.HashMap
uses gw.api.util.DisplayableException
uses tdic.util.dataloader.util.DataLoaderUtil
uses java.lang.Exception
uses org.slf4j.LoggerFactory

class BCPlanExport {
  public static final var LOG: org.slf4j.Logger = LoggerFactory.getLogger("DataLoader.Export")
  construct() {
  }

  // Method to export and download data

  public static function exportData() {
    exportDataToFile(DataLoaderUtil.getExportPlanDatafilePath(), true)
  }

  // Method to export data

  public static function exportDataToFile(filePath: String, download: boolean) {
    var excelExport = new ExcelExport(filePath)
    var sheetMap = retrieveData()
    try {
      excelExport.write(sheetMap)
      if (download) excelExport.download()
    } catch (e: Exception) {
      var msg = "BCPlanExport: Exception while exporting data: "
      LOG.error(msg, e)
      throw new DisplayableException(msg + e)
    }
  }

  // Method to retrieve data

  public static function retrieveData(): HashMap {
    LOG.info("BCPlanExport: Retrieving data started ***")
    var sheetMap = new HashMap()
    LOG.info("Exporting Plan Data")
    // Get BillingPlan Values
    var billPlanMap = new HashMap()
    var billPlanExport = new BCBillingPlanExport()
    billPlanMap.put(billPlanExport.getBillPlanEntityValues(), billPlanExport.getBillPlanColumnTypes())
    sheetMap.put(billPlanExport.getSheetName(), billPlanMap)
    // Get PaymentPlan Values
    var payPlanMap = new HashMap()
    var payPlanExport = new BCPaymentPlanExport()
    payPlanMap.put(payPlanExport.getPayPlanEntityValues(), payPlanExport.getPayPlanColumnTypes())
    sheetMap.put(payPlanExport.getSheetName(), payPlanMap)
    // Get Charge Pattern Values
    var chrgPattern = new HashMap()
    var chrgPatternExport = new BCChargePatternExport()
    chrgPattern.put(chrgPatternExport.getChrgPatternEntityValues(), chrgPatternExport.getChrgPatternColumnTypes())
    sheetMap.put(chrgPatternExport.getSheetName(), chrgPattern)
    // Get Delinquency Plan Values
    var dlnqPlan = new HashMap()
    var dlnqPlanExport = new BCDelinquencyPlanExport()
    dlnqPlan.put(dlnqPlanExport.getDelPlanEntityValues(), dlnqPlanExport.getDelPlanColumnTypes())
    sheetMap.put(dlnqPlanExport.getSheetName(), dlnqPlan)
    // Get Delinquency Workflow values
    var dlnqPlanWkfl = new HashMap()
    var dlnqPlanWkflExport = new BCDelPlanWorkflowExport()
    dlnqPlanWkfl.put(dlnqPlanWkflExport.getDelWorkflowEntityValues(), dlnqPlanWkflExport.getDelWorkflowColumnTypes())
    sheetMap.put(dlnqPlanWkflExport.getSheetName(), dlnqPlanWkfl)
    // Get Agency Bill Plan values
    var agencyBillPlan = new HashMap()
    var agencyBillPlanExport = new BCAgencyBillPlanExport()
    agencyBillPlan.put(agencyBillPlanExport.getAgcyBillPlanEntityValues(), agencyBillPlanExport.getAgcyBillPlanColumnTypes())
    sheetMap.put(agencyBillPlanExport.getSheetName(), agencyBillPlan)
    // Get payment plan overrides Values
    var payplanOverride = new HashMap()
    var payplanOverrideExport = new BCPaymentPlanOverrideExport()
    payplanOverride.put(payplanOverrideExport.getPayPlanOverrideEntityValues(), payplanOverrideExport.getPayPlanOverrideColumnTypes())
    sheetMap.put(payplanOverrideExport.getSheetName(), payplanOverride)
    // Get Commission plan values
    var commPlan = new HashMap()
    var commPlanExport = new BCCommissionPlanExport()
    commPlan.put(commPlanExport.getCommPlanEntityValues(), commPlanExport.getCommPlanColumnTypes())
    sheetMap.put(commPlanExport.getSheetName(), commPlan)
    // Get Commission subplan Values
    var commSubPlan = new HashMap()
    var commSubPlanExport = new BCCommissionSubPlanExport()
    commSubPlan.put(commSubPlanExport.getCommSubPlanEntityValues(), commSubPlanExport.getCommSubPlanColumnTypes())
    sheetMap.put(commSubPlanExport.getSheetName(), commSubPlan)
    // Get PaymentAllocationPlan Values
    var payAllocationPlanMap = new HashMap()
    var payAllocationPlanExport = new BCPaymentAllocationPlanExport()
    payAllocationPlanMap.put(payAllocationPlanExport.getPaymentAllocationPlanEntityValues(), payAllocationPlanExport.getPaymentAllocationPlanColumnTypes())
    sheetMap.put(payAllocationPlanExport.getSheetName(), payAllocationPlanMap)
    // Get ReturnPremiumPlan Values
    var returnPremiumPlanMap = new HashMap()
    var returnPremiumPlanExport = new BCReturnPremiumPlanExport()
    returnPremiumPlanMap.put(returnPremiumPlanExport.getReturnPremiumPlanEntityValues(), returnPremiumPlanExport.getReturnPremiumPlanColumnTypes())
    sheetMap.put(returnPremiumPlanExport.getSheetName(), returnPremiumPlanMap)
    // Get ReturnPremiumScheme Values
    var returnPremiumSchemeMap = new HashMap()
    var returnPremiumSchemeExport = new BCReturnPremiumSchemeExport()
    returnPremiumSchemeMap.put(returnPremiumSchemeExport.getReturnPremiumSchemeEntityValues(), returnPremiumSchemeExport.getReturnPremiumSchemeColumnTypes())
    sheetMap.put(returnPremiumSchemeExport.getSheetName(), returnPremiumSchemeMap)
    /**
     * US570 - Excel Data Loader Additions for custom GL Integration entities
     * 11/17/2014 Alvin Lee
     */
    // Get GL Filter Values
    var glFilterMap = new HashMap()
    var glFilterExport = new BCGLTransactionFilterExport()
    glFilterMap.put(glFilterExport.getGLFilterEntityValues(), glFilterExport.getGLFilterColumnTypes())
    sheetMap.put(glFilterExport.getSheetName(),glFilterMap)
    // Get GL Mapping Values
    var glTransactionMappingMap = new HashMap()
    var glMappingExport = new BCGLTransactionNameMappingExport()
    glTransactionMappingMap.put(glMappingExport.getGLMappingEntityValues(), glMappingExport.getGLMappingColumnTypes())
    sheetMap.put(glMappingExport.getSheetName(),glTransactionMappingMap)
    // Get GL T-Account Name Mapping Values
    var glTAccountMappingMap = new HashMap()
    var glTAccountMappingExport = new BCGLTAccountNameMappingExport()
    glTAccountMappingMap.put(glTAccountMappingExport.getGLTAccountNameMappingEntityValues(), glTAccountMappingExport.getGLTAccountNameMappingColumnTypes())
    sheetMap.put(glTAccountMappingExport.getSheetName(),glTAccountMappingMap)

    LOG.info("BCPlanExport: Retrieving data completed ***")
    return sheetMap
  }
}
