package tdic.util.dataloader.export.planexport

uses gw.api.database.Query

uses java.util.Collection
uses java.util.HashMap
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.ReturnPremiumPlanData

class BCReturnPremiumPlanExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return ReturnPremiumPlanData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getReturnPremiumPlanEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(ReturnPremiumPlanData.CD), queryReturnPremiumPlans(), \e -> getRowFromReturnPremiumPlan(e))
  }

  // query data

  public static function queryReturnPremiumPlans(): Collection <ReturnPremiumPlan> {
    var result = Query.make(ReturnPremiumPlan).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromReturnPremiumPlan(plan: ReturnPremiumPlan): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(ReturnPremiumPlanData.COL_PUBLIC_ID), plan.PublicID)
    colMap.put(colIndexString(ReturnPremiumPlanData.COL_NAME), plan.Name)
    colMap.put(colIndexString(ReturnPremiumPlanData.COL_DESCRIPTION), plan.Description)
    colMap.put(colIndexString(ReturnPremiumPlanData.COL_EFFECTIVE_DATE), plan.EffectiveDate.AsUIStyle)
    colMap.put(colIndexString(ReturnPremiumPlanData.COL_EXPIRATION_DATE), plan.ExpirationDate.AsUIStyle)
    colMap.put(colIndexString(ReturnPremiumPlanData.COL_QUALIFIER), plan.ChargeQualification)
    colMap.put(colIndexString(ReturnPremiumPlanData.COL_LISTBILL_EXCESS), plan.ListBillAccountExcessTreatment)

    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getReturnPremiumPlanColumnTypes(): HashMap {
    return getColumnTypes(ReturnPremiumPlanData.CD)
  }
}
