package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses java.math.BigDecimal
uses tdic.util.dataloader.data.plandata.AgencyBillPlanData
uses gw.api.upgrade.Coercions

class AgencyBillPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseAgencyBillPlanRow(row: Row): AgencyBillPlanData {
    var result = new AgencyBillPlanData()
    // Populate AgencyBillPlan Data
    result.PublicID = convertCellToString(row.getCell(AgencyBillPlanData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(AgencyBillPlanData.COL_NAME))
    result.WorkflowType = convertCellToString(row.getCell(AgencyBillPlanData.COL_WorkflowType))
    result.EffectiveDate = convertCellToDate(row.getCell(AgencyBillPlanData.COL_EFFECTIVE_DATE))
    result.ExpirationDate = convertCellToDate(row.getCell(AgencyBillPlanData.COL_EXPIRATION_DATE))
    result.CycleCloseDayOfMonthLogic = convertCellToString(row.getCell(AgencyBillPlanData.COL_CycleCloseDayOfMonthLogic))
    result.CycleCloseDayOfMonth = Coercions.makeIntFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_CycleCloseDayOfMonth)))
    result.PaymentTermsInDays = Coercions.makeIntFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_PaymentTermsInDays)))
    result.ExceptionForPastDueStatement = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_ExceptionForPastDueStatement)))
    result.StatementSentAfterCycleClose = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_StatementSentAfterCycleClose)))
    result.DaysAfterCycleCloseToSendStmnt = Coercions.makeIntFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_DaysAfterCycleCloseToSendStmnt)))
    result.SnapshotNonPastDueItems = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_SnapshotNonPastDueItems)))
    result.StatementsWithLowNetSuppressed = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_StatementsWithLowNetSuppressed)))
    result.NetThresholdForSuppressingStmt = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_NetThresholdForSuppressingStmt)))
    result.ReminderSentIfPromiseNotRcvd = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_ReminderSentIfPromiseNotRcvd)))
    result.DaysUntilPromiseReminderSent = Coercions.makeIntFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_DaysUntilPromiseReminderSent)))
    result.PromiseDueInDays = Coercions.makeIntFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_PromiseDueInDays)))
    result.ExceptionIfPromiseNotReceived = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_ExceptionIfPromiseNotReceived)))
    result.PromiseExceptionsSent = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_PromiseExceptionsSent)))
    result.AutoProcessWhenPaymentMatches = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_AutoProcessWhenPaymentMatches)))
    result.PaymentExceptionsSent = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_PaymentExceptionsSent)))
    result.FirstDunningSentIfNotPaid = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_FirstDunningSentIfNotPaid)))
    result.DaysUntilFirstDunningSent = Coercions.makeIntFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_DaysUntilFirstDunningSent)))
    result.SecondDunningSentIfNotPaid = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_SecondDunningSentIfNotPaid)))
    result.DaysUntilSecondDunningSent = Coercions.makeIntFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_DaysUntilSecondDunningSent)))
    result.ProducerWriteoffThreshold = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_ProducerWriteoffThreshold)))
    result.LowCommissionCleared = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_LowCommissionCleared)))
    result.ClearCommissionThreshold = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_ClearCommissionThreshold)))
    result.LowGrossCleared = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_LowGrossCleared)))
    result.ClearGrossThreshold = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_ClearGrossThreshold)))
    result.ClearingLogicTarget = convertCellToString(row.getCell(AgencyBillPlanData.COL_ClearingLogicTarget))
    result.CreateOffsetsOnBilledInvoices = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_CreateOffsetsOnBilledInvoices)))
    result.PmntSchdChngOffsetsOnBilled = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AgencyBillPlanData.COL_PmntSchdChngOffsetsOnBilled)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <AgencyBillPlanData> {
    return parseSheet(sheet, \r -> parseAgencyBillPlanRow(r), true)
  }
}
