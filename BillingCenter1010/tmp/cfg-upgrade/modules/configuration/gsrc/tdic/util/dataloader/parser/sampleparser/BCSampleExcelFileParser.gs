package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.processor.DataLoaderProcessor
uses tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
uses tdic.util.dataloader.parser.ExcelFileParser
uses java.io.FileInputStream
uses org.apache.poi.ss.usermodel. *

uses java.lang.Exception

class BCSampleExcelFileParser extends ExcelFileParser {
  construct() {
  }

 public static function importSampleDataFile(filePath: String, dataLoaderHelper: DataLoaderProcessor): DataLoaderProcessor {
    try
    {
      var fis = new FileInputStream(filePath)
      var wb = WorkbookFactory.create(fis)
      var invoiceStreamSheet = getSheet(wb, "InvoiceStream")
      var policySheet = getSheet(wb, "Policies")
      var accountSheet = getSheet(wb, "Accounts")
      var producerSheet = getSheet(wb, "Producers")
      var issuanceSheet = getSheet(wb, "Issuances")
      var newrenewalSheet = getSheet(wb, "NewRenewal")
      var changeSheet = getSheet(wb, "Changes")
      var cancellationSheet = getSheet(wb, "Cancellations")
      var renewalSheet = getSheet(wb, "Renewals")
      var directPaySheet = getSheet(wb, "DirectPay")
      var rewriteSheet = getSheet(wb, "Rewrites")
      var reinstatementSheet = getSheet(wb, "Reinstatements")
      var auditSheet = getSheet(wb, "Audit")
      var agencyBillPaymentSheet = getSheet(wb, "AgencyBillPay")
      var batchSheet = getSheet(wb, "Batch")
      var paymentReversalSheet = getSheet(wb, "PaymentReversal")
      var bcDataLoaderHelper = dataLoaderHelper as BCSampleDataLoaderProcessor
      /* Parse Producers */
      replaceDataWithParsedArray(bcDataLoaderHelper.ProducerArray, new ProducerWorksheetParser().parseSheet(producerSheet))
      /* Parse Invoice Streams */
      replaceDataWithParsedArray(bcDataLoaderHelper.InvoiceStreamArray, new InvoiceStreamWorksheetParser().parseSheet(invoiceStreamSheet))
      /* Parse Accounts */
      replaceDataWithParsedArray(bcDataLoaderHelper.AccountArray, new AccountWorksheetParser().parseSheet(accountSheet))
      /* Parse Policies */
      replaceDataWithParsedArray(bcDataLoaderHelper.PolicyArray, new PolicyWorksheetParser().parseSheet(policySheet))
      /* Parse Policy Issuances */
      replaceDataWithParsedArray(bcDataLoaderHelper.IssuanceArray, new IssuanceWorksheetParser().parseSheetandAddPolicyPeriod(issuanceSheet, bcDataLoaderHelper.PolicyArray))
      /* Parse Policy New Renewals */
      replaceDataWithParsedArray(bcDataLoaderHelper.NewRenewalArray, new NewRenewalWorksheetParser().parseSheetandAddPolicyPeriod(newrenewalSheet, bcDataLoaderHelper.PolicyArray))
      /* Parse Policy Changes */
      replaceDataWithParsedArray(bcDataLoaderHelper.PolicyChangeArray, new ChangeWorksheetParser().parseSheet(changeSheet))
      /* Parse Policy Renewals */
      replaceDataWithParsedArray(bcDataLoaderHelper.RenewalArray, new RenewalWorksheetParser().parseSheet(renewalSheet))
      /* Parse Policy Cancellations */
      replaceDataWithParsedArray(bcDataLoaderHelper.CancellationArray, new CancellationWorksheetParser().parseSheet(cancellationSheet))
      /* Parse Direct Payments */
      replaceDataWithParsedArray(bcDataLoaderHelper.DirectPaymentArray, new DirectPayWorksheetParser().parseSheet(directPaySheet))
      /* Parse Policy Rewrites */
      replaceDataWithParsedArray(bcDataLoaderHelper.RewriteArray, new RewriteWorksheetParser().parseSheetandAddPolicyPeriod(rewriteSheet, bcDataLoaderHelper.PolicyArray))
      /* Parse Policy Reinstatements */
      replaceDataWithParsedArray(bcDataLoaderHelper.ReinstatementArray, new ReinstatementWorksheetParser().parseSheet(reinstatementSheet))
      /* Parse Audits */
      replaceDataWithParsedArray(bcDataLoaderHelper.AuditArray, new AuditWorksheetParser().parseSheet(auditSheet))
      /* Parse Agency Bill Payments */
      replaceDataWithParsedArray(bcDataLoaderHelper.AgencyBillPaymentArray, new AgencyBillPayWorksheetParser().parseSheet(agencyBillPaymentSheet))
      /* Parse Batches */
      replaceDataWithParsedArray(bcDataLoaderHelper.BatchArray, new BatchWorksheetParser().parseSheet(batchSheet))
      /* Parse Payment Reversals */
      replaceDataWithParsedArray(bcDataLoaderHelper.PaymentReversalArray, new PaymentReversalWorksheetParser().parseSheet(paymentReversalSheet))
    }
        catch (e: Exception)
        {
          throw "Error occurred while importing the file and parsing : " + e.toString()
        }
    return dataLoaderHelper
  }
}
