package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.AddressData

class ContactData extends ApplicationData{

  construct() {

  }
  private var _contacttype                   : typekey.Contact  as ContactType
  private var _name                          : String           as Name
  private var _firstname                     : String           as FirstName
  private var _lastname                      : String           as LastName
  private var _primaryaddress                : AddressData      as PrimaryAddress
  private var _workphone                     : String           as WorkPhone
  private var _faxphone                      : String           as FaxPhone
  private var _email                         : String           as Email
}
