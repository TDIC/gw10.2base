package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.plandata.CommissionPlanData
uses gw.api.upgrade.Coercions

class CommissionPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseCommissionPlanRow(row: Row): CommissionPlanData {
    var result = new CommissionPlanData()
    // Populate Commission Plan Data
    result.PublicID = convertCellToString(row.getCell(CommissionPlanData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(CommissionPlanData.COL_NAME))
    result.EffectiveDate = convertCellToDate(row.getCell(CommissionPlanData.COL_EFFECTIVE_DATE))
    result.ExpirationDate = convertCellToDate(row.getCell(CommissionPlanData.COL_EXPIRATION_DATE))
    result.AllowedTiers_Gold = Coercions.makeBooleanFrom(convertCellToString(row.getCell(CommissionPlanData.COL_ALLOWED_TIERS_GOLD)))
    result.AllowedTiers_Silver = Coercions.makeBooleanFrom(convertCellToString(row.getCell(CommissionPlanData.COL_ALLOWED_TIERS_SILVER)))
    result.AllowedTiers_Bronze = Coercions.makeBooleanFrom(convertCellToString(row.getCell(CommissionPlanData.COL_ALLOWED_TIERS_BRONZE)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <CommissionPlanData> {
    return parseSheet(sheet, \r -> parseCommissionPlanRow(r), true)
  }
}