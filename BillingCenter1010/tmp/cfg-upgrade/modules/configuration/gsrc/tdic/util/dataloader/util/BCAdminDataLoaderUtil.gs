package tdic.util.dataloader.util

uses gw.api.database.Query
uses tdic.util.dataloader.data.admindata.ActivityPatternData
uses gw.api.database.Relop

class BCAdminDataLoaderUtil extends DataLoaderUtil {
  construct() {
  }

  public static function findActivityPatternByCode(code: String): ActivityPattern {
    return gw.api.database.Query.make(entity.ActivityPattern).compare("Code", Relop.Equals, code).select().getAtMostOneRow()
  }

  public static function findRegionByName(name: String): Region {
    return gw.api.database.Query.make(entity.Region).compare("Name", Relop.Equals, name).select().getAtMostOneRow()
  }

  public static function findRegionByPublicID(publicID: String): Region {
    return gw.api.database.Query.make(entity.Region).compare("PublicID", Relop.Equals, publicID).select().getAtMostOneRow()
  }

  public static function findRoleByName(name: String): Role {
    return gw.api.database.Query.make(entity.Role).compare("Name", Relop.Equals, name).select().getAtMostOneRow()
  }

  public static function findRolePrivilegeByPublicID(publicID: String): RolePrivilege {
    return gw.api.database.Query.make(entity.RolePrivilege).compare("PublicID", Relop.Equals, publicID).select().getAtMostOneRow()
  }

  public static function findAuthorityLimitByPublicID(publicID: String): AuthorityLimit {
    return gw.api.database.Query.make(entity.AuthorityLimit).compare("PublicID", Relop.Equals, publicID).select().getAtMostOneRow()
  }

  public static function findAuthorityLimitProfileByName(name: String): AuthorityLimitProfile {
    return gw.api.database.Query.make(entity.AuthorityLimitProfile).compare("PublicID", Relop.Equals, name).select().getAtMostOneRow()
  }

  public static function findCustomAuthorityLimitProfileByPublicID(publicID: String): AuthorityLimitProfile {
    var query = new Query(AuthorityLimitProfile)
    query.compare("PublicID", Equals, publicID)
    query.compare("Name", Equals, "Custom")
    query.withFindRetired(true)
    var result = query.select().getAtMostOneRow() as AuthorityLimitProfile
    return result
  }

  public static function findUserByPublicID(publicID: String): User {
    var x = Query.make(User).compare("PublicID", Equals, publicID).select().FirstResult
    return x
  }

  public static function findUserGroup(userID: User, groupID: Group): GroupUser {
    return gw.api.database.Query.make(entity.GroupUser).and(\r -> {
      r.compare("User", Relop.Equals, userID)
      r.compare("Group", Relop.Equals, groupID)
    }).select().getAtMostOneRow()
  }

  public static function findGroupByUserName(groupName: String): Group {
    return gw.api.database.Query.make(entity.Group).compare("Name", Relop.Equals, groupName).select().getAtMostOneRow()
  }

  public static function findAttributeByName(name: String): Attribute {
    return gw.api.database.Query.make(entity.Attribute).compare("Name", Relop.Equals, name).select().getAtMostOneRow()
  }

  public static function findAttributeByPublicId(publicId: String): Attribute {
    return gw.api.database.Query.make(entity.Attribute).compare("PublicID", Relop.Equals, publicId).select().getAtMostOneRow()
  }

  public static function findQueueByPublicID(publicID: String): AssignableQueue {
    return gw.api.database.Query.make(entity.AssignableQueue).compare("PublicID", Relop.Equals, publicID).select().getAtMostOneRow()
  }

  public static function findSecurityZoneByPublicID(publicID: String): SecurityZone {
    return gw.api.database.Query.make(entity.SecurityZone).compare("PublicID", Relop.Equals, publicID).select().getAtMostOneRow()
  }

  public static function findGroupByPublicID(publicID: String): Group {
    return gw.api.database.Query.make(entity.Group).compare("PublicID", Relop.Equals, publicID).select().getAtMostOneRow()
  }

  public static function findQueueByDisplayName(displayName: String): AssignableQueue {
    var sep = " - "
    // assuming queue's display name is "<queueName> - <groupName>"
    var i = displayName.indexOf(sep)
    var queueName = displayName.substring(0, i)
    var groupName = displayName.substring(i + sep.length)
    var query = new Query(AssignableQueue)
    query.compare("Name", Equals, queueName)
    var joinGroup = query.join("Group")
    joinGroup.compare("Name", Equals, groupName)
    return query.select().AtMostOneRow as AssignableQueue
  }

  public static function findHolidayByPublicID(publicID: String): Holiday {
    return gw.api.database.Query.make(entity.Holiday).compare("PublicID", Relop.Equals, publicID).select().getAtMostOneRow()
  }

  public static function printTypelistData() {
    printActivityCategory()
    printState()
    printAddressType()
    printCountry()
    printLanguageType()
    printTimeZoneType()
    printMaritalStatus()
    printGroupType()
    printYesNo()
    printUserAttributeType()
    printActivityPriority()
    printGender()
    printContactPhoneType()
    printRole()
    printGroup()
    printSecurityZone()
    printActivityPattern()
    printAuthorityLimitProfile()
  }

  public static function printAdminData() {
    printRole()
    printGroup()
    printSecurityZone()
    printActivityPattern()
    printAuthorityLimitProfile()
    printQueues()
    printRoleData()
    printRoleNameAndPermissions()
    printPermissions()
    printUser()
    printUserGroup()
    printAuthorityLimit()
    printUserAuthorityLimit()

  }
  public static function printActivityCategory() {
    print("Activity Category")
    for (y in ActivityCategory.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printState() {
    print("State")
    for (y in State.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printAddressType() {
    print("AddressType")
    for (y in AddressType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printCountry() {
    print("Country")
    for (y in Country.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printLanguageType() {
    print("LanguageType")
    for (y in LanguageType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printTimeZoneType() {
    print("TimeZoneType")
    for (y in TimeZoneType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printMaritalStatus() {
    print("MaritalStatus")
    for (y in MaritalStatus.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printGroupType() {
    print("GroupType")
    for (y in GroupType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printYesNo() {
    print("YesNo")
    for (y in YesNo.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printUserAttributeType() {
    print("UserAttributeType")
    for (y in UserAttributeType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printActivityPriority() {
    print("ActivityPriority")
    for (y in Priority.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printGender() {
    print("Gender")
    for (y in GenderType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printContactPhoneType() {
    print("ContactPhoneType")
    for (y in PrimaryPhoneType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printRole() {
    var query = gw.api.database.Query.make(entity.Role).select()
    print("Roles")
    for (y in query) {
      print(y.PublicID + "\t" + y.Name + "\t" + y.Description + "\t" + "TRUE")
    }
    print("")
  }

  public static function printGroup() {
    var query = gw.api.database.Query.make(entity.Group).select()
    print("Groups")
    for (y in query) {
      print(y.Name + "\t" + y.PublicID + "\t" + y.Parent + "\t" + y.Supervisor + "\t" + y.GroupType.Code + "\t" + "TRUE" + "\t" +
          y.LoadFactor + "\t" + y.SecurityZone)
    }
    print("")
  }

  public static function printSecurityZone() {
    var query = gw.api.database.Query.make(entity.SecurityZone).select()
    print("Security Zones")
    for (y in query) {
      print(y.Name + "\t" + y.PublicID + "\t" + y.Description + "\t" + "TRUE")
    }
    print("")
  }

  public static function printActivityPattern() {
    var query = gw.api.database.Query.make(entity.ActivityPattern).select()
    print("Activity Patterns")
    for (y in query) {
      var queue = ActivityPatternData.getAssignableQueue(y)
      var queueDN = (queue != null) ? queue.DisplayName : ""
      print(y.Code + "\t" + y.PublicID + "\t" + "TRUE " + "\t" + y.Subject + "\t" + y.Description + "\t" + y.ActivityClass + "\t" +
          y.Type + "\t" + y.Category + "\t" + y.Mandatory + "\t" + y.Priority + "\t" + y.Recurring + "\t" + y.TargetDays + "\t" +
          y.TargetHours + "\t" + y.TargetIncludeDays + "\t" + y.TargetStartPoint + "\t" + y.EscalationDays + "\t" +
          y.EscalationHours + "\t" + y.EscalationInclDays + "\t" + y.EscalationStartPt + "\t" + y.AutomatedOnly + "\t" + y.Command + "\t" + queueDN)
    }
    print("")
  }

  public static function printAuthorityLimitProfile() {
    var query = gw.api.database.Query.make(entity.AuthorityLimitProfile).select()
    print("Authority Limit Profiles")
    for (y in query.where(\a -> a.Custom == false)) {
      print(y.Name + "\t" + y.PublicID + "\t" + blankIfNull(y.Description) + "\t" + " " + "\t" + "TRUE")
    }
    print("")
  }

  public static function printQueues() {
    var query = gw.api.database.Query.make(entity.AssignableQueue).select()
    print("Queues")
    for (y in query) {
      print(y.DisplayName + "\t" + y.PublicID + "\t" + y.Name + "\t" + y.Group.Name + "\t" + y.Description + "\t" + y.SubGroupVisible + "\t" + "TRUE")
    }
    print("")
  }

  public static function printRoleData() {
    var query = gw.api.database.Query.make(entity.RolePrivilege).select()
    print("RoleData")
    for (y in query) {
      print(y.Role.PublicID + "\t" + y.Permission.Code + "\t" + y.Permission.Description)
    }
    print("")
  }

  public static function printRoleNameAndPermissions() {
    var query = gw.api.database.Query.make(entity.RolePrivilege).select()
    print("RoleData")
    for (y in query) {
      print(y.Role.Name + "\t" + y.Permission.Code + "\t" + y.Permission.Description)
    }
    print("")
  }

  public static function printPermissions() {
    print("PermissionNotes")
    var permissions = typekey.SystemPermissionType.getTypeKeys(false).copy()
    permissions = permissions.sortBy(\s -> s.Code)
    for (y in permissions) {
      print(y.Code + "\t" + y.Description)
    }
    print("")
  }

  public static function printUser() {
    var query = gw.api.database.Query.make(entity.User).select()
    print("Users")
    for (y in query) {
      var roleString = ""
      var roleArray = y.AllRoles.toTypedArray()
      for (i in {0, 1, 2}) {
        if (i > 0) roleString += "\t"
        if (i < roleArray.Count) roleString += roleArray[i].Name
      }
      var regionString = ""
      var regionArray = y.Regions
      for (i in {0, 1, 2}) {
        if (i > 0) regionString += "\t"
        if (i < regionArray.Count) regionString += regionArray[i].Region.Name
      }
      print("TRUE" + "\t" + y.Credential.Active + "\t" + y.PublicID + "\t"
          + blankIfNull(y.Contact.LastName) + "\t" + blankIfNull(y.Contact.FirstName) + "\t"
          + blankIfNull(y.Contact.MiddleName) + "\t" + blankIfNull(y.Contact.EmployeeNumber) + "\t"
          + y.Credential.UserName + "\t\t" + blankIfNull(y.JobTitle) + "\t"
          + blankIfNull(y.Department) + "\t" + blankIfNull(y.Contact.Gender) + "\t"
          + blankIfNull(y.Contact.HomePhone) + "\t" + blankIfNull(y.Contact.WorkPhone) + "\t"
          + blankIfNull(y.Contact.CellPhone) + "\t" + blankIfNull(y.Contact.FaxPhone) + "\t"
          + blankIfNull(y.Contact.PrimaryPhone) + "\t" + y.Contact.PrimaryAddress + "\t"
          + firstOrBlank(y.Contact.SecondaryAddresses) + "\t" + blankIfNull(y.Contact.FormerName) + "\t"
          + blankIfNull(y.Contact.EmailAddress1) + "\t" + blankIfNull(y.Contact.EmailAddress2) + "\t"
          + blankIfNull(y.Contact.DateOfBirth) + "\t" + y.ExternalUser + "\t"
          + roleString + "\t" + regionString + "\t" + blankIfNull(y.Language))
    }
    print("")
  }

  public static function printUserGroup() {
    var userQuery = gw.api.database.Query.make(entity.User).select()
    var groupQuery = gw.api.database.Query.make(entity.Group).select()
    print("UserGroups")
    var columnHeaders = ""
    for (z in groupQuery) {
      columnHeaders += z.Name + "\t"
    }
    columnHeaders += "Check User public-ID"
    print(columnHeaders)
    for (y in userQuery) {
      var rowString = ""
      for (z in groupQuery) {
        var gu = y.GroupUsers.firstWhere(\g -> g.Group.PublicID == z.PublicID)
        if (gu != null) {
          rowString += nullValue(gu.LoadFactor, "100")
          // assume 100% usability if nothing defined
          if (gu.Manager and gu.LoadFactorType == TC_LOADFACTORADMIN) {
            rowString += ":B"
          } else if (gu.Manager) {
            rowString += ":M"
          } else if (gu.LoadFactorType == TC_LOADFACTORADMIN) {
            rowString += ":A"
          }
        }
        rowString += "\t"
      }
      rowString += "(" + y.PublicID + ")"
      print(rowString)
    }
    print("")
  }

  public static function printAuthorityLimit() {
    var query = gw.api.database.Query.make(entity.AuthorityLimitProfile).select()
    print("Authority Limits")
    for (y in query.where(\a -> a.Custom == false)) {
      for (z in y.Limits) {
        print(y.Name + "\t" + z.PublicID + "\t" + "TRUE" + "\t" + z.LimitType + "\t" + blankIfZero(z.LimitAmount))
      }
    }
    print("")
  }

  public static function printUserAuthorityLimit() {
    var userQuery = gw.api.database.Query.make(entity.User).select()
    var limitTypes = AuthorityLimitType.getTypeKeys(false).copy().sortBy(\a -> a.Code)
    print("UserAuthorityLimits")
    for (y in userQuery) {
      var rowString = ""
      var profile = y.AuthorityProfile
      if (profile.Custom) {
        rowString += "\t" + y.Credential.UserName
        for (z in limitTypes) {
          var limit = profile.Limits.firstWhere(\a -> a.LimitType == z)
          rowString += "\t" + blankIfZero(limit.LimitAmount)
        }
      } else {
        rowString += blankIfNull(y.AuthorityProfile) + "\t" + y.Credential.UserName
      }
      print(rowString)
    }
    print("")
  }

  static function nullValue(value: Object, ifnull: String): String {
    var result = ifnull
    if (value != null) result = value.toString()
    return result
  }

  static function blankIfNull(value: Object): String {
    return nullValue(value, "")
  }

  static function blankIfZero(value: java.math.BigDecimal): String {
    var result = ""
    if (value != 0) result = value.toString()
    return result
  }

  static function firstOrBlank(value: Object[]): String {
    var result = ""
    if (value != null and value.Count > 0) result = value[0].toString()
    return result
  }
}
