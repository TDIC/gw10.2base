package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.SecurityZoneData
uses gw.api.upgrade.Coercions

class SecurityZoneWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseSecurityZoneRow(row: Row): SecurityZoneData {
    var result = new SecurityZoneData()
    // Populate SecurityZone Data
    result.Name = convertCellToString(row.getCell(SecurityZoneData.COL_NAME))
    result.PublicID = convertCellToString(row.getCell(SecurityZoneData.COL_PUBLIC_ID))
    result.Description = convertCellToString(row.getCell(SecurityZoneData.COL_DESCRIPTION))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(SecurityZoneData.COL_EXCLUDE)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <SecurityZoneData> {
    return super.parseSheet(sheet, \r -> parseSecurityZoneRow(r), true)
  }
}
