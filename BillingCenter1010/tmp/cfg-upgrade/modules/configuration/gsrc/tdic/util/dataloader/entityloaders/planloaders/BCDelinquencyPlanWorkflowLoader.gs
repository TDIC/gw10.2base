package tdic.util.dataloader.entityloaders.planloaders

//uses entity.DelinquencyPlanWorkflow

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData
uses tdic.util.dataloader.util.BCPlanDataLoaderUtil
uses java.lang.Exception

class BCDelinquencyPlanWorkflowLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aDelinquencyPlanWorkflowData = applicationData as DelinquencyPlanWorkflowData
    try {
      // get Del Plan; don't add workflow if no plan exists
      var aDelinquencyPlan = BCPlanDataLoaderUtil.findDelinquencyPlanByName(aDelinquencyPlanWorkflowData.DelinquencyPlanName)
      if (aDelinquencyPlan != null) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          // get existing Del Plan Reason or add new if it does not exist
          var aDelPlanReason = aDelinquencyPlan.DelinquencyPlanReasons.firstWhere(\d ->
              d.DelinquencyReason == aDelinquencyPlanWorkflowData.DelinquencyReason)
          if (aDelPlanReason == null) {
            // create new
            // new Del Plan Reason
            aDelPlanReason = new DelinquencyPlanReason()
            aDelinquencyPlan.addToDelinquencyPlanReasons(aDelPlanReason)
          } else {
            // update existing
            aDelPlanReason = bundle.add(aDelPlanReason)
          }
          aDelPlanReason.DelinquencyPlan = aDelinquencyPlan
          aDelPlanReason.DelinquencyReason = aDelinquencyPlanWorkflowData.DelinquencyReason
          aDelPlanReason.WorkflowType = typekey.Workflow.get(aDelinquencyPlanWorkflowData.WorkflowType)
          // add new Del Plan Event if it does not exist
          var aDelinquencyPlanEvent = aDelPlanReason.PlanEvents.firstWhere(\d -> d.EventName == aDelinquencyPlanWorkflowData.EventName)
          if (aDelinquencyPlanEvent == null) {
            // create new
            aDelinquencyPlanEvent = new DelinquencyPlanEvent()
            aDelinquencyPlanEvent.DelinquencyPlanReason = aDelPlanReason
          } else {
            // update existing
            aDelinquencyPlanEvent = bundle.add(aDelinquencyPlanEvent)
          }
          aDelinquencyPlanEvent.EventName = aDelinquencyPlanWorkflowData.EventName
          aDelinquencyPlanEvent.TriggerBasis = aDelinquencyPlanWorkflowData.EventTrigger
          aDelinquencyPlanEvent.OffsetDays = aDelinquencyPlanWorkflowData.EventOffset
          aDelinquencyPlanEvent.RelativeOrder = aDelinquencyPlanWorkflowData.EventRelativeOrder
          aDelinquencyPlanEvent.Automatic = aDelinquencyPlanWorkflowData.EventAutomatic
          aDelinquencyPlanWorkflowData.Skipped = false
          if (!aDelPlanReason.New or !aDelinquencyPlanEvent.New) {
            // update case
            if (aDelPlanReason.New or aDelPlanReason.getChangedFields().Count > 0 or
                aDelinquencyPlanEvent.New or aDelinquencyPlanEvent.getChangedFields().Count > 0) {
              aDelinquencyPlanWorkflowData.Updated = true
            } else {
              // none is new and none has changed data
              aDelinquencyPlanWorkflowData.Unchanged = true
            }
          }
          bundle.commit()
        })
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("DelinquencyPlanWorkflow Load: " + aDelinquencyPlanWorkflowData.DelinquencyPlanName
            + " " + aDelinquencyPlanWorkflowData.DelinquencyReason + e.toString())
      }
      aDelinquencyPlanWorkflowData.Error = true
    }
  }
}