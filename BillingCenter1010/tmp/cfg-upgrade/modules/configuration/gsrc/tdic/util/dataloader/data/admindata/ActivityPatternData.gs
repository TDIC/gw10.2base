package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses java.lang.Integer
uses gw.lang.reflect.ReflectUtil
uses tdic.util.dataloader.data.ColumnDescriptor

class ActivityPatternData extends ApplicationData {

  // constants
  public static final var SHEET : String = "ActivityPattern"
  public static final var COL_CODE : int = 0
  public static final var COL_PUBLIC_ID : int = 1
  public static final var COL_EXCLUDE : int = 2
  public static final var COL_SUBJECT : int = 3
  public static final var COL_DESCRIPTION : int = 4
  public static final var COL_ACTIVITY_CLASS : int = 5
  public static final var COL_TYPE : int = 6
  public static final var COL_CATEGORY :int = 7
  public static final var COL_MANDATORY : int = 8
  public static final var COL_PRIORITY : int = 9
  public static final var COL_RECURRING : int = 10
  public static final var COL_TARGET_DAYS : int = 11
  public static final var COL_TARGET_HOURS : int = 12
  public static final var COL_TARGET_INCLUDE_DAYS : int = 13
  public static final var COL_TARGET_START_POINT : int = 14
  public static final var COL_ESCALATION_DAYS: int = 15
  public static final var COL_ESCALATION_HOURS : int = 16
  public static final var COL_ESCALATION_INCL_DAYS : int = 17
  public static final var COL_ESCALATION_START_POINT : int = 18
  public static final var COL_AUTOMATED_ONLY : int = 19
  public static final var COL_COMMAND : int = 20
  public static final var COL_QUEUE_DISPLAYNAME : int = 21
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(0,"Code",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(1,"Public ID",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(2,"Exclude",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(3,"Subject",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(4,"Description",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(5,"Activity Class",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(6,"Type",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(7,"Category",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(8,"Mandatory",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(9,"Priority",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(10,"Recurring",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(11,"Target Days",ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(12,"Target Hours",ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(13,"Target Include Days",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(14,"Target Start Point",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(15,"Escallation Days",ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(16,"Escallation Hours",ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(17,"Escallation Include Days",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(18,"Escallation Start Point",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(19,"Automated Only",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(20,"Command",ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(21,"Queue",ColumnDescriptor.TYPE_STRING)
  }

  construct() {
  }
  
  // properties
  private var _code                       : String           as Code
  private var _subject                    : String           as Subject
  private var _description                : String           as Description
  private var _activityclass              : ActivityClass    as ActivityClass
  private var _type                       : ActivityType     as ActivityType
  private var _category                   : String           as Category
  private var _mandatory                  : Boolean          as Mandatory
  private var _priority                   : String           as Priority
  private var _recurring                  : Boolean          as Recurring
  private var _targetdays                 : Integer          as TargetDays
  private var _targethours                : Integer          as TargetHours
  private var _targetincludedays          : IncludeDaysType  as TargetIncludeDays
  private var _targetstartpoint           : StartPointType   as TargetStartPoint
  private var _escalationdays             : Integer          as EscalationDays
  private var _escalationhours            : Integer          as EscalationHours
  private var _escalationincldays         : IncludeDaysType  as EscalationInclDays
  private var _escalationstartpt          : StartPointType   as EscalationStartPt
  private var _automatedonly              : Boolean          as AutomatedOnly
  private var _queue                      : QueueData        as Queue
  private var _command                    : String           as Command

  // use reflection to avoid compile errors if activity pattern queue extension is not used
  public static var isQueueAssignable : boolean = (ReflectUtil.findProperty(ActivityPattern, "AssignableQueue_ext") != null)
  public static function getAssignableQueue(myActivityPattern : ActivityPattern) : AssignableQueue {
    var result : AssignableQueue = null
    if (isQueueAssignable) {
      result = ReflectUtil.getProperty(myActivityPattern, "AssignableQueue_ext") as AssignableQueue
    }
    return result
  }
  public static function setAssignableQueue(myActivityPattern : ActivityPattern, myAssignableQueue : AssignableQueue) {
    if (isQueueAssignable and myAssignableQueue != null) {
      ReflectUtil.setProperty(myActivityPattern, "AssignableQueue_ext", myAssignableQueue)
    }
  }
}
