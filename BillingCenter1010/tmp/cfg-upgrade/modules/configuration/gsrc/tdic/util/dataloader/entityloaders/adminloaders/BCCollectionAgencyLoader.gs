package tdic.util.dataloader.entityloaders.adminloaders

uses gw.api.database.Query
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.CollectionAgencyData
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface

class BCCollectionAgencyLoader extends BCLoader implements EntityLoaderInterface {

  construct() {}

  override function createEntity(applicationData: ApplicationData) {
    var aCollectionAgencyData = applicationData as CollectionAgencyData
    try {
      var currCollectionAgency = Query.make(CollectionAgency).compare(CollectionAgency#PublicID, Equals, aCollectionAgencyData.PublicID).select().AtMostOneRow
      // add or update if Exclude is false
      if (not aCollectionAgencyData.Exclude) {
        if (currCollectionAgency == null) {
           // add if not on file
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            var aCollectionAgency = new CollectionAgency()
            aCollectionAgency.PublicID = aCollectionAgencyData.PublicID
            aCollectionAgency.Name = aCollectionAgencyData.Name
          }, User.util.UnrestrictedUser)
          aCollectionAgencyData.Skipped = false
        } else {
          // Update the collection agency record if already existing in the system.
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            var aCollectionAgency = bundle.add(currCollectionAgency)
            aCollectionAgency.Name = aCollectionAgencyData.Name
            aCollectionAgencyData.Skipped = false
            if (aCollectionAgency.getChangedFields().Count > 0) {
              aCollectionAgencyData.Updated = true
            } else {
              aCollectionAgencyData.Unchanged = true
            }
            bundle.commit()
          }, User.util.UnrestrictedUser)
        }
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Collection Agency Load: " + aCollectionAgencyData.PublicID + e.toString())
      }
      aCollectionAgencyData.Error = true
    }
  }
}
