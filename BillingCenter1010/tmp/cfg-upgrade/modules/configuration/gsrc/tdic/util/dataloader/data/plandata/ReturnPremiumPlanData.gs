package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ColumnDescriptor

class ReturnPremiumPlanData extends PlanData {
  //constants
  public static final var SHEET: String = "ReturnPremiumPlan"
  public static final var COL_PUBLIC_ID: int = 0
  public static final var COL_NAME: int = 1
  public static final var COL_DESCRIPTION : int = 2
  public static final var COL_EFFECTIVE_DATE : int = 3
  public static final var COL_EXPIRATION_DATE : int = 4
  public static final var COL_QUALIFIER: int = 5
  public static final var COL_LISTBILL_EXCESS: int = 6
  public static final var CD: ColumnDescriptor[] = {
      new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_DESCRIPTION, "Description", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EFFECTIVE_DATE, "Effective Date", ColumnDescriptor.TYPE_DATE),
      new ColumnDescriptor(COL_EXPIRATION_DATE, "Expiration Date", ColumnDescriptor.TYPE_DATE),
      new ColumnDescriptor(COL_QUALIFIER, "Qualifier", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_LISTBILL_EXCESS, "ListBill Excess", ColumnDescriptor.TYPE_STRING)

  }
  construct() {
  }
  var _Qualifier           : ReturnPremiumChargeQualification as Qualifier
  var _ListBillExcess      : ListBillAccountExcess            as ListBillExcess

}