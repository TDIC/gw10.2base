package tdic.util.dataloader

uses java.util.HashMap
uses gw.api.database.Query
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory
uses java.lang.Exception

/**
 * User: RadhakrishnaS
 * Date: 7/20/16
 * Time: 2:32 PM
 */
class AddressBookUIDUpdater {
  private var _logger = LoggerFactory.getLogger("Batch.AddressBookUIDUpdater")
  private var uidMappings : HashMap<String ,String>

  private var succeeded : int as Succeeded
  private var failed : int as Failed
  private var notFound : int as NotFound

  construct(uidMap : HashMap<String ,String>) {
    this.uidMappings = uidMap

    succeeded = 0
    failed = 0
    notFound = 0
  }

  public function performUpdate() {
    var allContactsQuery = Query.make(Contact)
    allContactsQuery.compare("AddressBookUID", NotEquals, null)
    var allContacts = allContactsQuery.select()

    for(oldUID in uidMappings.keySet()) {
      var newUID = uidMappings.get(oldUID)
      var contactToUpdate = allContacts.firstWhere( \ ctt -> ctt.AddressBookUID == oldUID)
      if( contactToUpdate != null ) {
        _logger.info("Updating contact ${contactToUpdate} with AddressBookUID ${oldUID} to ${newUID}")
        if( this.updateContact(contactToUpdate, newUID) ) {
          _logger.info("Successfully updated contact ${contactToUpdate} from AddressBookUID ${oldUID} to ${newUID}")
          succeeded++
        }
        else {
          _logger.error("Failed to update contact ${contactToUpdate} from AddressBookUID ${oldUID} to ${newUID}")
          failed++
        }
      }
      else {
        _logger.warn("No contact found with AddressBookUID ${oldUID}")
        notFound++
      }
    }
  }

  private function updateContact(aContact : Contact, newAddressBookUID : String) : boolean {
    var isSuccess = true
    try {
      Transaction.runWithNewBundle(\bundle -> {
        aContact = bundle.add(aContact)
        aContact.AddressBookUID = newAddressBookUID
      }, "su")
    } catch(exc : Exception) {
      isSuccess = false
      exc.printStackTrace()
    }

    return isSuccess
  }
}