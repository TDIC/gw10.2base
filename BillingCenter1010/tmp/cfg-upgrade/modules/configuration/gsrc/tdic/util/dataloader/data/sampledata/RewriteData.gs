package tdic.util.dataloader.data.sampledata

uses java.math.BigDecimal
uses java.lang.Integer

class RewriteData extends NewPolicyData {

  construct() {

  }
  private var _priorpolicyperiod          : String                as     PriorPolicyPeriod
  private var _lookuppolicyperiod         : PolicyPeriodData      as     LookupPolicyPeriod
  private var _changepolicynumber         : Boolean               as     ChangePolicyNumber
  //Payment Plan Modifiers
  private var _matchplannedinstallments   : boolean               as     MatchPlannedInstallments
  private var _suppressdownpayment        : boolean               as     SuppressDownPayment
  private var _maxnuminstallments         : Integer               as     MaxNumberOfInstallmentsOverride
  private var _downpaymentoverride        : BigDecimal            as     DownPaymentOverridePercentage
  
}
