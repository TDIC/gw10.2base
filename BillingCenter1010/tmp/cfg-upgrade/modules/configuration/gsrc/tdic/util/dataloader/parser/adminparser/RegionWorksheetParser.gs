package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.RegionData
uses gw.api.upgrade.Coercions

class RegionWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseRegionRow(row: Row): RegionData {
    var result = new RegionData()
    // Populate Region Data
    result.Name = convertCellToString(row.getCell(RegionData.COL_NAME))
    result.PublicID = convertCellToString(row.getCell(RegionData.COL_PUBLIC_ID))
    result.Zones = convertCellToString(row.getCell(RegionData.COL_ZONES))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(RegionData.COL_EXCLUDE)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <RegionData> {
    return super.parseSheet(sheet, \r -> parseRegionRow(r), true)
  }
}
