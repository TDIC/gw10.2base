package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.GLTransactionNameMappingData
uses java.lang.IllegalArgumentException
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

/**
 * US570 - General Ledger Integration
 * 11/14/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.
 * Class to load the GL Transaction name mappings. It will check the existing mappings prior to creating a new one.
 */
class BCGLTransactionNameMappingLoader extends BCLoader implements EntityLoaderInterface {

  /**
   * Standard constructor
   */
  construct() {
  }

  /**
   * This will create the new GLIntegrationTransactionMapping_TDIC entity and commit the bundle
   * if it does not already exist.
   * 
   * @param applicationData A GLTransactionNameMappingData object containing the data for the GL Transaction Name Mapping
   */
  override function createEntity(applicationData : ApplicationData) {
    var aGLMappingData = applicationData as GLTransactionNameMappingData
    try {
      if (aGLMappingData.OriginalTransactionName == null) {
        throw new IllegalArgumentException("Original Transaction Name is null")
      }
      if (aGLMappingData.MappedTransactionName == null) {
        throw new IllegalArgumentException("Mapped Transaction Name is null")
      }
      var currMapping = GeneralUtil.findGLTransactionMappingByTransactionType(aGLMappingData.OriginalTransactionName)
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        var aGLMapping : GLIntegrationTransactionMapping_TDIC
        if (currMapping == null) { // create new
          aGLMapping = new GLIntegrationTransactionMapping_TDIC()
        } else { // update existing
          aGLMapping = bundle.add(currMapping)
        }
        aGLMapping.OriginalTransactionName = aGLMappingData.OriginalTransactionName
        aGLMapping.MappedTransactionName = aGLMappingData.MappedTransactionName
        bundle.commit()
        aGLMappingData.Skipped = false
      })
    } catch(e : Exception) {
      LOG.warn("GLTransactionNameMapping Load Failed: " + aGLMappingData.OriginalTransactionName + e.toString())
      aGLMappingData.Error = true
    }
  }

}