package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses java.util.ArrayList
uses tdic.util.dataloader.data.ColumnDescriptor

class UserGroupData extends ApplicationData {

  // constants
  public static final var SHEET : String = "UserGroup"
  public static final var COL_USER_REFERENCE_NAME : int = 0
  public static final var COL_USER_PUBLIC_ID : int = 1
  public static final var COL_GROUP1 : int = 2
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_USER_REFERENCE_NAME, "Enter the Load Factor for each user for each group in the cell (100, 75, etc)\n"+
                             "Add a :M if the user is a manager in the group\n" +
                             "Add a :A if the user is a load factor admin in the group\n" +
                             "Add a :B if the user is both a manager and a load factor admin\n", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_USER_PUBLIC_ID, "User Public-Id", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }

  // properties
  private var _userReferenceName       : String                       as UserReferenceName
  private var _userPublicID            : String                       as UserPublicID
  private var _LoadFactor              : ArrayList                    as LoadFactor = new ArrayList()
}
