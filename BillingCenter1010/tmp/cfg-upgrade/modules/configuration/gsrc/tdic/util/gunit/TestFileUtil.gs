package tdic.util.gunit

uses gw.testharness.TestBase
uses java.io.File

abstract class TestFileUtil {

  private construct() {
  }

  public static function buildDirectoryToSearch(rootDirectoryPath : String, packageName : String) : File {
    TestBase.assertTrue(rootDirectoryPath.startsWith("./"))
    TestBase.assertTrue(rootDirectoryPath.endsWith("/"))
    var packageWithSlashes = packageName.replace(".", "/")
    var relativePathWithSlashes = rootDirectoryPath + packageWithSlashes
    var relativePathWithProperFileSeparatorChars = relativePathWithSlashes.replace("/", File.separator)
    return createAbsoluteFile(relativePathWithProperFileSeparatorChars)
  }

  public static function extractFullyQualifiedClassName(gtestDir : File, file : File) : String {
    var gtestDirLen = gtestDir.AbsolutePath.length + 1
    var fullyQualifiedClassName = file.AbsolutePath.substring(gtestDirLen).replace("\\", ".")
    var gosuFileExtension = ".gs"
    TestBase.assertTrue(fullyQualifiedClassName.endsWith(gosuFileExtension))
    return fullyQualifiedClassName.substring(0, fullyQualifiedClassName.length - gosuFileExtension.length)
  }

  public static function createAbsoluteFile(relativePath : String) : File {
    return new File(new File(relativePath).AbsolutePath)
  }

}
