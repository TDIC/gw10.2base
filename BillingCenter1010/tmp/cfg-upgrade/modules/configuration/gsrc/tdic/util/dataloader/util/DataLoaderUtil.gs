package tdic.util.dataloader.util

uses java.util.Date
uses gw.api.database.Query
uses gw.api.tools.TestingClock
uses gw.api.util.DateUtil
uses java.lang.StringBuilder
uses org.slf4j.LoggerFactory
uses gw.api.database.Relop

class DataLoaderUtil {
  protected static final var LOG: org.slf4j.Logger = LoggerFactory.getLogger("DataLoader.Utility")
  construct() {
  }

  // properties files included since type loader will not do this in Ferrite (yet!)
  //    # import and export subpath only need to be different if you want to prevent overwriting of prepared import files
  public static final var IMPORT_SUBPATH: String = "config\\datafiles"
  public static final var EXPORT_SUBPATH: String = "config\\datafiles\\export"
  //    # use .xls for Excel 2003 compatible files (no more than 255 columns)
  //    # use .xlsm for Excel 2007+ files with macros, e.g. Admin Data template file (use for import only since POI libray does not support creating .xlsm files)
  //    # use .xlsx for Excel 2007+ files that don't contain macros, e.g. exported file
  public static final var IMPORT_ADMINFILE: String ="BCAdminDataLoader.xlsm"
  public static final var IMPORT_PLANFILE: String ="BCPlanDataLoader.xls"
  public static final var IMPORT_SAMPLEFILE: String ="BCSampleDataLoader.xls"
  public static final var EXPORT_ADMINFILE: String ="BCAdminDataLoader.xlsx"
  public static final var EXPORT_PLANFILE: String ="BCPlanDataLoader.xls"
  public static final var EXPORT_DBPAYMENTFILE: String ="DBpaymentfile.csv"
  public static final var EXPORT_ABPAYMENTFILE: String ="ABpaymentfile.csv"
  public static final var EXPORT_PAYMENTREVERSALFILE: String ="PaymentReversalfile.csv"
  //    # dateformat to be used (export and import)
  public static final var DATE_FORMAT: String="dd-MMM-yyyy"
  //    # prevent unrestricted user from being exported (user.UnrestrictedUser)
  public static final var EXPORT_UNRESTRICTED_USER: String ="true"
  //    # prevent system user from being exported (user.SystemUser)
  public static final var EXPORT_SYSTEM_USER: String ="true"
  //    # prevent default owner user from being exported (user.Credential.UserName == "defaultowner")
  public static final var EXPORT_DEFAULTOWNER_USER: String ="true"
  //     # prevent default root group from being exported (group.Name == "Default Root Group")
  public static final var EXPORT_DEFAULTROOT_GROUP: String ="true"
  //     # export value for password (passwords are encrypted, so export and reimport doesn't work, the values are only used when importing new users, not on update)
  public static final var EXPORT_PASSWORD: String ="gw"
  //      # export load factor for users assigned to group (an ampty field would mean the user is not associated to the group, so a value is needed here)
  public static final var EXPORT_LOADFACTOR: String ="100"

  //Sets the system close to new date
  public static function setTestingClock(transDate: Date) {
    LOG.debug("Date: " + transDate.toString())
    var clock = new TestingClock()
    var currDate = DateUtil.currentDate()
    if (transDate.compareIgnoreTime(currDate) > 0){
      clock.setDateTime(transDate)
    }
  }

  public static function toPrettyString(array: java.util.ArrayList<gw.entity.TypeKey>): String {
    if (array == null)
      return " - "
    if (array.Empty) {
      return " - "
    } else {
      var s = new StringBuilder()
      foreach (i in array) {
        if (!s.isEmpty()) {
          s.append(", ")
        }
        s.append(i.DisplayName)
      }
      return s.toString()
    }
  }

  // used by Sample and Plan loaders
  public static function findDelinquencyPlanByName(name: String): DelinquencyPlan {
    return (Query.make(entity.DelinquencyPlan).compare(DelinquencyPlan#Name, Equals, name).select().getFirstResult())
  }

  // Helper functions for excel data file paths
  public static function getExportAdminDatafilePath(): String {
    return getFullExportDatafilePath(EXPORT_ADMINFILE)
  }

  public static function getExportPlanDatafilePath(): String {
    return EXPORT_PLANFILE
  }

  public static function getRelativeImportDatafilePath(prop: String): String {
    return IMPORT_SUBPATH + "\\" + prop
  }

  public static function getRelativeExportDatafilePath(prop: String): String {
    return EXPORT_SUBPATH + "\\" + prop
  }

  public static function getFullImportDatafilePath(prop: String): String {
    return gw.api.util.ConfigAccess.getModuleRoot("configuration") + "\\" + IMPORT_SUBPATH + "\\" + prop
  }

  public static function getFullExportDatafilePath(prop: String): String {
    return gw.api.util.ConfigAccess.getModuleRoot("configuration") + "\\" + EXPORT_SUBPATH + "\\" + prop
  }

  public static function getImportAdminDatafilePath(): String {
    return getFullImportDatafilePath(IMPORT_ADMINFILE)
  }

  public static function getImportPlanDatafilePath(): String {
    return getFullImportDatafilePath(IMPORT_PLANFILE)
  }

  public static function getImportSampleDatafilePath(): String {
    return getFullImportDatafilePath(IMPORT_SAMPLEFILE)
  }

  public static function getRelativeAdminDatafilePath(): String {
    return getRelativeImportDatafilePath(IMPORT_ADMINFILE)
  }

  public static function getRelativePlanDatafilePath(): String {
    return getRelativeImportDatafilePath(IMPORT_PLANFILE)
  }

  public static function getRelativeSampleDatafilePath(): String {
    return getRelativeImportDatafilePath(IMPORT_SAMPLEFILE)
  }

  public static function getDateFormat(): String {
    return DATE_FORMAT != null ? DATE_FORMAT : "dd-MMM-yyyy"
  }

  public static function preventExport(user: User): boolean {
    if (EXPORT_UNRESTRICTED_USER == "false" and user.UnrestrictedUser) return true
    if (EXPORT_SYSTEM_USER == "false" and user.SystemUser) return true
    if (EXPORT_DEFAULTOWNER_USER == "false" and user.Credential.UserName == "defaultowner") return true
    return false
  }

  public static function preventExport(group: Group): boolean {
    if (EXPORT_DEFAULTROOT_GROUP == "false" and group.Name == "Default Root Group") return true
    return false
  }

  public static function getExportPassword(): String {
    return EXPORT_PASSWORD != null ? EXPORT_PASSWORD : "gw"
  }

  public static function getExportLoadFactor(): String {
    return EXPORT_LOADFACTOR != null ? EXPORT_LOADFACTOR : "100"
  }
}
