package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.webservice.policycenter.bc1000.entity.types.complex.RenewalInfo
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.RenewalData
uses gw.transaction.Transaction
uses java.util.ArrayList
uses gw.api.database.Query
uses java.lang.Exception
uses gw.api.database.Relop

class BCRenewalLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aRenewalData = applicationData as RenewalData
    // check for existing Prior PolicyPeriod; no update if orig Policy is not available
    var ppPriorAll = Query.make(entity.PolicyPeriod).compare(PolicyPeriod#PolicyNumber, Equals, aRenewalData.PriorPolicyPeriod).select()
    var ppPriorArray = ppPriorAll.toList()
    var ppPriorFind = ppPriorArray.firstWhere(\p -> p.TermNumber == ppPriorArray.max(\p2 -> p2.TermNumber))
    if (ppPriorFind != null) {
      var ppPrior = Query.make(PolicyPeriod).compare(PolicyPeriod#PublicID, Equals, ppPriorFind.PublicID).select().FirstResult
      var newRenewalInfo = createRenewalInfo(ppPrior, aRenewalData)
      if (aRenewalData.Charges != null) {
        buildCharges(newRenewalInfo, aRenewalData)
      } else {
        if(LOG.DebugEnabled) {
          LOG.debug("null Charges")
        }
      }
      try {
        Transaction.runWithNewBundle(\bundle -> {
          var bi = newRenewalInfo.executeRenewalBI()
          LOG.info("New Renewal: " + "\n" + " PolicyNumber: " + ppPrior.PolicyNumber + "\n")
          if (LOG.DebugEnabled) {
            LOG.debug("BillingInstruction: " + bi + " returned")
          }
          aRenewalData.Skipped = false
        })
      } catch (e: Exception) {
        if (LOG.InfoEnabled){
          LOG.info("Renewal Load: " + aRenewalData.PriorPolicyPeriod + e.toString())
        }
        aRenewalData.Error = true
      }
    }
  }

  private function createRenewalInfo(ppPrior:PolicyPeriod, aRenewalData: RenewalData): RenewalInfo {
    var rpi = new RenewalInfo()
    rpi.Description = aRenewalData.Description
    rpi.PriorPolicyNumber = ppPrior.PolicyNumber
    rpi.PolicyNumber = rpi.PriorPolicyNumber
    rpi.PriorTermNumber = ppPrior.TermNumber
    rpi.AccountNumber = ppPrior.Account.AccountNumber
    rpi.Currency = ppPrior.Currency.Code
    rpi.PaymentPlanPublicId = ppPrior.PaymentPlan.PublicID
    rpi.BillingMethodCode = ppPrior.BillingMethod.Code
    rpi.ModelDate = DateToXmlDate(aRenewalData.EntryDate)
    rpi.AssignedRisk = ppPrior.AssignedRisk
    rpi.UWCompanyCode = ppPrior.UWCompany.Code
    rpi.ProducerCodeOfRecordId = ppPrior.getDefaultProducerCodeForRole(TC_PRIMARY).PublicID
    rpi.JurisdictionCode = ppPrior.RiskJurisdiction.Code
    rpi.ProductCode = ppPrior.Policy.LOBCode.Code
    return rpi
  }

  private function buildCharges(rpi: RenewalInfo, id:RenewalData) {
    var d = DateToXmlDate(id.EntryDate)
    var chargesToProcess = id.Charges.where(\elt -> elt.ChargePattern != null and elt.ChargeAmount != null and elt.ChargeCurrency != null and elt.ChargeAmount != 0)
    chargesToProcess.each(\elt -> {
      rpi.addChargeInfo(ChargeDataToChargeInfo(elt, d))
    })
  }
}
