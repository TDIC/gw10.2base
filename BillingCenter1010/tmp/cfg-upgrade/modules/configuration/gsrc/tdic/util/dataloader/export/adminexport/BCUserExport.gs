package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.util.DataLoaderUtil
uses java.util.Collection
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.admindata.UserData

class BCUserExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return UserData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getUserEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(UserData.CD), queryUsers(), \e -> getRowFromUser(e))
  }

  // query data

  public static function queryUsers(): Collection <User> {
    var result = Query.make(User).select().where(\u -> !DataLoaderUtil.preventExport(u)).orderBy(\u -> u.PublicID)
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromUser(user: User): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(UserData.COL_REFERENCE_NAME), user.DisplayName + " (" + user.Credential + ")")
    colMap.put(colIndexString(UserData.COL_EXCLUDE), "false")
    colMap.put(colIndexString(UserData.COL_ACTIVE), user.Credential.Active)
    colMap.put(colIndexString(UserData.COL_PUBLIC_ID), user.PublicID)
    colMap.put(colIndexString(UserData.COL_LAST_NAME), user.Contact.LastName)
    colMap.put(colIndexString(UserData.COL_FIRST_NAME), user.Contact.FirstName)
    colMap.put(colIndexString(UserData.COL_MIDDLE_NAME), user.Contact.MiddleName)
    colMap.put(colIndexString(UserData.COL_EMPLOYEE_NUMBER), user.Contact.EmployeeNumber)
    colMap.put(colIndexString(UserData.COL_USERNAME), user.Credential.UserName)
    // Since all passwords are encrypted, by default export the password to GW
    colMap.put(colIndexString(UserData.COL_PASSWORD), DataLoaderUtil.getExportPassword())
    colMap.put(colIndexString(UserData.COL_JOB_TITLE), user.JobTitle)
    colMap.put(colIndexString(UserData.COL_DEPARTMENT), user.Department)
    colMap.put(colIndexString(UserData.COL_GENDER), user.Contact.Gender)
    colMap.put(colIndexString(UserData.COL_HOME_PHONE), user.Contact.HomePhone)
    colMap.put(colIndexString(UserData.COL_WORK_PHONE), user.Contact.WorkPhone)
    colMap.put(colIndexString(UserData.COL_CELL_PHONE), user.Contact.CellPhone)
    colMap.put(colIndexString(UserData.COL_FAX_PHONE), user.Contact.FaxPhone)
    colMap.put(colIndexString(UserData.COL_PRIMARY_PHONE), user.Contact.PrimaryPhone)
    colMap.put(colIndexString(UserData.COL_PRIMARY_ADDRESS), user.Contact.PrimaryAddress)
    colMap.put(colIndexString(UserData.COL_NONPRIMARY_ADDRESS), user.Contact.AllAddresses.firstWhere(\a -> a.DisplayName != user.Contact.PrimaryAddress.DisplayName))
    colMap.put(colIndexString(UserData.COL_FORMER_NAME), user.Contact.FormerName)
    colMap.put(colIndexString(UserData.COL_EMAIL_ADDRESS1), user.Contact.EmailAddress1)
    colMap.put(colIndexString(UserData.COL_EMAIL_ADDRESS2), user.Contact.EmailAddress2)
    colMap.put(colIndexString(UserData.COL_DATE_OF_BIRTH), user.Contact.DateOfBirth)
    colMap.put(colIndexString(UserData.COL_EXTERNAL_USER), user.ExternalUser)
    colMap.put(colIndexString(UserData.COL_ROLE1), user.Roles.Count > 0 ? user.Roles[0].Role : "")
    colMap.put(colIndexString(UserData.COL_ROLE2), user.Roles.Count > 1 ? user.Roles[1].Role : "")
    colMap.put(colIndexString(UserData.COL_ROLE3), user.Roles.Count > 2 ? user.Roles[2].Role : "")
    colMap.put(colIndexString(UserData.COL_REGION1), user.Regions.Count > 0 ? user.Regions[0].Region : "")
    colMap.put(colIndexString(UserData.COL_REGION2), user.Regions.Count > 1 ? user.Regions[1].Region : "")
    colMap.put(colIndexString(UserData.COL_REGION3), user.Regions.Count > 2 ? user.Regions[2].Region : "")
    colMap.put(colIndexString(UserData.COL_LANGUAGE), user.Language)
    colMap.put(colIndexString(UserData.COL_NOTES), user.Contact.Notes)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getUserColumnTypes(): HashMap {
    return getColumnTypes(UserData.CD)
  }
}
