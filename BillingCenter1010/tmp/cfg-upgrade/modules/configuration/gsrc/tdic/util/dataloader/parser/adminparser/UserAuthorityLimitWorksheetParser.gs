package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.UserAuthorityLimitData
uses tdic.util.dataloader.data.admindata.AuthorityLimitProfileData

class UserAuthorityLimitWorksheetParser extends WorksheetParserUtil {
  construct() {
  }
  var authorityLimitProfileArray = new ArrayList <AuthorityLimitProfileData>()
  function parseUserAuthorityLimitRow(row: Row): UserAuthorityLimitData {
    var result = new UserAuthorityLimitData()
    result.UserReferenceName = row.getCell(UserAuthorityLimitData.COL_USER_REFERENCE_NAME).StringCellValue
    result.UserPublicID = row.getCell(UserAuthorityLimitData.COL_USER_PUBLIC_ID).StringCellValue
    if (result.UserPublicID.Empty)
      return null
    result.AuthorityLimitPublicID = row.getCell(UserAuthorityLimitData.COL_AUTHORITY_LIMIT_PUBLIC_ID).StringCellValue
    if (result.UserPublicID != null and result.UserPublicID != "") {
      //get AuthLimitProfile from array
      var profileID = (convertCellToString(row.getCell(UserAuthorityLimitData.COL_PROFILE)))
      if (profileID != null) {
        result.Profile = authorityLimitProfileArray.firstWhere(\p -> p.Name == profileID)
      }
      else {
        // step through columns, add custom limit amount to array if cell is non-blank
        for (myCell in row index col) {
          // skip first four columns
          if (col > 3) {
            // add amount to array
            result.LimitAmounts.add(myCell.toString())
          }
        }
      }
    }
    return result
  }

  function parseUserAuthorityLimitSheet(sheet: Sheet, alpArray: ArrayList <AuthorityLimitProfileData>)
      : ArrayList <UserAuthorityLimitData> {
    authorityLimitProfileArray = alpArray
    return parseSheet(sheet, \r -> parseUserAuthorityLimitRow(r), false)
  }
}
