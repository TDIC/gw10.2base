package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.admindata.RoleData
uses java.util.Collection
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCRoleExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return RoleData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getRoleEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(RoleData.CD), queryRoles(), \e -> getRowFromRole(e))
  }

  // query data

  public static function queryRoles(): Collection <Role> {
    var result = Query.make(Role).select().orderBy(
      QuerySelectColumns.path(Paths.make(entity.Role#PublicID))
    )
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromRole(role: Role): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(RoleData.COL_PUBLIC_ID), role.PublicID)
    colMap.put(colIndexString(RoleData.COL_NAME), role.Name)
    colMap.put(colIndexString(RoleData.COL_DESCRIPTION), role.Description)
    colMap.put(colIndexString(RoleData.COL_EXCLUDE), "false")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getRoleColumnTypes(): HashMap {
    return getColumnTypes(RoleData.CD)
  }
}
