package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.GroupData
uses tdic.util.dataloader.data.admindata.QueueData
uses gw.api.upgrade.Coercions

class QueueWorksheetParser extends WorksheetParserUtil {
  construct() {
  }
  var groupArray = new ArrayList <GroupData>()
  function parseQueueRow(row: Row): QueueData {
    // read group reference
    var groupName = convertCellToString(row.getCell(QueueData.COL_GROUP))
    if (groupName == null or groupName == "" or groupName == {null} as java.lang.String) {
      var msg = row.Sheet.SheetName + " sheet: Value " + groupName
          + " in column " + QueueData.CD[QueueData.COL_GROUP].ColHeader
          + " of row " + row.RowNum
          + " is required but blank"
      LOG.error(msg)
    }
    var data = groupArray.firstWhere(\g -> g.Name == groupName)
    if (data == null) {
      var msg = row.Sheet.SheetName + " sheet: Value " + groupName
          + " in column " + QueueData.CD[QueueData.COL_GROUP].ColHeader
          + " of row " + row.RowNum
          + " is not a known group name"
      LOG.error(msg)
      throw msg
    }
    // Populate Queue Data
    var result = new QueueData()
    // igore DISPLAYNAME column
    result.PublicID = convertCellToString(row.getCell(QueueData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(QueueData.COL_NAME))
    result.Group = data
    result.Description = convertCellToString(row.getCell(QueueData.COL_DESCRIPTION))
    result.SubGroupVisible = Coercions.makeBooleanFrom(convertCellToString(row.getCell(QueueData.COL_SUB_GROUP_VISIBLE)))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(QueueData.COL_EXCLUDE)))
    return result
  }

  function parseQueueSheet(sheet: Sheet, grpArray: ArrayList <GroupData>): ArrayList <QueueData> {
    groupArray = grpArray
    return parseSheet(sheet, \r -> parseQueueRow(r), true)
  }
}
