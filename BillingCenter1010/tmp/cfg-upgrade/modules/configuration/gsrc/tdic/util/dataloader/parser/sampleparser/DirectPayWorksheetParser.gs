package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses gw.util.GWBaseDateEnhancement
uses tdic.util.dataloader.data.sampledata.DirectPaymentData
uses java.util.ArrayList
uses gw.api.upgrade.Coercions

class DirectPayWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var RECEIVED_DATE: int = 0
  var ACCOUNT_NAME: int = 1
  var POLICY_NUMBER: int = 2
  var DESCRIPTION: int = 3
  var PAYMENT_REF: int = 4
  var PAYMENT_METHOD: int = 5
  var PAYMENT_TOKEN: int = 6
  var AMOUNT: int = 7
  function parseDirectPayRow(row: Row): DirectPaymentData {
    var directPaymentData = new DirectPaymentData()
    // Populate DirectPayment data
    directPaymentData.AccountName = convertCellToString(row.getCell(ACCOUNT_NAME))
    if (directPaymentData.AccountName == null)
      return null
    directPaymentData.PolicyNumber = convertCellToString(row.getCell(POLICY_NUMBER))
    directPaymentData.Description = convertCellToString(row.getCell(DESCRIPTION))
    directPaymentData.ReferenceNumber = convertCellToString(row.getCell(PAYMENT_REF))
    directPaymentData.PaymentMethod = typekey.PaymentMethod.get(convertCellToString(row.getCell(PAYMENT_METHOD)))
    directPaymentData.PaymentToken = convertCellToString(row.getCell(PAYMENT_TOKEN))
    directPaymentData.Amount = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(AMOUNT)))
    directPaymentData.ReceivedDate = row.getCell(RECEIVED_DATE).DateCellValue
    directPaymentData.EntryDate = row.getCell(RECEIVED_DATE).DateCellValue
    if (directPaymentData.ReceivedDate == null  or directPaymentData.ReceivedDate < gw.api.util.DateUtil.currentDate()) {
      directPaymentData.ReceivedDate = gw.api.util.DateUtil.currentDate()
      directPaymentData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    return directPaymentData
  }

  override function parseSheet(sheet: Sheet): ArrayList <DirectPaymentData> {
    return parseSheet(sheet, \r -> parseDirectPayRow(r), false)
  }
}