package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.UserData
uses tdic.util.dataloader.data.admindata.RegionData
uses tdic.util.dataloader.data.admindata.RoleData
uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.data.admindata.UserContactData
uses gw.api.upgrade.Coercions

class UserWorksheetParser extends WorksheetParserUtil {
  construct() {
  }
  var regionArray= new ArrayList <RegionData>()
  var roleArray= new ArrayList <RoleData>()
  var addressArray= new ArrayList <AddressData>()
  function parseUserRow(row: Row): UserData {
    var result = new UserData()
    var contactData = new UserContactData()
    // Populate User Contact
    contactData.FirstName = convertCellToString(row.getCell(UserData.COL_FIRST_NAME))
    contactData.LastName = convertCellToString(row.getCell(UserData.COL_LAST_NAME))
    contactData.MiddleName = convertCellToString(row.getCell(UserData.COL_MIDDLE_NAME))
    if (contactData.MiddleName != null) {
      contactData.Name = contactData.FirstName + " " + contactData.MiddleName + " " + contactData.LastName
    }
    else {
      contactData.Name = contactData.FirstName + " " + contactData.LastName
    }
    contactData.HomePhone = convertCellToString(row.getCell(UserData.COL_HOME_PHONE))
    contactData.WorkPhone = convertCellToString(row.getCell(UserData.COL_WORK_PHONE))
    contactData.CellPhone = convertCellToString(row.getCell(UserData.COL_CELL_PHONE))
    contactData.FaxPhone = convertCellToString(row.getCell(UserData.COL_FAX_PHONE))
    contactData.PrimaryPhone = convertCellToString(row.getCell(UserData.COL_PRIMARY_PHONE))
    contactData.FormerName = convertCellToString(row.getCell(UserData.COL_FORMER_NAME))
    contactData.EmailAddress1 = convertCellToString(row.getCell(UserData.COL_EMAIL_ADDRESS1))
    contactData.EmailAddress2 = convertCellToString(row.getCell(UserData.COL_EMAIL_ADDRESS2))
    contactData.Gender = convertCellToString(row.getCell(UserData.COL_GENDER))
    contactData.DateOfBirth = convertStringToDate(convertCellToString(row.getCell(UserData.COL_DATE_OF_BIRTH)))
    contactData.EmployeeNumber = getStringCellValue(row.getCell(UserData.COL_EMPLOYEE_NUMBER))
    contactData.Notes = convertCellToString(row.getCell(UserData.COL_NOTES))
    //get Addresses from array
    var addressID = (convertCellToString(row.getCell(UserData.COL_PRIMARY_ADDRESS)))
    var nonPrimaryAddressID = (convertCellToString(row.getCell(UserData.COL_NONPRIMARY_ADDRESS)))
    if (addressArray != null){
      contactData.PrimaryAddress = addressArray.firstWhere(\p -> p.DisplayName == addressID)
      contactData.NonPrimaryAddress = addressArray.firstWhere(\p -> p.DisplayName == nonPrimaryAddressID)
    }
    //get Regions from array
    var regionID1 = (convertCellToString(row.getCell(UserData.COL_REGION1)))
    var regionID2 = (convertCellToString(row.getCell(UserData.COL_REGION2)))
    var regionID3 = (convertCellToString(row.getCell(UserData.COL_REGION3)))
    if (regionArray != null){
      result.Region1 = regionArray.firstWhere(\p -> p.Name == regionID1)
      result.Region2 = regionArray.firstWhere(\p -> p.Name == regionID2)
      result.Region3 = regionArray.firstWhere(\p -> p.Name == regionID3)
    }
    //get Roles from array
    var roleID1 = (convertCellToString(row.getCell(UserData.COL_ROLE1)))
    var roleID2 = (convertCellToString(row.getCell(UserData.COL_ROLE2)))
    var roleID3 = (convertCellToString(row.getCell(UserData.COL_ROLE3)))
    var roleID4 = (convertCellToString(row.getCell(UserData.COL_ROLE4)))
    var roleID5 = (convertCellToString(row.getCell(UserData.COL_ROLE5)))
    var roleID6 = (convertCellToString(row.getCell(UserData.COL_ROLE6)))
    var roleID7 = (convertCellToString(row.getCell(UserData.COL_ROLE7)))
    if (roleArray != null){
      result.Role1 = roleArray.firstWhere(\p -> p.Name == roleID1)
      result.Role2 = roleArray.firstWhere(\p -> p.Name == roleID2)
      result.Role3 = roleArray.firstWhere(\p -> p.Name == roleID3)
      result.Role4 = roleArray.firstWhere(\p -> p.Name == roleID4)
      result.Role5 = roleArray.firstWhere(\p -> p.Name == roleID5)
      result.Role6 = roleArray.firstWhere(\p -> p.Name == roleID6)
      result.Role7 = roleArray.firstWhere(\p -> p.Name == roleID7)
    }
    // Populate User Data
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(UserData.COL_EXCLUDE)))
    result.Active = Coercions.makeBooleanFrom(convertCellToString(row.getCell(UserData.COL_ACTIVE)))
    result.PublicID = convertCellToString(row.getCell(UserData.COL_PUBLIC_ID))
    result.Contact = contactData
    result.UserName = convertCellToString(row.getCell(UserData.COL_USERNAME))
    result.Password = convertCellToString(row.getCell(UserData.COL_PASSWORD))
    result.JobTitle = convertCellToString(row.getCell(UserData.COL_JOB_TITLE))
    result.Department = convertCellToString(row.getCell(UserData.COL_DEPARTMENT))
    result.ExternalUser = Coercions.makeBooleanFrom(convertCellToString(row.getCell(UserData.COL_EXTERNAL_USER)))
    result.Language = convertCellToString(row.getCell(UserData.COL_LANGUAGE))
    result.ReferenceName = row.getCell(UserData.COL_REFERENCE_NAME).StringCellValue
    return result
  }

  function parseUserSheet(sheet: Sheet, regArray: ArrayList <RegionData>,
                          rleArray: ArrayList <RoleData>, addrArray: ArrayList <AddressData>): ArrayList <UserData> {
    regionArray = regArray
    roleArray = rleArray
    addressArray = addrArray
    return parseSheet(sheet, \r -> parseUserRow(r), true)
  }
}
