package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.command.demo.GeneralUtil
uses gw.webservice.policycenter.bc1000.entity.types.complex.ReinstatementInfo
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses gw.transaction.Transaction
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.ReinstatementData
uses java.lang.Exception
uses gw.api.database.Query
uses gw.api.database.Relop

class BCReinstatementLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aReinstatementData = applicationData as ReinstatementData
    // check for existing PolicyPeriod
    var polPeriod = GeneralUtil.findPolicyPeriod(aReinstatementData.AssociatedPolicyPeriod)
    aReinstatementData.TermNumber = polPeriod.TermNumber
    if (polPeriod != null) {
      var newReinstatementInfo = createReinstatementInfo(aReinstatementData)
      if (aReinstatementData.Charges != null) {
        buildCharges(newReinstatementInfo, aReinstatementData)
      }
      else {
        if (LOG.DebugEnabled) {
          LOG.debug("null Charges")
        }
      }
      try {
        Transaction.runWithNewBundle(\bundle -> {
          if (LOG.DebugEnabled) {
            LOG.debug("about to call BillingAPI.sendBillingInstruction with Reinstatement: " + newReinstatementInfo)
          }
          LOG.info("New Policy Reinstatement: " + "\n" + " PolicyNumber: " + newReinstatementInfo.PolicyNumber + "\n")
          var bi = newReinstatementInfo.executeReinstatementBI()
          if (LOG.DebugEnabled) {
            LOG.debug("BillingInstruction: " + bi + " returned")
          }
          aReinstatementData.Skipped = false
        } )
      } catch (e: Exception) {
        if (LOG.InfoEnabled){
          LOG.info("Policy Reinstate Load: " + aReinstatementData.AssociatedPolicyPeriod + e.toString())
        }
        aReinstatementData.Error = true
      }
    }
  }

  private function buildCharges(info: ReinstatementInfo, data:ReinstatementData) {
    var d = DateToXmlDate(data.EntryDate)
    var chargesToProcess = data.Charges.where(\elt -> elt.ChargePattern != null and elt.ChargeAmount != null and elt.ChargeCurrency != null and elt.ChargeAmount != 0)
    chargesToProcess.each(\elt -> {
      info.addChargeInfo(ChargeDataToChargeInfo(elt, d))
    })
  }

  private function createReinstatementInfo(aReinstatementData: ReinstatementData): ReinstatementInfo {
    var aReinstatement = new ReinstatementInfo()
    aReinstatement.PolicyNumber = aReinstatementData.AssociatedPolicyPeriod
    aReinstatement.TermNumber = aReinstatementData.TermNumber
    aReinstatement.Description = aReinstatementData.Description
    aReinstatement.EffectiveDate = DateToXmlDate(aReinstatementData.ModificationDate)
    return aReinstatement
  }
}

