package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses tdic.util.dataloader.data.plandata.PaymentPlanOverrideData
uses gw.api.upgrade.Coercions

class PaymentPlanOverrideWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parsePaymentPlanOverrideRow(row: Row): PaymentPlanOverrideData {
    var result = new PaymentPlanOverrideData()
    // Populate Payment Plan Override Data
    result.LookupPaymentPlanData = convertCellToString(row.getCell(PaymentPlanOverrideData.COL_LookupPaymentPlanData))
    if (result.LookupPaymentPlanData == null)
      return null
    result.OverrideBillingInstruction = convertCellToString(row.getCell(PaymentPlanOverrideData.COL_OverrideBillingInstruction))
    result.DownPaymentPercent = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanOverrideData.COL_DOWNPAYMENT_PERCENT)))
    result.MaximumNumberOfInstallments = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanOverrideData.COL_MAXIMUM_NUMBER_OF_INSTALLMENTS)))
    result.DaysFromReferenceDateToDownPayment = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanOverrideData.COL_DAYS_FROM_REFERENCE_DATE_TO_DOWNPAYMENT)))
    result.DownPaymentAfter = typekey.PaymentScheduledAfter.get(convertCellToString(row.getCell(PaymentPlanOverrideData.COL_DOWNPAYMENT_AFTER)))
    result.DaysFromReferenceDateToFirstInstallment = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanOverrideData.COL_DAYS_FROM_REFERENCE_DATE_TO_FIRST_INSTALLMENT)))
    result.FirstInstallmentAfter = typekey.PaymentScheduledAfter.get(convertCellToString(row.getCell(PaymentPlanOverrideData.COL_FIRST_INSTALLMENT_AFTER)))
    result.DaysFromReferenceDateToOneTimeCharge = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanOverrideData.COL_DAYS_FROM_REFERENCE_DATE_TO_ONETIME_CHARGE)))
    result.OneTimeChargeAfter = typekey.PaymentScheduledAfter.get(convertCellToString(row.getCell(PaymentPlanOverrideData.COL_ONETIME_CHARGE_AFTER)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <PaymentPlanOverrideData> {
    return parseSheet(sheet, \r -> parsePaymentPlanOverrideRow(r), false)
  }
}