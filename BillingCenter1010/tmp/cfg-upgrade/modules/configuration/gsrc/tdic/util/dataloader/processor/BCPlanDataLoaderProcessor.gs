package tdic.util.dataloader.processor

uses java.util.ArrayList
uses tdic.util.dataloader.data.plandata.BillingPlanData
uses tdic.util.dataloader.data.plandata.ChargePatternData
uses tdic.util.dataloader.data.plandata.CommissionPlanData
uses tdic.util.dataloader.data.plandata.CommissionSubPlanData
uses tdic.util.dataloader.data.plandata.DelinquencyPlanData
uses tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData
uses tdic.util.dataloader.data.plandata.AgencyBillPlanData
uses tdic.util.dataloader.data.plandata.PaymentPlanData
uses tdic.util.dataloader.data.plandata.PaymentPlanOverrideData
uses tdic.util.dataloader.data.plandata.PaymentAllocationPlanData
uses tdic.util.dataloader.data.plandata.PlanLocalizationData
uses tdic.util.dataloader.entityloaders.planloaders.BCPlanLocalizationLoader
uses tdic.util.dataloader.entityloaders.planloaders.ReturnPremiumPlanLoader
uses tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData
uses tdic.util.dataloader.parser.planparser.BCPlanExcelFileParser
uses tdic.util.dataloader.entityloaders.planloaders.BCBillingPlanLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCPaymentPlanLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCPaymentPlanOverrideLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCDelinquencyPlanLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCDelinquencyPlanWorkflowLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCCommissionPlanLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCCommissionSubPlanLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCChargePatternLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCAgencyBillPlanLoader
uses tdic.util.dataloader.entityloaders.planloaders.PaymentAllocationPlanLoader
uses tdic.util.dataloader.data.plandata.ReturnPremiumPlanData
uses tdic.util.dataloader.entityloaders.planloaders.ReturnPremiumSchemeLoader
uses tdic.util.dataloader.util.DataLoaderUtil
uses java.lang.Exception
uses org.slf4j.LoggerFactory
uses tdic.util.dataloader.data.plandata.GLTransactionFilterData
uses tdic.util.dataloader.data.plandata.GLTransactionNameMappingData
uses tdic.util.dataloader.data.plandata.GLTAccountNameMappingData
uses tdic.util.dataloader.entityloaders.planloaders.BCGLTransactionFilterLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCGLTransactionNameMappingLoader
uses tdic.util.dataloader.entityloaders.planloaders.BCGLTAccountNameMappingLoader

class BCPlanDataLoaderProcessor extends DataLoaderProcessor {
  construct() {
    this.RelativePath = DataLoaderUtil.getRelativePlanDatafilePath()
  }
  static final var LOG = LoggerFactory.getLogger("PlanData.PlanDataProcessor")
  private var _billingPlanArray: ArrayList <BillingPlanData> as BillingPlanArray = new ArrayList <BillingPlanData>()
  private var _agencyBillPlanArray: ArrayList <AgencyBillPlanData> as AgencyBillPlanArray = new ArrayList <AgencyBillPlanData>()
  private var _paymentPlanArray: ArrayList <PaymentPlanData> as PaymentPlanArray = new ArrayList <PaymentPlanData>()
  private var _paymentPlanOverrideArray: ArrayList <PaymentPlanOverrideData> as PaymentPlanOverrideArray = new ArrayList <PaymentPlanOverrideData>()
  private var _delinquencyPlanArray: ArrayList <DelinquencyPlanData> as DelinquencyPlanArray = new ArrayList <DelinquencyPlanData>()
  private var _delinquencyPlanWorkflowArray: ArrayList <DelinquencyPlanWorkflowData> as DelinquencyPlanWorkflowArray = new ArrayList <DelinquencyPlanWorkflowData>()
  private var _commissionPlanArray: ArrayList <CommissionPlanData> as CommissionPlanArray = new ArrayList <CommissionPlanData>()
  private var _commissionSubPlanArray: ArrayList <CommissionSubPlanData> as CommissionSubPlanArray = new ArrayList <CommissionSubPlanData>()
  private var _chargePatternArray: ArrayList <ChargePatternData> as ChargePatternArray = new ArrayList <ChargePatternData>()
  private var _paymentAllocationPlanArray: ArrayList < PaymentAllocationPlanData > as PaymentAllocationPlanArray = new ArrayList < PaymentAllocationPlanData >()
  private var _returnPremiumPlanArray: ArrayList < ReturnPremiumPlanData > as ReturnPremiumPlanArray = new ArrayList < ReturnPremiumPlanData >()
  private var _returnPremiumSchemeArray: ArrayList < ReturnPremiumSchemeData > as ReturnPremiumSchemeArray = new ArrayList < ReturnPremiumSchemeData >()
  private var _localizationArray: ArrayList <PlanLocalizationData> as LocalizationArray = new ArrayList <PlanLocalizationData>()
  /**
   * US570 - Excel Data Loader Additions for custom GL Integration entities
   * 11/14/2014 Alvin Lee
   */
  private var _glTransactionFilterArray : ArrayList<GLTransactionFilterData> as GLTransactionFilterArray = new ArrayList<GLTransactionFilterData >()
  private var _glTransactionNameMappingArray : ArrayList<GLTransactionNameMappingData> as GLTransactionNameMappingArray = new ArrayList<GLTransactionNameMappingData>()
  private var _glTAccountNameMappingArray : ArrayList<GLTAccountNameMappingData> as GLTAccountNameMappingArray = new ArrayList<GLTAccountNameMappingData>()

  private var _chkBillingPlan: Boolean as ChkBillingPlan = false
  private var _chkAgencyBillPlan: Boolean as ChkAgencyBillPlan = false
  private var _chkPaymentPlan: Boolean as ChkPaymentPlan = false
  private var _chkPaymentPlanOverride: Boolean as ChkPaymentPlanOverride = false
  private var _chkDelinquencyPlan: Boolean as ChkDelinquencyPlan = false
  private var _chkDelinquencyPlanWorkflow: Boolean as ChkDelinquencyPlanWorkflow = false
  private var _chkCommissionPlan: Boolean as ChkCommissionPlan = false
  private var _chkCommissionSubPlan: Boolean as ChkCommissionSubPlan = false
  private var _chkChargePattern: Boolean as ChkChargePattern = true
  private var _chkPaymentAllocationPlan: Boolean as ChkPaymentAllocationPlan = false
  private var _chkReturnPremiumPlan: Boolean as ChkReturnPremiumPlan = true
  private var _chkReturnPremiumScheme: Boolean as ChkReturnPremiumScheme = true
  private var _chkLocalization: Boolean as ChkLocalization = true
  /**
   * US570 - Excel Data Loader Additions for custom GL Integration entities
   * 11/14/2014 Alvin Lee
   */
  private var _chkGLTransactionFilter : Boolean as ChkGLTransactionFilter = false
  private var _chkGLTransactionNameMapping : Boolean as ChkGLTransactionNameMapping = false
  private var _chkGLTAccountNameMapping : Boolean as ChkGLTAccountNameMapping = false

  private var _LoadCompleted: Boolean as LoadCompleted
  public function readAndImportFile() {
    readFile()
    importFile()
  }

  public function loadData() {
    // Process selected sheets
    if (ChkBillingPlan) {
      processBillingPlan()
    }
    if (ChkAgencyBillPlan) {
      processAgencyBillPlan()
    }
    if (ChkPaymentPlan) {
      processPaymentPlan()
    }
    if (ChkPaymentPlanOverride) {
      processPaymentPlanOverride()
    }
    if (ChkDelinquencyPlan) {
      processDelinquencyPlan()
    }
    if (ChkDelinquencyPlanWorkflow) {
      processDelinquencyPlanWorkflow()
    }
    if (ChkChargePattern) {
      processChargePattern()
    }
    if (ChkCommissionPlan) {
      processCommissionPlan()
    }
    if (ChkCommissionSubPlan) {
      processCommissionSubPlan()
    }
    if (ChkPaymentAllocationPlan) {
      processPaymentAllocationPlan()
    }
    if (ChkReturnPremiumPlan) {
      processReturnPremiumPlan()
    }
    if (ChkReturnPremiumScheme) {
      processReturnPremiumScheme()
    }
    if (ChkLocalization) {
      processLocalization()
    }
    /**
     * US570 - Excel Data Loader Additions for custom GL Integration entities
     * 11/14/2014 Alvin Lee
     */
    if (ChkGLTransactionFilter) {
      processGLTransactionFilter()
    }
    if (ChkGLTransactionNameMapping) {
      processGLTransactionNameMapping()
    }
    if (ChkGLTAccountNameMapping) {
      processGLTAccountNameMapping()
    }
    LoadCompleted = true
    LOG.info("Loaded data")
  }

  private function processBillingPlan() {
    for (billingPlan in BillingPlanArray) {
      var billingPlanLoader = new BCBillingPlanLoader()
      billingPlanLoader.createEntity(billingPlan)
    }
  }

  private function processAgencyBillPlan() {
    for (agencyBillPlan in AgencyBillPlanArray) {
      var agencyBillPlanLoader = new BCAgencyBillPlanLoader()
      agencyBillPlanLoader.createEntity(agencyBillPlan)
    }
  }

  function processPaymentPlan() {
    for (paymentPlan in PaymentPlanArray) {
      var paymentPlanLoader = new BCPaymentPlanLoader()
      paymentPlanLoader.createEntity(paymentPlan)
    }
  }

  function processPaymentPlanOverride() {
    for (paymentPlanOverride in PaymentPlanOverrideArray) {
      var paymentPlanOverrideLoader = new BCPaymentPlanOverrideLoader()
      paymentPlanOverrideLoader.createEntity(paymentPlanOverride)
    }
  }

  function processDelinquencyPlan() {
    for (delinquencyPlan in DelinquencyPlanArray) {
      var delinquencyPlanLoader = new BCDelinquencyPlanLoader()
      delinquencyPlanLoader.createEntity(delinquencyPlan)
    }
  }

  function processDelinquencyPlanWorkflow() {
    for (delinquencyPlanWorkflow in DelinquencyPlanWorkflowArray) {
      var delinquencyPlanWorkflowLoader = new BCDelinquencyPlanWorkflowLoader()
      delinquencyPlanWorkflowLoader.createEntity(delinquencyPlanWorkflow)
    }
  }

  function processChargePattern() {
    for (chargePattern in ChargePatternArray) {
      var chargePatternLoader = new BCChargePatternLoader()
      chargePatternLoader.createEntity(chargePattern)
    }
  }

  function processCommissionPlan() {
    for (commissionPlan in CommissionPlanArray) {
      var commissionPlanLoader = new BCCommissionPlanLoader()
      commissionPlanLoader.createCommissionPlanWithSubPlan(commissionPlan, CommissionSubPlanArray)
    }
  }

  function processCommissionSubPlan() {
    for (commissionSubPlan in CommissionSubPlanArray) {
      // Don't load 'default' sub plan; these are loaded with the commission plan
      if (commissionSubPlan.SubPlanName != "default") {
        var commissionSubPlanLoader = new BCCommissionSubPlanLoader()
        commissionSubPlanLoader.createEntity(commissionSubPlan)
      }
    }
  }

  function processPaymentAllocationPlan() {
    for (paymentAllocationPlan in PaymentAllocationPlanArray) {
      var loader = new PaymentAllocationPlanLoader ()
      loader.createEntity(paymentAllocationPlan)
    }
  }

  function processReturnPremiumPlan() {
    for (returnPremiumPlan in ReturnPremiumPlanArray) {
      var loader = new ReturnPremiumPlanLoader ()
      loader.createEntity(returnPremiumPlan)
    }
  }

  function processReturnPremiumScheme() {
    for (returnPremiumScheme in ReturnPremiumSchemeArray) {
      var loader = new ReturnPremiumSchemeLoader ()
      loader.createEntity(returnPremiumScheme)
    }
  }

  function processLocalization() {
    for (localization in LocalizationArray) {
      var loader = new BCPlanLocalizationLoader()
      loader.createEntity(localization)
    }
  }

  /**
   * US570 - Excel Data Loader Additions for custom GL Integration entities
   * 11/14/2014 Alvin Lee
   *
   * Method to process importing the GL Filters.
   */
  function processGLTransactionFilter() {
    for (glFilter in GLTransactionFilterArray){
      var glFilterLoader = new BCGLTransactionFilterLoader ()
      glFilterLoader.createEntity(glFilter)
    }
  }

  /**
   * US570 - Excel Data Loader Additions for custom GL Integration entities
   * 11/14/2014 Alvin Lee
   *
   * Method to process importing the GL Transaction Name Mappings.
   */
  function processGLTransactionNameMapping() {
    for (glTransactionMapping in GLTransactionNameMappingArray){
      var glTransactionMappingLoader = new BCGLTransactionNameMappingLoader ()
      glTransactionMappingLoader.createEntity(glTransactionMapping)
    }
  }

  /**
   * US570 - Excel Data Loader Additions for custom GL Integration entities
   * 11/14/2014 Alvin Lee
   *
   * Method to process importing the GL T-Account Name Mappings.
   */
  function processGLTAccountNameMapping() {
    for (glTAccountMapping in GLTAccountNameMappingArray){
      var glTAccountMappingLoader = new BCGLTAccountNameMappingLoader()
      glTAccountMappingLoader.createEntity(glTAccountMapping)
    }
  }

  private function importFile() {
    try {
      BCPlanExcelFileParser.importPlanFile(Path, this)
      if (!IsLoadingFromConfig) {
        TempFile.deleteOnExit()
      }
    } catch (e: Exception) {
      throw e.toString()
    }
  }
}

