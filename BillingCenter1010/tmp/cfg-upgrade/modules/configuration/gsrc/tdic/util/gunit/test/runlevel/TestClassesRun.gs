package tdic.util.gunit.test.runlevel

uses gw.api.system.server.Runlevel
uses java.util.TreeSet

abstract class TestClassesRun {

  public static var _runlevelsCalled : TreeSet<Runlevel> = new TreeSet<Runlevel>()

  static function clearStaticState() {
    _runlevelsCalled.clear()
  }

  private construct() {
  }

}
