package tdic.util.gunit.test.stubs

uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses gw.testharness.RunLevel
uses gw.api.system.server.Runlevel

@ServerTest
@RunLevel(Runlevel.NONE)
class TestNotAtEndOfClassName extends TestBase {

  function testNotRun() {
    TestBase.fail("NO tests in this class should run because the name of this class does not end with 'Test'.")
  }

}
