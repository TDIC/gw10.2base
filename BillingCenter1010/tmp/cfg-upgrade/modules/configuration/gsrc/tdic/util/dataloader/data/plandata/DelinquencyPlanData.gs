package tdic.util.dataloader.data.plandata

uses java.lang.Integer
uses java.math.BigDecimal
uses tdic.util.dataloader.data.ColumnDescriptor

class DelinquencyPlanData extends PlanData {
  // constants
  public static final var SHEET: String = "DelinquencyPlan"
  public static final var COL_PUBLIC_ID: int = 0
  public static final var COL_NAME: int = 1
  public static final var COL_EFFECTIVE_DATE: int = 2
  public static final var COL_EXPIRATION_DATE: int = 3
  public static final var COL_CANCELLATION_TARGET: int = 4
  public static final var COL_HOLD_INVOICING_ON_TARGETED_POLICIES: int = 5
  public static final var COL_GRACE_PERIOD_DAYS: int = 6
  public static final var COL_LATE_FEE: int = 7
  public static final var COL_REINSTATEMENT_FEE: int = 8
  public static final var COL_WRITE_OFF_THRESHOLD: int = 9
  public static final var COL_ENTER_DELINQUENCY_THRESHOLD_ACCOUNT: int = 10
  public static final var COL_ENTER_DELINQUENCY_THRESHOLD_POLICY: int = 11
  public static final var COL_CANCEL_POLICY: int = 12
  public static final var COL_EXIT_DELINQUENCY: int = 13
  public static final var COL_APPLICABLE_SEGMENTS: int = 14
  public static final var COL_CURRENCY: int = 15
  public static final var CD: ColumnDescriptor[] = {
      new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EFFECTIVE_DATE, "Effective Date", ColumnDescriptor.TYPE_DATE),
      new ColumnDescriptor(COL_EXPIRATION_DATE, "Expiration Date", ColumnDescriptor.TYPE_DATE),
      new ColumnDescriptor(COL_CANCELLATION_TARGET, "Cancellation Target", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_HOLD_INVOICING_ON_TARGETED_POLICIES, "Hold Invoicing on Targeted Policies", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_GRACE_PERIOD_DAYS, "Grace Period", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_LATE_FEE, "Late Fee", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_REINSTATEMENT_FEE, "Reinstatement Fee", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_WRITE_OFF_THRESHOLD, "Writeoff Threshold", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_ENTER_DELINQUENCY_THRESHOLD_ACCOUNT, "Enter Delinquency Threshold (Account)", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_ENTER_DELINQUENCY_THRESHOLD_POLICY, "Enter Delinquency Threshold (Policy)", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_CANCEL_POLICY, "Cancel Policy", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_EXIT_DELINQUENCY, "Exit Delinquency", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_APPLICABLE_SEGMENTS, "Applicable Segments", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CURRENCY, "Currency", ColumnDescriptor.TYPE_STRING)
  }
  construct() {
  }

  // properties
  var _CancellationTarget                     : CancellationTarget  as CancellationTarget
  var _HoldInvoicingOnTargetedPolicies        : Boolean             as HoldInvoicingOnTargetedPolicies
  var _GracePeriodDays                        : Integer             as GracePeriodDays
  var _LateFee                                : BigDecimal          as LateFee
  var _ReinstatementFee                       : BigDecimal          as ReinstatementFee
  var _WriteoffThreshold                      : BigDecimal          as WriteoffThreshold
  var _EnterDelinquencyThreshold_Account      : BigDecimal          as EnterDelinquencyThreshold_Account
  var _EnterDelinquencyThreshold_Policy       : BigDecimal          as EnterDelinquencyThreshold_Policy
  var _CancelPolicy                           : BigDecimal          as CancelPolicy
  var _ExitDelinquency                        : BigDecimal          as ExitDelinquency
  var _ApplicableSegments                     : ApplicableSegments  as ApplicableSegments
  var _Currency                               : Currency            as Currency
}
