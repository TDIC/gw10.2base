package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.processor.DataLoaderProcessor
uses tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
uses tdic.util.dataloader.parser.ExcelFileParser
uses java.io.FileInputStream
uses org.apache.poi.ss.usermodel. *
uses java.lang.Exception
uses tdic.util.dataloader.data.plandata.AgencyBillPlanData
uses tdic.util.dataloader.data.plandata.BillingPlanData
uses tdic.util.dataloader.data.plandata.PaymentPlanData
uses tdic.util.dataloader.data.plandata.DelinquencyPlanData
uses tdic.util.dataloader.data.plandata.CommissionPlanData
uses tdic.util.dataloader.data.plandata.CommissionSubPlanData
uses tdic.util.dataloader.data.plandata.ChargePatternData
uses tdic.util.dataloader.data.plandata.PaymentPlanOverrideData
uses tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData
uses tdic.util.dataloader.data.plandata.ReturnPremiumPlanData
uses tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData
uses tdic.util.dataloader.data.plandata.PaymentAllocationPlanData
uses tdic.util.dataloader.data.plandata.GLTransactionFilterData
uses tdic.util.dataloader.data.plandata.GLTransactionNameMappingData
uses tdic.util.dataloader.data.plandata.GLTAccountNameMappingData

class BCPlanExcelFileParser extends ExcelFileParser {
  construct() {
  }

  public static function importPlanFile(filePath: String, dataLoaderHelper: DataLoaderProcessor): DataLoaderProcessor {
    try
    {
      var fis = new FileInputStream(filePath)
      var wb = WorkbookFactory.create(fis)
      var billingPlanSheet = getSheet(wb, BillingPlanData.SHEET)
      var agencyBillPlanSheet = getSheet(wb, AgencyBillPlanData.SHEET)
      var paymentPlanSheet = getSheet(wb, PaymentPlanData.SHEET)
      var paymentPlanOverridesSheet = getSheet(wb, PaymentPlanOverrideData.SHEET)
      var delinquencyPlanSheet = getSheet(wb, DelinquencyPlanData.SHEET)
      var delinquencyPlanWorkflowSheet = getSheet(wb, DelinquencyPlanWorkflowData.SHEET)
      var commissionPlanSheet = getSheet(wb, CommissionPlanData.SHEET)
      var commissionSubPlanSheet = getSheet(wb, CommissionSubPlanData.SHEET)
      var chargePatternSheet = getSheet(wb, ChargePatternData.SHEET)
      var paymentAllocationPlanSheet = getSheet(wb, PaymentAllocationPlanData.SHEET)
      var returnPremiumPlanSheet = getSheet(wb, ReturnPremiumPlanData.SHEET)
      var returnPremiumSchemeSheet = getSheet(wb, ReturnPremiumSchemeData.SHEET)
      var bcPlanDataLoaderHelper = dataLoaderHelper as BCPlanDataLoaderProcessor
      /**
       * US570 - Excel Data Loader Additions for custom GL Integration entities
       * 11/14/2014 Alvin Lee
       */
      var glFilterSheet = getSheet(wb, GLTransactionFilterData.SHEET)
      var glMappingSheet = getSheet(wb, GLTransactionNameMappingData.SHEET)
      var glTAccountMappingSheet = getSheet(wb, GLTAccountNameMappingData.SHEET)
      /* Parse Direct Bill Plans */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.BillingPlanArray, new BillingPlanWorksheetParser().parseSheet(billingPlanSheet))
      /* Parse Agency Bill Plans */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.AgencyBillPlanArray, new AgencyBillPlanWorksheetParser().parseSheet(agencyBillPlanSheet))
      /* Parse Payment Plans */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.PaymentPlanArray, new PaymentPlanWorksheetParser().parseSheet(paymentPlanSheet))
      /* Parse Payment Plan Overrides */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.PaymentPlanOverrideArray, new PaymentPlanOverrideWorksheetParser().parseSheet(paymentPlanOverridesSheet))
      /* Parse Delinquency Plans */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.DelinquencyPlanArray, new DelinquencyPlanWorksheetParser().parseSheet(delinquencyPlanSheet))
      /* Parse Delinquency Workflow Events */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.DelinquencyPlanWorkflowArray, new DelinquencyPlanWorkflowWorksheetParser().parseSheet(delinquencyPlanWorkflowSheet))
      /* Parse Commission Plans */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.CommissionPlanArray, new CommissionPlanWorksheetParser().parseSheet(commissionPlanSheet))
      /* Parse Commission Default SubPlans stored in the Commission Plan sheet */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.CommissionSubPlanArray, new CommissionDefaultSubPlanWorksheetParser().parseSheet(commissionPlanSheet))
      /* Parse Conditional Commission SubPlans */
      // don't clear array -- Default SubPlans are loaded in the previous section
      bcPlanDataLoaderHelper.CommissionSubPlanArray.addAll(new CommissionSubPlanWorksheetParser().parseSheet(commissionSubPlanSheet))
      /* Parse Charge Patterns */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.ChargePatternArray, new ChargePatternWorksheetParser().parseSheet(chargePatternSheet))
      /* Parse Payment Allocation Plans */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.PaymentAllocationPlanArray, new PaymentAllocationPlanWorksheetParser().parseSheet(paymentAllocationPlanSheet))
      /* Parse Return Premium Plans */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.ReturnPremiumPlanArray, new ReturnPremiumPlanWorksheetParser().parseSheet(returnPremiumPlanSheet))
      /* Parse Return Premium Schemes */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.ReturnPremiumSchemeArray, new ReturnPremiumSchemeWorksheetParser().parseSheet(returnPremiumSchemeSheet))
      /**
       * US570 - Excel Data Loader Additions for custom GL Integration entities
       * 11/14/2014 Alvin Lee
       */
      /* Parse GL Filters */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.GLTransactionFilterArray, new GLTransactionFilterWorksheetParser ().parseSheet(glFilterSheet))
      /* Parse GL Mappings */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.GLTransactionNameMappingArray, new GLTransactionNameMappingWorksheetParser ().parseSheet(glMappingSheet))
      /* Parse GL T-Account Name Mappings */
      replaceDataWithParsedArray(bcPlanDataLoaderHelper.GLTAccountNameMappingArray, new GLTAccountNameMappingWorksheetParser().parseSheet(glTAccountMappingSheet))
    }
        catch (e: Exception)
        {
          throw "Error occurred while importing the file and parsing : " + e.toString()
        }
    return dataLoaderHelper
  }
}
