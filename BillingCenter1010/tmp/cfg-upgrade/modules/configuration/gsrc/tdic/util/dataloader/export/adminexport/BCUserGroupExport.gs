package tdic.util.dataloader.export.adminexport

uses org.apache.poi.ss.usermodel.Cell
uses java.util.HashMap
uses tdic.util.dataloader.util.DataLoaderUtil
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.admindata.UserGroupData
uses java.util.Collection

class BCUserGroupExport extends WorksheetExportUtil {
  construct(uCol: Collection <User>, gCol: Collection <Group>) {
    Users = uCol
    Groups = gCol
  }
  var _users: Collection <User> as Users
  var _groups: Collection <Group> as Groups
  // get sheet name

  public static function getSheetName(): String {
    return UserGroupData.SHEET
  }

  /* @return : HashMap containing the header name and values of user group entity
     @desc   : Method to fetch the user group entity values
  */

  public function getUserGroupEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getUserHeaderNames(), Users, \e -> getUserGroupRowFromUser(e))
  }

  // build column map from entity

  public function getUserGroupRowFromUser(user: User): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(UserGroupData.COL_USER_REFERENCE_NAME), user.DisplayName + " (" + user.Credential + ")")
    colMap.put(colIndexString(UserGroupData.COL_USER_PUBLIC_ID), user.PublicID)
    for (group in Groups index i) {
      var uGroup = user.GroupUsers.firstWhere(\g -> g.Group == group)
      if (uGroup != null) {
        var code: String
        if (uGroup.Manager == true) {
          code = ":M"
          if (uGroup.LoadFactorType == TC_LOADFACTORADMIN) {
            code = ":B"
          }
        } else if (uGroup.LoadFactorType == TC_LOADFACTORADMIN) {
          code = ":A"
        }
        if (uGroup.LoadFactor != null) {
          colMap.put(colIndexString(UserGroupData.COL_GROUP1 + i), uGroup.LoadFactor.toString() + (code == null ? "" : code))
        } else {
          colMap.put(colIndexString(UserGroupData.COL_GROUP1 + i), DataLoaderUtil.getExportLoadFactor() + (code == null ? "" : code))
        }
      } else {
        // not assigned to group
        colMap.put(colIndexString(UserGroupData.COL_GROUP1 + i), "")
      }
    }
    return colMap
  }

  /* @return : HashMap containing the user group entity names
     @desc   : Method to fetch the header name for the user group export sheet
  */

  public function getUserHeaderNames(): HashMap {
    var headerMap = new HashMap()
    // headers of fixed columns
    var fixed_colHeaders = UserGroupData.CD*.ColHeader
    for (colHeader in fixed_colHeaders index i) {
      headerMap.put(colIndexString(i), colHeader)
    }
    // headers of dynamic columns
    for (group in Groups index i) {
      headerMap.put(colIndexString(UserGroupData.COL_GROUP1 + i), group.Name)
    }
    return headerMap
  }

  /* @return : HashMap containing the type of each column in user group entity
     @desc   : Method to fetch the type of each column in user group entity. 
               Used to create cell type during excel export  
  */

  public function getUserGroupColumnTypes(): HashMap {
    var columnTypeMap = new HashMap()
    // types of fixed columns
    var fixed_colTypes = UserGroupData.CD*.ColType
    for (colType in fixed_colTypes index i) {
      columnTypeMap.put(i, colType)
    }
    // types of dynamic columns
    for (i in 0..|Groups.Count) {
      columnTypeMap.put(UserGroupData.COL_GROUP1 + i, Cell.CELL_TYPE_STRING)
    }
    return columnTypeMap
  }
}
