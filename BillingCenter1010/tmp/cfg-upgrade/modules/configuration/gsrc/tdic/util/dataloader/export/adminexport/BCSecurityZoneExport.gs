package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.admindata.SecurityZoneData
uses java.util.Collection
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCSecurityZoneExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return SecurityZoneData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getSecZoneEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(SecurityZoneData.CD), querySecZones(), \e -> getRowFromSecZone(e))
  }

  // query data

  public static function querySecZones(): Collection <SecurityZone> {
    var result = Query.make(SecurityZone).select().orderBy(
      QuerySelectColumns.path(Paths.make(entity.SecurityZone#PublicID))
    )
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromSecZone(secZone: SecurityZone): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(SecurityZoneData.COL_NAME), secZone.Name)
    colMap.put(colIndexString(SecurityZoneData.COL_PUBLIC_ID), secZone.PublicID)
    colMap.put(colIndexString(SecurityZoneData.COL_DESCRIPTION), secZone.Description)
    colMap.put(colIndexString(SecurityZoneData.COL_EXCLUDE), "false")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getSecZoneColumnTypes(): HashMap {
    return getColumnTypes(SecurityZoneData.CD)
  }
}
