package tdic.util.dataloader.export.adminexport

uses tdic.util.dataloader.export.ExcelExport
uses java.util.HashMap
uses gw.api.util.DisplayableException
uses tdic.util.dataloader.util.DataLoaderUtil
uses java.lang.Exception
uses org.slf4j.LoggerFactory

class BCAdminExport {
  public static final var LOG: org.slf4j.Logger = LoggerFactory.getLogger("DataLoader.Export")
  construct() {
  }

  // Method to export and download data

  public static function exportData() {
    exportDataToFile(DataLoaderUtil.getExportAdminDatafilePath(), true)
  }

  // Method to export data

  public static function exportDataToFile(filePath: String, download: boolean) {
    var excelExport = new ExcelExport(filePath)
    var sheetMap = retrieveData()
    try {
      excelExport.write(sheetMap)
      if (download) excelExport.download()
    } catch (e: Exception) {
      var msg = "BCAdminExport: Exception while exporting data: "
      LOG.error(msg, e)
      throw new DisplayableException(msg + e)
    }
  }

  // Method to retrieve data

  public static function retrieveData(): HashMap {
    LOG.info("BCAdminExport: Retrieving data started ***")
    var sheetMap = new HashMap()
    //Get Role Values
    var roles = BCRoleExport.queryRoles()
    var roleMap = new HashMap()
    var roleExport = new BCRoleExport()
    roleMap.put(roleExport.getRoleEntityValues(), roleExport.getRoleColumnTypes())
    sheetMap.put(roleExport.getSheetName(), roleMap)
    // Get Role Privilege
    var rpMap = new HashMap()
    var rolePrivilegeExport = new BCRolePrivilegeExport(roles)
    rpMap.put(rolePrivilegeExport.getRolePrivilegeEntityValues(), rolePrivilegeExport.getRolePrivilegeColumnTypes())
    sheetMap.put(rolePrivilegeExport.getSheetName(), rpMap)
    // Get Activity Pattern data
    var actPatternMap = new HashMap()
    var actPatternExport = new BCActivityPatternExport()
    actPatternMap.put(actPatternExport.getActPatternEntityValues(), actPatternExport.getActPatternColumnTypes())
    sheetMap.put(actPatternExport.getSheetName(), actPatternMap)
    // Get security pattern data
    var secZoneMap = new HashMap()
    var secZoneExport = new BCSecurityZoneExport()
    secZoneMap.put(secZoneExport.getSecZoneEntityValues(), secZoneExport.getSecZoneColumnTypes())
    sheetMap.put(secZoneExport.getSheetName(), secZoneMap)
    // Get Authority limit profile data
    var authLmtProfileMap = new HashMap()
    var authLimitProfileExport = new BCAuthorityLimitProfileExport()
    authLmtProfileMap.put(authLimitProfileExport.getAuthLimitProfileEntityValues(), authLimitProfileExport.getAuthLimitProfileColumnTypes())
    sheetMap.put(authLimitProfileExport.getSheetName(), authLmtProfileMap)
    // Get Authority limit data
    var authLmtMap = new HashMap()
    var authLimitExport = new BCAuthorityLimitExport()
    authLmtMap.put(authLimitExport.getAuthLimitEntityValues(), authLimitExport.getAuthLimitColumnTypes())
    sheetMap.put(authLimitExport.getSheetName(), authLmtMap)
    // Get Address data
    var addressMap = new HashMap()
    var addressExport = new BCAddressExport()
    addressMap.put(addressExport.getAddressEntityValues(), addressExport.getAddressColumnTypes())
    sheetMap.put(addressExport.getSheetName(), addressMap)
    // Get user data
    var users = BCUserExport.queryUsers()
    var userMap = new HashMap()
    var userExport = new BCUserExport()
    userMap.put(userExport.getUserEntityValues(), userExport.getUserColumnTypes())
    sheetMap.put(userExport.getSheetName(), userMap)
    // Get Group data
    var groups = BCGroupExport.queryGroups()
    var groupMap = new HashMap()
    var groupExport = new BCGroupExport()
    groupMap.put(groupExport.getGroupEntityValues(), groupExport.getGroupColumnTypes())
    sheetMap.put(groupExport.getSheetName(), groupMap)
    // Get Region data
    var regionMap = new HashMap()
    var regionExport = new BCRegionExport()
    regionMap.put(regionExport.getRegionEntityValues(), regionExport.getRegionColumnTypes())
    sheetMap.put(regionExport.getSheetName(), regionMap)
    // Get Attribute data
    var attributeMap = new HashMap()
    var attributeExport = new BCAttributeExport()
    attributeMap.put(attributeExport.getAttributeEntityValues(), attributeExport.getAttributeColumnTypes())
    sheetMap.put(attributeExport.getSheetName(), attributeMap)
    // Get User authority limit data
    var userAtLmt = new HashMap()
    var userAuthLimitExport = new BCUserAuthorityLimitExport(users)
    userAtLmt.put(userAuthLimitExport.getUserAuthLimitEntityValues(), userAuthLimitExport.getUserAuthLimitColumnTypes())
    sheetMap.put(userAuthLimitExport.getSheetName(), userAtLmt)
    // Get User Group data
    var userGroupLmt = new HashMap()
    var userGroupExport = new BCUserGroupExport(users, groups)
    userGroupLmt.put(userGroupExport.getUserGroupEntityValues(), userGroupExport.getUserGroupColumnTypes())
    sheetMap.put(userGroupExport.getSheetName(), userGroupLmt)
    // Get Queue data
    var queueMap = new HashMap()
    var queueExport = new BCQueueExport()
    queueMap.put(queueExport.getQueueEntityValues(), queueExport.getQueueColumnTypes())
    sheetMap.put(queueExport.getSheetName(), queueMap)
    // Get Holiday data
    var holidayMap = new HashMap()
    var holidayExport = new BCHolidayExport()
    holidayMap.put(holidayExport.getHolidayEntityValues(), holidayExport.getHolidayColumnTypes())
    sheetMap.put(holidayExport.getSheetName(), holidayMap)
    LOG.info("BCAdminExport: Retrieving data completed ***")
    return sheetMap
  }
}
