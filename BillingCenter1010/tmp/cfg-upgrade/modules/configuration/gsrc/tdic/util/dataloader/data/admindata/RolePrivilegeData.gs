package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class RolePrivilegeData extends ApplicationData {

  // constants
  public static final var SHEET : String = "RolePrivilege"
  public static final var COL_ROLE : int = 0
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_ROLE, "Role", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }
  
}
