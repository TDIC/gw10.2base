package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses java.util.ArrayList
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.sampledata.BatchData
uses gw.util.GWBaseDateEnhancement

class BatchWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var RUN_DATE: int = 1
  var BATCHNAME: int = 2
  function parseBatchRow(row: Row): BatchData {
    var batchData = new BatchData()
    // Populate DirectPayment data
    batchData.BatchName = convertCellToString(row.getCell(BATCHNAME))
    if (batchData.BatchName.Empty || batchData.BatchName == null)
      return null
    batchData.BatchRunDate = row.getCell(RUN_DATE).DateCellValue
    //batchData.BatchRunDate = convertCellToString(row.getCell(RUN_DATE)) as java.util.Date
    batchData.EntryDate = row.getCell(RUN_DATE).DateCellValue
    if (batchData.BatchRunDate == null  or batchData.BatchRunDate < gw.api.util.DateUtil.currentDate()) {
      batchData.BatchRunDate = gw.api.util.DateUtil.currentDate()
      batchData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    return batchData
  }

  override function parseSheet(sheet: Sheet): ArrayList <BatchData> {
    return parseSheet(sheet, \r -> parseBatchRow(r), false)
  }
}