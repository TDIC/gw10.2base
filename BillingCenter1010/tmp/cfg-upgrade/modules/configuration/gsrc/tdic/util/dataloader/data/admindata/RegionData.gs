package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class RegionData extends ApplicationData {

  // constants
  public static final var SHEET : String = "Region"
  public static final var COL_NAME : int = 0
  public static final var COL_PUBLIC_ID : int = 1
  public static final var COL_ZONES : int = 2
  public static final var COL_EXCLUDE : int = 3  
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ZONES, "Zones", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING)
  }
  
  construct() {

  }
  
  //properties
  private var _name                    : String                       as Name
  private var _zones                   : String                       as Zones
}
