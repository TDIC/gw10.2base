package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.UserData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses gw.command.demo.GeneralUtil
uses java.util.ArrayList
uses java.lang.Exception

class BCUserLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aUserData = applicationData as UserData
    try {
      var currUser = GeneralUtil.findUserByUserName(aUserData.UserName)
      // add or update if Exclude is false
      if (aUserData.Exclude != true) {
        if (currUser == null) {
          // add if not on file
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            var aUser = new User()
            // set User fields
            aUser.PublicID = aUserData.PublicID
            aUser.JobTitle = aUserData.JobTitle
            aUser.Department = aUserData.Department
            aUser.ExternalUser = aUserData.ExternalUser
            aUser.Language = typekey.LanguageType.get(aUserData.Language)
            // create new Credentials and connect to User
            var aCredential = new Credential()
            aCredential.Active = aUserData.Active
            aCredential.UserName = aUserData.UserName
            aCredential.Password = aUserData.Password
            aUser.Credential = aCredential
            /**
             * US436 - All users loaded through the Excel Data Loader should have the startup page default to Desktop.
             * 1/22/2015 Alvin Lee
             */
            // create new UserSettings and connect to User
            var aSettings = new UserSettings()
            aSettings.StartupPage = StartupPage.TC_DESKTOPGROUP
            aUser.UserSettings = aSettings
            // add up to three UserRegions
            addRegion(aUserData.Region1.Name, aUser)
            addRegion(aUserData.Region2.Name, aUser)
            addRegion(aUserData.Region3.Name, aUser)
            // build and add UserContact with Address
            var aUserContact = convertUserContactToBC(aUserData.Contact)
            if (!aUserContact.New)  {
              aUserContact = bundle.add(aUserContact)
            }
            // found UserContact won't be in bundle; put it there
            aUser.Contact = aUserContact
            if (aUserData.Role1.PublicID != null) {
              var aUserRole1 = new UserRole()
              aUserRole1.Role = BCAdminDataLoaderUtil.findRoleByName(aUserData.Role1.Name)
              aUser.addToRoles(aUserRole1)
            }  if (aUserData.Role2.PublicID != null) {
              var aUserRole2 = new UserRole()
              aUserRole2.Role =  BCAdminDataLoaderUtil.findRoleByName(aUserData.Role2.Name)
              aUser.addToRoles(aUserRole2)
            }  if (aUserData.Role3.PublicID != null) {
              var aUserRole3 = new UserRole()
              aUserRole3.Role =  BCAdminDataLoaderUtil.findRoleByName(aUserData.Role3.Name)
              aUser.addToRoles(aUserRole3)
            }  if (aUserData.Role4.PublicID != null) {
              var aUserRole4 = new UserRole()
              aUserRole4.Role =  BCAdminDataLoaderUtil.findRoleByName(aUserData.Role4.Name)
              aUser.addToRoles(aUserRole4)
            }
            if (aUserData.Role5.PublicID != null) {
              var aUserRole5 = new UserRole()
              aUserRole5.Role =  BCAdminDataLoaderUtil.findRoleByName(aUserData.Role5.Name)
              aUser.addToRoles(aUserRole5)
            }
            if (aUserData.Role6.PublicID != null) {
              var aUserRole6 = new UserRole()
              aUserRole6.Role =  BCAdminDataLoaderUtil.findRoleByName(aUserData.Role6.Name)
              aUser.addToRoles(aUserRole6)
            }
            if (aUserData.Role7.PublicID != null) {
              var aUserRole7 = new UserRole()
              aUserRole7.Role =  BCAdminDataLoaderUtil.findRoleByName(aUserData.Role7.Name)
              aUser.addToRoles(aUserRole7)
            }
             bundle.commit()  //-- this is handled in api call
          }, User.util.UnrestrictedUser)
          aUserData.Skipped = false
        } else {
          // Update the user record if already existing in the system.
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            var aUser = bundle.add(currUser)
            aUser.PublicID = aUserData.PublicID
            aUser.JobTitle = aUserData.JobTitle
            aUser.Department = aUserData.Department
            aUser.ExternalUser = aUserData.ExternalUser
            aUser.Language = typekey.LanguageType.get(aUserData.Language)
            aUser.Credential.Active = aUserData.Active
            aUser.Credential.UserName = aUserData.UserName
            // automatic password change not supported
            // aUser.Credential.Password = aUserData.Password
            var newRegion = new ArrayList <Region> ()
            var existingRegion = new ArrayList <Region>()
            // Get the list of regions from the excel loader
            if (aUserData.Region1.Name != null) {
              newRegion.add(BCAdminDataLoaderUtil.findRegionByName(aUserData.Region1.Name))
            }
            if (aUserData.Region2.Name != null) {
              newRegion.add(BCAdminDataLoaderUtil.findRegionByName(aUserData.Region2.Name))
            }
            if (aUserData.Region3.Name != null) {
              newRegion.add(BCAdminDataLoaderUtil.findRegionByName(aUserData.Region3.Name))
            }
            existingRegion.addAll(aUser.Regions*.Region?.toList() as ArrayList <Region>)
            var addedRegions = newRegion.subtract(existingRegion)
            var removedRegions = existingRegion.subtract(newRegion)
            // Add any new regions
            for (addRegion in addedRegions.where(\r -> r != null)) {
              var aUserRegion = new UserRegion()
              aUserRegion.Region = addRegion
              aUser.addToRegions(aUserRegion)
            }
            // Remove existing regions if they are not in the loader file
            for (remRegion in removedRegions) {
              var aUserRegion = aUser.Regions.firstWhere(\g -> g.Region == remRegion)
              aUser.removeFromRegions(aUserRegion)
            }
            // build and add UserContact with Address
            var aUserContact = convertUserContactToBC(aUserData.Contact)
            if (!aUserContact.New)  {
              aUserContact = bundle.add(aUserContact)
            }
            // found UserContact won't be in bundle; put it there
            aUser.Contact = aUserContact
            // Create the updated roles
            var newRoles = new ArrayList <Role> ()
            var existingRoles = new ArrayList <Role> ()
            if (aUserData.Role1.Name != null) {
              newRoles.add(BCAdminDataLoaderUtil.findRoleByName(aUserData.Role1.Name))
            }
            if (aUserData.Role2.Name != null) {
              newRoles.add(BCAdminDataLoaderUtil.findRoleByName(aUserData.Role2.Name))
            }
            if (aUserData.Role3.Name != null) {
              newRoles.add(BCAdminDataLoaderUtil.findRoleByName(aUserData.Role3.Name))
            }
            if (aUserData.Role4.Name != null) {
              newRoles.add(BCAdminDataLoaderUtil.findRoleByName(aUserData.Role4.Name))
            }
            if (aUserData.Role5.Name != null) {
              newRoles.add(BCAdminDataLoaderUtil.findRoleByName(aUserData.Role5.Name))
            }
            if (aUserData.Role6.Name != null) {
              newRoles.add(BCAdminDataLoaderUtil.findRoleByName(aUserData.Role6.Name))
            }
            if (aUserData.Role7.Name != null) {
              newRoles.add(BCAdminDataLoaderUtil.findRoleByName(aUserData.Role7.Name))
            }
            existingRoles.addAll(aUser.Roles*.Role?.toList() as ArrayList <Role>)
            var addedRoles = newRoles.subtract(existingRoles)
            var removedRoles = existingRoles.subtract(newRoles)
            // Add any new roles to the user
            for (addRole in addedRoles.where(\r -> r != null)) {
              var aUserRole = new UserRole()
              aUserRole.Role = addRole
              aUser.addToRoles(aUserRole)
            }
            // Remove roles from users
            for (remRole in removedRoles) {
              var aUserRole = aUser.Roles.firstWhere(\g -> g.Role == remRole)
              aUser.removeFromRoles(aUserRole)
            }
            aUserData.Skipped = false
            if (aUser.getChangedFields().Count > 0 or aUser.UserSettings.ChangedFields.Count > 0) {
              aUserData.Updated = true
            } else {
              aUserData.Unchanged = true
            }
            bundle.commit()
          }, User.util.UnrestrictedUser)
        }
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("User Load: " + aUserData.PublicID + e.toString())
      }
      aUserData.Error = true
    }
  }

  function addRegion(aRegion: String, user: User) {
    var region = BCAdminDataLoaderUtil.findRegionByName(aRegion)
    if (region != null) {
      var aUserRegion = new UserRegion()
      aUserRegion.User = user
      aUserRegion.Region = region
      user.addToRegions(aUserRegion)
    }
  }
}
