package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class CollectionAgencyData extends ApplicationData {
  // constants
  public static final var SHEET : String = "CollectionAgencies"
  public static final var COL_NAME : int = 0
  public static final var COL_PUBLIC_ID : int = 1
  public static final var COL_PHONE : int = 2
  public static final var COL_FAX : int = 3
  public static final var COL_EMAIL_ADDRESS : int = 4
  public static final var COL_ADDRESS : int = 5
  public static final var COL_EXCLUDE : int = 6

  public static final var CD : ColumnDescriptor[] = {
      new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PUBLIC_ID, "Public Id", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PHONE, "Phone", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_FAX, "Fax", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EMAIL_ADDRESS, "Email Address", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ADDRESS, "Address", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING)  }

  construct() {}

  private var _name    : String      as Name
  private var _phone   : String      as Phone
  private var _fax     : String      as Fax
  private var _email   : String      as Email
  private var _address : AddressData as Address}

