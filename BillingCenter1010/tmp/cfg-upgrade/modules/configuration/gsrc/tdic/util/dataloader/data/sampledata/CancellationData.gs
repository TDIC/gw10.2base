package tdic.util.dataloader.data.sampledata

class CancellationData extends ExistingPolicyData {

  construct() {

  }
  private var _cancellationreason         : String           as CancellationReason
  private var _cancellationtype           : CancellationType as CancellationType
  private var _holdunbilledpremiumcharges : Boolean          as HoldUnbilledPremiumCharges
}
