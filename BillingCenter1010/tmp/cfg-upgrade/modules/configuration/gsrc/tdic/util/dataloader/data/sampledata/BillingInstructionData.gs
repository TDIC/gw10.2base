package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData

class BillingInstructionData extends ApplicationData{

  construct() {

  } 
  private var _charges                    : ChargeData[]  as   Charges
  private var _description                : String        as   Description
}
