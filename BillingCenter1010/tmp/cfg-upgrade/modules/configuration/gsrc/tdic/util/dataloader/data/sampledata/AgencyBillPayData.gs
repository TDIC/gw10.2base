package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData
uses java.util.Date
uses java.math.BigDecimal

class AgencyBillPayData extends ApplicationData {
  construct() {
  }
  private var _receivedDate       : Date           as ReceivedDate
  private var _producer           : String         as Producer
  private var _prodCode           : String         as ProdCode
  private var _statementId        : String         as StatementID
  private var _description        : String         as Description
  private var _referencenumber    : String         as ReferenceNumber
  private var _paymentMethod      : PaymentMethod  as PaymentMethod
  private var _paymenttoken       : String         as PaymentToken
  private var _amount             : BigDecimal     as Amount
  private var _grossAmount        : BigDecimal[]   as GrossAmounts
  private var _commAmount         : BigDecimal[]   as CommissionAmounts
}
