package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PCProducerInfo_PrimaryContact
uses gw.webservice.policycenter.bc1000.entity.types.complex.NewProducerCodeInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewProducerInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCProducerInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.ProducerCodeInfo
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.ProducerData
uses gw.command.demo.GeneralUtil
uses gw.api.util.DateUtil
uses gw.transaction.Transaction
uses java.lang.Exception
uses gw.api.database.Query
uses gw.api.database.InOperation
uses gw.api.database.Relop

class BCProducerLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }
  var _commissionPlan: CommissionPlan as ProducerCommissionPlan
  var _agencyBillPlan: AgencyBillPlan as ProducerAgencyBillPlan
  var _billingRep: User as BillingRep
  var _producer: Producer as NewProducer
  var _BCContact: Contact as NewBCContact

  override function createEntity(applicationData: ApplicationData) {
    var producerData = applicationData as ProducerData
    NewBCContact = convertContactToBC(producerData.Contact)
    var producerName = producerData.ProducerName
    ProducerAgencyBillPlan = GeneralUtil.findAgencyBillPlanByName(producerData.AgencyBillPlan)
    ProducerCommissionPlan = GeneralUtil.findCommissionPlanByName(producerData.CommissionPlan)
    BillingRep = GeneralUtil.findUserByUserName(producerData.AccountRep)
    NewProducer = GeneralUtil.findProducerByName(producerName)
    //Only create new producer if name is not in use
    if (NewProducer == null) {
      createProducer(producerData)
    } else {
      updateProducer(producerData)
    }
  }

  private function createProducer(producerData: ProducerData) {
    try {
      Transaction.runWithNewBundle(\bundle -> {
        // Producer Details
        var producerInfo = createNewProducerInfo(producerData)
        NewProducer = producerInfo.toNewProducer(bundle)
        NewProducer.AccountRep = BillingRep
        NewProducer.AgencyBillPlan = ProducerAgencyBillPlan

        // Create the producer code
        var codeInfo = new NewProducerCodeInfo()
        codeInfo.Code = producerData.ProducerCode
        codeInfo.Active = producerData.Active
        codeInfo.ProducerPublicID = NewProducer.PublicID
        var commissionPlansMap = new HashMap<typekey.Currency, entity.CommissionPlan>()
        commissionPlansMap.put(typekey.Currency.TC_USD, ProducerCommissionPlan)
        var newCode = codeInfo.createNewProducerCode(NewProducer, NewProducer.Currency, commissionPlansMap, bundle)
        if (LOG.DebugEnabled) {
          LOG.debug("committed bundle: " + NewProducer.Bundle + "\n")
        }
        LOG.info("New Producer: " + NewProducer + "\n" + " ProducerCode: " + newCode + "\n")
        producerData.Skipped = false
      })
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Producer Load: " + producerData.ProducerName + e.toString())
      }
      producerData.Error = true
    }
  }

  private function updateProducer(producerData: ProducerData) {
    try {
      Transaction.runWithNewBundle(\bundle -> {
        // Producer Details
        var producerInfo = createProducerInfo(producerData)
        NewProducer = producerInfo.toProducer(bundle)
        NewProducer.AccountRep = BillingRep
        NewProducer.AgencyBillPlan = ProducerAgencyBillPlan

        // add/update producer codes
        var api = new gw.webservice.policycenter.bc1000.BillingAPI()
        if (api.isProducerCodeExist(NewProducer.PublicID, producerData.ProducerCode)) {
          //update existing producer code
          var codeInfo = new ProducerCodeInfo()
          codeInfo.PublicID = Query.make(ProducerCode).compare(ProducerCode#Code, Equals, producerData.ProducerCode).select().first().PublicID
          codeInfo.Code = producerData.ProducerCode
          codeInfo.Active = producerData.Active
          codeInfo.toProducerCode(bundle)
        } else {
          //add new producer code
          var codeInfo = new NewProducerCodeInfo()
          codeInfo.Code = producerData.ProducerCode
          codeInfo.Active = producerData.Active
          codeInfo.ProducerPublicID = NewProducer.PublicID
          var commissionPlansMap = new HashMap<typekey.Currency, entity.CommissionPlan>()
          commissionPlansMap.put(typekey.Currency.TC_USD, ProducerCommissionPlan)
          var newCode = codeInfo.createNewProducerCode(NewProducer, NewProducer.Currency, commissionPlansMap, bundle)
        }
      })
    } catch (e:Exception) {
    }
  }

  private function createNewProducerInfo(producerData : ProducerData) : PCNewProducerInfo {
    var producerInfo = new PCNewProducerInfo()
    producerInfo.ProducerName = producerData.ProducerName
    producerInfo.Tier = producerData.Tier != null ? producerData.Tier : "gold"
    producerInfo.PreferredCurrency = "usd"
    producerInfo.PrimaryContact = BCContactToPCProducerInfo_PrimaryContact(NewBCContact)
    return producerInfo
  }

  private function createProducerInfo(producerData : ProducerData) : PCProducerInfo {
    var producerInfo = new PCProducerInfo()
    producerInfo.PublicID = NewProducer.PublicID
    producerInfo.ProducerName = producerData.ProducerName
    producerInfo.Tier = producerData.Tier != null ? producerData.Tier : "gold"
    producerInfo.PrimaryContact = BCContactToPCProducerInfo_PrimaryContact(NewBCContact)
    return producerInfo
  }

  private function BCContactToPCProducerInfo_PrimaryContact(c:Contact) : PCProducerInfo_PrimaryContact {
    var contactInfo = new PCProducerInfo_PrimaryContact()
    var pcContactInfo = BCContactToPCContactInfo(c)
    contactInfo.AddressBookUID = pcContactInfo.AddressBookUID
    contactInfo.ContactType = pcContactInfo.ContactType
    contactInfo.FirstName = pcContactInfo.FirstName
    contactInfo.LastName = pcContactInfo.LastName
    contactInfo.Name = pcContactInfo.Name
    contactInfo.EmailAddress1 = pcContactInfo.EmailAddress1
    contactInfo.WorkPhone = pcContactInfo.WorkPhone
    contactInfo.WorkPhoneCountry = pcContactInfo.WorkPhoneCountry
    contactInfo.WorkPhoneExtension = pcContactInfo.WorkPhoneExtension
    //contactInfo.Addresses = pcContactInfo.Addresses
    return contactInfo
  }
}
