package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.ChargePatternData
uses tdic.util.dataloader.util.BCPlanDataLoaderUtil
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCChargePatternLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aChargePatternData = applicationData as ChargePatternData
    try {
      var currChargePattern = GeneralUtil.findChargePatternByChargeCode(aChargePatternData.ChargeCode)
      // add or update if Exclude is false
      if (aChargePatternData.Exclude != true){
        var tAccount = BCPlanDataLoaderUtil.findTAccountOwnerPatternByName(aChargePatternData.TAccount_Owner)
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          if (currChargePattern == null) {
            // create new
            // create based on subtype (Charge Type)
            switch (aChargePatternData.ChargeType) {
              case "ProRataCharge":
                  var aProRataCharge = new ProRataCharge()
                  aProRataCharge.PublicID = aChargePatternData.PublicID
                  aProRataCharge.ChargeCode = aChargePatternData.ChargeCode
                  aProRataCharge.ChargeName = aChargePatternData.ChargeName
                  aProRataCharge.Category = aChargePatternData.Category
                  aProRataCharge.TAccountOwnerPattern = tAccount
                  aProRataCharge.InvoiceTreatment = aChargePatternData.InvoicingApproach
                  aProRataCharge.Priority = aChargePatternData.Priority
                  aProRataCharge.IncludedInEquityDating = aChargePatternData.Include_In_Equity_Dating
                  aProRataCharge.Periodicity = aChargePatternData.Periodicity
                  aProRataCharge.PrintPriority_TDIC = aChargePatternData.PrintPriority
                  break
              case "PassThroughCharge":
                  var aPassThroughCharge = new PassThroughCharge()
                  aPassThroughCharge.PublicID = aChargePatternData.PublicID
                  aPassThroughCharge.ChargeCode = aChargePatternData.ChargeCode
                  aPassThroughCharge.ChargeName = aChargePatternData.ChargeName
                  aPassThroughCharge.Category = aChargePatternData.Category
                  aPassThroughCharge.TAccountOwnerPattern = tAccount
                  aPassThroughCharge.InvoiceTreatment = aChargePatternData.InvoicingApproach
                  aPassThroughCharge.Priority = aChargePatternData.Priority
                  aPassThroughCharge.IncludedInEquityDating = aChargePatternData.Include_In_Equity_Dating
                  aPassThroughCharge.PrintPriority_TDIC = aChargePatternData.PrintPriority
                  break
              case "ImmediateCharge":
                  var aImmediateCharge = new ImmediateCharge()
                  aImmediateCharge.PublicID = aChargePatternData.PublicID
                  aImmediateCharge.ChargeCode = aChargePatternData.ChargeCode
                  aImmediateCharge.ChargeName = aChargePatternData.ChargeName
                  aImmediateCharge.Category = aChargePatternData.Category
                  aImmediateCharge.TAccountOwnerPattern = tAccount
                  aImmediateCharge.InvoiceTreatment = aChargePatternData.InvoicingApproach
                  aImmediateCharge.Priority = aChargePatternData.Priority
                  aImmediateCharge.IncludedInEquityDating = aChargePatternData.Include_In_Equity_Dating
                  aImmediateCharge.PrintPriority_TDIC = aChargePatternData.PrintPriority
                  break
              case "CollateralCharge":
                  var aCollateralCharge = new CollateralCharge()
                  aCollateralCharge.PublicID = aChargePatternData.PublicID
                  aCollateralCharge.ChargeCode = aChargePatternData.ChargeCode
                  aCollateralCharge.ChargeName = aChargePatternData.ChargeName
                  aCollateralCharge.Category = aChargePatternData.Category
                  aCollateralCharge.TAccountOwnerPattern = tAccount
                  aCollateralCharge.InvoiceTreatment = aChargePatternData.InvoicingApproach
                  aCollateralCharge.Priority = aChargePatternData.Priority
                  aCollateralCharge.IncludedInEquityDating = aChargePatternData.Include_In_Equity_Dating
                  aCollateralCharge.PrintPriority_TDIC = aChargePatternData.PrintPriority
                  break
              case "RecaptureCharge":
                  var aRecaptureCharge = new RecaptureCharge()
                  aRecaptureCharge.PublicID = aChargePatternData.PublicID
                  aRecaptureCharge.ChargeCode = aChargePatternData.ChargeCode
                  aRecaptureCharge.ChargeName = aChargePatternData.ChargeName
                  aRecaptureCharge.Category = aChargePatternData.Category
                  aRecaptureCharge.TAccountOwnerPattern = tAccount
                  aRecaptureCharge.InvoiceTreatment = aChargePatternData.InvoicingApproach
                  aRecaptureCharge.Priority = aChargePatternData.Priority
                  aRecaptureCharge.IncludedInEquityDating = aChargePatternData.Include_In_Equity_Dating
                  aRecaptureCharge.PrintPriority_TDIC = aChargePatternData.PrintPriority
                  break
                default: // do nothing
            }
            aChargePatternData.Skipped = false
          } else {
            // update existing
            var chrgPattern = bundle.add(currChargePattern)
            chrgPattern.ChargeName = aChargePatternData.ChargeName
            chrgPattern.Category = aChargePatternData.Category
            chrgPattern.TAccountOwnerPattern = tAccount
            chrgPattern.InvoiceTreatment = aChargePatternData.InvoicingApproach
            chrgPattern.Priority = aChargePatternData.Priority
            chrgPattern.IncludedInEquityDating = aChargePatternData.Include_In_Equity_Dating
            chrgPattern.PrintPriority_TDIC = aChargePatternData.PrintPriority
            if (aChargePatternData.ChargeType == "ProRataCharge") {
              (chrgPattern typeas ProRataCharge).Periodicity = aChargePatternData.Periodicity
            }
            aChargePatternData.Skipped = false
            if (chrgPattern.getChangedFields().Count > 0) {
              aChargePatternData.Updated = true
            } else {
              aChargePatternData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("ChargePattern Load: " + aChargePatternData.PublicID + e.toString())
      }
      aChargePatternData.Error = true
    }
  }
}