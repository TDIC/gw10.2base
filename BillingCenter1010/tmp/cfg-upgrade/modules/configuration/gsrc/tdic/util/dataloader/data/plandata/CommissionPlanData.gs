package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ColumnDescriptor

class CommissionPlanData extends PlanData {

  // constants
  public static final var SHEET : String = "CommissionPlan"
  public static final var COL_PUBLIC_ID : int = 0
  public static final var COL_NAME : int = 1
  public static final var COL_EFFECTIVE_DATE : int = 2
  public static final var COL_EXPIRATION_DATE : int = 3
  public static final var COL_ALLOWED_TIERS_GOLD : int = 4
  public static final var COL_ALLOWED_TIERS_SILVER : int = 5
  public static final var COL_ALLOWED_TIERS_BRONZE : int = 6
  public static final var COL_PRIMARY_RATE : int = 7
  public static final var COL_SECONDARY_RATE : int = COL_PRIMARY_RATE+1
  public static final var COL_REFERRER_RATE : int = COL_PRIMARY_RATE+2
  public static final var COL_EARN_COMMISSIONS : int = COL_PRIMARY_RATE+3
  public static final var COL_SUSPEND_FOR_DELINQUENCY : int = COL_PRIMARY_RATE+4
  public static final var COL_PREMIUM_INCENTIVE_BONUS_PERCENTAGE : int = COL_PRIMARY_RATE+5
  public static final var COL_PREMIUM_INCENTIVE_THRESHOLD : int = COL_PRIMARY_RATE+6
  public static final var COL_COMMISSIONABLE_ITEM1 : int = COL_PRIMARY_RATE+7
  public static final var COL_COMMISSIONABLE_ITEM2 : int = COL_PRIMARY_RATE+8
  public static final var COL_COMMISSIONABLE_ITEM3 : int = COL_PRIMARY_RATE+9
  public static final var COL_COMMISSIONABLE_ITEM4 : int = COL_PRIMARY_RATE+10
  public static final var COL_CURRENCY: int = 18
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EFFECTIVE_DATE, "Effective Date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_EXPIRATION_DATE, "Expiration Date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_ALLOWED_TIERS_GOLD, "Allowed Tiers - Gold", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALLOWED_TIERS_SILVER, "Allowed Tiers - Silver", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALLOWED_TIERS_BRONZE, "Allowed Tiers - Bronze", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PRIMARY_RATE, "Default Plan Primary Rate", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_SECONDARY_RATE, "Default Plan Secondary Rate", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_REFERRER_RATE, "Default Plan Referrer Rate", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_EARN_COMMISSIONS, "Earn Commissions", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_SUSPEND_FOR_DELINQUENCY, "Suspended for Delinquency", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PREMIUM_INCENTIVE_BONUS_PERCENTAGE, "Premium Incentive Bonus Percentage", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PREMIUM_INCENTIVE_THRESHOLD, "Premium Incentive Threshold", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_COMMISSIONABLE_ITEM1, "Commissionable Item 1", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_COMMISSIONABLE_ITEM2, "Commissionable Item 2", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_COMMISSIONABLE_ITEM3, "Commissionable Item 3", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_COMMISSIONABLE_ITEM4, "Commissionable Item 4", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CURRENCY, "Currency", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }
  
  // properties
  var _allowedTiers_Gold                     : Boolean                as AllowedTiers_Gold
  var _allowedTiers_Silver                   : Boolean                as AllowedTiers_Silver
  var _allowedTiers_Bronze                   : Boolean                as AllowedTiers_Bronze
  var _Currency                              : Currency               as Currency

}
