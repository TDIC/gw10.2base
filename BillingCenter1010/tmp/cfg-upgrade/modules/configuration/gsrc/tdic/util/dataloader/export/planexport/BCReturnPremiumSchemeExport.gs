package tdic.util.dataloader.export.planexport

uses gw.api.database.Query
uses java.util.Collection
uses java.util.HashMap
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCReturnPremiumSchemeExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return ReturnPremiumSchemeData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getReturnPremiumSchemeEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(ReturnPremiumSchemeData.CD), queryReturnPremiumSchemes(), \e -> getRowFromReturnPremiumScheme(e))
  }

  // query data

  public static function queryReturnPremiumSchemes(): Collection <ReturnPremiumHandlingScheme> {
    var result = Query.make(ReturnPremiumHandlingScheme).select().orderBy( 
      QuerySelectColumns.path(Paths.make(entity.ReturnPremiumHandlingScheme#ReturnPremiumPlan))
    ).thenBy( 
      QuerySelectColumns.path(Paths.make(entity.ReturnPremiumHandlingScheme#Priority))
    )
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromReturnPremiumScheme(plan: ReturnPremiumHandlingScheme): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(ReturnPremiumSchemeData.COL_RETURN_PREMIUM_PLAN_NAME), plan.ReturnPremiumPlan.Name)
    colMap.put(colIndexString(ReturnPremiumSchemeData.COL_PRIORITY), plan.Priority)
    colMap.put(colIndexString(ReturnPremiumSchemeData.COL_CONTEXT), plan.HandlingCondition)
    colMap.put(colIndexString(ReturnPremiumSchemeData.COL_START_DATE_OPTION), plan.StartDateOption)
    colMap.put(colIndexString(ReturnPremiumSchemeData.COL_ALLOCATE_TIMING), plan.AllocateTiming)
    colMap.put(colIndexString(ReturnPremiumSchemeData.COL_ALLOCATE_METHOD), plan.AllocateMethod)
    colMap.put(colIndexString(ReturnPremiumSchemeData.COL_EXCESS_TREATMENT), plan.ExcessTreatment)

    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getReturnPremiumSchemeColumnTypes(): HashMap {
    return getColumnTypes(ReturnPremiumSchemeData.CD)
  }
}
