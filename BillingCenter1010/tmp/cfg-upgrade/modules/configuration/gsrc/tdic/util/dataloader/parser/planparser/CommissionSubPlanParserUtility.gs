package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.math.BigDecimal
uses java.lang.Integer
uses tdic.util.dataloader.data.plandata.CommissionSubPlanData
uses tdic.util.dataloader.data.plandata.CommissionItemData
uses tdic.util.dataloader.data.plandata.CommissionSubPlanRateData
uses java.util.List
uses gw.api.upgrade.Coercions

class CommissionSubPlanParserUtility extends WorksheetParserUtil {
  construct() {
  }

  // function used to parse the Default SubPlan from the CommmissionPlan sheet
  // and additional SubPlans from the CommisssionSubPlan sheet

  public static function populateSubPlanData(commissionSubPlanData: CommissionSubPlanData,
                                             row: Row, col: Integer): CommissionSubPlanData {
    // Define and instantiate variables
    var PRIMARY_RATE: int = col
    var SECONDARY_RATE: int = col + 1
    var REFERRER_RATE: int = col + 2
    var EARN_COMMISSIONS: int = col + 3
    var SUSPEND_FOR_DELINQUENCY: int = col + 4
    var PREMIUM_INCENTIVE_BONUS_PERCENTAGE: int = col + 5
    var PREMIUM_INCENTIVE_THRESHOLD: int = col + 6
    var subPlanRates = new ArrayList <CommissionSubPlanRateData>()
    var commissionItems = new ArrayList <CommissionItemData>()
    // Populate Commission SubPlan Data
    commissionSubPlanData.Earn_Commissions = convertCellToString(row.getCell(EARN_COMMISSIONS))
    commissionSubPlanData.Suspend_for_Delinquency = Coercions.makeBooleanFrom(convertCellToString(row.getCell(SUSPEND_FOR_DELINQUENCY)))
    commissionSubPlanData.PremiumIncentive_Bonus_Percentage = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(PREMIUM_INCENTIVE_BONUS_PERCENTAGE)))
    commissionSubPlanData.PremiumIncentive_Threshold = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(PREMIUM_INCENTIVE_THRESHOLD)))
    // Populate SubPlan Rates
    PopulateSubPlanRates(subPlanRates, Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(PRIMARY_RATE))), "primary")
    PopulateSubPlanRates(subPlanRates, Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(SECONDARY_RATE))), "secondary")
    PopulateSubPlanRates(subPlanRates, Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(REFERRER_RATE))), "referrer")
    commissionSubPlanData.SubPlanRates = subPlanRates?.toTypedArray()
    // Populate Commissionable Items
    var i = col + 7
    do {
      var commItemCell = i
      var commissionItem = new CommissionItemData()
      var cellValue = convertCellToString(row.getCell(commItemCell as short))
      if (cellValue != null and cellValue != "") {
        commissionItem.ChargePattern = cellValue
        commissionItems.add(commissionItem)
      }
      i = i + 1
    } while (i < col + 11 )
    commissionSubPlanData.CommissionItems = commissionItems?.toTypedArray()
    return commissionSubPlanData
  }

  private static function PopulateSubPlanRates(subPlanRates: List <CommissionSubPlanRateData>, rate: BigDecimal, role: String) {
    var subPlanRate = new CommissionSubPlanRateData()
    subPlanRate.Rate = rate
    subPlanRate.Role = role
    subPlanRates.add(subPlanRate)
  }
}