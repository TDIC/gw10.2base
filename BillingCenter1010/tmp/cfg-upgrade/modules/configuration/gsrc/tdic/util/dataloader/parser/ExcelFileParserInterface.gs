package tdic.util.dataloader.parser

uses tdic.util.dataloader.processor.DataLoaderProcessor
uses java.io.File

interface ExcelFileParserInterface {
  function readFile(webFile: gw.api.web.WebFile): File

  function importFile(filePath: String, dataLoaderHelper: DataLoaderProcessor): DataLoaderProcessor
}
