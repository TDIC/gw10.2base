package tdic.util.dataloader.data.sampledata

uses java.util.Date

class ExistingPolicyData extends BillingInstructionData {

  construct() {

  }
  private var _modificationdate           : Date      as ModificationDate
  private var _associatedpolicyperiod     : String    as AssociatedPolicyPeriod
  private var _termNumber                 : int       as TermNumber
}
