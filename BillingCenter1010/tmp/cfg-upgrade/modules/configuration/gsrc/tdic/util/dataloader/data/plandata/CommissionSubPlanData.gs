package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ApplicationData
uses java.math.BigDecimal
uses java.lang.Integer
uses tdic.util.dataloader.data.ColumnDescriptor

class CommissionSubPlanData extends ApplicationData {

  // constants
  public static final var SHEET : String = "CommissionSubPlan"
  public static final var COL_PLAN_NAME : int = 0
  public static final var COL_SUBPLAN_NAME : int = 1
  public static final var COL_PRIORITY : int = 2
  public static final var COL_PRIMARY_RATE : int = 3
  public static final var COL_SECONDARY_RATE : int = COL_PRIMARY_RATE+1
  public static final var COL_REFERRER_RATE : int = COL_PRIMARY_RATE+2
  public static final var COL_EARN_COMMISSIONS : int = COL_PRIMARY_RATE+3
  public static final var COL_SUSPEND_FOR_DELINQUENCY : int = COL_PRIMARY_RATE+4
  public static final var COL_PREMIUM_INCENTIVE_BONUS_PERCENTAGE : int = COL_PRIMARY_RATE+5
  public static final var COL_PREMIUM_INCENTIVE_THRESHOLD : int = COL_PRIMARY_RATE+6
  public static final var COL_COMMISSIONABLE_ITEM1 : int = COL_PRIMARY_RATE+7
  public static final var COL_COMMISSIONABLE_ITEM2 : int = COL_PRIMARY_RATE+8
  public static final var COL_COMMISSIONABLE_ITEM3 : int = COL_PRIMARY_RATE+9
  public static final var COL_COMMISSIONABLE_ITEM4 : int = COL_PRIMARY_RATE+10
  public static final var COL_ASSIGNED_RISK : int = 14
  public static final var COL_ALL_EVALUATIONS : int = 15
  public static final var COL_ALL_LOB_CODES : int = 16 // folowed by 7 LOB code columns
  public static final var COL_ALL_SEGMENTS : int = 24 // followed by 5 segment columns
  public static final var COL_ALL_JURISDICTIONS: int = 30
  public static final var COL_ALL_TERMS : int = 31 // followed by 5 terms columns
  public static final var COL_ALL_UW_COMPANIES : int = 37  
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_PLAN_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_SUBPLAN_NAME, "Sub Plan Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PRIORITY, "Priority", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_PRIMARY_RATE, "Sub Plan Primary rate", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_SECONDARY_RATE, "Sub Plan Secondary rate", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_REFERRER_RATE, "Sub Plan Referred rate", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_EARN_COMMISSIONS, "Earn  Commissions", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_SUSPEND_FOR_DELINQUENCY, "Suspend for Delinquency", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PREMIUM_INCENTIVE_BONUS_PERCENTAGE, "Premium Incentive Bonus Percentage", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_PREMIUM_INCENTIVE_THRESHOLD, "Premium Incentive Threshold", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_COMMISSIONABLE_ITEM1, "Commissionable Item 1", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_COMMISSIONABLE_ITEM2, "Commissionable Item 2", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_COMMISSIONABLE_ITEM3, "Commissionable Item 3", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_COMMISSIONABLE_ITEM4, "Commissionable Item 4", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ASSIGNED_RISK, "Assigned Risk", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_EVALUATIONS, "All Evaluations", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_LOB_CODES, "All LOB Codes", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_LOB_CODES + 1, LOBCode.TC_WORKERSCOMP.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_LOB_CODES + 2, LOBCode.TC_COMMERCIALPROPERTY.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_LOB_CODES + 3, LOBCode.TC_INLANDMARINE.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_LOB_CODES + 4, LOBCode.TC_GENERALLIABILITY.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_LOB_CODES + 5, LOBCode.TC_BUSINESSAUTO.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_LOB_CODES + 6, LOBCode.TC_PERSONALAUTO.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_LOB_CODES + 7, LOBCode.TC_BUSINESSOWNERS.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_SEGMENTS, "All Segments", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_SEGMENTS + 1, ApplicableSegments.TC_LARGEBUSINESS.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_SEGMENTS + 2, ApplicableSegments.TC_MEDIUMBUSINESS.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_SEGMENTS + 3, ApplicableSegments.TC_PERSONAL.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_SEGMENTS + 4, ApplicableSegments.TC_SMALLBUSINESS.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_SEGMENTS + 5, ApplicableSegments.TC_SUBPRIME.DisplayName, ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_JURISDICTIONS, "All Jurisdictions", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_TERMS, "All Terms", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_TERMS + 1, "Initial Term", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_TERMS + 2, "First Renewal", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_TERMS + 3, "Second Renewal", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_TERMS + 4, "Third Renewal", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_TERMS + 5, "Thereafter", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ALL_UW_COMPANIES, "All UW Companies", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }
  
  // properties
  var _PlanName                              : String                       as PlanName
  var _SubPlanName                           : String                       as SubPlanName
  var _SubPlanRates                          : CommissionSubPlanRateData[]  as SubPlanRates
  var _Earn_Commissions                      : String                       as Earn_Commissions
  var _Suspend_for_Delinquency               : Boolean                      as Suspend_for_Delinquency
  var _PremiumIncentive_Bonus_Percentage     : BigDecimal                   as PremiumIncentive_Bonus_Percentage
  var _PremiumIncentive_Threshold            : BigDecimal                   as PremiumIncentive_Threshold
  var _Priority                              : Integer                      as Priority
  var _CommissionItems                       : CommissionItemData[]         as CommissionItems
  var _allEvaluations                        : Boolean                      as AllEvaluations
  var _allLOBCodes                           : Boolean                      as AllLOBCodes
  var _LOBWorkersComp                        : Boolean                      as LOBWorkersComp
  var _LOBCommercialProperty                 : Boolean                      as LOBCommercialProperty
  var _LOBInlandMarine                       : Boolean                      as LOBInlandMarine
  var _LOBGeneralLiability                   : Boolean                      as LOBGeneralLiability
  var _LOBBusinessAuto                       : Boolean                      as LOBBusinessAuto
  var _LOBPersonalAuto                       : Boolean                      as LOBPersonalAuto
  var _LOBBusinessOwners                     : Boolean                      as LOBBusinessOwners
  var _allSegments                           : Boolean                      as AllSegments
  var _SegmentLargeBusiness                  : Boolean                      as SegmentLargeBusiness
  var _SegmentMediumBusiness                 : Boolean                      as SegmentMediumBusiness
  var _SegmentPersonal                       : Boolean                      as SegmentPersonal
  var _SegmentSmallBusiness                  : Boolean                      as SegmentSmallBusiness
  var _SegmentSubprime                       : Boolean                      as SegmentSubprime
  var _allJurisdictions                      : Boolean                      as AllJurisdictions
  var _allTerms                              : Boolean                      as AllTerms 
  var _TermsInitialBusiness                  : Boolean                      as TermsInitialBusiness
  var _TermsFirstRenewal                     : Boolean                      as TermsFirstRenewal
  var _TermsSecondRenewal                    : Boolean                      as TermsSecondRenewal
  var _TermsThirdRenewal                     : Boolean                      as TermsThirdRenewal
  var _TermsThereafter                       : Boolean                      as TermsThereafter
  var _allUWCompanies                        : Boolean                      as AllUWCompanies
  var _assignedRisk                          : AssignedRiskRestriction      as AssignedRisk 
}
