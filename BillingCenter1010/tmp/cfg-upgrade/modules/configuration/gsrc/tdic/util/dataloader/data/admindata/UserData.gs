package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class UserData extends ApplicationData {

  // constants
  public static final var SHEET : String = "User"
  public static final var COL_REFERENCE_NAME : int = 0
  public static final var COL_EXCLUDE : int = 1
  public static final var COL_ACTIVE : int = 2
  public static final var COL_PUBLIC_ID : int = 3
  public static final var COL_LAST_NAME : int = 4
  public static final var COL_FIRST_NAME : int = 5
  public static final var COL_MIDDLE_NAME : int = 6
  public static final var COL_EMPLOYEE_NUMBER : int = 7
  public static final var COL_USERNAME : int = 8
  public static final var COL_PASSWORD : int = 9
  public static final var COL_JOB_TITLE : int = 10
  public static final var COL_DEPARTMENT : int = 11
  public static final var COL_GENDER : int = 12
  public static final var COL_HOME_PHONE : int = 13
  public static final var COL_WORK_PHONE : int = 14
  public static final var COL_CELL_PHONE : int = 15
  public static final var COL_FAX_PHONE : int = 16
  public static final var COL_PRIMARY_PHONE : int = 17
  public static final var COL_PRIMARY_ADDRESS : int = 18
  public static final var COL_NONPRIMARY_ADDRESS : int = 19
  public static final var COL_FORMER_NAME : int = 20
  public static final var COL_EMAIL_ADDRESS1 : int = 21
  public static final var COL_EMAIL_ADDRESS2 : int = 22
  public static final var COL_DATE_OF_BIRTH : int = 23
  public static final var COL_EXTERNAL_USER : int = 24
  public static final var COL_ROLE1 : int = 25
  public static final var COL_ROLE2 : int = 26
  public static final var COL_ROLE3 : int = 27
  public static final var COL_ROLE4 : int = 28
  public static final var COL_ROLE5 : int = 29
  public static final var COL_ROLE6 : int = 30
  public static final var COL_ROLE7 : int = 31
  public static final var COL_REGION1 : int = 32
  public static final var COL_REGION2 : int = 33
  public static final var COL_REGION3 : int = 34
  public static final var COL_LANGUAGE : int = 35
  public static final var COL_NOTES : int = 36
  public static final var CD : ColumnDescriptor[] = {
      new ColumnDescriptor(COL_REFERENCE_NAME, "Reference Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ACTIVE, "Active", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PUBLIC_ID, "Public Id", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_LAST_NAME, "Last Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_FIRST_NAME, "First Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_MIDDLE_NAME, "Middle Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EMPLOYEE_NUMBER, "Employee Number", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_USERNAME, "User Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PASSWORD, "Password", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_JOB_TITLE, "Job Title", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_DEPARTMENT, "Department", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_GENDER, "Gender", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_HOME_PHONE, "Home Phone", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_WORK_PHONE, "Work Phone", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CELL_PHONE, "Cell Phone", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_FAX_PHONE, "Fax Phone", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PRIMARY_PHONE, "Primary Phone", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PRIMARY_ADDRESS, "Primary Address", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_NONPRIMARY_ADDRESS, "Non Primary Address", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_FORMER_NAME, "Former Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EMAIL_ADDRESS1, "Email Address 1", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EMAIL_ADDRESS2, "Email Address 2", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_DATE_OF_BIRTH, "Date of Birth", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EXTERNAL_USER, "External User", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ROLE1, "Role", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ROLE2, "Role", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ROLE3, "Role", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ROLE4, "Role", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ROLE5, "Role", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ROLE6, "Role", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ROLE7, "Role", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_REGION1, "Region", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_REGION2, "Region", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_REGION3, "Region", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_LANGUAGE, "Language", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_NOTES, "Notes", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }

  // properties
  private var _referencename           : String                       as ReferenceName
  private var _active                  : Boolean                      as Active
  private var _contact                 : UserContactData              as Contact
  private var _username                : String                       as UserName
  private var _password                : String                       as Password
  private var _jobtitle                : String                       as JobTitle
  private var _department              : String                       as Department
  private var _externaluser            : Boolean                      as ExternalUser
  private var _role1                   : RoleData                     as Role1
  private var _role2                   : RoleData                     as Role2
  private var _role3                   : RoleData                     as Role3
  private var _role4                   : RoleData                     as Role4
  private var _role5                   : RoleData                     as Role5
  private var _role6                   : RoleData                     as Role6
  private var _role7                   : RoleData                     as Role7
  private var _region1                 : RegionData                   as Region1
  private var _region2                 : RegionData                   as Region2
  private var _region3                 : RegionData                   as Region3
  private var _language                : String                       as Language

}
