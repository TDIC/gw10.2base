package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class QueueData extends ApplicationData {

  // constants
  public static final var SHEET : String = "Queue"
  public static final var COL_DISPLAYNAME : int = 0
  public static final var COL_PUBLIC_ID : int = 1
  public static final var COL_NAME : int = 2
  public static final var COL_GROUP : int = 3
  public static final var COL_DESCRIPTION : int = 4
  public static final var COL_SUB_GROUP_VISIBLE: int = 5
  public static final var COL_EXCLUDE : int = 6
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_DISPLAYNAME, "Display Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_GROUP, "Group", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DESCRIPTION, "Description", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_SUB_GROUP_VISIBLE, "Visible In Subgroups?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }

  // properties
  private var _name                    : String                       as Name
  private var _group                   : GroupData                    as Group
  private var _description             : String                       as Description
  private var _subGroupVisible         : Boolean                      as SubGroupVisible
  
  public property get DisplayName() : String {
    if (this != null)
      return _name + " - " + _group.Name
    else return null
  }
  
}
