package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.admindata.AttributeData
uses java.util.Collection
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCAttributeExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return AttributeData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getAttributeEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(AttributeData.CD), queryAttributes(), \e -> getRowFromAttribute(e))
  }

  // query data

  public static function queryAttributes(): Collection <Attribute> {
    var result = Query.make(Attribute).select().orderBy(
      QuerySelectColumns.path(Paths.make(entity.Attribute#PublicID))
    )
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromAttribute(attribute: Attribute): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(AttributeData.COL_NAME), attribute.Name)
    colMap.put(colIndexString(AttributeData.COL_PUBLIC_ID), attribute.PublicID)
    colMap.put(colIndexString(AttributeData.COL_DESCRIPTION), attribute.Description)
    colMap.put(colIndexString(AttributeData.COL_USER_ATTRIBUTE_TYPE), attribute.Type)
    colMap.put(colIndexString(AttributeData.COL_ACTIVE), attribute.Active)
    colMap.put(colIndexString(AttributeData.COL_EXCLUDE), "false")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getAttributeColumnTypes(): HashMap {
    return getColumnTypes(AttributeData.CD)
  }
}
