package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses tdic.util.dataloader.data.admindata.QueueData
uses tdic.util.dataloader.data.admindata.ActivityPatternData
uses gw.api.upgrade.Coercions

class ActivityPatternWorksheetParser extends WorksheetParserUtil {
  construct() {
  }
  var queueArray: ArrayList <QueueData>
  function parseActivityPatternSheet(sheet: Sheet, qArray: ArrayList <QueueData>): ArrayList <ActivityPatternData> {
    queueArray = qArray
    return parseSheet(sheet, \r -> parseActivityPatternRow(r), true)
  }

  function parseActivityPatternRow(row: Row): ActivityPatternData {
    // read group reference
    var queueDN = convertCellToString(row.getCell(ActivityPatternData.COL_QUEUE_DISPLAYNAME))
    var data = queueArray.firstWhere(\q -> q.DisplayName == queueDN)
    if (queueDN != null and queueDN != "" and data == null) {
      var msg = row.Sheet.SheetName + " sheet: Value " + queueDN
          + " in column " + ActivityPatternData.CD[ActivityPatternData.COL_QUEUE_DISPLAYNAME].ColHeader
          + " of row " + row.RowNum
          + " is not a known queue display name, ignoring value"
      LOG.warn(msg)
    }
    // Populate Activity Pattern Data
    var result = new ActivityPatternData()
    result.Code = convertCellToString(row.getCell(ActivityPatternData.COL_CODE))
    result.PublicID = convertCellToString(row.getCell(ActivityPatternData.COL_PUBLIC_ID))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(ActivityPatternData.COL_EXCLUDE)))
    result.Subject = convertCellToString(row.getCell(ActivityPatternData.COL_SUBJECT))
    result.Description = convertCellToString(row.getCell(ActivityPatternData.COL_DESCRIPTION))
    result.ActivityClass = typekey.ActivityClass.get(convertCellToString(row.getCell(ActivityPatternData.COL_ACTIVITY_CLASS)))
    result.ActivityType = typekey.ActivityType.get(convertCellToString(row.getCell(ActivityPatternData.COL_TYPE)))
    result.Category = convertCellToString(row.getCell(ActivityPatternData.COL_CATEGORY))
    result.Mandatory = Coercions.makeBooleanFrom(convertCellToString(row.getCell(ActivityPatternData.COL_MANDATORY)))
    result.Priority = convertCellToString(row.getCell(ActivityPatternData.COL_PRIORITY))
    result.Recurring = Coercions.makeBooleanFrom(convertCellToString(row.getCell(ActivityPatternData.COL_RECURRING)))
    result.TargetDays = Coercions.makeIntFrom(convertCellToString(row.getCell(ActivityPatternData.COL_TARGET_DAYS)))
    result.TargetHours = Coercions.makeIntFrom(convertCellToString(row.getCell(ActivityPatternData.COL_TARGET_HOURS)))
    result.TargetIncludeDays = typekey.IncludeDaysType.get(convertCellToString(row.getCell(ActivityPatternData.COL_TARGET_INCLUDE_DAYS)))
    result.TargetStartPoint = typekey.StartPointType.get(convertCellToString(row.getCell(ActivityPatternData.COL_TARGET_START_POINT)))
    result.EscalationDays = Coercions.makeIntFrom(convertCellToString(row.getCell(ActivityPatternData.COL_ESCALATION_DAYS)))
    result.EscalationHours = Coercions.makeIntFrom(convertCellToString(row.getCell(ActivityPatternData.COL_ESCALATION_HOURS)))
    result.EscalationInclDays = typekey.IncludeDaysType.get(convertCellToString(row.getCell(ActivityPatternData.COL_ESCALATION_INCL_DAYS)))
    result.EscalationStartPt = typekey.StartPointType.get(convertCellToString(row.getCell(ActivityPatternData.COL_ESCALATION_START_POINT)))
    result.AutomatedOnly = Coercions.makeBooleanFrom(convertCellToString(row.getCell(ActivityPatternData.COL_AUTOMATED_ONLY)))
    result.Queue = data
    result.Command = convertCellToString(row.getCell(ActivityPatternData.COL_COMMAND))
    return result
  }
}
