package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor
uses java.util.Date

class HolidayData extends ApplicationData {

  // constants
  public static final var SHEET : String = "Holiday"
  public static final var COL_NAME : int = 0
  public static final var COL_EXCLUDE : int = 1
  public static final var COL_PUBLIC_ID : int = 2
  public static final var COL_OCCURRENCE_DATE : int = 3
  public static final var COL_ALL_ZONES : int = 4
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_OCCURRENCE_DATE, "Occurrence Date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_ALL_ZONES, "All Zones", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }
  
  // properties
  private var _name                    : String                       as Name
  private var _occurrenceDate          : Date                     as OccurrenceDate
  private var _appliesToAllZones       : Boolean                      as AppliesToAllZones
  
}