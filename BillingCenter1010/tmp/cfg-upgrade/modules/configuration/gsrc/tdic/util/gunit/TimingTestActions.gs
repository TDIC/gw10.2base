package tdic.util.gunit

uses gw.lang.reflect.IMethodInfo
uses gw.lang.reflect.IType
uses gw.testharness.TestBase
uses java.lang.System
uses java.util.List

class TimingTestActions extends TestActionDelegatingBase {

  var _showClassTiming   : boolean as ShowClassTiming  = true
  var _showMethodTiming  : boolean as ShowMethodTiming = true

  construct(otherImplementation : ITestActions) {
    super(otherImplementation)
  }

  override function runTestsInClass(iType : IType, testMethods : List<IMethodInfo>) {
    var startTime = System.currentTimeMillis()

    super.runTestsInClass(iType, testMethods)

    if (ShowClassTiming) {
      var endTime = System.currentTimeMillis()
      var duration = endTime - startTime
      print(iType.Name + " " + duration)
    }
  }

  override function invokeTestMethod(method : IMethodInfo, testInstance : TestBase) {
    var startTime = System.currentTimeMillis()
    try {
      super.invokeTestMethod(method, testInstance)

      if (ShowMethodTiming) {
        var endTime = System.currentTimeMillis()
        var duration = endTime - startTime
        print(method.OwnersType.Name + " " + method.Name + " " + duration + " success")
      }
    } catch (ex) {
      if (ShowMethodTiming) {
        var endTime = System.currentTimeMillis()
        var duration = endTime - startTime
        print(method.OwnersType.Name + " " + method.Name + " " + duration + " exception")
      }
    }
  }

  override function end() {
    print("")
  }

}
