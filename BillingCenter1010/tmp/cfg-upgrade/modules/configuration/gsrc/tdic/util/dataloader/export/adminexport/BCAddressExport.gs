package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.export.WorksheetExportUtil
uses java.util.Collection

class BCAddressExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return AddressData.SHEET
  }

  // Function to retrieve the entity values

  public static function getAddressEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(AddressData.CD), queryAddresses(), \e -> getRowFromAddress(e))
  }

  // query data

  public static function queryAddresses(): Collection <Address> {
    var userContactQuery = Query.make(UserContact)
    var result = new java.util.HashSet <Address>()
    result.addAll(userContactQuery.select()*.AllAddresses?.toList())
    return result.orderBy(\a -> a.PublicID)
  }

  // build column map from entity

  public static function getRowFromAddress(uAddress: Address): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(AddressData.COL_DISPLAY_NAME), uAddress.DisplayName)
    colMap.put(colIndexString(AddressData.COL_ADDRESS_LINE1), uAddress.AddressLine1)
    colMap.put(colIndexString(AddressData.COL_ADDRESS_LINE2), uAddress.AddressLine2)
    colMap.put(colIndexString(AddressData.COL_ADDRESS_LINE3), uAddress.AddressLine3)
    colMap.put(colIndexString(AddressData.COL_CITY), uAddress.City)
    colMap.put(colIndexString(AddressData.COL_STATE), uAddress.State)
    colMap.put(colIndexString(AddressData.COL_POSTAL_CODE), uAddress.PostalCode)
    colMap.put(colIndexString(AddressData.COL_COUNTY), uAddress.County)
    colMap.put(colIndexString(AddressData.COL_COUNTRY), uAddress.Country)
    colMap.put(colIndexString(AddressData.COL_ADDRESS_TYPE), uAddress.AddressType)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getAddressColumnTypes(): HashMap {
    return getColumnTypes(AddressData.CD)
  }
}
