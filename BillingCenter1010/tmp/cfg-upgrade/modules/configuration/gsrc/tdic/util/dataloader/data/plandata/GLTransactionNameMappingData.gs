package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ColumnDescriptor

/**
 * US570 - General Ledger Integration
 * 11/14/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities - An object to store the data for the
 * GLIntegrationTransactionMapping_TDIC object.
 */
class GLTransactionNameMappingData extends PlanData {

  /**
   * Constant for String defining what Excel sheet to use.
   */
  public static final var SHEET : String = "GLTransactionNameMapping"
  
  /**
   * Constant for the column indicating the Original Transaction Name
   */
  public static final var COL_ORIGINAL_TRANSACTION_NAME : int = 0

  /**
   * Constant for the column indicating the Mapped Transaction Name
   */
  public static final var COL_MAPPED_TRANSACTION_NAME : int = 1
  
  /**
   * Mapping of the columns to the fields.
   */
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_ORIGINAL_TRANSACTION_NAME, "Original Transaction Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_MAPPED_TRANSACTION_NAME, "Mapped Transaction Name", ColumnDescriptor.TYPE_STRING)
  }

  /**
   * Standard constructor.
   */
  construct() {
  }
  
  /**
   * The Original Transaction Name for the GL Mapping
   */
  var _originalTransactionName : typekey.Transaction as OriginalTransactionName

  /**
   * The Mapped Transaction Name for the GL Mapping
   */
  var _mappedTransactionName : String as MappedTransactionName

}
