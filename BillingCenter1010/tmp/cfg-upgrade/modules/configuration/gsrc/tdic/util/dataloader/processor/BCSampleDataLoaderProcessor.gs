package tdic.util.dataloader.processor

uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.ProducerData
uses tdic.util.dataloader.data.sampledata.InvoiceStreamData
uses tdic.util.dataloader.data.sampledata.AccountData
uses tdic.util.dataloader.data.sampledata.BillingInstructionData
uses tdic.util.dataloader.data.sampledata.IssuanceData
uses tdic.util.dataloader.data.sampledata.NewRenewalData
uses tdic.util.dataloader.data.sampledata.PolicyChangeData
uses tdic.util.dataloader.data.sampledata.DirectPaymentData
uses tdic.util.dataloader.data.sampledata.RenewalData
uses tdic.util.dataloader.data.sampledata.CancellationData
uses tdic.util.dataloader.data.sampledata.PolicyPeriodData
uses tdic.util.dataloader.data.sampledata.RewriteData
uses tdic.util.dataloader.data.sampledata.ReinstatementData
uses tdic.util.dataloader.data.sampledata.AuditData
uses tdic.util.dataloader.data.sampledata.AgencyBillPayData
uses tdic.util.dataloader.data.sampledata.BatchData
uses tdic.util.dataloader.data.sampledata.PaymentReversalData
uses tdic.util.dataloader.parser.sampleparser.BCSampleExcelFileParser
uses java.util.Date
uses tdic.util.dataloader.util.BCSampleDataLoaderUtil
uses tdic.util.dataloader.entityloaders.sampleloaders.BCAccountLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCProducerLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCIssuanceLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCPolicyChangeLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCRenewalLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCCancellationLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCDirectPaymentLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCRewriteLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCReinstatementLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCAuditLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCAgencyBillPaymentLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCBatchLoader
uses tdic.util.dataloader.entityloaders.sampleloaders.BCPaymentReversalLoader
uses tdic.util.dataloader.util.DataLoaderUtil
uses java.lang.Exception
uses org.slf4j.LoggerFactory
uses java.util.List

class BCSampleDataLoaderProcessor extends DataLoaderProcessor {
  construct() {
    this.RelativePath = DataLoaderUtil.getRelativeSampleDatafilePath()
  }
  static final var LOG = LoggerFactory.getLogger("SampleData.SampleDataProcessor")
  private var _producerArray: ArrayList <ProducerData> as ProducerArray = new ArrayList <ProducerData>()
  private var _invoiceStreamArray: ArrayList <InvoiceStreamData> as InvoiceStreamArray = new ArrayList <InvoiceStreamData>()
  private var _accountArray: ArrayList <AccountData> as AccountArray = new ArrayList <AccountData>()
  private var _policyArray: ArrayList <PolicyPeriodData> as PolicyArray = new ArrayList <PolicyPeriodData>()
  private var _billingInstructionArray: ArrayList <BillingInstructionData> as BillingInstructionArray = new ArrayList <BillingInstructionData>()
  private var _issuanceArray: ArrayList <IssuanceData> as IssuanceArray = new ArrayList <IssuanceData>()
  private var _newRenewalArray: ArrayList <NewRenewalData> as NewRenewalArray = new ArrayList <NewRenewalData>()
  private var _policyChangeArray: ArrayList <PolicyChangeData> as PolicyChangeArray = new ArrayList <PolicyChangeData>()
  private var _DirectPaymentArray: ArrayList <DirectPaymentData> as DirectPaymentArray = new ArrayList <DirectPaymentData>()
  private var _renewalArray: ArrayList <RenewalData> as RenewalArray = new ArrayList <RenewalData>()
  private var _cancellationArray: ArrayList <CancellationData> as CancellationArray = new ArrayList <CancellationData>()
  private var _rewriteArray: ArrayList <RewriteData> as RewriteArray = new ArrayList <RewriteData>()
  private var _reinstatementArray: ArrayList <ReinstatementData> as ReinstatementArray = new ArrayList <ReinstatementData>()
  private var _auditArray: ArrayList <AuditData> as AuditArray = new ArrayList <AuditData>()
  private var _agencyBillPaymentArray: ArrayList <AgencyBillPayData> as AgencyBillPaymentArray = new ArrayList <AgencyBillPayData>()
  private var _batchArray: ArrayList <BatchData> as BatchArray = new ArrayList <BatchData>()
  private var _paymentReversalArray: ArrayList <PaymentReversalData> as PaymentReversalArray = new ArrayList <PaymentReversalData>()
  private var _chkAccount: Boolean as ChkAccount = true
  private var _chkInvoiceStream: Boolean as ChkInvoiceStream = true
  private var _chkProducer: Boolean as ChkProducer = true
  private var _chkPolicy: Boolean as ChkPolicy = true
  private var _chkIssuance: Boolean as ChkIssuance = true
  private var _chkNewRenewal: Boolean as ChkNewRenewal = false
  private var _chkChange: Boolean as ChkChange = false
  private var _chkRenew: Boolean as ChkRenew = false
  private var _chkCancel: Boolean as ChkCancel = false
  private var _chkDirectPayment: Boolean as ChkDirectPayment = true
  private var _chkRewrite: Boolean as ChkRewrite = false
  private var _chkReinstatement: Boolean as ChkReinstatement = false
  private var _chkAudit: Boolean as ChkAudit = false
  private var _chkAgencyBillPayment: Boolean as ChkAgencyBillPayment = false
  private var _chkBatch: Boolean as ChkBatch = true
  private var _chkPaymentReversal: Boolean as ChkPaymentReversal = false
  private var _LoadCompleted: Boolean as LoadCompleted
  private var _LoadAllPayments: Boolean as LoadAllPayments
  private var _LoadAllAgencyBillPayments: Boolean as LoadAllAgencyBillPayments

  public function readAndImportFile() {
    readFile()
    importFile()
  }

  public function loadData() {
    setProcessDates()
    loadDataByDate()
    LOG.info("Loaded Sample data")
  }

  private function loadDataByDate() {
    for (transDate in ProcessDates.sort()) {
      LOG.debug("Date: " + transDate.toString())
      BCSampleDataLoaderUtil.setTestingClock(transDate)
      loadDataforGivenDate(transDate)
    }
    LoadCompleted = true
  }

  private function loadDataforGivenDate(transDate: Date) {
    BCSampleDataLoaderUtil.runBatch()
    // Process selected sheets
    if (ChkAccount) {
      processAccounts(transDate)
    }
    if (ChkProducer) {
      processProducers(transDate)
    }
    if (ChkIssuance) {
      processIssuance(transDate)
    }
    /*if (ChkNewRenewal) {
      processNewRenewal(transDate)
    }*/
    if (ChkChange) {
      processChange(transDate)
    }
    if (ChkRenew) {
      processRenewal(transDate)
    }
    if (ChkCancel) {
      processCancellation(transDate)
    }
    if (ChkDirectPayment) {
      processDirectPayment(transDate)
    }
    if (ChkRewrite) {
      processRewrite(transDate)
    }
    if (ChkReinstatement) {
      processReinstatement(transDate)
    }
    if (ChkAudit) {
      processAudit(transDate)
    }
    if (ChkAgencyBillPayment) {
      processAgencyBillPayment(transDate)
    }
    if (ChkBatch) {
      processBatch(transDate)
    }
    if (ChkPaymentReversal) {
      processPaymentReversal(transDate)
    }
    BCSampleDataLoaderUtil.runBatch()
  }

  private function processAccounts(tDate: Date) {
    var accounts: List <AccountData>
    if (DateHandling == dateHandlingText[0]) {
      accounts = AccountArray.where(\a -> a.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      accounts = AccountArray
    }
    for (account in accounts) {
      var accountLoader = new BCAccountLoader()
      accountLoader.PossibleInvoiceStreams = InvoiceStreamArray
      accountLoader.createEntity(account)
    }
  }

  private function processProducers(tDate: Date) {
    var producers: List <ProducerData>
    if (DateHandling == dateHandlingText[0]) {
      producers = ProducerArray.where(\a -> a.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      producers = ProducerArray
    }
    for (producer in producers) {
      var producerLoader = new BCProducerLoader()
      producerLoader.createEntity(producer)
    }
  }

  function processIssuance(tDate: Date) {
    var issuances: List <IssuanceData>
    if (DateHandling == dateHandlingText[0]) {
      issuances = IssuanceArray.where(\a -> a.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      issuances = IssuanceArray
    }
    for (issuance in issuances) {
      var issuanceLoader = new BCIssuanceLoader()
      issuanceLoader.PossibleInvoiceStreams = InvoiceStreamArray
      issuanceLoader.createEntity(issuance)
    }
  }

  function processChange(tDate: Date) {
    var policyChanges: List <PolicyChangeData>
    if (DateHandling == dateHandlingText[0]) {
      policyChanges = PolicyChangeArray.where(\p -> p.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      policyChanges = PolicyChangeArray
    }
    for (policyChange in policyChanges) {
      var policyChangeLoader = new BCPolicyChangeLoader()
      policyChangeLoader.createEntity(policyChange)
    }
  }

  function processRenewal(tDate: Date) {
    var renewals: List <RenewalData>
    if (DateHandling == dateHandlingText[0]) {
      renewals = RenewalArray.where(\r -> r.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      renewals = RenewalArray
    }
    for (renewal in renewals) {
      var renewalChangeLoader = new BCRenewalLoader()
      renewalChangeLoader.createEntity(renewal)
    }
  }

  function processCancellation(tDate: Date) {
    var cancellations: List <CancellationData>
    if (DateHandling == dateHandlingText[0]) {
      cancellations = CancellationArray.where(\c -> c.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      cancellations = CancellationArray
    }
    for (cancellation in cancellations) {
      var cancellationLoader = new BCCancellationLoader()
      cancellationLoader.createEntity(cancellation)
    }
  }

  function processDirectPayment(tDate: Date) {
    var directPayments: List <DirectPaymentData>
    if (DateHandling == dateHandlingText[0]) {
      directPayments = DirectPaymentArray.where(\d -> d.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      directPayments = DirectPaymentArray
    }
    for (directPayment in directPayments) {
      var directPaymentLoader = new BCDirectPaymentLoader()
      directPaymentLoader.createEntity(directPayment)
    }
  }

  function processRewrite(tDate: Date) {
    var rewrites: List <RewriteData>
    if (DateHandling == dateHandlingText[0]) {
      rewrites = RewriteArray.where(\r -> r.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      rewrites = RewriteArray
    }
    for (rewrite in rewrites) {
      var rewriteChangeLoader = new BCRewriteLoader()
      rewriteChangeLoader.createEntity(rewrite)
    }
  }

  function processReinstatement(tDate: Date) {
    var reinstatements: List <ReinstatementData>
    if (DateHandling == dateHandlingText[0]) {
      reinstatements = ReinstatementArray.where(\r -> r.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      reinstatements = ReinstatementArray
    }
    for (reinstatement in reinstatements) {
      var reinstatementLoader = new BCReinstatementLoader()
      reinstatementLoader.createEntity(reinstatement)
    }
  }

  function processAudit(tDate: Date) {
    var audits: List <AuditData>
    if (DateHandling == dateHandlingText[0]) {
      audits = AuditArray.where(\r -> r.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      audits = AuditArray
    }
    for (audit in audits) {
      var auditLoader = new BCAuditLoader()
      auditLoader.createEntity(audit)
    }
  }

  function processAgencyBillPayment(tDate: Date) {
    var agencyBillPayments: List <AgencyBillPayData>
    if (DateHandling == dateHandlingText[0]) {
      agencyBillPayments = AgencyBillPaymentArray.where(\d -> d.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      agencyBillPayments = AgencyBillPaymentArray
    }
    for (agencyBillPayment in agencyBillPayments) {
      var agencyBillPaymentLoader = new BCAgencyBillPaymentLoader()
      agencyBillPaymentLoader.createEntity(agencyBillPayment)
    }
  }

  function processBatch(tDate: Date) {
    var batches: List <BatchData>
    if (DateHandling == dateHandlingText[0]) {
      batches = BatchArray.where(\d -> d.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      batches = BatchArray
    }
    for (batch in batches) {
      var batchLoader = new BCBatchLoader()
      batchLoader.createEntity(batch)
    }
  }

  function processPaymentReversal(tDate: Date) {
    var paymentReversals: List <PaymentReversalData>
    if (DateHandling == dateHandlingText[0]) {
      paymentReversals = PaymentReversalArray.where(\d -> d.EntryDate.AsUIStyle == tDate.AsUIStyle)
    } else {
      paymentReversals = PaymentReversalArray
    }
    for (paymentReversal in paymentReversals) {
      var PaymentReversalLoader = new BCPaymentReversalLoader()
      PaymentReversalLoader.createEntity(paymentReversal)
    }
  }

  private function importFile() {
    try {
      BCSampleExcelFileParser.importSampleDataFile(Path, this)
      if (!IsLoadingFromConfig) {
        TempFile.deleteOnExit()
      }
    } catch (e: Exception) {
      throw e.toString()
    }
  }

  public function setProcessDates() {
    /* 
    * If using the imported Create Date, then build the array of processDates for selected sheets.
    * The other date handling methods just use a single date
    */
    if (DateHandling == dateHandlingText[1]) {
      ProcessDates.add(gw.api.util.DateUtil.currentDate())
    }
    if (DateHandling == dateHandlingText[2]) {
      ProcessDates.add(DateHandlingOverride)
    }
    if (DateHandling == dateHandlingText[0]) {
      if (ChkAccount) {
        for (accountData in AccountArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == accountData.EntryDate.AsUIStyle)) {
            ProcessDates.add(accountData.EntryDate)
          }
        }
      }
      if (ChkProducer) {
        for (producerData in ProducerArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == producerData.EntryDate.AsUIStyle) and withinDateRange(producerData.EntryDate)) {
            ProcessDates.add(producerData.EntryDate)
          }
        }
      }
      if (ChkIssuance) {
        for (issuanceData in IssuanceArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == issuanceData.EntryDate.AsUIStyle)and withinDateRange(issuanceData.EntryDate)) {
            ProcessDates.add(issuanceData.EntryDate)
          }
        }
      }
      if (ChkNewRenewal) {
        for (newrenewalData in NewRenewalArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == newrenewalData.EntryDate.AsUIStyle)and withinDateRange(newrenewalData.EntryDate)) {
            ProcessDates.add(newrenewalData.EntryDate)
          }
        }
      }
      if (ChkChange) {
        for (policyData in PolicyChangeArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == policyData.EntryDate.AsUIStyle)and withinDateRange(policyData.EntryDate)) {
            ProcessDates.add(policyData.EntryDate)
          }
        }
      }
      if (ChkRenew) {
        for (RenewalData in RenewalArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == RenewalData.EntryDate.AsUIStyle)and withinDateRange(RenewalData.EntryDate)) {
            ProcessDates.add(RenewalData.EntryDate)
          }
        }
      }
      if (ChkCancel) {
        for (cancellationData in CancellationArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == cancellationData.EntryDate.AsUIStyle)and withinDateRange(cancellationData.EntryDate)) {
            ProcessDates.add(cancellationData.EntryDate)
          }
        }
      }
      if (ChkDirectPayment) {
        for (DirectPayData in DirectPaymentArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == DirectPayData.EntryDate.AsUIStyle)and withinDateRange(DirectPayData.EntryDate)) {
            if (LoadAllPayments) {
              ProcessDates.add(DirectPayData.EntryDate)
            } else if (!ProcessDates.Empty and ProcessDates.max().compareIgnoreTime(DirectPayData.EntryDate) != - 1) {
              ProcessDates.add(DirectPayData.EntryDate)
            } else if (ProcessingEndDate != null and ProcessingEndDate.compareIgnoreTime(DirectPayData.EntryDate) != - 1){
              ProcessDates.add(DirectPayData.EntryDate)
            }
          }
        }
      }
      if (ChkRewrite) {
        for (RewriteData in RewriteArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == RewriteData.EntryDate.AsUIStyle)and withinDateRange(RewriteData.EntryDate)) {
            ProcessDates.add(RewriteData.EntryDate)
          }
        }
      }
      if (ChkReinstatement) {
        for (ReinstatementData in ReinstatementArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == ReinstatementData.EntryDate.AsUIStyle)and withinDateRange(ReinstatementData.EntryDate)) {
            ProcessDates.add(ReinstatementData.EntryDate)
          }
        }
      }
      if (ChkAudit) {
        for (AuditData in AuditArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == AuditData.EntryDate.AsUIStyle)and withinDateRange(AuditData.EntryDate)) {
            ProcessDates.add(AuditData.EntryDate)
          }
        }
      }
      if (ChkAgencyBillPayment) {
        for (AgencyBillPayData in AgencyBillPaymentArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == AgencyBillPayData.EntryDate.AsUIStyle)and withinDateRange(AgencyBillPayData.EntryDate)) {
            if (LoadAllAgencyBillPayments) {
              ProcessDates.add(AgencyBillPayData.EntryDate)
            } else if (!ProcessDates.Empty and ProcessDates.max().compareIgnoreTime(AgencyBillPayData.EntryDate) != - 1) {
              ProcessDates.add(AgencyBillPayData.EntryDate)
            } else if (ProcessingEndDate != null and ProcessingEndDate.compareIgnoreTime(AgencyBillPayData.EntryDate) != - 1){
              ProcessDates.add(AgencyBillPayData.EntryDate)
            }
          }
        }
      }
      if (ChkBatch) {
        for (BatchData in BatchArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == BatchData.EntryDate.AsUIStyle)and withinDateRange(BatchData.EntryDate)) {
            ProcessDates.add(BatchData.EntryDate)
          }
        }
      }
      if (ChkPaymentReversal) {
        for (PaymentReversalData in PaymentReversalArray) {
          if (!ProcessDates.hasMatch(\ d -> d.AsUIStyle == PaymentReversalData.EntryDate.AsUIStyle)and withinDateRange(PaymentReversalData.EntryDate)) {
            ProcessDates.add(PaymentReversalData.EntryDate)
          }
        }
      }
    }
  }
}
