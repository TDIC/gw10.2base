package tdic.util.dataloader.export.planexport

uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.GLTransactionNameMappingData
uses java.util.HashMap
uses java.util.Collection
uses gw.api.database.Query

/**
 * US570 - General Ledger Integration
 * 11/17/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.  Export for Transaction name mappings.
 */
class BCGLTransactionNameMappingExport extends WorksheetExportUtil {

  /**
   * Standard constructor.
   */
  construct() {
  }
  
  /**
   * Gets the Excel sheet name for the GL Mappings.
   * 
   * @return A String containing the Excel sheet name for the GL Mappings
   */
  public static function getSheetName() : String {
    return GLTransactionNameMappingData.SHEET
  }
  
  /**
   * A method to fetch the entity values for the GL Mappings.
   * 
   * @return A HashMap containing the header name and values of the entity
   */
  public static function getGLMappingEntityValues () : HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(GLTransactionNameMappingData.CD), queryGLMappings(), \e -> getRowFromGLMapping(e))
  }
  
  /**
   * Queries the existing GL Mappings in the database.
   * 
   * @return A Collection<GLIntegrationTransactionMapping_TDIC> of the GL Mappings retrieved from the database
   */
  public static function queryGLMappings() : Collection<GLIntegrationTransactionMapping_TDIC> {
    var result = Query.make(GLIntegrationTransactionMapping_TDIC).select()
    return result.toCollection()
  }
  
  /**
   * Method to build column map from entity.
   * 
   * @param glMapping The GLIntegrationTransactionMapping_TDIC with the values to map
   * @return A HashMap containing the column name mapped to the entity's values
   */
  public static function getRowFromGLMapping(glMapping : GLIntegrationTransactionMapping_TDIC) : HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(GLTransactionNameMappingData.COL_ORIGINAL_TRANSACTION_NAME), glMapping.OriginalTransactionName)
    colMap.put(colIndexString(GLTransactionNameMappingData.COL_MAPPED_TRANSACTION_NAME), glMapping.MappedTransactionName)
    return colMap
  }
  
  /**
   * Method to fetch the type of each column in the entity.  Used to create cell type during excel export.
   * 
   * @return A HashMap containing the type of each column in the entity
   */
  static function getGLMappingColumnTypes() : HashMap {
    return getColumnTypes(GLTransactionNameMappingData.CD)
  }

}
