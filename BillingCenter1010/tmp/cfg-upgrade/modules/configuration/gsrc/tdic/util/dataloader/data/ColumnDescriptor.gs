package tdic.util.dataloader.data

uses org.apache.poi.ss.usermodel.Cell

class ColumnDescriptor {
  // constants
  // types defined here should align to the values of the corresponding cell types
  public static final var TYPE_STRING: int = Cell.CELL_TYPE_STRING
  public static final var TYPE_NUMBER: int = Cell.CELL_TYPE_NUMERIC
  public static final var TYPE_DATE: int = 6
  // not a defined Excel/POI cell type but used for formatting during export

  construct(myIndex: int, myHeader: String, myType: int) {
    ColIndex = myIndex
    ColType = myType
    ColHeader = myHeader
  }
  private var _index   : int    as ColIndex
  private var _type    : int    as ColType
  private var _header  : String as ColHeader
}
