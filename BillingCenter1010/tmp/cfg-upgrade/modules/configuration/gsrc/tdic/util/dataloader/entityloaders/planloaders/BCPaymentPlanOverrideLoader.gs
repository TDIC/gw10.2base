package tdic.util.dataloader.entityloaders.planloaders

//uses entity.PaymentPlan

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.PaymentPlanOverrideData
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCPaymentPlanOverrideLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aPaymentPlanOverrideData = applicationData as PaymentPlanOverrideData
    try {
      var currPaymentPlan = GeneralUtil.findPaymentPlanByName(aPaymentPlanOverrideData.LookupPaymentPlanData)
      // add if not on file and Exclude is false
      if (currPaymentPlan != null){
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          bundle.add(currPaymentPlan)
          // Create Charge Slicing Overrides
          var currOverride = currPaymentPlan.getOverridesFor(typekey.BillingInstruction.get(aPaymentPlanOverrideData.OverrideBillingInstruction))
          var Overrides: ChargeSlicingOverrides
          if (currOverride == null) {
            // create new
            Overrides = new ChargeSlicingOverrides()
            currPaymentPlan.setOverridesFor(typekey.BillingInstruction.get(aPaymentPlanOverrideData.OverrideBillingInstruction), Overrides)
          } else {
            // update existing
            Overrides = bundle.add(currOverride)
          }
          Overrides.DownPaymentPercent = aPaymentPlanOverrideData.DownPaymentPercent
          Overrides.MaximumNumberOfInstallments = aPaymentPlanOverrideData.MaximumNumberOfInstallments
          Overrides.DaysFromReferenceDateToDownPayment = aPaymentPlanOverrideData.DaysFromReferenceDateToDownPayment
          Overrides.DownPaymentAfter = aPaymentPlanOverrideData.DownPaymentAfter
          Overrides.DaysFromReferenceDateToFirstInstallment = aPaymentPlanOverrideData.DaysFromReferenceDateToFirstInstallment
          Overrides.FirstInstallmentAfter = aPaymentPlanOverrideData.FirstInstallmentAfter
          Overrides.DaysFromReferenceDateToOneTimeCharge = aPaymentPlanOverrideData.DaysFromReferenceDateToOneTimeCharge
          Overrides.OneTimeChargeAfter = aPaymentPlanOverrideData.OneTimeChargeAfter
          aPaymentPlanOverrideData.Skipped = false
          if (!Overrides.New) {
            if (Overrides.getChangedFields().Count > 0) {
              aPaymentPlanOverrideData.Updated = true
            } else {
              aPaymentPlanOverrideData.Unchanged = true
            }
          }
          bundle.commit()
        })
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("PaymentPlan Override Load: " + aPaymentPlanOverrideData.LookupPaymentPlanData + e.toString())
      }
      aPaymentPlanOverrideData.Error = true
    }
  }
}