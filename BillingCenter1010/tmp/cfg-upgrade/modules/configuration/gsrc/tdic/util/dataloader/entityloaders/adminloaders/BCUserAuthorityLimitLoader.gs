package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.admindata.UserAuthorityLimitData
uses tdic.util.dataloader.data.admindata.AuthorityLimitTypeData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses java.util.ArrayList
uses gw.command.demo.GeneralUtil
uses java.math.BigDecimal
uses java.lang.Exception

class BCUserAuthorityLimitLoader extends BCLoader {
  construct() {
  }

  public function createEntityWithLimitType(applicationData: ApplicationData, limitTypeArray: ArrayList <AuthorityLimitTypeData>) {
    var aUserAuthorityLimitData = applicationData as UserAuthorityLimitData
    // Check for valid user
    var aUser = BCAdminDataLoaderUtil.findUserByPublicID(aUserAuthorityLimitData.UserPublicID)
    var oldProfile = aUser.AuthorityProfile
    var oldLimits = oldProfile?.Limits?.orderBy(\i -> i.LimitType)
    if (aUser != null) {
      // and aUser.AuthorityProfile == null){
      try {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          aUser = bundle.add(aUser)
          if (aUserAuthorityLimitData.Profile != null) {
            // set User Authority Limit Profile to existing profile
            aUser.AuthorityProfile = GeneralUtil.findAuthorityLimitProfileByPublicId(aUserAuthorityLimitData.Profile.PublicID)
          }
          else {
            /* Custom profile -- add new Profile and Auth Limits then attach to User
            *  Limit values are stored in LimitAmounts array; Limit Types are stored in LimitTypeData with a column reference
            *  Step thru Limit Amount array and fetch the LimitType based on the column
            */
            // define PublicID that is unique for user's custom authority profile by using username (if undefined)
            var publicID = aUser.Credential.DisplayName
            if (aUserAuthorityLimitData.AuthorityLimitPublicID != null and
                aUserAuthorityLimitData.AuthorityLimitPublicID != "") {
              publicID = aUserAuthorityLimitData.AuthorityLimitPublicID
            }
            // reuse any existing "Custom" profile with the target Public ID in the DB
            var aAuthLimitProfile = BCAdminDataLoaderUtil.findCustomAuthorityLimitProfileByPublicID(publicID)
            if (aAuthLimitProfile == null) {
              // create new profile if no "Custom" profile for the target Public ID exists
              aAuthLimitProfile = new AuthorityLimitProfile()
            } else {
              // update existing profile
              aAuthLimitProfile = bundle.add(aAuthLimitProfile)
            }
            aAuthLimitProfile.PublicID = publicID
            aAuthLimitProfile.Name = "Custom"
            var x = 0
            // counter for columns
            for (limit in aUserAuthorityLimitData.LimitAmounts) {
              var limitType = limitTypeArray.firstWhere(\p -> p.Column == x + 5).LimitType
              var aAuthorityLimit = aAuthLimitProfile.Limits.firstWhere(\a -> a.LimitType == typekey.AuthorityLimitType.get(limitType))
              if (limit != null and limit as String != "") {
                // create or update existing limit
                if (aAuthorityLimit == null) {
                  // create
                  aAuthorityLimit = new AuthorityLimit()
                  aAuthorityLimit.Profile = aAuthLimitProfile
                  aAuthorityLimit.PublicID = aUserAuthorityLimitData.AuthorityLimitPublicID + ":" + x
                  aAuthorityLimit.LimitType = typekey.AuthorityLimitType.get(limitType)
                  aAuthorityLimit.LimitAmount = (limit as BigDecimal).ofDefaultCurrency()
                  aAuthLimitProfile.addToLimits(aAuthorityLimit)
                } else {
                  // update
                  aAuthorityLimit.LimitAmount = (limit as BigDecimal).ofDefaultCurrency()
                }
              } else if (aAuthorityLimit != null) {
                // remove any existing limit
                aAuthLimitProfile.removeFromLimits(aAuthorityLimit)
              }
              x = x + 1
            }
            aUser.AuthorityProfile = aAuthLimitProfile
          }
          // update status of user auth limits from user's point of view
          aUserAuthorityLimitData.Skipped = false
          if (oldProfile == aUser.AuthorityProfile) {
            var newLimits = aUser.AuthorityProfile.Limits.orderBy(\a -> a.LimitType)
            if (oldLimits.Count == newLimits.Count) {
              var i = 0
              while (i < oldLimits.Count and oldLimits[i].LimitAmount == newLimits[i].LimitAmount) {
                i++
              }
              if (i == oldLimits.Count) {
                // all elements equal
                aUserAuthorityLimitData.Unchanged = true
              } else {
                // at least one didn't match
                aUserAuthorityLimitData.Updated = true
              }
            } else {
              aUserAuthorityLimitData.Updated = true
            }
          } else if (oldProfile != null) {
            aUserAuthorityLimitData.Updated = true
          } else {
            // if there was no associated profile before this is considered to be loaded, not updated
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      } catch (e: Exception) {
        if (LOG.InfoEnabled){
          LOG.info("User Authority Limit Load: " + aUserAuthorityLimitData.UserReferenceName + " " + e.toString())
        }
        aUserAuthorityLimitData.Error = true
      }
    }
  }
}