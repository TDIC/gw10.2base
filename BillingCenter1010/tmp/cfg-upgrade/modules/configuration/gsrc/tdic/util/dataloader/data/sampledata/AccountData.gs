package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData
uses java.lang.Integer
uses java.util.Date

class AccountData extends ApplicationData {

  // constants
  public static final var COL_ACCOUNT_NAME : int = 0
  public static final var COL_ACCOUNT_NUMBER : int = 1
  public static final var COL_ACCOUNT_BILLING_LEVEL: int = 2
  public static final var COL_ACCOUNT_FEIN : int = 3
  public static final var COL_ACCOUNT_BILL_PLAN : int = 4
  public static final var COL_ACCOUNT_DELINQUENCY : int = 5
  public static final var COL_DUE_OR_INVOICE_DAY : int = 6
  public static final var COL_DAY_OF_MONTH : int = 7
  public static final var COL_TWICE_MONTH_FIRST : int = 8
  public static final var COL_TWICE_MONTH_SECOND : int = 9
  public static final var COL_DAY_OF_WEEK : int = 10
  public static final var COL_WEEK_ANCHOR : int = 11
  public static final var COL_ACCOUNT_CONTACT_TYPE : int = 12
  public static final var COL_ACCOUNT_CONTACT_NAME : int = 13
  public static final var COL_ACCOUNT_CONTACT_FIRST_NAME : int = 14
  public static final var COL_ACCOUNT_CONTACT_LAST_NAME : int = 15
  public static final var COL_ACCOUNT_CONTACT_ADDRESS_LINE1 : int = 16
  public static final var COL_ACCOUNT_CONTACT_ADDRESS_LINE2 : int = 17
  public static final var COL_ACCOUNT_CONTACT_CITY : int = 18
  public static final var COL_ACCOUNT_CONTACT_STATE : int = 19
  public static final var COL_ACCOUNT_CONTACT_ZIP : int = 20
  public static final var COL_ACCOUNT_CONTACT_COUNTRY : int = 21
  public static final var COL_ACCOUNT_CONTACT_PHONE : int = 22
  public static final var COL_ACCOUNT_CONTACT_FAXPHONE : int = 23
  public static final var COL_ACCOUNT_CONTACT_EMAIL : int = 24
  public static final var COL_ACCOUNT_CONTACT_PRIMARY_PAYER : int = 25
  public static final var COL_ACCOUNT_CONTACT_ROLE : int = 26
  public static final var COL_ACCOUNT_CONTACT_INVOICE_DELIVERY : int = 27
  public static final var COL_CREATE_DATE : int = 28
  public static final var COL_DISTRIBUTIONLIMIT_TYPE : int = 29
  public static final var COL_ACCOUNT_TYPE : int = 30
  public static final var COL_PAYMENTMETHOD:int= 31
  public static final var COL_TOKEN:int= 32
  public static final var COL_ACCOUNT_PAYMENT_PLAN: int = 33
  public static final var COL_ACCOUNT_INVOICE_STREAM: int = 34

  construct() {

  }
  
  // properties
  private var _accountNumber              : String                 as AccountNumber
  private var _billingLevel               : String                 as BillingLevel
  private var _billPlan                   : String                 as BillPlan
  private var _accountName                : String                 as AccountName
  private var _FEIN                       : String                 as FEIN
  private var _delinquencyPlan            : String                 as DelinquencyPlan
  private var _invoiceDOM                 : Integer                as InvoiceDOM
  private var _invoiceTwiceFirst          : Integer                as InvoiceTwiceFirst
  private var _invoiceTwiceSecond         : Integer                as InvoiceTwiceSecond
  private var _invoiceDayofWeek           : String                 as InvoiceDayofWeek
  private var _invoiceWeekAnchorDate      : Date                   as InvoiceWeekAnchorDate
  private var _dueOrInvoiceInd            : String                 as DueOrInvoiceInd
  private var _invoiceDelivery            : InvoiceDeliveryMethod  as InvoiceDelivery
  private var _contact                    : ContactData            as Contact
  private var _primarypayer               : Boolean                as PrimaryPayer
  private var _role                       : AccountRole            as Role
  private var _createdate                 : Date                   as CreateDate
  private var _distlimittype              : DistributionLimitType  as DistributionLimitType
  private var _accounttype                : AccountType            as AccountType
  private var _paymentmethodtype          : PaymentMethod          as PaymentMethod
  private var _token                      : String                 as Token
  private var _paymentPlan                : String                 as PaymentPlan
  private var _invoiceStream              : String                 as InvoiceStream
  
}
