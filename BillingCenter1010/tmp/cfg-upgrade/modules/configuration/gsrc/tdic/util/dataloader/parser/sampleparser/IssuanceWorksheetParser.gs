package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.sampledata.IssuanceData
uses tdic.util.dataloader.data.sampledata.PolicyPeriodData
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.ChargeData
uses java.math.BigDecimal
uses java.lang.Integer
uses gw.util.GWBaseDateEnhancement
uses gw.api.upgrade.Coercions

class IssuanceWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  // first column is offset days; not imported
  var CREATE_DATE: int = 1
  var POLICY_NUMBER: int = 2
  var DESCRIPTION: int = 3
  //Payment Plan Modifiers
  var MATCH_PLANNED_INSTALLMENTS: int = 20
  var SUPPRESS_DOWN_PAYMENT: int = 21
  var MAX_NUM_OF_INSTALLMENTS: int = 22
  var DOWN_PAYMENT_OVERRIDE: int = 23
  var policyArray: ArrayList <PolicyPeriodData>
  function parseIssuanceRow(row: Row): IssuanceData {
    // Define and instantiate variables 
    var issuanceData = new IssuanceData()
    var charges = new ArrayList <ChargeData>()
    var policyNumber = convertCellToString(row.getCell(POLICY_NUMBER))
    var ppData: PolicyPeriodData
    if (policyArray != null){
      ppData = policyArray.firstWhere(\p -> p.PolicyNumber == policyNumber)
    }
    if (ppData == null)
      return null
    // Populate charges; each charge has 4 columns: charge, amount, payer, stream
    var i = 4
    while (i < 20) {
      var chgCell = i
      var amtCell = i + 1
      var payerCell = i + 2
      var invoiceStreamCell = i + 3
      var amt = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(amtCell as short)))
      var chg = convertCellToString(row.getCell(chgCell as short))
      var payer = convertCellToString(row.getCell(payerCell as short))
      var invoiceStream = convertCellToString(row.getCell(invoiceStreamCell as short))
      populateCharges(charges, amt, chg, payer, invoiceStream)
      i = i + 4
    }
    // Populate Payment Plan Modifiers
    var bolMatchPlannedInstallments = setBoolean(convertCellToString(row.getCell(MATCH_PLANNED_INSTALLMENTS)), false)
    var bolSuppressDownPayment = setBoolean(convertCellToString(row.getCell(SUPPRESS_DOWN_PAYMENT)), false)
    var strMaxNumOfInstallments = convertCellToString(row.getCell(MAX_NUM_OF_INSTALLMENTS))
    var strDownPaymentOverride = convertCellToString(row.getCell(DOWN_PAYMENT_OVERRIDE))
    issuanceData.MatchPlannedInstallments = bolMatchPlannedInstallments
    issuanceData.SuppressDownPayment = bolSuppressDownPayment
    if (strMaxNumOfInstallments != null && strMaxNumOfInstallments != "") {
      issuanceData.MaxNumberOfInstallmentsOverride = Coercions.makeIntFrom(strMaxNumOfInstallments)
    }
    if (strDownPaymentOverride != null && strDownPaymentOverride != "") {
      issuanceData.DownPaymentOverridePercentage = (Coercions.makeBigDecimalFrom(strDownPaymentOverride)).multiply(100)
    }
    // Populate Issuance Data
    issuanceData.EntryDate = row.getCell(CREATE_DATE).DateCellValue
    if (issuanceData.EntryDate == null  or issuanceData.EntryDate < gw.api.util.DateUtil.currentDate()) {
      issuanceData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    issuanceData.IssuancePolicyPeriod = ppData
    issuanceData.Description = convertCellToString(row.getCell(DESCRIPTION))
    issuanceData.Charges = (charges?.toTypedArray())
    return issuanceData
  }

  override function parseSheet(sheet: Sheet): ArrayList <IssuanceData> {
    return null
    //Use parseSheetandAddPolicyPeriod instead
  }

  public function parseSheetandAddPolicyPeriod(sheet: Sheet, pcyArray: ArrayList <PolicyPeriodData>): ArrayList <IssuanceData> {
    policyArray = pcyArray
    return super.parseSheet(sheet, \r -> parseIssuanceRow(r), false)
  }
}