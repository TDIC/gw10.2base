package tdic.util.dataloader.export

uses java.util.HashMap
uses java.io.File
uses java.io.FileOutputStream
uses java.io.FileInputStream
uses org.apache.poi.ss.usermodel. *
uses org.apache.poi.xssf.usermodel.XSSFWorkbook
uses org.apache.poi.hssf.usermodel.HSSFWorkbook
uses java.lang.Double
uses gw.api.util.DisplayableException
uses java.util.Date
uses gw.api.document.DocumentContentsInfo
uses java.lang.Exception
uses tdic.util.dataloader.util.DataLoaderUtil
uses tdic.util.dataloader.data.ColumnDescriptor
uses java.lang.Comparable
uses org.slf4j.LoggerFactory
uses gw.api.upgrade.Coercions

class ExcelExport {
  public static final var LOG: org.slf4j.Logger = LoggerFactory.getLogger("DataLoader.Export")
  private var excelFile: File
  construct(filePath: String) {
    excelFile = new File(filePath)
  }

  /* @param : Excel workook, Excel sheet name, sheet data & column types
     @desc  : Reads value from the hashmap and writes it into an excel sheet
  */

  function writeSheet(workbook: Workbook, sheetName: String, excelData: HashMap, colTypes: HashMap) {
    LOG.info(sheetName + " sheet: Writing data started ***")
    var rowCount: int = 0
    var cellCount: int = 0
    var dateStyle = workbook.createCellStyle()
    var sheet = workbook.createSheet(sheetName)
    var font = workbook.createFont()
    var headerStyle = workbook.createCellStyle()
    var colStyle = workbook.createCellStyle()
    // Header with 90 rotation in the text
    var headerStyle2 = workbook.createCellStyle()
    // Create header style
    headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex())
    headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND)
    headerStyle.setBorderBottom(BorderStyle.THIN)
    headerStyle.setBorderTop(BorderStyle.THIN)
    headerStyle.setBorderLeft(BorderStyle.THIN)
    font.setFontName("Arial")
    font.setFontHeightInPoints(8);
    headerStyle.setFont(font)
    headerStyle.setWrapText(true)
    // Create style for columns other than headers
    colStyle.cloneStyleFrom(headerStyle)
    colStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex())
    // Create specific style for dates
    dateStyle.cloneStyleFrom(colStyle)
    dateStyle.setDataFormat(workbook.createDataFormat().getFormat(DataLoaderUtil.getDateFormat()))
    // Create anchor factory for comments
    var factory = workbook.getCreationHelper()
    if (sheetName == "RolePrivilege" or sheetName == "UserAuthorityLimit") {
      headerStyle2.cloneStyleFrom(headerStyle)
      headerStyle2.setRotation(90)
    }
    for (rowKey in excelData.keySet().orderBy(\o -> o as String)) {
      var row = sheet.createRow(rowCount)
      var rowMap = excelData.get(rowKey) as HashMap
      for (cellKey in rowMap.Keys.orderBy(\o -> o as String)) {
        var cell = row.createCell(cellCount)
        var cellObj = rowMap.get(cellKey)
        var customCellType = ColumnDescriptor.TYPE_STRING
        if (colTypes != null && !colTypes.Empty) {
          customCellType = Coercions.makePIntFrom(colTypes.get(cell.ColumnIndex))
        }
        // Create a numeric cell if cell type is numeric
        if (row.RowNum != 0 && customCellType == ColumnDescriptor.TYPE_NUMBER) {
          cell.setCellType(customCellType)
          try {
            cell.setCellValue(Coercions.makeDoubleFrom(cellObj))
          } catch (ex: Exception) {
            LOG.warn(sheetName + " sheet: Conversion to Double failed at row " + cell.getRowIndex() + ", column " + cell.getColumnIndex() + " with value " + cellObj + ", exporting as String")
            cell.setCellValue(cellObj as String)
          }
        } else if (customCellType == ColumnDescriptor.TYPE_DATE && row.RowNum != 0 && cellObj != null) {
          cell.setCellStyle(dateStyle)
          try {
            cell.setCellValue(Coercions.makeDateFrom(cellObj))
          } catch (ex: Exception) {
            LOG.warn(sheetName + " sheet: Conversion to Date failed at row " + cell.getRowIndex() + ", column " + cell.getColumnIndex() + " with value " + cellObj + ", exporting as String")
            cell.setCellValue(cellObj as String)
          }
        } else {
          if (row.RowNum == 0 and sheetName == "RolePrivilege") {
            var value = cellObj as String
            var comment: String = null
            var sepIndex = value.indexOf(":")
            if (sepIndex >= 0) {
              // found ":", so spearate value from comment
              comment = value.substring(sepIndex + 1)
              value = value.substring(0, sepIndex)
            }
            cell.setCellValue(value)
            if (comment != null) {
              // set comment
              // comments are text boxes that need an anchor to be visible
              var drawing = sheet.createDrawingPatriarch()
              var anchor = factory.createClientAnchor()
              // make comment visible in 5x1 space
              anchor.setCol1(cell.getColumnIndex())
              anchor.setCol2(cell.getColumnIndex() + 5)
              anchor.setRow1(cell.getRowIndex())
              anchor.setRow2(cell.getRowIndex() + 1)
              // create the comment
              var commentBox = drawing.createCellComment(anchor)
              var commentString = factory.createRichTextString(comment)
              commentBox.setString(commentString)
              commentBox.setVisible(false)
              // don't make it visible initially
              cell.setCellComment(commentBox)
            }
          } else {
            cell.setCellValue(cellObj as String)
          }
        }
        if (row.RowNum == 0) {
          if ((sheetName == "RolePrivilege" && cell.ColumnIndex > 0 ) or
              (sheetName == "UserAuthorityLimit" && cell.ColumnIndex > 3)) {
            cell.setCellStyle(headerStyle2)
          } else {
            cell.setCellStyle(headerStyle)
          }
        } else if (customCellType != 6 or cellObj == null){
          cell.setCellStyle(colStyle)
        }
        sheet.autoSizeColumn(cell.ColumnIndex as short)
        cellCount++
      }
      cellCount = 0
      rowCount++
    }
    sheet.createFreezePane(0, 1)
    LOG.info(sheetName + " sheet: Writing data completed ***")
  }

  public function write(excelSheet: HashMap): Object {
    var resultOK = false
    if (excelSheet != null) {
      LOG.info(excelFile.Name + " file: Writing data started ***")
      var excelWorkbook = createNewWorkbook(excelFile)
      var fileIsClosed = false
      for (sheet in excelSheet.Keys.orderBy(\o -> o as Comparable <Object>)) {
        var sData = excelSheet.get(sheet) as HashMap
        for (data in sData.Keys) {
          writeSheet(excelWorkbook, sheet as String, data as HashMap, sData.get(data) as HashMap)
        }
      }
      var fileOut: FileOutputStream
      try {
        fileOut = new FileOutputStream(excelFile)
        excelWorkbook.write(fileOut)
        fileOut.close()
        fileIsClosed = true
        resultOK = true
      } catch (e: Exception) {
        var msg = excelFile.Name + " file: Exception while writing data to file: "
        LOG.error(msg, e)
        throw new DisplayableException(msg + e)
      } finally {
        if (!fileIsClosed) fileOut.close()
      }
      LOG.info(excelFile.Name + " file: Writing data completed. File location: " + excelFile.AbsolutePath + " ***")
    }
    return new Boolean(resultOK)
  }

  /**
   * 01-Jun-2012: Anke Heinrich: download Excel file from server to client
   */
  public function download() {
    LOG.info(excelFile.Name + " file: Download started ***")
    var downloadCancelled = false
    var contentType = getContentType(excelFile)
    var is = new FileInputStream(excelFile)
    //var docInfo = new DocumentContentsInfo(DocumentContentsInfo.DOCUMENT_CONTENTS, is, contentType)
    try {
      gw.api.web.document.DocumentsHelper.renderDocumentContentsDirectly(excelFile.NameSansExtension, null)
    } catch (e: Exception) {
      /**
       * When the user cancels a download the above function throws a java.lang.RuntimeException which is cought here
       */
      var cause = e.getCause()
      if (cause.StackTraceAsString.contains("Connection reset by peer")) {
        downloadCancelled = true
      } else {
        // other exceptions are handled as real errors
        var msg = excelFile.Name + " file: Exception while downloading file: "
        LOG.error(msg, e)
        throw new DisplayableException(msg + e)
      }
    }
    if (downloadCancelled) {
      LOG.info(excelFile.Name + " file: Download cancelled ***")
    } else {
      LOG.info(excelFile.Name + " file: Download completed ***")
    }
  }

  /**
   * 01-Jun-2012: Anke Heinrich: create new Excel workbook by file extension
   */
  public static function createNewWorkbook(file: File): Workbook {
    var result: Workbook
    if (file.Extension == "xls") {
      // Excel97-2003
      result = new HSSFWorkbook()
    } else if (file.Extension.endsWith("m")) {
      // POI does not support creating macro-enabled excel files in V3.8
      var msg = file.Name + " file: Unsupported export file format: " + file.Extension
      LOG.error(msg)
      throw new DisplayableException(msg)
    } else {
      // Excel 2007 and above
      result = new XSSFWorkbook()
    }
    return result
  }

  /**
   * 01-Jun-2012: Anke Heinrich: get Excel content type by file extension
   */
  public static function getContentType(file: File): String {
    var result: String
    switch (file.Extension) {
      case "xls":
      case "xlt":
      case "xla": result = "application/vnd.ms-excel"
          break
      case "xlsx": result = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          break
      case "xltx": result = "application/vnd.openxmlformats-officedocument.spreadsheetml.template"
          break
      case "xlsm": result = "application/vnd.ms-excel.sheet.macroEnabled.12"
          break
      case "xltm": result = "application/vnd.ms-excel.template.macroEnabled.12"
          break
      case "xlam": result = "application/vnd.ms-excel.addin.macroEnabled.12"
          break
      case "xlsb": result = "application/vnd.ms-excel.sheet.binary.macroEnabled.12"
          break
    }
    return result
  }
}
