package tdic.util.dataloader.data.sampledata

uses java.math.BigDecimal
uses java.lang.Integer

class IssuanceData extends NewPolicyData {

  construct() {

  }
  private var _issuancepolicyperiod       : PolicyPeriodData      as IssuancePolicyPeriod
  //Payment Plan Modifiers
  private var _matchplannedinstallments   : boolean               as MatchPlannedInstallments
  private var _suppressdownpayment        : boolean               as SuppressDownPayment
  private var _maxnuminstallments         : Integer               as MaxNumberOfInstallmentsOverride
  private var _downpaymentoverride        : BigDecimal            as DownPaymentOverridePercentage
}
