package tdic.util.dataloader.data

uses org.apache.poi.ss.usermodel.Cell
uses typekey.AddressType
uses typekey.Country

class AddressData extends ApplicationData {
  // constants
  public static final var SHEET : String = "Address"
  public static final var COL_DISPLAY_NAME : int = 0
  public static final var COL_ADDRESS_LINE1 : int = 1
  public static final var COL_ADDRESS_LINE2 : int = 2
  public static final var COL_ADDRESS_LINE3 : int = 3
  public static final var COL_CITY : int = 4
  public static final var COL_STATE : int = 5
  public static final var COL_POSTAL_CODE : int = 6
  public static final var COL_COUNTY : int = 7
  public static final var COL_COUNTRY : int = 8
  public static final var COL_ADDRESS_TYPE : int = 9
  public static final var CD : ColumnDescriptor[] = {
      new ColumnDescriptor(COL_DISPLAY_NAME, "Display Name", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_ADDRESS_LINE1, "Address Line 1", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_ADDRESS_LINE2, "Address Line 2", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_ADDRESS_LINE3, "Address Line 3", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_CITY, "City", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_STATE, "State", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_POSTAL_CODE, "Postal Code", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_COUNTY, "County", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_COUNTRY, "Country", Cell.CELL_TYPE_STRING),
      new ColumnDescriptor(COL_ADDRESS_TYPE, "Address Type", Cell.CELL_TYPE_STRING)
  }

  construct() {
  }

  // properties
  private var _displayName                   : String          as DisplayName
  private var _addressline1                  : String          as AddressLine1
  private var _addressline2                  : String          as AddressLine2
  private var _addressline3                  : String          as AddressLine3
  private var _city                          : String          as City
  private var _state                         : String          as State
  private var _postalcode                    : String          as PostalCode
  private var _county                        : String          as County
  private var _country                       : Country         as Country
  private var _addresstype                   : AddressType     as AddressType

}