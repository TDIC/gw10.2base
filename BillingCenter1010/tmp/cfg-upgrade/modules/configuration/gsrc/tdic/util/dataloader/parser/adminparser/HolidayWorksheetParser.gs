package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.HolidayData
uses gw.api.upgrade.Coercions

class HolidayWorksheetParser extends WorksheetParserUtil {
  construct() {
  }

  function parseHolidayRow(row: Row): HolidayData {
    // Define and instantiate variables 
    var result = new HolidayData()
    // Populate Holiday Data
    result.Name = convertCellToString(row.getCell(HolidayData.COL_NAME))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(HolidayData.COL_EXCLUDE)))
    result.PublicID = convertCellToString(row.getCell(HolidayData.COL_PUBLIC_ID))
    result.OccurrenceDate = convertCellToDate(row.getCell(HolidayData.COL_OCCURRENCE_DATE))
    result.AppliesToAllZones = Coercions.makeBooleanFrom(convertCellToString(row.getCell(HolidayData.COL_ALL_ZONES)))
    return result
  }

  function parseHolidaySheet(sheet: Sheet): ArrayList <HolidayData> {
    return parseSheet(sheet, \r -> parseHolidayRow(r), true)
  }
}
