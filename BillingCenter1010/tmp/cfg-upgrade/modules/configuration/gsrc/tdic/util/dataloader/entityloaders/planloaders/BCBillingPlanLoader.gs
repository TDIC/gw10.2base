package tdic.util.dataloader.entityloaders.planloaders

uses gw.api.domain.invoice.DayQuantity
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.BillingPlanData
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCBillingPlanLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aBillingPlanData = applicationData as BillingPlanData
    try {
      var currBillingPlan = GeneralUtil.findBillingPlanByPublicId(aBillingPlanData.PublicID)
      // add or update
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var aBillingPlan: BillingPlan
        if (currBillingPlan == null) {
          // create new
          aBillingPlan = new BillingPlan()
          aBillingPlan.PublicID = aBillingPlanData.PublicID
        }
        else {
          // update existing
          aBillingPlan = bundle.add(currBillingPlan)
        }
        aBillingPlan.Name = aBillingPlanData.Name
        aBillingPlan.Description = aBillingPlanData.Description
        aBillingPlan.PaymentDueInterval = aBillingPlanData.LeadTime
        aBillingPlan.NonResponsivePmntDueInterval = aBillingPlanData.NonResponsiveLeadTime
        aBillingPlan.PaymentDueDayLogic = aBillingPlanData.FixPaymentDueDateOn
//        aBillingPlan.DunningInterval = aBillingPlanData.DunningLeadTime
        //aBillingPlan.Currencies
        aBillingPlan.EffectiveDate = aBillingPlanData.EffectiveDate
        aBillingPlan.ExpirationDate = aBillingPlanData.ExpirationDate
        aBillingPlan.InvoiceFee = aBillingPlanData.InvoiceFee.ofDefaultCurrency()
        aBillingPlan.PaymentReversalFee = aBillingPlanData.PaymentReversalFee.ofDefaultCurrency()
        aBillingPlan.SkipInstallmentFees = aBillingPlanData.SkipInstallmentFees
        aBillingPlan.Aggregation = aBillingPlanData.LineItemsShow
        aBillingPlan.SuppressLowBalInvoices = aBillingPlanData.SuppressLowBalInvoices
        aBillingPlan.LowBalanceThreshold = aBillingPlanData.LowBalanceThreshold.ofDefaultCurrency()
        aBillingPlan.LowBalanceMethod = aBillingPlanData.LowBalanceMethod
        aBillingPlan.AllowModOfManDisb = true
        aBillingPlan.ReviewDisbursementOver = aBillingPlanData.ReviewDisbursementsOver.ofDefaultCurrency()
        aBillingPlan.DelayDisbursement = aBillingPlanData.DelayDisbursement
        aBillingPlan.DisbursementOver = aBillingPlanData.AutomaticDisbursementOver.ofDefaultCurrency()
        aBillingPlan.AvailableDisbAmtType = aBillingPlanData.AvailableDisbAmtType
        aBillingPlan.CreateApprActForAutoDisb = aBillingPlanData.CreateApprActForAutoDisb
        aBillingPlan.SendAutoDisbAwaitingApproval = aBillingPlanData.SendAutoDisbAwaitingApproval
        //aBillingPlan.RequestInterval = aBillingPlanData.RequestInterval
        //aBillingPlan.ChangeDeadlineInterval = aBillingPlanData.ChangeDeadlineInterval
        //aBillingPlan.DraftInterval = aBillingPlanData.DraftInterval
        aBillingPlan.DraftDayLogic = aBillingPlanData.DraftDayLogic
        //aBillingPlan.DraftDayLogic = aBillingPlanData.LargePremiumLeadTime
        //aBillingPlan.DraftDayLogic = aBillingPlanData.LargePremiumThreshold
        aBillingPlanData.Skipped = false
        if (!aBillingPlan.New) {
          if (aBillingPlan.getChangedFields().Count > 0) {
            aBillingPlanData.Updated = true
          } else {
            aBillingPlanData.Unchanged = true
          }
        }
        bundle.commit()
      })
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("BillingPlan Load: " + aBillingPlanData.PublicID + e.toString())
      }
      aBillingPlanData.Error = true
    }
  }
}