package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.ChargeData
uses tdic.util.dataloader.data.sampledata.RenewalData
uses java.math.BigDecimal
uses java.lang.Integer
uses gw.util.GWBaseDateEnhancement
uses gw.api.upgrade.Coercions

class RenewalWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  // first column is offset days; not imported  
  var RENEWAL_DATE: int = 1
  var PRIOR_POLICY_NUMBER: int = 2
  var DESCRIPTION: int = 3
  //Payment Plan Modifiers
  var MATCH_PLANNED_INSTALLMENTS: int = 20
  var SUPPRESS_DOWN_PAYMENT: int = 21
  var MAX_NUM_OF_INSTALLMENTS: int = 22
  var DOWN_PAYMENT_OVERRIDE: int = 23
  function parseRenewalRow(row: Row): RenewalData {
    var origPolicyNumber = convertCellToString(row.getCell(PRIOR_POLICY_NUMBER))
    if (origPolicyNumber.Empty || origPolicyNumber == null)
      return null
    // Define and instantiate data variables
    var renewalData = new RenewalData()
    var charges = new ArrayList <ChargeData>()
    // Populate charges; each charge has 4 columns: charge, amount, payer, stream
    var i = 4
    while (i < 20) {
      var chgCell = i
      var amtCell = i + 1
      var payerCell = i + 2
      var invoiceStreamCell = i + 3
      var amt = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(amtCell as short)))
      var chg = convertCellToString(row.getCell(chgCell as short))
      var payer = convertCellToString(row.getCell(payerCell as short))
      var invoiceStream = convertCellToString(row.getCell(invoiceStreamCell as short))
      populateCharges(charges, amt, chg, payer, invoiceStream)
      i = i + 4
    }
    // Populate Payment Plan Modifiers
    var bolMatchPlannedInstallments = setBoolean(convertCellToString(row.getCell(MATCH_PLANNED_INSTALLMENTS)), false)
    var bolSuppressDownPayment = setBoolean(convertCellToString(row.getCell(SUPPRESS_DOWN_PAYMENT)), false)
    var strMaxNumOfInstallments = convertCellToString(row.getCell(MAX_NUM_OF_INSTALLMENTS))
    var strDownPaymentOverride = convertCellToString(row.getCell(DOWN_PAYMENT_OVERRIDE))
    renewalData.MatchPlannedInstallments = bolMatchPlannedInstallments
    renewalData.SuppressDownPayment = bolSuppressDownPayment
    if (strMaxNumOfInstallments != null && strMaxNumOfInstallments != "") {
      renewalData.MaxNumberOfInstallmentsOverride = Coercions.makeIntFrom(strMaxNumOfInstallments)
    }
    if (strDownPaymentOverride != null && strDownPaymentOverride != "") {
      renewalData.DownPaymentOverridePercentage = (Coercions.makeBigDecimalFrom(strDownPaymentOverride)).multiply(100)
    }
    // Populate Renewal DTO 
    renewalData.PriorPolicyPeriod = origPolicyNumber
    renewalData.Description = convertCellToString(row.getCell(DESCRIPTION))
    renewalData.EntryDate = row.getCell(RENEWAL_DATE).DateCellValue
    if (renewalData.EntryDate == null  or renewalData.EntryDate < gw.api.util.DateUtil.currentDate()) {
      renewalData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    renewalData.Charges = (charges?.toTypedArray())
    return renewalData
  }

  override function parseSheet(sheet: Sheet): ArrayList <RenewalData> {
    return parseSheet(sheet, \r -> parseRenewalRow(r), false)
  }
}