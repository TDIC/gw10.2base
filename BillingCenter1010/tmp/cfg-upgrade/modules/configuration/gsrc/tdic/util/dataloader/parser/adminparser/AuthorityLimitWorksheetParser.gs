package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.math.BigDecimal
uses tdic.util.dataloader.data.admindata.AuthorityLimitData
uses tdic.util.dataloader.data.admindata.AuthorityLimitProfileData
uses gw.api.upgrade.Coercions

class AuthorityLimitWorksheetParser extends WorksheetParserUtil {
  construct() {
  }
  var aArray = new ArrayList <AuthorityLimitProfileData>()
  function parseSheetWithProfile(sheet: Sheet, alpArray: ArrayList <AuthorityLimitProfileData>): ArrayList <AuthorityLimitData> {
    aArray = alpArray
    return parseSheet(sheet, \r -> parseAuthorityLimitProfileRow(r), true);
  }

  function parseAuthorityLimitProfileRow(row: Row): AuthorityLimitData {
    // Define and instantiate variables 
    var result = new AuthorityLimitData()
    //get AuthLimitProfile from array
    var profileID = (convertCellToString(row.getCell(AuthorityLimitData.COL_PROFILE)))
    if (aArray != null){
      result.Profile = aArray.firstWhere(\p -> p.Name == profileID)
    }
    // Populate AuthorityLimit Data
    result.PublicID = convertCellToString(row.getCell(AuthorityLimitData.COL_PUBLIC_ID))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AuthorityLimitData.COL_EXCLUDE)))
    result.LimitType = typekey.AuthorityLimitType.get(convertCellToString(row.getCell(AuthorityLimitData.COL_LIMIT_TYPE)))
    result.LimitAmount = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(AuthorityLimitData.COL_LIMIT_AMOUNT)))
    return result
  }
}
