package tdic.util.gunit

uses gw.testharness.TestBase
uses java.util.Collection

abstract class GUnitAssert {

  private construct() {
  }

  static function assertCollectionEquals(expected : Collection<String>, actual : Collection<String>) {
    assertCollectionEquals(null, expected, actual)
  }

  static function assertCollectionEquals(userMessage : String, expected : Collection<String>, actual : Collection<String>) {
    TestBase.assertEquals(
        userMessage,
        "\n" + expected.toString().replace(", ", ",\n "),
        "\n" +   actual.toString().replace(", ", ",\n "))
  }

}
