package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.ChargeData
uses java.math.BigDecimal
uses java.lang.Integer
uses gw.util.GWBaseDateEnhancement
uses tdic.util.dataloader.data.sampledata.ReinstatementData
uses gw.api.upgrade.Coercions

class ReinstatementWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  // first column is offset days; not imported
  var REINSTATEMENT_DATE: int = 1
  var ASSOCIATED_POLICY_PERIOD: int = 2
  var DESCRIPTION: int = 3
  var MATCH_PLANNED_INSTALLMENTS: int = 20
  var SUPPRESS_DOWN_PAYMENT: int = 21
  var MAX_NUM_OF_INSTALLMENTS: int = 22
  var DOWN_PAYMENT_OVERRIDE: int = 23
  function parseReinstatementRow(row: Row): ReinstatementData {
    var policyNumber = convertCellToString(row.getCell(ASSOCIATED_POLICY_PERIOD))
    if (policyNumber.Empty || policyNumber == null)
      return null
    var reinstatementData = new ReinstatementData()
    var charges = new ArrayList <ChargeData>()
    // Populate charges; each charge has 4 columns: charge, amount, payer, stream
    var i = 4
    while (i < 20) {
      var chgCell = i
      var amtCell = i + 1
      var payerCell = i + 2
      var invoiceStreamCell = i + 3
      var amt = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(amtCell as short)))
      var chg = convertCellToString(row.getCell(chgCell as short))
      var payer = convertCellToString(row.getCell(payerCell as short))
      var invoiceStream = convertCellToString(row.getCell(invoiceStreamCell as short))
      populateCharges(charges, amt, chg, payer, invoiceStream)
      i = i + 4
    }
    // Populate Payment Plan Modifiers
    var bolMatchPlannedInstallments = setBoolean(convertCellToString(row.getCell(MATCH_PLANNED_INSTALLMENTS)), false)
    var bolSuppressDownPayment = setBoolean(convertCellToString(row.getCell(SUPPRESS_DOWN_PAYMENT)), false)
    var strMaxNumOfInstallments = convertCellToString(row.getCell(MAX_NUM_OF_INSTALLMENTS))
    var strDownPaymentOverride = convertCellToString(row.getCell(DOWN_PAYMENT_OVERRIDE))
    reinstatementData.MatchPlannedInstallments = bolMatchPlannedInstallments
    reinstatementData.SuppressDownPayment = bolSuppressDownPayment
    if (strMaxNumOfInstallments != null && strMaxNumOfInstallments != "") {
      reinstatementData.MaxNumberOfInstallmentsOverride = Coercions.makeIntFrom(strMaxNumOfInstallments)
    }
    if (strDownPaymentOverride != null && strDownPaymentOverride != "") {
      reinstatementData.DownPaymentOverridePercentage = (Coercions.makeBigDecimalFrom(strDownPaymentOverride)).multiply(100)
    }
    // Populate Reinstatement data
    reinstatementData.AssociatedPolicyPeriod = policyNumber
    reinstatementData.ModificationDate = row.getCell(REINSTATEMENT_DATE).DateCellValue
    if (reinstatementData.ModificationDate == null  or reinstatementData.ModificationDate < gw.api.util.DateUtil.currentDate()) {
      reinstatementData.ModificationDate = gw.api.util.DateUtil.currentDate()
    }
    reinstatementData.Description = convertCellToString(row.getCell(DESCRIPTION))
    reinstatementData.EntryDate = row.getCell(REINSTATEMENT_DATE).DateCellValue
    reinstatementData.Charges = (charges?.toTypedArray())
    return reinstatementData
  }

  override function parseSheet(sheet: Sheet): ArrayList <ReinstatementData> {
    return parseSheet(sheet, \r -> parseReinstatementRow(r), false)
  }
}