package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.CommissionPlanData
uses tdic.util.dataloader.data.plandata.CommissionSubPlanData
uses java.util.ArrayList
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCCommissionPlanLoader extends BCLoader {
  construct() {
  }

  function createCommissionPlanWithSubPlan(applicationData: ApplicationData,
                                           commissionSubPlanArray: ArrayList <CommissionSubPlanData>) {
    var aCommissionPlanData = applicationData as CommissionPlanData
    try {
      var currCommissionPlan = GeneralUtil.findCommissionPlanByPublicId(aCommissionPlanData.PublicID)
      // get default SubPlan based on the Commission Plan Name
      var aSubPlanData = commissionSubPlanArray.firstWhere(\s -> s.PlanName == aCommissionPlanData.Name
          and s.SubPlanName == "default")
      // add or update
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var aCommissionPlan: CommissionPlan
        if (currCommissionPlan == null) {
          // create new
          aCommissionPlan = new CommissionPlan()
          aCommissionPlan.PublicID = aCommissionPlanData.PublicID
        }
        else {
          // update existing
          aCommissionPlan = bundle.add(currCommissionPlan)
        }
        aCommissionPlan.Name = aCommissionPlanData.Name
        aCommissionPlan.EffectiveDate = aCommissionPlanData.EffectiveDate
        aCommissionPlan.ExpirationDate = aCommissionPlanData.ExpirationDate
        //aCommissionPlan.Currencies
        aCommissionPlan.setAllowedTier(TC_GOLD, aCommissionPlanData.AllowedTiers_Gold)
        aCommissionPlan.setAllowedTier(TC_SILVER, aCommissionPlanData.AllowedTiers_Silver)
        aCommissionPlan.setAllowedTier(TC_BRONZE, aCommissionPlanData.AllowedTiers_Bronze)
        // add Default SubPlan
        var aSubPlan = aCommissionPlan.DefaultSubPlan
        aSubPlan.Name = "default"
        aSubPlan.PayableCriteria = typekey.PayableCriteria.get(aSubPlanData.Earn_Commissions)
        aSubPlan.SuspendForDelinquency = aSubPlanData.Suspend_for_Delinquency
        // Default Rates
        // set/reset rate for all roles, not just the defined ones (needed for update case)
        for (role in {"primary", "secondary", "referrer"}) {
          var rateData = aSubPlanData.SubPlanRates.firstWhere(\c -> c.Role == role)
          var rate = (rateData != null) ? rateData.Rate : 0
          aSubPlan.setBaseRate(typekey.PolicyRole.get(role), rate)
        }
        // Commissionable Items
        var newItems = new ArrayList <ChargePattern> ()
        for (aItem in aSubPlanData.CommissionItems) {
          var chrgItem = GeneralUtil.findChargePatternByChargeCode(aItem.ChargePattern)
          newItems.add(chrgItem)
        }
        var existingItems = new ArrayList <ChargePattern> ()
        existingItems.addAll(aSubPlan.CommissionableChargeItems*.ChargePattern?.toList() as ArrayList <ChargePattern>)
        var addedItems = newItems.subtract(existingItems)
        var removedItems = existingItems.subtract(newItems)
        // Add any new charge items to the subplan
        for (addItem in addedItems.where(\c -> c != null)) {
          var commChrgItem = new CommissionableChargeItem(bundle)
          commChrgItem.ChargePattern = addItem
          aSubPlan.addToCommissionableChargeItems(commChrgItem)
        }
        // Remove deleted charge items from subplan
        for (remItem in removedItems) {
          var commChrgItem = aSubPlan.CommissionableChargeItems.firstWhere(\c -> c.ChargePattern == remItem)
          aSubPlan.removeFromCommissionableChargeItems(commChrgItem)
        }
        if (!aCommissionPlan.New) {
          if (aCommissionPlan.getChangedFields().Count > 0) {
            aCommissionPlanData.Updated = true
          } else {
            aCommissionPlanData.Unchanged = true
          }
        }
        aCommissionPlanData.Skipped = false
        if (!aSubPlan.New) {
          if (aSubPlan.getChangedFields().Count > 0) {
            aSubPlanData.Updated = true
          } else {
            aSubPlanData.Unchanged = true
          }
        }
        aSubPlanData.Skipped = false
        bundle.commit()
      })
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("CommissionPlan Load: " + aCommissionPlanData.PublicID + e.toString())
      }
      aCommissionPlanData.Error = true
    }
  }
}