package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses java.math.BigDecimal
uses tdic.util.dataloader.data.ColumnDescriptor

class AuthorityLimitData extends ApplicationData {

  // constants
  public static final var SHEET : String = "AuthorityLimit"
  public static final var COL_PROFILE : int = 0
  public static final var COL_PUBLIC_ID : int = 1
  public static final var COL_EXCLUDE : int = 2
  public static final var COL_LIMIT_TYPE : int = 3
  public static final var COL_LIMIT_AMOUNT : int = 4
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_PROFILE, "Profile", ColumnDescriptor.TYPE_STRING),  
    new ColumnDescriptor(COL_PUBLIC_ID, "Public-ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_LIMIT_TYPE, "Limit Type", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_LIMIT_AMOUNT, "Limit Amount", ColumnDescriptor.TYPE_NUMBER)
  }


  construct() {

  }

  // properties
  private var _profile                 : AuthorityLimitProfileData    as Profile
  private var _limittype               : AuthorityLimitType           as LimitType
  private var _limitamount             : BigDecimal                   as LimitAmount

}
