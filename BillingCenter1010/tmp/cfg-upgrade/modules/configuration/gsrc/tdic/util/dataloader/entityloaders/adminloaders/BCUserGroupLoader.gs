package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.admindata.UserGroupData
uses tdic.util.dataloader.data.admindata.UserGroupColumnData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses java.util.ArrayList
uses gw.command.demo.GeneralUtil
uses java.lang.Integer
uses java.lang.Exception
uses gw.api.upgrade.Coercions

class BCUserGroupLoader extends BCLoader {
  construct() {
  }

  public function createEntityWithGroup(applicationData: ApplicationData, userGroupColumnArray: ArrayList <UserGroupColumnData>) {
    var aUserGroupData = applicationData as UserGroupData
    // Check for valid user 
    var aUser = BCAdminDataLoaderUtil.findUserByPublicID(aUserGroupData.UserPublicID)
    if (aUser != null){
      try {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          aUser = bundle.add(aUser)
          var oldGroups = aUser.GroupUsers.copy().orderBy(\g -> g.Group)
          /* add new GroupUser and attach to User and Group
          *  Load Factors are stored in LoadFactors array; Group Names are stored in UserGroupColumnData with a column reference
          *  Step thru LoadFactor array and fetch the Group based on the column
          *  Parse the LoadFactors for trailing characters:
          *       M = manager, A = load factor admin, B = both
          */
          var x = 3
          // counter for columns, start with third column of sheet
          for (loadFactor in aUserGroupData.LoadFactor) {
            var aGroup = GeneralUtil.findGroupByUserName(userGroupColumnArray.firstWhere(\p -> p.Column == x).Group)
            if (aGroup != null) {
              aGroup = bundle.add(aGroup)
              var currGroup = BCAdminDataLoaderUtil.findUserGroup(aUser, aGroup)
              // If load factor is not null, check whether the user is already a part of the group
              if (loadFactor != null and loadFactor as String != "") {
                var aUserGroup: GroupUser
                // If the user is not a part of the group then create a new group user
                if (currGroup == null) {
                  aUserGroup = new GroupUser()
                } else {
                  // If the user is already a part of the group then update the group user
                  aUserGroup = bundle.add(currGroup)
                }
                aUserGroup.Member = true
                aUserGroup.LoadFactorType = TC_LOADFACTORVIEW
                aUserGroup.User = aUser
                aUserGroup.Group = aGroup
                /* parse the LoadFactor for trailing characters
                * :M = user is a manager
                * :A = user is a load factor admin
                * :B = user is both
                */
                var loadString = loadFactor.toString()
                var pos = loadString.indexOf(":")
                var factor = 0
                if (pos > 0) {
                  factor = Coercions.makeIntFrom(loadString.substring(0, pos))
                  var c = loadString.substring(pos + 1)
                  if (c == "M" or c == "B") {
                    aUserGroup.Manager = true
                  }
                  if (c == "A" or c == "B") {
                    aUserGroup.LoadFactorType = TC_LOADFACTORADMIN
                  }
                }
                else {
                  factor = Coercions.makeIntFrom(loadString)
                }
                aUserGroup.LoadFactor = factor
                //(can't set this - get system permission error on ManageLoadFactorsPermissionHandler)
                aUser.addToGroupUsers(aUserGroup)
                aGroup.addToUsers(aUserGroup)
              } else {
                // If the load factor is null and the user is already a part of the group then
                // remove the user group from the user.
                if (currGroup != null) {
                  aUser.removeFromGroupUsers(currGroup)
                  // redundant: aGroup.removeFromUsers(currGroup)
                }
              }
            }
            x = x + 1
          }
          // update status of user group associations from user's point of view
          aUserGroupData.Skipped = false
          if (oldGroups.Count == aUser.GroupUsers.Count) {
            var newGroups = aUser.GroupUsers.orderBy(\g -> g.Group.Name)
            var i = 0
            while (i < oldGroups.Count and oldGroups[i] == newGroups[i]) {
              i++
            }
            if (i == oldGroups.Count) {
              // all elements equal
              aUserGroupData.Unchanged = true
            } else {
              // at least one didn't match
              aUserGroupData.Updated = true
            }
          } else if (oldGroups.Count > 0) {
            aUserGroupData.Updated = true
          } else {
            // if there was no associated groups before this is considered to be loaded, not updated
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      } catch (e: Exception) {
        if (LOG.InfoEnabled){
          LOG.info("User Group Load: " + aUserGroupData.UserReferenceName + " " + e.toString())
        }
        aUserGroupData.Error = true
      }
    }
  }
}