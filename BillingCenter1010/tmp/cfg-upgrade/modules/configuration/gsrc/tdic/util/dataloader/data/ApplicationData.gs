package tdic.util.dataloader.data

uses java.util.Date

class ApplicationData {
  construct() {
  }

  // common properties
  private var _publicID: String as PublicID
  private var _entryDate: Date as EntryDate
  // status properties
  private var _error       : boolean as Error = false
  private var _skipped     : boolean as Skipped = true
  private var _updated     : boolean as Updated = false
  private var _unchanged   : boolean as Unchanged = false
  private var _exclude     : boolean as Exclude
}
