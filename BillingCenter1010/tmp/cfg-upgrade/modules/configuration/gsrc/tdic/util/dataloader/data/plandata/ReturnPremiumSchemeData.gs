package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ColumnDescriptor
uses java.lang.Number
uses java.lang.Double

class ReturnPremiumSchemeData extends PlanData {
  //constants
  public static final var SHEET: String = "ReturnPremiumScheme"
  public static final var COL_RETURN_PREMIUM_PLAN_NAME: int = 0
  public static final var COL_PRIORITY: int = 1
  public static final var COL_CONTEXT: int = 2
  public static final var COL_START_DATE_OPTION: int = 3
  public static final var COL_ALLOCATE_TIMING: int = 4
  public static final var COL_ALLOCATE_METHOD: int = 5
  public static final var COL_EXCESS_TREATMENT: int = 6
  public static final var CD: ColumnDescriptor[] = {
      new ColumnDescriptor(COL_RETURN_PREMIUM_PLAN_NAME, "Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PRIORITY, "Priority", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_CONTEXT, "Context", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_START_DATE_OPTION, "Start Date Option", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ALLOCATE_TIMING, "Allocate Timing", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ALLOCATE_METHOD, "Allocate Method", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EXCESS_TREATMENT, "Excess Treatment", ColumnDescriptor.TYPE_STRING)
  }
  construct() {
  }
  var _ReturnPremiumPlanName  : String                          as ReturnPremiumPlanName
  var _Priority               : Double                          as Priority
  var _HandlingCondition      : ReturnPremiumHandlingCondition  as HandlingCondition
  var _StartDate              : ReturnPremiumStartDateOption    as StartDateOption
  var _AllocateTiming         : ReturnPremiumAllocateTiming     as AllocateTiming
  var _AllocateMethod         : ReturnPremiumAllocateMethod     as AllocateMethod
  var _ExcessTreatment        : ReturnPremiumExcessTreatment    as ExcessTreatment
}