package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor


class PlanLocalizationData extends ApplicationData {
  // constants
  public static final var SHEET : String = "Localization"
  public static final var COL_LANGUAGE : int = 0
  public static final var COL_BILLING_PLAN : int = 1
  public static final var COL_BILLING_PLAN_NAME : int = 2
  public static final var COL_BILLING_PLAN_DESC : int = 3
  public static final var COL_PAYMENT_PLAN : int = 4
  public static final var COL_PAYMENT_PLAN_NAME : int = 5
  public static final var COL_PAYMENT_PLAN_DESC : int = 6
  public static final var COL_DELINQUENCY_PLAN : int = 7
  public static final var COL_DELINQUENCY_PLAN_NAME :int = 8
  public static final var COL_COMMISSION_PLAN : int = 9
  public static final var COL_COMMISSION_PLAN_NAME : int = 10
  public static final var COL_COMMISSION_SUBPLAN : int = 11
  public static final var COL_COMMISSION_SUBPLAN_NAME : int = 12
  public static final var COL_AGENCY_BILL_PLAN : int = 13
  public static final var COL_AGENCY_BILL_PLAN_NAME : int = 14
  public static final var COL_PAY_ALLOCATION_PLAN : int = 15
  public static final var COL_PAY_ALLOCATION_PLAN_NAME : int = 16
  public static final var COL_PAY_ALLOCATION_PLAN_DESC : int = 17
  public static final var COL_RETURN_PREMIUM_PLAN : int = 18
  public static final var COL_RETURN_PREMIUM_PLAN_NAME : int = 19
  public static final var COL_RETURN_PREMIUM_PLAN_DESC : int = 20
  public static final var COL_CHARGE_PATTERN : int = 21
  public static final var COL_CHARGE_PATTERN_NAME : int = 22
  public static final var CD : ColumnDescriptor[] = {
      new ColumnDescriptor(0, "Language", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(1, "Billing Plan", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(2, "Billing Plan Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(3, "Billing Plan Desc", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(4, "Payment Plan", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(5, "Payment Plan Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(6, "Payment Plan Desc", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(7, "Delinquency Plan", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(8, "Delinquency Plan Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(9, "Commission Plan", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(10, "Commission Plan Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(11, "Commission SubPlan", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(12, "Commission SubPlan Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(13, "Agency Bill Plan", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(14, "Agency Bill Plan Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(15, "Pay Allocation Plan", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(16, "Pay Allocation Plan Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(17, "Pay Allocation Plan Desc", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(18, "Return Premium Plan", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(19, "Return Premium Plan Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(20, "Return Premium Plan Desc", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(21, "Charge Pattern", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(22, "Charge Pattern Name", ColumnDescriptor.TYPE_STRING)

  }

  construct() {
  }

  // properties
  private var _language              : LanguageType   as Language
  private var _billingplan           : String         as BillingPlan
  private var _billingplanname       : String         as BillingPlanName
  private var _billingplandesc       : String         as BillingPlanDesc
  private var _paymentplan           : String         as PaymentPlan
  private var _paymentplanname       : String         as PaymentPlanName
  private var _paymentplandesc       : String         as PaymentPlanDesc
  private var _delinquencyplan       : String         as DelinquencyPlan
  private var _delinquencyplanname   : String         as DelinquencyPlanName
  private var _commissionplan        : String         as CommissionPlan
  private var _commissionplanname    : String         as CommissionPlanName
  private var _commissionsubplan     : String         as CommissionSubPlan
  private var _commissionsubplanname : String         as CommissionSubPlanName
  private var _agencybillplan        : String         as AgencyBillPlan
  private var _agencybillplanname    : String         as AgencyBillPlanName
  private var _payallocationplan     : String         as PayAllocationPlan
  private var _payallocationplanname : String         as PayAllocationPlanName
  private var _payallocationplandesc : String         as PayAllocationPlanDesc
  private var _returnpremiumplan     : String         as ReturnPremiumPlan
  private var _returnpremiumplanname : String         as ReturnPremiumPlanName
  private var _returnpremiumplandesc : String         as ReturnPremiumPlanDesc
  private var _chargepattern         : String         as ChargePattern
  private var _chargepatternname     : String         as ChargePatternName
}