package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.CommissionSubPlanData
uses java.util.Collection
uses gw.api.upgrade.Coercions

class BCCommissionSubPlanExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return CommissionSubPlanData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getCommSubPlanEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(CommissionSubPlanData.CD), queryCommSubPlans(), \e -> getRowFromCommSubPlan(e))
  }

  // query data

  public static function queryCommSubPlans(): Collection <CondCmsnSubPlan> {
    var result = Query.make(CondCmsnSubPlan).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromCommSubPlan(plan: CondCmsnSubPlan): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(CommissionSubPlanData.COL_PLAN_NAME), plan.CommissionPlan)
    colMap.put(colIndexString(CommissionSubPlanData.COL_SUBPLAN_NAME), plan.Name)
    colMap.put(colIndexString(CommissionSubPlanData.COL_PRIORITY), plan.Priority)
    // common subplan data
    var subplan = plan
    colMap.put(colIndexString(CommissionSubPlanData.COL_PRIMARY_RATE), subplan.primary["Rate"])
    colMap.put(colIndexString(CommissionSubPlanData.COL_SECONDARY_RATE), subplan.secondary["Rate"])
    colMap.put(colIndexString(CommissionSubPlanData.COL_REFERRER_RATE), subplan.referrer["Rate"])
    colMap.put(colIndexString(CommissionSubPlanData.COL_EARN_COMMISSIONS), subplan.PayableCriteria)
    colMap.put(colIndexString(CommissionSubPlanData.COL_SUSPEND_FOR_DELINQUENCY), subplan.SuspendForDelinquency)
    var incentive: PremiumIncentive = null
    // import of incentives not supported yet, so don't bother exporting the values
    /*
    if (subplan.Incentives.Count > 0 and subplan.Incentives[0] typeis PremiumIncentive) {
      incentive = subplan.Incentives[0] as PremiumIncentive
    }
    */
    colMap.put(colIndexString(CommissionSubPlanData.COL_PREMIUM_INCENTIVE_BONUS_PERCENTAGE), incentive != null ? incentive.BonusPercentage : Coercions.makeBigDecimalFrom(""))
    colMap.put(colIndexString(CommissionSubPlanData.COL_PREMIUM_INCENTIVE_THRESHOLD), incentive != null ? incentive.Threshold : "")
    // get commissionable charge items
    var codes = subplan.CommissionableChargeItems*.ChargePattern*.ChargeCode
    colMap.put(colIndexString(CommissionSubPlanData.COL_COMMISSIONABLE_ITEM1), codes.Count > 0 ? codes[0] : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_COMMISSIONABLE_ITEM2), codes.Count > 1 ? codes[1] : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_COMMISSIONABLE_ITEM3), codes.Count > 2 ? codes[2] : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_COMMISSIONABLE_ITEM4), codes.Count > 3 ? codes[3] : "")
    // get conditional subplan data
    colMap.put(colIndexString(CommissionSubPlanData.COL_ASSIGNED_RISK), plan.AssignedRisk)
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_EVALUATIONS), plan.AllEvaluations)
    // export LOB codes
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_LOB_CODES), plan.AllLOBCodes)
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_LOB_CODES + 1), forLOB(plan, LOBCode.TC_WORKERSCOMP) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_LOB_CODES + 2), forLOB(plan, LOBCode.TC_COMMERCIALPROPERTY) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_LOB_CODES + 3), forLOB(plan, LOBCode.TC_INLANDMARINE) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_LOB_CODES + 4), forLOB(plan, LOBCode.TC_GENERALLIABILITY) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_LOB_CODES + 5), forLOB(plan, LOBCode.TC_BUSINESSAUTO) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_LOB_CODES + 6), forLOB(plan, LOBCode.TC_PERSONALAUTO) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_LOB_CODES + 7), forLOB(plan, LOBCode.TC_BUSINESSOWNERS) ? "true" : "")
    // export segments
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_SEGMENTS), plan.AllSegments)
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_SEGMENTS + 1), forSegment(plan, ApplicableSegments.TC_LARGEBUSINESS) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_SEGMENTS + 2), forSegment(plan, ApplicableSegments.TC_MEDIUMBUSINESS) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_SEGMENTS + 3), forSegment(plan, ApplicableSegments.TC_PERSONAL) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_SEGMENTS + 4), forSegment(plan, ApplicableSegments.TC_SMALLBUSINESS) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_SEGMENTS + 5), forSegment(plan, ApplicableSegments.TC_SUBPRIME) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_JURISDICTIONS), plan.AllJurisdictions)
    // to be extended for state selection
    // export terms
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_TERMS), plan.AllTerms)
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_TERMS + 1), forTerm(plan, 0) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_TERMS + 2), forTerm(plan, 1) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_TERMS + 3), forTerm(plan, 2) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_TERMS + 4), forTerm(plan, 3) ? "true" : "")
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_TERMS + 5), forTerm(plan, 4) ? "true" : "")
    // export uw companies
    colMap.put(colIndexString(CommissionSubPlanData.COL_ALL_UW_COMPANIES), plan.AllUWCompanies)
    // to be extended for uw company selection
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getCommSubPlanColumnTypes(): HashMap {
    return getColumnTypes(CommissionSubPlanData.CD)
  }

  // helper function to check if the plan is applicable fore the the given LOBCode

  static function forLOB(plan: CondCmsnSubPlan, lobCode: LOBCode): boolean {
    return !plan.AllLOBCodes and plan.LOBCodes.hasMatch(\e -> e.Selected and e.toString() == lobCode.DisplayName)
  }

  // helper function to check if the plan is applicable fore the the given segment

  static function forSegment(plan: CondCmsnSubPlan, segment: ApplicableSegments): boolean {
    return !plan.AllSegments and plan.SelectedSegments.hasMatch(\e -> e.Selected and e.toString() == segment.DisplayName)
  }

  // helper function to check if the plan is applicable fore the the given term

  static function forTerm(plan: CondCmsnSubPlan, term: int): boolean {
    return !plan.AllTerms and
        (term < 4 ? plan.getRenewalRange(term, term + 1).Covered : plan.getRenewalRangeOnOrAfter(term).Covered)
  }
}
