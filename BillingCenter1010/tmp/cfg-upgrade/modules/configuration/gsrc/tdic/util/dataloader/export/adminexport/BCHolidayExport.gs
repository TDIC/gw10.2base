package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.data.admindata.HolidayData
uses tdic.util.dataloader.export.WorksheetExportUtil
uses java.util.Collection
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCHolidayExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return HolidayData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getHolidayEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(HolidayData.CD), queryHolidays(), \e -> getRowFromHoliday(e))
  }

  // query data

  public static function queryHolidays(): Collection <Holiday> {
    var result = Query.make(Holiday).select().orderBy(
      QuerySelectColumns.path(Paths.make(entity.Holiday#OccurrenceDate))
    )
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromHoliday(holiday: Holiday): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(HolidayData.COL_NAME), holiday.Name)
    colMap.put(colIndexString(HolidayData.COL_EXCLUDE), "false")
    colMap.put(colIndexString(HolidayData.COL_PUBLIC_ID), holiday.PublicID)
    colMap.put(colIndexString(HolidayData.COL_OCCURRENCE_DATE), holiday.OccurrenceDate)
    colMap.put(colIndexString(HolidayData.COL_ALL_ZONES), holiday.AppliesToAllZones)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getHolidayColumnTypes(): HashMap {
    return getColumnTypes(HolidayData.CD)
  }
}
