package tdic.util.dataloader.data.plandata

uses java.lang.Integer
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class PaymentPlanOverrideData extends ApplicationData {

  // constants
  public static final var SHEET : String = "PaymentPlanOverrides"
  public static final var COL_LookupPaymentPlanData : int = 0
  public static final var COL_OverrideBillingInstruction : int = 1
  public static final var COL_DOWNPAYMENT_PERCENT : int = 2
  public static final var COL_MAXIMUM_NUMBER_OF_INSTALLMENTS : int = 3
  public static final var COL_DAYS_FROM_REFERENCE_DATE_TO_DOWNPAYMENT : int = 4
  public static final var COL_DOWNPAYMENT_AFTER : int = 5
  public static final var COL_DAYS_FROM_REFERENCE_DATE_TO_FIRST_INSTALLMENT : int = 6
  public static final var COL_FIRST_INSTALLMENT_AFTER : int = 7
  public static final var COL_DOWNPAYMENT_SECOND_INSTALLMENT: int = 8
  public static final var COL_DAYS_FROM_REFERENCE_DATE_TO_SECOND_INSTALLMENT : int = 9
  public static final var COL_SECOND_INSTALLMENT_AFTER : int = 10
  public static final var COL_DAYS_FROM_REFERENCE_DATE_TO_ONETIME_CHARGE : int = 11
  public static final var COL_ONETIME_CHARGE_AFTER : int = 12
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_LookupPaymentPlanData, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_OverrideBillingInstruction, "Override BillingInstruction", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DOWNPAYMENT_PERCENT, "Down Payment (%)", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_MAXIMUM_NUMBER_OF_INSTALLMENTS, "Max # Payments (not including down payment)", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_DAYS_FROM_REFERENCE_DATE_TO_DOWNPAYMENT, "Down Payment Invoiced days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_DOWNPAYMENT_AFTER, "Down Payment days after", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DAYS_FROM_REFERENCE_DATE_TO_FIRST_INSTALLMENT, "First Installment Invoiced days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_FIRST_INSTALLMENT_AFTER, "First Installment days after", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_DOWNPAYMENT_SECOND_INSTALLMENT, "Down Payment Second Installment", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_DAYS_FROM_REFERENCE_DATE_TO_SECOND_INSTALLMENT, "Second Installment Invoiced Days" , ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_SECOND_INSTALLMENT_AFTER, "Second Installment days after", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DAYS_FROM_REFERENCE_DATE_TO_ONETIME_CHARGE, "One-Time Charges Invoiced days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_ONETIME_CHARGE_AFTER, "One-Time Charges days after", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }
  
  // properties
  var _lookuppaymentplandata                              : String                          as LookupPaymentPlanData
  var _overridebillinginstruction                         : String                          as OverrideBillingInstruction
  var _DownPaymentPercent                                 : Integer                         as DownPaymentPercent
  var _MaximumNumberOfInstallments                        : Integer                         as MaximumNumberOfInstallments
  var _DaysFromReferenceDateToDownPayment                 : Integer                         as DaysFromReferenceDateToDownPayment
  var _DownPaymentAfter                                   : PaymentScheduledAfter           as DownPaymentAfter
  var _DaysFromReferenceDateToFirstInstallment            : Integer                         as DaysFromReferenceDateToFirstInstallment
  var _FirstInstallmentAfter                              : PaymentScheduledAfter           as FirstInstallmentAfter
  var _DownPaymentSecondInstallment                       : DownPaymentSecondInstallment    as DownPaymentSecondInstallment
  var _DaysFromReferenceDateToSecondInstallment           : Integer                         as DaysFromReferenceDateToSecondInstallment
  var _SecondInstallmentAfter                             : PaymentScheduledAfter           as SecondInstallmentAfter
  var _DaysFromReferenceDateToOneTimeCharge               : Integer                         as DaysFromReferenceDateToOneTimeCharge
  var _OneTimeChargeAfter                                 : PaymentScheduledAfter           as OneTimeChargeAfter
}
