package tdic.util.dataloader.entityloaders.adminloaders

uses gw.api.database.Query
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.AdminLocalizationData
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface

class BCAdminLocalizationLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aLocalizationData = applicationData as AdminLocalizationData
    try {
      if (aLocalizationData.Language != null) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          //for each column with data, create (or update) the localization record
          if (aLocalizationData.Role != null) {
            roleLocalization(aLocalizationData)
          }
          if (aLocalizationData.ActivityPattern != null) {
            activityPatternLocalization(aLocalizationData)
          }
          if (aLocalizationData.SecZone != null) {
            secZoneLocalization(aLocalizationData)
          }
          if (aLocalizationData.AuthLimitProfile != null) {
            authLimitProfileLocalization(aLocalizationData)
          }
          if (aLocalizationData.Group != null) {
            groupLocalization(aLocalizationData)
          }
          if (aLocalizationData.Region != null) {
            regionLocalization(aLocalizationData)
          }
          if (aLocalizationData.Attribute != null) {
            attributeLocalization(aLocalizationData)
          }
          if (aLocalizationData.Holiday != null) {
            holidayLocalization(aLocalizationData)
          }
          aLocalizationData.Skipped = false
          bundle.commit()
        }, User.util.UnrestrictedUser)
      } else {
        aLocalizationData.Skipped = true
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Localization Load: " + aLocalizationData.Language + " " + e.toString())
      }
      aLocalizationData.Error = true
    }
  }

  private function roleLocalization(aLocalizationData : AdminLocalizationData) {
    var role = Query.make(Role).compare(Role#Name, Equals, aLocalizationData.Role).select().first()
    if (role != null) {
      var roleName = Query.make(Role_Name_L10N)
          .compare(Role_Name_L10N#Owner, Equals, role)
          .compare(Role_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (roleName == null) {
        roleName = new Role_Name_L10N()
        roleName.Language = aLocalizationData.Language
        roleName.Owner = role
      } else {
        gw.transaction.Transaction.Current.add(roleName)
      }
      roleName.Value = aLocalizationData.RoleName

      var roleDesc = Query.make(Role_Description_L10N)
          .compare(Role_Description_L10N#Owner, Equals, role)
          .compare(Role_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (roleDesc == null) {
        roleDesc = new Role_Description_L10N()
        roleDesc.Language = aLocalizationData.Language
        roleDesc.Owner = role
      } else {
        gw.transaction.Transaction.Current.add(roleDesc)
      }
      roleDesc.Value = aLocalizationData.RoleDesc
    }
  }

  private function activityPatternLocalization(aLocalizationData : AdminLocalizationData) {
    var activityPattern = Query.make(ActivityPattern).compare(ActivityPattern#Code, Equals, aLocalizationData.ActivityPattern).select().first()
    if (activityPattern != null) {
      var activityPatternSubject = Query.make(ActivityPattern_Subject_L10N)
          .compare(ActivityPattern_Subject_L10N#Owner, Equals, activityPattern)
          .compare(ActivityPattern_Subject_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (activityPatternSubject == null) {
        activityPatternSubject = new ActivityPattern_Subject_L10N()
        activityPatternSubject.Language = aLocalizationData.Language
        activityPatternSubject.Owner = activityPattern
      } else {
        gw.transaction.Transaction.Current.add(activityPatternSubject)
      }
      activityPatternSubject.Value = aLocalizationData.ActvPatternSubj

      var activityPatternDesc = Query.make(ActivityPattern_Description_L10N)
          .compare(ActivityPattern_Description_L10N#Owner, Equals, activityPattern)
          .compare(ActivityPattern_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (activityPatternDesc == null) {
        activityPatternDesc = new ActivityPattern_Description_L10N()
        activityPatternDesc.Language = aLocalizationData.Language
        activityPatternDesc.Owner = activityPattern
      } else {
        gw.transaction.Transaction.Current.add(activityPatternDesc)
      }
      activityPatternDesc.Value = aLocalizationData.ActvPatternDesc
    }
  }

  private function secZoneLocalization(aLocalizationData : AdminLocalizationData) {
    var secZone = Query.make(SecurityZone).compare(SecurityZone#Name, Equals, aLocalizationData.SecZone).select().first()
    if (secZone != null) {
      var secZoneName = Query.make(SecurityZone_Name_L10N)
          .compare(SecurityZone_Name_L10N#Owner, Equals, secZone)
          .compare(SecurityZone_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (secZoneName == null) {
        secZoneName = new SecurityZone_Name_L10N()
        secZoneName.Language = aLocalizationData.Language
        secZoneName.Owner = secZone
      } else {
        gw.transaction.Transaction.Current.add(secZoneName)
      }
      secZoneName.Value = aLocalizationData.SecZoneName

      var secZoneDesc = Query.make(SecurityZone_Description_L10N)
          .compare(SecurityZone_Description_L10N#Owner, Equals, secZone)
          .compare(SecurityZone_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (secZoneDesc == null) {
        secZoneDesc = new SecurityZone_Description_L10N()
        secZoneDesc.Language = aLocalizationData.Language
        secZoneDesc.Owner = secZone
      } else {
        gw.transaction.Transaction.Current.add(secZoneDesc)
      }
      secZoneDesc.Value = aLocalizationData.SecZoneDesc
    }
  }

  private function authLimitProfileLocalization(aLocalizationData : AdminLocalizationData) {
    var authLimitProfile = Query.make(AuthorityLimitProfile).compare(AuthorityLimitProfile#Name, Equals, aLocalizationData.AuthLimitProfile).select().first()
    if (authLimitProfile != null) {
      var authLimitProfileName = Query.make(AuthorityLimitProfile_Name_L10N)
          .compare(AuthorityLimitProfile_Name_L10N#Owner, Equals, authLimitProfile)
          .compare(AuthorityLimitProfile_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (authLimitProfileName == null) {
        authLimitProfileName = new AuthorityLimitProfile_Name_L10N()
        authLimitProfileName.Language = aLocalizationData.Language
        authLimitProfileName.Owner = authLimitProfile
      } else {
        gw.transaction.Transaction.Current.add(authLimitProfileName)
      }
      authLimitProfileName.Value = aLocalizationData.AuthLimitProfileName

      var authLimitProfileDesc = Query.make(AuthorityLimitProfile_Description_L10N)
          .compare(AuthorityLimitProfile_Description_L10N#Owner, Equals, authLimitProfile)
          .compare(AuthorityLimitProfile_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (authLimitProfileDesc == null) {
        authLimitProfileDesc = new AuthorityLimitProfile_Description_L10N()
        authLimitProfileDesc.Language = aLocalizationData.Language
        authLimitProfileDesc.Owner = authLimitProfile
      }else {
        gw.transaction.Transaction.Current.add(authLimitProfileDesc)
      }
      authLimitProfileDesc.Value = aLocalizationData.AuthLimitProfileDesc
    }
  }

  private function groupLocalization(aLocalizationData : AdminLocalizationData) {
    var group = Query.make(Group).compare(Group#Name, Equals, aLocalizationData.Group).select().first()
    if (group != null) {
      var groupName = Query.make(Group_Name_L10N)
          .compare(Group_Name_L10N#Owner, Equals, group)
          .compare(Group_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (groupName == null) {
        groupName = new Group_Name_L10N()
        groupName.Language = aLocalizationData.Language
        groupName.Owner = group
      } else {
        gw.transaction.Transaction.Current.add(groupName)
      }
      groupName.Value = aLocalizationData.GroupName
    }
  }

  private function regionLocalization(aLocalizationData : AdminLocalizationData) {
    var region = Query.make(Region).compare(Region#Name, Equals, aLocalizationData.Region).select().first()
    if (region != null) {
      var regionName = Query.make(Region_Name_L10N)
          .compare(Region_Name_L10N#Owner, Equals, region)
          .compare(Region_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (regionName == null) {
        regionName = new Region_Name_L10N()
        regionName.Language = aLocalizationData.Language
        regionName.Owner = region
      } else {
        gw.transaction.Transaction.Current.add(regionName)
      }
      regionName.Value = aLocalizationData.RegionName
    }
  }

  private function attributeLocalization(aLocalizationData : AdminLocalizationData) {
    var attribute = Query.make(Attribute).compare(Attribute#Name, Equals, aLocalizationData.Attribute).select().first()
    if (attribute != null) {
      var attributeName = Query.make(Attribute_Name_L10N)
          .compare(Attribute_Name_L10N#Owner, Equals, attribute)
          .compare(Attribute_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (attributeName == null) {
        attributeName = new Attribute_Name_L10N()
        attributeName.Language = aLocalizationData.Language
        attributeName.Owner = attribute
      } else {
        gw.transaction.Transaction.Current.add(attributeName)
      }
      attributeName.Value = aLocalizationData.AttributeName

      var attributeDesc = Query.make(Attribute_Description_L10N)
          .compare(Attribute_Description_L10N#Owner, Equals, attribute)
          .compare(Attribute_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (attributeDesc == null) {
        attributeDesc = new Attribute_Description_L10N()
        attributeDesc.Language = aLocalizationData.Language
        attributeDesc.Owner = attribute
      } else {
        gw.transaction.Transaction.Current.add(attributeDesc)
      }
      attributeDesc.Value = aLocalizationData.AttributeDesc
    }
  }

  private function holidayLocalization(aLocalizationData : AdminLocalizationData) {
    var holiday = Query.make(Holiday).compare(Holiday#Name, Equals, aLocalizationData.Holiday).select().first()
    if (holiday != null) {
      var holidayName = Query.make(Holiday_Name_L10N)
          .compare(Holiday_Name_L10N#Owner, Equals, holiday)
          .compare(Holiday_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (holidayName == null) {
        holidayName = new Holiday_Name_L10N()
        holidayName.Language = aLocalizationData.Language
        holidayName.Owner = holiday
      } else {
        gw.transaction.Transaction.Current.add(holidayName)
      }
      holidayName.Value = aLocalizationData.Holiday
    }
  }
}