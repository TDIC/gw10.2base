package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.RoleData
uses gw.api.upgrade.Coercions

class RoleWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseRoleRow(row: Row): RoleData {
    var result = new RoleData()
    // Populate Role Data
    result.PublicID = convertCellToString(row.getCell(RoleData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(RoleData.COL_NAME))
    result.Description = convertCellToString(row.getCell(RoleData.COL_DESCRIPTION))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(RoleData.COL_EXCLUDE)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <RoleData> {
    return super.parseSheet(sheet, \r -> parseRoleRow(r), true)
  }
}
