package tdic.util.dataloader.entityloaders.sampleloaders

uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.PaymentReversalData
uses gw.transaction.Transaction
uses java.lang.Exception
uses gw.api.database.Relop

class BCPaymentReversalLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aPaymentReversalData = applicationData as PaymentReversalData
    try {
      var bundle = Transaction.getCurrent()
      var aPayment: PaymentMoneyReceived
      aPayment = gw.api.database.Query.make(entity.PaymentMoneyReceived).and(\r -> {
        r.compare("RefNumber", Relop.Equals, aPaymentReversalData.ReferenceNumber)
        r.compare("RefNumber", Relop.NotEquals, null)
        r.compare("ReversalReason", Relop.Equals, null)
      }).select().getFirstResult()
      if (aPayment != null) {
        bundle.add(aPayment)
      }
      else {
        var payments = gw.api.database.Query.make(entity.PaymentMoneyReceived).and(\r -> {
          r.compare("RefNumber", Relop.Equals, null)
          r.compare("ReversalReason", Relop.Equals, null)
        }).select()
        //as PaymentMoneyReceived
        aPayment = payments.toTypedArray().firstWhere(\p -> p.MoneyReceivedTransaction.TransactionNumber == aPaymentReversalData.TransactionNumber)
        bundle.add(aPayment)
      }
      //Reverse Payment
      if (aPayment.BaseDist != null) {
        aPayment.BaseDist.reverse(aPaymentReversalData.ReversalReason)
      } else {
        aPayment.reverse(aPaymentReversalData.ReversalReason)
      }
      bundle.commit()
      LOG.info("Payment Reversal Run Date : " + aPaymentReversalData.EntryDate + "\n" + " Reference #: " + aPaymentReversalData.ReferenceNumber + "\n" + " Transaction #: " + aPaymentReversalData.TransactionNumber + "\n")
      aPaymentReversalData.Skipped = false
    }
        catch (e: Exception) {
          if (LOG.InfoEnabled){
            LOG.info("Payment Reversal Transaction #: " + aPaymentReversalData.TransactionNumber + e.toString())
          }
          aPaymentReversalData.Error = true
        }
  }
}
