package tdic.util.dataloader.export.planexport

uses gw.api.database.Query
uses java.util.Collection
uses java.util.HashMap
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.PaymentAllocationPlanData
uses java.util.ArrayList
uses java.lang.Integer

class BCPaymentAllocationPlanExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return PaymentAllocationPlanData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getPaymentAllocationPlanEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(PaymentAllocationPlanData.CD), queryPaymentAllocationPlans(), \e -> getRowFromPaymentAllocationPlan(e))
  }

  // query data

  public static function queryPaymentAllocationPlans(): Collection <PaymentAllocationPlan> {
    var result = Query.make(PaymentAllocationPlan).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromPaymentAllocationPlan(plan: PaymentAllocationPlan): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(PaymentAllocationPlanData.COL_PUBLIC_ID), plan.PublicID)
    colMap.put(colIndexString(PaymentAllocationPlanData.COL_NAME), plan.Name)
    colMap.put(colIndexString(PaymentAllocationPlanData.COL_DESCRIPTION), plan.Description)
    colMap.put(colIndexString(PaymentAllocationPlanData.COL_EFFECTIVE_DATE), plan.EffectiveDate.AsUIStyle)
    colMap.put(colIndexString(PaymentAllocationPlanData.COL_EXPIRATION_DATE), plan.ExpirationDate.AsUIStyle)

    // create column values for Filters, with blanks for unused filters
    var filterArray = plan.DistributionFilters.orderBy( \ elt -> elt.Priority)
    for (i in 0..5){
      colMap.put(colIndexString(i+5), filterArray.Count > i ? filterArray[i].Code : " ")
    }
    // create column for each Pay Order value in use -- need to use special getter to fetch priority and use this to set the column number
    var payOrderCol : Integer
    for (payOrder in plan.InvoiceItemOrderings){
      payOrderCol = plan.getInvoiceItemOrderingFor(payOrder).Priority
      colMap.put(colIndexString(payOrderCol+12), payOrder.Code)
    }

    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getPaymentAllocationPlanColumnTypes(): HashMap {
    return getColumnTypes(PaymentAllocationPlanData.CD)
  }
}
