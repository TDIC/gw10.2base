package tdic.util.gunit.test.stubs

uses gw.testharness.RunLevel
uses gw.testharness.ServerTest
uses gw.api.system.server.Runlevel
uses gw.testharness.TestBase

@ServerTest
@RunLevel(Runlevel.NONE)
class OnePassingTest extends TestBase {

  function testPasses() {
    // This test always passes.
  }

}
