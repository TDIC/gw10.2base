package tdic.util.dataloader.parser

uses org.apache.poi.ss.usermodel.Sheet
uses java.util.ArrayList

interface WorksheetParserInterface {
  function parseSheet(sheet: Sheet): ArrayList
}
