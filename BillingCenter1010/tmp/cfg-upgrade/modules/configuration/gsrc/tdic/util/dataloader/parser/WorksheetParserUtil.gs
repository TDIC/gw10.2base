package tdic.util.dataloader.parser

uses java.text.SimpleDateFormat
uses org.apache.poi.ss.usermodel.Cell
uses tdic.util.dataloader.data.sampledata.ChargeData
uses java.math.BigDecimal
uses org.apache.poi.ss.usermodel.DataFormatter
uses org.apache.poi.ss.usermodel.Sheet
uses java.util.ArrayList
uses tdic.util.dataloader.data.ApplicationData
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.util.DataLoaderUtil
uses java.lang.Exception
uses gw.api.util.DisplayableException
uses org.apache.poi.ss.usermodel.DateUtil
uses org.slf4j.LoggerFactory
uses java.util.List
uses gw.api.upgrade.Coercions

class WorksheetParserUtil {
  public static final var LOG: org.slf4j.Logger = LoggerFactory.getLogger("DataLoader.Parser")

  construct() {
  }

  // Simple helper function to convert Excel cell contents to string

  public static function convertCellToString(aCell: Cell): String
  {
    if (aCell != null && aCell.toString().trim() != "")
    {
      if (aCell.CellType == Cell.CELL_TYPE_FORMULA) {
        // return the formula value
        switch (aCell.getCachedFormulaResultType()) {
          case Cell.CELL_TYPE_NUMERIC:
              return aCell.getNumericCellValue() as String
          case Cell.CELL_TYPE_STRING:
              return aCell.RichStringCellValue as String
        }
      }
      var formatter = new DataFormatter()
      return formatter.formatCellValue(aCell)
    }
    else
      return null
  }

  public static function convertCellToDate(cell: Cell): java.util.Date
  {
    if (cell != null && cell.toString().trim() != "")
    {
      if (DateUtil.isCellDateFormatted(cell)) {
        return cell.DateCellValue
      }
      return convertStringToDate(convertCellToString(cell))
    }
    else
      return null
  }

  public static function getStringCellValue(aCell: Cell): String {
    if (aCell != null && aCell.toString().trim() != "") {
      aCell.setCellType(Cell.CELL_TYPE_STRING)
      return aCell.StringCellValue.trim()
    } else
      return null
  }

  // Simple helper function to convert string to a Java date format

  public static function convertStringToDate(dateString: String): java.util.Date
  {
    if (dateString == null)
      return null
    else
    {
      var dateFormat = new SimpleDateFormat(DataLoaderUtil.getDateFormat())
      var date = dateFormat.parse(dateString)
      return date
    }
  }

  public static function populateCharges(charges: List <ChargeData>, amount: BigDecimal, chargePattern: String, pPayer: String, pInvoiceStream: String) {
    var charge = new ChargeData()
    charge.ChargeAmount = amount
    charge.ChargePattern = chargePattern
    charge.Payer = pPayer
    charge.InvoiceStream = pInvoiceStream
    charges.add(charge)
  }

  public static function setBoolean(booleanString: String, booleanDefault: Boolean): Boolean
  {
    var newBoolean = booleanDefault
    if (booleanString != null and booleanString != "") {
      newBoolean = Coercions.makeBooleanFrom(booleanString)
    }
    return newBoolean
  }

  public static function isEmptyRow(row: Row): boolean {
    var result = true
    if (row != null) {
      for (aCell in row.iterator()) {
        if (aCell != null and aCell.CellType != Cell.CELL_TYPE_BLANK and convertCellToString(aCell) != "") {
          result = false
          break
        }
      }
    }
    return result
  }

  public static function rowAsString(row: Row): String {
    var result = "["
    var firstDone = false
    if (row != null) for (aCell in row.iterator()) {
      if (firstDone) result += ", "
      else firstDone = true
      if (aCell != null) {
        result += convertCellToString(aCell)
      }
    }
    result += "]"
    return result
  }

  protected static function parseSheet<T extends ApplicationData>(sheet: Sheet, parseRow(aRow: Row): T, checkPublicId: Boolean): ArrayList <T> {
    var resArray = new ArrayList <T>()
    var currRowNum: int = - 1
    var currRowString = ""
    var firstRowDone = false
    var currRow: T
    if (sheet != null) {
      LOG.info(sheet.SheetName + " sheet: Parsing started ***")
      try {
        // Iterate through each row in the sheet
        for (var row in sheet.rowIterator()) {
          if (LOG.DebugEnabled) currRowString = rowAsString(row)
          // ignore empty rows
          if (!isEmptyRow(row)) {
            currRowNum = row.RowNum
            // skip the first row (it has headers)
            if (!firstRowDone) {
              firstRowDone = true
              continue
            }
            currRow = parseRow(row)
            // Save to array
            if (currRow != null) {
              if (checkPublicId) {
                if (currRow.PublicID != null)
                  resArray.add(currRow)
              } else {
                resArray.add(currRow)
              }
            }
          }
        }
      } catch (e: Exception) {
        var msg = sheet.SheetName + " sheet: Exception while parsing row " + currRowNum + " : "
        if (LOG.DebugEnabled) LOG.debug(sheet.SheetName + " sheet: Row: " + currRowString)
        LOG.error(msg, e)
        throw new DisplayableException(msg + e)
      }
      LOG.info(sheet.SheetName + " sheet: Parsing completed ***")
    }
    return resArray
  }
}
