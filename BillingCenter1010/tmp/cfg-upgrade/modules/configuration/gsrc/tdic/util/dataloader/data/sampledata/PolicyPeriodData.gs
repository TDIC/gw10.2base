package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData
uses java.util.Date
uses java.lang.Integer

class PolicyPeriodData extends ApplicationData{

  construct() {

  }
  private var  _accountName                :  String                    as  AccountName
  private var  _policyNumber               :  String                    as  PolicyNumber
  private var  _policyPerEffDate           :  Date                      as  PolicyPerEffDate
  private var  _policyPerExpirDate         :  Date                      as  PolicyPerExpirDate
  private var  _paymentPlan                :  String                    as  PaymentPlan
  private var  _billingMethod              :  String                    as  BillingMethod
  private var  _producerCode               :  String                    as  ProducerCode
  private var  _lOBCode                    :  LOBCode                   as  LOBCode
  private var  _underAudit                 :  Boolean                   as  UnderAudit
  private var  _assignedRisk               :  Boolean                   as  AssignedRisk
  private var  _underwriter                :  String                    as  Underwriter
  private var  _uWCompany                  :  UWCompany                 as  UWCompany
  private var  _contact                    :  ContactData               as  Contact
  private var  _role                       :  PolicyPeriodRole          as  Role
  private var  _paymentMethod              :  PaymentMethod             as  PaymentMethod
  private var  _termnumber                 :  Integer                   as  TermNumber
  private var  _listBillPayerAccount       :  String                    as  ListBillPayerAccount
  private var  _listBillPayerPaymentPlan   :  String                    as  ListBillPayerPaymentPlan
  private var  _listBillPayerInvoiceStream :  String                    as  ListBillPayerInvoiceStream
  private var  _dba                        :  String                    as  DBA  //example extension column
}
