package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses java.math.BigDecimal
uses tdic.util.dataloader.data.plandata.PaymentPlanData
uses gw.api.upgrade.Coercions

class PaymentPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parsePaymentPlanRow(row: Row): PaymentPlanData {
    var result = new PaymentPlanData()
    // Populate Payment Plan Data
    result.PublicID = convertCellToString(row.getCell(PaymentPlanData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(PaymentPlanData.COL_NAME))
    result.Description = convertCellToString(row.getCell(PaymentPlanData.COL_DESCRIPTION))
    result.EffectiveDate = convertCellToDate(row.getCell(PaymentPlanData.COL_EFFECTIVE_DATE))
    result.ExpirationDate = convertCellToDate(row.getCell(PaymentPlanData.COL_EXPIRATION_DATE))
    result.Reporting = Coercions.makeBooleanFrom(convertCellToString(row.getCell(PaymentPlanData.COL_REPORTING)))
    result.DownPaymentPercent = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(PaymentPlanData.COL_DOWNPAYMENT_PERCENT)))
    result.MaximumNumberOfInstallments = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanData.COL_MAXIMUM_NUMBER_OF_INSTALLMENTS)))
    result.Periodicity = typekey.Periodicity.get(convertCellToString(row.getCell(PaymentPlanData.COL_PERIODICITY)))
    result.InvoiceItemPlacementCutoffType = typekey.InvoiceItemPlacementCutoffType.get(convertCellToString(row.getCell(PaymentPlanData.COL_INVOICE_ITEM_PLACEMENT_CUTOFF_TYPE)))
    result.InvoicingBlackoutType = typekey.InvoicingBlackoutType.get(convertCellToString(row.getCell(PaymentPlanData.COL_INVOICING_BLACKOUT_TYPE)))
    result.DaysBeforePolicyExpirationForInvoicingBlackout = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanData.COL_DAYS_BEFORE_POLICY_EXPIRATION_FOR_INVOICING_BLACKOUT)))
    result.InstallmentFee = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(PaymentPlanData.COL_INSTALLMENT_FEE)))
    result.SkipFeeForDownPayment = Coercions.makeBooleanFrom(convertCellToString(row.getCell(PaymentPlanData.COL_SKIP_FEE_FOR_DOWNPAYMENT)))
    result.DaysFromReferenceDateToDownPayment = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanData.COL_DAYS_FROM_REFERENCE_DATE_TO_DOWNPAYMENT)))
    result.DownPaymentAfter = typekey.PaymentScheduledAfter.get(convertCellToString(row.getCell(PaymentPlanData.COL_DOWNPAYMENT_AFTER)))
    result.DaysFromReferenceDateToFirstInstallment = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanData.COL_DAYS_FROM_REFERENCE_DATE_TO_FIRST_INSTALLMENT)))
    result.FirstInstallmentAfter = typekey.PaymentScheduledAfter.get(convertCellToString(row.getCell(PaymentPlanData.COL_FIRST_INSTALLMENT_AFTER)))
    result.DaysFromReferenceDateToOneTimeCharge = Coercions.makeIntFrom(convertCellToString(row.getCell(PaymentPlanData.COL_DAYS_FROM_REFERENCE_DATE_TO_ONETIME_CHARGE)))
    result.OneTimeChargeAfter = typekey.PaymentScheduledAfter.get(convertCellToString(row.getCell(PaymentPlanData.COL_ONETIME_CHARGE_AFTER)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <PaymentPlanData> {
    return parseSheet(sheet, \r -> parsePaymentPlanRow(r), true)
  }
}
