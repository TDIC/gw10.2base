package tdic.util.dataloader.data.admindata

uses java.lang.Integer

class AuthorityLimitTypeData {

  construct() {

  }
  private var _column               : Integer               as Column
  private var _limitType            : String                as LimitType
}
