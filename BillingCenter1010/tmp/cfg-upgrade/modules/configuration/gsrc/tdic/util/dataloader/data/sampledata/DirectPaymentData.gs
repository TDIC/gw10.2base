package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData
uses java.util.Date
uses java.math.BigDecimal

class DirectPaymentData extends ApplicationData{

  construct() {

  }
  private var _accountName          : String           as AccountName
  private var _policynumber         : String           as PolicyNumber
  private var _description          : String           as Description
  private var _receivedDate         : Date             as ReceivedDate
  private var _referencenumber      : String           as ReferenceNumber
  private var _paymentMethod        : PaymentMethod    as PaymentMethod
  private var _paymentToken         : String           as PaymentToken    
  private var _amount               : BigDecimal       as Amount

}
