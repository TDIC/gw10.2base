package tdic.util.dataloader.entityloaders

uses gw.command.demo.GeneralUtil
uses gw.pl.currency.MonetaryAmount
uses gw.transaction.Transaction
uses gw.webservice.policycenter.bc1000.entity.types.complex.ChargeInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCContactInfo
uses gw.xml.date.XmlDateTime
uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.data.sampledata.ChargeData
uses tdic.util.dataloader.data.sampledata.ContactData
uses tdic.util.dataloader.data.admindata.UserContactData
uses gw.api.web.invoice.InvoicingOverrider
uses java.lang.Exception
uses tdic.util.dataloader.data.sampledata.PolicyPeriodData
uses gw.pl.persistence.core.Bundle
uses gw.api.domain.accounting.ChargeUtil
uses gw.api.domain.charge.ChargeInitializer
uses gw.api.database.InOperation
uses gw.api.database.Relop

class BCLoader extends EntityLoader {
  construct() {
  }

  protected function convertContactToBC(_ContactData: ContactData): Contact
  {
    var _BCContact: Contact
    //Try to find an existing Contact and return that object. Must also match the contact address.
    try
    {
      _BCContact = gw.api.database.Query.make(entity.Person).and(\r -> {
        r.compare("FirstName", Relop.Equals, _ContactData.FirstName)
        r.compare("LastName", Relop.Equals, _ContactData.LastName)
        r.compare("WorkPhone", Relop.Equals, _ContactData.WorkPhone)
        r.compare("FaxPhone", Relop.Equals, _ContactData.FaxPhone)
        r.compare("EmailAddress1", Relop.Equals, _ContactData.Email)
        r.compare("Name", Relop.Equals, _ContactData.Name)
        r.subselect("PrimaryAddress", InOperation.CompareIn, gw.api.database.Query.make(entity.Address), "ID").and(\r1 -> {
          r1.compare("AddressLine1", Relop.Equals, _ContactData.PrimaryAddress.AddressLine1)
          r1.compare("AddressLine2", Relop.Equals, _ContactData.PrimaryAddress.AddressLine2)
          r1.compare("City", Relop.Equals, _ContactData.PrimaryAddress.City)
          r1.compare("State", Relop.Equals, typekey.State.get(_ContactData.PrimaryAddress.State))
          r1.compare("PostalCode", Relop.Equals, _ContactData.PrimaryAddress.PostalCode)
          r1.compare("Country", Relop.Equals, _ContactData.PrimaryAddress.Country)
        })
      }).select().FirstResult
    }
        catch (e: Exception)
        {
          _BCContact = null
        }
    //If the contact does not exist populate a new one with the passed in data.
    if (_BCContact == null)
    {
      Transaction.runWithNewBundle(\bundle ->
      {
        if (_ContactData.ContactType == typekey.Contact.TC_PERSON) {
          var _newperson = new Person()
          _newperson.FirstName = _ContactData.FirstName
          _newperson.LastName = _ContactData.LastName
          _BCContact = _newperson
        } else {
          _BCContact = new Company()
          _BCContact.Name = _ContactData.Name
        }
        _BCContact.WorkPhone = _ContactData.WorkPhone
        _BCContact.FaxPhone = _ContactData.FaxPhone
        _BCContact.EmailAddress1 = _ContactData.Email
        var contactAddress: Address
        contactAddress = convertAddressToBC(_ContactData.PrimaryAddress)
        _BCContact.addAddress(contactAddress)
        _BCContact.Bundle.commit()
      }
      )
    }
    return _BCContact
  }

  protected function convertUserContactToBC(contactData: UserContactData): UserContact
  {
    var userContact: UserContact
    //Try to find an existing Contact and return that object. Must also match the contact address.
    try
    {
      userContact = gw.api.database.Query.make(entity.UserContact).and(\r -> {
        r.compare("FirstName", Relop.Equals, contactData.FirstName)
        r.compare("LastName", Relop.Equals, contactData.LastName)
        r.compare("WorkPhone", Relop.Equals, contactData.WorkPhone)
        r.compare("FaxPhone", Relop.Equals, contactData.FaxPhone)
        r.compare("EmailAddress1", Relop.Equals, contactData.EmailAddress1)
        r.compare("EmailAddress2", Relop.Equals, contactData.EmailAddress2)
        r.subselect("PrimaryAddress", InOperation.CompareIn, gw.api.database.Query.make(entity.Address), "ID").and(\r1 -> {
          r1.compare("AddressLine1", Relop.Equals, contactData.PrimaryAddress.AddressLine1)
          r1.compare("AddressLine2", Relop.Equals, contactData.PrimaryAddress.AddressLine2)
          r1.compare("City", Relop.Equals, contactData.PrimaryAddress.City)
          r1.compare("State", Relop.Equals, typekey.State.get(contactData.PrimaryAddress.State))
          r1.compare("PostalCode", Relop.Equals, contactData.PrimaryAddress.PostalCode)
          r1.compare("Country", Relop.Equals, contactData.PrimaryAddress.Country)
        })
      }).select().FirstResult
    }
        catch (e: Exception)
        {
          userContact = null
        }
    //If the contact does not exist populate a new one with the passed in data.
    if (userContact == null)
    {
      Transaction.runWithNewBundle(\bundle ->
      {
        userContact = new UserContact()
        //userContact.Type = contactData.ContactType
        userContact.Name = contactData.Name
        userContact.FirstName = contactData.FirstName
        userContact.LastName = contactData.LastName
        userContact.MiddleName = contactData.MiddleName
        userContact.FormerName = contactData.FormerName
        userContact.Gender = typekey.GenderType.get(contactData.Gender)
        userContact.HomePhone = contactData.HomePhone
        userContact.WorkPhone = contactData.WorkPhone
        userContact.CellPhone = contactData.CellPhone
        userContact.FaxPhone = contactData.FaxPhone
        userContact.PrimaryPhone = typekey.PrimaryPhoneType.get(contactData.PrimaryPhone)
        userContact.EmailAddress1 = contactData.EmailAddress1
        userContact.EmailAddress2 = contactData.EmailAddress2
        userContact.DateOfBirth = contactData.DateOfBirth
        userContact.Notes = contactData.Notes
        var contactAddress: Address
        contactAddress = convertAddressToBC(contactData.PrimaryAddress)
        userContact.addAddress(contactAddress)
        userContact.PrimaryAddress = contactAddress
        if (contactData.NonPrimaryAddress != null) {
          var contactSecAddress: Address
          contactSecAddress = convertAddressToBC(contactData.NonPrimaryAddress)
          userContact.addAddress(contactSecAddress)
        }
        userContact.Bundle.commit()
      }
      )
    }
    return userContact
  }

  protected function convertAddressToBC(addressData: AddressData): Address
  {
    var _Address: Address
    // Always add the address, even if the same address already exists. Each contact should have its own address.
    _Address = new Address()
    _Address.AddressLine1 = addressData.AddressLine1
    _Address.AddressLine2 = addressData.AddressLine2
    _Address.AddressLine3 = addressData.AddressLine3
    _Address.City = addressData.City
    _Address.State = typekey.State.get(addressData.State)
    _Address.PostalCode = addressData.PostalCode
    _Address.County = addressData.County
    _Address.Country = addressData.Country
    _Address.AddressType = addressData.AddressType
    return _Address
  }

  protected function buildCharges(bi: BillingInstruction, theCharges: ChargeData[]) {
    var chargesToProcess = theCharges.where(\elt -> elt.ChargePattern != null and elt.ChargeAmount != null and elt.ChargeAmount != 0)
    chargesToProcess.each(\elt -> {
      var initializer = bi.buildCharge(elt.ChargeAmount.ofDefaultCurrency(), ChargeUtil.getChargePatternByCode(elt.ChargePattern))
      populateOtherChargeInfo(elt, initializer)
    })
  }

  private function populateOtherChargeInfo(aChargeData: ChargeData, initializer: ChargeInitializer) {
    if (aChargeData.Payer != null and aChargeData.InvoiceStream != null) {
      var lbpAccount = GeneralUtil.findAccountByName(aChargeData.Payer)
      if (lbpAccount == null) {
        lbpAccount = gw.api.database.Query.make(entity.Account).select().firstWhere(\a -> a.AccountNumber == aChargeData.Payer)
      }
      if (lbpAccount != null) {
        var lbpInvoiceStream = lbpAccount.InvoiceStreams.firstWhere(\i -> i.DisplayName == aChargeData.InvoiceStream)
        if (lbpInvoiceStream != null) {
          initializer.updateWith(new InvoicingOverrider().withOverridingPayerAccount(lbpAccount).withOverridingInvoiceStream(lbpInvoiceStream))
        }
      }
    }
  }

  protected function createPolicyPeriodContactAndAddress(ppData: PolicyPeriodData, bundle: Bundle, polPeriod: PolicyPeriod, BCContact: Contact) {
    // build and add AccountContact and Address
    var thePPContact = convertPPContactToBC(ppData.PolicyNumber, ppData.Role, BCContact)
    if (!thePPContact.New) {
      // found AccountContact won't be in bundle; put it there 
      thePPContact = bundle.add(thePPContact)
    }
    polPeriod.addToContacts(thePPContact)
  }

  protected function convertPPContactToBC(polNumber: String, role: PolicyPeriodRole, BCContact: Contact): PolicyPeriodContact {
    var ppContact = gw.api.database.Query.make(entity.PolicyPeriodContact).and(\r -> {
      r.compare("ID", Relop.NotEquals, null)
      r.subselect("Contact", InOperation.CompareIn, gw.api.database.Query.make(entity.Contact), "ID").compare("ID", Relop.Equals, BCContact.ID)
      r.subselect("PolicyPeriod", InOperation.CompareIn, gw.api.database.Query.make(entity.PolicyPeriod), "ID").compare("PolicyNumber", Relop.Equals, polNumber)
    }).select().FirstResult

    if (ppContact == null) {
      //Build new AccountContact
      ppContact = new PolicyPeriodContact()
      ppContact.Contact = BCContact
      var ppContactRole = new PolPeriodContactRole()
      ppContactRole.Role = role
      ppContact.addToRoles(ppContactRole)
    }
    return ppContact
  }

  protected function ChargeDataToChargeInfo(cd:ChargeData, xd:XmlDateTime):ChargeInfo {
    var ci = new ChargeInfo()
    ci.Amount = new MonetaryAmount(cd.ChargeAmount, cd.ChargeCurrency).toString()
    ci.ChargePatternCode = cd.ChargePattern
    ci.WrittenDate = xd
    return ci
  }

  protected  function DateToXmlDate(d:Date):XmlDateTime {
    var xd = new XmlDateTime()
    xd.setDay(d.DayOfMonth)
    xd.setMonth(d.MonthOfYear)
    xd.setYear(d.YearOfDate)
    xd.setHour(d.Hour)
    xd.setMinute(d.Minute)
    xd.setSecond(d.Second)
    return xd
  }

  protected function BCContactToPCContactInfo(c:Contact): PCContactInfo {

    var pcContact = new PCContactInfo()
    if(c.AddressBookUID != null) {
      pcContact.AddressBookUID = c.AddressBookUID
    }
    else {
      pcContact.AddressBookUID = c.PublicID
    }
    pcContact.ContactType = c.Subtype.Code
    switch(c.Subtype){

      case TC_COMPANY:
        var tc = c as Company
        pcContact.Name = tc.Name
        break

      case TC_PERSON:
        var tc = c as Person
        pcContact.FirstName = tc.FirstName
        pcContact.LastName = tc.LastName
        break

      default:
        pcContact.Name = c.Name
    }

    pcContact.EmailAddress1 = c.EmailAddress1
    pcContact.WorkPhone = c.WorkPhone
    pcContact.WorkPhoneCountry = c.WorkPhoneCountry.Code
    pcContact.WorkPhoneExtension = c.WorkPhoneExtension
    /*pcContact.Addresses = new ArrayList<PCContactInfo_Addresses>()
    pcContact.Addresses.add(AddressToPCContactInfo(c.PrimaryAddress, true))*/

    return pcContact
  }

  /*protected function AddressToPCContactInfo(a:Address, isPrimary:boolean):PCContactInfo_Addresses {

    var pa = new PCContactInfo_Addresses()
    pa.AddressLine1 = a.AddressLine1
    pa.AddressLine2 = a.AddressLine2
    pa.City = a.City
    pa.Country = a.Country.Code
    pa.PostalCode = a.PostalCode
    pa.State = a.State.Code
    pa.Primary = isPrimary

    return pa
  }*/
}
