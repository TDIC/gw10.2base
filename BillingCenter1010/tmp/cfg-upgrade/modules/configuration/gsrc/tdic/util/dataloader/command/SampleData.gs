package tdic.util.dataloader.command

uses gw.command.Argument
uses gw.command.BCBaseCommand
uses tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
//uses com.guidewire.pl.quickjump.Argument
uses java.util.ArrayList
uses tdic.util.dataloader.util.BCSampleDataLoaderUtil
uses tdic.util.dataloader.util.DataLoaderUtil

@Export class SampleData extends BCBaseCommand {
  construct() {
    super();
  }
  public final var optionDateHandler: ArrayList = new ArrayList <String>(){
      "Run with imported Create Date",
      "Run with current system date",
      "Run with override date"}
  //Date Loading Options

  //ALL

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  //@Argument("dateHandler",  {"Run with imported Create Date", "Run with current system date", "Run with override date"})
  //@Argument("processDirectPayments", {"true", "false"})
  function loadAll() {
    var sampleData = createDataLoaderProcessor("ALL")
    var path = getArgumentAsString("path")
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.BillingDataLoaderWizard.go()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadAccounts

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadAccounts() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkAccount = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadProducers

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadProducers() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkProducer = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadIssuances

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadIssuances() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkIssuance = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadChanges

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadChanges() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkChange = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadRenewals

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadRenewals() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkRenew = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadCancellations

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadCancellations() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkCancel = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadAllDirectPayments

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadAllDirectPayments() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkDirectPayment = true
    sampleData.LoadAllPayments = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadDirectPayments

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadDirectPayments() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkDirectPayment = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadRewrites

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadRewrites() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkRewrite = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadReinstatements

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadReinstatements() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkReinstatement = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //runAllBatch

  function runAllBatch() {
    BCSampleDataLoaderUtil.runBatch()
  }

  //loadAllAgencyBillPayments

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadAllAgencyBillPayments() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkAgencyBillPayment = true
    sampleData.LoadAllAgencyBillPayments = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadAgencyBillPayments

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadAgencyBillPayments() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkAgencyBillPayment = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //Generate Direct Bill Payment File

  @Argument("path", DataLoaderUtil.getFullExportDatafilePath(DataLoaderUtil.EXPORT_DBPAYMENTFILE))
  function GenerateDirectBillPaymentFile() {
    var path = getArgumentAsString("path")
    BCSampleDataLoaderUtil.printRequiredPaymentsToFile(path)
  }

  //Generate Agency Bill Payment File

  @Argument("path", DataLoaderUtil.getFullExportDatafilePath(DataLoaderUtil.EXPORT_ABPAYMENTFILE))
  function GenerateAgencyBillPaymentFile() {
    var path = getArgumentAsString("path")
    BCSampleDataLoaderUtil.printABRequiredPaymentsToFile(path)
  }

  //Generate the Payment Reversal file

  @Argument("path", DataLoaderUtil.getFullExportDatafilePath(DataLoaderUtil.EXPORT_PAYMENTREVERSALFILE))
  function GeneratePaymentsReversalsToFile() {
    var path = getArgumentAsString("path")
    BCSampleDataLoaderUtil.printReversalsToFile(path)
  }

  /**Scenario Support functions**/
  //loadActPrdPolIssNewRn


  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadActPrdPolIssNewRn() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkAccount = true
    sampleData.ChkProducer = true
    sampleData.ChkPolicy = true
    sampleData.ChkIssuance = true
    sampleData.ChkNewRenewal = true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    BCSampleDataLoaderUtil.printRequiredPaymentsToFile()
    BCSampleDataLoaderUtil.printABRequiredPaymentsToFile()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  //loadCanclRewriteReState

  @Argument("path", DataLoaderUtil.getImportSampleDatafilePath())
  function loadCanclRewriteReState() {
    var path = getArgumentAsString("path")
    var sampleData = createDataLoaderProcessor("None")
    sampleData.ChkCancel = true
    sampleData.ChkRewrite = true
    sampleData.ChkReinstatement = true
    //sampleData.ChkDirectPayment=true
    //sampleData.ChkAgencyBillPayment=true
    //sampleData.LoadAllPayments=true
    //sampleData.LoadAllAgencyBillPayments=true
    if (path != null) {
      sampleData.Path = path
    }
    sampleData.readAndImportFile()
    sampleData.loadData()
    BCSampleDataLoaderUtil.printRequiredPaymentsToFile()
    BCSampleDataLoaderUtil.printABRequiredPaymentsToFile()
    pcf.DataUploadConfirmation.go(sampleData);
  }

  /****Support functions****/
  function createDataLoaderProcessor(loadEntity: String): BCSampleDataLoaderProcessor {
    var sampleData = new BCSampleDataLoaderProcessor()
    if (loadEntity == "ALL") {
      sampleData.ChkAccount = true
      sampleData.ChkProducer = true
      sampleData.ChkPolicy = true
      sampleData.ChkIssuance = true
      sampleData.ChkNewRenewal = true
      sampleData.ChkChange = true
      sampleData.ChkRenew = true
      sampleData.ChkCancel = true
      sampleData.ChkDirectPayment = true
      sampleData.ChkRewrite = true
      sampleData.ChkReinstatement = true
      sampleData.ChkAgencyBillPayment = true
      sampleData.ChkBatch = true
    }
    else if (loadEntity == "None") {
      sampleData.ChkAccount = false
      sampleData.ChkProducer = false
      sampleData.ChkPolicy = true
      sampleData.ChkIssuance = false
      sampleData.ChkNewRenewal = false
      sampleData.ChkChange = false
      sampleData.ChkRenew = false
      sampleData.ChkCancel = false
      sampleData.ChkDirectPayment = false
      sampleData.ChkRewrite = false
      sampleData.ChkReinstatement = false
      sampleData.ChkAgencyBillPayment = false
      sampleData.ChkBatch = false
    }
    else {
      sampleData.ChkAccount = false
      sampleData.ChkProducer = false
      sampleData.ChkPolicy = true
      sampleData.ChkIssuance = false
      sampleData.ChkNewRenewal = false
      sampleData.ChkChange = false
      sampleData.ChkRenew = false
      sampleData.ChkCancel = false
      sampleData.ChkDirectPayment = false
      sampleData.ChkRewrite = false
      sampleData.ChkReinstatement = false
      sampleData.ChkAgencyBillPayment = false
      sampleData.ChkBatch = false
      if (loadEntity == "Account") {
        sampleData.ChkAccount = true
      }
      if (loadEntity == "Producer") {
        sampleData.ChkProducer = true
      }
      if (loadEntity == "Policy") {
        sampleData.ChkPolicy = true
      }
      if (loadEntity == "Issuance") {
        sampleData.ChkIssuance = true
      }
      if (loadEntity == "NewRenewal") {
        sampleData.ChkNewRenewal = true
      }
      if (loadEntity == "Change") {
        sampleData.ChkChange = true
      }
      if (loadEntity == "Renew") {
        sampleData.ChkRenew = true
      }
      if (loadEntity == "Cancel") {
        sampleData.ChkCancel = true
      }
      if (loadEntity == "DirectPayment") {
        sampleData.ChkDirectPayment = true
      }
      if (loadEntity == "Rewrite") {
        sampleData.ChkRewrite = true
      }
      if (loadEntity == "Reinstatement") {
        sampleData.ChkReinstatement = true
      }
      if (loadEntity == "AgencyBillPayment") {
        sampleData.ChkAgencyBillPayment = true
      }
      if (loadEntity == "Batch") {
        sampleData.ChkBatch = true
      }
    }
    return sampleData;
  }
}
