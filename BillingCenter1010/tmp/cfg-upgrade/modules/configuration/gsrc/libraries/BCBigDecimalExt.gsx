package libraries

uses gw.api.util.Percentage
uses gw.api.util.CurrencyUtil
uses gw.api.util.Ratio
uses gw.pl.currency.MonetaryAmount

uses java.math.BigDecimal

@Export
enhancement BCBigDecimalExt : BigDecimal {
  
  function percentageOf(base : BigDecimal) : Percentage {
    if (base.signum() == 0) {
      return Percentage.ZERO_PERCENT
    }
    return Percentage.fromRatioBetween( this, base )
  }

  function percentageOfAsBigDecimal(base : BigDecimal ) : BigDecimal {  
    if (base.signum() == 0) {
      return BigDecimal.ZERO
    }
    return Ratio.valueOf(this, base).resolve(5).movePointRight(2)
  }

  function ofDefaultCurrency() : MonetaryAmount {
    return new MonetaryAmount(this, CurrencyUtil.getDefaultCurrency())
  }

}
