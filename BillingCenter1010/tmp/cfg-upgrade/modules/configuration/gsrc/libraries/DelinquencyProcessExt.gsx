package libraries


uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.system.BCLoggerCategory
uses gw.api.util.DateUtil
uses gw.api.web.admin.ActivityPatternsUtil
uses gw.command.demo.GeneralUtil
uses gw.pl.currency.MonetaryAmount
uses gw.pl.util.ArgCheck
uses gw.plugin.pas.PASMessageTransport
uses gw.xml.ws.WsdlConfig
uses org.slf4j.LoggerFactory
uses tdic.bc.config.invoice.TDIC_InvoicePURHelper
uses typekey.BillingInstruction
uses wsi.remote.gw.webservice.pc.pc1000.policyapi.PolicyAPI
uses entity.DelinquencyProcess
uses entity.Activity

@Export
enhancement DelinquencyProcessExt : DelinquencyProcess {
  // === API for workflow steps ========================================

  function onInception() {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess");
    logger.trace ("Calling onInception on " + this);

    /**
     * US160 - CR-Assign Direct Bill Delinquency to an Underwriter
     * 02/27/2015 Alvin Lee
     */

    assignDelinquencyToUnderwriter()

    this.inception()
    var eventDate = Date.CurrentDate
    this.evaluateEventDates_TDIC()
    var account = this.getAccount()
    var latestDueInvoiceItem = (this as PolicyDlnqProcess).Account.InvoiceItems
        .where(\elt -> elt.Invoice.Status == InvoiceStatus.TC_DUE)
        .orderByDescending(\it -> it.Invoice.DueDate)
        .thenByDescending(\it -> it.CreateTime).first()

    var delinquencyProcess = this as PolicyDlnqProcess
    var latestDueInvoice = delinquencyProcess.Account.Invoices.where(\elt -> elt.Due
        and elt.InvoiceItems.HasElements
        and elt.InvoiceItems.Count > 0
        and elt.InvoiceStream.PolicyPeriod_TDIC.Policy.PublicID == delinquencyProcess.PolicyPeriod.Policy.PublicID)
        .orderByDescending(\elt-> elt.DueDate)
        .thenByDescending(\it -> it.CreateTime).first()//.InvoiceItems.first().PolicyPeriod

    var latestDueNonZeroInvoice = (this as PolicyDlnqProcess).Account.Invoices.where(\elt -> elt.Due
        and elt.InvoiceItems.HasElements
        and elt.InvoiceItems.Count > 0
        and elt.InvoiceStream.AmountDue_TDIC > 0bd.ofDefaultCurrency()
        and elt.InvoiceStream.Policy.PublicID == delinquencyProcess.PolicyPeriod.Policy.PublicID)
        .orderByDescending(\elt-> elt.DueDate)
        .thenByDescending(\it -> it.CreateTime).first()

    logger.info("latest Due invoice : ${latestDueInvoice.InvoiceNumber} and Account Number : ${latestDueInvoice.Account.AccountNumber}")
    //GW-3081 - update latest due invoice for policy term associated with delinquency
    //(this as PolicyDlnqProcess).Invoice_TDIC = account.getLatestDueInvoice_TDIC((this as PolicyDlnqProcess).PolicyPeriod)
    (this as PolicyDlnqProcess).Invoice_TDIC = latestDueNonZeroInvoice != null ? latestDueNonZeroInvoice : latestDueInvoice
    //account.getLatestDueInvoice_TDIC(latestDueInvoiceItem.PolicyPeriod)

    // create an entry for the Account
    // since we are at inception and not creation, there is one Account-
    // level notice for each delinquency which enters inception.
    account.addHistoryFromGosu(
        eventDate,
        TC_ACCOUNTDELINQUENT,
        DisplayKey.get("Web.DelinquencyProcess.EntityDelinquent", (typeof account).DisplayName),
        null, null, false )

    // create an entry for the target if this is a not an account-
    // level delinquency
    if ( ! ( this.Target typeis Account ) ) {
      var typeName = (typeof this.Target).DisplayName
      if ( this.Target typeis PolicyPeriod ) {
        typeName = (typeof this.Target.Policy).DisplayName
      }
      this.Target.addHistoryEvent(
          eventDate,
          TC_POLICYDELINQUENT,
          DisplayKey.get("Web.DelinquencyProcess.EntityDelinquent",  typeName ) )
    }
  }

  function sendDunningLetterFromTarget() {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess");
    logger.trace ("Calling sendDunningLetterFromTarget on " + this);
    this.Target.sendDunningLetter(this)
  }

  function cancelTarget() {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess");
    logger.trace ("Calling cancelTarget on " + this);

    if (this typeis PolicyDlnqProcess) {
      var cancelPeriod = this.PolicyPeriod;
      var effectiveDate = this.calcCancelEffectiveDate_TDIC();
      if (effectiveDate.after(cancelPeriod.PolicyPerExpirDate)) {
        // Effective date is on the next term
        cancelPeriod = cancelPeriod.Policy.findPolicyPeriodByAsOfDate_TDIC(effectiveDate);
        if (cancelPeriod != null) {
          LoggerFactory.getLogger("Application.DelinquencyProcess").info("Cancellation effective date (" + effectiveDate
              + ") is after the end of the current term, cancellation will be processed on PolicyPeriod "
              + cancelPeriod);
        } else {
          LoggerFactory.getLogger("Application.DelinquencyProcess").error("Cancellation effective date (" + effectiveDate
              + ") is after the expiration date of the policy, cancellation will NOT be processed on PolicyPeriod " + cancelPeriod);
        }
      }
      if (cancelPeriod != null) {
        cancelPeriod.cancel(this);
      }
    } else {
      this.Target.cancel(this)
    }

    // cancel all policies if DelinquencyPlan says to
    if ( this.DelinquencyPlan.CancellationTarget == TC_ALLPOLICIESINACCOUNT ) {
      throw "Cancel target not implemented for " + this.DelinquencyPlan.CancellationTarget;
      // for ( plcyPeriod in this.Account.Policies*.PolicyPeriods.where(\ p -> p != this.Target)) {
      // plcyPeriod.cancel(this)
      // }
    }

    this.invokeTrigger(typekey.WorkflowTriggerKey.TC_CANCELEDINPAS)
  }

  function resetStagingDelinquencyPlanIfPossible() {
    // Reset account back to standard delinquency plan, as long as there aren't any loaded,
    // active delinquencies that haven't had their workflows started yet.
    var hasActiveNonStartedDelinquency =
        this.Account.DelinquencyProcesses.hasMatch(\ d ->
            d.RawStatus == TC_OPEN and d.Workflows.IsEmpty )
    if ( ! hasActiveNonStartedDelinquency ) {
      var stdDelinquencyPlanName = ScriptParameters.StandardDelinquencyPlan
      var stdPlanFinder = Query.make(DelinquencyPlan).compare("Name", Equals, stdDelinquencyPlanName).select()
      var stdPlan = stdPlanFinder.FirstResult
      this.Account.DelinquencyPlan = stdPlan
    }
  }

  function rescindOrReinstateTarget() {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess");
    logger.trace ("Calling rescindOrReinstateTarget on " + this);

    if (this typeis PolicyDlnqProcess) {
      if (this.CancelInfo_TDIC != null) {
        this.CancelInfo_TDIC.PolicyPeriod.rescindOrReinstate(this);
      } else {
        this.PolicyPeriod.rescindOrReinstate(this);
      }
    } else {
      this.Target.rescindOrReinstate(this)
    }
  }
  // === API called from workflow triggers =============================

  function exitDelinquency() {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess");
    logger.trace ("Calling exitDelinquency on " + this);
    this.ExitDate = Date.CurrentDate
    this.Phase = TC_EXITDELINQUENCY
    this.invokeTrigger( TC_EXITDELINQUENCY )
  }

  // === API called from entity ========================================
  /**
   * GW115 - Fix the update of 'Rescind/Reinstate' and 'Policy canceled' events in Delinquency Process
   * 12/3/2015 Hermia Kho
   *
   */
  /**
   * GW985 - 'Pending cancellation' not removed when payment is made
   * 6/2/2016 Hermia Kho
   * Fix incorrect } for the for loop "For (aPolicyPeriod in ...)" and add policyperiod term when querying invoice item to sum grossUnsettledAmount.
   * 7/1/2016 Hermia Kho
   * Fix code - Multi-check payment remove the past-due delinquency message but credit card payment did not.
   * Programming Notes: this.target.delinquentAmount value does not reflect the expected delinquent amount if the payment is processed thru multi-payment.
   * Therefore the system need to sum the grossUnsettled amount (delinquentAmount) on invoice items with invoice that are past due to determine the delinquent amount.  The
   * first part of the code reflects the calculation. That amount is then compared with the latest amount received to determine if exitDelinquency can be executed.
   * If the payment is via credit-card, the delinquent amount by the time this function is executed is already posted. If the delinquent amount is zero
   * then the delinquency is resolved and exitDelinquency is resolved.
   */
  /**
   *  GW2136 - onChargesPaid is executed when check payment, credit card payment or cancellation credit is invoked.
   *  Modify this function to catch cancellation credit.  If Billing Instruction = Cancellation, then exit the function.
   *  Per GW Support, the solution is to query the bundle as the data has not been committed at this point. If the Billing Instruction = Cancellation
   *  is found in the bundle, then execute a 'return'.
   */

  function onChargesPaid(chargesPaid : List<Charge>) {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess");
    logger.trace ("Calling onChargesPaid on " + this);
    var currentDate = java.util.Date.CurrentDate.trimToMidnight()
    //Do not process anything when came for cancellation BI
    if(chargesPaid.hasMatch(\ch -> ch.BillingInstruction.Subtype == BillingInstruction.TC_CANCELLATION
        and ch.ChargeDate.trimToMidnight() == currentDate)){
      logger.info("onChargesPaid || Returning for cancellation BI - doing nothing for the delinquency target "+this.Target)
      return
    }

    // NOTE: Collateral Delinquency Processes do not yet exist.  If you have choosen to start either
    // a policy or account level delinquency process for a collateral, you should provide
    // some kind of additional check either here or in DelinquencyTarget extension to
    // recognize if this delinquency is due to past due collateral, and to check the
    // Collateral due amount.  See Account.discoverDelinquencyProcesses()
    //GWPS-770 and GWPS-2040
    var period = (this.Target as PolicyPeriod)
    var delinquentAmount = period.Policy.OrderedPolicyPeriods?.sum(\s -> s.DelinquentAmount)
    logger.info("onChargesPaid || delinquentAmount >> " + delinquentAmount+" for policy period "+period+" || CancelStatus as  "+period.CancelStatus )

    //GWPS-2370 : Here as the delinquentAmount value is getting set to Zero if the Cancellation is getting bound so in that cases considering period.DueAmount field
    if(period.CancelStatus == PolicyCancelStatus.TC_CANCELED){
      logger.info("onChargesPaid || policy due Amount >> "+period.DueAmount+" for policy period "+period)
      delinquentAmount = period.DueAmount
    }

    // If the current delinquency amount is below the threshold to exit,
    // then trigger the process to stop.
    // This is only valid for Past Due delinquencies; configure exit for
    // configured delinquency types in a similar manner.
    // Adding for NotTaken delinquency.
    if ( ( this.Reason == TC_PASTDUE or this.Reason == TC_NOTTAKEN) and
        ( delinquentAmount.IsZero or delinquentAmount < this.DelinquencyPlan.getExitDelinquencyThreshold(this.Currency)) ) {
      exitDelinquency()
    }
  }

  // === other helpers =================================================

  function pushForwardHeldEvents(heldSince : Date) {
    var numDaysHeld = heldSince.differenceInDays( Date.CurrentDate )
    for (event in this.OrderedEvents?.toTypedArray()) {
      if (not event.Completed and event.ExactTargetDate == null) {
        if ( event.OffsetDays == null ) {
          event.OffsetDays = numDaysHeld
        } else {
          event.OffsetDays = event.OffsetDays + numDaysHeld
        }
      }
    }
  }

  function flagEventCompleted(eventName : DelinquencyEventName) {
    LoggerFactory.getLogger("Application.DelinquencyProcess").debug("DelinquencyProcessExt:: function flagEventCompleted " + eventName.DisplayName)
    var event : DelinquencyProcessEvent
    try {
      event = this.getProcessEventById(eventName)
      ArgCheck.nonNull(event, "DelinquencyProcessEvent \"" + eventName.Description + "\"")
    } catch ( e : Exception ) {
      this.Status = TC_ERROR
      BCLoggerCategory.DELINQUENCY_PROCESS.error("Error attempting to locate DelinquencyProcessEvent for eventId==\"" + eventName + "\" : " + e)
      throw e
    }
    event.flagCompleted()
  }

  function getApprovalDate(eventName : DelinquencyEventName) : Date {
    var event = this.getProcessEventById( eventName )
    return event.getTargetDate().addDays( -7 )
  }

  function createApprovalActivity(eventName : DelinquencyEventName, subject : String) : Activity {
    var event = this.getProcessEventById(eventName)

    if (event.ApprovalActivity == null) {
      var activityPattern = getActivityPattern("approval")
      var activity = new Activity(this.Bundle)
      activity.ActivityPattern = activityPattern
      activity.Subject = subject
      activity.Description = subject
      activity.TargetDate = event.TargetDate
      activity.EscalationDate = event.TargetDate
      activity.Priority = TC_NORMAL
      activity.Mandatory = true
      this.addToActivities(activity)
      event.ApprovalActivity = activity
    }

    return event.ApprovalActivity
  }

  function createAssignCAActivity(eventName : DelinquencyEventName, subject : String, desc : String) : Activity {
    var event = this.getProcessEventById(eventName)

    if (this.AssignCAActivity == null) {
      var activityPattern = getActivityPattern("notification")
      var activity = new Activity(this.Bundle)
      associateActivityWithPolicyPeriodAndAccount(activity)
      activity.ActivityPattern = activityPattern
      activity.Subject = subject
      activity.Description = desc
      activity.TargetDate = event.TargetDate
      activity.EscalationDate = event.TargetDate
      activity.Priority = TC_NORMAL
      activity.Mandatory = true
      this.addToActivities(activity)
      this.AssignCAActivity = activity
    }

    return this.AssignCAActivity
  }

  function getTargetDate(eventName : DelinquencyEventName) : Date {
    var event = this.getProcessEventById(eventName)
    return event.TargetDate
  }

  /**
   * US160 - CR-Assign Direct Bill Delinquency to an Underwriter
   * 02/27/2015 Alvin Lee
   *
   * Assigns this delinquency to the underwriter of the policy, retrieved from PolicyCenter.  This can only assign if
   * the target of the delinquency is a policy.
   */
  private function assignDelinquencyToUnderwriter() {
    LoggerFactory.getLogger("Application.DelinquencyProcess").trace ("DelinquencyProcessExt#assignDelinquencyToUnderwriter() - Attempting to assign delinquency for: " + this.Target.TargetDisplayName)
    var policyPeriod : PolicyPeriod = null
    var errorMessage : String = null
    // Obtain policy period for the policy number. Otherwise, unable to obtain underwriter field from PC since it is a policy-level field.
    if (this.Target typeis PolicyPeriod) {
      policyPeriod = this.Target
    }
    else if (this typeis PolicyDlnqProcess) {
      policyPeriod = this.PolicyPeriod
    }
    // An example of the target not being a policy period is if an account-level charge becomes delinquent.
    // Account-level billing will still have the target be a policy period as long as the delinquent charge is a policy-level charge.
    if (policyPeriod == null) {
      errorMessage = "Cannot assign delinquency since delinquency target is not a policy period.  Target: " + this.Target.TargetDisplayName
    }
    else {
      // Make call to PolicyCenter to retrieve Underwriter field
      var uwUserName = getUnderwriterFromPC(policyPeriod.PolicyNumber)
      if (uwUserName == null) {
        errorMessage = "Unable to retrieve Underwriter user name from PolicyCenter. Delinquency not assigned for policy: " + policyPeriod.PolicyNumberLong
      }
      else {
        // User must exist in BillingCenter
        var uwUser = GeneralUtil.findUserByUserName(uwUserName)
        if (uwUser == null) {
          errorMessage = "Unable to find user with user name '" + uwUserName
              + "' in BillingCenter. Delinquency not assigned for policy: " + policyPeriod.PolicyNumberLong
        }
        else  {
          LoggerFactory.getLogger("Application.DelinquencyProcess").trace ("DelinquencyProcessExt#assignDelinquencyToUnderwriter() - Attempting to assign delinquency to '"
              + uwUserName + "' for policy: " + policyPeriod.PolicyNumberLong)
          // User must be in a group to be properly assigned the delinquency
          var uwUserGroup : Group
          if (uwUser.GroupUsers != null) {
            uwUserGroup = uwUser.GroupUsers.first().Group
          }
          if (this.assign(uwUserGroup, uwUser)) {
            LoggerFactory.getLogger("Application.DelinquencyProcess").debug ("DelinquencyProcessExt#assignDelinquencyToUnderwriter() - Successfully assigned delinquency to user '" + uwUserName
                + "' and group '" + uwUserGroup.Name + "' for policy: " + policyPeriod.PolicyNumberLong)
          }
          else {
            errorMessage = "Unable to assign delinquency to user '" + uwUserName + "' and group '" + uwUserGroup.Name
                + "' for policy: " + policyPeriod.PolicyNumberLong
          }
        }
      }
    }
    // If there was an error, log and send an email notification
    if (errorMessage != null) {
      LoggerFactory.getLogger("Application.DelinquencyProcess").warn("DelinquencyProcessExt#assignDelinquencyToUnderwriter() - " + errorMessage)
      sendEmailNotification(errorMessage)
    }
  }

  /**
   * Makes API call to PolicyCenter to retrieve Underwriter user name for the specified policy.
   */
  @Param("policyNumber", "A String containing the policy number of the policy")
  @Returns("A String containing the user name of the Underwriter")
  private function getUnderwriterFromPC(policyNumber : String) : String {
    var uwUserName : String = null
    try {
      //var config = createWsiConfig()
      var api = new PolicyAPI()
      uwUserName = api.getPolicyUnderwriter(policyNumber)
    } catch (e: Exception) {
      LoggerFactory.getLogger("Application.DelinquencyProcess").warn("DelinquencyProcessExt#getUnderwriterFromPC() - Exception calling getPolicyUnderwriter from PolicyAPI web service for policy number: " + policyNumber, e)
    }
    return uwUserName
  }

  private function createWsiConfig() : WsdlConfig {
    // TODO mvu: platform will add more support to wsi webservice in studio so that we don't need
    // to hard code the password like this. Instead, we probably will enter the password in some config file in studio
    var config = new WsdlConfig()
    config.Guidewire.Locale = User.util.CurrentLocale.Code
    config.Guidewire.Authentication.Username = "su"
    config.Guidewire.Authentication.Password = "gw"
    return config
  }

  /**
   * Sends an email notification for failing to assign the delinquency.
   */
  @Param("message", "A String containing the error message")
  private function sendEmailNotification(message : String) {
    var notificationEmail = PropertyUtil.getInstance().getProperty("DelinquencyAssignmentEmail")
    if (notificationEmail == null) {
      LoggerFactory.getLogger("Application.DelinquencyProcess").error("DelinquencyProcessExt#sendEmailNotification() - Cannot retrieve notification email addresses with the key 'DelinquencyAssignmentEmail' from integration database.")
    }
    EmailUtil.sendEmail(notificationEmail, "Delinquency Assignment Failure on server " + gw.api.system.server.ServerUtil.ServerId,
        "The delinquency has failed to assign due to the following reason: " + message)
  }

  /**
   * US1364 Account-level Activities
   * 3/30/2015 Alvin Lee
   *
   * Tie activity to Policy Period and Account.
   */
  @Param("activity", "The Activity to associate with the Policy Period and/or Account")
  private function associateActivityWithPolicyPeriodAndAccount(activity : Activity) {
    if (this.Target typeis PolicyPeriod) {
      activity.PolicyPeriod = this.Target
      activity.Account = this.Target.Account
    }
    else if (this typeis PolicyDlnqProcess) {
      activity.PolicyPeriod = this.PolicyPeriod
      activity.Account = this.PolicyPeriod.Account
    }
    else if (this.Account != null) {
      activity.Account = this.Account
    }
  }

  private function getActivityPattern(patternCode : String) : ActivityPattern {
    return ActivityPatternsUtil.getActivityPattern(patternCode)
  }

  property get AmountDue_TDIC() : MonetaryAmount {
    return (this as PolicyDlnqProcess).PolicyPeriod.Policy.OrderedPolicyPeriods.sum(\s -> s.DelinquentAmount)
  }

  property get CurrentBalance_TDIC() : MonetaryAmount {
    return (this as PolicyDlnqProcess).PolicyPeriod.Policy.OrderedPolicyPeriods.sum(\s -> s.OutstandingUnsettledAmount) +
        (this as PolicyDlnqProcess).PolicyPeriod.Policy.OrderedPolicyPeriods.sum(\s -> s.UnbilledUnsettledAmount)
  }

  property get BarCode_TDIC(): String {
    var delinquency = this as PolicyDlnqProcess
    return TDIC_InvoicePURHelper.generateOCRBarCode(delinquency.Invoice_TDIC, CurrentBalance_TDIC, AmountDue_TDIC);
  }

  property get DueDate_TDIC(): Date {
    return this.CreateTime.addDays(10)
  }

  // Find out the list of policyperiods associated with the delinquencyprocess
  // Check for the Due invoices which has some due amount pending
  property get AssociatedPolicyPeriodsCount() : int {
    var delinquencyProcess = this as PolicyDlnqProcess
    var targetPolicyPeriodInvoiceCount = 0
    var dueInvoices = delinquencyProcess.Account.Invoices.where(\elt -> elt.Due
        and elt.InvoiceItems.HasElements
        and elt.InvoiceItems.Count > 0
        and elt.InvoiceStream.PolicyPeriod_TDIC.Policy.PublicID == delinquencyProcess.PolicyPeriod.Policy.PublicID
        and elt.AmountDue.IsNotZero)
        .orderByDescending(\elt-> elt.DueDate)
        .thenByDescending(\it -> it.CreateTime)
    // GBC-3128 - two term delinquency, current term paid, audit unpaid
    // dueInvoices variable takes care of the multi-term scenario delinquency where the latest term invoices are not fully paid yet.
    // If the delinquency is open for the prior term charge and the current term charges are fully paid then also
    // we want this method to return more than 1. Check how the Invoice_TDIC is set, for understanding the if loop below.
    if (delinquencyProcess.Invoice_TDIC.InvoiceStream.PolicyPeriod_TDIC != delinquencyProcess.PolicyPeriod) {
      targetPolicyPeriodInvoiceCount = 1
    } else {
      targetPolicyPeriodInvoiceCount = 0
    }

    var policyTermList : Set<Integer> = {}
    if(dueInvoices.HasElements and dueInvoices.size() > 0){
      dueInvoices*.InvoiceItems?.each(\ii -> {
        policyTermList.add(ii.PolicyPeriod.TermNumber)
      })
    }
    return policyTermList.Count + targetPolicyPeriodInvoiceCount
  }

  function notifyERENonPaymentToPC() {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess")

    if (this typeis PolicyDlnqProcess) {
      var policyPeriod = this.PolicyPeriod
      var dlnqProcess = this as PolicyDlnqProcess
      if (dlnqProcess.Invoice_TDIC.InvoiceItems.where(\elt -> elt.Charge.ChargePattern.ChargeCode == "EREPremium_TDIC").HasElements) {
        dlnqProcess.addEvent(PASMessageTransport.EVENT_ERE_CHARGE_WITHDRAWAL)
        logger.info("Notification to PC has been sent for the NonPayment of ERE charge for PolicyPeriod :  ${policyPeriod}")
      }
      logger.debug("ERE Charge withdrawl event not needed.")
    }
  }

  /**
   * Evaluate the event date and set it for the Not Taken delinquency workflow
   * GBC- 3179 Update Delinquency Schedule for cancel prior to Effective scenario
   */
  function evaluateEventDates_TDIC() {
    if(this typeis PolicyDlnqProcess){
      var policyPeriod = this.PolicyPeriod

      if(policyPeriod.Status == PolicyPeriodStatus.TC_FUTURE){
        // Set Target Date of the Notice of Intent to cancel event
        var noicEvent = this.ProcessEvents.firstWhere(\elt -> elt.EventName == DelinquencyEventName.TC_CANCELLATION)
        if(!noicEvent.Completed and this.InceptionDate.addDays(noicEvent.OffsetDays).before(policyPeriod.PolicyPerEffDate)){
          noicEvent.TargetDate = policyPeriod.PolicyPerEffDate
        }
        // Set Target Date of the Cancellation Request to PAS event
        var crPASEvent = this.ProcessEvents.firstWhere(\elt -> elt.EventName == DelinquencyEventName.TC_CANCELINPAS)
        if(!crPASEvent.Completed and this.InceptionDate.addDays(crPASEvent.OffsetDays).before(policyPeriod.PolicyPerEffDate)){
          crPASEvent.TargetDate = policyPeriod.PolicyPerEffDate
        }
      }
    }
  }

  /**
   * Function get the if the payment is distributed or not
   * @param moneyReceived
   * @param payment
   * @return MonetaryAmount
   */
  function getAmtDistForPayments(moneyReceived: entity.PaymentMoneyReceived, payment: entity.DirectBillPayment) : gw.pl.currency.MonetaryAmount {
    return moneyReceived.DistributedDenorm ? payment.NetDistributedToInvoiceItems : null
  }

  /**
   * Method to get the unapplied fund from Payments that happened after Cancellation Date
   * and Disbursements that happened before Cancellation in order to get exact Due Amount that insured has to pay
   * to Reinstate the policy
   * @param period
   * @return MonetaryAmount
   */
  //GWPS-770 and GWPS-2040
  function getUnDistributedAmountForPolicy(period : PolicyPeriod) : gw.pl.currency.MonetaryAmount {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess")
    logger.info("Entered getUnDistributedAmountForPolicy method")
    var moneyReceiveds = period.Policy.Account.findReceivedPaymentMoneysSortedByReceivedDate().where(\elt -> elt.UnappliedFund.PolicyPeriod_TDIC == period)
    var cancellationDate = period.Cancellation.CreateTime

    var amountUnDistributed = moneyReceiveds.where(\moneyRcvd -> moneyRcvd.ReceivedDate?.trimToMidnight() >= cancellationDate?.trimToMidnight()
        and this.getAmtDistForPayments(moneyRcvd, moneyRcvd.DirectBillPayment) == null and moneyRcvd.ReversalDate == null)?.sum(\elt -> elt.Amount)

    var dist = new gw.api.web.disbursement.DisbursementUtil().getAccountAndCollateralDisbursements(period.Policy.Account)
    var disbursementAmount = dist.where(\disbursement -> disbursement typeis AccountDisbursement and disbursement.CreateTime?.trimToMidnight() <= cancellationDate?.trimToMidnight()
        and disbursement.Status != DisbursementStatus.TC_SENT and disbursement.UnappliedFund.DisplayName == period.DisplayName)?.sum(\elt -> elt.Amount)
    var amtInUnappliedFundForPolicy = amountUnDistributed + disbursementAmount
    logger.info("UnDistributed amount from Payment and Disbursements"+amountUnDistributed+" and "+disbursementAmount)
    logger.info("Exited getUnDistributedAmountForPolicy method")
    return amtInUnappliedFundForPolicy

  }
}