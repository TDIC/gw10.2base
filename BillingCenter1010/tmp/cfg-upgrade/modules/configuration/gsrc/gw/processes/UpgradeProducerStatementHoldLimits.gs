package gw.processes

uses gw.plugin.Plugins
uses gw.plugin.parameter.ISystemParameters

/**
 *  This batch is meant to run once post upgrade to 9.0 to default the values on the new fields on Producer -
 *  HoldStatement, StatementHoldNegativeLimit and StatementHoldPositiveLimit.
 *  Before running the batch, this can be overridden to arrive at the defaults differently, per producer, using custom logic.
 *  The OOTB logic tries to mimic the older pre-9.0 behavior using the auto payment threshold from the ISystemParameters
 *  plugin to set these values.
 */
@Export
class UpgradeProducerStatementHoldLimits extends UpgradeProducerStatementHoldLimitsBase {
  /*
   * Override this method to supply custom default values for HoldStatement, StatementHoldNegativeLimit and
   * StatementHoldPositiveLimit per producer.
   */

  override function getStatementHoldInfoForProducer(producer: Producer, statementHoldInfo: StatementHoldInfo) {
    var systemParametersPlugin = Plugins.get(ISystemParameters)
    var autoPaymentThreshold = systemParametersPlugin.getProducerAutoPaymentThreshold(producer)
    var hundredInProducersCurrency = 100bd.ofCurrency(producer.Currency)

    if (autoPaymentThreshold == null) {
      if (producer.HoldStatement) {
        statementHoldInfo.StatementHoldNegativeLimit = hundredInProducersCurrency.negate()
      }
    } else {
      //autoPaymentThreshold is non-null
      statementHoldInfo.StatementHoldPositiveLimit = autoPaymentThreshold
      if (producer.HoldStatement) {
        statementHoldInfo.StatementHoldNegativeLimit = autoPaymentThreshold.max(hundredInProducersCurrency).negate()
      } else {
        statementHoldInfo.StatementHoldNegativeLimit = autoPaymentThreshold.negate()
        statementHoldInfo.HoldStatement = true
      }
    }
  }
}