package gw.webservice.policycenter.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/DisplayableBillingStatus" )
@Export
final class DisplayableBillingStatus {
  private var _Delinquent : boolean as Delinquent
  private var _TotalBilled : MonetaryAmount as TotalBilled
  private var _TotalBilledUnsettled : MonetaryAmount as TotalBilledUnsettled
  private var _PastDue : MonetaryAmount as PastDue
  private var _Unbilled : MonetaryAmount as Unbilled
  private var _BillingMethod : String as BillingMethodCode

  private var _TermUnappliedAmount_TDIC : MonetaryAmount as TermUnappliedAmount_TDIC;
  private var _CurrentBalance_TDIC : MonetaryAmount as CurrentBalance_TDIC;
  private var _WrittenOff_TDIC : MonetaryAmount as WrittenOff_TDIC

  construct() { }

  construct(policyPeriod : PolicyPeriod) {
    Delinquent = policyPeriod.hasActiveDelinquenciesOutOfGracePeriod()
    TotalBilled = policyPeriod.BilledAmount
    TotalBilledUnsettled = policyPeriod.BilledUnsettledAmount
    PastDue = policyPeriod.DelinquentAmount
    Unbilled = policyPeriod.UnbilledAmount
    BillingMethodCode = policyPeriod.BillingMethod.Code

    //var invoiceStream = policyPeriod.OverridingInvoiceStream;
    var invoiceStream = policyPeriod?.InvoiceItemsSortedByEventDate?.first()?.Invoice?.InvoiceStream

    if(invoiceStream != null){
      _TermUnappliedAmount_TDIC = invoiceStream?.UnappliedFund?.Balance - policyPeriod.DelinquencyCancelAmount_TDIC
      _CurrentBalance_TDIC = invoiceStream?.CurrentBalance_TDIC

    } else {

      _TermUnappliedAmount_TDIC = new MonetaryAmount(0.00bd, Currency.TC_USD)
      _CurrentBalance_TDIC = new MonetaryAmount(0.00bd, Currency.TC_USD)
    }

    //20170807 TJT: GW-2848
    _WrittenOff_TDIC = policyPeriod.WrittenOff_TDIC
  }
}