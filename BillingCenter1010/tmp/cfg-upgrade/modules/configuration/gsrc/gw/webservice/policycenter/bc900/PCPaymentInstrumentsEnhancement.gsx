package gw.webservice.policycenter.bc900

uses gw.api.locale.DisplayKey
uses gw.webservice.bc.bc900.PaymentInstrumentRecord
uses gw.webservice.bc.bc900.PaymentInstruments

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement PCPaymentInstrumentsEnhancement : PaymentInstruments {
  
  public static function toPCRecord(instrument : PaymentInstrument) : PaymentInstrumentRecord {
    var record = PaymentInstruments.toRecord(instrument)
    if (instrument.PaymentMethod == TC_RESPONSIVE) {
      record.DisplayName = DisplayKey.get("PaymentInstrument.API.Responsive.PCDisplayName")      
    }
    return record
  }

}
