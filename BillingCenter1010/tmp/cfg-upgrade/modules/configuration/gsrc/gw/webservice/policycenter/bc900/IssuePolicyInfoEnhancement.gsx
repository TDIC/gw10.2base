package gw.webservice.policycenter.bc900

uses gw.api.database.Query
uses gw.api.domain.invoice.InvoiceStreamFactory
uses gw.api.locale.DisplayKey
uses gw.api.web.invoice.InvoicingOverrider
uses gw.api.web.policy.NewPolicyUtil
uses gw.api.webservice.exception.BadIdentifierException
uses gw.pl.persistence.core.Bundle
uses gw.plugin.Plugins
uses gw.plugin.invoice.IPeriodicities
uses gw.webservice.policycenter.bc900.entity.types.complex.IssuePolicyInfo
uses gw.webservice.util.WebserviceEntityLoader
uses typekey.Currency
uses entity.InvoiceStream

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement IssuePolicyInfoEnhancement : IssuePolicyInfo {
  function executeIssuanceBI() : BillingInstruction {
    return executedIssuanceBISupplier()
  }

  /**
   * Local {@link NewPlcyPeriodBI new PolicyPeriod Billing Instruction}
   *    initialization. Base {@link PlcyBillingInstruction} initialization
   *    occurs in {@link BillingInstructionInfo#execute}.
   * <p/>
   * Setting of the {@link NewPlcPeriodBI#setPolicyPaymentPlan PolicyPaymentPlan}
   * must be invoked <em>before</em> {@link #populateIssuanceInfo()} when prior
   * {@link PolicyPeriod} exists but not otherwise because of invoicing override
   * (ListBill) validation that occurs.
   */
  function initPolicyPeriodBIInternal(final bi: NewPlcyPeriodBI) {
    bi.PrimaryProducerCodeRoleEntry.ProducerCode = this.ProducerCode
    bi.PolicyPaymentPlan = this.PaymentPlan
  }

  private function createNewPolicyBIInternal() : NewPlcyPeriodBI {
    return NewPolicyUtil.createIssuance(
        findOwnerAccount(), createPolicyPeriod())
  }

  /**
   * Look-up existing owner {@link Account account} for this
   *    {@link IssuePolicyInfo issue}.
   *
   * If the currency of the {@code Account} is not the same as for the {@code
   * IssuePolicyInfo}, then find one or create a new one in the same {@link
   * MixedCurrencyAccountGroup} for the existing owner {@code account}.
   */
  function findOwnerAccount() : Account {
    var account = findExistingAccount(this.AccountNumber)
    if (account.Currency != CurrencyValue) {
      /* Get associated splinter account for different currency... */
      account = findOrCreateSplinterCurrencyAccount(account)
    }
    return account
  }

  private function findExistingAccount(accountNumber : String) : Account {
    var account = findAccount(accountNumber)
    if (account == null) {
      throw new BadIdentifierException(DisplayKey.get("BillingAPI.Error.AccountNotFound", accountNumber))
    }
    return account
  }

  private function findAccount(accountNumber : String) : Account {
    // The only time there's a new account in the bundle is when it was made by getOrCreateAccountForPreview()
    var tempAccountForPreview = gw.transaction.Transaction.getCurrent().getBeansByRootType(Account)
      .firstWhere(\ b -> b typeis Account && b.AccountNumber == accountNumber) as Account

    return tempAccountForPreview
        ?: Query.make(Account).compare("AccountNumber", Equals, accountNumber).select().AtMostOneRow
  }

  /**
   * Try to find a match using the publicID (assuming publicID is not null).
   * If a match is found, the Policy Period already exists. Otherwise, create the new Policy Period
   */
  function createPolicyPeriod(isForPreview : boolean = false) : PolicyPeriod {
    if (this.PCPolicyPublicID != null
        and this.findByPolicyPublicIDAndTerm(this.PCPolicyPublicID, this.TermNumber) != null) {
      throw new BadIdentifierException(
          DisplayKey.get("Webservice.Error.PolicyPeriodExists", this.PCPolicyPublicID))
    }
    var policy = new Policy(CurrencyValue)
    var period = new PolicyPeriod(policy.Currency)
    policy.addToPolicyPeriods( period )
    period = populateIssuanceInfo(isForPreview, period)
    return period
  }

  function findOrCreateOwnerAccountForPreview() : Account {
     var account = findOrCreateAccountForPreview(this.AccountNumber)
    if (account.Currency != typekey.Currency.get(this.Currency)) {
      account = findOrCreateSplinterCurrencyAccount(account)
    }
    return account
  }

  private function findOrCreateAccountForPreview(accountNumber : String) : Account {
    var account = findAccount(accountNumber)
    if (account == null) {
      account = new Account(typekey.Currency.get(this.Currency))
      account.AccountNumber = this.AccountNumber
      PCAccountInfoEnhancement.initializeAccountDefaults(account)
    }
    return account
  }

  function toIssuanceForPreview() : Issuance {
    final var account = findOrCreateOwnerAccountForPreview()
    final var period = createPolicyPeriod(true)

    var issuance = new Issuance(period.Currency)
    issuance.IssuanceAccount = account
    issuance.initializeIssuancePolicyPeriod(period)
    issuance.PrimaryProducerCodeRoleEntry.ProducerCode = this.ProducerCode
    issuance.PolicyPaymentPlan = this.PaymentPlan
    this.createCharges(issuance)
    return issuance
  }

  /**
   * The {@link ProducerCode} of record for the {@link PolicyPeriod} being
   * issued.
   */
  property get ProducerCode() : ProducerCode {
    if (this.ProducerCodeOfRecordId == null) {
      return null
    }
    var mainProducerCode = WebserviceEntityLoader
        .loadByPublicID<ProducerCode>(this.ProducerCodeOfRecordId, "ProducerCodeOfRecordId")
    if (mainProducerCode.Currency == this.CurrencyValue) {
      return mainProducerCode
    }
    final var producerCurrencyGroup = mainProducerCode.Producer.ProducerCurrencyGroup
    if (producerCurrencyGroup != null) {
      // look in splinter Producer for Currency ProducerCodes...
      final var splinterCode = Query.make(entity.ProducerCode)
          .compare("Code", Equals, mainProducerCode.Code)
          .subselect("Producer", CompareIn, Query.make(ProducerCurrencyGroup)
              .compare("CurrencyInGroup", Equals, this.CurrencyValue)
              .compare("ForeignEntity", Equals, producerCurrencyGroup),
            "Owner")
        .select().AtMostOneRow
      if (splinterCode != null) {
        return splinterCode
      }
    }
    throw new BadIdentifierException(
        DisplayKey.get("BillingAPI.Error.ProducerCodeForCurrencyDoesNotExist",
            this.ProducerCodeOfRecordId, this.CurrencyValue))
  }

  function populateIssuanceInfo(isForPreview : Boolean, policyPeriod : PolicyPeriod) : PolicyPeriod {
    this.populateChangeInfo( policyPeriod )
    policyPeriod.BillingMethod = PolicyPeriodBillingMethod.get(this.BillingMethodCode)
    final var overridingPayerAccount = findOverridingPayerAccount(isForPreview)
    if (overridingPayerAccount != policyPeriod.OverridingPayerAccount) {
      policyPeriod.updateWith(
          new InvoicingOverrider().withOverridingPayerAccount(overridingPayerAccount))
    }

    final var overridingInvoiceStream =
        policyPeriod.AgencyBill
            ? null
            : getOverridingInvoiceStream(policyPeriod.Bundle, overridingPayerAccount, policyPeriod.ListBill)
    if (overridingInvoiceStream != policyPeriod.OverridingInvoiceStream) {
      policyPeriod.updateWith(
          new InvoicingOverrider().withOverridingInvoiceStream(overridingInvoiceStream))
    }
    policyPeriod.PolicyNumber = this.PolicyNumber
    policyPeriod.Policy.PCPublicID = this.PCPolicyPublicID
    policyPeriod.Policy.LOBCode = typekey.LOBCode.get(this.ProductCode)
    policyPeriod.BoundDate = this.ModelDate.toCalendar().Time
    policyPeriod.AssignedRisk = this.AssignedRisk
    policyPeriod.UWCompany = typekey.UWCompany.get(this.UWCompanyCode)
    policyPeriod.EligibleForFullPayDiscount = false
    policyPeriod.HoldInvoicingWhenDelinquent = false
    policyPeriod.ConfirmationNotificationState = TC_NOTIFYUPONSUFFICIENTPAYMENT
    policyPeriod.TermConfirmed = this.TermConfirmed

    if (this.HasScheduledFinalAudit) {
      policyPeriod.scheduleFinalAudit()
    }
    return policyPeriod
  }

  property get CurrencyValue() : Currency {
    return Currency.get(this.Currency)
  }

  /**
   * The {@link PaymentPlan} for the {@link PolicyPeriod} being issued.
   */
  property get PaymentPlan() : PaymentPlan {
    var paymentPlan = WebserviceEntityLoader
        .loadByPublicID<PaymentPlan>(this.PaymentPlanPublicId, "PaymentPlanPublicId")
    if (!paymentPlan.supportsCurrency(CurrencyValue)) {
      throw new BadIdentifierException(
          DisplayKey.get("Webservice.Error.PaymentPlanBadCurrency",
              this.PaymentPlanPublicId, paymentPlan.Currencies.join(", "), CurrencyValue))
    }
    return paymentPlan
  }

  private function findOverridingPayerAccount(isForPreview : boolean) : Account {
    if (this.AltBillingAccountNumber == null) {
       return null
    }
    var payerAccount = isForPreview
        ? findOrCreateAccountForPreview(this.AltBillingAccountNumber)
        : findExistingAccount(this.AltBillingAccountNumber)
    if (payerAccount.Currency != CurrencyValue) {
      /* Get associated splinter account for different currency... */
      payerAccount = findOrCreateSplinterCurrencyAccount(payerAccount)
    }
    return payerAccount
  }

  private function getOverridingInvoiceStream(bundle : Bundle, overridingPayer : Account, isListBill : boolean) : InvoiceStream {
    if (this.InvoiceStreamId != null) {
      return findInvoiceStreamWithPublicId(this.InvoiceStreamId)
    }
    if (this.NewInvoiceStream != null) {
      final var payer = overridingPayer ?: findOwnerAccount()
      return this.NewInvoiceStream.$TypeInstance.createInvoiceStreamFor(payer, bundle)
    }
    if (isListBill) {
      var periodicity = this.PaymentPlan.Periodicity
      // If the PaymentPlan's periodicity is Quarterly, for example, the InvoiceStream that we find or create should be monthly
      var invoiceStreamPeriodicity = Plugins.get(IPeriodicities).isMultipleOfMonthly(periodicity) ? Periodicity.TC_MONTHLY : periodicity
      return overridingPayer.InvoiceStreams.firstWhere(\ stream -> stream.Periodicity == invoiceStreamPeriodicity)
          ?: InvoiceStreamFactory.createInvoiceStreamFor(overridingPayer, invoiceStreamPeriodicity)
    }
    return null
  }

  private function findInvoiceStreamWithPublicId(publicID : String) : InvoiceStream {
    return WebserviceEntityLoader.loadInvoiceStream(publicID)
  }

  /**
   * Look up or create a splinter currency account for the specified account.
   *
   * The currency for the account is that specified on this {@link IssuePolicyInfo}.
   *
   * @param account the {@link Account} whose {@link Currency} is different than that
   *                of the policy to be issued by this {@code IssuePolicyInfo}.
   * @return The splinter currency {@code Account}.
   */
  private function findOrCreateSplinterCurrencyAccount(final account : Account) : Account {
    var splinterAccount : Account
    if (account.AccountCurrencyGroup == null) {
      splinterAccount = BillingAPI.createAccountForCurrency(account, CurrencyValue)
    } else {
      splinterAccount = findExistingAccountForCurrency(account.AccountCurrencyGroup)
      if (splinterAccount == null) {
        splinterAccount = BillingAPI.createAccountForCurrency(account, CurrencyValue)
      }
    }
    return splinterAccount
  }

  /**
   * Find and return existing sibling account for the specified account group
   *    with the currency value for this info'.
   */
  private function findExistingAccountForCurrency(accountGroup : MixedCurrencyAccountGroup) : Account {
    return BillingAPI.findExistingAccountForCurrency(accountGroup, CurrencyValue)
  }

  function executedIssuanceBISupplier(): NewPlcyPeriodBI {
    var bi = createNewPolicyBIInternal()
    initPolicyPeriodBIInternal(bi)
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
