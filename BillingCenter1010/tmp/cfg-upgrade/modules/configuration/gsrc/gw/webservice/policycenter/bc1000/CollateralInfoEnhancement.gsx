package gw.webservice.policycenter.bc1000

uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.webservice.policycenter.bc1000.entity.types.complex.CollateralInfo

@Export
enhancement CollateralInfoEnhancement: CollateralInfo {
  function executeCollateralBI() : BillingInstruction {
    return executedCollateralBISupplier()
  }

  function executeSegregatedCollateralBI() : BillingInstruction {
    return executedSegregatedCollateralBISupplier()
  }

  private function createCollateralBI(): CollateralBI {
    var bi = new CollateralBI(Currency.get(this.Currency))
    var account = this.findOwnerAccount()
    bi.Account = account
    bi.CollateralRequirement = findCollateralRequirement(account, this.CollateralRequirementID)
    return bi
  }

  private function createSegregatedCollateralBI(): SegregatedCollReqBI {
    var bi = new SegregatedCollReqBI(Currency.get(this.Currency))
    var account = this.findOwnerAccount()
    bi.Account = account
    bi.SegregatedCollReq = findSegregatedCollateralRequirement(account, this.CollateralRequirementID)
    return bi
  }

  private function findCollateralRequirement(account : Account, collateralRequirementID : String) : CollateralRequirement {
    var req = account.Collateral.Requirements.firstWhere( \ cr -> cr.PublicID == collateralRequirementID)
    if (req == null && collateralRequirementID != null) {
      throw new BadIdentifierException(DisplayKey.get("BillingAPI.Error.CollateralReqNotFound", account.AccountNumber, collateralRequirementID))
    } else if (req.Segregated) {
      throw new BadIdentifierException(DisplayKey.get("BillingAPI.Error.CollateralReqSegregated", collateralRequirementID))
    }
    return req
  }

  private function findSegregatedCollateralRequirement(account : Account, collateralRequirementID : String) : CollateralRequirement {
    if (collateralRequirementID == null) {
      throw new RequiredFieldException(DisplayKey.get("BillingAPI.Error.CollateralReqRequired"))
    }
    var req = account.Collateral.Requirements.firstWhere( \ cr -> cr.PublicID == collateralRequirementID)
    if (req == null) {
      throw new BadIdentifierException(DisplayKey.get("BillingAPI.Error.CollateralReqNotFound", account.AccountNumber, collateralRequirementID))
    } else if (!req.Segregated) {
      throw new BadIdentifierException(DisplayKey.get("BillingAPI.Error.CollateralReqNotSegregated", collateralRequirementID))
    }
    return req
  }

  function executedCollateralBISupplier(): CollateralBI {
    var bi = createCollateralBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }

  function executedSegregatedCollateralBISupplier(): SegregatedCollReqBI {
    var bi = createSegregatedCollateralBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
