package gw.webservice.policycenter.bc900

uses gw.webservice.policycenter.bc900.entity.types.complex.PremiumReportInfo

uses java.lang.*

/**
 * Defines behavior for the 900 API version of the WSDL entity that specifies
 * the data used to create a {@link entity.PremiumReportBI PremiumReport}
 * billing instruction.
 */
@Export
@gw.lang.Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement PremiumReportInfoEnhancement : PremiumReportInfo {
  function executePremiumReportBI(): BillingInstruction {
    return executedPremiumReportBISupplier()
  }

  /**
   * @deprecated Since 9.0.1. Use {@link #executePremiumReportBI()} instead.
   */
  @java.lang.Deprecated
  function execute() : String{
    return executePremiumReportBI().PublicID
  }

  private function createPremiumReportBI() : PremiumReportBI {
    final var policyPeriod = this.findPolicyPeriod()
    final var bi = new PremiumReportBI(policyPeriod.Currency)
    bi.AssociatedPolicyPeriod = policyPeriod
    bi.PeriodStartDate = this.AuditPeriodStartDate.toCalendar().Time
    bi.PeriodEndDate = this.AuditPeriodEndDate.toCalendar().Time
    bi.PaymentReceived = this.PaymentReceived
    return bi
  }

  function toPremiumReportForPreview() : PremiumReportBI {
    final var bi = createPremiumReportBI()
    this.initializeBillingInstruction(bi)
    return bi
  }

  function executedPremiumReportBISupplier(): PremiumReportBI {
    var bi = createPremiumReportBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
