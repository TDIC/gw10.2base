package gw.webservice.policycenter.bc900

uses gw.webservice.policycenter.bc900.entity.types.complex.FinalAuditInfo

uses java.lang.*

/**
 * Defines behavior for the 900 API version of the WSDL entity that specifies
 * the data used to create a final {@link entity.Audit Audit} billing
 * instruction.
 */
@Export
@gw.lang.Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement FinalAuditInfoEnhancement : FinalAuditInfo {
  function executeFinalAuditBI(): BillingInstruction {
    return executedFinalAuditBISupplier()
  }

  /**
   * @deprecated Since 9.0.1. Use {@link #executeFinalAuditBI()} instead.
   */
  @java.lang.Deprecated
  function execute() : String {
    return executeFinalAuditBI().PublicID
  }

  private function createFinalAuditBI() : Audit {
    final var policyPeriod = this.findPolicyPeriod()

    final var bi = new Audit(policyPeriod.Currency)
    bi.AssociatedPolicyPeriod = policyPeriod
    return bi
  }

  function toFinalAuditForPreview() : Audit {
    final var bi = createFinalAuditBI()
    this.initializeBillingInstruction(bi)
    return bi
  }

  function executedFinalAuditBISupplier(): Audit {
    var bi = createFinalAuditBI()
    bi.FinalAudit = true
    bi.TotalPremium = false
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
