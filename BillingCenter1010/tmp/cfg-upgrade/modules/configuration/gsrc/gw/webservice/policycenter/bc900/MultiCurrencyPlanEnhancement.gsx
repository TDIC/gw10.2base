package gw.webservice.policycenter.bc900

uses gw.api.database.Queries
uses gw.api.database.Relop

@Export
enhancement MultiCurrencyPlanEnhancement: MultiCurrencyPlan {
  public property get isSingleCurrency(): boolean{
    return this.Currencies.length == 1
  }
}
