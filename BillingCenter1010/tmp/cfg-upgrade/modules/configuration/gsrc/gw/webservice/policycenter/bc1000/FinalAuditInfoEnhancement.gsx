package gw.webservice.policycenter.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.webservice.policycenter.bc1000.entity.types.complex.FinalAuditInfo

/**
 * Defines behavior for the 910 API version of the WSDL entity that specifies
 * the data used to create a final {@link Audit Audit} billing
 * instruction.
 */
@Export
enhancement FinalAuditInfoEnhancement : FinalAuditInfo {
  function executeFinalAuditBI(): BillingInstruction {
    return executedFinalAuditBISupplier()
  }

  /**
   * @deprecated Since 9.0.1. Use {@link #executeFinalAuditBI()} instead.
   */
  @java.lang.Deprecated
  function execute() : String {
    return executeFinalAuditBI().PublicID
  }

  private function createFinalAuditBI() : Audit {
    final var policyPeriod = this.findPolicyPeriod()
    final var bi = new Audit(policyPeriod.Currency)
    bi.AssociatedPolicyPeriod = policyPeriod
    return bi
  }

  function toFinalAuditForPreview() : Audit {
    final var bi = createFinalAuditBI()
    this.initializeBillingInstruction(bi)
    return bi
  }

  function executedFinalAuditBISupplier(): Audit {
    var bi = createFinalAuditBI()
    bi.FinalAudit = true
    bi.TotalPremium = false
    // BrianS - Set the audit revision type from PolicyCenter.
    bi.RevisionType_TDIC = typekey.AuditRevisionType_TDIC.get(this.RevisionType_TDIC);
    this.initializeBillingInstruction(bi)

    /**
     * US669
     * 05/01/2015 Shane Murphy
     *
     * Values required for Audit documents Document Production
     */
    var policyPeriod = bi.PolicyPeriod
    bi.Bundle.add(policyPeriod)
    policyPeriod.AuditPeriodStart_TDIC = this.AuditPeriodStart_TDIC.toCalendar().Time
    policyPeriod.AuditPeriodEnd_TDIC = this.AuditPeriodEnd_TDIC.toCalendar().Time
    policyPeriod.TotalPremiumRPT_TDIC = this.TotalPremiumRPT_TDIC
    policyPeriod.TaxesAndSurcharges_TDIC = this.TaxesAndSurcharges_TDIC
    policyPeriod.TotalCostRPT_TDIC = this.TotalCostRPT_TDIC
    policyPeriod.TotalEstimatedCostRPT_TDIC = this.TotalEstimatedCostRPT_TDIC
    policyPeriod.Difference_TDIC = this.Difference_TDIC
    policyPeriod.AuditedBasis_TDIC = this.AuditedBasis_TDIC

    /**
     * US64 - Maintain Policy Payment Schedule
     * 09/11/2014 Vicente
     *
     * Integration OOTB modified to receive the AuditMethod If set the FinalAuditComplete_TDIC flag if the Audit Method
     * is not Estimated
     */
    if (this.AuditMethod_TDIC != "Estimated") {
      policyPeriod.FinalAuditComplete_TDIC = true
      /**
       * US 645 - Document Production
       * 01/06/15 shanem
       *
       * Final Audit Date required for Final Audit letter. Only set if FinalAuditComplete is true
       */
      policyPeriod.PremiumFinalAuditYear_TDIC = this.PremiumFinalAuditYear_TDIC
    }

    // GBC-3172 - Credits/refunds must be disbursed immediately
    var auditChargesAmountList = new ArrayList<MonetaryAmount>()
    if(this.ChargeInfos != null and this.ChargeInfos.HasElements){
      this.ChargeInfos.each(\elt -> {
        auditChargesAmountList.add(new MonetaryAmount(elt.Amount))
      })
    }

    var totalAuditChargesAmount = auditChargesAmountList.sum()

    if(totalAuditChargesAmount.IsNegative){
      bi.SpecialHandling = SpecialHandling.TC_BILLFUTUREITEMSIMMDT_TDIC
    }
    // When a WC policy has been renewed and is leveraging Policy Level Billing, any FA received from a prior term on
    // Policy Period Billing shall bill on the following 1st of the Month/Account's Default Invoice Day of Month.
    if(policyPeriod != policyPeriod.LatestPolicyPeriod and totalAuditChargesAmount.IsPositive){
      bi.SpecialHandling = SpecialHandling.TC_FABILLONNEXT_TDIC
    }

    bi.execute()
    return bi
  }
}
