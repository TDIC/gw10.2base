package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.ReinstatementInfo

/**
 * Defines behavior for the 910 API version of the WSDL entity that specifies
 * the data used to create a {@link Reinstatement Reinstatement} billing
 * instruction.
 */
@Export
enhancement ReinstatementInfoEnhancement : ReinstatementInfo {
  function executeReinstatementBI(): BillingInstruction {
    return executedReinstatementBISupplier()
  }

  /**
   * @deprecated Since 9.0.1. Use {@link #executeReinstatementBI()} instead.
   * @return
   */
  @java.lang.Deprecated
  function execute() : String {
    return executeReinstatementBI().PublicID
  }

  private function createReinstatementBI() : Reinstatement {
    var policyPeriod = this.findPolicyPeriod()
    final var bi = new Reinstatement(policyPeriod.Currency)
    policyPeriod = bi.Bundle.add(policyPeriod)
    BillingInstructionInfoMethods.assignOfferNumberToPolicyPeriodIfNotNull(policyPeriod, this.OfferNumber)
    bi.AssociatedPolicyPeriod = policyPeriod
    return bi
  }

  function toReinstatementForPreview() : Reinstatement {
    final var bi = createReinstatementBI()
    this.initializeBillingInstruction(bi)
    return bi
  }

  function executedReinstatementBISupplier(): Reinstatement {
    var bi = createReinstatementBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
