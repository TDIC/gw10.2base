package gw.webservice.policycenter.bc900

uses gw.payment.PaymentInstrumentFilters

uses gw.webservice.policycenter.bc900.entity.types.complex.PaymentPlanInfo

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement PaymentPlanInfoEnhancement : PaymentPlanInfo {
  function copyPaymentPlanInfo(plan : PaymentPlan){
    this.copyPlanCurrencyInfo(plan)
    this.Reporting = plan.Reporting
    this.AllowedPaymentMethods = PaymentInstrumentFilters
        .accountDetailsPaymentInstrumentFilter.map(\ paymentMethod -> paymentMethod.Code)
    this.InvoiceFrequency = plan.Periodicity.Code
    this.BillDateOrDueDateBilling = plan.PolicyLevelBillingBillDateOrDueDateBilling.Code
  }
}
