package gw.webservice.policycenter.bc1000

uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.system.BCLoggerCategory
uses gw.api.webservice.exception.FieldFormatException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.pl.persistence.core.Bundle
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PCContactInfo_ContactAddresses
uses gw.webservice.policycenter.bc1000.entity.types.complex.AddressInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.ContactAddressInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCContactInfo
uses entity.Contact
uses org.slf4j.LoggerFactory

@Export
enhancement PCContactInfoEnhancement : PCContactInfo {
  private function createContact(bundle : Bundle) : Contact {
    if (this.ContactType.toLowerCase() == "company") {
      return new Company(bundle)
    } else if (this.ContactType.toLowerCase() == "person") {
      return new Person(bundle)
    } else {
      throw new FieldFormatException(DisplayKey.get("Webservice.Error.InvalidContactType", this.ContactType))
    }
  }

  private function fill(contact : Contact) {
    if (this.AddressBookUID == null) {
      throw new RequiredFieldException(DisplayKey.get("Webservice.Error.ContactABUIDCannotBeNull", this))
    }
    if (contact typeis Company) {
      contact.Name = this.Name
      contact.NameKanji = this.NameKanji
    } else if (contact typeis Person) {
      contact.FirstName = this.FirstName
      contact.FirstNameKanji = this.FirstNameKanji
      contact.LastName = this.LastName
      contact.LastNameKanji = this.LastNameKanji
      contact.Particle = this.Particle
      contact.CellPhone = this.CellPhone
      contact.CellPhoneCountry = PhoneCountryCode.get(this.CellPhoneCountry)
      contact.CellPhoneExtension = this.CellPhoneExtension
    } else {
      throw new FieldFormatException(DisplayKey.get("Webservice.Error.InvalidContactType", this.ContactType))
    }
    contact.TaxID = this.TaxID
    contact.ExternalID = this.PublicID
    contact.AddressBookUID = this.AddressBookUID
    contact.FaxPhone = this.FaxPhone
    contact.FaxPhoneCountry = PhoneCountryCode.get(this.FaxPhoneCountry)
    contact.FaxPhoneExtension = this.FaxPhoneExtension
    contact.HomePhone = this.HomePhone
    contact.HomePhoneCountry = PhoneCountryCode.get(this.HomePhoneCountry)
    contact.HomePhoneExtension = this.HomePhoneExtension
    contact.WorkPhone = this.WorkPhone
    contact.WorkPhoneCountry = PhoneCountryCode.get(this.WorkPhoneCountry)
    contact.WorkPhoneExtension = this.WorkPhoneExtension
    contact.PrimaryPhone = PrimaryPhoneType.get(this.PrimaryPhone)
    contact.EmailAddress1 = this.EmailAddress1
    if (this.PrimaryAddress != null) {
      contact.PrimaryAddress = this.PrimaryAddress.$TypeInstance.toAddress()
    }
    for (a in this.ContactAddresses) {
      var contactAddress = a.$TypeInstance.toContactAddress()
      contact.addToContactAddresses(contactAddress)
    }
  }

  protected function findContact(bundle : Bundle) : Contact {
    if (this.AddressBookUID == null) {
        throw new RequiredFieldException(DisplayKey.get("Webservice.Error.ContactABUIDCannotBeNull", this))
    }
    final var addressBookUID = this.AddressBookUID
    var result : Contact
    var beansInBundle = bundle.getBeansByRootType(Contact)
        .where(\ cont -> (cont as Contact).AddressBookUID == addressBookUID)
    if (beansInBundle.Count > 1) {
      BCLoggerCategory.BILLING_API.info("ERROR: There are ${beansInBundle.Count} contacts with AddressBookUID of ${addressBookUID}, expect at most one")
    }
    result = beansInBundle.first() as Contact
    if (result == null) {
      final var results = Query.make(entity.Contact)
          .compare("AddressBookUID", Equals, addressBookUID)
          .select()
      if (results.Count > 1) {
        BCLoggerCategory.BILLING_API.info("ERROR: There are ${results.Count} contacts with AddressBookUID of ${this.AddressBookUID}, expect at most one")
      }
      result = results.AtMostOneRow
    }
    return result
  }

  function toContact(bundle : Bundle) : Contact {
    var contact = findContact(bundle)
    if (contact == null) {
      // print("Could not find contact with ExternalID='${PublicID}' to update")
      // race condition may case update contact to happen before create contact,
      // so in this case, we go ahead and create contact and the create contact come
      // later will be ignored.
      contact = createContact(bundle)
    } else {
      contact = bundle.add(contact)
    }
    fill(contact)
    contact = contact.syncWithContactManager()
    return contact
  }

  /**
   * US368: PC to BC - Billing Instructions
   * 17/09/2014 Vicente
   *
   * Modification to existing method to call overloaded method, passing in the PrimaryInsured role.
   * @param period PolicyPeriod where is the new contact
   * @return a Policy Period Contact with the Primary Insured role
   */
  function toPolicyPeriodContact(period: PolicyPeriod): PolicyPeriodContact {
    return toPolicyPeriodContact(period, PolicyPeriodRole.TC_PRIMARYINSURED)
  }

  /**
   * US368: PC to BC - Billing Instructions
   * 17/09/2014 Vicente
   *
   * Overloaded method to convert a PCContactInfo to a PolicyPeriod contact with the additional Role parameter.
   * @param period PolicyPeriod where is the new contact
   * @param role then role of the new contact
   * @return a Policy Period Contact with the input role
   */
  function toPolicyPeriodContact(period: PolicyPeriod, role: PolicyPeriodRole): PolicyPeriodContact {
    var periodContact = new PolicyPeriodContact(period.Bundle)
    var contact = findContact(period.Bundle)
    if (contact == null) {
      periodContact.Contact = createAndSyncContact(period.Bundle)
    } else {
      periodContact.Contact = contact
    }
    periodContact.PolicyPeriod = period
    var contactRole = new PolPeriodContactRole(period.Bundle)
    contactRole.Role = role
    periodContact.addToRoles(contactRole)
    return periodContact
  }

  function toAccountContact(account: Account, roles: AccountRole[]): AccountContact {
    var accountContact = new AccountContact(account.Bundle)
    var contact = findContact(account.Bundle)
    if (contact == null) {
      accountContact.Contact = createAndSyncContact(account.Bundle)
    } else {
      accountContact.Contact = contact
    }
    accountContact.Account = account
    for (role in roles) {
      var contactRole = new AccountContactRole(account.Bundle)
      contactRole.Role = role
      contactRole.AccountContact = accountContact
      accountContact.addToRoles(contactRole)
    }
    return accountContact
  }

  function toProducerContact(producer : Producer) : ProducerContact {
    var contact = findContact(producer.Bundle)
    if (contact == null) {
      contact = createAndSyncContact(producer.Bundle)
    }
    return createPrimaryProducerContactFor(producer.Bundle, contact)
  }

  internal static function createPrimaryProducerContactFor(bundle: Bundle, primaryContact: Contact) : ProducerContact {
    final var producerContact = new ProducerContact(bundle)
    producerContact.Contact = primaryContact
    final var contactRole = new ProducerContactRole(bundle)
    contactRole.Role = TC_PRIMARY
    producerContact.addToRoles(contactRole)
    return producerContact
  }

  function addPrimaryAddress(addressInfo: AddressInfo) {
    this.PrimaryAddress.$TypeInstance = addressInfo
  }

  function addContactAddress(contactAddressInfo : ContactAddressInfo) {
    var contactAddress = new PCContactInfo_ContactAddresses()
    contactAddress.Address = contactAddressInfo.Address
    contactAddress.AddressBookUID = contactAddressInfo.AddressBookUID
    this.ContactAddresses.add(contactAddress)
  }

  private function createAndSyncContact(bundle : Bundle) : Contact {
    var newContact = createContact(bundle)
    fill(newContact)
    newContact = newContact.syncWithContactManager()

    return newContact
  }

  /**
   * Removes a role from the policy period's primary contact, and adds another role.  Useful for switching from primary
   * insured to additional named insured, or vice versa.
   */
  @Param("existingContact","The PolicyPeriodContact entity on which to remove the primary role, if exists")
  @Param("roleToRemove","The PolicyPeriodRole to remove from the policy period contact")
  @Param("roleToAdd","The PolicyPeriodRole to add to the policy period contact")
  public static function swapRolesForPolicyContact_TDIC(existingContact : PolicyPeriodContact, roleToRemove : PolicyPeriodRole, roleToAdd : PolicyPeriodRole) {
    var primaryRole = existingContact.Roles.firstWhere( \ r -> r.Role == roleToRemove)
    if (primaryRole != null) {
      LoggerFactory.getLogger("Application.BillingAPI").debug("Removing '" + roleToRemove + "' role from contact.")
      primaryRole.remove()
    }
    if (!existingContact.Roles*.Role.contains(roleToAdd)) {
      LoggerFactory.getLogger("Application.BillingAPI").debug("Adding '" + roleToAdd + "' role to contact.")
      var contactRole = new PolPeriodContactRole(existingContact.Bundle)
      contactRole.Role = roleToAdd
      existingContact.addToRoles(contactRole)
    }
    else {
      LoggerFactory.getLogger("Application.BillingAPI").debug("Contact already has '" + roleToAdd + "' role.  No need to add.")
    }
  }

}