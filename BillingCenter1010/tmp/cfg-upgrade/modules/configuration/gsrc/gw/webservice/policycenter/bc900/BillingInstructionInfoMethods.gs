package gw.webservice.policycenter.bc900

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
class BillingInstructionInfoMethods {
  static function assignOfferNumberToPolicyPeriodIfNotNull(policyPeriod : PolicyPeriod, offerNumber : String) {
    if (offerNumber != null) policyPeriod.OfferNumber = offerNumber
  }
}