package gw.webservice.bc.bc900

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

@WsiExportable ("http://guidewire.com/bc/ws/gw/webservice/bc/bc900/DistributionItemRecord")
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
final class DistributionItemRecord {

  //PublicID of the InvoiceItem to distribute to
  private var _invoiceItemID : String as InvoiceItemID
  
  private var _grossAmount : MonetaryAmount as GrossAmount
  private var _commissionAmount : MonetaryAmount as CommissionAmount

  //Optional: Default value is Auto-Exception
  private var _disposition : DistItemDisposition as Disposition

}
