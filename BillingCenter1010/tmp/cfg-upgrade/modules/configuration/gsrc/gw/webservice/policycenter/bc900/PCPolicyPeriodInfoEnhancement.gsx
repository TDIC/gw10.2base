package gw.webservice.policycenter.bc900

uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.EntityStateException
uses gw.webservice.policycenter.bc900.entity.types.complex.PCPolicyPeriodInfo

/**
 * Defines behavior for a {@link PCPolicyPeriodInfo}. These are utility methods
 * (mostly lookup) used in implementing behavior in its subtype enhancements
 * such as {@link
 * gw.webservice.policycenter.bc900.PCPolicyPeriodInfoEnhancement#findPolicyPeriodForUpdate
 * find an existing} {@link entity.PolicyPeriod PolicyPeriod}.
 */
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement PCPolicyPeriodInfoEnhancement : PCPolicyPeriodInfo {

  /**
   *  Returns the PolicyPeriod that matches the PCPolicyPeriodInfo. A BadIdentifierException is thrown if one is not found.
   *
   *  Does a one-time update of PolicyPeriod.Policy for PolicyPeriods that have been upgraded from a version before 8.0.0.
   *  Call this method when you are planning to execute a Billing Instruction or other operation that will commit the bundle.
   *  Otherwise call findPolicyPeriod.
   *
   *  It should try to find a match using the PCPolicyPublicID first (assuming PCPolicyPublicID is not null).
   *  If a match is not found, then BC should try to find a match using the policy number.
   *
   *  If PCPolicyPublicID is not provided in the API calls,
   *  then BC should use the policy number to find a matching policy and process the API call (same behavior as in 7.0.0).
   *
   *  If a PolicyPeriod has been upgraded from a version before 8.0.0, its Policy might have null PCPublicID field. If a matching
   *  PolicyPeriod is found by the PolicyNumber, BC should update Policy.PCPublicID field with PCPolicyPeriodInfo.PCPolicyPublicID
   *
   *  @return the PolicyPeriod
   */
  function findPolicyPeriodForUpdate() : PolicyPeriod {
    var bundle = gw.transaction.Transaction.Current
    var period = findPolicyPeriod()
    period = bundle.add(period)
    updatePCPublicID(period)
    return period
  }

  /**
   * Returns the unarchived {@code PolicyPeriod} that matches the {@link
   * gw.webservice.policycenter.bc900.entity.PCPolicyPeriodInfo
   * PCPolicyPeriodInfo}. A {@link
   * gw.api.webservice.exception.BadIdentifierException BadIdentifierException}
   * is thrown if one is not found.
   *<p/>
   * It should try to find a match using the {@code PCPolicyPublicID} first
   * (assuming the value is not null). If a match is not found, then it should
   * try to find a match using the {@code PolicyNumber} ((same behavior as in
   * 7.0.0). In both cases, the {@code TermNumber} is also used to further
   * identify the {@code PolicyPeriod}
   *
   * @return the matching {@link entity.PolicyPeriod PolicyPeriod}
   * @throws gw.api.webservice.exception.EntityStateException if the
   *     {@code PolicyPeriod} is archived.
   */
  function findPolicyPeriod() : PolicyPeriod {
    var period = findByPolicyPublicIDOrPolicyNumber(this.PCPolicyPublicID, this.TermNumber, this.PolicyNumber)

    if (period == null) {
      var message = (this.PCPolicyPublicID != null)
          ? DisplayKey.get("Webservice.Error.CannotFindMatchingPolicyPeriodByPCPolicyPublicID", this.PCPolicyPublicID, this.PolicyNumber, this.TermNumber)
          : DisplayKey.get("Webservice.Error.CannotFindMatchingPolicyPeriod", this.PolicyNumber, this.TermNumber)

      throw new BadIdentifierException(message)
    }
    if (period.Archived) {
      throw new EntityStateException(
          DisplayKey.get('Webservice.Error.OperationNotPermittedOnArchivedPolicyPeriod'))
    }
    return period
  }

  /**
   *  If a PolicyPeriod has been upgraded from a version before 8.0.0, its Policy might have a null PCPublicID field. If a matching
   *  PolicyPeriod is found by the PolicyNumber, BC should update Policy.PCPublicID field with PCPolicyPeriodInfo.PCPolicyPublicID
   *
   *  If there are no PolicyPeriods which have been upgraded from a version before 8.0.0, this method is not necessary.
   */
  private function updatePCPublicID(period : PolicyPeriod) {
    var fieldIsNull = period.Policy.PCPublicID == null
    var newFieldValueExists = this.PCPolicyPublicID != null
    if (fieldIsNull and newFieldValueExists) {
      var policy = period.Bundle.add(period.Policy)
      policy.PCPublicID = this.PCPolicyPublicID
    }
  }

  /**
   * Find using either the PublicID of the Policy of PolicyCenter and the term number
   * or the PolicyNumber and the term number.
   *
   * @param pcPolicyPublicID PolicyCenter's public ID of the Policy
   * @param termNumber the term number
   * @param policyNumber the policy number
   * @return the only policy period
   */
  function findByPolicyPublicIDOrPolicyNumber(pcPolicyPublicID : String, termNumber : int, policyNumber : String) : PolicyPeriod {
    var period : PolicyPeriod
    if (this.PCPolicyPublicID != null) {
      period = findByPolicyPublicIDAndTerm(pcPolicyPublicID, termNumber)
      if (period != null) {
        return period
      }
    }
    return findByPolicyNumberAndTerm(policyNumber, termNumber)
  }

  /**
   * Find policy period by PolicyCenter's public ID of the Policy and the term number
   * @param publicIDInPC PolicyCenter's public ID of the Policy
   * @param termNumber the term number
   * @return the only policy period
   */
  function findByPolicyPublicIDAndTerm(publicIDinPC : String, termNumber : int) : PolicyPeriod {
    var policyPeriodQuery = Query.make(PolicyPeriod)
    policyPeriodQuery.compare(PolicyPeriod#TermNumber, Equals, termNumber)
    var policyTable = policyPeriodQuery.join(PolicyPeriod#Policy)
    policyTable.compare(Policy#PCPublicID, Equals, publicIDinPC)
    return policyPeriodQuery.select().FirstResult
  }

  /**
   * Find policy period by PolicyCenter's public ID of the Policy and the term number
   * @param policyNumber the policy number
   * @param termNumber the term number
   * @return the only policy period
   */
  function findByPolicyNumberAndTerm(policyNumber : String, termNumber : int) : PolicyPeriod {
    var policyPeriodQuery = Query.make(PolicyPeriod)
    policyPeriodQuery.compare(PolicyPeriod#PolicyNumber, Equals, policyNumber)
    policyPeriodQuery.compare(PolicyPeriod#TermNumber, Equals, termNumber)
    return policyPeriodQuery.select().FirstResult
  }
}