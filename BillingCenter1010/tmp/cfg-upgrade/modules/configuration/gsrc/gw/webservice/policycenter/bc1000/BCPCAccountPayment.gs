package gw.webservice.policycenter.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/BCPCAccountPayment" )
@Export
final class BCPCAccountPayment {
  var _amount: MonetaryAmount as Amount
  var _date: Date as Date
  var _moneyReceivedID : String as MoneyReceivedID
}
