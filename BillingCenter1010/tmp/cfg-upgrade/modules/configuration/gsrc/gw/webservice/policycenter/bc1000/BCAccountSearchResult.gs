package gw.webservice.policycenter.bc1000

uses gw.xml.ws.annotation.WsiExportable
uses typekey.Currency

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/BCAccountSearchResult" )
@Export
final class BCAccountSearchResult {
  public var AccountNumber : String
  public var AccountName : String
  public var AccountNameKanji : String
  public var PrimaryPayer : String
  public var Currency : Currency
  public var isListBill : boolean

  construct() {
  }
  
  construct(account : Account) {
    this.AccountName = account.AccountName
    this.AccountNameKanji = account.AccountNameKanji
    this.AccountNumber = account.AccountNumber
    this.PrimaryPayer = account.PrimaryPayer.DisplayName
    this.Currency = account.Currency
    this.isListBill = account.isListBill()
  }
}
