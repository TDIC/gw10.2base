package gw.webservice.policycenter.bc1000
uses gw.xml.ws.annotation.WsiExportable
uses gw.pl.currency.MonetaryAmount
uses tdic.bc.config.invoice.TDIC_InvoicePURHelper

uses java.math.BigDecimal

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/BCAccountSearchResult" )
@Export
final class DelinquencyInfo_TDIC {
  public var PolicyNumber : String;
  public var TermNumber : int;
  public var InvoiceNumber : String;
  public var CurrentBalance : MonetaryAmount;
  public var MinimumPaymentDue : MonetaryAmount;
  public var BarCode : String;

  public construct() {
  }

  public construct(policyPeriod : PolicyPeriod, invoice : Invoice ) {
    this.PolicyNumber      = policyPeriod.PolicyNumber;
    this.TermNumber        = policyPeriod.TermNumber;
    this.InvoiceNumber     = invoice.InvoiceNumber;
    this.CurrentBalance    = invoice.InvoiceStream.CurrentBalance_TDIC;
    this.MinimumPaymentDue = invoice.InvoiceStream.AmountDue_TDIC;
    this.BarCode           = TDIC_InvoicePURHelper.generateOCRBarCode(invoice);
  }


  public construct(policyPeriod : PolicyPeriod, invoice: Invoice, dueAmt:MonetaryAmount, balanceAmt:MonetaryAmount)  {
    this.PolicyNumber      = policyPeriod.PolicyNumber;
    this.TermNumber        = policyPeriod.TermNumber;
    this.InvoiceNumber     = invoice.InvoiceNumber;
    this.CurrentBalance    = balanceAmt
    this.MinimumPaymentDue = dueAmt
    this.BarCode           = TDIC_InvoicePURHelper.generateOCRBarCode(invoice, this.CurrentBalance, this.MinimumPaymentDue);
  }


}