package gw.webservice.policycenter.bc1000

uses gw.xml.ws.annotation.WsiExportable

uses java.math.BigDecimal

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/CommissionSubPlanChargePatternRateInfo" )
@Export
final class CommissionSubPlanChargePatternRateInfo {
  var _rate              : BigDecimal          as Rate
  var _role              : PolicyRole  as Role
  var _chargePatternCode : String              as ChargePatternCode

  construct() {}

  construct(commissionSubPlanChargePatternRate : CommissionSubPlanChargePatternRate) {
    this.Rate               = commissionSubPlanChargePatternRate.Rate
    this.Role               = commissionSubPlanChargePatternRate.Role
    this.ChargePatternCode  = commissionSubPlanChargePatternRate.ChargePattern.ChargeCode
  }


}