package gw.webservice.bc.bc1000

uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.DataConversionException
uses gw.webservice.util.WebserviceEntityLoader
uses gw.webservice.util.WebservicePreconditions

/**
 * Responsible for mapping BatchPayment related entities
 * It is meant to be as simple as possible
 * Due to separation of concerns it does not provide full validation of entities
 */
@Export
final class BatchPayments {

  public static function createFromDataTransferObject(dto: BatchPaymentEntryDTO) : BatchPaymentEntry {
    WebservicePreconditions.isNull(dto.PaymentType,"paymentType")
    requireNoMultipleTargets(dto)

    var batchPaymentEntry = createBaseEntryFromDataTransferObject(dto);

    if(dto.AccountNumber != null) batchPaymentEntry.AccountNumber = dto.AccountNumber
    if(dto.PolicyNumber != null) batchPaymentEntry.PolicyNumber = dto.PolicyNumber
    if(dto.AccountID != null) batchPaymentEntry.AccountNumber = WebserviceEntityLoader.loadAccount(dto.AccountID).AccountNumber
    if(dto.PolicyID != null) batchPaymentEntry.PolicyNumber = WebserviceEntityLoader.loadPolicyPeriod(dto.PolicyID).PolicyNumber
    if(dto.ProducerID != null) batchPaymentEntry.Producer = WebserviceEntityLoader.loadProducer(dto.ProducerID)
    if(dto.InvoiceID != null) batchPaymentEntry.Invoice = WebserviceEntityLoader.loadInvoice(dto.InvoiceID)

    return batchPaymentEntry
  }

  /**
   * This method is responsible for applying changes to batch payment based on BatchPaymentDTO object.
   * Properties that can be modified are only amount and payment entries ( entity limitation ).
   * Before batch payment entries update, this method removes any pre-existing entries
   * @param batchPayment source batch payment that will be modified
   * @param applyDTO DTO from which changes will be applied to batchPayment
   * @throws DataConversionException if amount present in DTO is negative
   */
  public static function applyDTO(batchPayment: BatchPayment, applyDTO: BatchPaymentDTO) {
    if (!applyDTO.Amount.IsPositive) {
      throw new DataConversionException(DisplayKey.get("Java.Error.AmountIsZero"))
    }
    batchPayment.Amount = applyDTO.Amount
    if (applyDTO.Payments != null) {
      if (batchPayment.Payments.HasElements) {
        batchPayment.Payments.each(\entry -> entry.remove())
      }
      applyDTO.Payments.each(\entryDTO -> batchPayment.addToPayments(createFromDataTransferObject(entryDTO)))
    }
  }

  static function toDataTransferObject(batchPaymentEntry: BatchPaymentEntry): BatchPaymentEntryDTO {
    return new BatchPaymentEntryDTO() {
      :PublicID = batchPaymentEntry.PublicID,
      :PaymentType = batchPaymentEntry.PaymentType,
      :PaymentInstrument = PaymentInstruments.toRecord(batchPaymentEntry.PaymentInstrument),
      :RefNumber = batchPaymentEntry.RefNumber,
      :Amount = batchPaymentEntry.Amount,
      :PaymentDate = batchPaymentEntry.PaymentDate,
      :AccountID = batchPaymentEntry.Account.PublicID,
      :PolicyID = batchPaymentEntry.PolicyPeriod.PublicID,
      :ProducerID = batchPaymentEntry.Producer.PublicID,
      :InvoiceID = batchPaymentEntry.Invoice.PublicID,
      :AccountNumber = batchPaymentEntry.AccountNumber,
      :PolicyNumber = batchPaymentEntry.PolicyNumber,
      :Description = batchPaymentEntry.Description
    }
  }

  public static function toDataTransferObject(batchPayment: BatchPayment): BatchPaymentDetailsDTO {
    return new BatchPaymentDetailsDTO() {
      :PublicID = batchPayment.PublicID,
      :BatchNumber = batchPayment.BatchNumber,
      :BatchStatus = batchPayment.BatchStatus,
      :Amount = batchPayment.Amount,
      :RemainingAmount = batchPayment.RemainingAmount,
      :CreationDate = batchPayment.CreateTime,
      :CreatedBy = batchPayment.CreateUser.DisplayName,
      :LastEditDate = batchPayment.UpdateTime,
      :LastEditedBy =batchPayment.UpdateUser.DisplayName,
      :PostedBy = batchPayment.PostedBy.DisplayName,
      :PostedDate = batchPayment.PostedDate,
      :ReversedBy = batchPayment.ReversedByUser.DisplayName,
      :ReversalDate = batchPayment.ReversalDate,
      :Payments = batchPayment.Payments.map(\entry -> toDataTransferObject(entry))
    }
  }

  private static function createBaseEntryFromDataTransferObject(batchPaymentEntryDTO: BatchPaymentEntryDTO): BatchPaymentEntry {
    var entry = new BatchPaymentEntry(batchPaymentEntryDTO.Amount.Currency){
      :PaymentInstrument = PaymentInstruments.toEntity(batchPaymentEntryDTO.PaymentInstrument),
      :RefNumber = batchPaymentEntryDTO.RefNumber,
      :Amount = batchPaymentEntryDTO.Amount,
      :Description = batchPaymentEntryDTO.Description
    }

    //PaymentEntry generates default PaymentDate which should be overriden only if optional parameter Payment Date is present
    if (Objects.nonNull(batchPaymentEntryDTO.PaymentDate)) {
      entry.PaymentDate = batchPaymentEntryDTO.PaymentDate
    }

    return entry
  }

  private static function requireNoMultipleTargets(dto: BatchPaymentEntryDTO) {
    if ({dto.AccountNumber, dto.PolicyNumber, dto.AccountID, dto.PolicyID, dto.ProducerID, dto.InvoiceID}
        .countWhere(\id -> id != null) > 1) {
      throw new DataConversionException(DisplayKey.get("Web.BatchPaymentEntriesLV.Error.MultiplePaymentRecipientSpecified"))
    }
  }
}
