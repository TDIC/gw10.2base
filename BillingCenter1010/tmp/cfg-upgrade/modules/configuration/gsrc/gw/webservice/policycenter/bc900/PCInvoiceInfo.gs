package gw.webservice.policycenter.bc900

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

uses java.math.BigDecimal
uses java.util.Collection
uses java.util.Date

/**
 * Defines representational information about an {@link Invoice} sent to a
 * Policy administration system such as PolicyCenter for display.
 */
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc900/PCInvoiceInfo" )
@Export
final class PCInvoiceInfo {
  var _invoiceNumber : String as InvoiceNumber
  var _status : String as Status
  var _invoiceDate : Date as InvoiceDate
  var _invoiceDueDate : Date as InvoiceDueDate
  var _amount : MonetaryAmount as Amount
  var _paid : MonetaryAmount as Paid
  var _unpaid : MonetaryAmount as Unpaid
  var _billed : MonetaryAmount as Billed
  var _pastDue : MonetaryAmount as PastDue
  var _invoiceStream : String as InvoiceStream
  var _paidStatus : String as PaidStatus

  construct() {}

  /**
   * Creates {@code PCInvoiceInfo}s for the {@link InvoiceItem}s of a {@code
   * PolicyPeriod} based on the containing {@link Invoice}s for them. The {@code
   * PCInvoiceInfo}s contain the sums of various {@code InvoiceItem} properties
   * on {@code Invoice}s where the {@code InvoiceItem}s belong to the specified
   * {@code PolicyPeriod}, which may not include all the {@code InvoiceItem}s on
   * an {@code Invoice}.
   *
   * @param policyPeriod The policy period.
   * @return An array of {@link PCInvoiceInfo}.
   */
  static internal function invoicesFor(policyPeriod : PolicyPeriod) : PCInvoiceInfo[] {
    return policyPeriod.InvoiceItems
        .partition(\ item -> item.Invoice)
        .values()
        .map(\ items -> new PCInvoiceInfo(items) /* items on same Invoice! */)
        .toTypedArray()
  }

  private construct(invoiceItems : Collection<InvoiceItem>) {
    // this assumes all InvoiceItems are on the same Invoice!
    var invoice = invoiceItems.first().Invoice
    _invoiceDate = invoice.EventDate
    _invoiceDueDate = invoice.DueDate
    final var invoiceItemsArray = invoiceItems.toTypedArray()
    _amount = invoiceItemsSum(invoiceItemsArray, \ sum, i -> sum.add(i.Amount), invoice.Currency)
    _paid = invoiceItemsSum(invoiceItemsArray, \ sum, i -> sum.add(i.PaidAmount), invoice.Currency)
    _unpaid = Amount.subtract(_paid).subtract(
        invoiceItemsSum(invoiceItemsArray, \ sum, i -> sum.add(i.GrossAmountWrittenOff), invoice.Currency)
    )
    _billed = invoice.Status == InvoiceStatus.TC_BILLED ? Unpaid : BigDecimal.ZERO.ofCurrency(Unpaid.Currency)
    _pastDue = invoice.Status == InvoiceStatus.TC_DUE ? Unpaid : BigDecimal.ZERO.ofCurrency(Unpaid.Currency)
    _invoiceStream = invoice.InvoiceStream.PCDisplayName // deliberate use of DisplayName, not Code
    PaidStatus = new gw.plugin.invoice.impl.Invoice().getPaidStatus(invoice).toString()
  }

  construct(invoice : Invoice) {
    _invoiceDate = invoice.EventDate
    _invoiceDueDate = invoice.DueDate
    _invoiceNumber = invoice.InvoiceNumber
    _status = invoice.Status.DisplayName // deliberate use of DisplayName, not Code
    _amount = invoice.Amount
    _unpaid = invoice.AmountDue
    _invoiceStream = invoice.InvoiceStream.PCDisplayName // deliberate use of DisplayName, not Code
    PaidStatus = new gw.plugin.invoice.impl.Invoice().getPaidStatus(invoice).toString()
  }

  private function invoiceItemsSum(invoiceItems : InvoiceItem[],
        sum(val : MonetaryAmount, elt2 : InvoiceItem) : MonetaryAmount,
        currency : Currency) : MonetaryAmount {
    return invoiceItems.reduce(new MonetaryAmount(BigDecimal.ZERO, currency), sum)
  }
}