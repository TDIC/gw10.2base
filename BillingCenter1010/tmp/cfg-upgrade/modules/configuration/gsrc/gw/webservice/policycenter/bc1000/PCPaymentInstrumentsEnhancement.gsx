package gw.webservice.policycenter.bc1000

uses gw.api.locale.DisplayKey
uses gw.webservice.bc.bc1000.PaymentInstrumentRecord
uses gw.webservice.bc.bc1000.PaymentInstruments

@Export
enhancement PCPaymentInstrumentsEnhancement : PaymentInstruments {
  
  public static function toPCRecord(instrument : PaymentInstrument) : PaymentInstrumentRecord {
    var record = PaymentInstruments.toRecord(instrument)
    if (instrument.PaymentMethod == TC_RESPONSIVE) {
      record.DisplayName = DisplayKey.get("PaymentInstrument.API.Responsive.PCDisplayName")      
    }
    return record
  }

}
