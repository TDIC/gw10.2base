package gw.webservice.bc.bc1000

uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.api.webservice.exception.SOAPException
uses gw.api.webservice.exception.SOAPServerException
uses gw.plugin.util.CurrentUserUtil
uses gw.transaction.Transaction
uses gw.webservice.util.WebserviceEntityLoader
uses gw.webservice.util.WebservicePreconditions
uses gw.xml.ws.annotation.WsiWebService

@WsiWebService("http://guidewire.com/bc/ws/gw/webservice/bc/bc1000/BCArchiveAPI")
@Export
class BCArchiveAPI {

  /**
   * Request retrieve of an archived policy period.
   * OOTB there is no reason passed in, and opts out of creating an activity.
   *
   * @param policyPeriodPublicID PublicID of an archived policy period
   * @param reason Reason for requesting the retrieve
   * @return PublicID of the new PolicyPeriodRetrieveRequest created
   */
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")
  @Throws(SOAPException, "If policy period with PublicID mathcing PolicyPeriodPublicID is not archived.")
  @Throws(RequiredFieldException, "If policyPeriodPublicID is null.")
  @Throws(BadIdentifierException, "If there is no policy period with PublicID matching policyPeriodPublicID.")
  function requestRetrieve(policyPeriodPublicID: String, reason: String) : String{
    var retrieveRequest : PolicyPeriodRetrieveRequest
    Transaction.runWithNewBundle(\bundle -> {
      WebservicePreconditions.notNull(policyPeriodPublicID, "policyPeriodPublicID")
      var policyPeriod = bundle.add(WebserviceEntityLoader.loadPolicyPeriod(policyPeriodPublicID))
      WebservicePreconditions.checkArgument(
          policyPeriod.Archived,
          DisplayKey.get("Webservice.Error.PolicyPeriodNotArchived", policyPeriodPublicID))

      retrieveRequest = policyPeriod.createRetrieveRequest(CurrentUserUtil.CurrentUser.User, reason, false)
    })
    return retrieveRequest.PublicID
  }

  /**
   * Set DoNotArchive bit to 'true' on the policy of a policy period
   *
   * @param policyPeriodPublicID PublicID of a policy period the policy of which needs the DoNotArchive field updated to 'true'
   */
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")
  @Throws(RequiredFieldException, "If policyPeriodPublicID is null.")
  @Throws(BadIdentifierException, "If there is no policy period with PublicID matching policyPeriodPublicID.")
  function setDoNotArchive(policyPeriodPublicID: String) {
    Transaction.runWithNewBundle(\bundle -> {
      WebservicePreconditions.notNull(policyPeriodPublicID, "policyPeriodPublicID")
      var policyPeriod = bundle.add(WebserviceEntityLoader.loadPolicyPeriod(policyPeriodPublicID))
      policyPeriod.Policy.setDoNotArchive(true)
    })

  }

  /**
   * Set DoNotArchive bit to 'false' on the policy of a policy period
   *
   * @param policyPeriodPublicID PublicID of a policy period the policy of which needs the DoNotArchive field updated to 'false'
   */
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")
  @Throws(RequiredFieldException, "If policyPeriodPublicID is null.")
  @Throws(BadIdentifierException, "If there is no policy period with PublicID matching policyPeriodPublicID.")
  function unsetDoNotArchive(policyPeriodPublicID: String) {
    Transaction.runWithNewBundle(\bundle -> {
      WebservicePreconditions.notNull(policyPeriodPublicID, "policyPeriodPublicID")
      var policyPeriod = bundle.add(WebserviceEntityLoader.loadPolicyPeriod(policyPeriodPublicID))
      policyPeriod.Policy.setDoNotArchive(false)
    })

  }

  /**
   * Check if the policy period is archived
   *
   * @param policyPeriodPublicID PublicID of a policy period to check if it is archived
   */
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")
  @Throws(RequiredFieldException, "If policyPeriodPublicID is null.")
  @Throws(BadIdentifierException, "If there is no policy period with PublicID matching policyPeriodPublicID.")
  function isArchived(policyPeriodPublicID : String) : boolean {
    WebservicePreconditions.notNull(policyPeriodPublicID, "policyPeriodPublicID")
    return WebserviceEntityLoader.loadPolicyPeriod(policyPeriodPublicID).isArchived()
  }

  /**
   * Check if the policy period is retrieved
   *
   * @param policyPeriodPublicID PublicID of a policy period to check if it is retrieved
   */
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")
  @Throws(RequiredFieldException, "If policyPeriodPublicID is null.")
  @Throws(BadIdentifierException, "If there is no policy period with PublicID matching policyPeriodPublicID.")
  function isRetrieved(policyPeriodPublicID : String) : boolean {
    WebservicePreconditions.notNull(policyPeriodPublicID, "policyPeriodPublicID")
    return WebserviceEntityLoader.loadPolicyPeriod(policyPeriodPublicID).isRetrieved()
  }

  /**
   * Check if the policy period has ever been archived or retrieved
   *
   * @param policyPeriodPublicID PublicID of a policy period to check if it is archived or retrieved
   */
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")
  @Throws(RequiredFieldException, "If policyPeriodPublicID is null.")
  @Throws(BadIdentifierException, "If there is no policy period with PublicID matching policyPeriodPublicID.")
  function hasEverBeenArchived(policyPeriodPublicID : String) : boolean {
    WebservicePreconditions.notNull(policyPeriodPublicID, "policyPeriodPublicID")
    var policyPeriod = WebserviceEntityLoader.loadPolicyPeriod(policyPeriodPublicID)
    return policyPeriod.isArchived() || policyPeriod.isRetrieved()
  }


}