package gw.webservice.policycenter.bc1000

uses entity.BillingInstruction
uses gw.api.domain.accounting.ChargeUtil
uses gw.api.domain.charge.ChargeInitializer
uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.SOAPServerException
uses gw.pl.currency.MonetaryAmount
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.ChargeInfo_ChargeCommissionRateOverrideInfos
uses gw.webservice.policycenter.bc1000.entity.enums.EarlyItemHandlingType
uses gw.webservice.policycenter.bc1000.entity.types.complex.ChargeCommissionRateOverrideInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.ChargeInfo

@Export
enhancement ChargeInfoEnhancement : ChargeInfo {
  function toCharge(billingInstruction: BillingInstruction) {
    var initializer = billingInstruction.buildCharge(new MonetaryAmount(this.Amount), ChargeUtil.getChargePatternByCode(this.ChargePatternCode))
    if (this.RecaptureUnappliedFundID != null && !this.RecaptureUnappliedFundID.Empty) {
      var recaptureUnappliedFund = gw.api.database.Query.make(UnappliedFund)
          .compare("PublicID", Equals, this.RecaptureUnappliedFundID)
          .select().getAtMostOneRow()
      if (recaptureUnappliedFund == null) {
        throw new BadIdentifierException(this.RecaptureUnappliedFundID)
      }
      initializer.RecaptureUnappliedFund = recaptureUnappliedFund
    }
    if (this.WrittenDate != null) {
      initializer.WrittenDate = this.WrittenDate.toCalendar().Time
    }
    initializer.ChargeGroup = this.ChargeGroup

    if (this.EarlyItemHandling == EarlyItemHandlingType.Billfirstitemtoday) {
      initializer.billFirstEntryToday()
    } else if (this.EarlyItemHandling == EarlyItemHandlingType.Billduefirstitemtoday) {
      initializer.billAndDueFirstEntryToday()
    } else if (this.EarlyItemHandling == EarlyItemHandlingType.Reinstatementbilldueearlyitemstoday) {
      if (!(billingInstruction typeis Reinstatement)) {
        throw new SOAPServerException(DisplayKey.get("Webservice.Error.ReinstatementEarlyItemForNonReinstatementBI",
            (typeof billingInstruction)))
      }
      initializer.reinstatementBillDueEarlyItemsToday()
    } else if (this.EarlyItemHandling != null) {
      throw new SOAPServerException(DisplayKey.get("Webservice.Error.InvalidEarlyItemHandling", this.EarlyItemHandling))
    }

    // rate overrides are valid only for policy period charges
    if (billingInstruction typeis PlcyBillingInstruction) {
      addOverrides(initializer)
    }
  }

  /**
   * Populate all fields based on the given charge. {@link ChargeInfo#RecaptureUnappliedFundID} is not populated.
   * @param charge The charge to copy from
   */
  function copyChargeInfo(charge : Charge) {
    this.Amount = charge.Amount.toString()
    this.ChargePatternCode = charge.ChargePattern.ChargeCode
    this.ChargeGroup = charge.ChargeGroup
    this.WrittenDate = charge.WrittenDate.XmlDateTime
  }

  private function addOverrides(chargeInitializer : ChargeInitializer) {
    for(entry in this.ChargeCommissionRateOverrideInfos.partition( \ info -> info.Role).entrySet()) {
      // check that there are no duplicate overrides for the same role
      if (entry.Value.size() > 1) {
        throw new SOAPServerException(DisplayKey.get("Webservice.Error.DuplicateRolesInCommissionRateOverrides", entry.Key))
      }
      // check that the role is valid
      var role = entry.Key
      if (PolicyRole.get(role) == null) {
        throw new SOAPServerException(DisplayKey.get("Webservice.Error.InvalidRoleInCommissionRateOverride", role))
      }
    }

    for (overrideInfo in this.ChargeCommissionRateOverrideInfos) {
      if (overrideInfo.Rate != null) {
        if (overrideInfo.Amount != null) {
          throw new SOAPServerException(DisplayKey.get("Webservice.Error.InvalidOverrideHasBothRateAndAmount", overrideInfo.Role))
        }
        chargeInitializer.overrideCommissionRate(PolicyRole.get(overrideInfo.Role), overrideInfo.Rate)
      }
      else if (overrideInfo.Amount != null) {
        chargeInitializer.overrideCommissionAmount(PolicyRole.get(overrideInfo.Role), new MonetaryAmount(overrideInfo.Amount))
      }
      else {
        throw new SOAPServerException(DisplayKey.get("Webservice.Error.InvalidOverrideHasNeitherRateNorAmount", overrideInfo.Role))
      }
    }
  }

  function addChargeCommissionRateOverrideInfo(chargeCommissionRateOverrideInfo : ChargeCommissionRateOverrideInfo){
    var elem = new ChargeInfo_ChargeCommissionRateOverrideInfos()
    elem.$TypeInstance = chargeCommissionRateOverrideInfo
    this.ChargeCommissionRateOverrideInfos.add(elem)
  }
}
