package gw.webservice.policycenter.bc1000

uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.transaction.ChargePatternHelper
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCContactInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.PolicyChangeInfo
uses org.slf4j.LoggerFactory

/**
 * Defines behavior for the 910 API version of the WSDL entity that specifies
 * the data used to create a {@link PolicyChange PolicyChange} billing
 * instruction.
 */
@Export
enhancement PolicyChangeInfoEnhancement : PolicyChangeInfo {
  function executePolicyChangeBI() : BillingInstruction {
    return executedChangeBISupplier()
  }

  private function createChangeBI(forPreview : boolean = false) : PolicyChange {
    var policyPeriod = forPreview
        ? this.findPolicyPeriod()
        : this.findPolicyPeriodForUpdate()
    var bi = new PolicyChange(policyPeriod.Currency)
    policyPeriod = bi.Bundle.add( policyPeriod )
    populateChangeInfo(policyPeriod)

    // OOTB has removed in v10, please check.
    /*if (this.HasScheduledFinalAudit and policyPeriod.Canceled) {
      bi.SpecialHandling = typekey.SpecialHandling.TC_HOLDFORAUDITALL
    }*/

    // if ERE Charge pattern is present in the BI then do the special handling
    if(this.ChargeInfos != null and this.ChargeInfos.HasElements and
        this.ChargeInfos.hasMatch(\elt -> elt.getChargePatternCode() == "EREPremium_TDIC")){
      bi.SpecialHandling = SpecialHandling.TC_BILLFUTUREITEMSIMMDT_TDIC

      // if the PI is ACH then set it to Responsive
      if(policyPeriod.InvoiceStream.OverridingPaymentInstrument != null){
        policyPeriod.InvoiceStream.OverridingPaymentInstrument = null
      }

      // Create TT to hold invoicing if pending disbursement is present in the system
      if(policyPeriod.Account.PendingDisbursementAmount != null){
        var troubleTicket = new TroubleTicket(bi.Bundle)
        troubleTicket.Priority = Priority.TC_URGENT
        troubleTicket.TicketType = TroubleTicketType.TC_PROCESSINGERROR
        troubleTicket.Title = DisplayKey.get("TDIC.TroubleTicket.ERE.Title", policyPeriod)
        troubleTicket.DetailedDescription = DisplayKey.get("TDIC.TroubleTicket.ERE.Description")
        troubleTicket.TargetDate = DateUtil.currentDate().addDays(1)
        troubleTicket.EscalationDate = DateUtil.currentDate().addDays(1)
        var troubleTicketHelper = new CreateTroubleTicketHelper(bi.Bundle)
        troubleTicketHelper.linkTroubleTicketWithAccount(troubleTicket, policyPeriod.Account)
        troubleTicket.Hold.setAppliedToHoldType(TC_INVOICESENDING, true)
        troubleTicket.Hold.setAppliedToHoldType(TC_PAYMENTDISTRIBUTION, true)
        var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Underwriting Admin")
        if (troubleTicket.assignGroup(groupName)) {
          var queueName = groupName.getQueue("UW Admin")
          if (queueName != null) {
            troubleTicket.setAssignedQueue(queueName)
          }
        }
        LoggerFactory.getLogger("Application.BillingAPI").info("ERE TT Assignment successful. Assigned Queue : " + troubleTicket.AssignedQueue.DisplayName)
      }
    }
    return bi
  }

  function populateChangeInfo(period : PolicyPeriod) : PolicyPeriod {
    period.RiskJurisdiction = Jurisdiction.get(this.JurisdictionCode)
    period.PolicyPerEffDate = this.PeriodStart == null ? period.PolicyPerEffDate : this.PeriodStart.toCalendar().Time
    period.PolicyPerExpirDate = this.PeriodEnd == null ? period.PolicyPerExpirDate : this.PeriodEnd.toCalendar().Time
    period.ServiceMembCivilReliefAct_TDIC = this.ServiceMembCivilReliefAct_TDIC
    period.RiskManagementDiscount_TDIC = this.RiskManagementDiscount_TDIC == null ? false : this.RiskManagementDiscount_TDIC
    period.RiskManagementExpires_TDIC = this.RiskManagementExpires_TDIC!=null?this.RiskManagementExpires_TDIC.toCalendar().Time : null
    //GWPS-2483 (To handle scenario where during NB if MultiDisc is False and during midterm
    // if it is added then on BC table the value for MultilineDisc is not getting set to True so added below mapping update the value to true)
    period.MultilineAll_TDIC = this.MultilineAll_TDIC == null ? false : this.MultilineAll_TDIC
    period.MultilinePLCP_TDIC = this.MultilinePLCP_TDIC == null ? false : this.MultilinePLCP_TDIC
    period.MultilineWCPL_TDIC = this.MultilineWCPL_TDIC == null ? false : this.MultilineWCPL_TDIC
    period.MultilineWC_TDIC = this.MultilineWC_TDIC == null ? false : this.MultilineWC_TDIC
    BillingInstructionInfoMethods.assignOfferNumberToPolicyPeriodIfNotNull(period, this.OfferNumber)
    initializeContact(period)
    return period
  }

  private function initializeContact(period : PolicyPeriod) {
    if (this.PrimaryNamedInsuredContact == null) return;

/**
     * US368: PC to BC - Billing Instructions
     * 17/09/2014 Vicente
     *
     * Contact Synchronization: synchronize new policy period contacts with any existing policy period contacts.
     * This change is required since TDIC needs to bring in and synchronize all additional named insured contacts.
     */
    // SITUATION 1:  If Policy Issuance, just create new contacts on policy as mentioned in the contact info objects.
    LoggerFactory.getLogger("Application.BillingAPI").debug("Contact Synchronization for policy: " + (period.PolicyNumber == null ? this.PolicyNumber : period.PolicyNumber))
    if (period.Contacts.IsEmpty) {
      LoggerFactory.getLogger("Application.BillingAPI").debug("Empty contact list on policy.  Populating with contacts from web service request.")
      var primaryInsured = this.PrimaryNamedInsuredContact.$TypeInstance.toPolicyPeriodContact(period)
      period.addToContacts(primaryInsured)
      LoggerFactory.getLogger("Application.BillingAPI").debug("Adding additional insured contacts to policy.")
      for (additionalNamedInsured in this.AdditionalNamedInsureds_TDIC) {
        var additionalInsured = additionalNamedInsured.$TypeInstance.toPolicyPeriodContact(period, PolicyPeriodRole.TC_ADDITIONALNAMEDINS)
        period.addToContacts(additionalInsured)
      }
    }
    // SITUATION 2:  Contact synchronization for existing policies
    else {
      LoggerFactory.getLogger("Application.BillingAPI").debug("Existing contact list on policy.  Synchronizing with contacts from web service request.")
      // 1) Synchronize Primary Insured
      // Check if PrimaryInsured on the existing policy has changed
      var existingPrimaryInsured = period.Contacts.firstWhere(\ c -> c.Roles*.Role.contains(TC_PRIMARYINSURED))
      if (this.PrimaryNamedInsuredContact.AddressBookUID != existingPrimaryInsured.Contact.AddressBookUID) {
        LoggerFactory.getLogger("Application.BillingAPI").debug("Existing primary contact does not match new primary contact.  Replacing with new primary contact.")
        // If changed, remove primary role from existing primary contact and add additional insured role
        PCContactInfo.swapRolesForPolicyContact_TDIC(existingPrimaryInsured, PolicyPeriodRole.TC_PRIMARYINSURED, PolicyPeriodRole.TC_ADDITIONALNAMEDINS)
        // Check if any additional contacts on the existing policy matches the new primary contact
        LoggerFactory.getLogger("Application.BillingAPI").debug("Checking if new primary contact exists an additional named insured on the current policy.")
        var existingPolicyContact = period.Contacts.firstWhere( \ policyContact -> policyContact.Contact.AddressBookUID == this.PrimaryNamedInsuredContact.AddressBookUID)
        if (existingPolicyContact != null) {
          // If found, remove additional insured role from existing policy contact and add primary insured role
          LoggerFactory.getLogger("Application.BillingAPI").debug("Found contact as an additional named insured.  Reusing contact as primary insured.")
          PCContactInfo.swapRolesForPolicyContact_TDIC(existingPolicyContact, PolicyPeriodRole.TC_ADDITIONALNAMEDINS, PolicyPeriodRole.TC_PRIMARYINSURED)
        }
        else {
          // Add contact with the primary role
          LoggerFactory.getLogger("Application.BillingAPI").debug("New primary contact does not exist anywhere on current policy.  Creating new primary contact.")
          var primaryInsured = this.PrimaryNamedInsuredContact.$TypeInstance.toPolicyPeriodContact(period)
          period.addToContacts(primaryInsured)
        }
      }
      // 2) Synchronize additional contacts
      // Check if there are contacts that need to be removed from this Policy Period
      LoggerFactory.getLogger("Application.BillingAPI").debug("Removing any existing contacts that are not a part of the new list of contacts.")
      for (contact in period.Contacts) {
        if (contact.Roles.hasMatch(\ role -> role.Role == typekey.PolicyPeriodRole.TC_ADDITIONALNAMEDINS)
            && !this.AdditionalNamedInsureds_TDIC.hasMatch(\ additionalNamedInsured -> additionalNamedInsured.AddressBookUID == contact.Contact.AddressBookUID)) {
          LoggerFactory.getLogger("Application.BillingAPI").debug("Found existing contact to be removed: " + contact.Contact.DisplayName)
          contact.remove()
        }
      }
      // Add any new contacts to this Policy Period
      LoggerFactory.getLogger("Application.BillingAPI").debug("Adding any new additional insured contacts that are not a part of the existing contacts.")
      for (additionalNamedInsured in this.AdditionalNamedInsureds_TDIC) {
        if (!period.Contacts.hasMatch(\ policyPeriodContact -> policyPeriodContact.Contact.AddressBookUID == additionalNamedInsured.AddressBookUID )) {
          var additionalInsured = additionalNamedInsured.$TypeInstance.toPolicyPeriodContact(period, PolicyPeriodRole.TC_ADDITIONALNAMEDINS)
          //var additionalInsured = additionalNamedInsured.$TypeInstance.toPolicyPeriodContact(period)
          period.addToContacts(additionalInsured)
        }
      }
    }
    var alreadyHasSpecifiedContact = period.Contacts
      .hasMatch(\policyPeriodContact -> policyPeriodContact.Contact.AddressBookUID == this.PrimaryNamedInsuredContact.AddressBookUID)

    if (!alreadyHasSpecifiedContact){
      period.Contacts.firstWhere(\ c -> c.Roles*.Role.contains(TC_PRIMARYINSURED))?.remove()  // period.PrimaryInsured denorm is not synced yet
      var primaryInsured = this.PrimaryNamedInsuredContact.$TypeInstance.toPolicyPeriodContact(period)
      period.addToContacts(primaryInsured)
    }
  }

  function toPolicyChangeForPreview() : PolicyChange {
    var bi = createChangeBI(true)
    this.initializeBillingInstruction(bi)
    return bi
  }

  property get TermConfirmSpecified() : boolean {
    return this.TermConfirmed != null
  }

  function executedChangeBISupplier(): PolicyChange {
    var bi = createChangeBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}