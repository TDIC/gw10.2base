package gw.plugin.messaging

uses gw.api.locale.DisplayKey
uses gw.api.database.Query
uses gw.api.system.BCLoggerCategory
uses gw.api.system.PLConfigParameters
uses gw.api.web.admin.UserUtil
uses gw.datatype.DataTypes
uses gw.plugin.contact.ContactCommunicationException
uses gw.plugin.contact.ContactSystemPlugin
uses gw.plugin.Plugins
uses org.apache.commons.lang.StringUtils

uses java.lang.Exception

@Export
class ContactMessageTransport implements MessageTransport {
  public static final var DEST_ID : int = 67
  public static final var TRANSACTION_ID_PREFIX : String = PLConfigParameters.PublicIDPrefix.Value + ":"

  override function send(message : Message, transformedPayload : String) {
    var contact = message.MessageRoot as Contact
    var updateUser = contact.UpdateUser  // get the update user before the contact modified by api call. So activity can be assigned correctly
    var plugin = Plugins.get(ContactSystemPlugin)
    try {
      switch (message.EventName) {
        case "ContactAdded":
        case "ContactChanged":
          addOrUpdateContactIfNecessary(contact, transformedPayload, getTransactionId(message))
          break
        case "ContactRemoved":
          if (not contact.IsLocalOnly) {
            plugin.removeContact(contact, transformedPayload, getTransactionId(message))
          }
          break
        default:
          BCLoggerCategory.CONTACT_MANAGER_INTEGRATION.error("Unknown Contact Message event: " + message.EventName)
      }
      message.reportAck()
    } catch (ce : ContactCommunicationException) {
      if (ce.notifyAdmin()) {
        createActivityForAdmin(contact, transformedPayload, ce)
      } else {
        createActivity(contact, transformedPayload, updateUser, ce)
      }
      if (!ce.Retryable) {
        message.reportAck()
      } else {
        message.reportError()
      }
    } catch(e : Exception) {
      message.ErrorDescription = e.LocalizedMessage
      BCLoggerCategory.CONTACT_MANAGER_INTEGRATION.debug("Exception occurred while sending message in CM", e)
      message.reportError()
    }
  }


  private function addOrUpdateContactIfNecessary(contact : Contact, transformedPayload : String, transactionID : String){
    var plugin = Plugins.get(ContactSystemPlugin)

    if (contact.ShouldSendToContactSystem and contact.IsLocalOnly){
      plugin.addContact(contact,transformedPayload, transactionID)
      BCLoggerCategory.CONTACT_MANAGER_INTEGRATION.debug("Contact '${contact}' is synced with Contact Manager")
    } else if (not contact.IsLocalOnly) {
      plugin.updateContact(contact, transformedPayload, transactionID)
    }
  }


  private function getTransactionId(message : Message) : String {
     return TRANSACTION_ID_PREFIX + message.ID
  }

  private function createActivity(contact : Contact, changes : String, updateUser : User, e : Exception) {
    createActivity(contact, changes, e, \ a -> {a.assignUserAndDefaultGroup(updateUser)})
  }

  private function createActivityForAdmin(contact : Contact, changes : String, e : Exception) {
    var user = getAdminUserForIntegrationHandling()
    if (user != null) {
      createActivity(contact, changes, e, \ a -> {a.assign( user.Organization.RootGroup, user )})
    }
  }


  private function createActivity(contact : Contact, payload : String, e : Exception,
                        assignUserCodeBlock : block(activity : Activity))  {

    var query = Query.make(AccountContact).compare("Contact", Equals, contact).select()
    if (query.Empty) {
      BCLoggerCategory.CONTACT_MANAGER_INTEGRATION.error("Could not add/update contact ${contact} to ContactManager with payload ${payload}", e)
      return
    }
    
    for (accountContact in query.iterator()){
      contact.Bundle.add(accountContact)
//    TODO: Figure out if we need this chunk of code
//    if (not contact.UpdateUser.canView(accountContact.Account)) {
//      continue
//    }      
      var activity = accountContact.newActivity()
      assignUserCodeBlock(activity)
      var message = e.Message == null ? e.Class.Name : e.Message
      var description : String
      if (contact.AddressBookUID == null) {
        activity.Subject =  DisplayKey.get("Web.ContactManager.Error.FailToAddContact.Subject", contact)
        description = DisplayKey.get("Web.ContactManager.Error.FailToAddContact.Description", message)
      } else {
        activity.Subject = DisplayKey.get("Web.ContactManager.Error.FailToUpdateContact.Subject", contact)
        description = DisplayKey.get("Web.ContactManager.Error.FailToUpdateContact.Description", message)
      }

      // Limit the length of activity descrition
      limitLengthAndSetActivityDescription(activity, description)

      // if we get this far, we've created an activity and can stop
      return
    }
  }

  /**
   * Limit the length of activity descrition to the size of a medium text field.
   *
   * @param activity - The activity in which we will set the description to
   * @param description - The description that will be added to the activity. It already contains any additional display key characters
   */
  private function limitLengthAndSetActivityDescription(activity: Activity, description: String) {
    var l = DataTypes.get("mediumtext", {}).asPersistentDataType().Length
    activity.Description = StringUtils.abbreviate(description, l)
  }

  /**
   * Return the user we would like to assign the activity to if there is a
   * unexpected exception thrown from contact manager. Extract this out
   * so it's easier for customization
   */
  private function getAdminUserForIntegrationHandling() : User {
     return UserUtil.getUserByName("admin")

  }

  override function resume() { }

  override property set DestinationID(id: int) { }

  override function shutdown() {
    BCLoggerCategory.CONTACT_MANAGER_INTEGRATION.error("Contact integration is shutdown")
  }

  override function suspend() {
    BCLoggerCategory.CONTACT_MANAGER_INTEGRATION.error("Contact integration is suspended")
  }

}
