package gw.plugin.payment.impl

uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths
uses gw.plugin.payment.ISuspensePayment

uses javax.annotation.Nonnull


@Export
class SuspensePayment implements ISuspensePayment {

  construct() {
  }

  override function apply(suspPayment: SuspensePayment) {
    if (suspPayment.AccountNumber != null) {
      var account = findAccountToApply(suspPayment.AccountNumber)
      if (account != null) {
        account = suspPayment.Bundle.add(account)
        suspPayment.applyTo(account)
      }
    } else if (suspPayment.PolicyNumber != null) {
      var policyPeriod = findPolicyPeriodToApply(suspPayment.PolicyNumber)
      if (policyPeriod != null) {
        policyPeriod = suspPayment.Bundle.add(policyPeriod)
        suspPayment.applyTo(policyPeriod)
      }
    } else if (suspPayment.ProducerName != null) {
      var producer = findProducerToApply(suspPayment.ProducerName)
      if (producer != null) {
        producer = suspPayment.Bundle.add(producer)
        suspPayment.applyTo(producer)
      }
    } else if (suspPayment.OfferNumber != null) {
      var policyPeriod = findPolicyPeriodToApplyByOfferNumber(suspPayment.OfferNumber)
      if (policyPeriod != null) {
        policyPeriod = suspPayment.Bundle.add(policyPeriod)
        suspPayment.applyTo(policyPeriod)
      }
    }
  }

  override @Nonnull function getUnappliedFundFor(suspensePayment: SuspensePayment, defaultUnappliedToBeApplied: UnappliedFund): UnappliedFund {
    return defaultUnappliedToBeApplied
  }

  override function findAccountToApply(accountNumber: String) : Account {
    // Returns the unique account or null if not found
    return Query.make(Account).compare(Account#AccountNumber, Equals, accountNumber).select().AtMostOneRow
  }

  override function findPolicyPeriodToApply(policyNumber: String) : PolicyPeriod {
    // Returns the latest PolicyPeriod or null if not found
    // Expecting a PolicyNumber (not a PolicyNumberLong)
    return findPolicyPeriodToApplyByReference(policyNumber, POLICY_NUMBER)
  }

  override function findProducerToApply(producerName: String) : Producer {
    // Returns a unique Producer. If there are multiple matches or no matches, returns null.
    var producer : Producer
    var producerMatches = Query.make(entity.Producer).compare(entity.Producer#Name, Equals, producerName).select()
    if (producerMatches.Count == 1) {
      producer = producerMatches.FirstResult
    }
    return producer
  }

  private function findPolicyPeriodToApplyByOfferNumber(offerNumber : String) : PolicyPeriod {
    return findPolicyPeriodToApplyByReference(offerNumber, OFFER_NUMBER)
  }

  enum ColumnReference {POLICY_NUMBER, OFFER_NUMBER}

  private function findPolicyPeriodToApplyByReference(referenceNumber : String, columnReference : ColumnReference) : PolicyPeriod {
    var policyPeriod : PolicyPeriod

    var policyPeriodQuery = Query.make(entity.PolicyPeriod)
        .compare(entity.PolicyPeriod#ArchiveState, Equals, null)             //Filter out currently archived policies

    if (columnReference == POLICY_NUMBER) {
      policyPeriodQuery.compare(entity.PolicyPeriod#PolicyNumber, Equals, referenceNumber)
    } else {
      policyPeriodQuery.compare(entity.PolicyPeriod#OfferNumber, Equals, referenceNumber)
    }

    var queryResult = policyPeriodQuery.select()

    var policyPeriodCount = queryResult.Count
    if (policyPeriodCount == 1) {
      policyPeriod = queryResult.FirstResult
    } else if (policyPeriodCount > 1) {
      queryResult.orderByDescending(QuerySelectColumns.path(Paths.make(PolicyPeriod#PolicyPerEffDate)))
      policyPeriod = queryResult.FirstResult
    }
    return policyPeriod
  }

}
