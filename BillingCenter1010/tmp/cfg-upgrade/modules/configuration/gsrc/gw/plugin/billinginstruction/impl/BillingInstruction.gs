package gw.plugin.billinginstruction.impl

uses gw.plugin.billinginstruction.IBillingInstruction
uses gw.transaction.ChargePatternHelper

@Export
class BillingInstruction implements IBillingInstruction {

  construct() {
  }

  public override function addAdditionalCharges(billingInstruction : BillingInstruction) {
  }

  public override function onSpecialHandlingHoldChargesForFinalAudit(charge : Charge) : void {
    charge.setHold( ChargeHoldStatus.TC_FINALAUDIT, null, false )
  }

  public override function onSpecialHandlingHoldAllUnbilledItemsForFinalAudit(policyPeriod : PolicyPeriod) : void {
    // Fix for GBC-962 - Charges are being held for policy payment reversal charges
    // SN: 04/13/2020 - Added CCFee and Recapture charge to the list
    var notToHoldCharges = {ChargePatternHelper.getChargePattern("PolicyPaymentReversalFee"),
        ChargePatternHelper.getChargePattern("CreditCardUsageFee"), ChargePatternHelper.getChargePattern("PolicyRecapture")}
    for (var charge in policyPeriod.Charges.where(\elt -> !notToHoldCharges.contains(elt.ChargePattern))) {
      charge.setHold( ChargeHoldStatus.TC_FINALAUDITUNBILLED, null, false )  
    }   
  }
  
}