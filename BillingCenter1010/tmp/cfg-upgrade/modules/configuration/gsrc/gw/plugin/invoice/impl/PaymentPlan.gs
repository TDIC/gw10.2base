package gw.plugin.invoice.impl

uses gw.api.domain.charge.ChargeInitializer
uses gw.api.domain.invoice.ChargeInstallmentChanger
uses gw.api.domain.invoice.ChargeInstallmentChanger.Entry
uses gw.plugin.invoice.IPaymentPlan
uses java.util.Date
uses gw.api.system.BCLoggerCategory
uses java.util.HashMap
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount
uses java.util.ArrayList
uses java.math.BigDecimal
uses java.math.RoundingMode
uses java.lang.UnsupportedOperationException
uses java.util.List
uses org.slf4j.LoggerFactory

@Export
class PaymentPlan implements IPaymentPlan {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("Application.Invoice");

  construct() {
  }

  override function recreatePlannedInstallmentEventDatesForPaymentPlanChange(
      charge : Charge, defaultRecreatedPlannedInstallmentEventDates : List<Date>) : List<Date> {
    _logger.debug ("PaymentPlan.recreatePlannedInstallmentEventDatesForPaymentPlanChange ("
        + charge.BillingInstruction.Subtype.DisplayName + " " + charge.Amount + ")");

    // GBC-954 - Change in Payment Plan has incorrect results - 2Pay to 4Pay
    var policyPeriod = charge.PolicyPeriod
    var paymentPlan = policyPeriod.NewPaymentPlan_TDIC

    if(paymentPlan.Periodicity == Periodicity.TC_QUARTERLY){
      var tdicRecreatedPlannedInstallmentEventDates = defaultRecreatedPlannedInstallmentEventDates.copy()
      // add the first installemnt date to the list of dates.
      var firstInstallmentDate = policyPeriod.PolicyPerEffDate.addDays(-64)
      tdicRecreatedPlannedInstallmentEventDates.add(firstInstallmentDate)
      Collections.sort(tdicRecreatedPlannedInstallmentEventDates)
      return tdicRecreatedPlannedInstallmentEventDates
    }
    return defaultRecreatedPlannedInstallmentEventDates

    /*var policyPeriod = charge.PolicyPeriod;
    var paymentPlan = policyPeriod.NewPaymentPlan_TDIC;
    if (paymentPlan == null) {
      paymentPlan = policyPeriod.PaymentPlan;
    }

    return createFullSetOfInstallmentEventDates_TDIC (policyPeriod, charge.BillingInstruction, charge.ChargeDate,
                                                      paymentPlan, defaultRecreatedPlannedInstallmentEventDates);*/
  }

  override function recreateInvoiceItemsForPaymentPlanChange(changer: ChargeInstallmentChanger) {
    _logger.debug ("PaymentPlan.recreateInvoiceItemsForPaymentPlanChange (ChargeInstallmentChanger)");
    // make no changes by default.

    /*if (_logger.TraceEnabled) {
      var entryPartition = changer.Entries.partition(\e -> e.EventDate);
      var amount : MonetaryAmount;
      for (eventDate in entryPartition.keySet().order()) {
        amount = entryPartition.get(eventDate).sum(\e -> e.Amount);
        if (amount.IsNotZero) {
          _logger.debug ("PaymentPlan::recreateInvoiceItemsForPaymentPlanChange - EventDate = " + eventDate
                            + ", Amount = " + amount);
        }
      }
    }

    // APW CODE
    var charge = changer.Charge;
    var policyPeriod = charge.PolicyPeriod;
    var paymentPlan = policyPeriod.NewPaymentPlan_TDIC;
    var apwPlan = gw.api.database.Query.make(PaymentPlan).compare(Plan#Name, Equals, "TDIC Monthly APW").select().first()
    if (paymentPlan == apwPlan) {

      _logger.info("PaymentPlan::recreateInvoiceItemsForPaymentPlanChange - Payment Plan change from "
          + charge.PaymentPlan + " to " + paymentPlan + " on PolicyPeriod " + charge.PolicyPeriod + ", "
          + charge.Amount + " " + charge);
      if (_logger.DebugEnabled) {
        for (entry in changer.Entries) {
          _logger.debug ("PaymentPlan::recreateInvoiceItemsForPaymentPlanChange - EventDate = " + entry.EventDate
              + ", OriginalAmount = " + entry.OriginalAmount + ", Amount = " + entry.Amount
              + ", InvoiceBilled = " + entry.Invoice.EventDate + ", InvoiceDue = " + entry.Invoice.DueDate);
        }
      }

      // this function is for handling all payment plans except apw which is handled by function distributeInvoiceItemsEvenly
      distributeInvoiceItemsEvenly(changer)
    }*/
  }

  override function createFullSetOfInstallmentEventDates(
      initializer: ChargeInitializer, defaultCreatedInstallmentEventDates: List <Date>): List <Date> {
    _logger.debug ("PaymentPlan.createFullSetOfInstallmentEventDates (ChargeInitializer)");
    return defaultCreatedInstallmentEventDates
    /*return createFullSetOfInstallmentEventDates_TDIC (initializer.PolicyPeriod, initializer.BillingInstruction,
                                                      initializer.ChargeDate, initializer.PolicyPeriod.PaymentPlan,
                                                      defaultCreatedInstallmentEventDates);*/
  }

  // TODO: Remove all lines of code till end.
  protected function createFullSetOfInstallmentEventDates_TDIC (policyPeriod : PolicyPeriod,
                                                                billingInstruction : BillingInstruction,
                                                                chargeDate : Date, paymentPlan : PaymentPlan,
                                                                defaultCreatedInstallmentEventDates : List<Date>) : List<Date> {
    var retval = defaultCreatedInstallmentEventDates;
    var overridePresent : boolean;

    _logger.trace ("PaymentPlan::createFullSetOfInstallmentEventDates_TDIC - Entering for "
        + billingInstruction.Subtype.DisplayName + " billing instruction on PolicyPeriod "
        + policyPeriod);
//    _logger.trace ("PaymentPlan::createFullSetOfInstallmentEventDates_TDIC - ChargeDate = " + chargeDate);
//    _logger.trace ("PaymentPlan::createFullSetOfInstallmentEventDates_TDIC - PaymentPlan = " + paymentPlan);
    _logger.trace ("PaymentPlan::createFullSetOfInstallmentEventDates_TDIC - defaultCreatedInstallmentEventDates = "
                      + defaultCreatedInstallmentEventDates.join(","));

    if (paymentPlan == PaymentPlan.QuarterlyV2PaymentPlan) {
      if (billingInstruction typeis Issuance or billingInstruction typeis Renewal) {
        overridePresent = overridesPresent_TDIC (billingInstruction.Subtype, paymentPlan,
            {typekey.BillingInstruction.TC_NEWPLCYPERIODBI,
                billingInstruction.Subtype});
        if (overridePresent == false) {
          retval = doubleFirstInstallment (chargeDate, defaultCreatedInstallmentEventDates);
        }
      } else if (billingInstruction typeis PolicyChange or billingInstruction typeis Cancellation or
          billingInstruction typeis Reinstatement) {
        var newPlcyPeriodBICharge = findNewPlcyPeriodBICharge_TDIC (policyPeriod);
        overridePresent = overridesPresent_TDIC (billingInstruction.Subtype, paymentPlan,
            {typekey.BillingInstruction.TC_NEWPLCYPERIODBI,
                newPlcyPeriodBICharge.BillingInstruction.Subtype,
                billingInstruction.Subtype});
        if (overridePresent == false) {
          // Determine invoice installment dates as if submission/renewal
          retval = getInstallmentEventDates_TDIC (paymentPlan, newPlcyPeriodBICharge);
        }
      } else if (billingInstruction typeis Audit) {
        // Do nothing
      } else {
        throw new UnsupportedOperationException ("createFullSetOfInstallmentEventDates not defined for "
            + billingInstruction.Subtype.DisplayName + " BillingInstruction");
      }
    }

    _logger.trace ("PaymentPlan::createFullSetOfInstallmentEventDates_TDIC - retval = " + retval.join(","));

    return retval;
  }

  /**
   * Double first installment, if needed.
   */
  protected function doubleFirstInstallment (chargeDate : Date, installmentDates : List<Date>) : List<Date> {
    var retval = new ArrayList<Date>();
    if (installmentDates.first() == chargeDate.trimToMidnight()) {
      // Double first installment
      retval.add (installmentDates.first());
    }
    retval.addAll (installmentDates);
    return retval;
  }

  /**
   * Check if there are any unexpected overrides.  Log warnings if so.
   */
  protected function overridesPresent_TDIC (biType : typekey.BillingInstruction, paymentPlan : PaymentPlan,
                                            overrideBITypes : List<typekey.BillingInstruction>) : boolean {
    var retval = false;

    for (overrideBIType in overrideBITypes) {
      if (paymentPlan.getOverridesFor(overrideBIType) != null) {
        retval = true;
        _logger.warn ("PaymentPlan::createFullSetOfInstallmentEventDates - " + paymentPlan.Name
            + " payment plan has an override for " + overrideBIType.DisplayName  + ".  Code for "
            + biType.DisplayName + " billing instruction will be ignored.");
      }
    }

    return retval;
  }

  /**
   * Get a charge corresponding to the new policy period billing instruction.
   */
  protected function findNewPlcyPeriodBICharge_TDIC (policyPeriod : PolicyPeriod) : Charge {
    var retval : Charge;

    for (charge in policyPeriod.Charges) {
      if (charge.BillingInstruction typeis NewPlcyPeriodBI) {
        retval = charge;
        break;
      }
    }

    return retval;
  }

  /**
   * Get the installment event dates corresponding to the payment plan and charge.
   */
  protected function getInstallmentEventDates_TDIC (paymentPlan : PaymentPlan, charge : Charge) : List<Date> {
    var retval = new ArrayList<Date>();
    var installmentDate : Date;
    var policyPeriod = charge.PolicyPeriod;

    var maxInstallments = paymentPlan.MaximumNumberOfInstallments;
    var firstInstallmentDate = getFirstInstallmentDate_TDIC (paymentPlan, charge);
    retval.add (firstInstallmentDate);

    for (i in 0 .. (maxInstallments-1)) {
      installmentDate = addPeriodicity_TDIC (firstInstallmentDate, paymentPlan.Periodicity, i);
      if (isInstallmentDateValid_TDIC (installmentDate, policyPeriod, paymentPlan)) {
        retval.add (installmentDate);
      } else {
        break;
      }
    }

    return retval;
  }

  /**
   * Get the first installment date based upon the payment plan and charge.
   */
  protected function getFirstInstallmentDate_TDIC (paymentPlan : PaymentPlan, charge : Charge) : Date {
    var retval : Date;

    switch (paymentPlan.FirstInstallmentAfter) {
      case PaymentScheduledAfter.TC_CHARGEDATE:
          retval = charge.ChargeDate.trimToMidnight();
          break;
      case PaymentScheduledAfter.TC_CHARGEEFFECTIVEDATE:
          retval = charge.EffectiveDate.trimToMidnight();
          break;
      case PaymentScheduledAfter.TC_CHARGEEFFECTIVEDATEPLUSONEPERIOD:
          retval = addPeriodicity_TDIC (charge.EffectiveDate.trimToMidnight(), paymentPlan.Periodicity, 1);
          break;
      case PaymentScheduledAfter.TC_POLICYEFFECTIVEDATE:
          retval = charge.PolicyPeriod.PolicyPerEffDate.trimToMidnight();
          break;
      case PaymentScheduledAfter.TC_POLICYEFFECTIVEDATEPLUSONEPERIOD:
          retval = addPeriodicity_TDIC (charge.PolicyPeriod.PolicyPerEffDate.trimToMidnight(), paymentPlan.Periodicity, 1);
          break;
    }

    return retval;
  }

  /**
   * Add the specified number of periodicities.
   */
  protected function addPeriodicity_TDIC (startDate : Date, periodicity : Periodicity, numPeriods : int) : Date {
    var retval : Date;

    switch (periodicity) {
      case Periodicity.TC_EVERYOTHERYEAR:
          retval = startDate.addYears (2*numPeriods);
          break;
      case Periodicity.TC_EVERYYEAR:
          retval = startDate.addYears (numPeriods);
          break;
      case Periodicity.TC_EVERYSIXMONTHS:
          retval = startDate.addMonths (6*numPeriods);
          break;
      case Periodicity.TC_EVERYFOURMONTHS:
          retval = startDate.addMonths (4*numPeriods);
          break;
      case Periodicity.TC_QUARTERLY:
          retval = startDate.addMonths (3*numPeriods);
          break;
      case Periodicity.TC_EVERYOTHERMONTH:
          retval = startDate.addMonths (2*numPeriods);
          break;
      case Periodicity.TC_MONTHLY:
          retval = startDate.addMonths (numPeriods);
          break;
      case Periodicity.TC_EVERYOTHERWEEK:
          retval = startDate.addWeeks (2*numPeriods);
          break;
      case Periodicity.TC_EVERYWEEK:
          retval = startDate.addWeeks (numPeriods);
          break;
        default:
        throw new UnsupportedOperationException ("addPeriodicity_TDIC not defined for " + periodicity + " Periodicity");
    }

    return retval;
  }

  /**
   * Check if the installment date is valid.
   */
  protected function isInstallmentDateValid_TDIC (billedDate : Date, policyPeriod : PolicyPeriod,
                                                  paymentPlan : PaymentPlan) : boolean {
    var comparisonDate : Date;

    switch (paymentPlan.InvoicingBlackoutType) {
      case InvoicingBlackoutType.TC_BILLED:
          comparisonDate = billedDate;
          break;
      case InvoicingBlackoutType.TC_DUE:
          comparisonDate = billedDate.addDays (policyPeriod.OverridingInvoiceStream.InvoicingLeadTime.DayCount);
          switch (policyPeriod.Policy.Account.BillingPlan.PaymentDueDayLogic) {
            case DayOfMonthLogic.TC_EXACT:
                // Do nothing
                break;
            case DayOfMonthLogic.TC_PREVBUSINESSDAY:
                if (DateUtil.isBusinessDay (comparisonDate, HolidayTagCode.TC_GENERAL) == false) {
                  comparisonDate = DateUtil.addBusinessDays (comparisonDate, -1, HolidayTagCode.TC_GENERAL);
                }
                break;
            case DayOfMonthLogic.TC_NEXTBUSINESSDAY:
                if (DateUtil.isBusinessDay (comparisonDate, HolidayTagCode.TC_GENERAL) == false) {
                  comparisonDate = DateUtil.addBusinessDays (comparisonDate,  1, HolidayTagCode.TC_GENERAL);
                }
                break;
            case DayOfMonthLogic.TC_FIRSTBUSINESSDAY:
                comparisonDate = comparisonDate.addDays (-(comparisonDate.DayOfMonth-1));
                if (DateUtil.isBusinessDay (comparisonDate, HolidayTagCode.TC_GENERAL) == false) {
                  comparisonDate = DateUtil.addBusinessDays (comparisonDate,  1, HolidayTagCode.TC_GENERAL);
                }
                break;
            case DayOfMonthLogic.TC_LASTBUSINESSDAY:
                comparisonDate = comparisonDate.addDays(-comparisonDate.DayOfMonth).addMonths(1);
                if (DateUtil.isBusinessDay (comparisonDate, HolidayTagCode.TC_GENERAL) == false) {
                  comparisonDate = DateUtil.addBusinessDays (comparisonDate,  -1, HolidayTagCode.TC_GENERAL);
                }
                break;
              default:
              throw new UnsupportedOperationException ("isInstallmentDateValid not defined for "
                  + policyPeriod.Policy.Account.BillingPlan.PaymentDueDayLogic
                  + " DayOfMonthLogic");
          }
          break;
        default:
        throw new UnsupportedOperationException ("isInstallmentDateValid not defined for "
            + paymentPlan.InvoicingBlackoutType + " InvoicingBlackoutType");
    }

    var cutoffDate = policyPeriod.PolicyPerExpirDate
        .addDays(-paymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout);

    // An installment on the cutoff date will not be used.
    return comparisonDate.before(cutoffDate);
  }

  //
  // APW FUNCTIONS
  //

  private function distributeInvoiceItemsEvenly(changer: ChargeInstallmentChanger) {
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Entering")
    // If policy period is created later than first invoice, distribute amount evenly across all remaining invoices.
    // This overrides the OOTB behavior of stacking all the old invoice items onto the first invoice.
    var invoiceMap = new HashMap<Invoice, List<Entry>>()
    var activeEntries = changer.Entries?.where(\entry -> entry.canSetAmount() && entry.canRemove()).orderBy( \ entry -> entry.Invoice.EventDate) //specific code to changing payment plan
    var currentDate = DateUtil.currentDate()
    var policyPeriodEffectiveDate = changer.Charge.PolicyPeriod.EffectiveDate
    var billedActiveEntries = changer.Entries?.where( \ entry -> entry.Invoice.Status == typekey.InvoiceStatus.TC_BILLED || entry.Invoice.Status == typekey.InvoiceStatus.TC_DUE).orderBy( \ entry -> entry.Invoice.EventDate)

    // First group entries together by Invoice
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Original View - Active Entry Count: " + activeEntries.Count)
    for (entry in activeEntries) {
      _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly -   Invoice Item: " + entry.Amount + " "
          + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber)
      var invoiceEntryMapping = invoiceMap.get(entry.Invoice)
      if (invoiceEntryMapping == null) {
        var entryList = new ArrayList<Entry>()
        entryList.add(entry)
        invoiceMap.put(entry.Invoice, entryList)
      }
      else {
        invoiceEntryMapping.add(entry)
      }
    }

    // If Audit Billing Instruction, do not attempt to modify any items
    if ((changer.Charge.BillingInstruction typeis Audit) or (changer.Charge.BillingInstruction typeis PolicyChange)) {
      _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Payment Plan change to APW, but for an audit charge. Returning without any modification.")
      return
    }

    //When the Payment Plan is changed from Semi-Annual to APW, sometimes the functionality OOTB returns no invoices for the first
    //months. The below code check this situation and create as many invoices as needed with 0 as amount money
    var firstActiveEntryDate = activeEntries.first().Invoice.EventDate
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - First Active Invoice Date: " + firstActiveEntryDate)
    var firstCorrectDate : Date
    //To calculate the necessary invoices the last billed/planned invoice month is subtract to the first invoice generated.
    //The result is the number of Installment that have to created
    if(billedActiveEntries.Count > 0) {
      firstCorrectDate = billedActiveEntries.last().Invoice.DueDate
    } else {
      if(DateUtil.compareIgnoreTime(currentDate, policyPeriodEffectiveDate) > 0){
        firstCorrectDate = currentDate
      } else {
        firstCorrectDate = policyPeriodEffectiveDate
      }
    }
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - First Correct Date: " + firstCorrectDate)
    var monthsLeft = DateUtil.getMonthsBetweenDates(firstCorrectDate, firstActiveEntryDate)
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Months between First Active Invoice Date and First Correct Date: " + monthsLeft)

    var zero = new MonetaryAmount(0, activeEntries.first().Amount.Currency)
    var previousDate : Date
    while(monthsLeft > 0){
      previousDate = activeEntries.first().Invoice.EventDate.addMonths(-monthsLeft)
      if (DateUtil.compareIgnoreTime(previousDate, policyPeriodEffectiveDate) > 0
          && DateUtil.compareIgnoreTime(previousDate, currentDate) > 0) {
        _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Less installments generated by default. New installment created in " + previousDate)
        //The first active entry date is take to calculate the new installment dates
        changer.createInstallment(zero, previousDate)
      }
      monthsLeft--
    }

    //When the Payment Plan is changed from Semi-Annual to APW, sometimes the functionality OOTB returns no invoices for the last
    //months. The below code check this situation and create as many invoices as needed with 0 as amount money
    var lastActiveEntryDate = activeEntries.last().Invoice.EventDate
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Last Active Invoice Date: " + lastActiveEntryDate)
    var dateTwoMonthsBeforeExpirationDate = changer.Charge.PolicyPeriod.ExpirationDate.addMonths(-2)
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Two Months Before Expiration Date: " + dateTwoMonthsBeforeExpirationDate)
    monthsLeft = DateUtil.getMonthsBetweenDates(lastActiveEntryDate, dateTwoMonthsBeforeExpirationDate)
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Months between Last Active Invoice Date and Two Months Before Expiration Date: " + monthsLeft)

    var nextDate : Date
    while(monthsLeft > 0){
      nextDate = activeEntries.last().Invoice.EventDate.addMonths(monthsLeft)
      if(DateUtil.compareIgnoreTime(dateTwoMonthsBeforeExpirationDate, nextDate) == 1){
        _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Less installments generated by default. New installment created in " + nextDate)
        //The first active entry date is take to calculate the new installment dates
        changer.createInstallment(zero, nextDate)
      }
      monthsLeft--
    }

    if (_logger.DebugEnabled) {
      _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - View after any installments created:")
      for (entry in changer.Entries.orderBy( \ entry -> entry.EventDate)) {
        _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly -   Invoice Item: " + entry.Amount + " "
            + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber)
      }
    }

    var amountToDistribute = changer.Entries.where( \ entry -> entry.addsItem()).sum(\entry -> entry.Amount)
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Total amount to distribute among entries: " + amountToDistribute)

    // For the Invoices with multiple entries, remove the additional entries.
    for (key in invoiceMap.Keys) {
      var invoiceEntryMapping = invoiceMap.get(key)
      if (invoiceEntryMapping.Count > 1) {
        while (invoiceEntryMapping.Count > 1) {
          var entryToRemove = invoiceEntryMapping.first()
          _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Removing entry - Invoice Item: "
              + entryToRemove.Amount + " " + entryToRemove.EventDate + " | Invoice: " + entryToRemove.Invoice + " "
              + entryToRemove.Invoice.InvoiceNumber)
          // Remove the entry
          entryToRemove.remove()
          invoiceEntryMapping.remove(entryToRemove)
        }
      }
    }

    if (_logger.DebugEnabled) {
      _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - View after any installments removed:")
      for (entry in changer.Entries.orderBy( \ entry -> entry.EventDate)) {
        _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly -   Invoice Item: " + entry.Amount + " "
            + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber)
      }
    }

    // Re-calculate active entries
    activeEntries = changer.Entries?.where(\entry -> entry.canSetAmount() && entry.canRemove()).orderBy( \ entry -> entry.Invoice.EventDate)

    //Entries with an event date before the current date are removed, unless they are all before the current date
    var activeEntriesBeforePolicyPeriod = changer.Entries?.where(\entry -> entry.canSetAmount() && entry.canRemove()).where( \ entry -> DateUtil.compareIgnoreTime(currentDate, entry.Invoice.EventDate) > 0)
    if(activeEntries.size() != activeEntriesBeforePolicyPeriod.size()){
      for(entry in activeEntriesBeforePolicyPeriod){
        _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Invoice Item removed because has an event date before the current date: "
            + entry.Amount + " " + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber)
        entry.remove()
      }
    }

    // Re-calculate active entries
    activeEntries = changer.Entries?.where(\entry -> entry.canSetAmount() && entry.canRemove()).orderBy( \ entry -> entry.Invoice.EventDate)

    // Distribute the amount among the rest of the entries.
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Final Entry Count: " + activeEntries.Count)
    var individualInvoiceAmount = amountToDistribute.divide(activeEntries.Count, 2, RoundingMode.DOWN)
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Amount for each invoice: " + individualInvoiceAmount)
    // Add the amount to each entry.
    activeEntries.each(\entry -> {
      entry.Amount = individualInvoiceAmount
      amountToDistribute = amountToDistribute.subtract(individualInvoiceAmount)
    })
    // Distribute the remainder starting with the first entry.  Note the subtraction method is used here since the
    // remainder() method of BigDecimal may not work properly when taking the parameter of a whole number divisor.
    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Remainder to add starting with the first invoice item: " + amountToDistribute)
    var oneCent = new BigDecimal("0.01").setScale(2, RoundingMode.HALF_UP).ofDefaultCurrency()
    for (entry in activeEntries) {
      if (amountToDistribute.IsPositive) {
        entry.Amount = entry.Amount.add(oneCent)
        amountToDistribute = amountToDistribute.subtract(oneCent)
      }
      else if (amountToDistribute.IsNegative) {
        entry.Amount = entry.Amount.subtract(oneCent)
        amountToDistribute = amountToDistribute.add(oneCent)
      }
      else {
        break
      }
    }

    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Final Entry View: ")
    activeEntries.each(\entry -> _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly -   Invoice Item: "
        + entry.Amount + " " + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber))

    _logger.debug("PaymentPlan::distributeInvoiceItemsEvenly - Exiting")
  }
}