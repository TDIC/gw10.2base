package gw.plugin.pas

uses gw.api.system.BCLoggerCategory
uses gw.plugin.Plugins
uses gw.plugin.messaging.MessageTransport
uses gw.plugin.pas.pc1000.PCPolicySystemPlugin

uses java.lang.Exception
uses java.lang.IllegalStateException

/**
 * PASMessageTransport receives policy administration system related messages and notifies the
 * policy administration system by using the IPolicySystemPlugin.
 */
@Export
class PASMessageTransport implements MessageTransport{
  public static final var DEST_ID : int = 100

  public static final var EVENT_CANCEL_NOW : String = "PAS_CancelNow"
  public static final var EVENT_RESCIND_CANCELLATION : String = "PAS_Rescind"
  public static final var EVENT_OFFER_PAID : String = "PAS_OfferPaid"
  public static final var EVENT_POLICY_PERIOD_CONFIRMED: String = "PAS_PolicyPeriodConfirmed"
  public static final var EVENT_ERE_PAYMENT_RECEIVED : String = "PAS_EREPaymentReceived"
  public static final var EVENT_ERE_CHARGE_WITHDRAWAL : String = "PAS_ERENonPayment"

  private var logger = BCLoggerCategory.PAS

  construct() {  }

  override function send( message: Message, payload: String ) : void {

    var eventName = message.EventName
    logger.info("PAS integration event: ${message.MessageRoot} - ${eventName}")
    var pasPlugin = Plugins.get(IPolicySystemPlugin)
    try{
      switch(eventName){
        case EVENT_POLICY_PERIOD_CONFIRMED:
          var policyPeriod = message.MessageRoot as PolicyPeriod
          pasPlugin.confirmPolicyPeriod(policyPeriod, getTransactionId(message))
          break
        case EVENT_CANCEL_NOW:
          var delinquencyCancel = message.MessageRoot as DelProcessCancel_TDIC;
          var pc1000Plugin = new PCPolicySystemPlugin();
          pc1000Plugin.requestCancellation(delinquencyCancel, getTransactionId(message));
          break
        case EVENT_OFFER_PAID:
          var payment = message.MessageRoot as SuspensePayment
          pasPlugin.notifyPaymentReceivedForRenewalOffer(payment, getTransactionId(message))
          break
        case EVENT_RESCIND_CANCELLATION:
          if (message.MessageRoot typeis DelProcessCancel_TDIC) {
            var delinquencyCancel = message.MessageRoot as DelProcessCancel_TDIC;
            var pc1000Plugin = new PCPolicySystemPlugin();
            pc1000Plugin.rescindCancellation(delinquencyCancel.JobNumber, getTransactionId(message));
          } else {
            // BrianS - Handle cancellations created before addition of DelProcessCancel_TDIC entity.
            var policyPeriod = message.MessageRoot as PolicyPeriod
            var delinquencyReason = policyPeriod.CancellationProcessEvent.DelinquencyProcess.Reason
            pasPlugin.rescindCancellation(policyPeriod, delinquencyReason, getTransactionId(message))
          }
          break
        case EVENT_ERE_PAYMENT_RECEIVED:
          var erePayment = message.MessageRoot as DirectBillMoneyRcvd
          var erePaymentRcvdDate = erePayment.ReceivedDate
          var policyNumber = erePayment.UnappliedFund.PolicyPeriod_TDIC.PolicyNumber
          var pc1000Plugin = new PCPolicySystemPlugin()
          pc1000Plugin.sendEREDetails(policyNumber, erePaymentRcvdDate, getTransactionId(message))
          break
        case EVENT_ERE_CHARGE_WITHDRAWAL:
          var delinquencyProcess = message.MessageRoot as PolicyDlnqProcess
          var policyNumber = delinquencyProcess.PolicyPeriod.PolicyNumber
          var pc1000Plugin = new PCPolicySystemPlugin()
          pc1000Plugin.createEREActivityForNonPayment(policyNumber, getTransactionId(message))
          break
        default:
          throw new IllegalStateException("Unexpected event sent to PAS: ${eventName}")
      }
      message.reportAck()
    } catch(e : Exception){
      logger.error("PAS Integration Error", e)
      message.ErrorDescription = e.Message
      message.reportError()
    }
  }

  private function getCancelNowDelinquencyReason(policyPeriod: PolicyPeriod) : DelinquencyReason {

    var delinquencyProcess = policyPeriod.CancellationProcessEvent.DelinquencyProcess
    // When DelinquencyPlan's Cancellation Target is "All Policies in Account",
    // Cancellation Reason is not available for the policies without delinquency process
    if(delinquencyProcess == null) {
      delinquencyProcess = policyPeriod.Account.DelinquencyProcesses.firstWhere(\process -> process != null)
    }
    return delinquencyProcess.Reason
  }

  private function getTransactionId(message : Message) : String {
    return message.Payload
  }

  override function resume() : void { }

  override property set DestinationID( id: int ) : void { }

  override function shutdown() : void { }

  override function suspend() : void { }

}
