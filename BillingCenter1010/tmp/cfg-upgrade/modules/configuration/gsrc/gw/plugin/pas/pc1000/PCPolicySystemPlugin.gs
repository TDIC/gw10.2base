package gw.plugin.pas.pc1000

uses gw.api.assignment.AutoAssignAssignee
uses gw.api.locale.DisplayKey
uses gw.api.system.BCConfigParameters
uses gw.api.system.BCLoggerCategory
uses gw.api.upgrade.Coercions
uses gw.api.util.DateUtil
uses gw.plugin.pas.IPolicySystemPlugin
uses wsi.remote.gw.webservice.pc.pc1000.cancellationapi.CancellationAPI
uses wsi.remote.gw.webservice.pc.pc1000.cancellationapi.enums.CancellationSource
uses wsi.remote.gw.webservice.pc.pc1000.cancellationapi.enums.ReasonCode
uses wsi.remote.gw.webservice.pc.pc1000.cancellationapi.faults.EntityStateException
uses wsi.remote.gw.webservice.pc.pc1000.policyrenewalapi.PolicyRenewalAPI
uses wsi.remote.gw.webservice.pc.pc1000.policyrenewalapi.faults.BadIdentifierException

/**
 * This implementation is used for integrating with Guidewire Policy Center 10.0.
 */
@Export
class PCPolicySystemPlugin implements IPolicySystemPlugin {

  private var logger = BCLoggerCategory.forCategory("PAS")

  construct() {  }

  override function confirmPolicyPeriod(policyPeriod : PolicyPeriod, transactionId : String) {
    logger.info("PCPolicySystemPlugin, policy period confirmed: ${policyPeriod.PolicyNumber} - Term: ${policyPeriod.TermNumber}")

    callPCRenewalAPI(\ api -> {
      api.confirmTerm(policyPeriod.PolicyNumber, policyPeriod.TermNumber, transactionId)
    })
  }

  override function notifyPaymentReceivedForRenewalOffer( payment : SuspensePayment,
                                                          transactionId : String) {
    logger.info("PCPolicySystemPlugin, renewal offer payment: ${payment.OfferNumber}")
    try {
      callPCRenewalAPI(\api -> {
        api.notifyPaymentReceivedForRenewalOffer(payment.OfferNumber, payment.OfferOption,
            payment.Amount, transactionId)
      })
    } catch (e : BadIdentifierException) {
      var activity = new SharedActivity(payment.Bundle)
      activity.ActivityPattern = ActivityPattern.Notification
      activity.Subject = DisplayKey.get("Integration.Error.UnknowOfferNumber", payment.OfferNumber)
      activity.Description = DisplayKey.get("Integration.Error.UnknowOfferNumberDesc")
    }
  }

  static private final var CancellationSourceMap = {
      DelinquencyReason.TC_NOTTAKEN -> CancellationSource.Insured,
      DelinquencyReason.TC_PASTDUE -> CancellationSource.Carrier}

  static private final var ReasonCodeMap = {
      DelinquencyReason.TC_NOTTAKEN -> ReasonCode.Nottaken,
      DelinquencyReason.TC_PASTDUE -> ReasonCode.Nonpayment}

  override function rescindCancellation(policyPeriod : PolicyPeriod,
                                        reason : DelinquencyReason, transactionId : String) {
    var delinquencyProcess = policyPeriod.DelinquencyProcesses.sortByDescending(\dp -> dp.StartDate).first();
    var startDate = delinquencyProcess.StartDate;

    // 20151015 TJ Talluto - this covers a problem when the original message didn't get thru (reason is null on the second try)
    if (reason == null){
      reason = delinquencyProcess.Reason
      logger.info("PCPolicySystemPlugin: Reason Null encountered. Retrieve from DelinquencyProcess: , ${startDate}, ${reason}")
    }

    // BrianS - If reason is Not Taken, then cancellation is backdated to the start of the term.
    if (reason == DelinquencyReason.TC_NOTTAKEN) {
      startDate = policyPeriod.PolicyPerEffDate;
    }
    logger.info("PCPolicySystemPlugin, rescindCancellation: ${policyPeriod.PolicyNumberLong}, ${reason}")
    rescindPCCancellation(policyPeriod.PolicyNumber, startDate, CancellationSourceMap.get(reason),
        ReasonCodeMap.get(reason), transactionId)
  }

  // may have to remove if not in use.
  /**
   * Rescind cancellation by Job Number.
   */
  public function rescindCancellation (jobNumber : String, transactionId : String) : void {
    logger.info("PCPolicySystemPlugin, rescindCancellationByJobNumber: " + jobNumber);
    callPCCancellationAPI (\api -> {
      api.rescindCancellationByJobNumber(jobNumber);
    })
  }

  public function sendEREDetails (policyNumber : String, ereReceivedDate : Date, transactionId : String) : void {
    logger.info("PCPolicySystemPlugin, sendEREDetails: " + ereReceivedDate);
    callPCCancellationAPI (\api -> {
      api.sendEREPaymentDate_TDIC(policyNumber,ereReceivedDate, transactionId);
    })
  }

  public function createEREActivityForNonPayment (policyNumber : String, transactionId : String) : void {
    logger.info("PCPolicySystemPlugin, createEREActivityForNonPayment for PolicyPeriod: " + policyNumber)
    callPCCancellationAPI (\api -> {
      api.createEREDelinquentActivity_TDIC(policyNumber, transactionId)
    })
  }

  override function requestCancellation( policyPeriod : PolicyPeriod,
                                         reason : DelinquencyReason, transactionId : String ) {
    logger.info("PCPolicySystemPlugin, inside override requestCancellation: ${policyPeriod.PolicyNumberLong}, ${reason}" + "..Need Intervention further.")
    throw "Method has been replaced with requestCancellation(delinquencyCancel : DelProcessCancel_TDIC, transactionId : String)";
  }

  function requestCancellation (delinquencyCancel : DelProcessCancel_TDIC, transactionId : String) {
    var policyPeriod = delinquencyCancel.PolicyPeriod;
    var delinquencyProcess = delinquencyCancel.DelinquencyProcess;
    var reason = delinquencyProcess.Reason;
    logger.info("PCPolicySystemPlugin, requestCancellation: ${policyPeriod.PolicyNumberLong}, ${reason}")
    var pcReasonCode : ReasonCode
    var source : CancellationSource
    var cancellationDate : Date

    var effectiveTime = Coercions.makeDateFrom(BCConfigParameters.PASEffectiveTime.Value)
    var effectiveTimeMidnight = effectiveTime.trimToMidnight()
    var timeAfterMidnight = effectiveTime.Time - effectiveTimeMidnight.Time
    pcReasonCode = ReasonCodeMap.get(reason)
    source = CancellationSourceMap.get(reason)
    var paidThroughDate = policyPeriod.PaidThroughDate
    switch(reason) {
      case DelinquencyReason.TC_NOTTAKEN:
        cancellationDate = delinquencyProcess.calcCancelEffectiveDate_TDIC()
        break
      case DelinquencyReason.TC_PASTDUE:
        // if multiple term invoiceitems are associated with the delinquencyprocess
        // then send the cancellation effective date as today.
        if(delinquencyProcess.AssociatedPolicyPeriodsCount > 1){
          logger.info("PCPolicySystemPlugin, requestCancellation: Multiterm policyperiods existence : ${delinquencyProcess.AssociatedPolicyPeriodsCount}")
          cancellationDate = DateUtil.currentDate()
          paidThroughDate = policyPeriod.EffectiveDate
        } else {
          cancellationDate = delinquencyProcess.calcCancelEffectiveDate_TDIC()
        }
        break
      default:
        throw new IllegalStateException("Not supported delinquency reason ${reason}")
    }

    if (cancellationDate.after(policyPeriod.ExpirationDate)) {
      var activity = policyPeriod.createActivity(ActivityPattern.Notification,
          DisplayKey.get("Integration.Error.TooLateToCancelSubj", policyPeriod),
          DisplayKey.get("Integration.Error.TooLateToCancelDesc", policyPeriod, cancellationDate),
          null, null, true)
      AutoAssignAssignee.INSTANCE.assignToThis(activity)
    } else {
      try {
        beginPCCancellation(delinquencyCancel, policyPeriod.PolicyNumber, cancellationDate, paidThroughDate,
            pcReasonCode, source, transactionId, delinquencyCancel.DelinquencyProcess.PublicID)
      } catch(e : EntityStateException) {
        var activity = policyPeriod.createActivity(ActivityPattern.Notification,
            DisplayKey.get("Integration.Error.CancelFailSubj", policyPeriod),
            DisplayKey.get("Integration.Error.CancelFailDesc", e.LocalizedMessage),
            null, null, true)
        AutoAssignAssignee.INSTANCE.assignToThis(activity)
        e.printStackTrace()
      }
    }
  }

  protected function rescindPCCancellation(policyNumber : String, delinquencyStartDate : Date,
                                           source : CancellationSource, reasonCode : ReasonCode, transactionId : String) {
    callPCCancellationAPI(\ api -> {
      api.rescindCancellation(policyNumber,
          delinquencyStartDate,
          source,
          reasonCode,
          transactionId)
    })
  }

  protected function beginPCCancellation(delinquencyCancel : DelProcessCancel_TDIC, policyNumber : String,
                                         cancellationDate : Date, paidThroughDate : Date, reasonCode : ReasonCode,
                                         source : CancellationSource, transactionId : String, delinquencyProcessPublicID : String){
    logger.info ("PCPolicySystemPlugin, beginPCCancellation: PolicyNumber = " + policyNumber
        + ", CancellationDate = " + cancellationDate + ", PaidThroughDate = " + paidThroughDate
        + ", ReasonCode = " + reasonCode + ", Source = " + source)
    callPCCancellationAPI(\ api -> {
      var jobNumber = api.beginCancellation_TDIC(policyNumber, cancellationDate, paidThroughDate,
          true, // let PC recalculate the earliest date to cancel from source and reason code
          source, reasonCode, null, // refund calc method
          "", // description
          transactionId, delinquencyProcessPublicID)
      logger.info ("PCPolicySystemPlugin, beginPCCancellation: Cancellation job number = " + jobNumber);
      delinquencyCancel.JobNumber = jobNumber;
    })
  }

  protected function callPCCancellationAPI(call : block(p : CancellationAPI)) {
    var api = new CancellationAPI()
    try {
      call(api)
    } catch(e : wsi.remote.gw.webservice.pc.pc1000.cancellationapi.faults.AlreadyExecutedException) {
      // ignore this duplicated call
    }
  }

  protected function callPCRenewalAPI(call : block(p : PolicyRenewalAPI)) {

    var api = new PolicyRenewalAPI()
    try {
      call(api)
    } catch(e : wsi.remote.gw.webservice.pc.pc1000.policyrenewalapi.faults.AlreadyExecutedException) {
      // ignore this duplicated call
    }
  }
}