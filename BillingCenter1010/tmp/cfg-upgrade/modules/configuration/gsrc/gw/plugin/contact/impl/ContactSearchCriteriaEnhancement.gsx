package gw.plugin.contact.impl

uses gw.plugin.contact.ContactResult
uses java.lang.Exception
uses java.util.ArrayList
uses java.util.HashSet
uses gw.api.database.Query
uses gw.api.database.IQueryBeanResult
uses gw.api.locale.DisplayKey

@Export
enhancement ContactSearchCriteriaEnhancement : entity.ContactSearchCriteria {

 function performSearch() : ContactResultWrapper {
    var uids = new HashSet()
    var warningMsg = ""
    var searchResults = new ArrayList<ContactResult>()
    var maxNumberResults = gw.api.system.BCConfigParameters.MaxContactSearchResults.Value
    var contactConfigPlugin = new ContactConfigPlugin()

    //US122, 01/13/2015 Vicente, At least one of the Searchable Fields has to be entered
    //If Company --> Company Name
    //If Person --> (First or Last) Name, ADA#
    if(not User.util.CurrentUser.UnrestrictedUser
       and not contactConfigPlugin.minimumCriteriaSet(this)){
      if(this.ContactSubtype == typekey.Contact.TC_PERSON){
        throw new gw.api.util.DisplayableException(DisplayKey.get("TDIC.Search.ContactOrADARequired.Error"))
      } else {
        throw new gw.api.util.DisplayableException(DisplayKey.get("TDIC.Search.CompanyNameRequired.Error"))
      }

    //US122, 01/05/2015 Vicente, New searchable field ADA #. Other fields are ignored when the ADA number is provided.
    //If the ADA Number is not provided the OOTB contact search is executed
    } else if(this.ContactSubtype == typekey.Contact.TC_PERSON and this.ADANumber_TDIC != null){
      if(otherThanADAPopulated_TDIC()){
        warningMsg = DisplayKey.get("TDIC.Search.OtherFieldIgnoredWhenADA.Warning")
      }

      var internalResults = searchContactsByADA_TDIC()

      if(internalResults.Count == 0){
        warningMsg += DisplayKey.get("Java.Search.NoResults")
      } else if(internalResults.Count > maxNumberResults){
        warningMsg += DisplayKey.get("Java.Search.TooManyResults", maxNumberResults)
      } else {
        addResults_TDIC(internalResults, searchResults, uids)
      }
    } else {
      var internalResults  = this.searchInternalContacts()

      addResults_TDIC(internalResults, searchResults, uids)

      try {
        var remoteResults = this.searchExternalContacts()
        for(result in remoteResults){
          if(not uids.contains(result.ContactAddressBookUID)){
            searchResults.add(result)
          }
        }
      } catch(e : Exception) {
        //US122, 01/05/2015 Vicente, This warning should not display after a successful search
        if(searchResults.Count == 0){
          warningMsg = e.Message
        }

      }
    }
    return new ContactResultWrapper(searchResults.toTypedArray(), warningMsg)
  }
  
  /*Read: 
    This temp function is added only to:
    1. in-line with PC since they are not implementing external ProducerContact for 7.x version now.
    2. for easy removal, instead of modifying the original method.
    3. Avoid possible performance issue.
    if you are implementing external ProducerContact from CM. Simply take this function out.
  */
  function performProducerContactInternalSearch() : ContactResultWrapper {
    var warningMsg : String
    var searchResults = new ArrayList<ContactResult>()
    
    var internalResults = this.searchInternalContacts()
    var resultsIterator = internalResults.iterator()
    while (resultsIterator.hasNext()){
      var contact = resultsIterator.next()
      searchResults.add(new ContactResultInternal(contact))
    }
    return new ContactResultWrapper(searchResults.toTypedArray(), warningMsg)
  }

/**
 * US122
 * 01/09/2015 Vicente
 *
 * Returns the OfficialIDs with the input ADA Number if exists of null
 */
    @Param("adaNumber","Input ADA Number")
    @Returns("Returns the OfficialID with the input ADA Number if exists of null")
    private function addResults_TDIC(internalResults: IQueryBeanResult<Contact>, searchResults: ArrayList<ContactResult>, uids: HashSet) {
      var resultsIterator = internalResults.iterator()
      while (resultsIterator.hasNext()){
        var contact = resultsIterator.next()
        searchResults.add(new ContactResultInternal(contact))
        if(contact.AddressBookUID != null){
          uids.add(contact.AddressBookUID)
        }
      }
  }

  /**
   * US122
   * 01/09/2015 Vicente
   *
   * Returns the OfficialIDs with the input ADA Number if exists of null
   */
  @Param("adaNumber","Input ADA Number")
  @Returns("Returns the OfficialID with the input ADA Number if exists of null")
  private function searchContactsByADA_TDIC() : IQueryBeanResult<Contact> {
    var contactQuery = Query.make(Contact)

    var officialIDQuery = gw.api.database.Query.make(OfficialID)
    officialIDQuery.compare(OfficialID#OfficialIDType, Equals, typekey.OfficialIDType.TC_ADANUMBER_TDIC)
    officialIDQuery.compare(OfficialID#OfficialIDValue, Equals, this.ADANumber_TDIC)

    contactQuery.subselect(Contact#ID, CompareIn, officialIDQuery, OfficialID#Contact)

    return contactQuery.select()
  }

  /**
   * US122
   * 01/13/2015 Vicente
   *
   * Returns true if other field than ADA Number populated
   */
  @Returns("Returns true if other field than ADA Number populated")
  private function otherThanADAPopulated_TDIC() : boolean {
    return this.FirstName.HasContent or this.Keyword.HasContent or this.TaxID.HasContent
        or this.Address.City.HasContent or this.Address.State != null
        or this.Address.Country != null or this.Address.PostalCode.HasContent
  }
}
