package gw.plugin.contact.impl
uses gw.plugin.contact.ContactResult

/**
 * Parent class of all OOTB implementations of the ContactResult interface.  Provides
 * default implementations of the overwriteContactFields method and PrimaryPhoneValue
 * property.  Sub-classes (including customer implementations) can choose to replace
 * or extend these methods if necessary.
 */
@Export
abstract class AbstractContactResult implements ContactResult {
  
  override property get PrimaryPhoneValue() : String {
    if (PrimaryPhoneType == typekey.PrimaryPhoneType.TC_HOME) {
      return this.HomePhoneValue
    } else if (PrimaryPhoneType == typekey.PrimaryPhoneType.TC_WORK) {
      return this.WorkPhoneValue
    } else if (PrimaryPhoneType == typekey.PrimaryPhoneType.TC_MOBILE) {
      return this.CellPhoneValue
    }
    return null
  }

}
