package gw.plugin.invoice.impl

uses gw.plugin.invoice.IPeriodicities
uses java.lang.Integer

@Export
class Periodicities implements IPeriodicities {

  static final var _monthlyPeriodicities = {
      Periodicity.TC_MONTHLY
  }

  // Monthly is not included as a "multiple of monthly"
  static final var _multiplesOfMonthly = {

      // each Periodicity is mapped to -> months-per-period
      Periodicity.TC_EVERYOTHERMONTH -> 2,
      Periodicity.TC_QUARTERLY ->       3,
      Periodicity.TC_EVERYFOURMONTHS -> 4,
      Periodicity.TC_EVERYSIXMONTHS ->  6,
      Periodicity.TC_EVERYYEAR ->      12,
      Periodicity.TC_EVERYOTHERYEAR -> 24

  }

  override function isMonthly(periodicity: Periodicity): boolean {
    return _monthlyPeriodicities.contains(periodicity)
  }

  override function isMultipleOfMonthly(periodicity: Periodicity): boolean {
    return _multiplesOfMonthly.containsKey(periodicity)
  }

  override function monthsPerPeriod(periodicity: Periodicity): Integer {
    return _multiplesOfMonthly.get(periodicity)
  }
}