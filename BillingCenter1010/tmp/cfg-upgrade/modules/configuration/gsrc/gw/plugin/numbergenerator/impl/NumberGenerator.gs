package gw.plugin.numbergenerator.impl;

uses gw.api.util.BCUtils;
uses gw.plugin.numbergenerator.INumberGenerator

@Export
class NumberGenerator implements INumberGenerator {

  private final var DEFAULT_LENGTH = 10

  construct() {
  }

  public override function generateAccountNumber(account : Account) : String {
    if (account.AccountNumber == null) {
      return BCUtils.generateRandomID(account, DEFAULT_LENGTH)
    } else {
      return account.AccountNumber
    }
  }

  public override function generateDisbursementNumber(disbursement : Disbursement) : String {
    return BCUtils.generateRandomID(disbursement, DEFAULT_LENGTH)
  }

  public override function generateInvoiceNumber(invoice : Invoice) : String {
    return BCUtils.generateRandomID(invoice, DEFAULT_LENGTH)
  }

  public override function generateStatementNumber(statement : ProducerStatement) : String {
    return BCUtils.generateRandomID(statement, DEFAULT_LENGTH)
  }

  public override function generateTransactionNumber(transaction : Transaction) : String {
    return BCUtils.generateRandomID(transaction, DEFAULT_LENGTH)
  }

  public override function generateTroubleTicketNumber(troubleTicket : TroubleTicket) : String {
    return BCUtils.generateRandomID(troubleTicket, DEFAULT_LENGTH)
  }

  public override function generateCollateralRequirementNumber(requirement : CollateralRequirement) : String {
    return BCUtils.generateRandomID(requirement, DEFAULT_LENGTH)
  }

  public override function generateBatchPaymentNumber(batchPayment : BatchPayment) : String {
    return BCUtils.generateRandomID(batchPayment, DEFAULT_LENGTH)
  }
}