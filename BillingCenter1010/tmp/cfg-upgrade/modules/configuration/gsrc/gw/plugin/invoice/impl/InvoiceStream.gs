package gw.plugin.invoice.impl

uses gw.api.domain.invoice.AnchorDate
uses gw.api.domain.invoice.ChargeDateContainer
uses gw.api.domain.invoice.InvoicePayer
uses gw.plugin.invoice.IInvoiceStream

uses java.util.ArrayList


@Export
class InvoiceStream implements IInvoiceStream {

  construct() {
  }

  override function getInvoiceStreamPeriodicityFor( payer : InvoicePayer, paymentPlan : PaymentPlan,
      defaultInvoiceStreamPeriodicity : Periodicity ) : Periodicity {
    /* Customer Note -- Allowing a Producer to have a non-monthly invoice stream requires the Customer
       to configure anchor dates for that periodicity in getAnchorDatesForCustomPeriodicity() in this plugin */
    return payer.Producer ? Periodicity.TC_MONTHLY : defaultInvoiceStreamPeriodicity
  }

  override function getExistingInvoiceStreamFor( payer : InvoicePayer, owner : TAccountOwner,
      invoiceStreamPeriodicity : Periodicity, defaultExistingInvoiceStream : entity.InvoiceStream ) : entity.InvoiceStream {
    return defaultExistingInvoiceStream
  }

  override function customizeNewInvoiceStream( payer : InvoicePayer, owner : TAccountOwner, newInvoiceStream : entity.InvoiceStream ) {
  }

  override function customizeNewInvoiceStream( payer : InvoicePayer, newInvoiceStream : entity.InvoiceStream ) {
  }

  override function getAnchorDatesForCustomPeriodicity( invoicePayer : InvoicePayer, customPeriodicity : Periodicity )
      : List<AnchorDate> {
    return new ArrayList<AnchorDate>();
  }

  override function getAnchorDatesForPolicyLevelStream(
      payer: InvoicePayer, owner: TAccountOwner, chargeDateContainer: ChargeDateContainer,
      paymentPlan: PaymentPlan,
      defaultAnchorDates: List<AnchorDate>) : List<AnchorDate>{
    return defaultAnchorDates
  }

}
