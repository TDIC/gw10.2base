package gw.plugin.archive.impl

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.archiving.DistArchiveReferenceFactory
uses gw.archiving.InvoiceArchiveReferenceFactory
uses gw.plugin.archiving.ArchiveSource

uses java.lang.Override

/**
 * BC implementation of the archive source plugin
 */
@Export
class BCArchiveSourcePlugin extends ArchiveSource {

  @Override
  override function updateInfoOnStore(info : RootInfo) {
    var policyPeriod = info as PolicyPeriod
    // Update PolicyPeriod Here

    /*
    Create archive references on dists and invoices that are being frozen.
    Comment out this line if you do not want either.
    */
    createArchiveReferences(policyPeriod)

    /*
    An example of storing additional information on the PolicyPeriodArchiveSummary
     */
    cacheOpenActivityCount(policyPeriod)
  }

  private function createArchiveReferences(policyPeriod : PolicyPeriod) {
    /*
    The below creates archive references on an invoice freezing now during the archiving
    of this policy period. Comment out this line if you do not want these.
    */
    new InvoiceArchiveReferenceFactory().createArchiveReferences(policyPeriod)

    /*
    The below creates archive references on a dist freezing now during the archiving
    of this policy period. Comment out this line if you do not want these.
    */
    new DistArchiveReferenceFactory().createArchiveReferences(policyPeriod)
  }

  private function cacheOpenActivityCount(policyPeriod: PolicyPeriod) {
    var activityQuery = Query.make(Activity.Type)
    activityQuery.compare(Activity#PolicyPeriod, Relop.Equals, policyPeriod)
    activityQuery.compare(Activity#Status, Relop.Equals, ActivityStatus.TC_OPEN)

    // explicit type declaration req'd b/c studio (IDE-2979)
    var policyPeriodArchiveSummary : PolicyPeriodArchiveSummary = policyPeriod.PolicyPeriodArchiveSummary
    policyPeriodArchiveSummary.OpenActivitiesCount = activityQuery.select().Count
  }
}
