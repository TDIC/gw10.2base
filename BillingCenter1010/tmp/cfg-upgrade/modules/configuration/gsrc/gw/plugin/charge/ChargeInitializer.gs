package gw.plugin.charge

uses java.util.HashMap
uses gw.api.domain.charge.ChargeInitializer.Entry
uses java.util.ArrayList
uses java.math.BigDecimal
uses java.math.RoundingMode
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount
uses java.util.Date
uses java.util.List
uses org.slf4j.LoggerFactory


/**
 * US72, US73, US74, US91, US138, US139, US140 - Installment Scheduling
 * 09/05/2014 Alvin Lee
 *
 * Implementation of the ChargeInitializer plugin for installment scheduling.
 */

@Export
public class ChargeInitializer implements IChargeInitializer {
  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("Application.Invoice")

//  protected static var CHANGE_PREVIEW_DESCRIPTION : String = "*** ChargeInitializer.getPolicyChangeAllocation ***";

  public override function customizeChargeInitializer(initializer : gw.api.domain.charge.ChargeInitializer) {
    var billingInstruction = initializer.BillingInstruction;
    var message : String;
/*
    // Do not do any customization if this is the change initiated to calculate ratios for policy changes.
    if (billingInstruction.Description == CHANGE_PREVIEW_DESCRIPTION) {
      return;
    }
*/
    _logger.debug ("ChargeInitializer::customizeChargeInitializer - Entering for "
                      + billingInstruction.Subtype.DisplayName + " billing instruction on Policy Period "
                      + initializer.PolicyPeriod + " and " + initializer.ChargePattern + " charge.");

    _logger.trace ("ChargeInitializer::customizeChargeInitializer - BillingPlan = " + initializer.PolicyPeriod.Account.BillingPlan);
    _logger.trace ("ChargeInitializer::customizeChargeInitializer - PaymentPlan = "
                      + initializer.PolicyPeriod.PaymentPlan + " (id = " + initializer.PolicyPeriod.PaymentPlan.ID
                      + ")");

    if (_logger.DebugEnabled) {
      message = "ChargeInitializer::customizeChargeInitializer - Invoices (before adjustments):";
      for (invoice in initializer.InvoiceStream.Invoices.orderBy(\i -> i.Date)) {
        message += "\nInvoice " + invoice.InvoiceNumber
                          + " - EventDate = " + invoice.EventDate + ", PaymentDueDate " + invoice.PaymentDueDate
                          + ", Amount = " + invoice.Amount_amt + ", TotalPaid = " + invoice.TotalPaid
                          + ", AmountDue = " + invoice.AmountDue_amt;
      }
      _logger.debug (message);

      message = "ChargeInitializer::customizeChargeInitializer - Entries (before adjustments):";
      for (entry in initializer.Entries) {
        message += "\nEntry: " + entry.Amount
            + " Event Date = " + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber;
      }
      _logger.debug (message);
    }
/*
    adjustInvoicesAndEntries (initializer);

    if (_logger.DebugEnabled) {
      message = "ChargeInitializer::customizeChargeInitializer - Invoices (after adjustments):";
      for (invoice in initializer.InvoiceStream.Invoices.orderBy(\i -> i.Date)) {
        message += "\nInvoice " + invoice.InvoiceNumber
            + " - EventDate = " + invoice.EventDate + ", PaymentDueDate " + invoice.PaymentDueDate
            + ", Amount = " + invoice.Amount_amt + ", TotalPaid = " + invoice.TotalPaid
            + ", AmountDue = " + invoice.AmountDue_amt;
      }
      _logger.debug (message);

      message = "ChargeInitializer::customizeChargeInitializer - Entries (after adjustments):";
      for (entry in initializer.Entries) {
        message += "\nEntry: " + entry.Amount
            + " Event Date = " + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber;
      }
      _logger.debug (message);
    }
*/
    var apwPlan = gw.api.database.Query.make(PaymentPlan).compare(Plan#Name, Equals, "TDIC Monthly APW").select().first()
    if (initializer.PolicyPeriod.PaymentPlan == apwPlan) {
      //TODO: uncomment when pc is up. Disabled HPExtreame event.
      //distributeInvoiceItemsEvenly(initializer)
    } // end of not apwPlan
    _logger.debug("ChargeInitializer::customizeChargeInitializer - Exiting -------------")

    // bill immediately the downpayment
    /*if((initializer.BillingInstruction typeis Issuance
        or initializer.BillingInstruction typeis Renewal
        or initializer.BillingInstruction typeis Rewrite
        or initializer.BillingInstruction typeis NewRenewal)
        and initializer.PolicyPeriod.PaymentPlan.HasDownPayment) {
      var depositEntries = initializer.Entries.where(\elt -> elt.InvoiceItemType == InvoiceItemType.TC_DEPOSIT)

      var newInvoice : Invoice
      var dueDate = gw.api.util.DateUtil.currentDate().trimToMidnight().addDays(initializer.InvoiceStream.InvoicingLeadTime.DayCount)
      newInvoice = initializer.InvoiceStream.getPlannedExistingInvoiceWithDates_Ext(DateUtil.currentDate(), dueDate)
      if(newInvoice == null){
        newInvoice = initializer.InvoiceStream.createAndAddInvoice(DateUtil.currentDate())
        newInvoice.setPaymentDueDate(dueDate)
      }
      depositEntries.each(\elt -> {
        elt.Invoice = newInvoice
      })
    }*/

    /**
     * roll up future planned invoices for ERE Charge, flat cancel and Audit credit presence and bill immediately.
     */
    if((initializer.BillingInstruction typeis PolicyChange or initializer.BillingInstruction typeis Cancellation or initializer.BillingInstruction typeis Audit)
        and initializer.BillingInstruction.SpecialHandling == SpecialHandling.TC_BILLFUTUREITEMSIMMDT_TDIC) {
      _logger.debug("Configuring special handling for Billing instruction : ${initializer.BillingInstruction.Subtype} on Policy : ${initializer.PolicyPeriod?.PolicyNumber} to roll up future planned invoices onto the next invoice.")
      handleFuturePlannedInvoiceAndBillImmediately(initializer)
    }

    if(initializer.BillingInstruction typeis Audit
        and initializer.BillingInstruction.SpecialHandling == SpecialHandling.TC_FABILLONNEXT_TDIC) {
      _logger.debug("Configuring special handling for Billing instruction : ${initializer.BillingInstruction.Subtype} on Policy : ${initializer.PolicyPeriod?.PolicyNumber} to bill on the next invoice.")
      handleFinalAuditInvoiceToBillOnNext(initializer)
    }
  }

  private function handleFuturePlannedInvoiceAndBillImmediately(initializer : gw.api.domain.charge.ChargeInitializer){
    _logger.debug("Inside handleFuturePlannedInvoiceAndBillImmediately Billing instruction : ${initializer.BillingInstruction.Subtype} on Policy : ${initializer.PolicyPeriod?.PolicyNumber} to roll future planned invoices onto the next available invoice.")

    // Getting Invoice stream
    var invoiceStream = initializer.InvoiceStream

    // Creating a new Invoice with Bill Date of Charge Date
    var targetInvoice : Invoice
    _logger.debug("Creating a new Invoice with Bill Date of Charge Date")

    var dueDate = gw.api.util.DateUtil.currentDate().trimToMidnight().addDays(initializer.InvoiceStream.InvoicingLeadTime.DayCount)
    targetInvoice = initializer.InvoiceStream.getPlannedExistingInvoiceWithDates_Ext(DateUtil.currentDate(), dueDate)
    if(targetInvoice == null){
      targetInvoice = invoiceStream.createAndAddInvoice(gw.api.util.DateUtil.currentDate())
      targetInvoice.PaymentDueDate = dueDate
    }
    // Assign new Invoice to all entries on the current Charge
    initializer.Entries.each(\elt -> {
      elt.Invoice = targetInvoice
    })

    // roll up all existing future invoices and assign to the target invoice
    var plannedInvoices = initializer.PolicyPeriod.InvoiceItems.where( \ elt -> elt?.Invoice?.Planned)
    _logger.debug("plannedInvoiceExists : ${plannedInvoices.HasElements}")

    if(plannedInvoices.HasElements){
      plannedInvoices.each( \ invoiceitem -> {
        invoiceitem.Invoice = targetInvoice
        _logger.debug("rolled up all existing future invoices and assigned to the target invoice.")
      })
    }
  }

  private function handleFinalAuditInvoiceToBillOnNext(initializer : gw.api.domain.charge.ChargeInitializer) {
    _logger.debug("Inside handleFinalAuditInvoiceToBillOnNext Billing instruction : ${initializer.BillingInstruction.Subtype} on Policy : ${initializer.PolicyPeriod?.PolicyNumber} to bill on the next available invoice.")

    // Getting Invoice stream
    var invoiceStream = initializer.InvoiceStream

    // Creating a new Invoice with Bill Date of Charge Date
    var targetInvoice : Invoice
    _logger.debug("Creating a new Invoice with Bill Date of 1st day of next month")

    var billDate : Date
    if(DateUtil.currentDate().DayOfMonth != 1){
      billDate = DateUtil.createDateInstance(DateUtil.currentDate().MonthOfYear + 1, 1, DateUtil.currentDate().YearOfDate)
    } else if(DateUtil.currentDate().MonthOfYear == 12) {
      billDate = DateUtil.createDateInstance(1, 1, DateUtil.currentDate().YearOfDate + 1)
    } else {
      billDate = DateUtil.currentDate()
    }

    var dueDate = billDate.trimToMidnight().addDays(initializer.InvoiceStream.InvoicingLeadTime.DayCount)
    targetInvoice = initializer.InvoiceStream.getPlannedExistingInvoiceWithDates_Ext(billDate, dueDate)
    if(targetInvoice == null){
      targetInvoice = invoiceStream.createAndAddInvoice(billDate)
      targetInvoice.PaymentDueDate = dueDate
    }
    // Assign new Invoice to all entries on the current Charge
    initializer.Entries.each(\elt -> {
      elt.Invoice = targetInvoice
    })
  }

/*
  protected function adjustInvoicesAndEntries (initializer : gw.api.domain.charge.ChargeInitializer) : void {
    var backdatedEntries : List<Entry>;
    var billingInstruction = initializer.BillingInstruction;
    var earliestInvoiceEventDate : Date;
    var invoiceStream = initializer.InvoiceStream;
    var newInvoice : Invoice;

    if (billingInstruction typeis PolicyChange) {
      // Allocate credits across all invoices, charges across planned invoices.
      var startDate : Date;
      if (initializer.Entries.first().Amount.IsPositive) {
        var latestBilledOrDueInvoice = getLatestBilledOrDueInvoice (initializer.PolicyPeriod);
        if (latestBilledOrDueInvoice != null) {
          startDate = latestBilledOrDueInvoice.EventDate.addDays(1);    // 1 Day added because we want all invoice items
          // that occur after the last billed or due
          // invoice
        }
      }
      allocateEntriesProportionally (initializer, startDate);
    }

    // Remove unused invoices
    for (invoice in invoiceStream.Invoices.where(\i -> i.Planned)) {
      // Don't remove invoices with charges or invoice items
      if (invoice.InvoiceItems.IsEmpty and initializer.Entries.hasMatch(\e -> e.Invoice == invoice) == false) {
        _logger.trace ("Removing invoice " + invoice.InvoiceNumber + " (EventDate = " + invoice.EventDate + ")");
        invoice.remove();
      }
    }
  }
*/

  /**
   * Get the latest billed or due invoice.  Ideally, this could be done by just looking at the invoice
   * stream, but the latest billed or due invoice may be associated with the policy level invoice
   * stream.
  */
/*
  protected function getLatestBilledOrDueInvoice (policyPeriod : PolicyPeriod) : Invoice {
    return policyPeriod.Invoices.where(\i -> i.BilledOrDue).maxBy(\i -> i.EventDate);
  }
*/

  /**
   * Allocate the entries across the invoices.  If start date is specified, invoices before the start date will
   * be ignored.
  */
/*
  protected function allocateEntriesProportionally (initializer : gw.api.domain.charge.ChargeInitializer,
                                                    startDate : Date) : void {
    var entries = initializer.Entries;

    var allocation = getPolicyChangeAllocation (initializer.PolicyPeriod, startDate);
    if (_logger.TraceEnabled) {
      for (date in allocation.Keys.order()) {
        _logger.trace ("ChargeInitializer.allocateEntriesProportionally - EventDate = " + date
                          + ", Ratio = " + allocation.get(date));
      }
    }

    if (allocation.Count > 0) {
      var entryAmount : MonetaryAmount;
      var eventPercentage : BigDecimal;

      var amountRemaining = entries.sum (\e -> e.Amount);
      var percentageRemaining : BigDecimal = 1;

      for (eventDate in allocation.Keys.order()) {
        eventPercentage = allocation.get(eventDate);
        entryAmount = amountRemaining.multiply(eventPercentage/percentageRemaining).setScale(2, UP);

        // Create a new installment entry.
        initializer.createInstallment (entryAmount, eventDate);

        amountRemaining -= entryAmount;
        percentageRemaining -= eventPercentage;
      }

      // Remove the original entries.
      for (entry in entries) {
        entry.remove();
      }
    }
  }
*/

  /**
   * Get event dates and allocation percentage for breakup of policy change charge.  An map of event dates
   * and the percentage allocated to that event date will be returned.
   */
/*
  protected function getPolicyChangeAllocation (policyPeriod : PolicyPeriod,
                                                startDate : Date) : HashMap<Date,BigDecimal> {
    var policyChange : PolicyChange;
    var ratio : BigDecimal;
    var slice : Pair<Date,BigDecimal>;

    // Create a preview of a $1,000 full term endorsement to grab the invoice item event dates
    // as defined by the current Payment Plan.
    var policyChangeInfo = new PolicyChangeInfo();
    policyChangeInfo.PolicyNumber  = policyPeriod.PolicyNumber;
    policyChangeInfo.TermNumber    = policyPeriod.TermNumber;
    policyChangeInfo.PeriodStart   = policyPeriod.PolicyPerEffDate.XmlDateTime;
    policyChangeInfo.PeriodEnd     = policyPeriod.PolicyPerExpirDate.XmlDateTime;
    policyChangeInfo.EffectiveDate = policyPeriod.PolicyPerEffDate.XmlDateTime;

    // Set the description, which will cause this charge initializer to NOT modify the invoice items that
    // are generated based upon the Payment Plan.  If this was not done, an endless loop would occur.
    policyChangeInfo.Description   = CHANGE_PREVIEW_DESCRIPTION;

    var totalPremium = new MonetaryAmount (1000, policyPeriod.Currency);
    var chargeInfo = new ChargeInfo();
    chargeInfo.Amount = totalPremium as String;
    chargeInfo.ChargePatternCode = "Premium";
    policyChangeInfo.addChargeInfo(chargeInfo);

    // A non-persistent bundle is used so that the preview won't be committed to the database.
    BCBundleUtil.runWithNonPersistentBundle(\bundle -> {
      policyChange = policyChangeInfo.toPolicyChangeForPreview();
      policyChange.execute();
    });

    var invoiceItems = policyChange.InvoiceItems;
    if (startDate != null) {
      invoiceItems = invoiceItems.where (\ii -> ii.EventDate >= startDate);
    }

    var retval = new HashMap<Date,BigDecimal>();

    if (invoiceItems.HasElements) {
      var invoiceItemMap = invoiceItems.partition (\ii -> ii.EventDate);
      totalPremium = invoiceItems.sum (\ii -> ii.Amount);

      for (eventDate in invoiceItemMap.Keys.order() index i) {
        ratio = invoiceItemMap.get(eventDate).sum(\ii -> ii.Amount) / totalPremium;

        if (retval.containsKey(eventDate)) {
          // Multiple event dates were changed, keep a running total.
          ratio = ratio + retval.get(eventDate);
        }
        retval.put (eventDate, ratio);
      }
    }

    return retval;
  }
*/

  //
  // APW FUNCTIONS
  //
  private function distributeInvoiceItemsEvenly(initializer : gw.api.domain.charge.ChargeInitializer) {
    _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Entering")
    // If policy period is created later than first invoice, distribute amount evenly across all remaining invoices.
    // This overrides the OOTB behavior of stacking all the old invoice items onto the first invoice.
    var invoiceMap = new HashMap<Invoice, List<Entry>>()
    var sortedEntries = initializer.Entries.orderBy(\ entry -> entry.EventDate)

    // First group entries together by Invoice
    _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Original View:")
    for (entry in sortedEntries) {
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly -   Invoice Item: " + entry.Amount + " "
          + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber)
      var invoiceEntryMapping = invoiceMap.get(entry.Invoice)
      if (invoiceEntryMapping == null) {
        var entryList = new ArrayList<Entry>()
        entryList.add(entry)
        invoiceMap.put(entry.Invoice, entryList)
      }
      else {
        invoiceEntryMapping.add(entry)
      }
    }

    // If Audit Billing Instruction, do not attempt to modify any items
    if ((initializer.BillingInstruction typeis Audit) or (initializer.BillingInstruction typeis PolicyChange) ) {
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Audit Billing Instruction Received for "
          + initializer.PolicyPeriod.PaymentPlan + " payment plan. Returning without any modification.")
      return
    }

    var policyPeriodEffectiveDate = initializer.PolicyPeriod.EffectiveDate
    var currentDate = DateUtil.currentDate()

    // If this charge is a Reinstatement charge that corresponds with a down payment cancellation charge, or a
    // Cancellation charge the amount is not distributed among the rest of the entries; it is placed on the first invoice
    var amountToDistribute = initializer.Entries.sum( \ entry -> entry.Amount)
    var newInvoices = initializer.Entries*.Invoice
    var currentInvoices = initializer.PolicyPeriod.Account.Invoices.where( \ invoice -> invoice.InvoiceStream == newInvoices.first().InvoiceStream)
    var downPayment = currentInvoices*.InvoiceItems.firstWhere( \ invoice -> invoice.Type == typekey.InvoiceItemType.TC_DEPOSIT
        and invoice.Charge.Amount.Amount > 0 and invoice.Charge.Amount == amountToDistribute.negate() )
    if( initializer.BillingInstruction typeis Cancellation
        or (initializer.BillingInstruction typeis Reinstatement and downPayment!= null)) {
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Total amount corresponds with a cancel down payment charge and is placed in the first entry")
      var firstEntry = sortedEntries.first()
      firstEntry.Amount = amountToDistribute
      initializer.Entries.each( \ entry -> {
        if(entry != firstEntry){
          entry.remove()
        }
      })

      //If the active entry is before the effective date is moved to the first
      while(DateUtil.compareIgnoreTime(policyPeriodEffectiveDate, firstEntry.EventDate) == 1) {
        firstEntry.EventDate = firstEntry.EventDate.addMonths(1)
      }
    } else {
      //When the Payment Plan is changed from Semi-Annual to APW, sometimes the functionality OOTB returns no invoices for the first
      //months. The below code check this situation and create as many invoices as needed with 0 as amount money
      var firstActiveEntryDate = sortedEntries.first().Invoice.EventDate
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - First Active Invoice Date: " + firstActiveEntryDate)
      var firstCorrectDate : Date
      //To calculate the necessary invoices the last billed/planned invoice month is subtract to the first invoice generated.
      //The result is the number of Installment that have to created
      var billedActiveEntries = sortedEntries?.where( \ entry -> entry.Invoice.Status == typekey.InvoiceStatus.TC_BILLED
          || entry.Invoice.Status == typekey.InvoiceStatus.TC_DUE).orderBy( \ entry -> entry.Invoice.EventDate)
      if(billedActiveEntries.Count > 0) {
        firstCorrectDate = billedActiveEntries.last().Invoice.DueDate
      } else {
        if(DateUtil.compareIgnoreTime(currentDate, policyPeriodEffectiveDate) > 0){
          firstCorrectDate = currentDate
        } else {
          firstCorrectDate = policyPeriodEffectiveDate
        }
      }
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - First Correct Date: " + firstCorrectDate)
      var monthsLeft = DateUtil.getMonthsBetweenDates(firstCorrectDate, firstActiveEntryDate)
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Months between First Active Invoice Date and First Correct Date: " + monthsLeft)

      var zero = new MonetaryAmount(0, sortedEntries.first().Amount.Currency)
      var previousDate : Date
      while(monthsLeft > 0){
        previousDate = sortedEntries.first().Invoice.EventDate.addMonths(-monthsLeft)
        if (DateUtil.compareIgnoreTime(previousDate, policyPeriodEffectiveDate) > 0
            && DateUtil.compareIgnoreTime(previousDate, currentDate) > 0) {
          _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Less installments generated by default. New installment created in " + previousDate)
          //The first active entry date is take to calculate the new installment dates
          initializer.createInstallment(zero, previousDate)
        }
        monthsLeft--
      }

      //When the Payment Plan is changed from Semi-Annual to APW, sometimes the functionality OOTB returns no invoices for the last
      //months. The below code check this situation and create as many invoices as needed with 0 as amount money
      var lastActiveEntryDate = sortedEntries.last().Invoice.EventDate
      var dateTwoMonthsBeforeExpirationDate = initializer.PolicyPeriod.ExpirationDate.addMonths(-2)
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Two Months Before Expiration Date: " + dateTwoMonthsBeforeExpirationDate)
      monthsLeft = DateUtil.getMonthsBetweenDates(lastActiveEntryDate, dateTwoMonthsBeforeExpirationDate)
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Months between Last Active Invoice Date and Two Months Before Expiration Date: " + monthsLeft)

      var nextDate : Date
      while(monthsLeft > 0){
        nextDate = lastActiveEntryDate.addMonths(monthsLeft)
        if(DateUtil.compareIgnoreTime(dateTwoMonthsBeforeExpirationDate, nextDate) == 1){
          _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Less installments generated by default. New installment created in " + nextDate)
          //The first active entry date is take to calculate the new installment dates
          initializer.createInstallment(zero, nextDate)
        }
        monthsLeft--
      }

      if (_logger.DebugEnabled) {
        _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - View after any installments created:")
        for (entry in initializer.Entries.orderBy( \ entry -> entry.EventDate)) {
          _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly -   Invoice Item: " + entry.Amount + " "
              + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber)
        }
      }

      amountToDistribute = initializer.Entries.sum( \ entry -> entry.Amount)
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Total amount to distribute among entries: " + amountToDistribute)

      // For the Invoices with multiple entries, remove the additional entries.
      for (key in invoiceMap.Keys) {
        var invoiceEntryMapping = invoiceMap.get(key)
        if (invoiceEntryMapping.Count > 1) {
          while (invoiceEntryMapping.Count > 1) {
            var entryToRemove = invoiceEntryMapping.first()
            _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Removing entry - Invoice Item: "
                + entryToRemove.Amount + " " + entryToRemove.EventDate + " | Invoice: " + entryToRemove.Invoice
                + " " + entryToRemove.Invoice.InvoiceNumber)
            // Remove the entry
            entryToRemove.remove()
            invoiceEntryMapping.remove(entryToRemove)
          }
        }
      }

      if (_logger.DebugEnabled) {
        _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - View after any installments removed:")
        for (entry in initializer.Entries.orderBy( \ entry -> entry.EventDate)) {
          _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly -   Invoice Item: " + entry.Amount + " "
              + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber)
        }
      }

      //Entries with an event date before the current date are removed, unless they are all before the current date
      var entriesBeforePolicyPeriod = sortedEntries?.where( \ entry -> DateUtil.compareIgnoreTime(currentDate, entry.Invoice.EventDate) > 0)
      if(initializer.Entries.size() != entriesBeforePolicyPeriod.size()){
        for(entry in entriesBeforePolicyPeriod){
          _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Invoice Item removed because has an event date before the current date: "
              + entry.Amount + " " + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber)
          entry.remove()
        }
      }

      // Distribute the amount among the rest of the entries.
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Final Entry Count: " + initializer.Entries.Count)
      var individualInvoiceAmount = amountToDistribute.divide(initializer.Entries.Count, 2, RoundingMode.DOWN)
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Amount for each invoice: " + individualInvoiceAmount)
      // Add the amount to each entry.
      initializer.Entries.each( \ entry -> {
        entry.Amount = individualInvoiceAmount
        amountToDistribute = amountToDistribute.subtract(individualInvoiceAmount)
      })

      // Distribute the remainder starting with the first entry.  Note the subtraction method is used here since the
      // remainder() method of BigDecimal may not work properly when taking the parameter of a whole number divisor.
      _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Remainder to add starting with the first invoice item: " + amountToDistribute)
      var oneCent = new BigDecimal("0.01").setScale(2, RoundingMode.HALF_UP).ofDefaultCurrency()
      for (entry in initializer.Entries.orderBy( \ entry -> entry.Invoice.EventDate)) {
        if (amountToDistribute.IsPositive) {
          entry.Amount = entry.Amount.add(oneCent)
          amountToDistribute = amountToDistribute.subtract(oneCent)
        }
        else if (amountToDistribute.IsNegative) {
          entry.Amount = entry.Amount.subtract(oneCent)
          amountToDistribute = amountToDistribute.add(oneCent)
        }
        else {
          break
        }
      }

    }
    _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Final Entry View: ")
    initializer.Entries.orderBy( \ entry -> entry.Invoice.EventDate).each( \ entry -> _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly -  Invoice Item: "
        + entry.Amount + " " + entry.EventDate + " | Invoice: " + entry.Invoice + " " + entry.Invoice.InvoiceNumber))

    _logger.debug("ChargeInitializer::distributeInvoiceItemsEvenly - Exiting")
  }
}
