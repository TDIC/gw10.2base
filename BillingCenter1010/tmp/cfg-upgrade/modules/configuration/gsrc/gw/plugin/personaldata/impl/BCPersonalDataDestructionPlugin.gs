package gw.plugin.personaldata.impl

uses gw.api.personaldata.PersonalDataDestroyer
uses gw.plugin.personaldata.AbstractPersonalDataDestructionPlugin
uses gw.plugin.personaldata.PersonalDataDisposition
uses org.apache.commons.lang.NotImplementedException

@Export
class BCPersonalDataDestructionPlugin extends AbstractPersonalDataDestructionPlugin {

  override function shouldDestroyUser(userContact: UserContact): PersonalDataDisposition {
    //Nothing is destroyed by default, change accordingly when enabling personal data removal
    return MUST_NOT_DESTROY
  }

  override function notifyDataProtectionOfficer(root: DestructionRootPinnable, title: String, message: String, errorDate: Date) {
    new DataProtectionOfficerNotifier(title, message, errorDate).notify(root)
  }

  override function notifyExternalSystemsRequestProcessed(requester: PersonalDataDestructionRequester) {
    //To notify the external system from which the request to purge the Contact with the given AddressBookUID originated
  }

  override property get Destroyer(): PersonalDataDestroyer {
    return new BCPersonalDataDestroyer()
  }
}