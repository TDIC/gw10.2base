package gw.transaction

@Export
class TransferFundsReversalWizardContext {

  var _reversalReason : ReversalReason
  var _fundsTransferReversal : FundsTransferReversal
  
  construct() {
    _fundsTransferReversal = new FundsTransferReversal()
  }

  property  set TransferFundTransaction(transferFund : TransferTransaction) {
    _fundsTransferReversal.FundsTransferTransaction = transferFund
    _fundsTransferReversal.initiateApprovalActivityIfUserLacksAuthority()
  }

  property  get TransferFundTransaction() : TransferTransaction {
    return _fundsTransferReversal.FundsTransferTransaction
  }


  function getSourceName() : String{
    if (TransferFundTransaction.FundsTransfer.SourceUnapplied != null) {
      return TransferFundTransaction.FundsTransfer.SourceUnapplied.TransactionSpecialDisplayName;
    } else {
      return TransferFundTransaction.FundsTransfer.SourceProducer.DisplayName
    }
  }
  
  function getDestinationName() : String{
    if (TransferFundTransaction.FundsTransfer.TargetUnapplied != null) {
      return  TransferFundTransaction.FundsTransfer.TargetUnapplied.TransactionSpecialDisplayName;
    } else {
      return TransferFundTransaction.FundsTransfer.TargetProducer.DisplayName
    }
  }
  
  function setReversalReason(reason : ReversalReason){
    _reversalReason = reason
  }
  
  function createReversal(){
    _fundsTransferReversal.ReversalReason = _reversalReason
    _fundsTransferReversal.execute()
  }

  property get FundsTraversalReversal() : FundsTransferReversal{
    return _fundsTransferReversal
  }
  
}