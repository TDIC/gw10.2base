package gw.agencybill

uses gw.api.locale.DisplayKey
uses gw.api.util.BCBigDecimalUtil
uses gw.pl.currency.MonetaryAmount

@Export
class AgencyDistributionHelper {
  private var _warningMessage : String

  construct(){
    _warningMessage = ""
  }

  function processWarningMessage() : String {
    return _warningMessage
  }

  function isNegative( agencyCycleDist : AgencyCycleDist, moneyReceived: AgencyBillMoneyRcvd) : Boolean{
    var unditribuedAmount : MonetaryAmount
    var producer : Producer

    if(agencyCycleDist == null && moneyReceived == null){
      return false
    } else if (moneyReceived == null){
      unditribuedAmount = agencyCycleDist.Amount - agencyCycleDist.NetDistributedToInvoiceItems - agencyCycleDist.NetInSuspense
      producer = agencyCycleDist.Producer
    } else {
      unditribuedAmount = moneyReceived.Amount - moneyReceived.NetAmountDistributed - moneyReceived.NetSuspenseAmount
      producer = moneyReceived.Producer
    }

    if(BCBigDecimalUtil.lessThan(producer.UnappliedAmount, unditribuedAmount)) {
      _warningMessage = DisplayKey.get("Web.PaymentReversalConfirmation.NegativeBalanceWarning")
      return true
    }
    return false
  }
}