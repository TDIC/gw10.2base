package gw.web.payment.batch

uses gw.api.util.CurrencyUtil
uses pcf.BatchPaymentDetailsPage
uses pcf.DesktopBatchPaymentsSearch
uses typekey.Currency

@Export
class DesktopBatchPaymentsView {

  var _defaultCurrency: Currency as CurrencyToCreateNewBatchWith

  construct(defaultCurrency: Currency) {
    _defaultCurrency = defaultCurrency
  }

  property get AllCurrDerivedFromCurrencyMode(): List<Currency> {
    return CurrencyUtil.isMultiCurrencyMode() ?
        Currency.getTypeKeys(false) :
        Collections.singletonList(_defaultCurrency)
  }

  static function goToBatchPaymentDetailsOrReloadPageIfBatchPaymentNull(batchPayment :BatchPayment) {
    batchPayment = BatchPayment.finder.findBy(batchPayment.getBatchNumber())
    if(batchPayment != null) {
      BatchPaymentDetailsPage.go(batchPayment)
    } else {
      DesktopBatchPaymentsSearch.go()
    }
  }
}
