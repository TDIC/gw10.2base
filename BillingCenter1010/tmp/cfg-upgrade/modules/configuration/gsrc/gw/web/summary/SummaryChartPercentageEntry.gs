package gw.web.summary

uses gw.api.util.Percentage
uses gw.api.util.StringUtil
uses gw.api.web.color.GWColor
uses gw.pl.currency.MonetaryAmount

uses java.math.BigDecimal

/**
 * Defines a percentage entry for a summary chart.
 * A percentage entry's value is a percentage value in points.
 */
@Export
class SummaryChartPercentageEntry extends SummaryChartEntry {
  /**
   * Construct a percentage entry from the specified percentage points.
   */
  construct(displayKeyID : String, points : BigDecimal, color : GWColor) {
    super(displayKeyID, points, color)
  }

  /**
   * Construct a percentage entry from the ratio of two monetary values.
   */
  construct(displayKeyID : String, value : MonetaryAmount, totalValue : MonetaryAmount, color : GWColor) {
    // value is points...
    this(displayKeyID, Percentage.fromDisplaySafeRatioBetween(value, totalValue)
            .Points.resolve(4, HALF_UP),
        color)
  }

  override property get DisplayValue() : Object {
    return StringUtil.formatPercentagePoints(ChartValue, 0)
  }
}