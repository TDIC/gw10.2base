package gw.web.summary

uses com.ibm.icu.text.DateTimePatternGenerator
uses com.ibm.icu.text.SimpleDateFormat
uses com.ibm.icu.util.ULocale
uses gw.api.locale.DisplayKey
uses gw.api.util.LocaleUtil
uses gw.pl.currency.MonetaryAmount

uses java.util.Date

/**
 * Defines the abstract encapsulation of the value and display logic used to
 * display the financials information on a summary screen.
 */
@Export
abstract class SummaryFinancialsHelper {
  protected var _tAccountOwner: TAccountOwner

  /**
   * The {@link Date} format to be used for the "on" clause in a label.
   */
  public final static var ON_DATE_LABEL_FORMAT : String = "EEEE, MMMM dd, yyyy"

  property get OnDateLabelFormat() : String {
    return ON_DATE_LABEL_FORMAT
  }

  /**
   * Format a date for use in an "on" label: day month day-of-month.
   */
  static function onDateLabel(labelId : String, date : Date) : String {
    return DisplayKey.get(labelId, date.format(ON_DATE_LABEL_FORMAT))
  }

  abstract property get NextInvoiceDueLabel() : String

  abstract property get LastPaymentReceivedLabel() : String

  property get UnbilledAmount() : MonetaryAmount {
    return _tAccountOwner.UnbilledUnsettledAmount
  }

  property get BilledAmount() : MonetaryAmount {
    return _tAccountOwner.BilledUnsettledAmount
  }

  property get DueAmount() : MonetaryAmount {
    return _tAccountOwner.DueUnsettledAmount
  }

  property get DelinquentAmount() : MonetaryAmount {
    return _tAccountOwner.DelinquentAmount
  }

  property get OutstandingAmount() : MonetaryAmount {
    return _tAccountOwner.OutstandingUnsettledAmount
  }

  property get RemainingBalance() : MonetaryAmount {
    return _tAccountOwner.RemainingUnsettledBalance
  }

  property get PaidAmount() : MonetaryAmount {
    return _tAccountOwner.PaidAmount
  }

  property get PaidOrWrittenOffAmount() : MonetaryAmount {
    return PaidAmount + WrittenOffAmount
  }

  property get TotalValue() : MonetaryAmount {
    return _tAccountOwner.TotalValue
  }

  property get UnpaidAmount() : MonetaryAmount {
    return _tAccountOwner.UnpaidAmount
  }

  property get WrittenOffAmount() : MonetaryAmount {
    return _tAccountOwner.WriteoffExpense - _tAccountOwner.NegativeWriteoff
  }

  protected function getBestDateFormatAccordingToLocale(labelDateFormat : String, date : Date): String {
    var uLocale = new ULocale(LocaleUtil.CurrentLocaleType.Code)
    var pattern = DateTimePatternGenerator.getInstance(uLocale).getBestPattern(labelDateFormat)
    return new SimpleDateFormat(pattern, uLocale).format(date)
  }
}