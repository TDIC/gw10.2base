package gw.web.summary

uses gw.api.web.color.GWColor
uses gw.pl.currency.MonetaryAmount

/**
 * Defines a {@link MonetaryAmount} entry for a summary chart.
 */
@Export
class SummaryChartMonetaryEntry extends SummaryChartEntry {
  /**
   *  The entry value.
   */
  var _value : MonetaryAmount as readonly Value

  construct(displayKeyID : String, value : MonetaryAmount, color : GWColor) {
    super(displayKeyID, value.Amount, color)
    _value = value
  }

  override property get DisplayValue() : Object {
    return this.Value
  }
}