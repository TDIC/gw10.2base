package gw.web.summary

uses gw.api.locale.DisplayKey
uses gw.util.Pair

uses java.math.BigDecimal
uses java.util.List

/**
 * Defines a helper for summary (pie) charts that supports display of a pie
 * chart with an associated vertically oriented legend. The order of the entries
 * in the legend are the same as the specified {@link SummaryChartEntry}s while
 * the color of the pie slice is that of the {@link SummaryChartEntry#Color}
 * property.
 *
 * The colors are specified in order of the {@link SummaryChartEntry.Color}
 * ordinals.
 *
 * @see SummaryChartPanelSet
 */
@Export
class SummaryChartHelper {
  /**
   * The summary chart entries with which to display a chart.
   */
  var _summaryChartEntries : List<SummaryChartEntry>
      as readonly SummaryChartEntries
  var _chartEntries : List<Pair<String, BigDecimal>>
  var _legends : List<String>

  /**
   * Construct a summary chart helper from a list of entries.
   *
   * @param summaryChartEntries A list of {@link SummaryChartEntry}s
   *                            in the order that the legends should
   *                            be displayed. Color is specified by
   *                            the {@link SummaryChartEntry#Color}.
   */
  construct(summaryChartEntries : List<SummaryChartEntry>) {
    _summaryChartEntries = summaryChartEntries
  }

  /**
   * The entries that make up the data series for the chart.
   *
   * @return A list of legend SummaryChartEntries to display
   */
  property get ChartEntries() : List<SummaryChartEntry> {
    return _summaryChartEntries
  }

}
