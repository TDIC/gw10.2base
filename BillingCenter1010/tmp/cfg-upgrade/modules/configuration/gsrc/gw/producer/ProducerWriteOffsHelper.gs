package gw.producer

uses gw.api.domain.accounting.AbstractWriteOff
uses gw.api.locale.DisplayKey

uses java.lang.Throwable
uses java.util.List

@Export
class ProducerWriteOffsHelper {

  static function reverse(writeOff : AbstractWriteOff, currentLocation : pcf.ProducerWriteOffs) {
    currentLocation.startEditing()
    try {
      writeOff.reverse(currentLocation)
      currentLocation.commit()
    } catch ( t : Throwable ) {
      currentLocation.cancel()
      throw t
    }
  }

  static function statusFor(writeOff : AbstractWriteOff) : String {
    if (writeOff.Reversed) {
      return DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Reversed") 
    }
    if (writeOff.PendingReversal) {
      return DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.ReversalPending")
    } 
    if (isExecuted(writeOff)) {
      return DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Executed")      
    } 
    if (writeOff.Rejected) {
        return DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Rejected")
    } 
    if (writeOff.Pending) {
        return DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Pending")
    }
    return DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Unknown")
  }

  static function findProducerWriteOffsFor(producer : Producer) : List<AbstractWriteOff> {
    return producer.findAssociatedWriteoffs()
  }

  static function findProducerWriteOffsConstrainedByAge(producer : Producer, writeOffAgeRestriction : gw.producer.ProducerWriteOffAgeRestriction) : List<AbstractWriteOff> {
    if (writeOffAgeRestriction.hasAgeRestriction()) {
      return producer.findAssociatedWriteoffsWithCreateTimeNoOlderThan(writeOffAgeRestriction.getMaximumWriteoffAgeInDays())
    } else {
      return findProducerWriteOffsFor(producer)
    }
  }

  
  /**
   * For the purposes of showing the status of a write-off, the answer to the question of whether or
   * not we should show its status as "Executed" is more complicated than simply if the write-off has
   * been executed.  A write-off may be executed but also reversed, or executed but also pending 
   * reversal.  In these cases, we wouldn't want to show the status as "Executed" but instead as
   * "Reversed" or "Pending Reversal" respectively.
   */
  static function isExecuted(writeOff : AbstractWriteOff) : boolean {
    return writeOff.Executed && !writeOff.Reversed && !writeOff.PendingReversal
  }
}
