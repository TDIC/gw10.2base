package gw.acc.activityenhancement

uses gw.api.assignment.Assignee
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.locale.DisplayKey

class DesktopQueuedActivitiesUtil {
 
  private construct() {
  }
  
  public static function assignActivitiesToMe(activities : Activity[]) : void {
    gw.transaction.Transaction.runWithNewBundle(\b -> {
      for(act in activities) {
        act = b.add(act)
        act.assign(act.AssignedGroup, User.util.CurrentUser)
      }
    })
  }

  public static function getQueues() : AssignableQueue[] {
    var queues = AssignableQueue.finder.findVisibleQueuesForUser(User.util.CurrentUser) as IQueryBeanResult<AssignableQueue>
    return queues.toTypedArray()
  }

  public static function getTotalNumberOfActivitiesForUser() : int{
    return getQueues().sum(\q -> q.Activities_Ext.Count)
  }
  
  public static function assignActivity(assignee : Assignee, act : Activity) : void {
    // check if this is a queue. Queue has a pattern of queue - group
    var i = assignee.toString().indexOf("-")
    if (i > 0) {
      var group = assignee.toString().substring(i + 2)
      var queue = assignee.toString().substring (0, i - 1)
      var grp = Query.make(Group).compare(Group#Name, Equals, group).select().first()
      if (grp != null) {
        act.assignGroup(grp)
        var que = act.AssignedGroup.getQueue(queue)
        if (que != null){
          act.assignActivityToQueue(que, grp)
          return
        }
      }   
    }
    // not a queue; default to the OOTB assignment
    assignee.assignToThis(act)
    return   
  }
  
  public static function assignActivities(popup : gw.api.assignment.AssignmentPopup, activities : Activity[]) : boolean {
    var result = popup.performAssignment();
    // handle queue assignment not supported OOTB
    if (popup.SelectedFromList typeis AssignableQueue) {
      activities.each(\ act -> assignActivity(popup.SelectedFromList, act))
    }
    return result
  }

  public static function assignActivitiesInNewBundle(popup : gw.api.assignment.AssignmentPopup, activities : Activity[]) : Boolean{
    var result = popup.performAssignment();
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      if (popup.SelectedFromList typeis AssignableQueue) {
        activities.each(\ act -> {
          act = bundle.add(act)
          assignActivity(popup.SelectedFromList, act)
        })
      }
    })
    return result
  }

  public static function SuggestedAssignees (popup : gw.api.assignment.AssignmentPopup) : gw.api.assignment.Assignee[] {
    var grps = User.util.CurrentUser.AllGroups
    var queues = Query.make(AssignableQueue).select().where(\q -> grps.contains(q.Group))
    return popup.SuggestedAssignees.concat(queues.toTypedArray())
  }

  public static function SelectFromSearchLabel (popup : gw.api.assignment.AssignmentPopup) : String {
    var result = popup.SelectFromSearchLabel
    var searchTypes = popup.Picker.AllowedAssignmentSearchTypes
    if (!searchTypes.contains(AssignmentSearchType.TC_QUEUE)) {
      var label = popup.SelectFromSearchLabel.chomp(":")
      result = DisplayKey.get("Accelerator.Activity.AssignActivitiesPopup.SelectFromSearchLabel", label)
    }
    return result
  }
  
  public static function AllowedAssignmentSearchTypes (picker : gw.api.assignment.AssigneePicker) : List<Object> {
    var result = picker.AllowedAssignmentSearchTypes
    if (!result.contains(AssignmentSearchType.TC_QUEUE)) {
      result.add(AssignmentSearchType.TC_QUEUE)
    }
    return result 
  }
}
