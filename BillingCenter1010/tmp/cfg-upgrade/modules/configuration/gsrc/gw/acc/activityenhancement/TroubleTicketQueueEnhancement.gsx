package gw.acc.activityenhancement

uses gw.api.assignment.Assignee

/**
 * Add queue functionality for trouble tickets to match queue functionality
 * added by Activity enhancement accelerator.
 */
enhancement TroubleTicketQueueEnhancement: TroubleTicket {

  /**
   * Returns the suggested assignees for the trouble ticket, including the assignable
   * queues associated with the current user's groups.
   */
  property get SuggestedAssigneesIncludingQueues_Ext() : Assignee[] {
    var queues = AssignableQueue.finder.findVisibleQueuesForUser(User.util.CurrentUser)
          as gw.api.database.IQueryBeanResult<AssignableQueue>
    return this.SuggestedAssignees
        .concat(this.InitialAssigneeForPicker)
        .concat(queues.toTypedArray() as gw.api.assignment.Assignee[])
  }

  public function assignTo_Ext(anAssignee : Assignee) {
    if (anAssignee typeis AssignableQueue) { // assigning TT to a queue not supported OOTB
      this.AssignedQueue = anAssignee
      this.AssignedGroup = anAssignee.Group
      this.AssignedUser = null
      this.AssignedByUser = User.util.CurrentUser

    } else { // OOTB assignment
      anAssignee.assignToThis(this)
    }
  }
}
