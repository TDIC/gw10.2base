package gw.acc.activityenhancement.subtypes

uses com.guidewire.bc.domain.approval.BCApprovalHandler

class NegWrofApprActivityApprovalMethods
    extends NegativeWriteoffApprovalMethods <NegWrofApprActivity> {

  construct(activity : NegWrofApprActivity) {
    super(activity)
  }

  override property get NegativeWriteoff(): NegativeWriteoff {
    return Activity.NegativeWriteoff
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return Activity.ApprovalHandler
  }
}