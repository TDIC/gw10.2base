package gw.acc.activityenhancement

uses gw.api.database.Query
uses gw.transaction.Transaction
uses gw.entity.IEntityPropertyInfo
uses gw.api.database.IQueryBeanResult

/**
 * Enhancement functions related to activities.
 */
enhancement AssignableQueueActivityEnhancement : entity.AssignableQueue {

  property get QueueContainsActivities_Ext() : boolean {
    return this.Activities_Ext.Count != 0
  }

  property get QueueContainsTroubleTickets_Ext() : boolean {
    var act = Query.make(TroubleTicket).compare(TroubleTicket#AssignedQueue, Equals, this)
        .select().Count
    return act != 0
  }

  /**
   * Retrieves the next activity from the queue and assigns it to the current
   * user.
   */
  function assignNextQueuedActivityToMe_Ext() {
    Transaction.runWithNewBundle(\ bundle -> {
      var act = this.Activities_Ext.sortBy(\act -> act.AssignmentDate).first()
      if (act != null) {
        act = bundle.add(act)
        act.assign(act.AssignedGroup, User.util.CurrentUser)
        bundle.commit()
      }
    })
  }

  /**
   * Retrieves the next trouble ticket from the queue and assigns it to the current
   * user.
   */
  function assignNextQueuedTroubleTicketToMe_Ext() {
    Transaction.runWithNewBundle(\ bundle -> {
      var tt = Query.make(TroubleTicket).compare(TroubleTicket#AssignedQueue, Equals, this)
          .select()
          .orderBy(TroubleTicket#AssignmentDate.PropertyInfo as IEntityPropertyInfo)
          .FirstResult
      if (tt != null) {
        tt = bundle.add(tt)
        tt.assign(tt.AssignedGroup, User.util.CurrentUser)
        bundle.commit()
      }
    })
  }

  /**
   *
   * @return activities for the queue except approval activities created by the current user.
   */
  // GBC-3177 UW Admins who create transactions requiring approval can not view them in queue
  property get Activities_Ext() : List<Activity> {
    var actvities =  Query.make(Activity).compare(Activity#AssignedQueue, Equals, this)
    var notCompletedActivities = actvities.compare(Activity#Status, NotEquals, ActivityStatus.TC_COMPLETE)
        .select()
    return notCompletedActivities?.where(\act -> !(act typeis ActivityCreatedByAppr)
        or (act typeis ActivityCreatedByAppr)) // and act.CreateUser != User.util.CurrentUser))
  }

  property get TroubleTickets_Ext() : List<TroubleTicket> {
    var troubleTickets = Query.make(TroubleTicket).compare(TroubleTicket#AssignedQueue, Equals, this)
    .select()
    return troubleTickets.where(\elt -> elt.TicketStatus != TroubleTicketStatus.TC_CLOSED.DisplayName)
  }

}
