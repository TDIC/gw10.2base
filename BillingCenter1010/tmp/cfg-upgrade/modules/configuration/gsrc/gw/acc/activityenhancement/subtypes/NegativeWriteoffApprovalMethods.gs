package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity

abstract class NegativeWriteoffApprovalMethods<T extends ActivityCreatedByAppr>
    implements IApprovalActivity {

  private var _activity : T as Activity

  protected construct(activity : T) {
    _activity = activity
  }

  protected abstract property get NegativeWriteoff() : NegativeWriteoff

  final override function associateRelatedEntities() {
    var negativeWriteoff = this.NegativeWriteoff
    if (negativeWriteoff typeis AcctNegativeWriteoff) {
      // Account -> Actions -> New Transaction -> Negative Write-Off (Reversal)
      _activity.Account = negativeWriteoff.Account
    } else if (negativeWriteoff typeis ProdNegativeWriteoff) {
      // Producer -> Actions -> Negative Write-Off (Reversal)
      _activity.Producer_Ext = negativeWriteoff.Producer
    }
  }
}