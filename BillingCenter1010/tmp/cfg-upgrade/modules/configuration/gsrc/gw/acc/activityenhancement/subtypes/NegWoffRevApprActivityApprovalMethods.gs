package gw.acc.activityenhancement.subtypes

uses com.guidewire.bc.domain.approval.BCApprovalHandler

class NegWoffRevApprActivityApprovalMethods
    extends NegativeWriteoffApprovalMethods <NegWoffRevApprActivity> {

  construct(activity : NegWoffRevApprActivity) {
    super(activity)
  }

  override property get NegativeWriteoff() : NegativeWriteoff {
    return Activity.NegativeWriteoffRev.NegativeWriteoff
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return Activity.ApprovalHandler
  }
}