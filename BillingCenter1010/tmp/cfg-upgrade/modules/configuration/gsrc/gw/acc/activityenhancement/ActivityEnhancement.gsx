package gw.acc.activityenhancement

uses gw.api.assignment.Assignee
uses pcf.ActivityDetailWorksheet

/**
 * Enhancement functions for the activities enhancement accelerator.
 */
enhancement ActivityEnhancement : entity.Activity {

  /**
   * Returns the suggested assignees for the activity, including the assignable
   * queues associated with the current user's groups.
   */
  property get SuggestedAssigneesIncludingQueues_Ext() : Assignee[] {
    var queues = AssignableQueue.finder.findVisibleQueuesForUser(User.util.CurrentUser)
        as gw.api.database.IQueryBeanResult<AssignableQueue>
    return this.SuggestedAssignees
        .concat(queues.toTypedArray() as gw.api.assignment.Assignee[])
  }

  /**
   * Adjust description text for approval activity.
   * This will remove the (null) approver name if approval activities are assigned to a queue.
   * The passed in edit mode will be respected and by only adjusting the description if in edit mode.
   */
  public function getActivityWithAdjustedDescription_Ext(editMode: Boolean) : Activity {
    if ((editMode == true) and (this.AssignedTo typeis AssignableQueue) and (this.Description != null)) {
      this.Description = this.Description.replaceAll("null's approval", "approval") // only works for US_EN but harmless for others.
    }
    return this
  }

  /**
   * Retrieve rejection notification
   */
  private function getRejectNotification() : Activity {
    var result = this.Bundle.getBeansByRootType(Activity).firstWhere(\ a -> {
      // Rejection Notification has same subtype as Approval Activity but uses Notification Pattern
      var condition = false
      var act = (a as Activity)
      if (act.ActivityPattern == ActivityPattern.Notification
          and act.Subtype == this.Subtype) {
        condition = true
      }
      return condition
    }) as Activity

    if (result != null) {
      // copy related entities
      result.Account = this.Account
      result.PolicyPeriod = this.PolicyPeriod
      result.Producer_Ext = this.Producer_Ext

      // fix completion bug for Disbursement reject notification (JIRA BC-9557)
      if (result typeis DisbApprActivity) {
        result.Status = ActivityStatus.TC_OPEN
      }
    }
    return result
  }

  /**
   * Reject activity and retrieve rejection notification
   */
  public function rejectActivity_Ext() : Activity {
    if (this typeis DisbApprActivity) {
      gw.api.web.activity.DisbApprActivityUtil.rejectActivity(this);
    } else {
      gw.api.web.activity.ActivityUtil.rejectActivity(this)
    }
    return getRejectNotification()
  }

  /**
   * Reject disbursement activity with holding future disbursements and retrieve rejection notification
   */
  public function rejectActivityAndHoldDisbursements_Ext() : Activity {
    if (this typeis DisbApprActivity) {
      gw.api.web.activity.DisbApprActivityUtil.rejectActivityWithHold(this);
    }
    return getRejectNotification()
  }

  /**
   * Reject and go to new notification activity created
   */
  public function rejectButtonAction_Ext(rejectAndHoldFlag: Boolean, location: pcf.api.Location) {
    var rejectNotification = rejectAndHoldFlag ? this.rejectActivityAndHoldDisbursements_Ext() : this.rejectActivity_Ext()
    location.commit()
    if (rejectNotification != null) ActivityDetailWorksheet.goInWorkspace(rejectNotification)
  }

  /**
   * Adjust the "assigned to" for approval activity.
   * This will use the default for the approval activity pattern, if it is set to a queue.
   */
  public function getAssignedToForApproval_Ext(editMode: Boolean) : Activity {
    if (editMode == true and this.ActivityPattern.Code == "approval") {
      var defaultQueue = this.ActivityPattern.AssignableQueue_Ext
      if (defaultQueue != null) {
        this.AssignedQueue = defaultQueue
        this.AssignedUser = null
      }
    }
    return this
  }
  function  SetApproverName() {
    (this as DisbApprActivity).Approver_TDIC = User.util.CurrentUser
  }

}
