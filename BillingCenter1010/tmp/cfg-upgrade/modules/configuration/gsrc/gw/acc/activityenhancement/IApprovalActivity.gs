package gw.acc.activityenhancement

/**
 * This interface is a common interface that all BillingCenter activities can/
 * should implement to automatically associate a related policy, account or
 * producer.
 */
interface IApprovalActivity {
  /**
   * Adds the appropriate associations for this activity, if any.
   */
  function associateRelatedEntities()

  /**
   * Gets the approval handler for this activity.
   */
  property get ApprovalHandler()
      : com.guidewire.bc.domain.approval.BCApprovalHandler
}