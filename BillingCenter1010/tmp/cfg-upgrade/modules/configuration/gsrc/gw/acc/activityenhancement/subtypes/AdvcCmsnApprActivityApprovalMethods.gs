package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity
uses com.guidewire.bc.domain.approval.BCApprovalHandler

class AdvcCmsnApprActivityApprovalMethods
    implements IApprovalActivity {

  var _activity : AdvcCmsnApprActivity

  construct(activity : AdvcCmsnApprActivity) {
    _activity = activity
  }

  override function associateRelatedEntities() {
    // Producer -> Actions -> New Comm Payment -> Advance
    var payment = _activity.AdvanceCmsnPayment
    _activity.Producer_Ext = payment.Producer
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return _activity.ApprovalHandler
  }
}