package gw.acc.acct360.accountview

uses gw.api.domain.account.AccountTransfer
uses gw.api.web.payment.WhenModifyingDirectBillMoney
uses java.util.ArrayList
uses org.slf4j.LoggerFactory
uses java.lang.Exception
uses gw.pl.currency.MonetaryAmount
uses pcf.AccountSummaryPopup
uses pcf.Acct360ChargeViewPopup
uses pcf.Acct360InvoiceViewPopup
uses pcf.Acct360ViewReversedPaymentPopup
uses pcf.ModifyDirectBillPaymentPopup
uses pcf.PolicyDetailSummaryPopup
uses typekey.AccountBalanceTxn_ext
uses typekey.BaseMoneyReceived


/**
 * Account 360 Accelerator
 * This class is used for Account Balance Summary Transaction calculation.
 * It contains all the functions necessary to create the entities and
 * present the entities on the screens.
 *
 **/
class AccountBalanceTxnUtil {

  /**
   * Description: This method returns an array of Acct360TranDetail. The array contains account balance summary transactions for an
   * account.
   *
   * @param accObj Account used to search for transactions.
   * @param plcyPeriod Policy Period used to search for transactions.
   * @param policy Policy used to search for transactions
   * @param product Product used to search for transactions
   * @returns Acct360TranDetail[]
   */
  static function createTransactionRecords(accObj: Account, plcyPeriod: PolicyPeriod, policy : Policy, product : LOBCode): Acct360TranDetail[] {
    var returnList = new ArrayList<Acct360TranDetail>()
    var runningTotal = 0bd.ofCurrency(accObj.Currency)
    var runningTotal1 = 0bd.ofCurrency(accObj.Currency)
    var paidAmount = 0bd.ofCurrency(accObj.Currency)
    var dueAmount = 0bd.ofCurrency(accObj.Currency)
    var balAmount = 0bd.ofCurrency(accObj.Currency)
    var acct360Tran : entity.AccountBalanceTxn_ext[]

    if (plcyPeriod != null){
      acct360Tran = accObj.AccountBalanceTxns_Ext.where( \ p -> p.PolicyPeriod == plcyPeriod ).sortBy(\s -> s.CreateTime)
    } else if (policy != null){
      acct360Tran = accObj.AccountBalanceTxns_Ext.where( \ p -> p.PolicyPeriod != null and p.PolicyPeriod.Policy == policy).sortBy(\s -> s.CreateTime)
    } else if (product != null){
      acct360Tran = accObj.AccountBalanceTxns_Ext.where( \ p -> p.PolicyPeriod != null and p.PolicyPeriod.Policy.LOBCode == product ).sortBy(\s -> s.CreateTime)
    } else {
      acct360Tran = accObj.AccountBalanceTxns_Ext.sortBy(\s -> s.CreateTime)
    }


    var newRecord : Acct360TranDetail
    for (eachResult in acct360Tran) {
      newRecord = new Acct360TranDetail()
      newRecord.AcctBalanceTran = eachResult // in case additional detail is needed on the screen.
      newRecord.TransactionDate = eachResult.TransactionDate
      newRecord.EffectiveDate = (eachResult typeis AccountBalanceBillInst_ext)?eachResult.BillingInstruction.EffectiveDate :eachResult.TransactionDate
      newRecord.PolicyNumber = eachResult.PolicyNumber
      newRecord.PolicyPeriod = eachResult.PolicyPeriod
      newRecord.InvoiceNumber = eachResult.InvoiceNumber
      newRecord.Description = eachResult.Description
      newRecord.DueAmount = eachResult.DueAmount
      newRecord.NetAmount = calculateNetAmount(newRecord)
      balAmount = balAmount + eachResult.BalanceAmount
      newRecord.BalanceAmount = (balAmount.IsNegative)?0bd.ofCurrency(accObj.Currency):balAmount
      paidAmount = (eachResult.PaidAmount != null)?eachResult.PaidAmount:0bd.ofCurrency(eachResult.Transactions.first().Currency)
      if (eachResult typeis AccountBalanceItemMoved_ext) {
        if ((eachResult.Transactions.first().Transaction as ChargeBilled).Charge.Amount.IsPositive && eachResult.PaidAmount.IsPositive) {
          dueAmount = eachResult.DueAmount + eachResult.PaidAmount
        } else if (eachResult.Transactions.IsEmpty){
          /* policy tranfer on target account. Set due amount to 0 */
          dueAmount = 0bd.ofCurrency(accObj.Currency)
        } else {
          dueAmount = eachResult.DueAmount  + eachResult.PaidAmount
        }
      } else if (eachResult.DueAmount != null &&
          eachResult typeis AccountBalanceTxnBilled_ext) {
        // only a billed transaction will have a due amount.
        // add the paid amount to it.

        dueAmount = eachResult.DueAmount + eachResult.PaidAmount
      }
      if (eachResult.Subtype == typekey.AccountBalanceTxn_ext.TC_ACCOUNTBALANCETXNBILLED_EXT
            && paidAmount > 0bd.ofCurrency(accObj.Currency) ) {
        // this is a negative invoice set paid amount to zero
        paidAmount = 0bd.ofCurrency(accObj.Currency)
      }
      newRecord.PaidAmount = paidAmount
      newRecord.CheckRef = eachResult.ReferenceNumber
      newRecord.PaymentMethod = eachResult.PaymentMethod
      newRecord.TransactionType = eachResult.Transactions.first().Transaction.Subtype

      if (eachResult typeis AccountBalTxnNonBal_ext) {
        newRecord.NonBalanceTran = true
      } else {
        newRecord.NonBalanceTran = false
      }
      // populate invoice if the transaction is invoicing transaction
      newRecord.Invoice = (eachResult.Transactions.first().Transaction typeis ChargeInvoicingTxn)
          ?(eachResult.Transactions.first().Transaction as ChargeInvoicingTxn).InvoiceItem.Invoice
          :null
      // populate the payment information if the transaction is a payment transaction
      newRecord.Payment = (eachResult.Transactions.first().Transaction typeis ChargePaidFromUnapplied)
          ?(eachResult.Transactions.first().Transaction as ChargePaidFromUnapplied).PaymentItem.BaseDist.BaseMoneyReceived as PaymentMoneyReceived
          :null

      // populate the billing instruction information if the transaction is a billing instruction transaction
      newRecord.BillInstruction = (eachResult.Transactions.first().Transaction typeis InitialChargeTxn)
          ?(eachResult.Transactions.first().Transaction as InitialChargeTxn).Charge.BillingInstruction
          :null

      if (eachResult typeis AccountBalanceItemMoved_ext
          && eachResult.MoveType == AcctBalItemMovedType_Ext.TC_ABTODBPAYMENT) {
        runningTotal = runningTotal.add(paidAmount)
      } else if (eachResult typeis AccountBalanceTxnBilled_ext
            && (dueAmount >= 0bd.ofCurrency(accObj.Currency) )) {
        // only affect the running total for positive billed items. A negative due amount
        // is means they do not owe any money.
        runningTotal = runningTotal.add(dueAmount)
      } else if (eachResult typeis AccountBalanceItemMoved_ext) {
        // negative item moved to a invoice need to include this in the balance
        runningTotal = runningTotal.add(dueAmount)
      } else if (eachResult.Subtype == AccountBalanceTxn_ext.TC_ACCOUNTBALTXNPAYMENT_EXT
                 or eachResult.Subtype == AccountBalanceTxn_ext.TC_ACCOUNTBALTXNWRITEOFF_EXT){
        if (eachResult.DueAmount.IsNotZero) {
          runningTotal = runningTotal.add(eachResult.DueAmount)
        } else {
          // fix for the total balance for the write-off scenario.
          runningTotal1 = runningTotal.add(eachResult.PaidAmount)
          if(runningTotal1.IsNegative){
            runningTotal = runningTotal
          } else {
            runningTotal = runningTotal + runningTotal1
          }
        }
      }

      newRecord.NetAmount = calculateNetAmount(newRecord)
      newRecord.OutstandingBalance =  runningTotal //(runningTotal.IsNegative)?0bd.ofCurrency(accObj.Currency) : runningTotal
      returnList.add(newRecord)

    }
    return returnList.toTypedArray().sortBy(\o -> o.TransactionDate)
  }

  /**
   * Description: This method is used log data when an exception is hit during the
   * main account 360 functions this data will enable debugging of issues.
   *
   * @param acct Account
   * @param polPeriod PolicyPeriod
   * @param exp Exception
   *
   */

  public static function logExceptionData (acct : Account, polPeriod : PolicyPeriod, exp : Exception) {
    var _logger = LoggerFactory.getLogger("APPLICATION")
    var acctString = (acct == null)?"none":acct.DisplayName
    var polString = (polPeriod == null)?"none":polPeriod.DisplayName
    _logger.error("Exception encountered in Account 360 processing. Processing Account: " + acctString
                  + " Policy Period: " + polString )
    _logger.error("Exception encountered in Account 360 processing. Exception: " + exp.StackTraceAsString)
  }

  /*
   * Description: This method will be called in the AB to DB policy bill method change. It will look for any
   * off set transactions that is on the agency bill still and generate the billed transactions for those items to ensure
   * the account balance function is correct.
   *
   * * @param polPeriod PolicyPeriod
   *
   */

  public static function processAgencyBillOffsetItems (polPeriod : PolicyPeriod) {
    /* after the transfer has occured any invoice items left on the agent will be offset items.
     * Collect them.
     */
    var invItemsToOffset = polPeriod.InvoiceItems.where( \ ii -> ii.AgencyBill && ii.Invoice.Planned)
    for (ii in invItemsToOffset) {
      // generate the billed transactions for this item
      ii.onInvoiceBilled()
      if (ii.ReversedInvoiceItem != null && ii.ReversedInvoiceItem.Invoice.Due) {
        ii.doDueTransaction()
      }
    }
  }

  /**
   * Description: This method to create the move item record. It is done before the move action
   * so that we can identify what is the move action later when we have the BC transactions to
   * match to it.
   *
   * @param acctObj Account
   * @param invoiceItems InvoiceItems[]
   * @param moveType AcctBalItemMovedType_ext
   */

  public static function createMoveItemRecord (acctObj : Account, invoiceItems : InvoiceItem[], moveType : AcctBalItemMovedType_Ext ) {
    var newSummaryRecord : AccountBalanceItemMoved_ext

    for (ii in invoiceItems) {
      if (!acctObj.AccountBalanceTxns_Ext.hasMatch(\ab -> ab typeis AccountBalanceItemMoved_ext &&
                                                          ab.New &&
                                                          ii.PolicyPeriod != null &&
                                                          ab.PolicyPeriod == ii.PolicyPeriod &&
                                                          ab.Invoice == ii.Invoice )) {
        newSummaryRecord = new AccountBalanceItemMoved_ext()
        newSummaryRecord.TransactionDate = gw.api.util.DateUtil.currentDate()
        newSummaryRecord.Account = acctObj
        newSummaryRecord.PolicyPeriod = (ii.PolicyPeriod == null) ? null : ii.PolicyPeriod
        newSummaryRecord.MoveType = moveType
        newSummaryRecord.InvoiceItem = ii
        newSummaryRecord.Invoice = ii.Invoice
        newSummaryRecord.BalanceAmount = 0bd.ofCurrency(acctObj.Currency)
        newSummaryRecord.PaidAmount  = 0bd.ofCurrency(acctObj.Currency)
        newSummaryRecord.DueAmount  = 0bd.ofCurrency(acctObj.Currency)
        if (acctObj == null) { // get around issue with integration behavior
          newSummaryRecord.Account = ii.AccountPayer
          ii.AccountPayer.addToAccountBalanceTxns_Ext(newSummaryRecord)
        } else {
          newSummaryRecord.Account = acctObj
          acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
        }
      }
    }
  }

  /**
   * Description: This method to create the policy tranfer record to move the account balance.
   * The balance needs to be reduced on the source account and added to the target account.
   * It acts like a type of Billing Instruction to affect account balances.
   *
   * @param acctTrans AccountTransfer
   */

  public static function createTransferRecord (acctTrans : AccountTransfer) {
    var newSummaryRecord : AccountBalanceItemMoved_ext
    
    for (polTrans in acctTrans.PolicyPeriods.where(\p -> p.PolicyPeriod.DirectBill && p.Transfer )){
      // Outbound transaction on source policy
      newSummaryRecord = new AccountBalanceItemMoved_ext ()
      newSummaryRecord.Account = polTrans.AccountTransfer.FromAccount
      newSummaryRecord.PolicyPeriod = polTrans.PolicyPeriod
      newSummaryRecord.MoveType = AcctBalItemMovedType_Ext.TC_POLICYTRANOUT
      newSummaryRecord.BalanceAmount = (polTrans.PolicyPeriod.UnpaidAmount != null)?polTrans.PolicyPeriod.UnpaidAmount.negate():0bd.ofCurrency(polTrans.PolicyPeriod.Currency)
      newSummaryRecord.InvoiceItem = polTrans.PolicyPeriod.InvoiceItems.first()
      newSummaryRecord.TransactionDate = gw.api.util.DateUtil.currentDate()
      newSummaryRecord.DueAmount = (polTrans.PolicyPeriod.UnpaidAmount != null)?polTrans.PolicyPeriod.UnpaidAmount.negate():0bd.ofCurrency(polTrans.PolicyPeriod.Currency)
      newSummaryRecord.PaidAmount = 0bd.ofCurrency(polTrans.PolicyPeriod.Currency)
      polTrans.PolicyPeriod.Account.addToAccountBalanceTxns_Ext(newSummaryRecord)
      // Inbound transaction on target policy
      polTrans.PolicyPeriod.Account.addToAccountBalanceTxns_Ext(newSummaryRecord)
      newSummaryRecord = new AccountBalanceItemMoved_ext ()
      newSummaryRecord.Account = polTrans.AccountTransfer.ToAccount
      newSummaryRecord.PolicyPeriod = polTrans.PolicyPeriod
      newSummaryRecord.MoveType = AcctBalItemMovedType_Ext.TC_POLICYTRANSFEREDIN
      newSummaryRecord.BalanceAmount = (polTrans.PolicyPeriod.UnpaidAmount != null)?polTrans.PolicyPeriod.UnpaidAmount:0bd.ofCurrency(polTrans.PolicyPeriod.Currency)
      newSummaryRecord.InvoiceItem = polTrans.PolicyPeriod.InvoiceItems.first()
      newSummaryRecord.TransactionDate = gw.api.util.DateUtil.currentDate()
      newSummaryRecord.DueAmount = (polTrans.PolicyPeriod.UnpaidAmount != null)?polTrans.PolicyPeriod.UnpaidAmount:0bd.ofCurrency(polTrans.PolicyPeriod.Currency)
      newSummaryRecord.PaidAmount = 0bd.ofCurrency(polTrans.PolicyPeriod.Currency)
      polTrans.AccountTransfer.ToAccount.addToAccountBalanceTxns_Ext(newSummaryRecord)
    }

  }

  /**
   * Description: This method creates the policy tranfer record to move the account balance.
   * The balance needs to be reduced on the direct bill account.
   * It acts like a type of Billing Instruction to affect account balances.
   *
   * @param polPeriod PolicyPeriod
   */

  public static function createDBToABTransferRecord (polPeriod : PolicyPeriod) {
    var newSummaryRecord : AccountBalanceItemMoved_ext

    newSummaryRecord = new AccountBalanceItemMoved_ext ()
    newSummaryRecord.Account = polPeriod.Account
    newSummaryRecord.PolicyPeriod = polPeriod
    newSummaryRecord.MoveType = AcctBalItemMovedType_Ext.TC_DBTOABTRANSFER
    newSummaryRecord.BalanceAmount = (polPeriod.UnpaidAmount != null)?polPeriod.UnpaidAmount.negate():0bd.ofCurrency(polPeriod.Currency)
    newSummaryRecord.InvoiceItem = polPeriod.InvoiceItems.first()
    newSummaryRecord.TransactionDate = gw.api.util.DateUtil.currentDate()
    newSummaryRecord.DueAmount = (polPeriod.UnpaidAmount != null)?polPeriod.UnpaidAmount.negate():0bd.ofCurrency(polPeriod.Currency)
    newSummaryRecord.PaidAmount = 0bd.ofCurrency(polPeriod.Currency)
    polPeriod.Account.addToAccountBalanceTxns_Ext(newSummaryRecord)
  }

  /**
   * Description: This method creates the policy tranfer record to move the account balance.
   * The balance needs to be increased on the direct bill account.
   * Also need to pull over the payments that occured during the time the policy was an
   * agency bill policy.
   *
   * It acts like a type of Billing Instruction to affect account balances.
   *
   * @param polPeriod PolicyPeriod
   */

  public static function createABToDBTransferRecord (polPeriod : PolicyPeriod) {
    var newSummaryRecord : AccountBalanceItemMoved_ext

    newSummaryRecord = new AccountBalanceItemMoved_ext ()
    newSummaryRecord.Account = polPeriod.Account
    newSummaryRecord.PolicyPeriod = polPeriod
    newSummaryRecord.MoveType = AcctBalItemMovedType_Ext.TC_ABTODBTRANSFER
    newSummaryRecord.BalanceAmount = (polPeriod.UnpaidAmount != null)?polPeriod.UnpaidAmount:0bd.ofCurrency(polPeriod.Currency)
    newSummaryRecord.InvoiceItem = polPeriod.InvoiceItems.first()
    newSummaryRecord.TransactionDate = gw.api.util.DateUtil.currentDate()
    newSummaryRecord.DueAmount = (polPeriod.UnpaidAmount != null)?polPeriod.UnpaidAmount:0bd.ofCurrency(polPeriod.Currency)
    newSummaryRecord.PaidAmount = 0bd.ofCurrency(polPeriod.Currency)
    polPeriod.Account.addToAccountBalanceTxns_Ext(newSummaryRecord)

    /* Need to pull the payments received while the policy was into the account 360 entries
     * This will ensure the balance is correct. Create a move item entry and calculate the
     * amount paid or written off and create and populate the paid amount field.
     */

    var lastTransfer = polPeriod.Account.AccountBalanceTxns_Ext.lastWhere(\ab -> ab typeis AccountBalanceItemMoved_ext &&
        ab.MoveType == AcctBalItemMovedType_Ext.TC_DBTOABTRANSFER)
    var allInvRelatedItems : InvoiceItem[]
    if (lastTransfer == null) {
      allInvRelatedItems = polPeriod.InvoiceItems.where(\ii -> ii.ActivePaymentItems.hasMatch(\di -> di.BaseDist typeis AgencyCyclePayment ) )
    } else {
      allInvRelatedItems = polPeriod.InvoiceItems.where(\ii -> ii.ActivePaymentItems.hasMatch(\di -> di.BaseDist typeis AgencyCyclePayment
          && di.BaseDist.CreateTime.afterOrEqual(lastTransfer.CreateTime)) )
    }

    if (allInvRelatedItems.Count > 0) {
      newSummaryRecord = new AccountBalanceItemMoved_ext()
      newSummaryRecord.Account = polPeriod.Account
      newSummaryRecord.PolicyPeriod = polPeriod
      newSummaryRecord.MoveType = AcctBalItemMovedType_Ext.TC_ABTODBPAYMENT
      newSummaryRecord.BalanceAmount = 0bd.ofCurrency(polPeriod.Currency)
      newSummaryRecord.InvoiceItem = polPeriod.InvoiceItems.first()
      newSummaryRecord.TransactionDate = gw.api.util.DateUtil.currentDate()
      newSummaryRecord.DueAmount = 0bd.ofCurrency(polPeriod.Currency)
      var paidAmount = 0bd.ofCurrency(polPeriod.Currency)
      for (ii in allInvRelatedItems) {
        paidAmount += ii.ActivePaymentItems.where(\di -> di.BaseDist typeis AgencyCyclePayment).sum(\p -> p.GrossAmountToApply)
      }

      newSummaryRecord.PaidAmount = (paidAmount).negate()

      polPeriod.Account.addToAccountBalanceTxns_Ext(newSummaryRecord)
    }
  }


  private static function calculateNetAmount (tran : Acct360TranDetail) : MonetaryAmount {
    if (tran.DueAmount != null && tran.PaidAmount != null && tran.DueAmount.IsNotZero && tran.PaidAmount.IsNotZero) {
      return tran.PaidAmount
    } else if (tran.DueAmount != null && tran.DueAmount.IsNotZero) {
      return tran.DueAmount
    } else if (tran.AcctBalanceTran typeis AccountBalanceBillInst_ext) {
      return tran.AcctBalanceTran.BalanceAmount
    }
    else {
      return tran.PaidAmount
    }
  }

  /**
   * Description: This function is used to support the account balance screen and the navigation
   * functions on that screen.
   *
   * @param account Account
   * @param trans acc.acct360.accountview.Acct360TranDetail
   *
   */

  public static function navigate360(account : Account, trans: Acct360TranDetail) {
    if (trans.TransactionType == null && !trans.NonBalanceTran) {
      // means it is a policy transfer open up the policy period
      Acct360ChargeViewPopup.push(trans.PolicyPeriod)
    } else if (trans.TransactionType == typekey.Transaction.TC_CHARGEPAIDFROMACCOUNT || trans.TransactionType == typekey.Transaction.TC_DIRECTBILLMONEYRECEIVEDTXN) {
      if (!trans.Payment.Reversed && trans.Payment.Subtype != BaseMoneyReceived.TC_ZERODOLLARREVERSAL) {
        ModifyDirectBillPaymentPopup.push(account, trans.Payment as DirectBillMoneyRcvd, WhenModifyingDirectBillMoney.EditDistribution)
      } else {
        Acct360ViewReversedPaymentPopup.push(trans.Payment as DirectBillMoneyRcvd)
      }
    } else if (trans.TransactionType == typekey.Transaction.TC_CHARGEBILLED) {
      Acct360InvoiceViewPopup.push(trans.Invoice as AccountInvoice)
    } else if (trans.TransactionType == typekey.Transaction.TC_INITIALCHARGETXN) {
      Acct360ChargeViewPopup.push(trans.BillInstruction)
    } else if (trans.PolicyPeriod != null) {
      PolicyDetailSummaryPopup.push(trans.PolicyPeriod)
    } else {
      AccountSummaryPopup.push(account)
    }
  }
}