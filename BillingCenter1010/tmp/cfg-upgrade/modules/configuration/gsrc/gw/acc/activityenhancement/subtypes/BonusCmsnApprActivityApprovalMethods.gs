package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity
uses com.guidewire.bc.domain.approval.BCApprovalHandler

class BonusCmsnApprActivityApprovalMethods
    implements IApprovalActivity {

  var _activity : BonusCmsnApprActivity

  construct(activity : BonusCmsnApprActivity) {
    _activity = activity
  }

  override function associateRelatedEntities() {
    // Producer -> Actions -> New Comm Payment -> Bonus
    var payment = _activity.BonusCmsnPayment
    _activity.Producer_Ext = payment.Producer
   /* even though the bonus could be associated with a commission on a specific
    * policy, the activity should not be related with the policy since there is
    * no policy money involved
    */
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return _activity.BonusCmsnPayment.ApprovalHandler
  }
}