package gw.acc.activityenhancement.subtypes

uses com.guidewire.bc.domain.approval.BCApprovalHandler

class WriteoffApprActivityApprovalMethods
    extends WriteoffActivityApprovalMethods <WriteoffApprActivity> {

  construct(activity: WriteoffApprActivity) {
    super(activity)
  }

  override property get Writeoff() : Writeoff {
    return Activity.Writeoff
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return Activity.ApprovalHandler
  }
}