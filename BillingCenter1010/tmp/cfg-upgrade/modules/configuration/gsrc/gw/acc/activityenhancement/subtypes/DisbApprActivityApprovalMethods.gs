package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity
uses com.guidewire.bc.domain.approval.BCApprovalHandler

class DisbApprActivityApprovalMethods implements IApprovalActivity {

  var _activity : DisbApprActivity

  construct(activity: DisbApprActivity) {
    _activity = activity
  }

  override function associateRelatedEntities() {
    var disbursementTarget = _activity.Disbursement.DisbursementTarget
    if (disbursementTarget typeis Account) {
      // Account -> Actions -> New Transaction -> Disbursement
      _activity.Account = disbursementTarget
    } else if (disbursementTarget typeis Producer) {
      // Producer -> Actions -> Disbursement
      // Producer -> Actions -> Disburse Credit Items
      _activity.Producer_Ext = disbursementTarget
    } else if (disbursementTarget typeis PolicyPeriod) {
      // policy-level payments not fully supported yet
      _activity.PolicyPeriod = disbursementTarget
      _activity.Account = disbursementTarget.Account
    }
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return _activity.Disbursement.ApprovalHandler
  }
}