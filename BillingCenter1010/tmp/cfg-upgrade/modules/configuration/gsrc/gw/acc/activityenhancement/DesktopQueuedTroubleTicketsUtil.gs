package gw.acc.activityenhancement

uses gw.api.assignment.Assignee
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query

class DesktopQueuedTroubleTicketsUtil {

  public static function assignTroubleTicketsToMe(troubleTickets : TroubleTicket[]) : void {
    var bundle = gw.transaction.Transaction.getCurrent()
    for(tt in troubleTickets)
    {
      tt.assign(tt.AssignedGroup, User.util.CurrentUser)
    }
    bundle.commit()
  }

  public static function getQueues() : AssignableQueue[] {
    var queues = AssignableQueue.finder.findVisibleQueuesForUser(User.util.CurrentUser) as IQueryBeanResult<AssignableQueue>
    return queues.toTypedArray()
  }

  public static function getTotalNumberOfTroubleTicketsForUser() : int{
    return getQueues().sum(\q -> q.TroubleTickets_Ext.Count)
  }

  public static function assignTroubleTicket(assignee : Assignee, tt : TroubleTicket) : void {
    // check if this is a queue. Queue has a pattern of queue - group
    var index = assignee.toString().indexOf("-")
    if (index > 0)
    {
      var group = assignee.toString().substring(index + 2)
      var queue = assignee.toString().substring (0, index - 1)
      var grp = Query.make(Group).compare(Group#Name, Equals, group).select().first()
      if (grp != null)
      {
        tt.assignGroup(grp)
        var que = tt.AssignedGroup.getQueue(queue)
        if (que != null)
        {
          tt.assignTo_Ext(que)
          return
        }
      }
    }
    // not a a queue default to the OOTB assignment
    assignee.assignToThis(tt)
    return
  }

  public static function assignTroubleTickets(popup : gw.api.assignment.AssignmentPopup, troubleTickets : TroubleTicket[]) {
      troubleTickets.each(\tt -> assignTroubleTicket(popup.Picker.Selection, tt))
  }

  public static function assignTroubleTicketsInNewBundle(popup : gw.api.assignment.AssignmentPopup, troubletickets : TroubleTicket[]){
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        troubletickets.each(\ act -> {
          act = bundle.add(act)
          assignTroubleTicket(popup.SelectedFromList, act)
        })
    })
  }
}