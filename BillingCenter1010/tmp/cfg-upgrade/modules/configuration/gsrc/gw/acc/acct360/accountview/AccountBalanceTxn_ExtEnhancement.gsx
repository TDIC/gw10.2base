package gw.acc.acct360.accountview

uses gw.api.locale.DisplayKey
uses typekey.*
uses typekey.AccountBalanceTxn_ext

/**
 * Account 360 Accelerator
 * This class defines enhancements used to support the display of the account 360 screen.
 *
 **/
enhancement AccountBalanceTxn_ExtEnhancement : entity.AccountBalanceTxn_ext {

  /*Added this property to get the Invoice number for AccountBalanceTxnBilled_ext row in Account360 screen*/
  property get InvoiceNumber():String{
    if(this.Subtype==typekey.AccountBalanceTxn_ext.TC_ACCOUNTBALANCETXNBILLED_EXT){
      return (this as AccountBalanceTxnBilled_ext).Invoice.InvoiceNumber
    }else{
      return null
    }
  }

  /*Added this property to get the Payment Method for AccountBalTxnPayment_Ext row in Account360 screen*/
  property get PaymentMethod():PaymentMethod{
    if(this.Subtype== AccountBalanceTxn_ext.TC_ACCOUNTBALTXNPAYMENT_EXT){
      var acctBalPayObj=this as AccountBalTxnPayment_ext
      var tranObj=acctBalPayObj.Transactions.first().Transaction
      var directBillMoneyReceivedObj=(tranObj as ChargePaidFromUnapplied).PaymentItem.BaseDist.BaseMoneyReceived as PaymentMoneyReceived
      if(directBillMoneyReceivedObj!=null){
        var paymentMethod=directBillMoneyReceivedObj.PaymentInstrument.PaymentMethod
        return paymentMethod
      }else{
        return null
      }
    }else{
      return null
    }
  }

  /* Added this property to get the Description for a row in Account360 screen*/
  property get Description():String{
    if(this.Subtype==typekey.AccountBalanceTxn_ext.TC_ACCOUNTBALANCETXNBILLED_EXT){
      if(this.PolicyPeriod!=null){
        return DisplayKey.get("Accelerator.Acc360.AccountView.billed", this.InvoiceNumber)
      }else{
        return DisplayKey.get("Accelerator.Acc360.AccountView.billed", this.InvoiceNumber)+ " - " +(this.Transactions.first().Transaction as ChargeBilled).Charge.ChargePattern.ChargeName
      }
    }else if(this.Subtype== AccountBalanceTxn_ext.TC_ACCOUNTBALTXNPAYMENT_EXT){
      var isReversalPayment=!this.Transactions.hasMatch( \ t -> !t.Transaction.Reversal)
      var isModifiedPayment=this.Transactions.hasMatch( \ t -> (t.Transaction as ChargePaidFromUnapplied).PaymentItem.Modifying)
      if(!isReversalPayment){
        if(this.PolicyPeriod!=null){
          return (isModifiedPayment)?DisplayKey.get("Accelerator.Acc360.AccountView.payment.modified", this.ReferenceNumber):DisplayKey.get("Accelerator.Acc360.AccountView.payment.made", this.ReferenceNumber)
        }else{
          return DisplayKey.get("Accelerator.Acc360.AccountView.payment.made", this.ReferenceNumber)+" - "+ (this.Transactions.first().Transaction as ChargePaidFromUnapplied).Charge.ChargePattern.ChargeName
        }
      }else{
        return DisplayKey.get("Accelerator.Acc360.AccountView.payment.reversed", this.ReferenceNumber)
      }
    } else if(this typeis AccountBalTxnWriteoff_ext){
      var writeOffReason = this.WriteOff.Reason.DisplayName
      if(this.Transactions.first().Transaction.Reversal) {
        return DisplayKey.get("Accelerator.Acc360.AccountView.writeoff.reversal", writeOffReason)
      } else {
        return DisplayKey.get("Accelerator.Acc360.AccountView.writeoff.made", writeOffReason)
      }
    } else if (this typeis AccountBalanceItemMoved_ext) {
      return this.MoveType.Description
    } else if (this typeis AccountBalanceBillInst_ext) {
      if (this.BillingInstruction typeis ReversalBillingInstruction) {
        return this.BillingInstruction.Charges.first().DisplayName
      } else if (this.BillingInstruction typeis AcctBillingInstruction) {
        return DisplayKey.get("Web.InvoiceItemsLV.AccountCharge")
      } else {
        return this.BillingInstruction.DisplayName
      }
    } else if (this typeis AccountBalTxnNonBal_ext) {
      return this.TransactionDescription
    }
    return null
  }


  /* Added this property to get the Policy number for a row in Account360 screen*/
  property get PolicyNumber():String{
    if (this.PolicyPeriod != null) {
      return this.PolicyPeriod.DisplayName
    } else if (this typeis AccountBalTxnNonBal_ext) {
      return DisplayKey.get("Accelerator.Acc360.AccountView.NonBalance.AccountNote")
    } else {
      return DisplayKey.get("Web.InvoiceItemsLV.AccountCharge")
    }
  }

}
