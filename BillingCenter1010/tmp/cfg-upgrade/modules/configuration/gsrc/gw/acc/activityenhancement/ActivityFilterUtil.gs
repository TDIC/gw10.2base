package gw.acc.activityenhancement

uses com.guidewire.pl.system.filters.BeanBasedQueryFilter
uses com.guidewire.pl.system.util.DateTimeUtil
uses gw.api.filters.StandardQueryFilter
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.api.filters.TypeKeyFilterSet
uses gw.api.util.CoreFilters
uses gw.api.database.Query
uses gw.plugin.util.CurrentUserUtil

class ActivityFilterUtil {
  
  private construct() {
  }
  
  /**
   * Get filter set for activity status
   * The first status 'Open' will be pre-filtered
   */

  static property get ActivityStatusFilterOptions() : StandardQueryFilter[]
  {
    var filterOptions = new TypeKeyFilterSet( Activity.Type.TypeInfo.getProperty( "Status" ) ).getFilterOptions()
    // reorder array, so ALL filter is last
    var result = new StandardQueryFilter[filterOptions.Count]
    result[filterOptions.Count-1] = filterOptions[0]
    for (i in 0..filterOptions.Count-2) {
      result[i] = filterOptions[i + 1]
    }
    return result
  }

  /**
   * Get filter for all open activities.
   */
  static final var _openFilter : ActivityFilter as AllOpenActivitiesFilter =
      new ActivityFilter(DisplayKey.get("Java.ActivityFilterSet.AllOpen"), ActivityStatus.TC_OPEN, null, null, null, null)

  /**
   * Get filter for all activities opened this week.
   */
  static final var _openedThisWeekFilter : ActivityFilter as AllActivitiesOpenedThisWeekFilter =
      new ActivityFilter( DisplayKey.get("Java.ActivityFilterSet.OpenedThisWeek"), null, null, null, DateTimeUtil.getWeekStartDate(), null )

  /**
   * Get filter for all activities completed in last 30 days.
   */
  static final var _completedInLast30DaysFilter : ActivityFilter as AllActivitiesCompletedInLast30DaysFilter =
      new ActivityFilter( DisplayKey.get("Java.ActivityFilterSet.CompletedInLastNDays", (30)), null, null, null, null, DateUtil.currentDate().addMonths( -1 ) )

  /**
   * Get filter for all urgent open activities.
   */
  static final var _openUrgentFilter : ActivityFilter as AllOpenUrgentActivitiesFilter =
      new ActivityFilter( DisplayKey.get("Java.ActivityFilterSet.OpenUrgent"), ActivityStatus.TC_OPEN, Priority.TC_URGENT, null, null, null )

  /**
   * Get filter for all escalated activities.
   */
  static final var _escalatedFilter : ActivityFilter as AllEscalatedActivitiesFilter =
      new ActivityFilter( DisplayKey.get("Java.ActivityFilterSet.EscalatedFilter"), null, null, true, null, null )

  /**
   * Get filter set for activities that works with array in Account/Policy/Producer -> Activities
   * The first filter 'AllOpen' will be pre-filtered
   */
  static final var _filterOptions : BeanBasedQueryFilter[] as ActivityFilterOptions = {
      AllOpenActivitiesFilter,
      AllActivitiesOpenedThisWeekFilter,
      AllActivitiesCompletedInLast30DaysFilter,
      AllOpenUrgentActivitiesFilter,
      AllEscalatedActivitiesFilter,
      CoreFilters.ALL
  }

  /**
   * Get filter set for activity patterns that works with array in Account/Policy/Producer -> Activities
   * Similar function gw.activity.ActivityPatternFilterSet().FilterOptions works only for queries
   */
  static property get ActivityPatternsFilterOptions() : BeanBasedQueryFilter[]
  {
    var options = Query.make(ActivityPattern).select()
    var filter = new BeanBasedQueryFilter[options.Count + 1]
    filter[0] = CoreFilters.ALL
    for (option in options index i) {
      filter[i+1] = new ActivityPatternFilter(option)
    }
    return filter
  }
  
  /**
   * Get filter set for activity subtypes used in filter for Desktop -> My Activities
   */
  static final var _desktopActivityTypes = {
      typekey.Activity.TC_ACCTINACTIVEACTIVITY,
      typekey.Activity.TC_ADVCCMSNAPPRACTIVITY,
      typekey.Activity.TC_BONUSCMSNAPPRACTIVITY,
      typekey.Activity.TC_CHRGRVSLAPPRACTIVITY,
      typekey.Activity.TC_CREDITAPPRACTIVITY,
      typekey.Activity.TC_CREDITREVERSALAPPRACTIVITY,
      typekey.Activity.TC_DISBAPPRACTIVITY,
      typekey.Activity.TC_FUNDSXFERAPPRACTIVITY,
      typekey.Activity.TC_FUNDSTRANSFERREVERSALAPPROVALACTIVITY,
      typekey.Activity.TC_NEGWROFAPPRACTIVITY,
      typekey.Activity.TC_NEGWOFFREVAPPRACTIVITY,
      typekey.Activity.TC_WRITEOFFAPPRACTIVITY,
      typekey.Activity.TC_WTOFFREVAPPRACTIVITY
  }
  
  static property get DesktopActivitySubtypesFilterOptions() : StandardQueryFilter[]
  {
    return new TypeKeyFilterSet( Activity.Type.TypeInfo.getProperty( "Subtype" ) ).getFilterOptions(
      _desktopActivityTypes, true)
  }

  static property get UsersActivitiesList() : User[]{
    var userList = new ArrayList<User>()
    var groupUsers = Arrays.asList(User.util.CurrentUser.GroupUsers)
    var groups = groupUsers*.Group
    for(group in groups) {
       if(group.ChildGroups.Count > 0){
          var childGrps = group.ChildGroups.toList()
          for (childGrp in childGrps){
            childGrp.Members.each(\chldgrpusr -> {
              var member = chldgrpusr.User
              if(!userList.contains(member)){
                userList.add(member)
              }
            })
          }
       }
    }
    return userList.toTypedArray()
  }

  /**
   * Get filter set for activity sybtypes used in filter for Account -> Activities
   */
  static final var _accountActivitySubtypes = {
      typekey.Activity.TC_ACCTINACTIVEACTIVITY,
      typekey.Activity.TC_CHRGRVSLAPPRACTIVITY,
      typekey.Activity.TC_CREDITAPPRACTIVITY,
      typekey.Activity.TC_CREDITREVERSALAPPRACTIVITY,
      typekey.Activity.TC_DISBAPPRACTIVITY,
      typekey.Activity.TC_FUNDSXFERAPPRACTIVITY,
      typekey.Activity.TC_FUNDSTRANSFERREVERSALAPPROVALACTIVITY,
      typekey.Activity.TC_NEGWROFAPPRACTIVITY,
      typekey.Activity.TC_NEGWOFFREVAPPRACTIVITY,
      typekey.Activity.TC_WRITEOFFAPPRACTIVITY,
      typekey.Activity.TC_WTOFFREVAPPRACTIVITY
  }

  static property get AccountActivitySubtypesFilterOptions() : StandardQueryFilter[]
  {
    return new TypeKeyFilterSet( Activity.Type.TypeInfo.getProperty( "Subtype" ) ).getFilterOptions(
      _accountActivitySubtypes, true)
  }

  /**
   * Get filter set for activity sybtypes used in filter for Policy -> Activities
   */
  static final var _policyActivitySubtypes = {
      typekey.Activity.TC_CHRGRVSLAPPRACTIVITY,
      typekey.Activity.TC_WRITEOFFAPPRACTIVITY,
      typekey.Activity.TC_WTOFFREVAPPRACTIVITY
  }

  static property get PolicyActivitySubtypesFilterOptions() : StandardQueryFilter[]
  {
    return new TypeKeyFilterSet( Activity.Type.TypeInfo.getProperty( "Subtype" ) ).getFilterOptions(
      _policyActivitySubtypes, true)
  }

  /**
   * Get filter set for activity sybtypes used in filter for Producer -> Activities
   */
  static final var _producerActivitySubtypes = {
      typekey.Activity.TC_ADVCCMSNAPPRACTIVITY,
      typekey.Activity.TC_BONUSCMSNAPPRACTIVITY,
      typekey.Activity.TC_DISBAPPRACTIVITY,
      typekey.Activity.TC_FUNDSXFERAPPRACTIVITY,
      typekey.Activity.TC_FUNDSTRANSFERREVERSALAPPROVALACTIVITY,
      typekey.Activity.TC_NEGWROFAPPRACTIVITY,
      typekey.Activity.TC_NEGWOFFREVAPPRACTIVITY,
      typekey.Activity.TC_WRITEOFFAPPRACTIVITY,
      typekey.Activity.TC_WTOFFREVAPPRACTIVITY
  }

  static property get ProducerActivitySubtypesFilterOptions() : StandardQueryFilter[]
  {
    return new TypeKeyFilterSet( Activity.Type.TypeInfo.getProperty( "Subtype" ) ).getFilterOptions(
      _producerActivitySubtypes, true)
  }

  /**
   * Filter value range for Activity Subtypes in Search -> Activities
   * This function returns the possible activity subtypes from the given values
   */
  static function filterActivitySubtypes(values : typekey.Activity[]) : java.util.List<typekey.Activity> {
    return _desktopActivityTypes.intersect(values.toSet()).toList()
  }
}
