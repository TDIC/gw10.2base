package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity
uses com.guidewire.bc.domain.approval.BCApprovalHandler

class ChrgRvslApprActivityApprovalMethods
    implements IApprovalActivity {

  var _activity : ChrgRvslApprActivity

  construct(activity : ChrgRvslApprActivity) {
    _activity = activity
  }

  override function associateRelatedEntities() {
    // Account -> Actions -> New Transaction -> Charge Reversal
    // Producer charges not supported yet by BC V7.0
    var charge = _activity.ChargeReversal.Charge
    _activity.Account = charge.getOwnerAccount()
    _activity.PolicyPeriod = charge.PolicyPeriod
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return _activity.ApprovalHandler
  }
}