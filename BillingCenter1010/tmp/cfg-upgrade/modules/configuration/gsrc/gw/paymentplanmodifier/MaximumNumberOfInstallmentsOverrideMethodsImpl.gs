package gw.paymentplanmodifier

uses gw.api.domain.invoice.PaymentPlanModifierMethods

@Export
class MaximumNumberOfInstallmentsOverrideMethodsImpl implements PaymentPlanModifierMethods{

  private var _maximumNumberOfInstallmentsOverride : MaximumNumberOfInstallmentsOverride;

  construct (maximumNumberOfInstallmentsOverride : MaximumNumberOfInstallmentsOverride) {
    _maximumNumberOfInstallmentsOverride = maximumNumberOfInstallmentsOverride
  }

  override function modify(paymentPlan : PaymentPlan) {
    paymentPlan.MaximumNumberOfInstallments = _maximumNumberOfInstallmentsOverride.MaximumNumberOfInstallments
  }
}
