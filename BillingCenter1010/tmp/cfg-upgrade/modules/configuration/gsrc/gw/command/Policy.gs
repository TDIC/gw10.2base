package gw.command

uses gw.command.Argument
uses com.guidewire.pl.system.dependency.PLDependencies
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.databuilder.AccountBuilder
uses gw.api.databuilder.AgencyBillPaymentFixtureBuilder
uses gw.api.databuilder.AuditBuilder
uses gw.api.databuilder.CancellationBuilder
uses gw.api.databuilder.CommissionPlanBuilder
uses gw.api.databuilder.DirectBillPaymentFixtureBuilder
uses gw.api.databuilder.GeneralBillingInstructionBuilder
uses gw.api.databuilder.PolicyChangeBillingInstructionBuilder
uses gw.api.databuilder.PolicyPeriodBuilder
uses gw.api.databuilder.PremiumReportBIBuilder
uses gw.api.databuilder.ProducerBuilder
uses gw.api.databuilder.ReinstatementBillingInstructionBuilder
uses gw.api.databuilder.RenewalBillingInstructionBuilder
uses gw.api.databuilder.RewriteBillingInstructionBuilder
uses gw.api.databuilder.SuppressDownPaymentBuilder
uses gw.api.domain.accounting.ChargeUtil
uses gw.api.path.Paths
uses gw.api.system.BCConfigParameters
uses gw.api.util.CurrencyUtil
uses gw.api.util.DateUtil
uses gw.api.web.invoice.InvoicingOverrider
uses gw.bc.archive.BCArchiveRunCommandUtil
uses gw.workqueue.WorkQueueTestUtil

@Export
class Policy extends BCBaseCommand {
  
  construct() {
    super()
  }
  
  function printInfo() : String{
    var period = getCurrentPolicyPeriod()
    return new StringBuilder()
      .append("OverridingPayerAccount: ").append(period.OverridingPayerAccount)
      .append("\nInvoice Stream: ").append(period.OverridingInvoiceStream)
      .toString()
  }

  @Argument("OverridingPayerAccount", Query.make(Account).select(), \ a -> (a as Account).AccountNumber)
  @Argument("InvoiceStreamCode", Query.make(InvoiceStream).select(), \ i -> (i as InvoiceStream).PublicID)
  function edit() : String{
    var policyPeriod = getCurrentPolicyPeriod()
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      policyPeriod = bundle.add(policyPeriod)
      var overridingPayerAccount =
        Query.make(Account).compare("AccountNumber", Equals, getArgumentAsString("OverridingPayerAccount")).select().AtMostOneRow
      var overridingInvoiceStream =
        Query.make(InvoiceStream).compare("PublicID", Equals, getArgumentAsString("InvoiceStreamCode")).select().AtMostOneRow
      policyPeriod.updateWith(new InvoicingOverrider()
        .withOverridingPayerAccount(overridingPayerAccount)
        .withOverridingInvoiceStream(overridingInvoiceStream))           
    })
    return printInfo()
  }
  
  function asExpired(){
    var policyPeriod = new PolicyPeriodBuilder()
            .onDefaultAccount()
            .withExpirationDate(Date.Yesterday)
            .createAndCommit()
    pcf.PolicyDetailSummary.go(policyPeriod)
  }
  
  function withNegativeChange() {
    var account = new AccountBuilder().create()
    var producer = new ProducerBuilder()
      .withDefaultAgencyBillPlan()
      .withDefaultProducerCodeAndCommissionPlan()
      .create()
    var policyPeriod = new PolicyPeriodBuilder()
      .onAccount(account)  
      .asAgencyBill()
      .withPrimaryProducerCode(producer.ProducerCodes[0])
      .withPremiumWithDepositAndInstallments(1000)
      .createAndCommit()
    new PolicyChangeBillingInstructionBuilder()
      .onPolicyPeriod(policyPeriod)
      .addChargePremium(-(300bd).ofCurrency(account.Currency))
      .execute()
      .createAndCommit()
    pcf.PolicyDetailSummary.go(policyPeriod)
  }
  
   function sendPremiumReportBIToPolicy() {
     var policyPeriod = getCurrentPolicyPeriod()
     gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
     new PremiumReportBIBuilder()
            .withModificationDate(DateUtil.currentDate())
            .withPeriodEndDate(DateUtil.currentDate())
            .withPeriodStartDate(DateUtil.currentDate())
            .addChargePremium(500bd.ofCurrency(policyPeriod.Currency))
            .withPolicyPeriod(policyPeriod)
            .execute()
            .create(bundle)
    })
  }

  function sendPremiumReportBIToPolicyWithPaymentReceived(){
     var policyPeriod = getCurrentPolicyPeriod()
     gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
                  
     new PremiumReportBIBuilder()
            .withModificationDate(DateUtil.currentDate())
            .withPeriodEndDate(DateUtil.currentDate())
            .withPeriodStartDate(DateUtil.currentDate())
            .addChargePremium(500bd.ofCurrency(policyPeriod.Currency))
            .withPolicyPeriod(policyPeriod)
            .paymentWasReceived()
            .execute()
            .create(bundle)
    })
  }
     
  function sendFinalAuditBIToPolicy(){
    var policyPeriod = getCurrentPolicyPeriod()
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      
    var finalAuditBI = new AuditBuilder()
      .finalAudit()
      .totalPremium()
      .withAuditPolicyPeriod(policyPeriod)
      .withAuditDate(DateUtil.addMonths(DateUtil.currentDate(), 1))
      .addChargePremium(2100bd.ofCurrency(policyPeriod.Currency))
      .addChargeTaxes(68bd.ofCurrency(policyPeriod.Currency))
      .create(bundle)
      
    finalAuditBI.execute()
   })
  }
  
  @Argument("Premium", "-1000")
  function cancelThis(){
    var policyPeriod = getCurrentPolicyPeriod()
    var premiumAmt = getArgumentAsMonetaryAmount("Premium", policyPeriod.Currency)
     gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
   
      new CancellationBuilder()
      .onPolicyPeriod(policyPeriod)
      .withCancellationDate(currentDate())
      .addChargePremium(premiumAmt)
      .withReason("By Run command")
      .execute()
      .create(bundle)
    })
  }

  @Argument("Amount1", "-1000")
  @Argument("ChargeCode1", "Premium")
  @Argument("Amount2", "-120")
  @Argument("ChargeCode2", "Taxes")
  function cancelThisWith2Charges() : String {
    var policyPeriod = getCurrentPolicyPeriod()

    var chargeCode1 = getArgumentAsString("ChargeCode1")
    var chargeCode2 = getArgumentAsString("ChargeCode2")
    var chargeAmount1 = getArgumentAsMonetaryAmount("Amount1", policyPeriod.Currency)
    var chargeAmount2 = getArgumentAsMonetaryAmount("Amount2", policyPeriod.Currency)
    var chargePattern1 = ChargeUtil.getChargePatternByCode(chargeCode1)
    var chargePattern2 = ChargeUtil.getChargePatternByCode(chargeCode2)

    if(chargePattern1 == null) return "Unknown charge pattern " + chargeCode1
    if(chargePattern2 == null) return "Unknown charge pattern " + chargeCode2

    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      new CancellationBuilder()
          .onPolicyPeriod(policyPeriod)
          .withCancellationDate(currentDate())
          .addCharge(chargeAmount1, chargePattern1)
          .addCharge(chargeAmount2, chargePattern2)
          .withReason("By Run command")
          .execute()
          .create(bundle)
    })

    return "Policy cancelled"
  }

  function closeThis() {
    var policyPeriod = closeOrArchivePolicy(getCurrentPolicyPeriod(), false)

    if (!policyPeriod.Closed) {
      var s = "Failed to close policy period. "
      for (var e in policyPeriod.CloseErrorReasons) {
        s = s + e + ", "
      }
      displayMessage(s)
    } else {
      displayMessage("Successfully closed policy period")
    }
  }

  function archiveThis() {
    var policyPeriod = closeOrArchivePolicy(getCurrentPolicyPeriod(), true)

    if (policyPeriod.getArchiveState() != ArchiveState.TC_ARCHIVED) {
      var message = "Failed to archive policy period"
      if (policyPeriod.ArchiveFailure != null) {
        message = message + " : Archive Failure " + policyPeriod.ArchiveFailure
      }
      if (policyPeriod.ExcludeReason != null) {
        message = message + " : Exclude Reason " + policyPeriod.ExcludeReason
      }
      displayMessage(message)
    } else {
      displayMessage("Successfully archived policy period")
    }
  }

  function retrieveThis(): String {
    var policyPeriod = getCurrentPolicyPeriod()
    if (policyPeriod.ArchiveState != TC_ARCHIVED) {
      return "Cannot retrieve a policy period unless it has been archived"
    }
    try {
      BCArchiveRunCommandUtil.retrieve(policyPeriod)
      return "Retrieved policy period successfully"
    } catch (e: Exception) {
      e.printStackTrace();
      return "Could not retrieve policy period: " + e
    }
  }

  @Argument("Premium", "1000")
  function reinstateThis() : String {
    var policyPeriod = getCurrentPolicyPeriod()
    if(not policyPeriod.Canceled or policyPeriod.Archived){
      return "A policy must be cancelled and not archived in order for it to be reinstated"
    }

    var premiumAmt = getArgumentAsMonetaryAmount("Premium", policyPeriod.Currency)
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {

      new ReinstatementBillingInstructionBuilder()
      .onPolicyPeriod(policyPeriod)
      .withReinstatementDate(currentDate())
      .addChargePremium(premiumAmt)
      .execute()
      .create(bundle)
    })
    return "Policy ${policyPeriod.PolicyNumber} reinstated"
  }

  @Argument("Amount1", "1000")
  @Argument("ChargeCode1", "Premium")
  @Argument("Amount2", "120")
  @Argument("ChargeCode2", "Taxes")
  function reinstateThisWith2Charges() : String {
    var policyPeriod = getCurrentPolicyPeriod()

    var chargeCode1 = getArgumentAsString("ChargeCode1")
    var chargeCode2 = getArgumentAsString("ChargeCode2")
    var chargeAmount1 = getArgumentAsMonetaryAmount("Amount1", policyPeriod.Currency)
    var chargeAmount2 = getArgumentAsMonetaryAmount("Amount2", policyPeriod.Currency)
    var chargePattern1 = ChargeUtil.getChargePatternByCode(chargeCode1)
    var chargePattern2 = ChargeUtil.getChargePatternByCode(chargeCode2)

    if(chargePattern1 == null) return "Unknown charge pattern " + chargeCode1
    if(chargePattern2 == null) return "Unknown charge pattern " + chargeCode2

    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      new ReinstatementBillingInstructionBuilder()
          .onPolicyPeriod(policyPeriod)
          .withReinstatementDate(currentDate())
          .addCharge(chargeAmount1, chargePattern1)
          .addCharge(chargeAmount2, chargePattern2)
          .execute()
          .create(bundle)
    })
    return "Policy reinstated"
  }

  function cancelThisWithUnbilledHold(){
    var policyPeriod = getCurrentPolicyPeriod()
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      new CancellationBuilder()
      .onPolicyPeriod(policyPeriod)
      .withCancellationDate(currentDate())
      .withReason("By Run command")
      .withSpecialHandling(TC_HOLDFORAUDITALL)
      .execute()
      .create(bundle)
    })
  }

  function flatCancelThis(){
    var policyPeriod = getCurrentPolicyPeriod()
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      new CancellationBuilder()
      .onPolicyPeriod(policyPeriod)
      .withCancellationDate(currentDate())
      .addChargePremium((-1000bd).ofCurrency(policyPeriod.Currency))
      .withReason("By Run command")
      .withType(CancellationType.TC_FLAT)
      .execute()
      .create(bundle)
    })
  }

  function addChargeWithNoDownPayment(){
    var policyPeriod = getCurrentPolicyPeriod()
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var suppressDownPayment = new SuppressDownPaymentBuilder()
            .create(bundle)
      new PolicyChangeBillingInstructionBuilder()
          .onPolicyPeriod(policyPeriod)
          .withPaymentPlanModifier(suppressDownPayment)
          .addChargePremium(950bd.ofCurrency(policyPeriod.Account.Currency))
          .execute()
          .create(bundle)
    })
  }
  
  function renewThis(){
    var policyPeriod = getCurrentPolicyPeriod()
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      new RenewalBillingInstructionBuilder()
      .withPriorPolicyPeriod(policyPeriod)
      .execute()
      .create(bundle)
    })
  }
  
  function rewriteThis(){
    var policyPeriod = getCurrentPolicyPeriod()
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      new RewriteBillingInstructionBuilder()
      .withPriorPolicyPeriod(policyPeriod)
      .execute()
      .create(bundle)
    })
  }

  @Argument("currency", CurrencyUtil.getDefaultCurrency())
  function withTwoProducers() {

    var currency = getArgument("currency")

    var account = new AccountBuilder().withCurrency(typekey.Currency.get(currency)).create()
    var commissionPlan = new CommissionPlanBuilder()
        .withSingleCurrency(typekey.Currency.get(currency))
        .withPremiumCommissionableItem()
        .withPrimaryRate(20bd.ofCurrency(typekey.Currency.get(currency)))
        .withSecondaryRate(10bd.ofCurrency(typekey.Currency.get(currency)))
        .create()
    var producer = new ProducerBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("Code" + randomNumber, commissionPlan)
        .create()
    var policyPeriod = new PolicyPeriodBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .onAccount(account)
        .asAgencyBill()
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withProducerCodeByRole(PolicyRole.TC_SECONDARY, producer.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.PolicyDetailSummary.go(policyPeriod)
  }

   function makeDirectBillDistToPayFirstInvoiceItemInFull(){
    var policyPeriod = getCurrentPolicyPeriod()
    var invoiceItems = policyPeriod.getInvoiceItemsSortedByEventDate()

    new DirectBillPaymentFixtureBuilder()
      .withFullPaymentForInvoiceItem(invoiceItems[0])
      .createFixture()

    pcf.PolicyDetailSummary.go(policyPeriod)
  
  }
   @Argument("invoiceItemIndex", "0")
   @Argument("payment", "10")
   function makeDirectBillDistToPayInvoiceItem(){
    var policyPeriod = getCurrentPolicyPeriod()
    var invoiceItems = policyPeriod.getInvoiceItemsSortedByEventDate()

    new DirectBillPaymentFixtureBuilder()
      .withPartialPaymentForInvoiceItem(invoiceItems[getArgumentAsInt( "invoiceItemIndex" )],
            getArgumentAsMonetaryAmount("payment", policyPeriod.Currency))
      .createFixture()

    pcf.PolicyDetailSummary.go(policyPeriod)
  }
  function makeDirectBillDistToPartiallyPayFirstInvoiceItem(){
    var policyPeriod = getCurrentPolicyPeriod()
    var invoiceItems = policyPeriod.getInvoiceItemsSortedByEventDate()
    new DirectBillPaymentFixtureBuilder()
      .withPartialPaymentForInvoiceItem(invoiceItems[0], 50bd.ofCurrency(policyPeriod.Account.Currency))
      .createFixture()

    pcf.PolicyDetailSummary.go(policyPeriod)
   }

  function getLastUpdated(){
    var query = Query.make(PolicyPeriod)
    var results = query
                    .select()
                    .orderByDescending(QuerySelectColumns.path(Paths.make(PolicyPeriod#UpdateTime)))
    pcf.PolicyDetailSummary.go(results.FirstResult)
  }

   @Argument("depositRequirement", "100")
  function issueDepositRequirementOnBI(){
    var policyPeriod = getCurrentPolicyPeriod()
    var billingInstruction = new GeneralBillingInstructionBuilder()
            .onPolicyPeriod(policyPeriod)
            .withDepositRequirement(getArgumentAsMonetaryAmount("depositRequirement", policyPeriod.Currency))
            .create()
    billingInstruction.execute()
    billingInstruction.getBundle().commit()
    pcf.AccountCollateral.go(policyPeriod.Account)
  }

  function createSlushyDirectBillPP() {
    var policyPeriod = BCArchiveRunCommandUtil.createSlushyDirectBillPolicyPeriodWithOnlyInvoiceItemFrozen().First
    pcf.PolicySummary.go(policyPeriod)
  }

  function createSlushyListBillPP() {
    var policyPeriod = BCArchiveRunCommandUtil.createSlushyListBillPolicyPeriodWithOnlyInvoiceItemFrozen().First
    pcf.PolicySummary.go(policyPeriod)
  }

  function createSlushyAgencyBillPP() {
    var policyPeriod = BCArchiveRunCommandUtil.createSlushyAgencyBillPolicyPeriodWithOnlyInvoiceItemFrozen()
    pcf.PolicySummary.go(policyPeriod)
  }

  function closeOrArchivePolicy(policyPeriod: PolicyPeriod, archive: boolean) : PolicyPeriod {
    try {
      if (not policyPeriod.Closed) {
        if (policyPeriod.UnpaidAmount.IsNotZero) {
          gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
            if (policyPeriod.DirectBill or policyPeriod.ListBill) {
              new DirectBillPaymentFixtureBuilder()
                  .withFullPaymentForPolicyPeriod(policyPeriod)
                  .createFixture(bundle)
            } else if (policyPeriod.AgencyBill) {
              new AgencyBillPaymentFixtureBuilder()
                  .withFullDistributionFor(policyPeriod)
                  .createFixture(bundle)
            }
          })
        }

        stopPolicyQueues()
        DateUtil.addDays(policyPeriod.ExpirationDate, BCConfigParameters.PolicyClosureBufferDays.Value).setClock()
        runBatchProcess(BatchProcessType.TC_INVOICE)
        runBatchProcess(BatchProcessType.TC_STATEMENTBILLED)
        runBatchProcess(BatchProcessType.TC_INVOICEDUE)
        runBatchProcess(BatchProcessType.TC_STATEMENTDUE)
        runBatchProcess(BatchProcessType.TC_PRODUCERPAYMENT)
        runBatchProcess(BatchProcessType.TC_POLICYCLOSURE)
        policyPeriod.refresh()
      }

      if (archive and not policyPeriod.Archived) {
        DateUtil.addDays(policyPeriod.NextArchiveCheckDate, 1).setClock()
        runBatchProcess(BatchProcessType.TC_ARCHIVE)
        policyPeriod.refresh()
      }
    } finally {
      restartPolicyQueues()
    }

    return policyPeriod
  }

  function stopPolicyQueues() {
    new BatchProcessType[]{BatchProcessType.TC_INVOICE,
        BatchProcessType.TC_STATEMENTBILLED,
        BatchProcessType.TC_INVOICEDUE,
        BatchProcessType.TC_STATEMENTDUE,
        BatchProcessType.TC_PRODUCERPAYMENT,
        TC_POLICYCLOSURE}
        .each(\queueType -> WorkQueueTestUtil.stopAndWaitUntilStopped(queueType))
  }

  function restartPolicyQueues() {
    var manager = PLDependencies.getDistributedWorkerManager()

    new BatchProcessType[]{BatchProcessType.TC_INVOICE,
        BatchProcessType.TC_STATEMENTBILLED,
        BatchProcessType.TC_INVOICEDUE,
        BatchProcessType.TC_STATEMENTDUE,
        BatchProcessType.TC_PRODUCERPAYMENT,
        TC_POLICYCLOSURE}
        .each(\queueType -> manager.startQueue(manager.getWorkQueue(queueType.Code)))
  }
}
