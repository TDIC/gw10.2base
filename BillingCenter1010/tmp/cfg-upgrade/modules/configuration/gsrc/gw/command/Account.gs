package gw.command

uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.databuilder.*
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem
uses gw.api.path.Paths
uses gw.api.util.CurrencyUtil
uses gw.api.util.SampleDataGenerator
uses gw.api.web.payment.DirectBillPaymentFactory
uses gw.bc.archive.BCArchiveRunCommandUtil
uses typekey.Currency

@Export
@DefaultMethod("create")
class Account extends BCBaseCommand {
  construct() {
    super()
  }

  @Argument("accountNumber", {"deviantART", "NorthWind", "ClosetMonkey"})
  @Argument("invoiceDayOfMonth", "15")
  @Argument("billingLevel", {"AccountLevelBilling", "PolicyLevelBilling"})
  @Argument("paymentAllocationPlan", Query.make(PaymentAllocationPlan).select(), \x -> (x as PaymentAllocationPlan).Name)
  @Argument("currency", CurrencyUtil.getDefaultCurrency())
  function create(): Account {
    var currency = getArgument("currency")

    var accountBuilder = new AccountBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withInvoiceDayOfMonth(getArgumentAsInt("invoiceDayOfMonth"))

    if (getArgument("accountNumber") != null) {
      accountBuilder.withNumber(getArgument("accountNumber") + "-" + randomNumber)
          .withCurrency(typekey.Currency.get(currency))
    }

    if (getArgument("billingLevel") == "PolicyLevelBilling") {
      accountBuilder.asPolicyDefaultUnappliedLevelBilling()
          .withCurrency(typekey.Currency.get(currency))
    }
    setPaymentAllocationPlan(accountBuilder, getArgumentAsString("paymentAllocationPlan"))
    var account = accountBuilder.createAndCommit()
    pcf.AccountOverview.go(account)
    return account
  }

  @Argument("amount", "1200")
  function withExecutedDisbursement() {
    var account = new AccountBuilder()
        .withName(BCDataBuilder.createRandomWordPair())
        .createAndCommit()
    var disbursement = new AccountDisbursementBuilder()
        .onAccountWithAddress(account)
        .withDueDate(Date.Today.addDays(- 7))
        .withAmount(getArgumentAsMonetaryAmount("amount", account.Currency))
        .withStatus(DisbursementStatus.TC_APPROVED)
        .withReason(Reason.TC_AUTOMATIC)
        .createAndCommit()
    disbursement.makeDisbursement()
    disbursement.getBundle().commit()
    pcf.AccountOverview.go(account)
  }

  function asListBillDefault() {
    var number = randomNumber
    var person = new PersonBuilder()
        .withFirstName("Person ")
        .withLastName(number)
        .withDefaultAddress()
        .create()

    var accountContact = new AccountContact(person.Bundle)
    accountContact.setPrimaryPayer(true)
    accountContact.setContact(person)

    var account = new AccountBuilder()
        .addContact(accountContact)
        .asListBillWithDefaults()
        .withDefaultBillingPlan()
        .withDefaultDelinquencyPlan()
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("amount", "1200")
  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  @Argument("policyNumber", "")
  @Arguments("create")
  function with1PolicyWithNoProducer() {
    var account = create()
    getArgument("paymentPlan")
    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null || !paymentPlan.supportsCurrency(account.Currency)){
      paymentPlan = new PaymentPlanBuilder()
          .withSingleCurrency(account.Currency)
          .withDownPaymentPercent(10)
          .withMaximumNumberOfInstallments(11)
          .withPeriodicity(TC_MONTHLY)
          .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
          .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
          .create()
    }
    var policyNumber = getArgument("policyNumber")
    if (policyNumber.length == 0) {
      policyNumber = "PolicyNumber_" + UniqueKeyGenerator.get().nextKey()
    }
    new PolicyPeriodBuilder()
        .withCurrency(account.Currency)
        .onAccount(account)
        .asDirectBill()
        .withPolicyNumber(policyNumber)
        .withPaymentPlan(paymentPlan)
        .withPremiumWithDepositAndInstallments(getArgumentAsBigDecimal("amount"))
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("currency", CurrencyUtil.getDefaultCurrency())
  function withDefault() {
    var currency = getArgument("currency")
    var number = randomNumber
    var person = new PersonBuilder()
        .withFirstName("Person ")
        .withLastName(number)
        .withDefaultAddress()
        .create()

    var accountContact = new AccountContact(person.Bundle)
    accountContact.setPrimaryPayer(true)
    accountContact.setContact(person)

    var account = new AccountBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .addContact(accountContact)
        .withBillingPlan(new BillingPlanBuilder().withSingleCurrency(typekey.Currency.get(currency)).create())
        .withDelinquencyPlan(new DelinquencyPlanBuilder().withSingleCurrency(typekey.Currency.get(currency)).asPastDueWithDefaultEvents().create())
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  function withTypeCollectionAgency() {
    var number = randomNumber
    var person = new PersonBuilder()
        .withFirstName("Person ")
        .withLastName(number)
        .withDefaultAddress()
        .create()
    var accountContact = new AccountContact(person.Bundle)
    accountContact.setPrimaryPayer(true)
    accountContact.setContact(person)
    var account = new AccountBuilder()
        .addContact(accountContact)
        .withAccountType(TC_COLLECTIONAGENCY)
        .withDelinquencyPlan("QA1DELINQUENCYPLAN07")
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  @Argument("invoiceDayOfMonth", "15")
  @Argument("paymentAllocationPlan", Query.make(PaymentAllocationPlan).select(), \x -> (x as PaymentAllocationPlan).Name)
  @Argument("currency", CurrencyUtil.getDefaultCurrency())
  function with1PolicyWithPrimaryProducer() {
    loadSampleDataIfNotLoaded()

    var currency = getArgument("currency")

    var accountBuilder = new AccountBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDelinquencyPlan(getQADelinquencyPlan07EquivalentInOtherCurrency(typekey.Currency.get(currency)))
        .withInvoiceDayOfMonth(getArgumentAsInt("invoiceDayOfMonth"))
    setPaymentAllocationPlan(accountBuilder, getArgumentAsString("paymentAllocationPlan"))
    var account = accountBuilder.create()
    var commissionPlan = new CommissionPlanBuilder()
        .withSingleCurrency(typekey.Currency.get(currency))
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withPrimaryRate(10)
        .create()
    var producer = new ProducerBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan(BCDataBuilder.createRandomWordPair(), commissionPlan)
        .create()

    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null || !paymentPlan.supportsCurrency(Currency.get(currency))){
      paymentPlan = new PaymentPlanBuilder()
          .withSingleCurrency(typekey.Currency.get(currency))
          .withDownPaymentPercent(10)
          .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
          .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
          .withMaximumNumberOfInstallments(11)
          .withPeriodicity(TC_MONTHLY)
          .create()
    }
    new PolicyPeriodBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .onAccount(account)
        .withPaymentPlan(paymentPlan)
        .doNotHoldInvoicingWhenDelinquent()
        .asDirectBill()
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  @Argument("invoiceDayOfMonth", "15")
  @Argument("paymentAllocationPlan", Query.make(PaymentAllocationPlan).select(), \x -> (x as PaymentAllocationPlan).Name)
  function withPolicyWithOverridingPayerAccount() {
    loadSampleDataIfNotLoaded()

    var account = new AccountBuilder()
        .withDelinquencyPlan("QA1DELINQUENCYPLAN07")
        .withInvoiceDayOfMonth(getArgumentAsInt("invoiceDayOfMonth"))
        .create()
    var payerAccountBuilder = new AccountBuilder()
        .withDelinquencyPlan("QA1DELINQUENCYPLAN07")
        .withInvoiceDayOfMonth(getArgumentAsInt("invoiceDayOfMonth"))
    setPaymentAllocationPlan(payerAccountBuilder, getArgumentAsString("paymentAllocationPlan"))
    var payerAccount = payerAccountBuilder.create()
    var commissionPlan = new CommissionPlanBuilder()
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withPrimaryRate(10)
        .withOnPaymentReceivedPayableCriteria()
        .create()
    var producer = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan(BCDataBuilder.createRandomWordPair(), commissionPlan)
        .create()

    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null){
      paymentPlan = new PaymentPlanBuilder()
          .withDownPaymentPercent(10)
          .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
          .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
          .withMaximumNumberOfInstallments(11)
          .withPeriodicity(TC_MONTHLY)
          .create()
    }
    new PolicyPeriodBuilder()
        .onAccount(account)
        .withPaymentPlan(paymentPlan)
        .doNotHoldInvoicingWhenDelinquent()
        .asDirectBill()
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1234)
        .withOverridingPayerAccount(payerAccount)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  @Argument("invoiceDayOfMonth", "15")
  @Argument("paymentAllocationPlan", Query.make(PaymentAllocationPlan).select(), \x -> (x as PaymentAllocationPlan).Name)
  @Argument("currency", CurrencyUtil.getDefaultCurrency())
  function with1PolicyWithPrimaryProducerPayOnPay() {
    loadSampleDataIfNotLoaded()

    var currency = getArgument("currency")

    var accountBuilder = new AccountBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDelinquencyPlan(getQADelinquencyPlan07EquivalentInOtherCurrency(typekey.Currency.get(currency)))
        .withInvoiceDayOfMonth(getArgumentAsInt("invoiceDayOfMonth"))
    setPaymentAllocationPlan(accountBuilder, getArgumentAsString("paymentAllocationPlan"))
    var account = accountBuilder.create()

    var commissionPlan = new CommissionPlanBuilder()
        .withSingleCurrency(typekey.Currency.get(currency))
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withPrimaryRate(10)
        .withOnPaymentReceivedPayableCriteria()
        .create()
    var producer = new ProducerBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan(BCDataBuilder.createRandomWordPair(), commissionPlan)
        .create()

    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null || !paymentPlan.supportsCurrency(Currency.get(currency))){
      paymentPlan = new PaymentPlanBuilder()
          .withSingleCurrency(Currency.get(currency))
          .withDownPaymentPercent(10)
          .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
          .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
          .withMaximumNumberOfInstallments(11)
          .withPeriodicity(TC_MONTHLY)
          .create()
    }
    new PolicyPeriodBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .onAccount(account)
        .withPaymentPlan(paymentPlan)
        .doNotHoldInvoicingWhenDelinquent()
        .asDirectBill()
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  @Argument("invoiceDayOfMonth", "15")
  @Argument("paymentAllocationPlan", Query.make(PaymentAllocationPlan).select(), \x -> (x as PaymentAllocationPlan).Name)
  @Argument("currency", CurrencyUtil.getDefaultCurrency())
  function with1PolicyWithPrimaryAndSecondaryProducer() {
    loadSampleDataIfNotLoaded()

    var currency = getArgument("currency")

    var accountBuilder = new AccountBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDelinquencyPlan(getQADelinquencyPlan07EquivalentInOtherCurrency(typekey.Currency.get(currency)))
        .withInvoiceDayOfMonth(getArgumentAsInt("invoiceDayOfMonth"))
    setPaymentAllocationPlan(accountBuilder, getArgumentAsString("paymentAllocationPlan"))
    var account = accountBuilder.create()
    var commissionPlan = new CommissionPlanBuilder()
        .withSingleCurrency(typekey.Currency.get(currency))
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withPrimaryRate(10)
        .withSecondaryRate(5)
        .create()
    var primaryProducer = new ProducerBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan(BCDataBuilder.createRandomWordPair(), commissionPlan)
        .create()
    var secondaryProducer = new ProducerBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan(BCDataBuilder.createRandomWordPair(), commissionPlan)
        .create()

    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null || !paymentPlan.supportsCurrency(Currency.get(currency))){
      paymentPlan = new PaymentPlanBuilder()
          .withSingleCurrency(typekey.Currency.get(currency))
          .withDownPaymentPercent(10)
          .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
          .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
          .withMaximumNumberOfInstallments(11)
          .withPeriodicity(TC_MONTHLY)
          .create()
    }
    new PolicyPeriodBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .onAccount(account)
        .withPaymentPlan(paymentPlan)
        .doNotHoldInvoicingWhenDelinquent()
        .asDirectBill()
        .withPrimaryProducerCode(primaryProducer.ProducerCodes[0])
        .withProducerCodeByRole(PolicyRole.TC_SECONDARY, secondaryProducer.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1100)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  function loadSampleDataIfNotLoaded() {
    var sampleCommissionPlanQuery = Query.make(CommissionPlan).compare("Name", Equals, "QA1COMMISSIONPLAN01").select()
    if (sampleCommissionPlanQuery.Empty) {
      print("Producer.gs initialization: Loading Sample Data...")
      SampleDataGenerator.generateDefaultSampleData()
    }
  }

  function withPremiumReportEnabledPolicyWithTaxesChargeOnly() {
    var account = new AccountBuilder()
        .create()

    var commissionPlan = new CommissionPlanBuilder()
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withPrimaryRate(10)
        .create()

    var producer = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan(BCDataBuilder.createRandomWordPair(), commissionPlan)
        .create()

    var charges = new ChargeBuilder()
        .withAmount(50bd.ofCurrency(account.Currency))
        .asTaxes()

    new PolicyPeriodBuilder()
        .onAccount(account)
        .withPaymentPlan("Premium Reporting")
        .issuedWithCharge(charges)
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  @Argument("currency", CurrencyUtil.getDefaultCurrency())
  function withPayOnPayCommissionPolicy() {
    var currency = getArgument("currency")

    var account = new AccountBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withName(BCDataBuilder.createRandomWordPair())
        .withDelinquencyPlan(getQADelinquencyPlan07EquivalentInOtherCurrency(typekey.Currency.get(currency)))
        .create()
    var commissionPlan = new CommissionPlanBuilder()
        .withSingleCurrency(typekey.Currency.get(currency))
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withOnPaymentReceivedPayableCriteria()
        .withPrimaryRate(10)
        .create()

    var producer = new ProducerBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("PC-" + BCDataBuilder.createRandomWordPair(), commissionPlan)
        .create()

    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult

    if (paymentPlan == null || !paymentPlan.supportsCurrency(Currency.get(currency))) {
      paymentPlan = new PaymentPlanBuilder()
          .withSingleCurrency(typekey.Currency.get(currency))
          .withDownPaymentPercent(10)
          .withMaximumNumberOfInstallments(11)
          .withPeriodicity(TC_MONTHLY)
          .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
          .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
          .create()
    }

    new PolicyPeriodBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .onAccount(account)
        .asDirectBill()
        .withPaymentPlan(paymentPlan)
        .doNotHoldInvoicingWhenDelinquent()
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  function withPayOnPayThreeProducersPolicy() {
    var account = new AccountBuilder()
        .withDefaultDelinquencyPlan()
        .create()
    var commissionPlan = new CommissionPlanBuilder()
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withOnPaymentReceivedPayableCriteria()
        .withPrimaryRate(10)
        .withSecondaryRate(5)
        .withReferrerRate(2)
        .create()

    var producer = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("PCode" + randomNumber, commissionPlan)
        .create()

    var producer2 = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("PCode2" + randomNumber, commissionPlan)
        .create()

    var producer3 = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("PCode3" + randomNumber, commissionPlan)
        .create()

    var paymentPlan = new PaymentPlanBuilder()
        .withDownPaymentPercent(10)
        .withMaximumNumberOfInstallments(11)
        .withPeriodicity(TC_MONTHLY)
        .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
        .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
        .create()

    new PolicyPeriodBuilder()
        .onAccount(account)
        .asDirectBill()
        .withPaymentPlan(paymentPlan)
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withProducerCodeByRole(PolicyRole.TC_SECONDARY, producer2.ProducerCodes[0])
        .withProducerCodeByRole(PolicyRole.TC_REFERRER, producer3.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  function withPayOnBillCommissionPolicy() {
    var account = new AccountBuilder()
        .withDefaultDelinquencyPlan()
        .create()
    var commissionPlan = new CommissionPlanBuilder()
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withOnBillingPayableCriteria()
        .withPrimaryRate(10)
        .create()

    var producer = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("PCode" + randomNumber, commissionPlan)
        .create()
    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null){
      paymentPlan = new PaymentPlanBuilder()
          .withDownPaymentPercent(10)
          .withMaximumNumberOfInstallments(11)
          .withPeriodicity(TC_MONTHLY)
          .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
          .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
          .create()
    }
    new PolicyPeriodBuilder()
        .onAccount(account)
        .asDirectBill()
        .withPaymentPlan(paymentPlan)
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  function withPayOnPaySecondaryCommissionPolicy() {
    var account = new AccountBuilder()
        .withDefaultDelinquencyPlan()
        .create()
    var commissionPlan = new CommissionPlanBuilder()
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withOnPaymentReceivedPayableCriteria()
        .withPrimaryRate(10)
        .withSecondaryRate(5)
        .create()

    var producer = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("PCode" + randomNumber, commissionPlan)
        .create()

    var producer2 = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("PCode2" + randomNumber, commissionPlan)
        .create()
    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null){
      paymentPlan = new PaymentPlanBuilder()
          .withDownPaymentPercent(10)
          .withMaximumNumberOfInstallments(11)
          .withPeriodicity(TC_MONTHLY)
          .withDaysBeforePolicyExpirationForInvoicingBlackout(0)
          .withFirstInstallmentAfterPolicyEffectiveDatePlusOnePeriod()
          .create()
    }

    new PolicyPeriodBuilder()
        .onAccount(account)
        .asDirectBill()
        .withPaymentPlan(paymentPlan)
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withProducerCodeByRole(PolicyRole.TC_SECONDARY, producer2.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("invoiceDay", 1)
  function withInvoiceDayIs() {
    var account = new AccountBuilder()
        .withInvoiceDayOfMonth(getArgumentAsInt("invoiceDay"))
        .withDefaultDelinquencyPlan()
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  function withPayOnPayCommissionPolicyPartiallyPaid() {
    var account = new AccountBuilder()
        .withDefaultDelinquencyPlan()
        .withDistributionUpToAmountUnderContract()
        .create()
    var commissionPlan = new CommissionPlanBuilder()
        .allowOnAllTiers()
        .withPremiumCommissionableItem()
        .withOnPaymentReceivedPayableCriteria()
        .withPrimaryRate(10)
        .create()

    var producer = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan("PCode" + randomNumber, commissionPlan)
        .create()

    var paymentPlan = Query.make(PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null){
      paymentPlan = new PaymentPlanBuilder()
          .create()
    }
    new PolicyPeriodBuilder()
        .onAccount(account)
        .asDirectBill()
        .withPaymentPlan(paymentPlan)
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()

    var payment = DirectBillPaymentFactory.pay(account, 10bd.ofCurrency(account.Currency))
    payment.Bundle.commit()

    pcf.AccountOverview.go(account)
  }

  @Argument("FixedDueDayOfMonth", 15)
  function withFixedDueDayIs() {
    var account = new AccountBuilder()
        .asDueDateBilling()
        .withInvoiceDayOfMonth(getArgumentAsInt("FixedDueDayOfMonth"))
        .withDefaultDelinquencyPlan()
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  function withFullPayDiscountPolicy() {
    var account = new AccountBuilder()
        .withDistributionUpToAmountUnderContract()
        .create()

    new PolicyPeriodBuilder()
        .onAccount(account)
        .asDirectBill()
        .withPolicyNumber("Policy-" + randomNumber)
        .withPaymentPlan("QA1PAYMENTPLAN01")
        .withPremiumWithDepositAndInstallments(1000)
        .eligibleForFullPayDiscount()
        .withFullPayDiscountDate(gw.api.util.DateUtil.addDays(gw.api.util.DateUtil.currentDate(), 7))
        .withDiscountedPaymentThreshold(900bd.ofCurrency(account.Currency))
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  function withFullPayDiscountPolicyAndPayment() {
    var account = new AccountBuilder()
        .withDistributionUpToAmountUnderContract()
        .create()
    new PolicyPeriodBuilder()
        .onAccount(account)
        .asDirectBill()
        .withPolicyNumber("Policy-" + randomNumber)
        .withPaymentPlan("QA1PAYMENTPLAN01")
        .withPremiumWithDepositAndInstallments(1000)
        .eligibleForFullPayDiscount()
        .withFullPayDiscountDate(gw.api.util.DateUtil.addDays(gw.api.util.DateUtil.currentDate(), 7))
        .withDiscountedPaymentThreshold(900bd.ofCurrency(account.Currency))
        .createAndCommit()
    account.makeSingleCashPaymentUsingNewBundle(1000bd.ofCurrency(account.Currency))
    pcf.AccountOverview.go(account)
  }

  function addPolicy() {
    var account = getCurrentAccount()
    new PolicyPeriodBuilder()
        .withCurrency(account.Currency)
        .onAccount(account)
        .withPaymentPlan(new PaymentPlanBuilder().withSingleCurrency(account.Currency))
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  function makeOneInvoiceBilled(): String {
    addMonths(1)
    runBatchProcess(BatchProcessType.TC_INVOICE)
    return "Clock increased by 1 month and 'Invoice' batch process run"
  }

  function makeOneBilledInvoiceDue(): String {
    addMonths(1)
    runBatchProcess(BatchProcessType.TC_INVOICEDUE)
    return "Clock increased by 2 month and 'InvoiceDue' batch process run"
  }

  function makeOnePlannedInvoiceDue(): String {
    addMonths(2)
    runBatchProcess(BatchProcessType.TC_INVOICE)
    runBatchProcess(BatchProcessType.TC_INVOICEDUE)
    return "Clock increased by 2 month and 'InvoiceDue' batch process run"
  }

  function withCashCollateralRequirement() {
    var account = new AccountBuilder()
        .withDistributionUpToAmountUnderContract()
        .createAndCommit()
    new CollateralRequirementBuilder()
        .onAccount(account)
        .withRequired(100bd.ofCurrency(account.Currency))
        .asCashWithInitialCharge()
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  function withPayerInvoiceItemGoToPayer() {
    var ownerAccount = new AccountBuilder()
        .create()
    var payerAccount = new AccountBuilder()
        .create()
    var instruction = new AccountGeneralBillingInstructionBuilder()
        .onAccount(ownerAccount)
        .addCharge(100bd.ofCurrency(ownerAccount.Currency), ChargePatternKey.ACCOUNTLATEFEE.get())
        .execute()
        .createAndCommit()

    var invoiceItems = instruction.getCharges()[0].getInvoiceItems()

    payerAccount.becomePayerOfInvoiceItems(true, null, ReversePaymentsWhenMovingInvoiceItem.No, invoiceItems)
    payerAccount.Bundle.commit()
    pcf.AccountOverview.go(payerAccount)
  }

  function withPayerInvoiceItemGoToOwner() {
    var ownerAccount = new AccountBuilder()
        .create()
    var payerAccount = new AccountBuilder()
        .create()
    var instruction = new AccountGeneralBillingInstructionBuilder()
        .onAccount(ownerAccount)
        .addCharge(100bd.ofCurrency(ownerAccount.Currency), ChargePatternKey.ACCOUNTLATEFEE.get())
        .execute()
        .createAndCommit()

    var invoiceItems = instruction.getCharges()[0].getInvoiceItems()

    payerAccount.becomePayerOfInvoiceItems(true, null, ReversePaymentsWhenMovingInvoiceItem.No, invoiceItems)
    payerAccount.Bundle.commit()
    pcf.AccountOverview.go(ownerAccount)
  }

  function getLastUpdated() {
    var query = Query.make(Account)
    var results = query
        .select()
        .orderByDescending(QuerySelectColumns.path(Paths.make(Account#UpdateTime)))
    var account = results.FirstResult
    pcf.AccountOverview.go(account)
  }

  @Argument("payment", "0")
  function issueDirectBillPayment() {
    var account = getCurrentAccount()
    DirectBillPaymentFactory.pay(account, getArgumentAsMonetaryAmount("payment", account.Currency))
    account.getBundle().commit()
  }

  function allotFunds() {
    var account = getCurrentAccount()
    account.allotAllFunds()
    account.Bundle.commit()
  }

  @Argument("amount", "0")
  function makeDisbursement() {
    var account = getCurrentAccount()
    var disbursement = new AccountDisbursementBuilder()
        .withCurrency(account.Currency)
        .onAccountWithAddress(account)
        .withAmount(getArgumentAsMonetaryAmount("amount", account.Currency))
        .withStatus(DisbursementStatus.TC_APPROVED)
        .withReason(Reason.TC_AUTOMATIC)
        .createAndCommit()
    disbursement.makeDisbursement()
    disbursement.getBundle().commit()
  }

  @Argument("currency")
  function createChildAccountWith1Policy() {
    var currency = getArgument("currency")
    var parentAccount = getCurrentAccount()
    if (currency == null) {
      currency = parentAccount.Currency.Code
    }
    var account = new AccountBuilder()
        .withParentAccount(parentAccount)
        .withCurrency(typekey.Currency.get(currency))
        .withDistributionUpToAmountUnderContract()
        .withBillingPlan(new BillingPlanBuilder().withSingleCurrency(typekey.Currency.get(currency)).create())
        .withDelinquencyPlan(new DelinquencyPlanBuilder().withSingleCurrency(typekey.Currency.get(currency)).asPastDueWithDefaultEvents().create())
        .create()

    var paymentPlan = new PaymentPlanBuilder().withSingleCurrency(Currency.get(currency))
    new PolicyPeriodBuilder()
        .withCurrency(typekey.Currency.get(currency))
        .onAccount(account)
        .asDirectBill()
        .withPaymentPlan(paymentPlan)
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  @Argument("FixedDueDayOfMonth", "15")
  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  @Argument("billingPlan", Query.make(BillingPlan).select(), \x -> (x as BillingPlan).Name)
  @Argument("paymentAllocationPlan", Query.make(PaymentAllocationPlan).select(), \x -> (x as PaymentAllocationPlan).Name)
  function withFixedDueDate() {
    var paymentPlan = Query.make(entity.PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null){
      paymentPlan = Query.make(entity.PaymentPlan).compare("Name", Equals, "Monthly 10").select().FirstResult
    }

    var billingPlan = Query.make(entity.BillingPlan).compare("Name", Equals, getArgumentAsString("billingPlan")).select().FirstResult
    if (billingPlan == null){
      billingPlan = Query.make(entity.BillingPlan).compare("Name", Equals, "QA1BILLINGPLAN01").select().FirstResult
    }

    var accountBuilder = new AccountBuilder()
        .asDueDateBilling()
        .withInvoiceDayOfMonth(getArgumentAsInt("FixedDueDayOfMonth"))
        .withBillingPlan(billingPlan)
        .withDelinquencyPlan("QA1DELINQUENCYPLAN07")
        .withPaymentMethod(TC_CASH)
    setPaymentAllocationPlan(accountBuilder, getArgumentAsString("paymentAllocationPlan"))
    var account = accountBuilder.create()

    var producer = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan(BCDataBuilder.createRandomWordPair(), "QA1COMMISSIONPLAN08")
        .create()

    new PolicyPeriodBuilder()
        .onAccount(account)
        .asDirectBill()
        .doNotHoldInvoicingWhenDelinquent()
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withPaymentPlan(paymentPlan)
        .withPremiumAndTaxes(1000, 200)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  //create account with policy.  If not plan is selected, the "Monthly 10" payment plan, standard billing plan and standard delinquency plan will be selected

  @Argument("FixedDueDayOfMonth", "15")
  @Argument("paymentPlan", Query.make(PaymentPlan).select(), \x -> (x as PaymentPlan).Name)
  @Argument("billingPlan", Query.make(BillingPlan).select(), \x -> (x as BillingPlan).Name)
  @Argument("delinquencyPlan", Query.make(DelinquencyPlan).select(), \x -> (x as DelinquencyPlan).Name)
  @Argument("paymentAllocationPlan", Query.make(PaymentAllocationPlan).select(), \x -> (x as PaymentAllocationPlan).Name)
  function withStandardPlans() {
    var paymentPlan = Query.make(entity.PaymentPlan).compare("Name", Equals, getArgumentAsString("paymentPlan")).select().FirstResult
    if (paymentPlan == null){
      paymentPlan = Query.make(entity.PaymentPlan).compare("Name", Equals, "Monthly 10").select().FirstResult
    }

    var billingPlan = Query.make(entity.BillingPlan).compare("Name", Equals, getArgumentAsString("billingPlan")).select().FirstResult
    if (billingPlan == null){
      billingPlan = Query.make(entity.BillingPlan).compare("Name", Equals, "Standard Mail").select().FirstResult
    }

    var delinquencyPlan = Query.make(entity.DelinquencyPlan).compare("Name", Equals, getArgumentAsString("delinquencyPlan")).select().FirstResult
    if (delinquencyPlan == null){
      delinquencyPlan = Query.make(entity.DelinquencyPlan).compare("Name", Equals, "Standard Delinquency Plan").select().FirstResult
    }

    var accountBuilder = new AccountBuilder()
        .asDueDateBilling()
        .withInvoiceDayOfMonth(getArgumentAsInt("FixedDueDayOfMonth"))
        .withBillingPlan(billingPlan)
        .withDelinquencyPlan(delinquencyPlan)
        .withPaymentMethod(TC_CASH)
    setPaymentAllocationPlan(accountBuilder, getArgumentAsString("paymentAllocationPlan"))
    var account = accountBuilder.create()

    var producer = new ProducerBuilder()
        .withDefaultAgencyBillPlan()
        .withProducerCodeHavingCommissionPlan(BCDataBuilder.createRandomWordPair(), "QA1COMMISSIONPLAN08")
        .create()

    new PolicyPeriodBuilder()
        .onAccount(account)
        .asDirectBill()
        .doNotHoldInvoicingWhenDelinquent()
        .withPrimaryProducerCode(producer.ProducerCodes[0])
        .withPaymentPlan(paymentPlan)
        .withPremiumAndTaxes(1000, 200)
        .createAndCommit()
    pcf.AccountOverview.go(account)
  }

  function withListBillPolicy() {
    var listBillAccount = new AccountBuilder()
        .asListBillWithDefaults()
        .createAndCommit()
    var paymentPlan = listBillAccount.PaymentPlans[0];
    var invoiceStream = listBillAccount.InvoiceStreams[0]

    var ownerAccount = new AccountBuilder()
        .createAndCommit()

    var policyNumber = "PolicyNumber_" + UniqueKeyGenerator.get().nextKey()

    new PolicyPeriodBuilder()
        .onAccount(ownerAccount)
        .withOverridingPayerAccount(listBillAccount)
        .withOverridingInvoiceStream(invoiceStream)
        .asListBill()
        .withPolicyNumber(policyNumber)
        .withPaymentPlan(paymentPlan)
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()
    pcf.AccountOverview.go(ownerAccount)
  }

  function withPolicyWithSomeItemsBilled() {
    var policyPeriod = new IssuanceBuilder().createAndCommit()

    for (var i in 0..2) {
      var item = policyPeriod.InvoiceItemsSortedByEventDate[i]
      item.Invoice.makeBilledWithClockAdvanceToEventDate()
      item.Bundle.commit()
    }
  }

  // same as withDefault, but with designated unapplied billing level & one policy (w/ charge) hooked to a designated unapplied

  function withDesignatedUnapplieds() {
    var currency = CurrencyUtil.getDefaultCurrency()
    var number = randomNumber
    var person = new PersonBuilder()
        .withFirstName("Person")
        .withLastName(number)
        .withDefaultAddress()
        .create()

    var accountContact = new AccountContact(person.Bundle)
    accountContact.setPrimaryPayer(true)
    accountContact.setContact(person)

    var account = new AccountBuilder()
        .withCurrency(currency)
        .addContact(accountContact)
        .withBillingPlan(new BillingPlanBuilder().withSingleCurrency(currency).create())
        .withDelinquencyPlan(new DelinquencyPlanBuilder().withSingleCurrency(currency).asPastDueWithDefaultEvents().create())
        .asPolicyDesignatedUnappliedLevelBilling()
        .createAndCommit()

    new PolicyPeriodBuilder()
        .withCurrency(account.Currency)
        .onAccount(account)
        .withPaymentPlan(new PaymentPlanBuilder().withSingleCurrency(account.Currency))
        .withPremiumWithDepositAndInstallments(1000)
        .createAndCommit()

    pcf.AccountOverview.go(account)
  }

  // creates a designated unapplied on the current account, as if for custom billing

  function addDesignatedUnappliedFunds() {
    var account = getCurrentAccount()
    account.createDesignatedUnappliedFund(BCDataBuilder.createRandomWordPair())
    account.Bundle.commit()
    pcf.AccountOverview.go(account)
  }

  private function getQADelinquencyPlan07EquivalentInOtherCurrency(currency: Currency): DelinquencyPlan {
    var delinquencyPlan = Query.make(DelinquencyPlan).compare("Name", Equals, "QA1DELINQUENCYPLAN07").select().FirstResult
    if (delinquencyPlan == null || !delinquencyPlan.supportsCurrency(currency)) {
      return new DelinquencyPlanBuilder()
          .withSingleCurrency(currency)
          .withCancellationTarget(CancellationTarget.TC_DELINQUENTPOLICYONLY)
          .withPlanReason({ new DelinquencyPlanReasonBuilder()
              .forReason(DelinquencyReason.TC_PASTDUE)
              .withWorkflow(typekey.Workflow.TC_STDDELINQUENCY)
              .withStandardEvents() })
          .withApplicableSegments(ApplicableSegments.TC_ALL)
          .withAccountEnterDelinquencyThresholdForCurrency(currency, 10bd.ofCurrency(currency))
          .withCancellationThresholdForCurrency(currency, 20bd.ofCurrency(currency))
          .withExitDelinquencyThresholdForCurrency(currency, 5bd.ofCurrency(currency))
          .withLateFeeAmountForCurrency(currency, 0bd.ofCurrency(currency))
          .withReinstatementFeeAmountForCurrency(currency, 50bd.ofCurrency(currency))
          .createAndCommit()
    }

    return delinquencyPlan
  }

  private function setPaymentAllocationPlan(accountBuilder: AccountBuilder, paymentAllocationPlanName: String) {
    var paymentAllocationPlan =
        Query.make(PaymentAllocationPlan).compare("Name", Equals, paymentAllocationPlanName).select().FirstResult
    if (paymentAllocationPlan != null) {
      accountBuilder.withPaymentAllocationPlan(paymentAllocationPlan)
    }
  }

  /**
   * Creates a policy period where the first invoice item shares an invoice with the single invoice item of
   * the archivable policy period and therefore is frozen. The policy period of the frozen invoice item is not
   * archived and never has been, it simply shares an invoice with an archived policy period.
   * By the nature of freezing requirements, this frozen invoice item will be billed, due, exactly settled and
   * at least {@link gw.api.system.BCConfigParameters#ArchivePolicyPeriodDays} old.
   * PP1     <----|Archived|
   * PP2          | Frozen |----|Unfrozen|----|Unfrozen|----|Unfrozen|---->
   *                   |
   *          Shared Frozen Invoice
   *
   * Lands on PP2
   */
  function withSlushyPP2() {
    var policyPeriod = BCArchiveRunCommandUtil.createFrozenDirectBillInvoiceItem().PolicyPeriod
    pcf.PolicySummary.go(policyPeriod)
  }

  /**
   * Create a slushy Renewal -- it shares a frozen invoice with the prior PolicyPeriod which is archived.
   *
   * Create a PolicyPeriod
   * Advance Clock 10 months (not a full year)
   * Renew the PolicyPeriod, that places item(s) for the renewed PolicyPeriod on invoice(s) that contain items from the original PolicyPeriod.
   * Pay the renewal's invoice items which overlap with the prior PolicyPeriod.
   * Close and archive the prior Policy Period to make the Renewed policy period slushy.
   */
  function withSlushyRenewal() {
    var renewal = BCArchiveRunCommandUtil.createSlushyRenewal(null, TC_DIRECTBILL)
    pcf.PolicySummary.go(renewal)
  }

  /**
   * Create a slushy Direct Bill payment -- it has a frozen dist item from paying a frozen invoice item.
   */
  function withSlushyDirectBillPayment() {
    var slushyDirectBillPayment = BCArchiveRunCommandUtil.createSlushyExecutedDirectBillPayment()
    pcf.AccountOverview.go(slushyDirectBillPayment.getAccount())
  }
}
