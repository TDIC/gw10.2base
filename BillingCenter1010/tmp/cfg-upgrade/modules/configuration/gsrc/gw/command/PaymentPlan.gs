package gw.command

uses gw.api.databuilder.PaymentPlanBuilder

@Export
class PaymentPlan extends BaseCommand {

  function withUserVisibleFalse(): String{
    var paymentPlan =  new PaymentPlanBuilder().asNotUserVisible().createAndCommit()
    pcf.PaymentPlans.go()
    return paymentPlan + " was created invisible"
  }

  function withUserVisibleTrue(): String {
    var paymentPlan = new PaymentPlanBuilder().asUserVisible().createAndCommit()
    pcf.PaymentPlans.go()
    return paymentPlan + " was created visible"
  }

  function withNoAlignmentForInstallments(): String {
    var paymentPlan = new PaymentPlanBuilder().withLegacyLumpyInvoices().createAndCommit()
    pcf.PaymentPlans.go()
    return paymentPlan + " w/o alignment of installments to invoices"
  }
}
