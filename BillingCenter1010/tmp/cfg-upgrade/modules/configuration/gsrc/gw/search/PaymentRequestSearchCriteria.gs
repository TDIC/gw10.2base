package gw.search

uses gw.api.database.DBDateRange
uses gw.api.database.IQueryBeanResult
uses gw.api.database.ISelectQueryBuilder
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount

uses java.io.Serializable
uses java.util.Date

@Export
class PaymentRequestSearchCriteria implements Serializable {

  final static var SHOULD_IGNORE_CASE = true

  var _status : PaymentRequestStatus as Status            /* The status of the disbursement */
  var _accountNumber : String as AccountNumber            /* The related account number */
  var _minAmount : MonetaryAmount as MinAmount            /* The minimum amount to search for */
  var _maxAmount : MonetaryAmount as MaxAmount            /* The maximum amount to search for */
  var _earliestRequestDate : Date as EarliestRequestDate  /* Earliest request date to search */
  var _latestRequestDate : Date as LatestRequestDate      /* Latest request date to search */
  var _earliestDraftDate : Date as EarliestDraftDate      /* Earliest draft date to search */
  var _latestDraftDate : Date as LatestDraftDate          /* Latest draft date to search */
  var _earliestStatusDate : Date as EarliestStatusDate    /* Earliest status date to search */
  var _latestStatusDate : Date as LatestStatusDate        /* Latest status date to search */
  var _currency : Currency as Currency                    /* The currency unit to restrict the search to */
  var _token : String as Token                            /* The Token associated with the PaymentInstrument that was used to make the PaymentRequest */
  var _method : PaymentMethod as Method                   /* Method of payment */

  function performSearch(): IQueryBeanResult<PaymentRequestSearchView> {
    SearchHelper.checkForDateExceptions(EarliestRequestDate, LatestRequestDate)
    SearchHelper.checkForDateExceptions(EarliestDraftDate, LatestDraftDate)
    SearchHelper.checkForDateExceptions(EarliestStatusDate, LatestStatusDate)
    SearchHelper.checkForNumericExceptions(MinAmount, MaxAmount)
    var query = buildQuery()
    return query.select()
  }

  function buildQuery() : Query<PaymentRequestSearchView> {
    var query = Query.make(PaymentRequestSearchView)
    addQueryRestrictions(query)
    return query
  }

  protected function addQueryRestrictions(query : Query) {
    restrictSearchByStatus(query)
    restrictSearchByAccount(query)
    restrictSearchByMinAndMaxAmount(query)
    restrictSearchByDate(query)
    restrictSearchByDraftDate(query)
    restrictSearchByStatusDate(query)
    restrictSearchByPaymentInstrument(query)
    restrictSearchByCurrency(query)
    restrictSearchByUserSecurityZone(query)
  }

  private function restrictSearchByDate(query : Query) {
    if (EarliestRequestDate != null || LatestRequestDate != null) {
      var endOfLatestRequestDate = LatestRequestDate != null ? DateUtil.endOfDay(LatestRequestDate) : LatestRequestDate
      query.between(PaymentRequest#RequestDate, EarliestRequestDate, endOfLatestRequestDate)
    }
  }

  private function restrictSearchByDraftDate(query : Query) {
    if (EarliestDraftDate != null || LatestDraftDate != null) {
      var endOfLatestDraftDate = LatestDraftDate != null ? DateUtil.endOfDay(LatestDraftDate) : LatestDraftDate
      query.between(PaymentRequest#DraftDate, EarliestDraftDate, endOfLatestDraftDate)
    }
  }

  private function restrictSearchByStatusDate(query : Query) {
    if (EarliestStatusDate != null || LatestStatusDate != null) {
      var endOfLatestStatusDate = LatestStatusDate != null ? DateUtil.endOfDay(LatestStatusDate) : LatestStatusDate
      query.between(PaymentRequest#StatusDate, EarliestStatusDate, endOfLatestStatusDate)
    }
  }

  private function restrictSearchByStatus(query : Query) {
    if(Status != null) {
      query.compare(PaymentRequest#Status, Equals, Status)
    }
  }

  private function restrictSearchByCurrency(query : Query) {
    if (Currency != null) {
      query.compare(PaymentRequest#Currency, Equals, Currency)
    }
  }

  private function restrictSearchByMinAndMaxAmount(query : Query) {
    if (MinAmount != null || MaxAmount != null) {
      query.between(PaymentRequest#Amount, MinAmount, MaxAmount)
    }
  }

  private function restrictSearchByAccount(query : Query) {
    if(AccountNumber.NotBlank) {
      query.join(PaymentRequest#Account)
          .startsWith(Account#AccountNumber, AccountNumber, SHOULD_IGNORE_CASE)
    }
  }

  private function hasPaymentInstrumentCriteria() : boolean {
    return (Method != null || Token != null)
  }

  private function restrictSearchByPaymentInstrument(query : Query) {
    if(hasPaymentInstrumentCriteria()) {
      var paymentInstrumentQuery = query.join(PaymentRequest#PaymentInstrument)
      if(Token.NotBlank) {
        paymentInstrumentQuery.compare(PaymentInstrument#Token, Equals, Token)
      }
      if(Method != null) {
        paymentInstrumentQuery.compare(PaymentInstrument#PaymentMethod, Equals, Method)
      }
    }
  }

  private function restrictSearchByUserSecurityZone(query: Query) {
    if (perm.System.acctignoresecurityzone) {
      return // no need to consider security zones
    }
    var accountQuery = query.join(PaymentRequest#Account)
    accountQuery.or(\restriction -> {
      restriction.compare(Account#SecurityZone, Equals, null) // Everyone can see null SecurityZone
      restriction.compareIn(Account#SecurityZone, User.util.CurrentUser.GroupUsers*.Group*.SecurityZone)
    })
  }

}