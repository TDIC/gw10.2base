package gw.module.impl

uses entity.DelinquencyProcess
uses gw.lang.reflect.features.PropertyReference
uses gw.module.IFeesThresholds
uses gw.pl.currency.MonetaryAmount

@Export
class FeesThresholds implements IFeesThresholds {

  override function getInvoiceFeeAmountOverride(defaultAmount : MonetaryAmount, invoice : AccountInvoice) : MonetaryAmount {
    return defaultAmount
  }

  override function getPaymentReversalFeeAmountOverride(defaultAmount : MonetaryAmount, moneyRcvd : DirectBillMoneyRcvd) : MonetaryAmount {
    return defaultAmount
  }

  override function getLowBalanceThresholdAmountOverride(defaultAmount : MonetaryAmount, invoice : AccountInvoice) : MonetaryAmount {
    return defaultAmount
  }

  override function getGracePeriodDaysOverride(defaultValue : int, delinquencyTarget : DelinquencyProcess) : int {
    return defaultValue
  }

  override function getDisbursementOverOverride(defaultDisbursementOver : MonetaryAmount, unappliedFund : UnappliedFund, disbursementReason : Reason) : MonetaryAmount {
    return defaultDisbursementOver
  }

  override function getReviewDisbursementOverOverride(defaultAmount : MonetaryAmount, disbursement : AccountDisbursement) : MonetaryAmount {
    return defaultAmount
  }

  override property get PCFMode() : String {
    return "default"
  }

  override function cloneBillingPlan(billingPlan : BillingPlan, includeSpecializations : boolean) : BillingPlan {
    return billingPlan.makeClone() as BillingPlan
  }

  override function cloneDelinquencyPlan(delinquencyPlan : DelinquencyPlan, includeSpecializations : boolean) : DelinquencyPlan {
    return delinquencyPlan.makeClone() as DelinquencyPlan
  }

  override property get InvoiceProperties() : List<PropertyReference<Object, Object>> {
    return null
  }

  override function getPaymentDueIntervalOverride(defaultInterval : int, account : Account, policy : Policy, paymentMethod : PaymentMethod) : int {
    return defaultInterval
  }
}