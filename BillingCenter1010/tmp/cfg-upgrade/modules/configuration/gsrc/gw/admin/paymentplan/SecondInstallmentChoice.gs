package gw.admin.paymentplan

uses gw.api.locale.DisplayKey
uses java.util.List

@Export
enum SecondInstallmentChoice { //
  NOT_OVERRIDDEN(\-> DisplayKey.get('Web.PaymentPlanDetailScreen.NotOverridden'), null),
  NO(\-> DisplayKey.get('Web.InstallmentTreatmentSet.No'), false),
  YES(\-> DisplayKey.get('Web.InstallmentTreatmentSet.Yes'), true)
  //
  private var _displayResolver: block(): String as DisplayResolver
  private var _booleanValue: Boolean as BooleanValue
  //
  static private var _valuesWhenRequired = {YES, NO}
  static private var _valuesWhenNotRequired = {NOT_OVERRIDDEN, YES, NO}
  //

  private construct(resolveDisplayKey(): String, booleanValue : Boolean) {
    _displayResolver = resolveDisplayKey
    _booleanValue = booleanValue
  }

  override function toString(): String {
    return DisplayResolver()
  }

  function asBoolean(): Boolean {
    return _booleanValue
  }

  static function parse(value : Boolean) : SecondInstallmentChoice {
    switch (value) {
      case Boolean.TRUE:
        return YES
      case Boolean.FALSE:
        return NO
      default:
        return NOT_OVERRIDDEN
    }
  }

  static function getValues(canDisplayNotOverridden : boolean): List< SecondInstallmentChoice > {
    return canDisplayNotOverridden
        ? _valuesWhenNotRequired
        : _valuesWhenRequired
  }
}