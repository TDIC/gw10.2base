package gw.admin.paymentplan

uses java.lang.Integer
uses java.math.BigDecimal
uses java.util.ArrayList
uses gw.api.locale.DisplayKey
uses java.util.List

/**
 * Auxiliary class to PaymentPlanViewHelper.gs that wraps a List of ChargeSlicingOverrides entities and ensures that
 * the field values of all entities in the List are synchronized.
 *
 * When "First Term Only" is selected on NewPaymentPlanWizard_SecondStepScreen, ChargeSlicingOverrides are created on the
 * PaymentPlan for "Renewal" and "New Renewal" BillingInstructions.
 *
 */
@Export
class SubsequentTermsViewHelper extends InstallmentViewHelper {
  private var _overridesList  : List<ChargeSlicingOverrides> as OverridesList
  /*
   A copy of each field on the ChargeSlicingOverrides entity is maintained in order to synchronize the values across each
   ChargeSlicingOverrides entity in the List
  */
  private var _hasDownPayment: Boolean
  private var _downPaymentPercent: BigDecimal
  private var _maximumNumberOfInstallments: Integer
  private var _daysFromReferenceDateToDownPayment: Integer
  private var _daysFromReferenceDateToFirstInstallment: Integer
  private var _daysFromReferenceDateToSecondInstallment: Integer
  private var _daysFromReferenceDateToOneTimeCharge: Integer
  private var _downPaymentAfter: PaymentScheduledAfter as DownPaymentAfter
  private var _firstInstallmentAfter: PaymentScheduledAfter as FirstInstallmentAfter
  private var _secondInstallmentAfter: PaymentScheduledAfter as SecondInstallmentAfter
  private var _oneTimeChargeAfter: PaymentScheduledAfter as OneTimeChargeAfter
  private var _alignInstallmentsToInvoices: Boolean
  private var _hasOffSequenceInstallment: Boolean
  //

  /* constructor and initialization code *****************************************************************************/

  construct() {
    _hasDownPayment=false //for ChargeSlicingOverrides this value is ternary: null, false, or true
    _hasOffSequenceInstallment = false
    SecondInstallmentChoice = NO
    OverridesList = new ArrayList<ChargeSlicingOverrides>()
  }

  /* getters and setters (public) ***********************************************************************************/

  property set OverridesList(value: List<ChargeSlicingOverrides>) {
    _overridesList = value
    HasDownPayment=_hasDownPayment
    MaximumNumberOfInstallments= _maximumNumberOfInstallments
    DaysFromReferenceDateToDownPayment = _daysFromReferenceDateToDownPayment
    DaysFromReferenceDateToFirstInstallment = _daysFromReferenceDateToFirstInstallment
    DaysFromReferenceDateToSecondInstallment = _daysFromReferenceDateToSecondInstallment
    DaysFromReferenceDateToOneTimeCharge = _daysFromReferenceDateToOneTimeCharge
    AlignInstallmentsToInvoices = _alignInstallmentsToInvoices
    HasSecondInstallment = _hasOffSequenceInstallment
    initWhenFields()
  }

  property get OverridesList(): List<ChargeSlicingOverrides> {
    return _overridesList
  }

 /* overridden getters and setters (public) *************************************************************************/

  //// Down Payment and Maximum Number of Installment fields

  override property set HasDownPayment(value: Boolean) {
    _hasDownPayment = value
    _overridesList.each(\overrides -> {overrides.HasDownPayment = value
    })
  }

  override property get HasDownPayment(): Boolean {
    return _hasDownPayment
  }

  override property set DownPaymentPercent(value: BigDecimal) {
    _downPaymentPercent = value
    _overridesList.each(\overrides -> {overrides.DownPaymentPercent = value
    })
  }

  override property get DownPaymentPercent(): BigDecimal {
    return _downPaymentPercent
  }

  override property set MaximumNumberOfInstallments(value: Integer) {
    _maximumNumberOfInstallments = value
    _overridesList.each(\overrides -> {overrides.MaximumNumberOfInstallments = value
    })
  }

  override property get MaximumNumberOfInstallments(): Integer {
    return _maximumNumberOfInstallments
  }

  override property get RequiredFieldCalculator() : gw.admin.paymentplan.RequiredFieldCalculator {
    return new RequiredFieldCalculator(gw.admin.paymentplan.RequiredFieldCalculator.DownPaymentMode.FirstTermOnly.Code, gw.admin.paymentplan.RequiredFieldCalculator.ScreenLocation.SUBSEQUENT_TERMS, this)
  }

  //// PaymentScheduledAfter fields

  override property set DownPaymentAfter(value: PaymentScheduledAfter) {
    _downPaymentAfter = value
    _overridesList.each(\overrides -> {overrides.DownPaymentAfter = value
    })
  }

  override property get DownPaymentAfter(): PaymentScheduledAfter {
    return _downPaymentAfter
  }

  override property set FirstInstallmentAfter(value: PaymentScheduledAfter) {
    _firstInstallmentAfter = value
    _overridesList.each(\overrides -> {overrides.FirstInstallmentAfter = value
    })
  }

  override property get FirstInstallmentAfter(): PaymentScheduledAfter {
    return _firstInstallmentAfter
  }

  override property set SecondInstallmentAfter(value: PaymentScheduledAfter) {
    _secondInstallmentAfter = value
    _overridesList.each(\overrides -> {overrides.SecondInstallmentAfter = value
    })
  }

  override property get SecondInstallmentAfter(): PaymentScheduledAfter {
    return _secondInstallmentAfter
  }

  override property set OneTimeChargeAfter(value: PaymentScheduledAfter) {
    _oneTimeChargeAfter = value
    _overridesList.each(\overrides -> {overrides.OneTimeChargeAfter = value
    })
  }

  override  property get OneTimeChargeAfter(): PaymentScheduledAfter {
    return _oneTimeChargeAfter
  }

  //// Align InvoiceItem to Installments fields

  override property set AlignInstallmentsToInvoices(value: Boolean) {
    _alignInstallmentsToInvoices = value
  }

  override property get AlignInstallmentsToInvoices(): Boolean {
    return _alignInstallmentsToInvoices
  }

  //// Off Sequence Installment fields

  override property set HasSecondInstallment(value: Boolean) {
    _hasOffSequenceInstallment = value
    _overridesList.each(\overrides -> {overrides.HasSecondInstallment = value})
  }

  override property get HasSecondInstallment(): Boolean {
    return _hasOffSequenceInstallment
  }

  /* overridden getters and setters (protected) *************************************************************************/

  //// callbacks for DaysFromReferenceDateToXXX fields

  override protected property get InternalDaysFromReferenceDateToDownPayment(): Integer {
    return _daysFromReferenceDateToDownPayment
  }

  override protected property set InternalDaysFromReferenceDateToDownPayment(value: Integer) {
    _daysFromReferenceDateToDownPayment = value
    _overridesList.each(\overrides -> {overrides.DaysFromReferenceDateToDownPayment = value
    })
  }

  override protected property get InternalDaysFromReferenceDateToFirstInstallment(): Integer {
    return _daysFromReferenceDateToFirstInstallment
  }

  override protected property set InternalDaysFromReferenceDateToFirstInstallment(value: Integer) {
    _daysFromReferenceDateToFirstInstallment = value
    _overridesList.each(\overrides -> {overrides.DaysFromReferenceDateToFirstInstallment = value
    })
  }

  override protected property get InternalDaysFromReferenceDateToSecondInstallment(): Integer {
    return _daysFromReferenceDateToSecondInstallment
  }

  override protected property set InternalDaysFromReferenceDateToSecondInstallment(value: Integer) {
    _daysFromReferenceDateToSecondInstallment = value
    _overridesList.each(\overrides -> {overrides.DaysFromReferenceDateToSecondInstallment = value
    })
  }

  override protected property get InternalDaysFromReferenceDateToOneTimeCharge(): Integer {
    return _daysFromReferenceDateToOneTimeCharge
  }

  override protected property set InternalDaysFromReferenceDateToOneTimeCharge(value: Integer) {
    _daysFromReferenceDateToOneTimeCharge = value
    _overridesList.each(\overrides -> {overrides.DaysFromReferenceDateToOneTimeCharge = value
    })
  }

  override final property get NoneSelectedLabel() : String {
    return null
  }

  override property get BasePaymentPlanHasDownPayment() : Boolean {
    return true
  }

  override property get BasePaymentPlanHasSecondInstallment() : Boolean {
    return false
  }

}