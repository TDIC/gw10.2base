package gw.document;

uses gw.api.web.document.DocumentsHelper
uses gw.plugin.document.IDocumentTemplateDescriptor

@Export
public class DocumentBCContext extends BaseDocumentApplicationContext {
  private var _documentContainer : DocumentContainer

  construct(documentContainer : DocumentContainer) {
    _documentContainer = documentContainer
  }

  override function createDocumentCreationInfo(): DocumentCreationInfo {
    return DocumentsHelper.createDocumentCreationInfo(_documentContainer)
  }

  override function createDocumentDetailsHelper(documents : Document[]): DocumentDetailsApplicationHelper {
    return new DocumentMetadataBCHelper(documents);
  }

  override function createDocumentCreationInfo(documentTemplateDescriptor: IDocumentTemplateDescriptor): DocumentCreationInfo {
    return DocumentsHelper.createDocumentCreationInfo(_documentContainer, documentTemplateDescriptor)
  }

    override function generateDocument(documentCreationInfo: DocumentCreationInfo) {
    var parameters = DocumentsHelper.getDocumentCreationParameters(documentCreationInfo)
    var documentContentsInfo =
        DocumentProduction.createDocumentSynchronously(documentCreationInfo.DocumentTemplateDescriptor,
            parameters, documentCreationInfo.Document)
    DocumentsHelper.outputDocumentContents(documentCreationInfo, documentContentsInfo);
  }
}
