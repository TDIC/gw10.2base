package gw.document

uses gw.api.locale.DisplayKey
uses java.util.Date

@Export
class DocumentContainerMethods {
  public static function addHistoryIfDCIsAccount(documentContainer : DocumentContainer) {
    if (documentContainer typeis Account) {
      documentContainer.addHistoryFromGosu( Date.CurrentDate, 
          HistoryEventType.TC_DOCUMENTSUPDATED, 
          DisplayKey.get("Java.AccountHistory.DocumentsUpdated"), 
          null, null, true )
    }
  }
}
