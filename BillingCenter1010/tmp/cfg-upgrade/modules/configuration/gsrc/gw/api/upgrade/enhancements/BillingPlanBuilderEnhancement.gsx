package gw.api.upgrade.enhancements

uses gw.api.databuilder.BillingPlanBuilder
uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * File created by Guidewire Upgrade Tools.
 */
enhancement BillingPlanBuilderEnhancement : BillingPlanBuilder {

  @DeprecatedAndRestoredByUpgrade
  function withInvoiceFee(amount : MonetaryAmount) : BillingPlanBuilder {
      this.withInvoiceFeeForCurrency(CurrencyUtil.DefaultCurrency, amount)
      return this;
    }

    @DeprecatedAndRestoredByUpgrade
    function withPaymentReversalFee(amount : MonetaryAmount) : BillingPlanBuilder {
      this.withPaymentReversalFeeForCurrency(CurrencyUtil.DefaultCurrency, amount)
      return this;
    }

    @DeprecatedAndRestoredByUpgrade
    function withLowBalanceThreshold(amount : MonetaryAmount) : BillingPlanBuilder {
      this.withLowBalanceThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
      return this;
    }

    @DeprecatedAndRestoredByUpgrade
    function withReviewDisbursementOver(amount : MonetaryAmount) : BillingPlanBuilder {
      this.withReviewDisbursementOverForCurrency(CurrencyUtil.DefaultCurrency, amount)
      return this;
    }

    @DeprecatedAndRestoredByUpgrade
    function withDisbursementOver(amount : MonetaryAmount) : BillingPlanBuilder {
      this.withDisbursementOverForCurrency(CurrencyUtil.DefaultCurrency, amount)
      return this;
    }

    @DeprecatedAndRestoredByUpgrade
    function carryForwardLowBalanceUnder(amount : MonetaryAmount) : BillingPlanBuilder {
      this.carryForwardLowBalanceUnder(CurrencyUtil.DefaultCurrency, amount)
      return this;
    }

    @DeprecatedAndRestoredByUpgrade
    function writeOffLowBalanceUnder(amount : MonetaryAmount) : BillingPlanBuilder {
      this.carryForwardLowBalanceUnder(CurrencyUtil.DefaultCurrency, amount)
      return this;
    }

    @DeprecatedAndRestoredByUpgrade
    function withCurrency(amount : MonetaryAmount) : BillingPlanBuilder {
      this.withSingleCurrency(amount.Currency)
      return this;
    }

}
