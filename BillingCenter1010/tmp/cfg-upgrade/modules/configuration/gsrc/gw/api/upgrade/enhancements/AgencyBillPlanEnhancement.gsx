package gw.api.upgrade.enhancements

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * File created by Guidewire Upgrade Tools.
 */
enhancement AgencyBillPlanEnhancement : AgencyBillPlan {

  @DeprecatedAndRestoredByUpgrade
  property get NetThresholdForSuppressingStmt() : MonetaryAmount {
    return this.getNetThresholdForSuppressingStmt(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set NetThresholdForSuppressingStmt(amount : MonetaryAmount) : void {
    this.setNetThresholdForSuppressingStmtForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get ClearCommissionThreshold() : MonetaryAmount {
    return this.getClearCommissionThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set ClearCommissionThreshold(amount : MonetaryAmount) : void {
    this.setClearCommissionThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get ClearGrossThreshold() : MonetaryAmount {
    return this.getClearGrossThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set ClearGrossThreshold(amount : MonetaryAmount) : void {
    this.setClearGrossThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get ProducerWriteoffThreshold() : MonetaryAmount {
    return this.getProducerWriteoffThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set ProducerWriteoffThreshold(amount : MonetaryAmount) : void {
    this.setProducerWriteoffThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }
}
