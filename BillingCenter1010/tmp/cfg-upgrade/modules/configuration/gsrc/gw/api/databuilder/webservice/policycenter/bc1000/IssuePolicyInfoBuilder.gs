package gw.api.databuilder.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.IssuePolicyInfo

/**
 * Defining generic types so that they don't need to be explicitly declared when using the builder.
 */
@Export
class IssuePolicyInfoBuilder extends IssuePolicyInfoBuilderBase<IssuePolicyInfo, IssuePolicyInfoBuilder> {
}