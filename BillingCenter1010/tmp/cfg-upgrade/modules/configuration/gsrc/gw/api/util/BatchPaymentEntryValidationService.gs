package gw.api.util

uses gw.api.locale.DisplayKey
uses gw.payment.batch.BatchPaymentEntryValidator
uses gw.payment.batch.ValidationOutcome

@Export
class BatchPaymentEntryValidationService {

  private static var validator = new BatchPaymentEntryValidator()

    /**
     * Verifies that there is no Invoice specified if we're making a Suspense Payment
     *
     * @returns an error string if the payment entry is for a suspense payment and Invoice is non-null.
     */
    public static function verifyInvoiceFor(bPaymentEntry: BatchPaymentEntry) : String {
      return validator.validateInvoiceFor(bPaymentEntry).getValidationErrorMessageIfFail()
          .orElse(validateInvoiceNotFrozen(bPaymentEntry))
    }

    /**
     * Verifies that the amount is non null
     *
     * @returns an error string if the amount is null, otherwise just returns null
     */
    public static function verifyAmountFor(bPaymentEntry: BatchPaymentEntry) : String {
      return validator.validateAmountFor(bPaymentEntry).getValidationErrorMessageIfFail().orElse(null)
    }

    /**
     * Verifies that the account exists, if it's a regular payment, and doesn't exist, if it's a suspense payment.
     *
     * @return null if these conditions are true, an error string if they aren't.
     */
    public static function verifyAccountFor(bPaymentEntry: BatchPaymentEntry) : String {
      return validator.validateAccountFor(bPaymentEntry).getValidationErrorMessageIfFail().orElse(null)
    }

    /**
     * Verifies that the policy exists, if it's a regular payment, and doesn't exist,
     * if it's a suspense payment and is not archived
     *
     * @return null if these conditions are true, an error string if they aren't.
     */
    public static function verifyPolicyFor(bPaymentEntry: BatchPaymentEntry) : String {
      return validator.validatePolicyFor(bPaymentEntry).getValidationErrorMessageIfFail()
          .orElse(verifyPolicyIsNotArchived(bPaymentEntry.getPolicyPeriod()))
    }

    /**
     * Verifies that the producer exists, if it's an Agency Money Received payment.
     *
     * @return null if these conditions are true, an error string if they aren't.
     */
    public static function verifyProducerFor(bPaymentEntry: BatchPaymentEntry) : String {
      return validator.validateProducerFor(bPaymentEntry).getValidationErrorMessageIfFail().orElse(null)
    }

  public static function verifyPolicyIsNotArchived(policyNumber : String ) : String {
    return verifyPolicyIsNotArchived(PolicyPeriod.finder.getPolicyPeriodIncludingClosedFromNumber(policyNumber))
  }

  private static function verifyPolicyIsNotArchived(policyPeriod: PolicyPeriod)  : String{
    return (policyPeriod != null and policyPeriod.Archived)
        ? DisplayKey.get("Web.BatchPaymentEntriesLV.Error.PolicyArchived")
        : null
  }

  private static function validateInvoiceNotFrozen(bPaymentEntry: BatchPaymentEntry): String{
      return bPaymentEntry.Invoice?.Frozen ? DisplayKey.get("Web.InvoiceSearchConverter.Error.FrozenInvoice") : null
  }

  /**
   * Verifies that the amount is non null
   *
   * @throws gw.api.util.DisplayableException if not valid
   */
  public static function verifyAmountOnChange(bPaymentEntry: BatchPaymentEntry){
    if(bPaymentEntry.Amount != null){
      handleValidationOutcomeAsDiplayableException(
          DisplayKey.get("Web.BatchPaymentEntriesLV.Amount"),
          validator.validateAmountFor(bPaymentEntry))
    }
  }

  private static function handleValidationOutcomeAsDiplayableException(label: String, validationOutcome: ValidationOutcome){
    validationOutcome.getValidationErrorMessageIfFail().ifPresent(
        \message -> throwLabelledDiplayableException(label, message)
    )
  }

  private static function throwLabelledDiplayableException(label : String, message: String){
    var formattedMessage = "${label} : ${message}"
    throw new DisplayableException(formattedMessage)
  }

}