package gw.api.domain.commission

@Export
public interface CommissionDrilldownRow {

  property get Name() : String

  property get CommissionableGross() : String

  property get Rate() : String

  property get Active() : Boolean

  property get Reserve() : String
  property get Earned() : String
  property get Paid() : String
  property get WrittenOff() : String
  property get Total() : String

  property get RowType() : String

  property get Children() : List<CommissionDrilldownRow>
}