package gw.api.databuilder.webservice.policycenter.bc1000

uses gw.api.databuilder.BCDataBuilder
uses gw.api.databuilder.MonetaryTestConstants
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PCAccountInfo_InsuredContact
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewAccountInfo

@Export
class PCNewAccountInfoBuilder {

  var _pcNewAccountInfo : PCNewAccountInfo

  construct() {
    _pcNewAccountInfo = new PCNewAccountInfo()
    withBillingLevel(BillingLevel.TC_ACCOUNT)
    withInsuredIsBilling(true)

    var randomWordPair = BCDataBuilder.createRandomWordPair();
    withAccountNumber(randomWordPair)
    withAccountName(randomWordPair)
  }

  public function create() : PCNewAccountInfo {
    if(_pcNewAccountInfo.InsuredContact == null){
      _pcNewAccountInfo.InsuredContact = createDefaultInsuredContactInfo()
    }
    return _pcNewAccountInfo
  }

  public function withBillingLevel(billingLevel : BillingLevel) : PCNewAccountInfoBuilder {
    _pcNewAccountInfo.BillingLevel = billingLevel.Code
    return this
  }

  public function withInsuredIsBilling(insuredIsBilling : boolean) : PCNewAccountInfoBuilder {
    _pcNewAccountInfo.InsuredIsBilling = insuredIsBilling
    return this
  }

  public function withAccountNumber(accountNumber : String) : PCNewAccountInfoBuilder {
    _pcNewAccountInfo.AccountNumber = accountNumber
    return this
  }

  public function withAccountName(accountName : String) : PCNewAccountInfoBuilder {
    _pcNewAccountInfo.AccountName = accountName
    return this
  }

  private function createDefaultInsuredContactInfo() : PCAccountInfo_InsuredContact {
    var insuredContact = new PCAccountInfo_InsuredContact()
    insuredContact.$TypeInstance = new PCContactInfoBuilder().create()
    return insuredContact
  }
}