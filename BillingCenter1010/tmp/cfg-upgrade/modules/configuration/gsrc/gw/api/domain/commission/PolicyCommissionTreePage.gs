package gw.api.domain.commission

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.tree.RowTreeRootNode

uses java.util.ArrayList
uses java.util.List

@Export
class PolicyCommissionTreePage {
  var _policyCommissionTree : RowTreeRootNode as readonly PolicyCommissionTree
  var _policyCommissionDrilldownRows : List<PolicyCommissionDrilldownRow>

  construct(policyPeriod : PolicyPeriod, policyRole : PolicyRole) {
    refresh(policyPeriod, policyRole)
  }

  public final function refresh(policyPeriod : PolicyPeriod, policyRole : PolicyRole) {
    _policyCommissionDrilldownRows = new ArrayList<PolicyCommissionDrilldownRow>()

    if (policyPeriod == null) {
      return
    }

    var query = Query.make(ItemCommission)
    var policyCommissionTable = query.join("PolicyCommission")
    policyCommissionTable.compare("Role", Relop.Equals, policyRole)
    policyCommissionTable.compare("PolicyPeriod", Relop.Equals, policyPeriod)

    query.select().each(\ ic -> {
      var row = getPolicyCommissionRow(ic.PolicyCommission)

      row.addCommissionAmounts(ic)
    })

    _policyCommissionTree = new RowTreeRootNode(
        _policyCommissionDrilldownRows.sortBy(\ row -> row._policyCommission.ProducerCode.toString()),
            \ pc -> (pc as CommissionDrilldownRow).Children
    )
  }

  private function getPolicyCommissionRow(policyCommission : PolicyCommission) : PolicyCommissionDrilldownRow {
    var row = _policyCommissionDrilldownRows.firstWhere( \ row -> row._policyCommission == policyCommission)
    if (row == null) {
      row = new PolicyCommissionDrilldownRow(policyCommission)
      _policyCommissionDrilldownRows.add(row)
    }
    return row
  }
}