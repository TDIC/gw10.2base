package gw.api.upgrade.enhancements

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * File created by Guidewire Upgrade Tools.
 */
enhancement DelinquencyPlanEnhancement : DelinquencyPlan {

  @DeprecatedAndRestoredByUpgrade
  property get AcctEnterDelinquencyThreshold() : MonetaryAmount {
    return this.getAcctEnterDelinquencyThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set AcctEnterDelinquencyThreshold(amount : MonetaryAmount) : void {
    this.setAcctEnterDelinquencyThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get CancellationThreshold() : MonetaryAmount {
    return this.getCancellationThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set CancellationThreshold(amount : MonetaryAmount) : void {
    this.setCancellationThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get ExitDelinquencyThreshold() : MonetaryAmount {
    return this.getExitDelinquencyThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set ExitDelinquencyThreshold(amount : MonetaryAmount) : void {
    this.setExitDelinquencyThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get LateFee() : MonetaryAmount {
    return this.getLateFee(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set LateFee(amount : MonetaryAmount) : void {
    this.setLateFeeForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get PolEnterDelinquencyThreshold() : MonetaryAmount {
    return this.getPolEnterDelinquencyThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set PolEnterDelinquencyThreshold(amount : MonetaryAmount) : void {
    this.setPolEnterDelinquencyThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get ReinstatementFee() : MonetaryAmount {
    return this.getReinstatementFee(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set ReinstatementFee(amount : MonetaryAmount) : void {
    this.setReinstatementFeeForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get WriteoffThreshold() : MonetaryAmount {
    return this.getWriteoffThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set WriteoffThreshold(amount : MonetaryAmount) : void {
    this.setWriteoffThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

}
