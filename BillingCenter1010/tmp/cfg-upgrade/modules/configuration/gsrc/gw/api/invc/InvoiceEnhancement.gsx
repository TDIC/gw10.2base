package gw.api.invc

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

uses java.util.Date

@Export
enhancement InvoiceEnhancement : entity.Invoice {
  /**
   * Advance the clock to the day after the invoice's event date and make it billed
   */
  function makeBilledWithClockAdvanceToEventDate(){
    gw.transaction.Transaction.runWithNewBundle( \b -> {
      var invoice = b.add(this)
      if(invoice.EventDate.addDays(1).after(Date.CurrentDate) ){
        (invoice.EventDate.addDays(1)).setClock()
      }
      invoice.makeBilled()
    })
  }

  /*
 * returns the total outstanding amount of the invoice
 */
  property get TotalOutstandingAmountTDIC() : MonetaryAmount {
    var outstandingAmount = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    outstandingAmount = this.InvoiceItems.sum(\ii -> (ii as InvoiceItem).OutstandingBalanceTDIC)
    if(outstandingAmount != null){
      return outstandingAmount
    }else {
      return new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    }
  }
}
