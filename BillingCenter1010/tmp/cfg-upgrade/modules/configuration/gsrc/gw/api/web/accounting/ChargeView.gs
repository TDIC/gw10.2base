package gw.api.web.accounting

uses gw.pl.currency.MonetaryAmount

uses java.math.BigDecimal

@Export
class ChargeView extends ChargeViewBase {
  private static var _secondary = PolicyRole.TC_SECONDARY
  private static var _referrer = PolicyRole.TC_REFERRER

  construct(charge : Charge) {
    super(charge)
  }

  /**
   * @return The default secondary commission rate override for this charge.
   */
  property get SecondaryCommissionRateOverride() : BigDecimal {
      return getCommissionRateOverride(_secondary);
  }

  /**
   * Overrides the commission rate on the default secondary charge commission for the given role.
   *
   * @param rate The rate to override the default secondary charge commission with.
   */
  property set SecondaryCommissionRateOverride(rate : BigDecimal) {
    setCommissionRateOverride(_secondary, rate);
  }

  /**
   * @return The secondary commission amount override for this charge on the given role.
   */
  property get SecondaryCommissionAmountOverride() : MonetaryAmount {
    return getCommissionAmountOverride(_secondary);
  }

  /**
   * Overrides the commission amount on the secondary charge commission for the given role.
   *
   * @param amount The amount to override the secondary charge commission with.
   */
  property set SecondaryCommissionAmountOverride(amount : MonetaryAmount) {
    setCommissionAmountOverride(_secondary, amount);
  }

  /**
   * @return The default referrer commission rate override for this charge.
   */
  property get ReferrerCommissionRateOverride() : BigDecimal{
    return getCommissionRateOverride(_referrer);
  }

  /**
   * Overrides the commission rate on the default referrer charge commission for the given role.
   *
   * @param rate The rate to override the default referrer charge commission with.
   */
  property set ReferrerCommissionRateOverride(rate : BigDecimal) {
    setCommissionRateOverride(_referrer, rate);
  }

  /**
   * @return The referrer commission amount override for this charge on the given role.
   */
  property get ReferrerCommissionAmountOverride() : MonetaryAmount {
    return getCommissionAmountOverride(_referrer);
  }

  /**
   * Overrides the commission amount on the referrer charge commission for the given role.
   *
   * @param amount The amount to override the referrer charge commission with.
   */
  property set ReferrerCommissionAmountOverride(amount : MonetaryAmount) {
    setCommissionAmountOverride(_referrer, amount);
  }
}