package gw.invoice

uses entity.Invoice
uses entity.InvoiceStream
uses gw.api.domain.charge.ChargeInitializer
uses gw.api.domain.invoice.ChargeInstallmentChanger
uses gw.api.domain.invoice.ChargeSlicer
uses gw.api.domain.invoice.ChargeSlicers
uses gw.api.financials.MonetaryAmounts
uses gw.api.locale.DisplayKey
uses gw.api.util.BCBigDecimalUtil
uses gw.api.util.BCDateUtil
uses gw.api.util.BCUtils
uses gw.api.util.DateUtil
uses gw.api.util.Ratio
uses gw.entity.IEntityPropertyInfo
uses gw.lang.reflect.features.PropertyReference
uses gw.pl.currency.MonetaryAmount

uses java.math.RoundingMode
uses java.util.stream.Collectors

@Export
class MatchCancellationSnapshotsChargeSlicer implements ChargeSlicer {

  static final var isNonArrayExtension = \prop : IEntityPropertyInfo -> BCUtils.isNonArrayExtension(prop)
  static final var invoiceItemExtensionPropNames = InvoiceItem.TypeInfo.Properties
      .stream()
      .filter(\prop -> prop typeis IEntityPropertyInfo)
      .map(\prop -> prop as IEntityPropertyInfo)
      .filter(isNonArrayExtension)
      .map(\ prop -> prop.Name)
      .collect(Collectors.toList<String>())

  static final var cancellationItemSnapshotExtProps = InvoiceItemCancellationSnapshot.TypeInfo.Properties
      .stream()
      .filter(\prop -> prop typeis IEntityPropertyInfo)
      .map(\prop -> prop as IEntityPropertyInfo)
      .filter(isNonArrayExtension)
      .filter(\ prop -> invoiceItemExtensionPropNames.contains(prop.Name))
      .collect(Collectors.toList<IEntityPropertyInfo>())

  override function createEntries(chargeInitializer: ChargeInitializer) {
    var snapshots = InvoiceItemCancellationSnapshot.finder
        .findPolicyPeriodCancellationSnapshotsForChargeGroupAndChargePattern(chargeInitializer.getPolicyPeriod(), chargeInitializer.getChargeGroup(), chargeInitializer.getChargePattern())
        .toList()

    createEntriesFromSnapshots(chargeInitializer, snapshots)

    var snapshotTotal = snapshots.sum(chargeInitializer.Amount.Currency, \s -> s.Amount)
    var difference = chargeInitializer.Amount.subtract(snapshotTotal)
    if (difference.IsPositive) {
      // Add positive adjustment entry
      chargeInitializer.addEntry(difference, InvoiceItemType.TC_REINSTATEMENTADJUSTMENT, DateUtil.currentDate())
    } else if (difference.IsNegative){
      // Update entries with pro-rata amounts
      updateEntriesProRataForNegativeAdjustment(chargeInitializer, snapshotTotal)
    }
  }

  override function recreateInvoiceItems(changer: ChargeInstallmentChanger, totalAmountForInvoiceItems: MonetaryAmount) {
    throw new UnsupportedOperationException(DisplayKey.get("Java.ChargeSlicerSelector.MatchCancellationSnapshotsChargeSlicer.RecreateInvoiceItemsNotSupported"))
  }

  private function createEntriesFromSnapshots(chargeInitializer: ChargeInitializer, snapshots: List<InvoiceItemCancellationSnapshot>) {
    var processedSnapshots = mergeAndCreateEntriesFromSnapshots(chargeInitializer, snapshots, {InvoiceItemType.TC_DEPOSIT, InvoiceItemType.TC_ONETIME})

    var remainingSnapshots = new HashSet<InvoiceItemCancellationSnapshot>(snapshots)
    remainingSnapshots.removeAll(processedSnapshots)
    remainingSnapshots.each(\snapshot -> createEntryFromSnapshot(chargeInitializer, snapshot, snapshot.Amount))
  }

  /**
   * If multiple snapshots found for the given type,
   * - a merged entry is created with the earliest date and combined amount
   * - extension properties and commission rate overrides will be copied from the earliest snapshot
   */
  private function mergeAndCreateEntriesFromSnapshots(chargeInitializer: ChargeInitializer, snapshots: List<InvoiceItemCancellationSnapshot>, types: InvoiceItemType[]) : Set<InvoiceItemCancellationSnapshot> {
    var processedSnapshots = new HashSet<InvoiceItemCancellationSnapshot>()
    for (type in types) {
      var snapshotsOfGivenType = snapshots.where(\snapshot -> snapshot.getType() == type)
      if (snapshotsOfGivenType.size() > 0) {
        var earliestSnapshot = snapshotsOfGivenType.sortBy(\snapshot -> snapshot.EventDate).first();
        var mergedAmount = snapshotsOfGivenType.sum(chargeInitializer.Amount.Currency, \deposit -> deposit.Amount)
        createEntryFromSnapshot(chargeInitializer, earliestSnapshot, mergedAmount, snapshotsOfGivenType.size() > 1)
        processedSnapshots.addAll(snapshotsOfGivenType)
      }
    }
    return processedSnapshots
  }

  private function createEntryFromSnapshot(chargeInitializer: ChargeInitializer, snapshot: InvoiceItemCancellationSnapshot, amount: MonetaryAmount, isMerged : boolean = false) {
    var entry = chargeInitializer.addEntry(amount, snapshot.Type, snapshot.EventDate)
    updateInvoiceIfNeeded(snapshot, entry, chargeInitializer.InvoiceStream);
    if (!isMerged) {
      copyInstallmentNumber(snapshot, entry)
      copyCommissionRateOverrides(snapshot, entry)
      copyExtensions(snapshot, entry)
    }
  }


  private function updateInvoiceIfNeeded(snapshot : InvoiceItemCancellationSnapshot, entry : ChargeInitializer.Entry, invoiceStream : InvoiceStream) {
    //If invoice bill date is in the past, or if invoice details already match, don't do anything
    if(DateUtil.compareIgnoreTime(snapshot.InvoiceBillDate, DateUtil.currentDate()) < 0 or invoiceDetailsMatch(entry.Invoice, snapshot)){
      return
    }

    var existingInvoice = invoiceStream.getInvoices()
        .firstWhere(\elt -> elt.Planned && invoiceDetailsMatch(elt, snapshot))

    if (existingInvoice == null) {
      existingInvoice = invoiceStream.createAndAddInvoice(snapshot.InvoiceBillDate, snapshot.InvoiceDueDate);
      existingInvoice.AdHoc = snapshot.AdhocInvoice
    }
    entry.Invoice = existingInvoice
  }

  private function invoiceDetailsMatch(entryInvoice : Invoice, snapshot : InvoiceItemCancellationSnapshot) : boolean {
    return entryInvoice.AdHoc == snapshot.AdhocInvoice and
        BCDateUtil.isSameDayIgnoringTime(entryInvoice.EventDate, snapshot.InvoiceBillDate) and
        BCDateUtil.isSameDayIgnoringTime(entryInvoice.PaymentDueDate, snapshot.InvoiceDueDate)
  }

  private function copyInstallmentNumber(snapshot : InvoiceItemCancellationSnapshot, entry : ChargeInitializer.Entry) {
    entry.setInstallmentNumber(snapshot.getInstallmentNumber())
  }

  private function copyCommissionRateOverrides(snapshot : InvoiceItemCancellationSnapshot, entry : ChargeInitializer.Entry){
    for (var overrideSnapshot in snapshot.getCommissionRateOverrides()) {
      entry.overrideCommissionRate(overrideSnapshot.getRole(), overrideSnapshot.getRate())
    }
  }

  private function copyExtensions(snapshot : InvoiceItemCancellationSnapshot, entry : ChargeInitializer.Entry) {
    for (extensionProp in cancellationItemSnapshotExtProps) {
      entry.setExtensionProperty(
          new PropertyReference<InvoiceItem, Object>(InvoiceItem, extensionProp.Name),
          extensionProp.Accessor.getValue(snapshot));
    }
  }

  /**
   * 1. Find deposit entry and apply entire amount if it is the only item else apply pro-rata amount (default rounding mode)
   * 2. Find onetime entry and apply entire amount if it is the only item else apply pro-rata amount (rounding down mode)
   * 2. Update all entries with pro-rata amounts
   * 3. Apply remainder treatment on all entries except deposit
   */
  private function updateEntriesProRataForNegativeAdjustment(chargeInitializer: ChargeInitializer, snapshotTotal: MonetaryAmount): void {
    var currency = chargeInitializer.Amount.Currency
    var entriesToUpdate = new ArrayList<ChargeInitializer.Entry>(chargeInitializer.Entries)
    var remainingSnapshotsTotal = snapshotTotal
    var remainingAmountToDistribute = chargeInitializer.Amount

    //Update amount for OneTime entry - ProRata amount if other types present with OneTime item
    var onetimeEntry = entriesToUpdate.firstWhere(\entry -> entry.InvoiceItemType == TC_ONETIME)
    if (onetimeEntry != null) {
      if (entriesToUpdate.size() == 1) {
        onetimeEntry.setAmount(remainingAmountToDistribute)
        return
      }
      var referenceSnapshotAmount = onetimeEntry.getAmount()
      var proRataAmount = calculateProRataAmount(referenceSnapshotAmount, remainingSnapshotsTotal, remainingAmountToDistribute, currency, RoundingMode.DOWN)
      onetimeEntry.setAmount(proRataAmount)
      remainingSnapshotsTotal = remainingSnapshotsTotal.subtract(referenceSnapshotAmount)
      remainingAmountToDistribute = remainingAmountToDistribute.subtract(proRataAmount)
      entriesToUpdate.remove(onetimeEntry)
    }

    //Update amount for Deposit entry - ProRata amount if Deposit and Installments present
    var depositEntry = entriesToUpdate.firstWhere(\entry -> entry.InvoiceItemType == TC_DEPOSIT)
    if(depositEntry != null) {
      if (entriesToUpdate.size() == 1) {
        depositEntry.setAmount(remainingAmountToDistribute)
        return
      }
      var proRataAmount = calculateProRataAmount(depositEntry.getAmount(), remainingSnapshotsTotal, remainingAmountToDistribute, currency, BCBigDecimalUtil.getRoundingMode())
      depositEntry.setAmount(proRataAmount)
      remainingAmountToDistribute = remainingAmountToDistribute.subtract(proRataAmount)
      entriesToUpdate.remove(depositEntry)
    }

    //Update amount ProRata for remaining entries with remainder treatment
    updateEntriesWithProRataAmountAndRemainderDistribution(entriesToUpdate, remainingAmountToDistribute)
  }

  private function calculateProRataAmount(referenceAmount : MonetaryAmount,
                                          snapshotTotal: MonetaryAmount,
                                          amountToDistribute: MonetaryAmount,
                                          currency: Currency,
                                          roundingMode: RoundingMode) : MonetaryAmount {
    var ratio = Ratio.valueOf(referenceAmount, snapshotTotal)
    return ratio.multiply(amountToDistribute.Amount).toMonetaryAmount(currency, roundingMode)
  }

  /**
   * Update pro-rata amount (rounding mode DOWN) on the entries
   */
  private function updateEntriesWithProRataAmountAndRemainderDistribution(entriesToUpdate: List<ChargeInitializer.Entry>, amountToDistribute: MonetaryAmount) {
    if (entriesToUpdate.size() > 0) {
      entriesToUpdate = entriesToUpdate.sortBy(\e -> e.EventDate)
      var proRataAmountsByEntry = ChargeSlicers.distributeProportionallyBasedOnEntryAmounts(amountToDistribute, entriesToUpdate)
      entriesToUpdate.each(\entry -> entry.setAmount(proRataAmountsByEntry.get(entry)))
    }
  }
}
