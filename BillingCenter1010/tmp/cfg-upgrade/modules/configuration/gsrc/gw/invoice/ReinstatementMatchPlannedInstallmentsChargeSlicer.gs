package gw.invoice

uses com.google.common.collect.ImmutableList
uses gw.api.domain.charge.ChargeInitializer

/**
 * The ReinstatementMatchPlannedInstallmentsChargeSlicer will slice the new Charge's Amount by matching the planned installments
 * of the reference Charge(s) and, aggregating the installments by Invoice, distribute the amount proportionally among
 * the planned installments.
 */
@Export
class ReinstatementMatchPlannedInstallmentsChargeSlicer extends MatchPlannedInstallmentsChargeSlicer {
  /**
   * Returns the Charges of the ChargeInitializer's PolicyPeriod which match the ChargeInitializer's ChargePattern.
   * @param chargeInitializer the ChargeInitializer which is using this ReinstatementMatchPlannedInstallmentsChargeSlicer as its
   *                          ChargeSlicer
   */
  function referenceCharges(chargeInitializer: ChargeInitializer) : ImmutableList<Charge> {
    return MatchPlannedInstallmentsMethods.referenceChargesForReinstatement(chargeInitializer)
  }

  /**
   * Returns the planned InvoiceItems of the reference Charge(s) of a ChargeInitializer where the EventDate is on or
   * after the ChargeInitializer's EffectiveDate
   */
  function matchingInvoiceItems(referenceCharges : ImmutableList<Charge>, chargeInitializer: ChargeInitializer) : InvoiceItem[] {
    return MatchPlannedInstallmentsMethods.matchingInvoiceItemsForReinstatement(referenceCharges, chargeInitializer)
  }

  /**
   * Returns true when at least one planned down payment exists.
   */
  function includeDownPayment(referenceCharges : ImmutableList<Charge>, chargeInitializer : ChargeInitializer) : boolean{
    return referenceCharges*.InvoiceItems.hasMatch(\invoiceItem -> invoiceItem.Deposit and invoiceItem.Invoice.Planned)
  }
}