package gw.scriptparameter

uses java.math.BigDecimal
/**
* File created by Guidewire Upgrade Tools.
*/
@Export
enhancement Ex_ScriptParametersEnhancement : ScriptParameters {

    public static property get TDIC_DataSource(): String {
        return ScriptParameters.getParameterValue("TDIC_DataSource") as String
    }

    public static property get APWInvoiceDayOfMonth1(): Integer {
        return ScriptParameters.getParameterValue("APWInvoiceDayOfMonth1") as Integer
    }

    public static property get APWInvoiceDayOfMonth2(): Integer {
        return ScriptParameters.getParameterValue("APWInvoiceDayOfMonth2") as Integer
    }

    public static property get DocumentApprovalNeeded(): Boolean {
        return ScriptParameters.getParameterValue("DocumentApprovalNeeded") as Boolean
    }

    public static property get SecurityZoneUpdated(): Boolean {
        return ScriptParameters.getParameterValue("SecurityZoneUpdated") as Boolean
    }

}