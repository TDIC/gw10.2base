package gw.pcf.note

uses gw.api.util.LocaleUtil

@Export
class NoteHelper {

  private construct() {}
  
  static function createNoteWithCurrentUsersLanguage() : Note {
    var note = new Note()
    note.Language = LocaleUtil.getCurrentLanguageType()
    //  hermiaK, 11/18/2014, US80: setting the "Security Type" to default to "Internal Only"
    note.SecurityType = NoteSecurityType.TC_INTERNALONLY
    return note
  }

}
