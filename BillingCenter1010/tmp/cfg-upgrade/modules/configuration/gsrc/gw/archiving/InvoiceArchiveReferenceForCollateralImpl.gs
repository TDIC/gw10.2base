package gw.archiving

@Export
class InvoiceArchiveReferenceForCollateralImpl implements ArchiveReferenceByChargeOwner {
  var _invoiceArchiveReferenceForCollateral: InvoiceArchiveReferenceForCollateral

  construct(invoiceArchiveReferenceForCollateral: InvoiceArchiveReferenceForCollateral) {
      _invoiceArchiveReferenceForCollateral = invoiceArchiveReferenceForCollateral
  }

  override property get ChargeTAccountOwner() : TAccountOwner {
    return _invoiceArchiveReferenceForCollateral.Collateral
  }
}