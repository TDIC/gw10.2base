package gw.archiving

@Export
class DistArchiveReferenceForCollateralRequirementImpl implements ArchiveReferenceByChargeOwner {
  var _distArchiveReferenceForCollateralRequirement: DistArchiveReferenceForCollateralRequirement

  construct(distArchiveReferenceForCollateralRequirement: DistArchiveReferenceForCollateralRequirement) {
      _distArchiveReferenceForCollateralRequirement = distArchiveReferenceForCollateralRequirement
  }

  override property get ChargeTAccountOwner() : TAccountOwner {
    return _distArchiveReferenceForCollateralRequirement.CollateralRequirement
  }
}