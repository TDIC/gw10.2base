package com.tdic.util.misc

uses gw.api.email.EmailContact
uses gw.pl.logging.LoggerCategory

uses java.lang.Exception

/**
 * 10/16/2014 Alvin Lee
 *
 * Utility class for sending email.
 */
class EmailUtil {
  private static var _logger = LoggerCategory.TDIC_INTEGRATION
  /*
   * Utility method to send email.  Note that the sender name and sender email will use the default defined in the
   * emailMessageTransport plugin, so those will not be defined in this method.
   */

  @Param("recipientEmailAddresses", "A comma-delimited String containing all email addresses of the recipients")
  @Param("subject", "A String containing the subject of the email")
  @Param("body", "A String containing the body of the email")
  @Returns("A boolean indicating if the email was successfully sent")
  static public function sendEmail(recipientEmailAddresses: String, subject: String, body: String): boolean {
    _logger.debug("Email#sendEmail() - Entering")
    _logger.debug("  recipientEmailAddresses = " + recipientEmailAddresses)
    _logger.debug("  subject = " + subject)
    _logger.debug("  body = " + body)

    if (recipientEmailAddresses.Empty) {
      _logger.error("Email#sendEmail - Cannot send email without any recipients")
      return false
    }

    try {
      var sender = new EmailContact()
      var recipientEmails = recipientEmailAddresses.split(",");
      var emailToSend: gw.api.email.Email = null
      for (emailAddress in recipientEmails) {
        var recipient = new EmailContact()
        recipient.EmailAddress = emailAddress.trim()
        if (emailToSend == null) {
          emailToSend = new gw.api.email.Email(recipient, sender, subject, body)
          _logger.debug("Email#sendEmail() - Email initialized with recipient: " + recipient.EmailAddress)
        }
        else {
          emailToSend.addToRecipient(recipient)
          _logger.debug("Email#sendEmail() - Added Recipient: " + recipient.EmailAddress)
        }
      }
      gw.api.email.EmailUtil.sendEmailWithBody(null, emailToSend)
      _logger.debug("Email#sendEmail() - Email added to queue")
      return true
    } catch (e: Exception) {
      _logger.error("Email#sendEmail() - Exception occurred while sending email: " + e.LocalizedMessage, e)
      return false
    }
  }
}
