package com.tdic.util.flatfile.model.classes

uses java.io.Serializable

class Field implements Serializable {
  private var _startPos: int as StartPosition
  private var _length: int as Length
  private var _fieldName: String as Name
  private var _param1: String as Parameter1
  private var _format: String as Format
  private var _justify: String as Justify
  private var _fill: String as Fill
  private var _truncate: String as Truncate
  private var _stripType: String as StripType
  private var _strip: String as Strip
  private var _data: String as Data
  private var _param2: String as Parameter2
  construct() {
  }

  construct(startPos: int, fieldLength: int, fieldName: String,
            param1: String, format: String, justify: String, fill: String, truncate: String,
            stripType: String, strip: String, data: String, param2: String) {
    _startPos = startPos
    _length = fieldLength
    _fieldName = fieldName
    _param1 = param1
    _format = format
    _justify = justify
    _fill = fill
    _truncate = truncate
    _stripType = stripType
    _data = data
    _param2 = param2
  }
}