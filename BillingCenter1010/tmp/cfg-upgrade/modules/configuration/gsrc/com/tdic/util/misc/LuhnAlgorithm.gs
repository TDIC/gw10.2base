package com.tdic.util.misc

uses java.lang.Character
uses java.lang.Integer
uses java.lang.StringBuffer
uses java.util.HashMap
uses java.util.Map

/**
 * User: skiriaki/ktavva
 * Date: 3/23/15
 */
class LuhnAlgorithm {
  static public function calcChecksum(num: String): int {
    var _sum: int = 0

    var _len = num.length() - 1
    var _multiplier = 1
    for (var i in 0.._len) {
      var _tmpI = Integer.parseInt(num.charAt(i))
      var _product: int
      _product = _tmpI * _multiplier

      // Add the digits together if the doubling gives a two digit number (cannot be more than 9*2=18)
      if (_product > 9){
        _product -= 9
      }
      _sum += _product

      // Switch multiplier
      if (_multiplier == 1) {
        _multiplier = 2
      } else {
        _multiplier = 1
      }
    }

    // Checksum is the sum/10 if remainder is zero. Else it's the remainder subtracted from 10

    var _c2 = _sum % 10
    if (_c2 == 0) {
      return _c2
    } else {
      return (10 - _c2)
    }
  }

  private static function buildMap(): Map<Character, Character> {
    var keys = new Character[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'}
    var values = new Character[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5'}

    var map = new HashMap<Character, Character>()
    keys.eachWithIndex(\key, i -> {
      map.put(key, values[i])
    })
    return map
  }

  public static function convertPolicyNum(pn: String): String {
    var _map = buildMap()
    pn = pn.toUpperCase()

    var str = new StringBuffer()

    for (var j in 1..pn.length()) {
      str.append(_map.get(pn.charAt(j - 1)))
    }
    return str.toString()
  }
}