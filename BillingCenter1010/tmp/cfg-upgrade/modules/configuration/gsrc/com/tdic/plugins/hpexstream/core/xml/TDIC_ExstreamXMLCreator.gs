/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.xml

uses tdic.bc.integ.plugins.hpexstream.bo.TDIC_DocumentRequestSource
uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateIds

abstract class TDIC_ExstreamXMLCreator {
  construct() {
  }

  /**
   * US555
   * 10/13/2014 shanem
   *
   * Returns the Exstream transaction data XML for a given GW entity.  There are various strategies for
   * indicating which Exstream report template to generate.  This sample implementation assumes
   * that the Exstream report name is passed in the transaction data XML.
   */
  @Param("aTemplateId", "The Id of the Exstream template being requested.")
  @Param("anEventName", "Event name.")
  @Param("isSystemGenerated", "Boolean indicating if the document is being generated by the system.")
  @Param("aBusinessObjectClass", "Class to create.")
  @Param("aSchema", "Schema to use in creating the model.")
  @Returns("The Exstream transaction data XML.")
  abstract function generateTransactionXML(aTemplateIdList: TDIC_TemplateIds, anEventName: String, isSystemGenerated: boolean, aBusinessObjectClass: String, aSchema: String): String

  /**
   * GW526
   * 12/07/2015 Kesava Tavva
   *
   * Returns the Exstream transaction data XML for a given GW entity.  There are various strategies for
   * indicating which Exstream report template to generate.  This sample implementation assumes
   * that the Exstream report name is passed in the transaction data XML.
   */
  @Param("aTemplateId", "The Id of the Exstream template being requested.")
  @Param("anEventName", "Event name.")
  @Param("isSystemGenerated", "Boolean indicating if the document is being generated by the system.")
  @Param("aBusinessObjectClass", "Class to create.")
  @Param("aSchema", "Schema to use in creating the model.")
  @Param("docRequestSource", "Document request source details")
  @Returns("The Exstream transaction data XML.")
  abstract function generateTransactionXML(aTemplateIdList: TDIC_TemplateIds, anEventName: String, isSystemGenerated: boolean, aBusinessObjectClass: String, aSchema: String, docRequestSource: TDIC_DocumentRequestSource): String
}
