package acc.onbase.webservice

uses acc.onbase.util.LoggerFactory
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 */

/**
 * Guidewire web service returns billing center account/policy/producer metadata information.
 */
@WsiWebService("http://onbase/acc/billing/webservice/QueryBillingAPI")
@WsiPermissions({SystemPermissionType.TC_SOAPADMIN})
@WsiAvailability(MULTIUSER)
class QueryBillingAPI {
  /**
   * Logger for OnBaseDMS
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  /**
   * Web service method returns the account GX model.
   *
   * @param accountNumber
   * @return the account GX model
   */
  function getAccountQueryModel(accountNumber: String): acc.onbase.api.si.model.accountquerymodel.Account {
    var account: Account = null
    if (accountNumber.NotBlank) {
      account = gw.api.database.Query.make(Account).compare("AccountNumber", Equals, accountNumber).select().getAtMostOneRow()
      if (account == null) {
        //invalid account number entered
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", accountNumber))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", accountNumber))
      }
    } else {
      // no account number entered
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingAccountNumber"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingAccountNumber"))
    }

    return new acc.onbase.api.si.model.accountquerymodel.Account(account)
  }

  /**
   * Web service method returns the producer(s) GX model.
   *
   * @param producerName The Name of the producer
   * @param producerCode One of the producer Codes
   * @return a list of GX Model containing producer models
   */
  function getProducerQueryModel(producerName: String, producerCode: String): List<acc.onbase.api.si.model.producerquerymodel.Producer> {
    var producersModel = new ArrayList<acc.onbase.api.si.model.producerquerymodel.Producer>()
    if (producerCode != null) {
      var codes = Query.make(entity.ProducerCode).compare(ProducerCode#Code, Equals, producerCode).select()
      if (codes.isEmpty()) {
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ProducerCodeNotFound", producerCode))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ProducerCodeNotFound", producerCode))
      }
      foreach (code in codes) {
        producersModel.add(new acc.onbase.api.si.model.producerquerymodel.Producer(code.Producer))
      }
    } else if (producerName != null) {
      var producers = Query.make(entity.Producer).compare(Producer#Name, Equals, producerName).select()
      if (producers.isEmpty()) {
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ProducerNotFound", producerName))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ProducerNotFound", producerName))
      }
      foreach (producer in producers) {
        producersModel.add(new acc.onbase.api.si.model.producerquerymodel.Producer(producer))
      }
    } else {
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingProducerCodeAndName"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingProducerCodeAndName"))
    }
    return producersModel
  }
}
