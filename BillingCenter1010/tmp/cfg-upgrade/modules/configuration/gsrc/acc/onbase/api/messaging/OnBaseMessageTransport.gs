package acc.onbase.api.messaging

uses acc.onbase.api.service.ServicesManager
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses gw.api.util.DateUtil
uses gw.plugin.InitializablePlugin
uses gw.plugin.messaging.MessageTransport

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * OnBase Message Transport class to send document archive requests to OnBase.
 * <p>
 * Last Changes:
 * 09/27/2016 - Alicia Schroeder
 * * Initial implementation.
 * <p>
 * 08/25/2017 - Anirudh Mohan
 * * Updated BC Security Zone Updates to use EIS Message Items
 **/

/**
 * Billing Center 8 OnBase MessageTransport plugin code.
 */
class OnBaseMessageTransport implements MessageTransport, InitializablePlugin {

  private var _destinationId: int

  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)


  override function send(message: entity.Message, transformedPayload: String) {
    var service = ServicesManager.getSubmitMessage()

    try {
      var messageID = service.submitMessage(transformedPayload)
      message.SenderRefID = Long.toString(messageID)
      message.reportAck()
    } catch (ex: Exception) {
      if (message.RetryCount < OnBaseConfigurationFactory.Instance.MaxRetries) {
        _logger.debug("Error during message transport, scheduling retry", ex)
        var retryTime = DateUtil.addSeconds(DateUtil.currentDate(), (message.RetryCount + 1) * OnBaseConfigurationFactory.Instance.RetryIntervalSeconds)
        message.reportError(retryTime)
      } else {
        _logger.error("Reached maximum retry count, marking message as failed", ex)
        message.reportError(ErrorCategory.TC_MAXRETRIESREACHED)
      }
    }
  }

  override function suspend() {
  }

  override function shutdown() {
  }

  override function resume() {
  }

  override property set Parameters(map : Map) {

  }

  override property set DestinationID(destinationID : int) {
      _destinationId = destinationID
  }
}
