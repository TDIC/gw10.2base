package acc.onbase.api.exception

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Server connection exception in the services-tier
 * <p>
 * Last Changes:
 * 02/11/2015 - Richard R. Kantimahanthi
 * * Initial implementation.
 */

class ServicesTierServerConnectionException extends ServicesTierException {
  construct(msg: String) {
    super(msg);
  }

  construct(msg: String, ex: Throwable) {
    super(msg, ex);
  }
}