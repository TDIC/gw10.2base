package acc.onbase.configuration

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 *
 * Enum to select the global OnBase client
 */
enum OnBaseClientType {
  Unity, Web
}