package acc.onbase.api.security

uses acc.onbase.configuration.OnBaseConfigurationFactory

uses javax.crypto.Mac
uses javax.crypto.spec.SecretKeySpec

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Last Changes:
 * 02/10/2017 - Daniel Q. Yu
 * * Initial implementation.
 * <p>
 * 02/15/2017 - Daniel Q. Yu
 * * Added Check Sum Method.
 */

/**
 * Billing Center security utilities.
 */
class SecurityManager {
  /**
   * Compute hash bytes from key
   *
   * @param docId   OnBase Document Id.
   * @param hashKey hash key.
   */
  public static function getHashBytes(docID: String, hashKey: String): byte[] {

    var secretKey = new SecretKeySpec(hashKey.getBytes("ASCII"), "HmacSHA256")
    var mac = Mac.getInstance("HmacSHA256")
    mac.init(secretKey)
    var hashBytes = mac.doFinal(docID.getBytes("ASCII"))

    return hashBytes
  }

  /**
   * Compute docId hash for docpop URL checksum.
   *
   * @param docId OnBase Document Id.
   * @return The hash hex string for the document id.
   */
  public static function computeDocIdCheckSum(docId: String): String {
    // compute hashHex hashBytes.
    var key = OnBaseConfigurationFactory.Instance.PopChecksumKey
    var hashBytes = getHashBytes(docId, key)

    // convert hashHex to hex string.
    var hashHex = new StringBuffer();
    foreach (b in hashBytes) {
      var hex = Integer.toHexString(0xFF & b);
      if (hex.length() == 1) {
        hashHex.append('0');
      }
      hashHex.append(hex);
    }

    return hashHex.toString()
  }

  /**
   * This method has been copied from Policy Center Onbase accelerator to implement WebCustomQuery and it would be
   * removed in future after updating accelerator.
   * @param checksumValue
   * @return hashed checksumValue.
   */
  public static function computeCheckSum(checksumValue: String): String {
    // compute hashHex hashBytes.
    var key = new SecretKeySpec(OnBaseConfigurationFactory.Instance.PopChecksumKey.getBytes("ASCII"), "HmacSHA256")
    var mac = Mac.getInstance("HmacSHA256")
    mac.init(key);
    var hashBytes = mac.doFinal(checksumValue.getBytes("ASCII"))

    // convert hashHex to hex string.
    var hashHex = new StringBuffer();
    foreach(b in hashBytes) {
      var hex = Integer.toHexString(0xFF & b);
      if (hex.length() == 1) {
        hashHex.append('0');
      }
      hashHex.append(hex);
    }

    return hashHex.toString()
  }
}