package acc.onbase.api.application

uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.configuration.DocumentLinkType
uses acc.onbase.util.LoggerFactory
uses gw.api.database.Query
uses gw.api.locale.DisplayKey

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Last Changes:
 * 09/12/2016 - Daniel Q. Yu
 * * Initial implementation for BC8 doclink metadata package.
 * <p>
 * 09/29/2016 - Daniel Q. Yu
 * * Change document search for related entities from AND to OR
 * <p>
 * 10/20/2107 - Anirudh Mohan
 * * Filter out "Retired" Documents.
 */

/**
 * Document linking application.
 */
class DocumentLinking {
  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  /**
   * Link a document to entity.
   *
   * @param entity   The entity which documents linked to.
   * @param document The document to be linked to the entity.
   * @param linkType The document link type.
   */
  public function linkDocumentToEntity(entity: KeyableBean, document: Document, linkType: DocumentLinkType) {
    linkDocumentsToEntity(entity, new Document[]{document}, linkType)
  }


  /**
   * Link multiple documents to entity.
   *
   * @param entity    The entity which documents linked to.
   * @param documents The list of document ids to be linked to the entity.
   * @param linkType  The document link type.
   */
  public function linkDocumentsToEntity(entity: KeyableBean, documents: Object[], linkType: DocumentLinkType) {

    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentLinking.linkDocumentToEntity(" + entity + "," + documents + "," + linkType + ")")
    }
    var user = User.util.CurrentUser == null ? User.util.UnrestrictedUser : User.util.CurrentUser
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      foreach (doc in documents) {
        var link = new OnBaseDocumentLink_Ext()
        link.LinkType = linkType.toString()
        link.LinkedEntity = entity.PublicID
        link.Document = doc as Document
      }
    }, user)
  }

  /**
   * Unlink a document from entity.
   *
   * @param entity   The entity which document to be unlinked from.
   * @param document The document to be unlinked from the entity.
   * @param linkType The document link type.
   */
  public function unlinkDocumentFromEntity(entity: KeyableBean, document: Document, linkType: DocumentLinkType) {
    unlinkDocumentsFromEntity(entity, new Document[]{document}, linkType)
  }

  /**
   * Unlink multiple documents from entity.
   *
   * @param documents The list of documents to be unlinked from the entity.
   * @param linkType  The document link type.
   * @param entityId  The entity id which document to be unlinked from.
   */
  public function unlinkDocumentsFromEntity(entity: KeyableBean, documents: Object[], linkType: DocumentLinkType) {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentLinking.unlinkDocumentsFromEntity(" + entity + "," + documents + "," + linkType + ")")
    }

    var user = User.util.CurrentUser == null ? User.util.UnrestrictedUser : User.util.CurrentUser
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      foreach (doc in documents) {
        var query = Query.make(OnBaseDocumentLink_Ext)
        query.compare("LinkType", Equals, linkType.toString())
        query.compare("LinkedEntity", Equals, entity.PublicID)
        query.compare("Document", Equals, doc as Document)
        var result = query.select().first()
        if (result != null) {
          bundle.delete(result)
        }
      }
    }, user)
  }

  /**
   * Get documents linked to entity.
   *
   * @param entityId The entity which documents linked to.
   * @param linkType The document link type.
   * @return A list of OnBase documents which linked to the entity.
   */
  public function getDocumentsLinkedToEntity(entity: KeyableBean, linkType: DocumentLinkType): List<Document> {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method DocumentLinking.getDocumentsLinkedToEntity(" + entity + "," + linkType + ")")
    }
    var linkedDocs = new ArrayList<Document>()
    var query = Query.make(OnBaseDocumentLink_Ext)
    query.compare("LinkType", Equals, linkType.toString())
    query.compare("LinkedEntity", Equals, entity.PublicID)
    var queryResult = query.select().where(\res -> !res.Document.Retired)
    foreach (r in queryResult) {
      linkedDocs.add(r.Document)
    }

    return linkedDocs
  }

  /**
   * Get documents not linked to an entity in OnBase.
   *
   * @param entity   The entity which documents linked to.
   * @param linkType The document link type.
   * @param beans    Additional entity beans to be passed in for process.
   * @return DocumentSearchResult object contains all documents linked to this entity.
   */
  public function getDocumentsNotLinkedToEntity(entity: KeyableBean, linkType: DocumentLinkType, beans: Object[]): List<Document> {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentMetadataSource.getDocumentsNotLinkedToEntity(" + entity + ", " + linkType + ", " + beans + ")...")
    }
    var relatedDocs = new ArrayList<Document>()
    var notLinked = new ArrayList<Document>()
    foreach (bean in beans) {
      var criteria = new DocumentSearchCriteria()
      if (bean typeis Account) {
        criteria.Account = bean
      } else if (bean typeis Policy) {
        criteria.Policy = bean
      } else if (bean typeis Producer) {
        criteria.Producer = bean
      }

      if (criteria.Account == null && criteria.Policy == null && criteria.Producer == null) {
        continue
      }
      criteria.IncludeObsoletes = false
      criteria.DateCriterionChoice.ChosenOption = null
      var results = criteria.performSearch(true)
      foreach (r in results) {
        if (!relatedDocs.contains(r)) {
          relatedDocs.add(r as Document)
        }
      }
    }
    var linked = getDocumentsLinkedToEntity(entity, linkType)
    foreach (doc in relatedDocs) {
      if (!linked.contains(doc)) {
        notLinked.add(doc)
      }
    }
    return notLinked
  }

  /**
   * Check if a document is linked to an entity.
   *
   * @param docUID   The document id to be checked.
   * @param linkType The document link type.
   * @param entity   The entity to be checked.
   * @return True if the document is linked to the entity.
   */
  public function isDocumentLinkedToEntity(document: Document, linkType: DocumentLinkType, entity: KeyableBean): boolean {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method DocumentLinking.isDocumentLinkedToEntity(" + entity + ", " + document + "," + linkType + ")")
    }
    var results = getDocumentsLinkedToEntity(entity, linkType)
    for (doc in results) {
      if (doc.DocUID == document.DocUID) {
        return true
      }
    }
    return false
  }

  /**
   * Get the UI Label for document linking.
   *
   * @param entity   The entity which documents linked to.
   * @param linkType The document link type.
   * @return The UI Label.
   */
  public static function getLinkingDocumentUILabel(entity: KeyableBean, linkType: DocumentLinkType): String {
    if (!OnBaseConfigurationFactory.Instance.EnableLinkedDocumentCount) {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_NoCount")
    }
    var docCount = new DocumentLinking().getDocumentsLinkedToEntity(entity, linkType).Count
    if (docCount > 0) {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_ViewDocuments", docCount)
    } else {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_NoDocument")
    }
  }

  /**
   * Helper Method to get Account from Trouble Ticket.
   *
   * @param ticket The trouble ticket.
   * @return The account associated with this trouble ticket
   */
  public static function getRelatedFromTroubleTicket(ticket: TroubleTicket): KeyableBean[] {
    var related = new ArrayList<KeyableBean>()
    var account = gw.api.database.Query.make(Account).compare("AccountNumber", Equals, ticket.AccountNumber).select().getAtMostOneRow()
    foreach (entity in ticket.TicketRelatedEntities) {
      if (entity typeis Account) {
        related.add(entity)
      } else if (entity typeis Policy) {
        related.add(entity)
      } else if (entity typeis PolicyPeriod) {
        related.add(entity.Policy)
      } else if (entity typeis Producer) {
        related.add(entity)
      }
    }
    return related.toArray(new KeyableBean[related.size()])
  }
}
