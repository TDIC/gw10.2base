package acc.onbase.enhancement

/**
 * Created by tbrenneison on 5/21/2018.
 */
enhancement OnBasePolicyEnhancement: Policy {
    property get InForcePolicyNumber() : String {
    return this.PolicyPeriods.firstWhere(\elt -> elt.Status == PolicyPeriodStatus.TC_INFORCE).PolicyNumber
  }
}
