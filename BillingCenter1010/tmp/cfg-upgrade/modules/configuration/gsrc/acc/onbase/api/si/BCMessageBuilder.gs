package acc.onbase.api.si

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 */
public class BCMessageBuilder extends MessageBuilder {


  /**
   * Create a new builder for a specific message type
   *
   * @param messageType type of message, typically an event name
   */
  construct(messageType: String) {
    super(messageType)
  }

  /**
   * Add the corresponding model of the entity, the document is linked to, to the message builder.
   *
   * @param document The input document.
   */

  public function addEntityModelToMessage(document: Document) {
    if (document.Policy != null) {
      // Get Current Policy Period
      var currentPolicyPeriod = entity.PolicyPeriod.finder.getCurrentPolicyPeriodByPolicy(document.Policy)
      var policyPeriodModel = new acc.onbase.api.si.model.policyperiodmodel.PolicyPeriod(currentPolicyPeriod)
      addMessagePart('policyperiod', policyPeriodModel)
    }
    if (document.Account != null) {
      var accountModel = new acc.onbase.api.si.model.accountmodel.Account(document.Account)
      addMessagePart('account', accountModel)
    }
    if (document.Producer != null) {
      var producerModel = new acc.onbase.api.si.model.producermodel.Producer(document.Producer)
      addMessagePart('producer', producerModel)
    }

  }


  /**
   * Build a Dcoument Message to be sent to Onbase during Document Archival
   *
   * @param document The document whose keywords have to be sent to OnBase
   * @return builder which contains the Document and Claim Model added to the Message
   */
  public static function buildArchiveMessage(document: Document): MessageBuilder {
    var defaultLabel = acc.onbase.api.si.model.documentmodel.Document.Label.default_label
    var documentModel = new acc.onbase.api.si.model.documentmodel.Document(document,{defaultLabel})
    var builder = new BCMessageBuilder("ArchiveDocumentBC")
    builder.addMessagePart('document', documentModel)
    builder.addEntityModelToMessage(document)
    return builder
  }
}