package acc.onbase.api.pluginimpl

uses acc.onbase.api.application.DocumentArchival
uses acc.onbase.api.application.DocumentRetrieval
uses acc.onbase.api.exception.NullContentException
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.configuration.OnBaseClientType
uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.api.util.DisplayableException
uses gw.document.DocumentContentsInfo
uses gw.plugin.InitializablePlugin
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentContentSource
uses gw.xml.ws.WsdlFault

uses java.io.InputStream

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Last Changes:
 * 05/12/2016 - Anirudh Mohan
 * * Merged OnBaseDocumentContentSource.gs and DocumentContenSource.gs into 1 file for efficiency
 * * DOCUMENT_CONTENTS being used as a response type for getDocumentContentsInfo(Retrieving docs) instead of URL
 * 05/16/2016 - Anirudh Mohan
 * * Implemented the updateDocument function
 * 06/02/2016  - Anirudh Mohan
 * * Changed the keywords to match the ones in Billing center
 * <p>
 * 06/07/2016 - Anirudh Mohan
 * * Implemented removeDocument function
 * <p>
 * * 06/09/2016 - Anirudh Mohan
 * * Updated the keyword in updateDoc from relateddocumenthandle to documentidforrevision
 * <p>
 * 06/21/2016 - Anirudh Mohan
 * * Fixed the getDocumentContentsInformation to be able to view docs from Web Client also by viewing it via a window.open
 * <p>
 * * 09/28/2016 - Alicia Schroeder
 * * Added the keywords securitytype and billingsecurityzone
 * <p>
 * 02/10/2017 - Daniel Q. Yu
 * * Uses SecurityManager to get Bill Security Zone keyword value.
 * * Change strings to KeywordMap enums.
 * <p>
 * *  04/28/2017 - Tori Brenneison
 * * Changes to GetDocumentContentsInformation
 * * Display content in Unity Client orphan window w/ instructions for user to close window
 * * Use document.location.href to avoid orphaned window for Web Client
 * * Use hidden iframe to avoid extra orphaned window in IE for Unity Client
 */

/**
 * IDocumentContentSource plugin implementation with OnBase as DMS.
 */
class DocumentContentSource implements IDocumentContentSource, InitializablePlugin {
  /** Logger for OnBaseDMS */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.PluginLoggerCategory)

  /**
   * If true document can be stored, updated and removed from OnBase.
   */
  override property get InboundAvailable(): boolean {
    return true
  }

  /**
   * If true document can be searched and viewed from OnBase.
   */
  override property get OutboundAvailable(): boolean {
    return true
  }

  /**
   * Add a document to OnBase. Document content and metadata are archived to OnBase here.
   *
   * This method returns false if we also want Guidewire OOTB implementation to save
   * the document metadata when IDocumentMetadataSource plugin is not used.
   *
   * @param documentContents The document content input stream.
   * @param document The document object.
   * @return True if document meta data information has been saved. Or false then IDocumentMetadataSource.saveDocument will be called to save meta data.
   */
  override function addDocument(documentContents: InputStream, document: Document): boolean {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentContentSource.addDocument(" + documentContents + ", " + document + ")")
    }
    // Document content is null, update document meta data only.
    if (documentContents == null) {
      if (isDocument(document)) {
        document.DateModified = DateUtil.currentDate()
      } else {
        _logger.error("Calling DocumentContentSource.addDocument for new document without document content.");
        throw new DisplayableException("Calling DocumentContentSource.addDocument for new document without document content.")
      }
      if (_logger.DebugEnabled) {
        _logger.debug("Document " + document.DocUID + " has been added to OnBase.")
      }
      // return true to ignore IDocumentMetadataSource.saveDocument or false uses Guidewire OOTB implementation to save metadata again.
      return Plugins.isEnabled("IDocumentMetadataSource")
    }
    // Call acc.onbase.api.application to do the real work.
    var docUID = null as String
    try {
      var archivalApp = new DocumentArchival()
      docUID = archivalApp.archiveDocument(documentContents, document, OnBaseConfigurationFactory.Instance.AsyncDocumentFolder, OnBaseConfigurationFactory.Instance.AsyncDocumentSize)
    } catch (ex1: NullContentException) {
      if (isDocument(document)) {
        // Document is created document template and from rules, do nothing here.
      } else {
        throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.DocumentArchiveFailure.STR_GW_ZeroByteDocumentAttempt", document.Name))
      }
      return Plugins.isEnabled("IDocumentMetadataSource")
    } catch (ex2: WsdlFault) {
      var errorMessage = DisplayKey.get("Accelerator.OnBase.Messaging.STR_GW_CannotArchive", document.Name)
      //if document content has been archived but keyword update fails, reset the docUID to null
      if (document.DocUID.HasContent) {
        document.DocUID = null
      }
      _logger.error("Adding document to OnBase failed!", ex2)
      throw new DisplayableException(errorMessage)
    } catch (ex3: Exception) {
      var errorMessage = DisplayKey.get("Accelerator.OnBase.Messaging.STR_GW_AddingDocFailed")
      _logger.error("Adding document to OnBase failed!", ex3)
      throw new DisplayableException(errorMessage)
    }
    if (docUID == null) {
      // No metadata needs to be saved, so return true here.
      return true
    } else {
      // return true to ignore IDocumentMetadataSource.saveDocument or false uses Guidewire OOTB implementation to save metadata again.
      return Plugins.isEnabled("IDocumentMetadataSource")
    }
  }

  /**
   * Display document in OnBase Unity/Web client.
   *
   * @param document The document to be displayed.
   * @param includeDocumentContents If true includes document content. Currently not used by integration.
   *
   * @return The DocumentContentsInfo object for this document.
   */
  override function getDocumentContentsInfo(document: Document, includeDocumentContents: boolean): DocumentContentsInfo {
    var retrievalApp = new DocumentRetrieval()
    var js = null as String
    var contents = null as String
    //dci's hidden frame is false by default.
    if (OnBaseConfigurationFactory.Instance.ClientType == OnBaseClientType.Unity) {
      var uri = retrievalApp.getDocumentUnityURL(document.DocUID)
      contents = "<html><head><title>" + DisplayKey.get("Accelerator.OnBase.STR_GW_OpeningUnityClient_Title") + "</title></head><body><h1>" + DisplayKey.get("Accelerator.OnBase.STR_GW_OpeningUnityClient_Launch") + "</h1><h2>" + DisplayKey.get("Accelerator.OnBase.STR_GW_OpeningUnityClient_Close") + "</h2><iframe src ='" + uri + "' style=\"width:0;height:0;border:0;border:none;\"></iframe></body></html>"
    } else {
      var uri = retrievalApp.getDocumentWebURL(document.DocUID, OnBaseConfigurationFactory.Instance.WebClientType)
      js = "document.location.href=('" + uri + "');"
      contents = "<html><head><script>" + js + "</script></head></html>"
    }
    var dci = new DocumentContentsInfo(DocumentContentsInfo.ContentResponseType.DOCUMENT_CONTENTS, contents, "text/html")
    return dci
  }

  /**
   * Get the document content in OnBase for external use.
   *
   * @param document The document which content to be retrieved from OnBase.
   *
   * @return The DocumentContentsInfo object for this document.
   */
  override function getDocumentContentsInfoForExternalUse(document: Document): DocumentContentsInfo {
    var dci = new DocumentContentsInfo(DocumentContentsInfo.ContentResponseType.DOCUMENT_CONTENTS, getDocumentInputStream(document), document.getMimeType())
    return dci
  }

  /**
   * Get the document content input stream from OnBase.
   *
   * @param document The document to be downloaded.
   *
   * @return The document input stream.
   */
  function getDocumentInputStream(document: Document): InputStream {
    var retrievalApp = new DocumentRetrieval()
    return retrievalApp.getDocumentContent(document)
  }

  /**
   * Is this a valid document?
   *
   * @param document The document to be checked.
   *
   * @return True if it is a valid document.
   */
  override function isDocument(document: Document): boolean {
    if (document.DocUID != null && !document.DocUID.equalsIgnoreCase("none")) {
      return true
    } else {
      return false
    }
  }

  /**
   * Remove document from OnBase.
   *
   * @param document The document to be deleted.
   *
   * @return True if IDocumentMetadataPlugin is disabled or if metadata is removed and no further action is required.
   * @return False Otherwise.
   */
  override function removeDocument(document: Document): boolean {
    return true
  }

  /**
   * Update document in OnBase. Store the document as a revision
   *
   * @param document The document to be updated.
   * @param documentIS The document content input stream.
   *
   * @return True if document has been updated.
   */
  override function updateDocument(document: Document, documentIS: InputStream): boolean {
    //## todo: Implement me
    //return true so that OnBaseDocumentMetadataSource won't be used
    _logger.error("OnBaseDocumentContentSource.updateDocument not implemented.")
    throw new DisplayableException("OnBaseDocumentContentSource.updateDocument not implemented.")
  }



  /**
   * Set plugin parameters.
   *
   * @param parameters The parameters for this plugin.
   */
  override property set Parameters(map : Map) {

  }
}
