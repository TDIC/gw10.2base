package acc.onbase.webservice

uses acc.onbase.configuration.DocumentLinkType
uses acc.onbase.api.application.DocumentLinking
uses acc.onbase.util.LoggerFactory
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.transaction.Transaction
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService
uses entity.Activity

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Last Changes:
 * 03/13/2018 - Daniel Q. Yu
 * * Initial implementation
 */

@WsiWebService("http://onbase/acc/billing/webservice/OnBaseActivityAPI")
@WsiPermissions({SystemPermissionType.TC_SOAPADMIN})
@WsiAvailability(MULTIUSER)
class OnBaseActivityAPI {
  /**
   * Logger for OnBaseDMS
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  function createNewActivity(activity: acc.onbase.api.si.model.activitymodel.Activity, docUIDs: String[], isShared: boolean): String {
    // Get the activity pattern code for this activity
    var activityPattern: ActivityPattern = null
    if (activity.ActivityPattern.Code.HasContent) {
      activityPattern = Query.make(ActivityPattern).compare("Code", Equals, activity.ActivityPattern.Code).select().AtMostOneRow
      if (activityPattern == null) {
        //invalid activity pattern code entered
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidActivityCode", activity.ActivityPattern.Code))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidActivityCode", activity.ActivityPattern.Code))
      }
    } else {
      // no activity pattern code entered
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingActivityCode"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingActivityCode"))
    }

    // Get documents for this activity
    var docList = new ArrayList<Document>()
    if (docUIDs.length > 0) {
      foreach (e in docUIDs) {
        var document = Query.make(entity.Document).compare(entity.Document#DocUID, Relop.Equals, e).select().AtMostOneRow
        if (document == null) {
          // docUID not found
          _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", e))
          throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", e))
        }
        docList.add(document)
      }
    } else {
      // missing docUID
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
    }

    var troubleTicket: TroubleTicket = null
    var account: Account = null
    var policy: Policy = null
    var producer: Producer = null

    if (activity.TroubleTicket.TroubleTicketNumber.HasContent) {
      troubleTicket = Query.make(TroubleTicket).compare("TroubleTicketNumber", Equals, activity.TroubleTicket.TroubleTicketNumber).select().AtMostOneRow
      if (troubleTicket == null) {
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidTroubleTicketNumber", activity.TroubleTicket.TroubleTicketNumber))
      }
    } else {
      // Following are only needed for a new troubleticket.

      // Check title, description, type and priority.
      if (!activity.TroubleTicket.Title.HasContent || !activity.TroubleTicket.DetailedDescription.HasContent || activity.TroubleTicket.TicketType == null || activity.TroubleTicket.Priority == null) {
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingTroubleTicketInfo"))
      }
      //Find Account, Policy and Producer if provided.
      foreach (entry in activity.TroubleTicket.TroubleTicketJoinEntities.Entry) {
        if (entry.Account.AccountNumber.HasContent) {
          account = Query.make(Account).compare("AccountNumber", Equals, entry.Account.AccountNumber).select().AtMostOneRow
          if (account == null) {
            throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", entry.Account.AccountNumber))
          }
        } else if (entry.PolicyPeriod.PolicyNumber.HasContent) {
          policy = PolicyPeriod.finder.getCurrentPolicyPeriodByNumber(entry.PolicyPeriod.PolicyNumber).Policy
          if (policy == null) {
            throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidPolicyNumber", entry.PolicyPeriod.PolicyNumber))
          }
        } else if (entry.Producer.PublicID.HasContent) {
          producer = Query.make(Producer).compare("PublicID", Equals, entry.Producer.PublicID).select().getAtMostOneRow()
          if (producer == null) {
            throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidProducerPublicID", entry.Producer.PublicID))
          }
        }
      }
    }

    // Create activity
    var newActivity: Activity = null
    Transaction.runWithNewBundle(\newBundle -> {
      //Find Trouble Ticket by Trouble Ticket number. If the ticket number not passed then create a new trouble ticket.
      if (troubleTicket != null) {
        troubleTicket = newBundle.add(troubleTicket)
      } else {
        troubleTicket = new TroubleTicket()
        troubleTicket.Title = activity.TroubleTicket.Title
        troubleTicket.DetailedDescription = activity.TroubleTicket.DetailedDescription
        troubleTicket.TicketType = activity.TroubleTicket.TicketType
        troubleTicket.Priority = activity.TroubleTicket.Priority
        troubleTicket.autoAssign()
        //Get a new instance of CreateTroubleTicketHelper class.
        var ttHelper = new CreateTroubleTicketHelper()
        //Associate the trouble ticket to the Account
        if (account != null) {
          account = newBundle.add(account)
          ttHelper.linkTroubleTicketWithAccount(troubleTicket, account)
        }
        //Associate the trouble ticket to the Policy
        if (policy != null) {
          policy = newBundle.add(policy)
          ttHelper.linkTroubleTicketWithPolicy(troubleTicket, policy)
        }
        //Associate the trouble ticket to the Producer
        if (producer != null) {
          producer = newBundle.add(producer)
          ttHelper.linkTroubleTicketWithProducer(troubleTicket, producer)
        }
      }
      //create a new activity/shared activity under the trouble ticket
      newActivity = (isShared ? troubleTicket.createSharedActivity() : troubleTicket.createActivity())
      if (!isShared) {
        //randomly assign a user if it is a normal activity, don't assign anyone if it is a shared activity
        newActivity.autoAssign()
      }
      newActivity.ActivityPattern = activityPattern
      newActivity.Subject = DisplayKey.get("Accelerator.OnBase.WS.STR_GW_NewActivitySubject", Arrays.toString(docUIDs))
    } )

    // Link documents to the activity
    new DocumentLinking().linkDocumentsToEntity(newActivity, docList.toArray(), DocumentLinkType.activityid)

    return newActivity.PublicID
  }
}