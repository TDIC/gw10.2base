package acc.onbase.api.service.interfaces

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Last Changes:
 * 01/13/2015 - Daniel Q. Yu
 * * Initial implementation.
 */

/**
 * Interface to call OnBase Synchronized Archive Document service.
 */
interface ArchiveDocumentSyncInterface {
  /**
   * Archive document synchronously.
   *
   * @param documentContents The document content in bytes.
   * @param document The entity.document object.
   *
   * @return The newly archived document id.
   */
  public function archiveDocument(documentContents: byte[], document: Document): String
}
