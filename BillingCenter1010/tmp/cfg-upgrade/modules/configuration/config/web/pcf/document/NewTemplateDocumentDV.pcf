<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <DetailViewPanel
    id="NewTemplateDocumentDV">
    <Require
      name="documentBCContext"
      type="gw.document.DocumentBCContext"/>
    <Require
      name="documentCreationInfo"
      type="gw.document.DocumentCreationInfo"/>
    <Require
      name="documentContainer"
      type="DocumentContainer"/>
    <Variable
      initialValue="documentCreationInfo.Document"
      name="document"
      type="Document"/>
    <Variable
      initialValue="documentContainer typeis Account ? documentContainer : null"
      name="account"
      type="Account"/>
    <Variable
      initialValue="documentContainer typeis Policy ? documentContainer : null"
      name="policy"
      type="Policy"/>
    <Variable
      initialValue="documentContainer typeis Producer ? documentContainer : null"
      name="producer"
      type="Producer"/>
    <Variable
      initialValue="document.Language"
      name="languageType"
      type="LanguageType"/>
    <Variable
      initialValue="DisplayKey.get(&quot;Java.Document.Creation.Template.Step3&quot;)"
      name="Step3Label"
      type="String"/>
    <Variable
      initialValue="return initDV()"
      name="initialized"
      type="boolean"/>
    <ReferencedWidget
      widget="DocumentCreationScreen"/>
    <InputColumn>
      <Label
        label="DisplayKey.get(&quot;Web.NewTemplateDocumentDV.DocumentContents&quot;)"/>
      <Label
        label="DisplayKey.get(&quot;Web.NewTemplateDocumentDV.Step1SelectTemplate&quot;)"/>
      <DocumentTemplateInput
        clearEnabled="false"
        editable="documentBCContext.DocumentCreationReadOnly"
        freeInputEnabled="false"
        id="TemplatePicker"
        label="DisplayKey.get(&quot;Java.Document.Creation.Template&quot;)"
        onPick="documentBCContext.resetTemplateBasedInfo(documentCreationInfo, documentCreationInfo.DocumentTemplateDescriptor); documentCreationInfo.evaluateDynamicWidgets()"
        pickLocation="DocumentTemplateSearchPopup.push(documentContainer, documentCreationInfo)"
        required="true"
        value="documentCreationInfo.DocumentTemplateDescriptor"
        valueType="gw.plugin.document.IDocumentTemplateDescriptor"/>
      <TypeKeyInput
        editable="documentBCContext.DocumentCreationReadOnly"
        id="Language"
        label="DisplayKey.get(&quot;Web.DocumentTemplateSearch.Language&quot;)"
        value="languageType"
        valueType="typekey.LanguageType"
        visible="LanguageType.getTypeKeys( false ).Count &gt; 1">
        <PostOnChange
          deferUpdate="false"
          onChange="changeLanguage()"/>
      </TypeKeyInput>
      <InputSet
        visible="documentCreationInfo.DocumentTemplateDescriptor != null">
        <Label
          label="DisplayKey.get(&quot;Web.NewTemplateDocumentDV.Step2SpecifyObjectValues&quot;)"/>
        <InputSetRef
          def="DocumentTemplateContextObjectIteratorInputSet(documentBCContext, documentCreationInfo)"
          mode="documentCreationInfo.DocumentTemplateDescriptor.TemplateId"
          widgets="DocumentCreationScreen"/>
        <InputSetRef
          def="DocumentCreationInputSet(documentBCContext, documentCreationInfo, Step3Label)"
          widgets="DocumentCreationScreen"/>
      </InputSet>
    </InputColumn>
    <Code><![CDATA[uses gw.pl.persistence.core.Bean
      uses gw.document.SimpleSymbol
      uses gw.api.util.LocaleUtil
      uses gw.document.DocumentsUtilBase

      function initDV(): boolean {
        documentCreationInfo.addSymbols({
          // Full set of symbols available to Template Descriptors in earlier versions of CC
          // Included for backwards compatibility and can likely be pruned
          "DocumentCreationInfo"->documentCreationInfo,
          "Document"->document,
          "language"->languageType,
          "documentContainer"->documentContainer,

          // The following are added with explicit types to facilitate
          // null-safe access within template descriptor Gosu expressions.
          // Otherwise, null values would be mapped to the Object type.
          "Account"->new SimpleSymbol(Account, account),
          "Policy"->new SimpleSymbol(Policy, policy),
          "Producer"->new SimpleSymbol(Producer, producer)
        })
        if (documentCreationInfo.DocumentTemplateDescriptor != null) {
          documentCreationInfo.evaluateDynamicWidgets()
        }
        return true
      }

      function changeLanguage() {
        documentBCContext.resetTemplateBasedInfo(documentCreationInfo,
          DocumentsUtilBase.fetchDocumentTemplate(documentCreationInfo.DocumentTemplateDescriptor.TemplateId,
            LocaleUtil.toLanguage(languageType), CurrentLocation))
        documentCreationInfo.evaluateDynamicWidgets()
      }

    ]]></Code>
  </DetailViewPanel>
</PCF>