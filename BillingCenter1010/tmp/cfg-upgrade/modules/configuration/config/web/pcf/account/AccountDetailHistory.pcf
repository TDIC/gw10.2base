<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Page
    canVisit="perm.System.accttabview and perm.System.accthistview"
    id="AccountDetailHistory"
    showUpLink="true"
    title="DisplayKey.get(&quot;Web.AccountDetailHistory.Title&quot;)">
    <LocationEntryPoint
      signature="AccountDetailHistory(account : Account)"/>
    <Variable
      name="account"
      type="Account"/>
    <Screen
      id="AccountDetailHistoryScreen">
      <Toolbar/>
      <ListViewPanel
        id="AccountDetailHistoryLV"
        stretch="true">
        <RowIterator
          editable="false"
          elementName="accountHistory"
          value="getAccountHistories()"
          valueType="gw.api.database.IQueryBeanResult&lt;entity.AccountHistory&gt;">
          <ToolbarFilter
            label="DisplayKey.get(&quot;Web.AccountDetailHistoryLV.FilterByEventDate&quot;)"
            name="DateFilter">
            <ToolbarFilterOption
              filter="new gw.api.web.history.AccountHistoriesFilters.Last30Days()"/>
            <ToolbarFilterOption
              filter="new gw.api.web.history.AccountHistoriesFilters.Last60Days()"/>
            <ToolbarFilterOption
              filter="new gw.api.web.history.AccountHistoriesFilters.Last90Days()"/>
            <ToolbarFilterOption
              filter="new gw.api.web.history.AccountHistoriesFilters.Last120Days()"/>
            <ToolbarFilterOption
              filter="new gw.api.web.history.AccountHistoriesFilters.Last180Days()"
              selectOnEnter="true"/>
            <ToolbarFilterOption
              filter="new gw.api.web.history.AccountHistoriesFilters.LastYear()"/>
            <ToolbarFilterOption
              filter="new gw.api.web.history.AccountHistoriesFilters.Last3Years()"/>
            <ToolbarFilterOption
              filter="new gw.api.web.history.AccountHistoriesFilters.All()"/>
          </ToolbarFilter>
          <ToolbarFilter
            label="DisplayKey.get(&quot;Web.AccountDetailHistoryLV.FilterByEventType&quot;)"
            name="HistoryEventTypeFilter">
            <ToolbarFilterOptionGroup
              filters="new gw.api.filters.TypeKeyFilterSet( History.Type.TypeInfo.getProperty( &quot;EventType&quot; ) ).getFilterOptions()"/>
          </ToolbarFilter>
          <Row>
            <DateCell
              id="Date"
              label="DisplayKey.get(&quot;Web.AccountDetailHistoryLV.Date&quot;)"
              sortOrder="1"
              value="accountHistory.EventDate"/>
            <TextCell
              action="TransactionDetailPopup.push(accountHistory.Transaction)"
              grow="true"
              id="Transaction"
              label="DisplayKey.get(&quot;Web.AccountDetailHistoryLV.Transaction&quot;)"
              value="accountHistory.Transaction.TransactionNumber"/>
            <TextCell
              action="goToPolicyTransferDetail(accountHistory)"
              available="isPolicyTransfer(accountHistory)"
              grow="true"
              id="Description"
              label="DisplayKey.get(&quot;Web.AccountDetailHistoryLV.Description&quot;)"
              sortOrder="2"
              value="accountHistory.Description"/>
            <TextCell
              grow="true"
              id="RefNumber"
              label="DisplayKey.get(&quot;Web.AccountDetailHistoryLV.RefNumber&quot;)"
              value="accountHistory.RefNumber"/>
            <MonetaryAmountCell
              align="left"
              currency="account.Currency"
              enableSort="false"
              formatType="currency"
              id="TransactionAmount"
              label="DisplayKey.get(&quot;Web.AccountDetailHistoryLV.TransactionAmount&quot;)"
              value="accountHistory.Amount"/>
            <TextCell
              action="UserDetailPage.push(accountHistory.User)"
              id="User"
              label="DisplayKey.get(&quot;Web.AccountDetailHistoryLV.User&quot;)"
              value="accountHistory.User"
              valueType="entity.User"
              wrap="false"/>
          </Row>
        </RowIterator>
      </ListViewPanel>
    </Screen>
    <Code><![CDATA[uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query

function goToPolicyTransferDetail(item : AccountHistory) {
  PolicyTransferDetail.go(item as PolicyHistory);
}

function isPolicyTransfer(item : AccountHistory) : Boolean {
  return (item.EventType == TC_POLICYTRANSFERRED)
}

function getAccountHistories() : IQueryBeanResult<AccountHistory> {
  var accountQuery = Query.make(AccountHistory)
  accountQuery.compare("Subtype", NotEquals, typekey.History.TC_POLICYHISTORY)
  accountQuery.or(\ restriction -> {
    restriction.compare("Account", Equals, account)
    restriction.compare("OtherAccount", Equals, account)
  })  
  return accountQuery.select()
}]]></Code>
  </Page>
</PCF>