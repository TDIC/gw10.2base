<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Screen
    id="NewPolicyWizardSummaryStepScreen">
    <Require
      name="account"
      type="Account"/>
    <Require
      name="policyPeriod"
      type="PolicyPeriod"/>
    <Require
      name="producerCodeRoleEntries"
      type="ProducerCodeRoleEntry[]"/>
    <Require
      name="invoicingOverridesView"
      type="gw.invoice.InvoicingOverridesView"/>
    <Require
      name="issuance"
      type="Issuance"/>
    <Variable
      initialValue="new gw.api.web.producer.ProducerSearchConverter()"
      name="producerSearchConverter"
      type="gw.api.web.producer.ProducerSearchConverter"/>
    <Variable
      initialValue="gw.api.database.Query.make(SecurityZone).select()"
      name="allSecurityZones"
      type="gw.api.database.IQueryBeanResult&lt;SecurityZone&gt;"/>
    <Variable
      initialValue="new gw.api.web.account.AccountSearchConverter()"
      name="accountSearchConverter"
      type="gw.api.web.account.AccountSearchConverter"/>
    <Variable
      initialValue="gw.api.database.Query.make(entity.ReturnPremiumPlan).select()"
      name="allReturnPremiumPlans"
      type="gw.api.database.IQueryBeanResult&lt;entity.ReturnPremiumPlan&gt;"/>
    <Toolbar>
      <WizardButtons/>
    </Toolbar>
    <DetailViewPanel
      id="NewPolicyDV">
      <InputColumn>
        <Label
          label="DisplayKey.get(&quot;Web.AccountDetailNewPolicy.PolicyInfo&quot;)"/>
        <TextInput
          editable="true"
          id="PolicyNumber"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.PolicyNumber&quot;)"
          required="true"
          value="policyPeriod.PolicyNumber"/>
        <TextInput
          editable="true"
          id="DBA"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.DBA&quot;)"
          value="policyPeriod.DBA"/>
        <DateInput
          editable="true"
          id="PolicyPerEffDate"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.PolicyPerEffDate&quot;)"
          required="true"
          value="policyPeriod.PolicyPerEffDate"/>
        <DateInput
          editable="true"
          id="PolicyPerExpirDate"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.PolicyPerExpirDate&quot;)"
          required="true"
          validationExpression="policyPeriod.PolicyPerExpirDate &gt;= policyPeriod.PolicyPerEffDate ? null : DisplayKey.get(&quot;Web.NewPolicyDV.ExpirationDateError&quot;)"
          value="policyPeriod.PolicyPerExpirDate"/>
        <TypeKeyInput
          editable="true"
          id="PolicyLOB"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.LOBCode&quot;)"
          value="policyPeriod.Policy.LOBCode"
          valueType="typekey.LOBCode"/>
        <BooleanRadioInput
          editable="true"
          id="AssignedRisk"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.AssignedRisk&quot;)"
          value="policyPeriod.AssignedRisk"/>
        <TypeKeyInput
          editable="true"
          id="RiskJurisdiction"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.RiskJurisdiction&quot;)"
          value="policyPeriod.RiskJurisdiction"
          valueType="typekey.Jurisdiction"/>
        <TypeKeyInput
          editable="true"
          id="UWCompany"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.UWCompany&quot;)"
          value="policyPeriod.UWCompany"
          valueType="typekey.UWCompany"/>
        <RangeInput
          editable="true"
          id="SecurityZone"
          label="DisplayKey.get(&quot;Web.PolicyDetailDV.SecurityZone&quot;)"
          value="policyPeriod.SecurityZone"
          valueRange="allSecurityZones"
          valueType="entity.SecurityZone"/>
        <TextInput
          editable="true"
          id="Underwriter"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.Underwriter&quot;)"
          value="policyPeriod.Underwriter"/>
        <BooleanRadioInput
          available="!policyPeriod.AgencyBill"
          editable="true"
          id="EligibleForFullPayDiscount"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.EligibleForFullPayDiscount&quot;)"
          value="policyPeriod.EligibleForFullPayDiscount"
          visible="!policyPeriod.AgencyBill"/>
        <BooleanRadioInput
          editable="true"
          id="RequireFinalAudit"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.RequireFinalAudit&quot;)"
          required="true"
          value="policyPeriod.RequireFinalAudit"/>
        <Label
          label="DisplayKey.get(&quot;Web.AccountDetailNewPolicy.DefaultPayer&quot;)"/>
        <RangeInput
          editable="true"
          id="BillingMethod"
          label="DisplayKey.get(&quot;Web.AccountDetailNewPolicy.Producers.BillingMethod&quot;)"
          required="true"
          value="policyPeriod.BillingMethod"
          valueRange="PolicyPeriodBillingMethod.getTypeKeys(false)"
          valueType="typekey.PolicyPeriodBillingMethod">
          <PostOnChange
            onChange="if (policyPeriod.AgencyBill) {policyPeriod.EligibleForFullPayDiscount = false; invoicingOverridesView.OverridingPayerAccount =null; invoicingOverridesView.OverridingInvoiceStream =null}"/>
        </RangeInput>
        <PickerInput
          editable="true"
          id="OverridingPayerAccount"
          inputConversion="accountSearchConverter.getAccount(VALUE)"
          label="DisplayKey.get(&quot;Web.AccountDetailNewPolicy.DefaultPayer.OverridingPayerAccount&quot;)"
          pickLocation="AccountSearchPopup.push()"
          required="policyPeriod.isListBill()"
          validationExpression="policyPeriod.isListBill() and invoicingOverridesView.OverridingPayerAccount.isListBill() and policyPeriod.PaymentPlan == null ? null : gw.api.web.account.PolicyPeriods.checkForOverridingPayerAccountError(policyPeriod, invoicingOverridesView.OverridingPayerAccount)"
          value="invoicingOverridesView.OverridingPayerAccount"
          valueType="entity.Account"
          visible="!policyPeriod.AgencyBill">
          <PostOnChange
            onChange="maybeResetPaymentPlan()"/>
        </PickerInput>
        <Label
          label="DisplayKey.get(&quot;Web.AccountDetailNewPolicy.Plans&quot;)"/>
        <RangeInput
          editable="true"
          id="PaymentPlan"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.PaymentPlan&quot;)"
          required="true"
          value="issuance.PolicyPaymentPlan"
          valueRange="invoicingOverridesView.RelatedPaymentPlans"
          valueType="entity.PaymentPlan">
          <PostOnChange
            onChange="invoicingOverridesView.clearOverridingInvoiceStreamIfIncompatibleWithPaymentPlan(issuance.PolicyPaymentPlan);"/>
        </RangeInput>
        <RangeInput
          editable="true"
          id="OverridingInvoiceStream"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.OverridingInvoiceStream&quot;)"
          required="policyPeriod.isListBill()"
          value="invoicingOverridesView.OverridingInvoiceStream"
          valueRange="policyPeriod.AgencyBill ? {} : gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(invoicingOverridesView.DefaultPayer, issuance.PolicyPaymentPlan)"
          valueType="entity.InvoiceStream"
          visible="!policyPeriod.AgencyBill"/>
        <RangeInput
          editable="true"
          id="DelinquencyPlan"
          label="DisplayKey.get(&quot;Web.NewAccountDV.DelinquencyPlan&quot;)"
          required="false"
          value="policyPeriod.PolicyPeriodDelinquencyPlan"
          valueRange="policyPeriod.getApplicableDelinquencyPlans()"
          valueType="entity.DelinquencyPlan"/>
        <RangeInput
          editable="true"
          id="ReturnPremiumPlan"
          label="DisplayKey.get(&quot;Web.PolicyDetailDV.ReturnPremiumPlan&quot;)"
          required="true"
          value="policyPeriod.ReturnPremiumPlan"
          valueRange="allReturnPremiumPlans"
          valueType="entity.ReturnPremiumPlan"/>
        <Label
          label="DisplayKey.get(&quot;Web.AccountDetailNewPolicy.Producers&quot;)"/>
        <InputIterator
          elementName="producerCodeRoleEntry"
          value="producerCodeRoleEntries"
          valueType="entity.ProducerCodeRoleEntry[]">
          <TextInput
            editable="true"
            id="Producer"
            inputConversion="producerSearchConverter.getProducer(VALUE)"
            label="producerCodeRoleEntry.Role"
            onPick="autoSelectSoleProducerCode(producerCodeRoleEntry)"
            requestValidationExpression="validateProducer(VALUE, producerCodeRoleEntry.Role)"
            required="policyPeriod.AgencyBill and producerCodeRoleEntry.Role == PolicyRole.TC_PRIMARY"
            value="producerCodeRoleEntry.Producer"
            valueType="entity.Producer">
            <MenuItem
              action="ProducerSearchPopup.push()"
              icon="&quot;search&quot;"
              iconType="svgFileName"
              id="ProducerPicker"/>
            <PostOnChange
              onChange="autoSelectSoleProducerCode(producerCodeRoleEntry)"/>
          </TextInput>
          <RangeInput
            editable="true"
            id="ProducerCode"
            label="DisplayKey.get(&quot;Web.NewPolicyDV.Code&quot;)"
            optionLabel="VALUE.Code"
            required="policyPeriod.AgencyBill and producerCodeRoleEntry.Role == PolicyRole.TC_PRIMARY"
            value="producerCodeRoleEntry.ProducerCode"
            valueRange="producerCodeRoleEntry.Producer.ActiveProducerCodes"
            valueType="entity.ProducerCode"/>
        </InputIterator>
      </InputColumn>
      <InputFooterSection>
        <ListViewInput
          boldLabel="true"
          label="DisplayKey.get(&quot;Web.NewPolicyDV.Contacts&quot;)"
          labelAbove="true">
          <Toolbar>
            <ToolbarButton
              hideIfReadOnly="true"
              id="addNewContact"
              label="DisplayKey.get(&quot;Button.Add&quot;)">
              <MenuItem
                action="NewPolicyContactPopup.push(policyPeriod, Company)"
                id="addNewCompany"
                label="DisplayKey.get(&quot;Web.PolicyDetailContacts.AddNewCompany&quot;)"/>
              <MenuItem
                action="NewPolicyContactPopup.push(policyPeriod, Person)"
                hideIfReadOnly="true"
                id="addNewPerson"
                label="DisplayKey.get(&quot;Web.PolicyDetailContacts.AddNewPerson&quot;)"/>
            </ToolbarButton>
            <IteratorButtons
              addVisible="false"
              iterator="NewPolicyContactsLV"/>
            <PickerToolbarButton
              action="ContactSearchPopup.push(false)"
              available="true"
              hideIfReadOnly="true"
              id="addExistingContact"
              label="DisplayKey.get(&quot;Web.PolicyDetailContacts.AddExistingContact&quot;)"
              onPick="gw.contact.ContactConnection.connectContactToPolicy(PickedValue, policyPeriod)"/>
          </Toolbar>
          <ListViewPanel
            id="NewPolicyContactsLV">
            <RowIterator
              editable="true"
              elementName="policyContact"
              id="policyContactIterator"
              toRemove="policyPeriod.removeFromContacts(policyContact)"
              value="policyPeriod.Contacts"
              valueType="entity.PolicyPeriodContact[]">
              <Row>
                <TextCell
                  id="ContactName"
                  label="DisplayKey.get(&quot;Web.NewPolicyContactsLV.Name&quot;)"
                  value="policyContact"
                  valueType="entity.PolicyPeriodContact"/>
                <TextCell
                  id="ContactAddress"
                  label="DisplayKey.get(&quot;Web.NewPolicyContactsLV.Address&quot;)"
                  value="policyContact.Contact.PrimaryAddress"
                  valueType="entity.Address"/>
                <TextCell
                  id="ContactRoles"
                  label="DisplayKey.get(&quot;Web.NewPolicyContactsLV.Roles&quot;)"
                  value="gw.api.web.policy.PolicyPeriodUtil.getRolesForDisplay(policyContact)"/>
              </Row>
            </RowIterator>
          </ListViewPanel>
        </ListViewInput>
      </InputFooterSection>
    </DetailViewPanel>
    <Code><![CDATA[function autoSelectSoleProducerCode(producerCodeRoleEntry : ProducerCodeRoleEntry) {
        var activeProducerCodes = producerCodeRoleEntry.Producer.ActiveProducerCodes;
        if (activeProducerCodes.length == 1) {
          producerCodeRoleEntry.ProducerCode = activeProducerCodes[0];
        }
      }
function validatePrimaryProducer(producer : Producer) : String{
  if(not policyPeriod.AgencyBill){
    return null
  }
  return producer.AgencyBillPlan == null ? DisplayKey.get("Web.NewPolicyDV.InvalidPrimaryProducer") : null
}

function maybeResetPaymentPlan() {
  var overridingPayerAccount = invoicingOverridesView.OverridingPayerAccount
  if (policyPeriod.ListBill && 
      overridingPayerAccount != null &&
      !overridingPayerAccount.PaymentPlans.contains(issuance.PolicyPaymentPlan)) {
    issuance.PolicyPaymentPlan = null
  }
}

function validateProducer(producer : Producer, role : PolicyRole) : String{
  if(producer.Currency != account.Currency){
    return DisplayKey.get("Web.NewPolicyDV.InvalidProducer", account.Currency)
  }
  if(role == PolicyRole.TC_PRIMARY){
   return validatePrimaryProducer(producer)
  }
  return null
}
  ]]></Code>
  </Screen>
</PCF>