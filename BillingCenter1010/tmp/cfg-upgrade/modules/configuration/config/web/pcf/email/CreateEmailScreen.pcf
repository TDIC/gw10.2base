<?xml version="1.0" encoding="UTF-8"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Screen
    editable="true"
    id="CreateEmailScreen">
    <Require
      name="symbolTable"
      type="java.util.Map&lt;String, Object&gt;"/>
    <Require
      name="docContainer"
      type="DocumentContainer"/>
    <Require
      name="emailTemplate"
      type="String"/>
    <Require
      name="documentsToSend"
      type="Document[]"/>
    <Variable
      initialValue="null"
      name="documentToSave"
      type="Document"/>
    <Variable
      initialValue="emailTemplate == null"
      name="noDefaultTemplate"
      type="Boolean"/>
    <Variable
      initialValue="gw.api.contact.AddressBookUtil.newAddressBookContactSource()"
      name="externalContactSource"
      type="gw.api.contact.ExternalContactSource"/>
    <Variable
      initialValue="docContainer != null and perm.Document.create and gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentContentSource)"
      name="saveAsDocument"
      type="boolean"/>
    <Variable
      initialValue="false"
      name="showCC"
      type="boolean"/>
    <Variable
      initialValue="false"
      name="showBCC"
      type="boolean"/>
    <Variable
      initialValue="gw.api.util.LocaleUtil.getDefaultLanguageType()"
      name="language"
      type="LanguageType"/>
    <Variable
      initialValue="initNewEmail()"
      name="NewEmail"
      type="gw.api.email.Email"/>
    <Toolbar>
      <ToolbarButton
        action="sendEmailToRecipients(NewEmail)"
        available="true"
        id="ToolbarButton0"
        label="DisplayKey.get(&quot;Web.Email.SendEmail&quot;)"
        visible="true"/>
      <ToolbarButton
        action="CurrentLocation.cancel()"
        available="true"
        id="ToolbarButton1"
        label="DisplayKey.get(&quot;Web.Email.Cancel&quot;)"
        visible="true"/>
      <ToolbarDivider/>
      <PickerToolbarButton
        __disabled="true"
        action="PickEmailTemplatePopup.push(new gw.api.email.EmailTemplateSearchCriteria( symbolTable.Keys?.asArrayOf(String)))"
        id="EmailWorksheet_UseTemplateButton"
        label="DisplayKey.get(&quot;Web.Email.UseTemplate&quot;)"
        onPick="NewEmail.useEmailTemplate(PickedValue, symbolTable );  language = gw.api.util.LocaleUtil.toLanguageType(PickedValue.Locale)"
        shortcut="P"
        visible="noDefaultTemplate"/>
    </Toolbar>
    <AlertBar
      id="NoDefaultTemplate"
      label="DisplayKey.get(&quot;Web.Email.Template.NotFound&quot;, emailTemplate)"
      showConfirmMessage="false"
      visible="emailTemplate != null and noDefaultTemplate"/>
    <DetailViewPanel>
      <InputColumn>
        <TypeKeyInput
          editable="true"
          id="Language"
          label="DisplayKey.get(&quot;Web.EmailTemplateSearch.Language&quot;)"
          required="true"
          value="language"
          valueType="typekey.LanguageType"
          visible="LanguageType.getTypeKeys( false ).Count &gt;  1 and emailTemplate != null">
          <PostOnChange
            onChange="executeTemplate(NewEmail)"/>
        </TypeKeyInput>
        <InputSetRef
          def="CreateEmailScreenRecipientInputSet(DisplayKey.get(&quot;Web.Email.ToRecipients&quot;), NewEmail.ToRecipients)"
          editable="true"
          id="ToRecipientLVInput"/>
        <ButtonInput
          action="showCC = true"
          id="ShowCCRecipients"
          labelAbove="true"
          value="DisplayKey.get(&quot;Web.Email.AddCCRecipients&quot;)"
          visible="!showCC"/>
        <InputSetRef
          def="CreateEmailScreenRecipientInputSet(DisplayKey.get(&quot;Web.Email.CCRecipients&quot;), NewEmail.CcRecipients)"
          editable="true"
          id="ToCCRecipientLVInput"
          visible="showCC"/>
        <ButtonInput
          action="showBCC = true"
          id="ShowBCCRecipients"
          labelAbove="true"
          value="DisplayKey.get(&quot;Web.Email.AddBCCRecipients&quot;)"
          visible="!showBCC"/>
        <InputSetRef
          def="CreateEmailScreenRecipientInputSet(DisplayKey.get(&quot;Web.Email.BCCRecipients&quot;), NewEmail.BccRecipients)"
          editable="true"
          id="ToBCCRecipientLVInput"
          visible="showBCC"/>
        <InputDivider/>
        <CheckBoxInput
          align="left"
          editable="true"
          id="SaveAsDocument"
          labelAbove="true"
          value="saveAsDocument"
          valueLabel="DisplayKey.get(&quot;Web.Email.SaveAsDocument&quot;)"
          visible="docContainer != null and perm.Document.create and gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentContentSource)"/>
      </InputColumn>
      <InputColumn>
        <TextInput
          editable="true"
          id="SenderName"
          label="DisplayKey.get(&quot;Web.Email.SenderName&quot;)"
          value="NewEmail.Sender.Name"/>
        <TextInput
          editable="true"
          id="SenderEmail"
          label="DisplayKey.get(&quot;Web.Email.SenderEmail&quot;)"
          value="NewEmail.Sender.EmailAddress"/>
        <TextInput
          editable="true"
          id="Subject"
          label="DisplayKey.get(&quot;Web.Email.Subject&quot;)"
          required="true"
          value="NewEmail.Subject"/>
        <TextAreaInput
          editable="true"
          id="Body"
          label="DisplayKey.get(&quot;Web.Email.Body&quot;)"
          numRows="10"
          required="true"
          value="NewEmail.Body"/>
        <ListViewInput
          boldLabel="true"
          editable="true"
          label="DisplayKey.get(&quot;Web.Email.AttachedDocuments&quot;)">
          <Toolbar>
            <PickerToolbarButton
              action="PickExistingDocumentPopup.push(docContainer)"
              id="AddDocumentButton"
              label="DisplayKey.get(&quot;Web.Email.Add&quot;)"
              onPick="NewEmail.addDocument(PickedValue)"
              shortcut="A"
              visible="true"/>
            <IteratorButtons
              addVisible="false"
              iterator="SampleAttachedDocumentsLV"/>
          </Toolbar>
          <ListViewPanel
            editable="true"
            id="SampleAttachedDocumentsLV">
            <RowIterator
              editable="true"
              elementName="AttachedDocument"
              toRemove="NewEmail.removeDocument( AttachedDocument )"
              value="NewEmail.Documents"
              valueType="java.util.List&lt;entity.Document&gt;">
              <Row>
                <TextCell
                  editable="true"
                  id="Document"
                  label="DisplayKey.get(&quot;Web.Email.DocumentName&quot;)"
                  value="AttachedDocument.Name"/>
              </Row>
            </RowIterator>
          </ListViewPanel>
        </ListViewInput>
      </InputColumn>
    </DetailViewPanel>
    <Code><![CDATA[function initNewEmail() : gw.api.email.Email {
  var rtn = new gw.api.email.Email()
  if (emailTemplate != null) {
    executeTemplate(rtn)
  }
  if (documentsToSend != null) {
    for (document in documentsToSend) {
      rtn.addDocument( document )
    }
  }
  return rtn
}

function executeTemplate(rtn : gw.api.email.Email) {
  var templatePlugin = gw.plugin.Plugins.get(gw.plugin.email.IEmailTemplateSource)
  var template = templatePlugin.getEmailTemplate(gw.api.util.LocaleUtil.toLanguage(language), emailTemplate)
  if (template == null) {
    noDefaultTemplate = true
    throw new gw.api.util.DisplayableException(DisplayKey.get("Web.Activity.EmailTemplate.Language", emailTemplate, language))
  }
  else {
   rtn.useEmailTemplate(template, symbolTable)
  }
}

function sendEmailToRecipients(emailToSend : gw.api.email.Email) {
  var warnings = gw.api.email.EmailUtil.emailContentsValid(emailToSend)
  if (warnings.length > 0) {
    throw new gw.api.util.DisplayableException(warnings)
  }
  if (saveAsDocument) {
    var templatePlugin = gw.plugin.Plugins.get(gw.plugin.document.IDocumentTemplateSource)
    var template = templatePlugin.getDocumentTemplate("CreateEmailSent.gosu.htm", gw.api.util.LocaleUtil.getDefaultLocale())
    if (template == null) {
      throw new gw.api.util.DisplayableException(DisplayKey.get("Web.CreateEmailScreen.Error.NoManualEmailSentTemplate"))
    } else {
      documentToSave = documentToSave != null ? documentToSave : new Document()
      documentToSave.Name  = emailToSend.Subject
      documentToSave.MimeType = template.MimeType
      documentToSave.Type = typekey.DocumentType.get(template.TemplateType)
      documentToSave.Section = typekey.DocumentSection.get(template.getMetadataPropertyValue( "section" ) as String) // assignment will force it to SectionType
      documentToSave.SecurityType = typekey.DocumentSecurityType.get(template.DefaultSecurityType)
      documentToSave.Status = TC_FINAL
      documentToSave.Recipient = emailToSend.ToRecipients.first().Name
      documentToSave.Author = User.util.CurrentUser.DisplayName
      documentToSave.Inbound = false
      documentToSave.DateCreated = gw.api.util.DateUtil.currentDate()
      docContainer.addDocument( documentToSave )
      
      var paramMap = new java.util.HashMap(symbolTable)
      paramMap.put("User", User.util.CurrentUser)
      paramMap.put("Email", emailToSend)
      paramMap.put("DateSent", gw.api.util.DateUtil.currentDate())
      gw.document.DocumentProduction.createAndStoreDocumentSynchronously(template, paramMap, documentToSave)

    }
  } else if (documentToSave != null) {
    documentToSave.remove()
  }
  gw.api.email.EmailUtil.sendEmailWithBody(docContainer as KeyableBean, emailToSend)
  // it didn't throw so reset email template so that other templates can be used
  var actv = symbolTable.get("Activity")
  if (emailTemplate != null and actv typeis Activity) {
    if (actv.EmailTemplate == emailTemplate) {
      actv.EmailTemplate = null
    }
  }
  CurrentLocation.commit()
}]]></Code>
  </Screen>
</PCF>