package rules.Preupdate.BaseMoneyReceivedPreupdate_dir.TDIC_WC100000_dir.TDIC_BMPU110000DirectBillPmtRcd_dir.TDIC_BMPU111000PaymentReversed_dir

uses gw.api.system.BCLoggerCategory
uses org.slf4j.LoggerFactory

@gw.rules.RuleDisabled
@gw.rules.RuleName("TDIC_BMPU111300 - Consecutive ACH NSF Activity")
internal class TDIC_BMPU111300ConsecutiveACHNSFActivity {
  static function doCondition(baseMoneyReceived  :  entity.BaseMoneyReceived) : boolean {
/*start00rule*/
return baseMoneyReceived typeis DirectBillMoneyRcvd
    && baseMoneyReceived.PaymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_ACH
    && baseMoneyReceived.ReversalReason == PaymentReversalReason.TC_R01
/*end00rule*/
}

  static function doAction(baseMoneyReceived  :  entity.BaseMoneyReceived, actions : gw.rules.Action) {
/*start00rule*/
/**
 * US1012 - Create Activity
 * 2/10/2015 Hermia Kho
 *
 * Create Activity for consecutive APW reversed for NSF.
 */
var _logger = LoggerFactory.getLogger("Rules")
var directBillMoneyReceived = baseMoneyReceived as DirectBillMoneyRcvd
_logger.debug("TDIC_BMPU111300 : Reversal with Payment Instrument ACH encountered for account "
    + directBillMoneyReceived.Account + ". Checking for consecutive APW NSF reversal...")
if (directBillMoneyReceived.IsConsecutiveAPWPaymentAlsoNSF_TDIC) {
  _logger.debug("TDIC_BMPU111300 : Either prior or next ACH payment received was also reversed for insufficient funds.  Create activity next.")
  var actCreate = new Activity()
  actCreate.Account = directBillMoneyReceived.Account
  if (directBillMoneyReceived.Invoice?.Policies?.Count == 1) {
    actCreate.PolicyPeriod = directBillMoneyReceived.Invoice.Policies.first()
  }
  //actCreate.ActivityPattern = ActivityPattern.APWActivity
  actCreate.Description = "Two consecutive returned payments via APW. Payment instrument must be changed from APW. Acct# = "
      + directBillMoneyReceived.Account.AccountNumber
  if (actCreate.autoAssign()) {
    _logger.debug("TDIC_BMPU111300 : auto assigned successful. Assigned Group : " + actCreate.AssignedGroup.DisplayName + " | Assigned User: " + actCreate.AssignedUser.DisplayName)
  }
  else {
    _logger.warn("TDIC_BMPU111300 : auto assigned failed")
  }
}
else {
  _logger.debug("TDIC_BMPU111300 : No consecutive ACH NSF reversal.  No action taken to create activity.")
} 
/*end00rule*/
  }
}
