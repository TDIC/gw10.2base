package rules.Preupdate.BillingInstructionPreupdate_dir.TDIC_WC100000_dir

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.system.BCLoggerCategory
uses java.util.Date
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory

@gw.rules.RuleName("TDIC_BIPU120000 - Reinstatement")
internal class TDIC_BIPU120000Reinstatement {
  static function doCondition(billingInstruction:entity.BillingInstruction) : boolean {
/*start00rule*/
return billingInstruction.New && billingInstruction typeis Reinstatement
/*end00rule*/
}

  static function doAction(billingInstruction:entity.BillingInstruction, actions : gw.rules.Action) {
/*start00rule*/
var reinstatementInstruction = billingInstruction as Reinstatement

/**
 * US78 - View History - Policy
 * 08/21/2014 Alvin Lee
 *
 * Policy Reinstated History Event
 */
reinstatementInstruction.AssociatedPolicyPeriod.addHistoryFromGosu(Date.CurrentDate, HistoryEventType.TC_POLICYREINSTATED,
    DisplayKey.get("TDIC.PolicyHistory.Reinstated", reinstatementInstruction.AssociatedPolicyPeriod.PolicyNumberLong),
    null, null, false)

/**
 * US149 - Non-Pay Delinquency
 * 02/27/2015 Tim Talluto
 *
 * Close Trouble Tickets and Disbursements upon Reinstatement
 */
// locate and close TroubleTicket
for (eachTroubleTicket in reinstatementInstruction.AssociatedPolicyPeriod.Account.getHoldingTroubleTickets(HoldType.TC_DISBURSEMENTS)){
  if (eachTroubleTicket.Title == "Hold Cancellation Payment - Pending Reinstatement Request" and !eachTroubleTicket.IsClosed){
    eachTroubleTicket.CloseDate = gw.api.util.DateUtil.currentDate()
  }
}

// locate and close the Disbursement
/**
 * GW1114 - Modify to fix the bundle issue on reinstatement when there are disbursements to reject
 * 02/18/2016 Tim Talluto
 */
for (eachDisbursement in Query.make(AccountDisbursement)
                              .compare ("Account", Relop.Equals, reinstatementInstruction.AssociatedPolicyPeriod.Account)
                              //FIXME TJT - do we want to match a reason code?
                              //.compare("Reason", Relop.Equals, typekey.Reason.TC_AUTOMATIC)
                              .or (\ orCriteria -> {
                                orCriteria.compare("Status", Relop.Equals, DisbursementStatus.TC_APPROVED)
                                orCriteria.compare("Status", Relop.Equals, DisbursementStatus.TC_AWAITINGAPPROVAL)
                                })

                              .select()) {
  // looping thru disbursements
  LoggerFactory.getLogger("Rules").debug("TDIC_BIPU120000 - Reinstatement execute rejectDisbursement next: PublicId " + eachDisbursement.PublicID)
  var bundle = gw.transaction.Transaction.Current
  eachDisbursement = bundle.add(eachDisbursement)
  //201705116 TJT: GW-2572
  // Make sure we can Reject the Disbursement before trying to reject it
  // Maybe this checks to see if it is present and not already approved or rejected?
  if (eachDisbursement.canReject()){
    eachDisbursement.rejectDisbursement()
  }
}


// Look for a TT that is holding Disbursements, release the Disbursement Hold, and close TT if no more holds
// Borrowed and refactored from gw.webservice.bc.bc801.IBillingCenterAPI.gs,  private function releaseHoldsOnAccountOrPolicyPeriod()
if (reinstatementInstruction.AssociatedPolicyPeriod.Account.HasActiveTroubleTickets){
  var bundle = gw.transaction.Transaction.Current
  for (eachTT in reinstatementInstruction.AssociatedPolicyPeriod.Account.getHoldingTroubleTickets(typekey.HoldType.TC_DISBURSEMENTS)){
    eachTT = bundle.add(eachTT)
    var hold = bundle.add(eachTT.Hold)
    for (eachHoldTypeEntry in hold.HoldTypes.where( \ elt -> elt.HoldType == typekey.HoldType.TC_DISBURSEMENTS )) {
      if (hold.Disbursements.ReleaseDate != null){ // if null, was prob the user clicking the Reject & Hold button on the UI
        eachHoldTypeEntry = bundle.add(eachHoldTypeEntry)
        hold.removeFromHoldTypes(eachHoldTypeEntry)
        // 20170519 TJT - copied to HoldPreupdate rule so that the Reinstatement BI and the Batch job will both close an empty TT
        //eachTT.Hold.checkForHoldReleases()
        if (hold.HoldTypes.length == 0) {
          eachTT.close()
        }
      }
    }
  }
}

/*end00rule*/
  }
}
