package rules.Preupdate.InvoicePreupdate_dir

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount
uses org.slf4j.LoggerFactory

@gw.rules.RuleName("TDIC_IPU1000 - Persist Prior Balance")
internal class TDIC_IPU1000PersistPriorBalance {
  static function doCondition(invoice : entity.Invoice) : boolean {
/*start00rule*/
    return (invoice typeis AccountInvoice) && invoice.Changed && invoice.isFieldChanged(Invoice#Status) &&
        invoice.Billed && invoice.getOriginalValue(Invoice#Status) == InvoiceStatus.TC_PLANNED
/*end00rule*/
  }

  static function doAction(invoice : entity.Invoice, actions : gw.rules.Action) {
/*start00rule*/
    var _logger = LoggerFactory.getLogger("Rules")
    var accInvoice = invoice as AccountInvoice
    var account = accInvoice.Account
    var policyPeriod = accInvoice.InvoiceStream.PolicyPeriod_TDIC
    _logger.debug("TDIC_IPU1000 : policyPeriod - " + policyPeriod + " Invoicenumber = " + invoice.InvoiceNumber)
    if(policyPeriod != null){
      var unappliedFund = policyPeriod?.OverridingInvoiceStream != null ? policyPeriod?.OverridingInvoiceStream?.UnappliedFund
        : policyPeriod?.Policy?.getDesignatedUnappliedFund(account)
      _logger.debug("TDIC_IPU1000 : unappliedFund - " + unappliedFund)
      var amountZero = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
      var amountDue = (accInvoice.AmountDue == null) ? amountZero : accInvoice.AmountDue
      var unappliedFundBalance = amountZero

      if(unappliedFund != null and unappliedFund.Balance != null)
             unappliedFundBalance = unappliedFund.Balance

      var previousBalance = accInvoice.InvoiceStream.InvoicesSortedByEventDate.where(\elt -> elt.BilledOrDue
          and elt.InvoiceNumber != accInvoice.InvoiceNumber).sum(\a -> a.TotalOutstandingAmountTDIC)
      _logger.debug("TDIC_IPU1000 : unappliedFundBalance = " + unappliedFundBalance + " previousBalance  = " + previousBalance + " amountDue = " + amountDue)
      if(previousBalance.IsNotZero and previousBalance.IsPositive){
        accInvoice.PriorBalance_TDIC = (amountDue + previousBalance) - unappliedFundBalance
      } else {
        accInvoice.PriorBalance_TDIC = amountDue - unappliedFundBalance
      }
    }


/*end00rule*/
  }
}
