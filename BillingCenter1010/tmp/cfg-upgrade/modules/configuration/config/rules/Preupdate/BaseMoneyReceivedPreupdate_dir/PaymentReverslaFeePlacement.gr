package rules.Preupdate.BaseMoneyReceivedPreupdate_dir

uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.domain.accounting.ChargeReturnObject
uses gw.api.financials.MonetaryAmounts
uses gw.api.util.DateUtil
uses org.slf4j.LoggerFactory
uses com.guidewire.bc.util.BCEntities
uses com.guidewire.pl.system.util.DateTimeUtil

uses java.math.BigDecimal

@gw.rules.RuleName("PaymentReverslaFeePlacement")
internal class PaymentReverslaFeePlacement {
  static function doCondition(baseMoneyReceived : entity.BaseMoneyReceived) : boolean {
/*start00rule*/
    return baseMoneyReceived typeis DirectBillMoneyRcvd &&
        !(baseMoneyReceived typeis ZeroDollarDMR) &&
        !(baseMoneyReceived typeis ZeroDollarReversal) &&
        baseMoneyReceived.Reversed and (baseMoneyReceived.OriginalVersion!=null ? !(baseMoneyReceived.OriginalVersion as DirectBillMoneyRcvd).Reversed : true)
/*end00rule*/
  }

  static function doAction(baseMoneyReceived : entity.BaseMoneyReceived, actions : gw.rules.Action) {
/*start00rule*/

    var dbmr = (baseMoneyReceived as DirectBillMoneyRcvd)
    var _logger = LoggerFactory.getLogger("Rules")
    var bundle = gw.transaction.Transaction.getCurrent()
    var policyPeriod = dbmr.BaseDist?.ReversedDistItems?.firstWhere(\elt -> elt.PolicyPeriod != null).PolicyPeriod
    if(policyPeriod==null) {
      policyPeriod = dbmr.BaseDist.DistItems*.InvoiceItem*.Charge?.first()?.PolicyPeriod
    }
    if(policyPeriod==null) {
      policyPeriod = dbmr.PolicyPeriod
    }
    if (policyPeriod != null) {
      var isOnlyPymntReversalInAccount : boolean = !policyPeriod?.Account.AllPolicyPeriods.hasMatch(\elt -> elt.Charges?.hasMatch(\elt1 -> elt1.ChargePattern.ChargeCode.equalsIgnoreCase("PolicyPaymentReversalFee")
          and elt1.CreateTime?.trimToMidnight() == DateUtil.currentDate().trimToMidnight()))
      var validReasons : PaymentReversalReason[] = {PaymentReversalReason.TC_R01, PaymentReversalReason.TC_R02, PaymentReversalReason.TC_R08, PaymentReversalReason.TC_RETURNEDCHECK, PaymentReversalReason.TC_ATFAULT_TDIC}
      if (isOnlyPymntReversalInAccount && validReasons.contains(dbmr.ReversalReason)) {
        _logger.info("Rule : BaseMoneyReceivedPreUpdate : PaymentReversalFeePlacement : for Policy " + policyPeriod.PolicyNumber+" adding reversal fee upon reversal of amount "+dbmr.Amount)
        var reversalFeeAmount = ScriptParameters.getParameterValue("TDIC_PaymentReversalFeeAmount")!=null ? (ScriptParameters.getParameterValue("TDIC_PaymentReversalFeeAmount") as BigDecimal) : null
        if (reversalFeeAmount != null && reversalFeeAmount > 0) {
          var overridingPayer : Account = (policyPeriod.DefaultPayer typeis Account)
              ? policyPeriod.DefaultPayer
              : policyPeriod.Account
          var chargeData : ChargeReturnObject = new ChargeReturnObject(ChargePatternKey.POLICYPAYMENTREVERSALFEE.get(),
              policyPeriod,
              overridingPayer)
          var nextPlannedInvoice = policyPeriod.InvoicesSortedByEventDate.
              where(\inv -> inv.Planned and inv.EventDate?.trimToMidnight() > DateUtil.currentDate()?.trimToMidnight())?.first()
          if (nextPlannedInvoice == null) {
            var stream = policyPeriod.InvoicesSortedByEventDate?.last()?.InvoiceStream
            stream = bundle.add(stream)
            var anchorDates = {stream.AnchorDates?.first()?.toDate()}
            if (stream.AnchorDates?.size() > 1)
              anchorDates.add(stream.AnchorDates[1]?.toDate())
            var dateSequence = new gw.plugin.invoice.impl.DateSequence().createPeriodicSequenceWith(Periodicity.TC_MONTHLY, (anchorDates.toTypedArray()))
            nextPlannedInvoice = stream.createAndAddInvoice(dateSequence.firstAfter(DateUtil.currentDate()?.trimToMidnight()))
            _logger.info("Rule : BaseMoneyReceivedPreUpdate : PaymentReversalFeePlacement : for Policy " + policyPeriod.PolicyNumber+" ; Created new planned Invoice : "+nextPlannedInvoice.InvoiceNumber)
          } else {
            _logger.info("Rule : BaseMoneyReceivedPreUpdate : PaymentReversalFeePlacement : for Policy " + policyPeriod.PolicyNumber+" ; Existing planned Invoice found : "+nextPlannedInvoice.InvoiceNumber)
          }
          var generalBI : BillingInstruction = (BCEntities.createWithSameCurrencyAs(General.TYPE, chargeData.getOwner() as InCurrencySilo) as BillingInstruction)
          (generalBI as General).setModificationDate(DateTimeUtil.getNow());
          (generalBI as General).setAssociatedPolicyPeriod(chargeData.getOwner() as PolicyPeriod)
          var initializer = generalBI.buildCharge(MonetaryAmounts.zeroIfNull(reversalFeeAmount, policyPeriod.Account.Currency), chargeData.ChargePattern)
          if (chargeData.getPayer() != null) {
            initializer.setOverridingAccountPayer(chargeData.getPayer())
          }
          generalBI.execute()

          if (nextPlannedInvoice != null) {
            var items = initializer.Charge.InvoiceItems
            nextPlannedInvoice = bundle.add(nextPlannedInvoice)
            for (item in items) {
              item = bundle.add(item)
            }
            gw.api.web.invoice.InvoiceUtil.moveInvoiceItems(nextPlannedInvoice, items)
          }
        } else {
          _logger.info("Rule : BaseMoneyReceivedPreUpdate : PaymentReversalFeePlacement : for Policy " + policyPeriod.PolicyNumber + " ReversalFee is not valid")
        }
      }
    }
/*end00rule*/
  }
}
