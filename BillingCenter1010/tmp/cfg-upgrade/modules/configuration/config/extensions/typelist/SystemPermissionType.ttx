<?xml version="1.0"?>
<!-- Typelists related to security -->
<typelistextension
  xmlns="http://guidewire.com/typelists"
  desc="Defines all permissions that can be granted to users via privileges and roles."
  name="SystemPermissionType">
  <typecode
    code="workflowview"
    desc="Permission to view the Workflow page"
    name="View workflow page"/>
  <!-- UI Permissions -->
  <typecode
    code="viewdesktop"
    desc="Permission to view the desktop"
    name="View desktop"/>
  <typecode
    code="useradmin"
    desc="Permission to see the user/group administration screens. Also governs use of command-line tools and user attribute admin."
    name="View user administration screens"/>
  <typecode
    code="admintabview"
    desc="Permission to view the Admin tab"
    name="View admin tab"/>
  <typecode
    code="pmntplanview"
    desc="Permission to view payment plans"
    name="View payment plans"/>
  <typecode
    code="pmntplancreate"
    desc="Permission to create a new payment plan"
    name="Create payment plan"/>
  <typecode
    code="pmntplanedit"
    desc="Permission to change an existing payment plan"
    name="Edit payment plan"/>
  <typecode
    code="retpremplanview"
    desc="Permission to view return premium plans"
    name="View return premium plans"/>
  <typecode
    code="retpremplancreate"
    desc="Permission to create a new return premium plan"
    name="Create return premium plan"/>
  <typecode
    code="retpremplanedit"
    desc="Permission to change an existing return premium plan"
    name="Edit return premium plan"/>
  <typecode
    code="payallocplanview"
    desc="Permission to view payment allocation plans"
    name="View payment allocation plans"/>
  <typecode
    code="payallocplancreate"
    desc="Permission to create a new payment allocation plan"
    name="Create payment allocation plan"/>
  <typecode
    code="payallocplanedit"
    desc="Permission to change an existing payment allocation plan"
    name="Edit payment allocation plan"/>
  <typecode
    code="agencybillplanview"
    desc="Permission to view agency bill plans"
    name="View agency bill plans"/>
  <typecode
    code="agencybillplancreate"
    desc="Permission to create a new agency bill plan"
    name="Create agency bill plan"/>
  <typecode
    code="agencybillplanedit"
    desc="Permission to change an existing agency bill plan"
    name="Edit agency bill plan"/>
  <typecode
    code="billplanview"
    desc="Permission to view billing plans"
    name="View billing plans"/>
  <typecode
    code="billplancreate"
    desc="Permission to create a new billing plan"
    name="Create billing plan"/>
  <typecode
    code="billplanedit"
    desc="Permission to change an existing billing plan"
    name="Edit billing plan"/>
  <typecode
    code="delplanview"
    desc="Permission to view delinquency plans"
    name="View delinquency plans"/>
  <typecode
    code="delplancreate"
    desc="Permission to create a new delinquency plan"
    name="Create delinquency plans"/>
  <typecode
    code="delplanedit"
    desc="Permission to change an existing delinquency plan"
    name="Edit delinquency plan"/>
  <typecode
    code="colagencyview"
    desc="Permission to view collection agencies"
    name="View collection agencies"/>
  <typecode
    code="colagencycreate"
    desc="Permission to create a new collection agency"
    name="Create collection agency"/>
  <typecode
    code="colagencyedit"
    desc="Permission to change an existing collection agency"
    name="Edit collection agency"/>
  <typecode
    code="colagencydelete"
    desc="Permission to remove an existing collection agency"
    name="Remove collection agency"/>
  <typecode
    code="commplanview"
    desc="Permission to view commission plans"
    name="View commission plans"/>
  <typecode
    code="commplancreate"
    desc="Permission to create a new commission plan"
    name="Create commission plans"/>
  <typecode
    code="commplanedit"
    desc="Permission to change an existing commission plan"
    name="Edit commission plan"/>
  <typecode
    code="chargepatternview"
    desc="Permission to view charge patterns"
    name="View charge patterns"/>
  <typecode
    code="chargepatterncreate"
    desc="Permission to create a new charge pattern"
    name="Create charge patterns"/>
  <typecode
    code="chargepatternedit"
    desc="Permission to edit an existing charge pattern"
    name="Edit charge patterns"/>
  <typecode
    code="usersearch"
    desc="Permission to search for users"
    name="User search"/>
  <typecode
    code="alpmanage"
    desc="Permission create, edit, or delete authority limit profiles"
    name="Manage authority limit profiles"/>
  <typecode
    code="eventmessageview"
    desc="Permission to view the event messages page"
    name="View event messages page"/>
  <typecode
    code="myttktview"
    desc="Permission to view Desktop | My Trouble Tickets screen"
    name="View desktop trouble tickets"/>
  <typecode
    code="mydelinquenciesview"
    desc="Permission to view Desktop | My Delinquencies screen"
    name="View desktop delinquencies"/>
  <typecode
    code="mydisbview"
    desc="Permission to view Desktop | My Disbursements screen"
    name="View desktop disbursements"/>
  <typecode
    code="myagencyitemsview"
    desc="Permission to view Desktop | My Agency Items screen"
    name="View desktop agency items"/>
  <typecode
    code="transferreversal"
    desc="Permission to Execute a transfer reversal"
    name="Execute Transfer Reversal"/>
  <typecode
    code="acctcontview"
    desc="Permission to view Accounts | Contacts screen"
    name="View account contacts screen"/>
  <typecode
    code="acctchargesview"
    desc="Permission to view Accounts | Charges screen"
    name="View account charges screen"/>
  <typecode
    code="acctcollview"
    desc="Permission to view Accounts | Collateral screen"
    name="View account collateral screen"/>
  <typecode
    code="acctdisbview"
    desc="Permission to view Accounts | Disbursements screen"
    name="View account disbursements screen"/>
  <typecode
    code="acctinvcview"
    desc="Permission to view Accounts | Invoices screen"
    name="View account invoices screen"/>
  <typecode
    code="acctpmntview"
    desc="Permission to view Accounts | Payments screen"
    name="View account payments screen"/>
  <typecode
    code="accttxnview"
    desc="Permission to view Accounts | Transactions screen"
    name="View account transactions screen"/>
  <typecode
    code="acctplcyview"
    desc="Permission to view Accounts | Policies screen"
    name="View account policies screen"/>
  <typecode
    code="accthistview"
    desc="Permission to view Accounts | History screen"
    name="View account history screen"/>
  <typecode
    code="acctevalview"
    desc="Permission to view Accounts | Evaluation screen"
    name="View account evaluation screen"/>
  <typecode
    code="acctttktview"
    desc="Permission to view Accounts | Trouble Tickets screen"
    name="View account trouble tickets screen"/>
  <typecode
    code="acctdocview"
    desc="Permission to view Accounts | Documents screen"
    name="View account documents screen"/>
  <typecode
    code="acctnoteview"
    desc="Permission to view Accounts | Notes screen"
    name="View account notes screen"/>
  <typecode
    code="acctledgerview"
    desc="Permission to view Accounts | Ledger screen"
    name="View account ledger screen"/>
  <typecode
    code="acctjournalview"
    desc="Permission to view Accounts | Journal screen"
    name="View account journal screen"/>
  <typecode
    code="acctdelview"
    desc="Permission to view Accounts | Delinquencies screen"
    name="View account delinquencies screen"/>
  <typecode
    code="plcycommview"
    desc="Permission to view Policies | Commissions screen"
    name="View policy commissions screen"/>
  <typecode
    code="plcycontview"
    desc="Permission to view Policies | Contacts screen"
    name="View policy contacts screen"/>
  <typecode
    code="plcychargesview"
    desc="Permission to view Policies | Charges screen"
    name="View policy charges screen"/>
  <typecode
    code="plcydelview"
    desc="Permission to view Policies | Delinquencies screen"
    name="View policy delinquencies screen"/>
  <typecode
    code="plcydocview"
    desc="Permission to view Policies | Documents screen"
    name="View policy documents screen"/>
  <typecode
    code="plcyhistview"
    desc="Permission to view Policies | History screen"
    name="View policy histories screen"/>
  <typecode
    code="plcyjournalview"
    desc="Permission to view Policies | Journal screen"
    name="View policy journal screen"/>
  <typecode
    code="plcyledgerview"
    desc="Permission to view Policies | Ledger screen"
    name="View policy ledger screen"/>
  <typecode
    code="plcynoteview"
    desc="Permission to view Policies | Notes screen"
    name="View policy notes screen"/>
  <typecode
    code="plcypmntview"
    desc="Permission to view Policies | Payments screen"
    name="View policy payments screen"/>
  <typecode
    code="plcyttktview"
    desc="Permission to view Policies | Trouble Tickets screen"
    name="View policy trouble tickets screen"/>
  <typecode
    code="plcytxnview"
    desc="Permission to view Policies | Transactions screen"
    name="View policy transactions screen"/>
  <typecode
    code="prodcontview"
    desc="Permission to view Producers | Contacts screen"
    name="View producer contacts screen"/>
  <typecode
    code="prodplcyview"
    desc="Permission to view Producers | Policies screen"
    name="View producer policies screen"/>
  <typecode
    code="prodpmntview"
    desc="Permission to view Producers | Payments screen"
    name="View producer payments screen"/>
  <typecode
    code="prodpmntedit"
    desc="Permission to edit Producers | Payments screen"
    name="Edit producer payments screen"/>
  <typecode
    code="prodpromview"
    desc="Permission to view Producers | Promises screen"
    name="View producer promises screen"/>
  <typecode
    code="prodpromedit"
    desc="Permission to edit Producers | Promises screen"
    name="Edit producer promises screen"/>
  <typecode
    code="prodabcyclesview"
    desc="Permission to view Producers | Agency Bill Cycles screen"
    name="View agency bill cycles screen"/>
  <typecode
    code="prodabopenitemsview"
    desc="Permission to view Producers | Agency Bill Open Items screen"
    name="View agency bill open items screen"/>
  <typecode
    code="prodabexceptionsview"
    desc="Permission to view Producers | Agency Bill Exceptions screen"
    name="View agency bill exceptions screen"/>
  <typecode
    code="proddbtxnview"
    desc="Permission to a view Producers | Transactions (Direct Bill) screen"
    name="View producer transactions screen"/>
  <typecode
    code="proddbstmtview"
    desc="Permission to view Producers | Statements (Direct Bill) screen"
    name="View producer direct bill statements screen"/>
  <typecode
    code="prodttktview"
    desc="Permission to view Producers | Trouble Tickets screen"
    name="View producer trouble tickets screen"/>
  <typecode
    code="proddocview"
    desc="Permission to view Producers | Documents screen"
    name="View producer documents screen"/>
  <typecode
    code="prodnoteview"
    desc="Permission to view Producers | Notes screen"
    name="View producer notes screen"/>
  <typecode
    code="prodhistview"
    desc="Permission to view Producers | History screen"
    name="View producer history screen"/>
  <typecode
    code="prodledgerview"
    desc="Permission to view Producers | Ledger screen"
    name="View producer ledger screen"/>
  <typecode
    code="prodjournalview"
    desc="Permission to view Producers | Journal screen"
    name="View producer journal screen"/>
  <typecode
    code="prodwriteoffsview"
    desc="Permission to view Producers | Write-Offs screen"
    name="View producer write-offs screen"/>
  <typecode
    code="prodwriteoffsedit"
    desc="Permission to make edits on the Producers | Write-Offs screen"
    name="Make edits on the producer write-offs screen"/>
  <typecode
    code="prodsuspitemsview"
    desc="Permission to view Producers | Suspense Items screen"
    name="View producer suspense items screen"/>
  <typecode
    code="prodsuspitemsedit"
    desc="Permission to make edits on the Producers | Suspense Items screen"
    name="Make edits on the producer suspense items screen"/>
  <typecode
    code="invcsearch"
    desc="Permission to search for invoices"
    name="Search invoices"/>
  <typecode
    code="pmntsearch"
    desc="Permission to search for payments"
    name="Search payments"/>
  <typecode
    code="txnsearch"
    desc="Permission to search for transactions"
    name="Search transactions"/>
  <typecode
    code="actsearch"
    desc="Permission to search for activities"
    name="Search activities"/>
  <typecode
    code="ttktsearch"
    desc="Permission to search for trouble tickets"
    name="Search trouble tickets"/>
  <typecode
    code="delsearch"
    desc="Permission to search for delinquencies"
    name="Search delinquencies"/>
  <typecode
    code="disbsearch"
    desc="Permission to search for disbursements"
    name="Search disbursements"/>
  <typecode
    code="outpmntsearch"
    desc="Permission to search for outgoing payments"
    name="Search outgoing payments"/>
  <typecode
    code="agencymoneyrecdsearch"
    desc="Permission to search for agency money receipts"
    name="Search Agency Money Receipts"/>
  <typecode
    code="pmntreqsearch"
    desc="Permission to search for payment requests"
    name="Search payment requests"/>
  <typecode
    code="acctchargesedit_TDIC"
    desc="Permission to edit Accounts | Charges screen"
    name="Edit account charges screen"/>
  <typecode
    code="plcychargesedit_TDIC"
    desc="Permission to edit Policies | Charges screen"
    name="Edit policy charges screen"/>
  <typecode
    code="disbreview_TDIC"
    desc="Mark a Disbursement as Reviewed"
    name="Review Disbursements"/>
  <typecode
    code="deliqedit_TDIC"
    desc="Permission to edit Deliquency plan only for super user"
    name="Edit Deliquency plan"/>
  <typecode
    code="plcyreopen_TDIC"
    desc="Reopen Policy"
    name="Reopen Policy"/>
  <typecode
    code="TransferNonTermLevelUnapplied_TDIC"
    desc="Transfer to a non term-level unapplied fund"
    name="Transfer to a non term-level unapplied fund"/>
  <typecode
    code="businessadmin"
    desc="General business settings admin"
    name="Business settings admin"/>
  <typecode
    code="monitoradmin"
    desc="General monitoring admin"
    name="Monitoring admin"/>
  <typecode
    code="utilitiesadmin"
    desc="General utilities admin"
    name="Utilities admin"/>
  <typecode
    code="acctfundstrackingview"
    desc="Permission to view Account | Funds Tracking screen"
    name="View account funds tracking screen"/>
  <typecode
    code="moveinvitem"
    desc="Permission to Move Invoice Items on Charges Screen"
    name="Permission to Move Invoice Items on Charges Screen"/>
  <typecode
    code="modifyinvitem"
    desc="Permission to Modify Invoice Items on Charges Screen"
    name="Permission to Modify Invoice Items on Charges Screen"/>
  <typecode
    code="pmntreqonetimecreate_TDIC"
    desc="Permission to create one time payment request"
    name="Create One Time Payment Request"/>
  <typecode
    code="ownsensclaim"
    desc="Permission to own sensitive claims"
    name="Own sensitive claims"
    retired="true"/>
  <typecode
    code="ownsensclaimsub"
    desc="Permission to own subobjects on sensitive claims"
    name="Own sensitive claim subobjects"
    retired="true"/>
  <typecode
    code="viewsensdoc"
    desc="Permission to view a sensitive document"
    name="View sensitive documents"
    retired="true"/>
  <typecode
    code="editsensdoc"
    desc="Permission to edit a sensitive document"
    name="Edit sensitive documents"
    retired="true"/>
  <typecode
    code="delsensdoc"
    desc="Permission to delete a sensitive document"
    name="Delete sensitive documents"
    retired="true"/>
  <typecode
    code="viewprivnote"
    desc="Permission to view a private note"
    name="View private note"
    retired="true"/>
  <typecode
    code="editprivnote"
    desc="Permission to edit a private note"
    name="Edit private note"
    retired="true"/>
  <typecode
    code="delprivnote"
    desc="Permission to delete a private note"
    name="Delete private note"
    retired="true"/>
  <typecode
    code="viewsensnote"
    desc="Permission to view a sensitive note"
    name="View sensitive note"
    retired="true"/>
  <typecode
    code="editsensnote"
    desc="Permission to edit a sensitive note"
    name="Edit sensitive note"
    retired="true"/>
  <typecode
    code="delsensnote"
    desc="Permission to delete a sensitive note"
    name="Delete sensitive note"
    retired="true"/>
  <typecode
    code="viewmednote"
    desc="Permission to view a medical note"
    name="View medical note"
    retired="true"/>
  <typecode
    code="editmednote"
    desc="Permission to edit a medical note"
    name="Edit medical note"
    retired="true"/>
  <typecode
    code="delmednote"
    desc="Permission to delete a medical note"
    name="Delete medical note"
    retired="true"/>
  <typecode
    code="viewSensSIUdetails"
    desc="Permission to view sensitive SIU details"
    name="View sensitive SIU details"
    retired="true"/>
  <typecode
    code="editSensSIUdetails"
    desc="Permission to edit sensitive SIU details"
    name="Edit sensitive SIU details"
    retired="true"/>
  <typecode
    code="viewSensMCMdetails"
    desc="Permission to view sensitive Medical Case Management details"
    name="View sensitive Med Case Mgmt details"
    retired="true"/>
  <typecode
    code="editSensMCMdetails"
    desc="Permission to edit sensitive Medical Case Management details"
    name="Edit sensitive Med Case Mgmt details"
    retired="true"/>
  <typecode
    code="StorageUpdate"
    desc="Permission to edit claim storage information"
    name="Edit claim storage information"
    retired="true"/>
  <typecode
    code="viewsubrodetails"
    desc="Permission to view Subrogation-related information"
    name="View Subrogation details"
    retired="true"/>
  <typecode
    code="editsubrodetails"
    desc="Permission to edit Subrogation-related information"
    name="Edit Subrogation details"
    retired="true"/>
  <typecode
    code="report_user"
    desc="Permission to view reports in report server"
    name="Show reports"
    retired="true"/>
  <typecode
    code="report_manager"
    desc="Permission to view reports and dashboard in report server"
    name="Show reports and dashboard"
    retired="true"/>
  <typecode
    code="viewrefdata"
    desc="Permission to view administration reference data"
    name="View reference data"
    retired="true"/>
  <typecode
    code="editrefdata"
    desc="Permission to edit administration reference data"
    name="Edit reference data"
    retired="true"/>
  <typecode
    code="viewpolicysystem"
    desc="Permission to view policy in policy system"
    name="View policy system"
    retired="true"/>
  <typecode
    code="riedit"
    desc="Permission to edit RI transactions &amp; agreements"
    name="Edit RI transactions &amp; agreements"
    retired="true"/>
  <typecode
    code="riview"
    desc="Permission to view RI transactions &amp; agreements "
    name="View RI transactions &amp; agreements"
    retired="true"/>
  <typecode
    code="viewAdminHistory_ext"
    desc="Permission to view admin auditing records"
    name="View Admin History"
    retired="true"/>
  <typecode
    code="plcydtlsscrnedit_TDIC"
    desc="Permission to edit Policies | Details screen"
    name="Edit policy Details screen"/>
  <typecode
    code="manualDlnq_TDIC"
    desc="Start and Stop delinquency manually"
    name="Start and Stop delinquency manually"/>
  <typecode
    code="hideprodactivt_TDIC"
    desc="Permission to hide producer activities for Broker"
    name="hide producer activities for Broker"/>
  <typecode
    code="hidecontsearch_TDIC"
    desc="Permission to hide contact search for Broker"
    name="hide contact search for Broker"/>
  <typecode
    code="pmntmanproc_tdic"
    name="Process single payment_tdic"/>
</typelistextension>