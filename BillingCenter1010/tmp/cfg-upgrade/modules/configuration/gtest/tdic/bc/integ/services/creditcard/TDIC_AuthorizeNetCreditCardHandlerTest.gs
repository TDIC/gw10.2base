package tdic.bc.integ.services.creditcard

uses gw.api.util.DisplayableException
uses java.lang.Exception
uses java.lang.Thread
uses java.math.BigDecimal
uses java.util.Calendar
uses gw.api.locale.DisplayKey

/**
 * US679, US680, DE67
 * 01/14/2015 Rob Kelly
 *
 * Tests the functionality of <code>TDIC_AuthorizeNetCreditCardHandler</code>.
 */
class TDIC_AuthorizeNetCreditCardHandlerTest extends tdic.TDICTestBase {

  // this Login ID and Transaction Key are associated with the Authorize.Net developer account tdicun1ttest, to be used for unit testing *only*
  /*private var apiLoginID = "4h2Xcm4ALU"
  private var transactionKey = "9h3643gRg9MTqvSv"
  private var authNetHandler : TDIC_AuthorizeNetCreditCardHandler
  private var paymentCutoffTime : String

  override function beforeMethod() {

    var rightNow = Calendar.getInstance()
    this.paymentCutoffTime = rightNow.get(Calendar.HOUR) + 2 + ":30"
    this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler(apiLoginID, transactionKey, "sandbox", "10000", this.paymentCutoffTime)
  }

  function testStartWithNullAccountNullPaymentInstrument() {

    this.authNetHandler.startCreditCardProcess(null, null)
  }

  function testStartWithNullAccount() {

    this.authNetHandler.startCreditCardProcess(null, createTestCreditCardPaymentInstrument())
  }

  function testStartWithNullPaymentInstrument() {

    this.authNetHandler.startCreditCardProcess(createTestAccount(), null)
  }

  function testStartWithNonCreditCardPaymentInstrument() {

    this.authNetHandler.startCreditCardProcess(createTestAccount(), createTestCashPaymentInstrument())
  }

  function testStartWithInvalidLoginID() {

    try {

      this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("XXX", "6w736Vn87GtpzR4u", "sandbox", "300000", this.paymentCutoffTime)
      this.authNetHandler.startCreditCardProcess(createTestAccount(), createTestCreditCardPaymentInstrument())
      fail("failure - DisplayableException should be thrown for invalid Login ID")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains("User authentication failed due to invalid authentication values"))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for invalid Login ID: ${e.getMessage()}")
    }
  }

  function testStartWithInvalidEnvironment() {

    try {

      this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "XXX", "300000", this.paymentCutoffTime)
      this.authNetHandler.startCreditCardProcess(createTestAccount(), createTestCreditCardPaymentInstrument())
      fail("failure - DisplayableException should be thrown for invalid Environment")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains("Unsupported Value"))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for invalid Environment: ${e.getMessage()}")
    }
  }

  function testStartWithNegativeTimeOut() {

    try {

      this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "sandbox", "-1", this.paymentCutoffTime)
      this.authNetHandler.startCreditCardProcess(createTestAccount(), createTestCreditCardPaymentInstrument())
      fail("failure - DisplayableException should be thrown for negative Time Out")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains("Unsupported Value"))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for negative Time Out: ${e.getMessage()}")
    }
  }

  function testStartWithInvalidTimeOut() {

    try {

      this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "sandbox", "abc", this.paymentCutoffTime)
      this.authNetHandler.startCreditCardProcess(createTestAccount(), createTestCreditCardPaymentInstrument())
      fail("failure - DisplayableException should be thrown for invalid Time Out")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains("Unsupported Value"))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for invalid Time Out: ${e.getMessage()}")
    }
  }

  function testConstructWithNullCutoffTime() {

    this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "sandbox", "300000", null)
    var cutoffTime = Calendar.getInstance()
    cutoffTime.setTime(this.authNetHandler.getPaymentCutOffTime())
    assertEquals("failure - cut off hour not set to default value", 17, cutoffTime.get(Calendar.HOUR_OF_DAY))
    assertEquals("failure - cut off minute not set to default value", 29, cutoffTime.get(Calendar.MINUTE))
  }

  function testConstructWithInvalidCutoffTimeFormat() {

    this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "sandbox", "300000", "1630")
    var cutoffTime = Calendar.getInstance()
    cutoffTime.setTime(this.authNetHandler.getPaymentCutOffTime())
    assertEquals("failure - cut off hour not set to default value", 17, cutoffTime.get(Calendar.HOUR_OF_DAY))
    assertEquals("failure - cut off minute not set to default value", 29, cutoffTime.get(Calendar.MINUTE))
  }

  function testConstructWithInvalidCutoffTimeHour() {

    this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "sandbox", "300000", "25:30")
    var cutoffTime = Calendar.getInstance()
    cutoffTime.setTime(this.authNetHandler.getPaymentCutOffTime())
    assertEquals("failure - cut off hour not set to default value", 17, cutoffTime.get(Calendar.HOUR_OF_DAY))
    assertEquals("failure - cut off minute not set to default value", 29, cutoffTime.get(Calendar.MINUTE))
  }

  function testConstructWithInvalidCutoffTimeMinute() {

    this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "sandbox", "300000", "17:60")
    var cutoffTime = Calendar.getInstance()
    cutoffTime.setTime(this.authNetHandler.getPaymentCutOffTime())
    assertEquals("failure - cut off hour not set to default value", 17, cutoffTime.get(Calendar.HOUR_OF_DAY))
    assertEquals("failure - cut off minute not set to default value", 29, cutoffTime.get(Calendar.MINUTE))
  }

  function testConstructWithInvalidCutoffTimeTime() {

    this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "sandbox", "300000", "i7:30")
    var cutoffTime = Calendar.getInstance()
    cutoffTime.setTime(this.authNetHandler.getPaymentCutOffTime())
    assertEquals("failure - cut off hour not set to default value", 17, cutoffTime.get(Calendar.HOUR_OF_DAY))
    assertEquals("failure - cut off minute not set to default value", 29, cutoffTime.get(Calendar.MINUTE))
  }

  function testConstructWithValidCutoffTime() {

    this.authNetHandler = new TDIC_AuthorizeNetCreditCardHandler("7tU4q4Dp", "6w736Vn87GtpzR4u", "sandbox", "300000", "16:15")
    var cutoffTime = Calendar.getInstance()
    cutoffTime.setTime(this.authNetHandler.getPaymentCutOffTime())
    assertEquals("failure - cut off hour incorrect", 16, cutoffTime.get(Calendar.HOUR_OF_DAY))
    assertEquals("failure - cut off minute incorrect", 15, cutoffTime.get(Calendar.MINUTE))
  }

  function testNotTimedOutBeforeStart() {

    assertFalse("failure - newly constructed Credit Card Handler should not be timed out", this.authNetHandler.isTimedOut())
  }

  function testStartCreatesProfile() {

    this.authNetHandler.startCreditCardProcess(createTestAccount(), createTestCreditCardPaymentInstrument())
    assertNotNull("failure - customer profile id should not be null", this.authNetHandler.getCustomerProfileID())
    this.authNetHandler.finishCreditCardProcess()
  }

  function testStartCreatesHostedPageToken() {

    var paymentInstrument = createTestCreditCardPaymentInstrument()
    this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
    assertNotNull("failure - hosted page token is null", paymentInstrument.Token)
    this.authNetHandler.finishCreditCardProcess()
  }

  function testPopulateWithNullPaymentInstrument() {

    this.authNetHandler.startCreditCardProcess(createTestAccount(), createTestCreditCardPaymentInstrument())
    this.authNetHandler.populateCreditCardFields(null)
    this.authNetHandler.finishCreditCardProcess()
  }

  function testPopulateWithNonCreditCardPaymentInstrument() {

    this.authNetHandler.startCreditCardProcess(createTestAccount(), createTestCreditCardPaymentInstrument())
    this.authNetHandler.populateCreditCardFields(createTestCashPaymentInstrument())
    this.authNetHandler.finishCreditCardProcess()
  }

  function testPopulateWithoutStart() {

    try {

      this.authNetHandler.populateCreditCardFields(createTestCreditCardPaymentInstrument())
      fail("failure - DisplayableException should be thrown for populate without start")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains(DisplayKey.get("TDIC.CreditCardHandler.ProcessNotStarted")))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for populate without start: ${e.getMessage()}")
    }
  }

  function testPopulateWithoutPaymentProfile() {

    var paymentInstrument = createTestCreditCardPaymentInstrument()
    try {

      this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
      this.authNetHandler.populateCreditCardFields(paymentInstrument)
      fail("failure - DisplayableException should be thrown for populate without payment profile")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains("No payment profile returned"))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for populate without payment profile: ${e.getMessage()}")
    }
    finally {

      this.authNetHandler.finishCreditCardProcess()
      assertNull("failure - Credit Card Number should be null without payment profile", paymentInstrument.CreditCardNumber_TDIC)
    }
  }

  function testPopulateAfterTimeOut() {

    try {

      var paymentInstrument = createTestCreditCardPaymentInstrument()
      this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
      Thread.sleep(10001)
      this.authNetHandler.populateCreditCardFields(paymentInstrument)
      fail("failure - DisplayableException should be thrown for populate after time out")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains(DisplayKey.get("TDIC.CreditCardHandler.ProcessTimeOut")))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for populate after time out: ${e.getMessage()}")
    }
    finally {

      this.authNetHandler.finishCreditCardProcess()
    }
  }

  function testNumberOfTimedOutHandlers() {

    var paymentInstrument = createTestCreditCardPaymentInstrument()
    this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
    Thread.sleep(10001)
    var timedOutHandlers = TDIC_AuthorizeNetCreditCardHandler.getTimedOutHandlers()
    assertEquals("failure - incorrect number of timed out handlers returned", 1, timedOutHandlers.size())
    this.authNetHandler.finishCreditCardProcess()
  }

  function testCorrectTimedOutHandlerReturned() {

    var paymentInstrument = createTestCreditCardPaymentInstrument()
    this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
    Thread.sleep(10001)
    var timedOutHandlers = TDIC_AuthorizeNetCreditCardHandler.getTimedOutHandlers()
    assertTrue("failure - incorrect handler returned", timedOutHandlers.first().equals(this.authNetHandler))
    this.authNetHandler.finishCreditCardProcess()
  }

  function testPopulateRetrievesPaymentProfile() {

    var paymentInstrument = createTestCreditCardPaymentInstrument()
    this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
    createPaymentProfile(this.authNetHandler.getCustomerProfileID())
    this.authNetHandler.populateCreditCardFields(paymentInstrument)
    var paymentProfileID = this.authNetHandler.getPaymentProfileID()
    this.authNetHandler.finishCreditCardProcess()
    assertNotNull("failure - payment profile id should not be null", paymentProfileID)
  }

  function testPopulateRetrievesCardNumber() {

    var paymentInstrument = createTestCreditCardPaymentInstrument()
    this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
    createPaymentProfile(this.authNetHandler.getCustomerProfileID())
    this.authNetHandler.populateCreditCardFields(paymentInstrument)
    this.authNetHandler.finishCreditCardProcess()
    assertEquals("failure - Credit Card Number Incorrect", "XXXX1111", paymentInstrument.CreditCardNumber_TDIC)
  }

  function testSubmitPaymentWithoutStart() {

    try {

      this.authNetHandler.submitPayment(new BigDecimal(100), createTestCreditCardPaymentInstrument(), "Test Description")
      fail("failure - DisplayableException should be thrown for submit payment without start")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains(DisplayKey.get("TDIC.CreditCardHandler.ProcessNotStarted")))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for submit payment without start: ${e.getMessage()}")
    }
  }

  function testSubmitPaymentWithoutPopulate() {

    try {

      var paymentInstrument = createTestCreditCardPaymentInstrument()
      this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
      this.authNetHandler.submitPayment(new BigDecimal(100), createTestCreditCardPaymentInstrument(), "Test Description")
      fail("failure - DisplayableException should be thrown for submit payment without populate")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains(DisplayKey.get("TDIC.CreditCardHandler.NoCreditCardNumber")))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for submit payment without populate: ${e.getMessage()}")
    }
    finally {

      this.authNetHandler.finishCreditCardProcess()
    }
  }

  function testSubmitPaymentWithNegativeAmount() {

    try {

      var paymentInstrument = createTestCreditCardPaymentInstrument()
      this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
      createPaymentProfile(this.authNetHandler.getCustomerProfileID())
      this.authNetHandler.populateCreditCardFields(paymentInstrument)
      this.authNetHandler.submitPayment(new BigDecimal(-100), paymentInstrument, "Test Description")
      fail("failure - DisplayableException should be thrown for submit payment with negative amount")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains("amount -100 is invalid"))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for submit payment with negative amount: ${e.getMessage()}")
    }
    finally {

      this.authNetHandler.finishCreditCardProcess()
    }
  }

  function testSubmitPaymentWithZeroAmount() {

    try {

      var paymentInstrument = createTestCreditCardPaymentInstrument()
      this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
      createPaymentProfile(this.authNetHandler.getCustomerProfileID())
      this.authNetHandler.populateCreditCardFields(paymentInstrument)
      this.authNetHandler.submitPayment(new BigDecimal(0), paymentInstrument, "Test Description")
      fail("failure - DisplayableException should be thrown for submit payment with zero amount")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains("amount 0 is invalid"))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for submit payment with zero amount: ${e.getMessage()}")
    }
    finally {

      this.authNetHandler.finishCreditCardProcess()
    }
  }

  function testSubmitPaymentWithDescriptionLengthEqualToMaxPermitted() {

    var paymentInstrument = createTestCreditCardPaymentInstrument()
    this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
    createPaymentProfile(this.authNetHandler.getCustomerProfileID())
    this.authNetHandler.populateCreditCardFields(paymentInstrument)
    var authCode = this.authNetHandler.submitPayment(new BigDecimal(200), paymentInstrument, buildDescription(this.authNetHandler.getMaximumPaymentDescriptionLength()))
    this.authNetHandler.finishCreditCardProcess()
    assertNotNull("failure - submit payment should return non-null auth code", authCode)
  }

  function testSubmitPaymentWithDescriptionLongerThanMaxPermitted() {

    var description = buildDescription(this.authNetHandler.getMaximumPaymentDescriptionLength() + 1)
    try {

      var paymentInstrument = createTestCreditCardPaymentInstrument()
      this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
      createPaymentProfile(this.authNetHandler.getCustomerProfileID())
      this.authNetHandler.populateCreditCardFields(paymentInstrument)
      this.authNetHandler.submitPayment(new BigDecimal(100), paymentInstrument, description)
      fail("failure - DisplayableException should be thrown for submit payment with description longer than max permitted")
    }
    catch (ude : DisplayableException) {

      assertEquals("failure - Incorrect Exception Message: " + ude.Message, 1 as Boolean, ude.Message.contains("The length of the payment description \"" + description + "\" exceeds the maximum permitted length"))
    }
    catch (e : Exception) {

      fail("failure - ${e.Class.Name} thrown for submit payment with description longer than max permitted: ${e.getMessage()}")
    }
    finally {

      this.authNetHandler.finishCreditCardProcess()
    }
  }

  function testSubmitPaymentReturnsAuthCode() {

    var paymentInstrument = createTestCreditCardPaymentInstrument()
    this.authNetHandler.startCreditCardProcess(createTestAccount(), paymentInstrument)
    createPaymentProfile(this.authNetHandler.getCustomerProfileID())
    this.authNetHandler.populateCreditCardFields(paymentInstrument)
    var authCode = this.authNetHandler.submitPayment(new BigDecimal(100), paymentInstrument, "Test Description")
    this.authNetHandler.finishCreditCardProcess()
    assertNotNull("failure - submit payment should return non-null auth code", authCode)
  }

  function testFinishWithoutStart() {

    this.authNetHandler.finishCreditCardProcess()
  }

  private function createPaymentProfile(aCustomerProfileID : String) {

    var paymentProfile = net.authorize.data.cim.PaymentProfile.createPaymentProfile()
    paymentProfile.setCustomerType(net.authorize.data.xml.CustomerType.INDIVIDUAL)

    var billingInfo = net.authorize.data.xml.Address.createAddress()
    billingInfo.setFirstName("Joe")
    billingInfo.setLastName("Test")
    billingInfo.setCompany("CompanyA")
    billingInfo.setAddress("123 Street")
    billingInfo.setCity("AnyCity")
    billingInfo.setState("CA")
    billingInfo.setCountry("US")
    billingInfo.setZipPostalCode("90123")
    paymentProfile.setBillTo(billingInfo)

    var creditCard = net.authorize.data.creditcard.CreditCard.createCreditCard()
    creditCard.setCreditCardNumber("4111111111111111")
    creditCard.setExpirationDate("2029-07")
    creditCard.setCardCode("012")
    paymentProfile.addPayment(net.authorize.data.xml.Payment.createPayment(creditCard))

    var merchant = net.authorize.Merchant.createMerchant(net.authorize.Environment.SANDBOX, this.apiLoginID, this.transactionKey)
    var transaction = merchant.createCIMTransaction(net.authorize.cim.TransactionType.CREATE_CUSTOMER_PAYMENT_PROFILE)
    transaction.setRefId(java.lang.Long.toString(java.lang.System.currentTimeMillis()))
    transaction.setCustomerProfileId(aCustomerProfileID)
    transaction.addPaymentProfile(paymentProfile)
    transaction.setValidationMode(net.authorize.cim.ValidationModeType.TEST_MODE)

    merchant.postTransaction(transaction)
  }

  private function buildDescription(descriptionLength : int) : String {

    var sb = new java.lang.StringBuilder()
    for (i in 1..descriptionLength) {

      sb.append("a")
    }
    return sb.toString()
  }*/
}