package tdic.bc.integ.plugins.generalledger.helper

uses tdic.bc.integ.plugins.generalledger.dto.GLBucketGroup
uses tdic.bc.integ.plugins.generalledger.dto.GLFile
uses tdic.bc.integ.plugins.generalledger.dto.GLTransactionLineWritableBean
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper

uses java.lang.RuntimeException
uses java.math.BigDecimal
uses java.text.SimpleDateFormat

@gw.testharness.ServerTest

/**
 * US570 - General Ledger Integration
 * 01/15/2015 Alvin Lee
 *
 * GUnit test class for the General Ledger Integration.
 */
class TDIC_GLIntegrationHelperTest extends gw.testharness.TestBase {

  /**
   * The GL bucket group used across the unit tests.
   */
  private var _bucketGroup : GLBucketGroup

  /**
   * Creates the group of cash receipts buckets to be used across the unit tests.
   */
  override function beforeMethod() {
    _bucketGroup = new GLBucketGroup()
  }

  /**
   * Test getting the mapped T-Account name.  Note:  Requires Excel Plan Data Loader.
   */
  function testGetMappedTAccountName() {
    print("Original T-Account Name: " + GLOriginalTAccount_TDIC.TC_UNBILLED.Code)
    var mappedName = TDIC_GLIntegrationHelper.getMappedTAccountName(typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE, LedgerSide.TC_DEBIT, false, InvoiceStatus.TC_PLANNED, GLOriginalTAccount_TDIC.TC_UNBILLED.Code)
    print("Mapped T-Account Name: " + mappedName)
    assertEquals(mappedName, "1265-2230")
  }

  /**
   * Test mapping the account unit.
   */
  function testGetMappedAccountUnit() {
    var acctUnit = TDIC_GLIntegrationHelper.getMappedAccountUnit(CompanyAccountCode_TDIC.TC_BALSHEET, "WC7WorkersComp", "AK")
    print("Account Unit: " + acctUnit)
    assertEquals(acctUnit, "000.02.40")
  }

  /**
   * Negative test mapping the account unit with an invalid state code.
   */
  function testGetMappedAccountUnitNegative() {
    var exceptionThrown = false
    try {
      var acctUnit = TDIC_GLIntegrationHelper.getMappedAccountUnit(CompanyAccountCode_TDIC.TC_BALSHEET, "WC7WorkersComp", "XX")
    } catch (re : RuntimeException) {
      print("Caught expected exception: " + re.LocalizedMessage)
      exceptionThrown = true

    }
    assertTrue(exceptionThrown)
  }

  /**
   * Test processing a DisbursementPaid transaction line.
   */
  function testProcessDisbursementTransaction() {
    var disbursementUnappliedBucket = _bucketGroup.NameBucketMap.get("DisbPaid")
    assertNotNull(disbursementUnappliedBucket)
    assertEquals(disbursementUnappliedBucket.Description, "DISBURSEMENT PAID")

    TDIC_GLIntegrationHelper.processDisbursementTransaction(SampleDisbursementTransactionLine, _bucketGroup)
    print("New total amount: " + disbursementUnappliedBucket.TotalBucketAmount)
    assertTrue(disbursementUnappliedBucket.TotalBucketAmount.compareTo(new BigDecimal("25.20")) == 0)
    print("Mapped T-Account code: " + disbursementUnappliedBucket.MappedTAccountCode)
    assertEquals(disbursementUnappliedBucket.MappedTAccountCode, "2667-0000")
  }

  /**
   * Test processing a ChargeWrittenOff transaction line.
   */
  function testProcessWriteOffTransaction() {
    var writeOffDueBucket = _bucketGroup.NameBucketMap.get("WriteOffRevUnbilled")
    assertNotNull(writeOffDueBucket)
    assertEquals(writeOffDueBucket.Description, "WRITE-OFF REV")

    TDIC_GLIntegrationHelper.processWriteOffTransaction(SampleWriteOffTransactionLine, _bucketGroup)
    print("New total amount: " + writeOffDueBucket.TotalBucketAmount)
    assertTrue(writeOffDueBucket.TotalBucketAmount.compareTo(new BigDecimal("11.03")) == 0)
    print("Mapped T-Account code: " + writeOffDueBucket.MappedTAccountCode)
    assertEquals(writeOffDueBucket.MappedTAccountCode, "1265-2230")
  }

  /**
   * Test processing a AccountNegativeWriteoffTxn transaction line.
   */
  function testProcessNegativeWriteOffTransaction() {
    var negativeWriteOffBucket = _bucketGroup.NameBucketMap.get("NegWriteOff")
    assertNotNull(negativeWriteOffBucket)
    assertEquals(negativeWriteOffBucket.Description, "NEG WRITE-OFF")

    TDIC_GLIntegrationHelper.processNegativeWriteOffTransaction(SampleNegativeWriteOffTransactionLine, _bucketGroup)
    print("New total amount: " + negativeWriteOffBucket.TotalBucketAmount)
    assertTrue(negativeWriteOffBucket.TotalBucketAmount.compareTo(new BigDecimal("-0.88")) == 0)
    print("Mapped T-Account code: " + negativeWriteOffBucket.MappedTAccountCode)
    assertEquals(negativeWriteOffBucket.MappedTAccountCode, "6510-0000")
  }

  /**
   * Test processing a ChargeBilled transaction line.
   */
  function testProcessChargeBilledTransaction() {
    var chargeBilledBucket = _bucketGroup.NameBucketMap.get("ChargeBilledBilled")
    assertNotNull(chargeBilledBucket)
    assertEquals(chargeBilledBucket.Description, "CHARGE BILLED")

    TDIC_GLIntegrationHelper.processChargeBilledTransaction(SampleChargeBilledTransactionLine, _bucketGroup)
    print("New total amount: " + chargeBilledBucket.TotalBucketAmount)
    assertTrue(chargeBilledBucket.TotalBucketAmount.compareTo(new BigDecimal("500.00")) == 0)
    print("Mapped T-Account code: " + chargeBilledBucket.MappedTAccountCode)
    assertEquals(chargeBilledBucket.MappedTAccountCode, "1265-0000")
  }

  /**
   * Test processing a ChargePaidFromAccount transaction line.
   */
  function testProcessChargePaidFromAccountTransaction() {
    var chargeDueBucket = _bucketGroup.NameBucketMap.get("ChargePaidFromAccountFuture")
    assertNotNull(chargeDueBucket)
    assertEquals(chargeDueBucket.Description, "CHARGE PAID FROM ACCOUNT")

    TDIC_GLIntegrationHelper.processChargePaidFromAccountTransaction(SampleChargePaidTransactionLine, _bucketGroup)
    print("New total amount: " + chargeDueBucket.TotalBucketAmount)
    assertTrue(chargeDueBucket.TotalBucketAmount.compareTo(new BigDecimal("-137.11")) == 0)
    print("Mapped T-Account code: " + chargeDueBucket.MappedTAccountCode)
    assertEquals(chargeDueBucket.MappedTAccountCode, "2665-0000")
  }

  /**
   * Test processing a PolicyPeriodBecameEffective transaction line.
   */
  function testProcessPolicyPeriodBecameEffectiveTransaction() {
    var futureBucket = _bucketGroup.NameBucketMap.get("PolicyPeriodBecameEffOffset")
    assertNotNull(futureBucket)
    assertEquals(futureBucket.Description, "ADVANCE PREMIUM")

    TDIC_GLIntegrationHelper.processPolicyPeriodBecameEffectiveTransaction(SamplePolicyEffectiveTransactionLine, _bucketGroup)
    print("New total amount: " + futureBucket.TotalBucketAmount)
    assertTrue(futureBucket.TotalBucketAmount.compareTo(new BigDecimal("3080.00")) == 0)
    print("Mapped T-Account code: " + futureBucket.MappedTAccountCode)
    assertEquals(futureBucket.MappedTAccountCode, "2665-0000")
  }

  /**
   * Test encoding lines in the GL flat file.
   */
  function testConvertDataToFlatFileLines() {
    var disbursementUnappliedBucket = _bucketGroup.NameBucketMap.get("DisbPaid")
    assertNotNull(disbursementUnappliedBucket)
    TDIC_GLIntegrationHelper.processDisbursementTransaction(SampleDisbursementTransactionLine, _bucketGroup)
    var transLine = SampleDisbursementTransactionLine
    var sdf = new SimpleDateFormat("yyyy-MM-dd")
    var glFile = new GLFile()
    disbursementUnappliedBucket.Date = sdf.parse(transLine.TransactionDate.substring(0, 10))
    disbursementUnappliedBucket.AccountUnit = TDIC_GLIntegrationHelper.getMappedAccountUnit(CompanyAccountCode_TDIC.TC_BALSHEET, transLine.LOB, transLine.StateCode)
    disbursementUnappliedBucket.MappedTAccountCode = disbursementUnappliedBucket.MappedTAccountCode.remove('-')
    disbursementUnappliedBucket.UID = 56
    glFile.addToGLBuckets(disbursementUnappliedBucket)
    var lines = TDIC_GLIntegrationHelper.convertDataToFlatFileLines(glFile)
    assertNotNull(lines)
    assertEquals(lines.Count, 1)
    var glLine = lines.get(0)
    print("CSV-formatted Line: " + glLine)
    assertEquals(glLine, "GW-LGL-GLT,052556,5,0005000.06.40,0026670000,IG,05/25/2016,WC05251656,DISBURSEMENT PAID,USD,0,25.20,0,0,GL,,,05/25/2016,,,,0,05/25/2016,,,,,0,,,0,0,0,0,0,0")
  }

  /**
   * Gets a sample DisbursementPaid line.
   */
  @Returns("A GLTransactionLineWritableBean representing a DisbursementPaid line")
  private property get SampleDisbursementTransactionLine() : GLTransactionLineWritableBean {
    var transLine = new GLTransactionLineWritableBean()
    transLine.Amount = new BigDecimal("25.20")
    transLine.Description = "Disbursement Paid"
    transLine.GWTAccountName = "Designated Unapplied"
    transLine.LineItemType = LedgerSide.TC_DEBIT.Code
    transLine.LOB = LOBCode.TC_WC7WORKERSCOMP.Code
    transLine.MappedTAccountName = "2667-0000"
    transLine.StateCode = Jurisdiction.TC_CA.Code
    transLine.TransactionDate = "2016-05-25 23:33:40.000"
    transLine.TransactionSubtype = typekey.Transaction.TC_DISBURSEMENTPAID.Code
    return transLine
  }

  /*
   * Gets a sample ChargeWrittenOff line.
   */
  @Returns("A GLTransactionLineWritableBean representing a ChargeWrittenOff line")
  private property get SampleWriteOffTransactionLine() : GLTransactionLineWritableBean {
    var transLine = new GLTransactionLineWritableBean()
    transLine.Amount = new BigDecimal("11.03")
    transLine.Description = "Premium Written Off (Reversed)"
    transLine.GWTAccountName = "Premium Due"
    transLine.InvoiceStatus = InvoiceStatus.TC_PLANNED.Code
    transLine.LineItemType = LedgerSide.TC_DEBIT.Code
    transLine.LOB = LOBCode.TC_WC7WORKERSCOMP.Code
    transLine.MappedTAccountName = "1265-2230"
    transLine.PolicyPeriodStatus = PolicyPeriodStatus.TC_FUTURE.Code
    transLine.StateCode = Jurisdiction.TC_CA.Code
    transLine.TransactionDate = "2016-07-27 15:09:14.000"
    transLine.TransactionSubtype = typekey.Transaction.TC_CHARGEWRITTENOFF.Code
    return transLine
  }

  /*
   * Gets a sample AccountNegativeWriteoffTxn line.
   */
  @Returns("A GLTransactionLineWritableBean representing a AccountNegativeWriteoffTxn line")
  private property get SampleNegativeWriteOffTransactionLine() : GLTransactionLineWritableBean {
    var transLine = new GLTransactionLineWritableBean()
    transLine.Amount = new BigDecimal("0.88")
    transLine.Description = "Account Negative Write-Off"
    transLine.GWTAccountName = "Negative Write-Off"
    transLine.LineItemType = LedgerSide.TC_CREDIT.Code
    transLine.MappedTAccountName = "6510-0000"
    transLine.TransactionDate = "2016-07-24 23:05:05.000"
    transLine.TransactionSubtype = typekey.Transaction.TC_ACCOUNTNEGATIVEWRITEOFFTXN.Code
    return transLine
  }

  /**
   * Gets a sample ChargeBilled line.
   */
  @Returns("A GLTransactionLineWritableBean representing a ChargeBilled line")
  private property get SampleChargeBilledTransactionLine() : GLTransactionLineWritableBean {
    var transLine = new GLTransactionLineWritableBean()
    transLine.Amount = new BigDecimal("500.00")
    transLine.Description = "Premium Billed"
    transLine.GWTAccountName = "Premium Billed"
    transLine.InvoiceStatus = InvoiceStatus.TC_BILLED.Code
    transLine.LineItemType = LedgerSide.TC_DEBIT.Code
    transLine.LOB = LOBCode.TC_WC7WORKERSCOMP.Code
    transLine.MappedTAccountName = "1265-0000"
    transLine.PolicyPeriodStatus = PolicyPeriodStatus.TC_INFORCE.Code
    transLine.StateCode = Jurisdiction.TC_CA.Code
    transLine.TransactionDate = "2016-07-21 16:51:55.000"
    transLine.TransactionSubtype = typekey.Transaction.TC_CHARGEBILLED.Code
    return transLine
  }

  /**
   * Gets a sample ChargePaidFromAccount line.
   */
  @Returns("A GLTransactionLineWritableBean representing a ChargePaidFromAccount line")
  private property get SampleChargePaidTransactionLine() : GLTransactionLineWritableBean {
    var transLine = new GLTransactionLineWritableBean()
    transLine.Amount = new BigDecimal("137.11")
    transLine.Description = "Premium Paid From Account"
    transLine.GWTAccountName = "Premium Due"
    transLine.InvoiceStatus = InvoiceStatus.TC_BILLED.Code
    transLine.LineItemType = LedgerSide.TC_CREDIT.Code
    transLine.LOB = LOBCode.TC_WC7WORKERSCOMP.Code
    transLine.MappedTAccountName = "2665-0000"
    transLine.PolicyPeriodStatus = PolicyPeriodStatus.TC_FUTURE.Code
    transLine.StateCode = Jurisdiction.TC_CA.Code
    transLine.TransactionDate = "2016-07-20 17:27:40.000"
    transLine.TransactionSubtype = typekey.Transaction.TC_CHARGEPAIDFROMACCOUNT.Code
    return transLine
  }

  /**
   * Gets a sample PolicyPeriodBecameEffective line.
   */
  @Returns("A GLTransactionLineWritableBean representing a PolicyPeriodBecameEffective line")
  private property get SamplePolicyEffectiveTransactionLine() : GLTransactionLineWritableBean {
    var transLine = new GLTransactionLineWritableBean()
    transLine.Amount = new BigDecimal("3080.00")
    transLine.Description = "Policy Period Effective Date Status Change"
    transLine.GWTAccountName = "future"
    transLine.LineItemType = LedgerSide.TC_DEBIT.Code
    transLine.LOB = LOBCode.TC_WC7WORKERSCOMP.Code
    transLine.MappedTAccountName = "2665-0000"
    transLine.PolicyPeriodStatus = PolicyPeriodStatus.TC_INFORCE.Code
    transLine.StateCode = Jurisdiction.TC_CA.Code
    transLine.TransactionDate = "2016-04-22 17:21:46.000"
    transLine.TransactionSubtype = typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE.Code
    return transLine
  }

}