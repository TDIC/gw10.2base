package tdic.bc.config.enhancement

uses gw.api.databuilder.PersonBuilder


/**
 * US62
 * 09/09/2014 Vicente Ortega
 *
 * GUnit test for Contact Enhancement.
 */
@gw.testharness.ServerTest
@gw.testharness.RunLevel(MULTIUSER)
class ContactEnhancementTest extends gw.testharness.TestBase{

  private static var _contact : Person

  override function beforeMethod() {
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var personBuilder = new PersonBuilder()
      _contact = personBuilder.create()
    }, "su")
  }

  /**
   * Test if ADANumberOfficialID_TDIC sets the ADA Number correctly
   */
  function testADANumberOfficialID_TDICMethod() {
    print("Testing ADANumberOfficialID_TDIC")
    _contact.ADANumberOfficialID_TDIC = "111111"

    assertEquals(_contact.ADANumberOfficialID_TDIC, "111111")
    assertEquals(_contact.OfficialIDs.firstWhere( \ a -> a.OfficialIDType == OfficialIDType.TC_ADANUMBER_TDIC).OfficialIDValue, "111111")
  }

}