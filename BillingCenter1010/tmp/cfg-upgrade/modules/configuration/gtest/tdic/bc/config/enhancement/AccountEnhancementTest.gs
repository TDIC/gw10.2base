package tdic.bc.config.enhancement
uses gw.api.databuilder.AccountBuilder
uses gw.api.databuilder.PaymentInstrumentBuilder
uses gw.api.databuilder.PersonBuilder
uses gw.api.databuilder.PolicyPeriodBuilder
uses gw.api.databuilder.PolicyBuilder
uses gw.api.util.DateUtil

/**
 * GUnit test for Account Enhancement.
 * This test has no dependencies, and does not require the server to be at any run level.
 */
@gw.testharness.ServerTest
@gw.testharness.RunLevel(MULTIUSER)
class AccountEnhancementTest extends gw.testharness.TestBase {

    private static var _account : Account

    override function beforeMethod() {
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        //Account is created
        var accountBuilder = new AccountBuilder()
        _account = accountBuilder.createAndCommit()

      }, "su")
   }

  /**
   * US62
   * 18/09/2014 Vicente
   *
   * The method is used to create an Account with a Policy with a contact with the input ADA Number
   */
  @Param("ADANumber", "This is the ADA Nmber set to the Contact in the Policy of the new Account created")
  @Returns("A new account with a Policy with a contact with the input ADA Number")
  function createAccountWithPolicyWithContactADANumber(ADANumber : String) : Account {
    var account : Account
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {

      var accountBuilder = new AccountBuilder()
      account = accountBuilder.createAndCommit()

      var person = new PersonBuilder()
          .withFirstName( "Michael" )
          .withLastName( "Collins")
          .create()

      var policyPeriodBuilder = new PolicyPeriodBuilder()
          .withCurrency(account.Currency)
          .withEffectiveDate(DateUtil.currentDate())
          .withContact(person)

      var policy = new PolicyBuilder()
          .onAccount(account)
          .withPolicyPeriod(policyPeriodBuilder)
          .createAndCommit()

      var contact = policy.PolicyPeriods.first().Contacts.first().Contact
      contact.ADANumberOfficialID_TDIC = ADANumber

    }, "su")

    return account
  }


  /**
   * Test if HasACHPaymentInstrument returns True when the Account has a Payment Instrument with an ACH/EFT Payment Method
   */
  function testHasACHPaymentInstrumentWhenACHPaymentMethod() {
    print("Testing testHasACHPaymentInstrumentWhenACHPaymentMethod")

    var pInstrument = new PaymentInstrumentBuilder()
        .withPaymentMethod(PaymentMethod.TC_ACH)
        .create()
    pInstrument.Account = _account

    assertEquals(_account.HasACHPaymentInstrument_TDIC, true)
  }

  /**
   * Test if HasACHPaymentInstrument returns False when the Account has not a Payment Instrument with an ACH/EFT Payment Method
   */
  function testHasACHPaymentInstrumentWhenNonACHPaymentMethod() {
    print("Testing testHasACHPaymentInstrumentWhenNonACHPaymentMethod")
    var pInstrument = new PaymentInstrumentBuilder()
        .withPaymentMethod(PaymentMethod.TC_RESPONSIVE)
        .create()
    pInstrument.Account = _account

    assertEquals(_account.HasACHPaymentInstrument_TDIC, false)
  }

  /**
   * Test if HasACHPaymentInstrument returns False when the Account has no Payment Instruments at all
   */
  function testHasACHPaymentInstrumentWhenNonPaymentInstruments() {
    print("Testing testHasACHPaymentInstrumentWhenNonACHPaymentMethod")

    assertEquals(_account.HasACHPaymentInstrument_TDIC, false)
  }


  /**
   * Test if AssociatedAccounts() returns one Account with a contact with same ADA Number as his Primary Insured contact
   */
  function testAssociatedAccountsEqualADANumber() {
    print("Testing testCompanyTaxIDWhenFEINnotPopulated")

    var ADANumber = "33333"
    var account1 = createAccountWithPolicyWithContactADANumber(ADANumber)
    var account2 = createAccountWithPolicyWithContactADANumber(ADANumber)

    assertTrue(account2.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Account == account1))
    //assertTrue(account2.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Contact.ADANumber_TDICOfficialID == ADANumber))

  }

  /**
   * Test if AssociatedAccounts() does not return one Account with a contact with different ADA Number as his
   * Primary Insured contact
   */
  function testAssociatedAccountsNonEqualADANumber() {
    print("Testing testCompanyTaxIDWhenFEINnotPopulated")

    var ADANumberAccount1 = "33333"
    var ADANumberAccount2 = "99999"
    var account1 = createAccountWithPolicyWithContactADANumber(ADANumberAccount1)
    var account2 = createAccountWithPolicyWithContactADANumber(ADANumberAccount2)

    assertFalse(account2.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Account == account1))
    //assertFalse(account2.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Contact.ADANumber_TDICOfficialID == ADANumberAccount1))

  }

  /**
   * Test if AssociatedAccounts() returns several Accounts with a contact with same ADA Number as his Primary Insured contact
   */
  function testAssociatedAccountsSeveralAccountsEqualADANumber() {
    print("Testing testAssociatedAccountsSeveralAccountsEqualADANumber")

    var ADANumber = "33333"
    var account1 = createAccountWithPolicyWithContactADANumber(ADANumber)
    var account2 = createAccountWithPolicyWithContactADANumber(ADANumber)
    var account3 = createAccountWithPolicyWithContactADANumber(ADANumber)
    var account4 = createAccountWithPolicyWithContactADANumber(ADANumber)

    assertTrue(account4.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Account == account1))
    //assertTrue(account4.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Contact.ADANumber_TDICOfficialID == ADANumber))
    assertTrue(account4.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Account == account2))
    //assertTrue(account4.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Contact.ADANumber_TDICOfficialID == ADANumber))
    assertTrue(account4.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Account == account3))
    //assertTrue(account4.AssociatedAccounts_TDIC.hasMatch( \ relatedAcct -> relatedAcct.Contact.ADANumber_TDICOfficialID == ADANumber))
  }

  /**
   * Test if updateAccountPaymentInstruments remove all the ACH payment methods when the default payment instrument is
   * responsive
   */
  function testUpdateAccountPaymentInstrumentsWithResponsiveDefaultPaymentInstrument() {
    print("Testing testUpdateAccountPaymentInstrumentsWithResponsiveDefaultPaymentInstrument")

    gw.transaction.Transaction.runWithNewBundle( \ bundle -> {
      _account = bundle.add(_account)

      var pInstrumentACH = new PaymentInstrumentBuilder()
          .withPaymentMethod(PaymentMethod.TC_ACH)
          .create()
      pInstrumentACH.Account = _account
      _account.DefaultPaymentInstrument = pInstrumentACH

      _account.DefaultPaymentInstrument = _account.PaymentInstruments.firstWhere( \ pInstrument -> pInstrument.PaymentMethod == PaymentMethod.TC_RESPONSIVE)

      print("Account Payment Instruments: ")
      for (pInstrument in _account.PaymentInstruments) {
        print("  " + pInstrument)
      }

      _account.updateAccountPaymentInstruments()

      assertTrue(_account.PaymentInstruments.contains(pInstrumentACH))
      assertTrue(_account.PaymentInstruments.firstWhere( \ pInstrument -> pInstrument.PaymentMethod == PaymentMethod.TC_ACH).Invalidated_TDIC)
    }, "su")
  }

  /**
   * Test if updateAccountPaymentInstruments remove all the ACH payment methods except the default payment instrument it is
   * ACH
   */
  function testUpdateAccountPaymentInstrumentsWithACHDefaultPaymentInstrument() {
    print("Testing testUpdateAccountPaymentInstrumentsWithACHDefaultPaymentInstrument")

    gw.transaction.Transaction.runWithNewBundle( \ bundle -> {
      var pInstrumentACH1 = new PaymentInstrumentBuilder()
          .withPaymentMethod(PaymentMethod.TC_ACH).withToken("1")
          .create()
      pInstrumentACH1.Account = _account

      _account = bundle.add(_account)
      _account.DefaultPaymentInstrument = pInstrumentACH1

      var pInstrumentACH2 = new PaymentInstrumentBuilder()
          .withPaymentMethod(PaymentMethod.TC_ACH).withToken("2")
          .create()
      pInstrumentACH2.Account = _account
      _account.DefaultPaymentInstrument = pInstrumentACH2

      _account.updateAccountPaymentInstruments()

      print("Account Payment Instruments: ")
      for (pInstrument in _account.PaymentInstruments) {
        print("  " + pInstrument + " | Token: " + pInstrument.Token + " | Invalidated: " + pInstrument.Invalidated_TDIC)
      }

      assertTrue(_account.PaymentInstruments.contains(pInstrumentACH1))
      assertTrue(_account.PaymentInstruments.contains(pInstrumentACH2))
      assertTrue(_account.PaymentInstruments.firstWhere( \ pInstrument -> pInstrument.Token == "1").Invalidated_TDIC)
      assertFalse(_account.PaymentInstruments.firstWhere( \ pInstrument -> pInstrument.Token == "2").Invalidated_TDIC)
    }, "su")
  }
}