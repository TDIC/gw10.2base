package tdic.plugin.invoice.impl

uses gw.pl.currency.MonetaryAmount
uses gw.api.database.Query

uses gw.invoice.InvoiceItemFilter
uses gw.api.domain.invoice.PaymentPlanChanger

uses java.util.Date
uses java.math.RoundingMode
uses java.math.BigDecimal

/**
 * GUnit test for PaymentPlan Plugin.
 */
@gw.testharness.ServerTest
@gw.testharness.RunLevel(MULTIUSER)
class PaymentPlanTest extends tdic.TDICTestBase{
  private static var _account : Account
  private static var _apwPlan : PaymentPlan
  private static var _annualPlan : PaymentPlan
  private static var _semiAnnualPlan : PaymentPlan
  private static var _achPaymentInstrument : PaymentInstrument

  /**
   * Initialize an account with TDIC Billing Plan and the correct day of month (in a normal account creation the day of month is set by a pre-update rule)
   */
  override function beforeMethod() {
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      _apwPlan = gw.api.database.Query.make(PaymentPlan).compare(Plan#Name, Equals, "TDIC Monthly APW").select().first()
      _annualPlan = gw.api.database.Query.make(PaymentPlan).compare(Plan#Name, Equals, "TDIC Annual").select().first()
      _semiAnnualPlan = gw.api.database.Query.make(PaymentPlan).compare(Plan#Name, Equals, "TDIC Semi-Annual").select().first()
      _achPaymentInstrument = createTestACHPaymentInstrument()

      var tdicBillingPlan = Query.make(entity.BillingPlan).compare(BillingPlan#Name, Equals, "TDIC Billing Plan").select().FirstResult
      _account = createAccount(tdicBillingPlan, Date.CurrentDate.addDays(-tdicBillingPlan.PaymentDueInterval).addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment).DayOfMonth)
      _account.BillDateOrDueDateBilling = typekey.BillDateOrDueDateBilling.TC_DUEDATEBILLING
    }, "su")
  }

  private function createPolicyPeriodWith1100ChargeAndSemiAnnualPlan(effectiveDate: Date, expirationDate: Date): PolicyPeriod {
    return createPolicyPeriod(effectiveDate, expirationDate, new MonetaryAmount(1100,_account.Currency), _account, _semiAnnualPlan)
  }

  private function createPolicyPeriodWith900ChargeAndSemiAnnualPlan(effectiveDate: Date, expirationDate: Date): PolicyPeriod {
    return createPolicyPeriod(effectiveDate, expirationDate, new MonetaryAmount(900,_account.Currency), _account, _semiAnnualPlan)
  }

  /**
   * Test if a Semi-Annual policy with effective date four year in the future is changed correctly to Annual. It should have
   * one invoice with billed invoice date 22 days before the effective date and due date 1 day before.
   */
  function testFutureSemiAnnualPolicyPeriodChangeToAnnual() {
    print("Testing future Semi-Annual policy with effective date 4 years in the future, changing payment plan to Annual...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addYears(4)
      var expirationDate = Date.CurrentDate.addYears(5)

      var policyPeriod = createPolicyPeriodWith1100ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)

      //Payment Plan change to Annual
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      policyPeriod.NewPaymentPlan_TDIC = _annualPlan
      var changer = new PaymentPlanChanger(policyPeriod, _annualPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()

      var invoices = policyPeriod.Invoices.orderBy( \ invoice -> invoice.EventDate)

      assertEquals(_annualPlan,  policyPeriod.NewPaymentPlan_TDIC)
      assertEquals(1, invoices.Count)
      assertEquals(new MonetaryAmount(1100.00,_account.Currency), invoices.first().Amount)
      assertEquals(0, (effectiveDate.addDays(-_account.BillingPlan.PaymentDueInterval).addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment)).compareIgnoreTime(invoices.first().EventDate))
      assertEquals(0, (effectiveDate.addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment)).compareIgnoreTime(invoices.first().PaymentDueDate))
    }, "su")
    print("Passed!")
  }

  /**
   * Test if a Semi-Annual policy with effective date two years before the current date is changed correctly to Annual.
   * It should have one invoice with billed invoice date equals to the current date.
   */
  function testTwoYearBackDateSemiAnnualPolicyPeriodChangeToAnnual() {
    print("Testing backdated Semi-Annual policy with effective date 2 years in the past, changing payment plan to Annual...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addYears(-2)
      var expirationDate = Date.CurrentDate.addYears(-1)

      var policyPeriod = createPolicyPeriodWith1100ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)

      //Payment Plan change to Annual
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      policyPeriod.NewPaymentPlan_TDIC = _annualPlan
      var changer = new PaymentPlanChanger(policyPeriod, _annualPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()

      var invoices = policyPeriod.Invoices.orderBy( \ invoice -> invoice.EventDate)

      assertEquals(_annualPlan,  policyPeriod.NewPaymentPlan_TDIC)
      assertEquals(1, invoices.Count)
      assertEquals(new MonetaryAmount(1100.00,_account.Currency), invoices.first().Amount)
      assertEquals(0, (Date.CurrentDate).compareIgnoreTime(invoices.first().EventDate))
      assertEquals(0, (Date.CurrentDate.addDays(_account.BillingPlan.PaymentDueInterval)).compareIgnoreTime(invoices.first().PaymentDueDate))
    }, "su")
    print("Passed!")
  }


  /**
   * Test if a Semi-Annual policy with effective date five months before the current date is changed correctly to Annual.
   * It should have one invoice with billed invoice date equals to the current date.
   */
  function testFiveMonthsBackDateSemiAnnualPolicyPeriodChangeToAnnual() {
    print("Testing backdated Semi-Annual policy with effective date 5 months in the past, changing payment plan to Annual...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addMonths(-5)
      var expirationDate = effectiveDate.addYears(1)

      var policyPeriod = createPolicyPeriodWith1100ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)

      //Payment Plan change to Annual
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      policyPeriod.NewPaymentPlan_TDIC = _annualPlan
      var changer = new PaymentPlanChanger(policyPeriod, _annualPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()

      var invoices = policyPeriod.Invoices.orderBy( \ invoice -> invoice.EventDate)

      assertEquals(_annualPlan,  policyPeriod.NewPaymentPlan_TDIC)
      assertEquals(1, invoices.Count)
      assertEquals(new MonetaryAmount(1100.00,_account.Currency), invoices.first().Amount)
      assertEquals(0, (Date.CurrentDate).compareIgnoreTime(invoices.first().EventDate))
      assertEquals(0, (Date.CurrentDate.addDays(_account.BillingPlan.PaymentDueInterval)).compareIgnoreTime(invoices.first().PaymentDueDate))
    }, "su")
    print("Passed!")
  }

  /**
   * Test if a Semi-Annual policy with effective date ten months before the current date is changed correctly to Annual.
   * It should have one invoice with billed invoice date equals to the current date.
   */
  function testTenMonthsBackDateSemiAnnualPolicyPeriodChangeToAnnual() {
    print("Testing backdated Semi-Annual policy with effective date 10 months in the past, changing payment plan to Annual...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addMonths(-10)
      var expirationDate = effectiveDate.addYears(1)

      var policyPeriod = createPolicyPeriodWith1100ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)

      //Payment Plan change to Annual
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      policyPeriod.NewPaymentPlan_TDIC = _annualPlan
      var changer = new PaymentPlanChanger(policyPeriod, _annualPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()

      var invoices = policyPeriod.Invoices.orderBy( \ invoice -> invoice.EventDate)

      assertEquals(_annualPlan,  policyPeriod.NewPaymentPlan_TDIC)
      assertEquals(1, invoices.Count)
      assertEquals(new MonetaryAmount(1100.00,_account.Currency), invoices.first().Amount)
      assertEquals(0, (Date.CurrentDate).compareIgnoreTime(invoices.first().EventDate))
      assertEquals(0, (Date.CurrentDate.addDays(_account.BillingPlan.PaymentDueInterval)).compareIgnoreTime(invoices.first().PaymentDueDate))
    }, "su")
    print("Passed!")
  }

  /**
   * Test if a Semi-Annual policy with effective date four year in the future is changed correctly to APW. It should have
   * 10 invoices (because the last two months there are no invoices) with billed invoice date the day 22 of each
   * month and due date 14 days after.
   */
  function testFutureSemiAnnualPolicyPeriodChangeToAPW() {
    print("Testing future Semi-Annual policy with effective date 4 years in the future, changing payment plan to APW...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addYears(4)
      var expirationDate = effectiveDate.addYears(1)

      var policyPeriod = createPolicyPeriodWith900ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)

      _account.InvoiceDayOfMonth = 22;
      _account.DefaultPaymentInstrument = _achPaymentInstrument

      //Payment Plan change to Annual
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      policyPeriod.NewPaymentPlan_TDIC = _apwPlan
      var changer = new PaymentPlanChanger(policyPeriod, _apwPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()

      var invoices = policyPeriod.Invoices.orderBy( \ invoice -> invoice.EventDate)

      assertEquals(_apwPlan,  policyPeriod.NewPaymentPlan_TDIC)
      for(var i in 0..invoices.Count-1){
        assertTrue(new MonetaryAmount(900.00,_account.Currency).divide(invoices.Count, RoundingMode.HALF_UP).subtract(invoices[i].Amount) == new MonetaryAmount(BigDecimal.ZERO, _account.Currency)
            || new MonetaryAmount(900.00,_account.Currency).divide(invoices.Count, RoundingMode.HALF_UP).subtract(invoices[i].Amount) == new MonetaryAmount(new BigDecimal("0.01"), _account.Currency))
        assertEquals(0, (invoices[i].EventDate.addDays(_account.BillingPlan.NonResponsivePmntDueInterval).compareIgnoreTime(invoices[i].PaymentDueDate)))
      }
    }, "su")
    print("Passed!")
  }

  /**
   * Test if a Semi-Annual policy with effective date two years before the current date is changed correctly to APW.
   * It should have one invoice with billed invoice date the day 22 of the current month and due date 14 days after.
   */
  function testTwoYearsBackDateSemiAnnualPolicyPeriodChangeToAPW() {
    print("Testing backdated Semi-Annual policy with effective date 2 years in the past, changing payment plan to APW...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addYears(-2)
      var expirationDate = Date.CurrentDate.addYears(-1)

      var policyPeriod = createPolicyPeriodWith1100ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)

      _account.InvoiceDayOfMonth = 22;
      _account.DefaultPaymentInstrument = _achPaymentInstrument

      //Payment Plan change to APW
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      policyPeriod.NewPaymentPlan_TDIC = _apwPlan
      var changer = new PaymentPlanChanger(policyPeriod, _apwPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()

      var invoices = policyPeriod.Invoices.orderBy( \ invoice -> invoice.EventDate)

      var firstInvoiceDate : Date
      if(Date.CurrentDate.DayOfMonth > 8){
        firstInvoiceDate = new Date(Date.CurrentDate.YearOfDate - 1900, Date.CurrentDate.MonthOfYear, 8, 0, 0, 0)
      } else {
        firstInvoiceDate = new Date(Date.CurrentDate.YearOfDate - 1900, Date.CurrentDate.MonthOfYear -1, 8, 0, 0, 0)
      }

      assertEquals(_apwPlan,  policyPeriod.NewPaymentPlan_TDIC)
      assertEquals(1, invoices.Count)
      assertEquals(new MonetaryAmount(1100.00,_account.Currency), invoices.first().Amount)
      assertEquals(0, firstInvoiceDate.compareIgnoreTime(invoices[0].EventDate))
      assertEquals(0, (firstInvoiceDate.addDays(_account.BillingPlan.NonResponsivePmntDueInterval).compareIgnoreTime(invoices[0].PaymentDueDate)))
    }, "su")
    print("Passed!")
  }


  /**
   * Test if a Semi-Annual policy with effective date five months before the current date is changed correctly to APW.
   * It should have invoices (because the last two months there are no invoices) with billed invoice date the day 22 of each
   * month and due date 14 days after.
   */
  function testFiveMonthsBackDateSemiAnnualPolicyPeriodChangeToAPW() {
    print("Testing backdated Semi-Annual policy with effective date 5 months in the past, changing payment plan to APW...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addMonths(-5)
      var expirationDate = effectiveDate.addYears(1)

      var policyPeriod = createPolicyPeriodWith1100ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)

      _account.InvoiceDayOfMonth = 22;
      _account.DefaultPaymentInstrument = _achPaymentInstrument

      //Payment Plan change to APW
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      policyPeriod.NewPaymentPlan_TDIC = _apwPlan
      var changer = new PaymentPlanChanger(policyPeriod, _apwPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()

      var invoices = policyPeriod.Invoices.orderBy( \ invoice -> invoice.EventDate)

      assertEquals(_apwPlan,  policyPeriod.NewPaymentPlan_TDIC)
      for(var i in 0..invoices.Count-1){
        assertTrue(new MonetaryAmount(1100.00,_account.Currency).divide(invoices.Count, RoundingMode.HALF_UP).subtract(invoices[i].Amount) == new MonetaryAmount(BigDecimal.ZERO, _account.Currency)
            || new MonetaryAmount(1100.00,_account.Currency).divide(invoices.Count, RoundingMode.HALF_UP).subtract(invoices[i].Amount) == new MonetaryAmount(new BigDecimal("0.01"), _account.Currency))
        assertEquals(0, (invoices[i].EventDate.addDays(_account.BillingPlan.NonResponsivePmntDueInterval).compareIgnoreTime(invoices[i].PaymentDueDate)))
      }
    }, "su")
    print("Passed!")
  }

  /**
   * Test if a Semi-Annual policy with effective date ten months before the current date is changed correctly to APW.
   * It should have one invoice with billed invoice date the day 22 of the current month and due date 14 days after.
   */
  function testTenMonthsBackDateSemiAnnualPolicyPeriodChangeToAPW() {
    print("Testing backdated Semi-Annual policy with effective date 10 months in the past, changing payment plan to APW...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addMonths(-10)
      var expirationDate = effectiveDate.addYears(1)

      var policyPeriod = createPolicyPeriodWith1100ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)

      _account.InvoiceDayOfMonth = 22;
      _account.DefaultPaymentInstrument = _achPaymentInstrument

      //Payment Plan change to APW
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      policyPeriod.NewPaymentPlan_TDIC = _apwPlan
      var changer = new PaymentPlanChanger(policyPeriod, _apwPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
      changer.execute()

      var invoices = policyPeriod.Invoices.orderBy( \ invoice -> invoice.EventDate)

      var firstInvoiceDate : Date
      if(Date.CurrentDate.DayOfMonth > 8){
        firstInvoiceDate = new Date(Date.CurrentDate.YearOfDate - 1900, Date.CurrentDate.MonthOfYear, 8, 0, 0, 0)
      } else {
        firstInvoiceDate = new Date(Date.CurrentDate.YearOfDate - 1900, Date.CurrentDate.MonthOfYear -1, 8, 0, 0, 0)
      }

      assertEquals(_apwPlan,  policyPeriod.NewPaymentPlan_TDIC)
      assertEquals(1, invoices.Count)
      assertEquals(new MonetaryAmount(1100.00,_account.Currency), invoices.first().Amount)
      assertEquals(0, firstInvoiceDate.compareIgnoreTime(invoices[0].EventDate))
      assertEquals(0, (firstInvoiceDate.addDays(_account.BillingPlan.NonResponsivePmntDueInterval).compareIgnoreTime(invoices[0].PaymentDueDate)))
    }, "su")
    print("Passed!")
  }

}