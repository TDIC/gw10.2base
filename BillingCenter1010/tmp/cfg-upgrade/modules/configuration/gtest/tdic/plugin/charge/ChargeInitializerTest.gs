package tdic.plugin.charge

uses gw.pl.currency.MonetaryAmount
uses gw.api.database.Query

uses java.util.Date

/**
 * GUnit test for ChargeInitializer Plugin.
 */
@gw.testharness.ServerTest
@gw.testharness.RunLevel(MULTIUSER)
class ChargeInitializerTest extends tdic.TDICTestBase{
  private static var _account : Account
  private static var _semiAnnualPlan : PaymentPlan

  /**
   * Initialize an account with TDIC Billing Plan and the correct day of month (in a normal account creation the day of month is set by a pre-update rule)
   */
  override function beforeMethod() {
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      _semiAnnualPlan = gw.api.database.Query.make(PaymentPlan).compare(Plan#Name, Equals, "TDIC Semi-Annual").select().first()

      var tdicBillingPlan = Query.make(entity.BillingPlan).compare(BillingPlan#Name, Equals, "TDIC Billing Plan").select().FirstResult
      _account = createAccount(tdicBillingPlan, Date.CurrentDate.addDays(-tdicBillingPlan.PaymentDueInterval).addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment).DayOfMonth)
    }, "su")
  }

  private function createPolicyPeriodWith1200ChargeAndSemiAnnualPlan(effectiveDate: Date, expirationDate: Date): PolicyPeriod {
    return createPolicyPeriod(effectiveDate, expirationDate, new MonetaryAmount(1200,_account.Currency), _account, _semiAnnualPlan)
  }

  /**
   * Test if a Semi-Annual policy with effective date four year in the future is created correctly. It should have two invoices,
   * the first billed invoice date 21 days before the effective date and the second invoice billed date 21 days before 6 months after the
   * effective date.
   */
  function testFutureSemiAnnualPolicyPeriod() {
    print("Testing future Semi-Annual policy with effective date 4 years in the future...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addYears(4)
      var expirationDate = Date.CurrentDate.addYears(5)

      var policyPeriod = createPolicyPeriodWith1200ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)
      var invoices = policyPeriod.Account.NonBlankInvoicesSortedByDate.where( \ invoice -> invoice.Policies.hasMatch( \ p -> p == policyPeriod)).orderBy( \ invoice -> invoice.EventDate)

      assertEquals(2, invoices.Count)
      assertEquals(new MonetaryAmount(600.00,_account.Currency), invoices.first().Amount)
      assertEquals(new MonetaryAmount(600.00,_account.Currency), invoices.last().Amount)

      assertEquals(0, (effectiveDate.addDays(-_account.BillingPlan.PaymentDueInterval).addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment)).compareIgnoreTime(invoices.first().EventDate))
      assertEquals(0, (effectiveDate.addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment)).compareIgnoreTime(invoices.first().PaymentDueDate))
      assertEquals(0, (effectiveDate.addMonths(6).addDays(-_account.BillingPlan.PaymentDueInterval).addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment)).compareIgnoreTime(invoices.last().EventDate))
      assertEquals(0, (effectiveDate.addMonths(6).addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment)).compareIgnoreTime(invoices.last().PaymentDueDate))
    }, "su")
    print("Passed!")
  }

  /**
   * Test if a Semi-Annual policy with effective date two years before correctly. It should have one invoice with billed
   * date equals to the current date.
   */
  function testFourYerBackDateSemiAnnualPolicyPeriod() {
    print("Testing backdated Semi-Annual policy with effective date 2 years in the past...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addYears(-2)
      var expirationDate = Date.CurrentDate.addYears(-1)

      var policyPeriod = createPolicyPeriodWith1200ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)
      var invoices = policyPeriod.Account.NonBlankInvoicesSortedByDate.where( \ invoice -> invoice.Policies.hasMatch( \ p -> p == policyPeriod)).orderBy( \ invoice -> invoice.EventDate)

      assertEquals(1, invoices.Count)
      assertEquals(new MonetaryAmount(1200.00,_account.Currency), invoices.first().Amount)
      assertEquals(0, (Date.CurrentDate).compareIgnoreTime(invoices.first().EventDate))
      assertEquals(0, (Date.CurrentDate.addDays(_account.BillingPlan.PaymentDueInterval)).compareIgnoreTime(invoices.first().PaymentDueDate))
    }, "su")
    print("Passed!")
  }


  /**
   * Test if a Semi-Annual policy with effective date five months before correctly. It should have two charges the first billed
   * invoice date the current date and the second invoice billed date 21 days before 6 months after the
   * effective date.
   */
  function testFiveMonthsBackDateSemiAnnualPolicyPeriod() {
    print("Testing backdated Semi-Annual policy with effective date 5 months in the past...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addMonths(-5)
      var expirationDate = effectiveDate.addYears(1)

      var policyPeriod = createPolicyPeriodWith1200ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)
      var invoices = policyPeriod.Account.NonBlankInvoicesSortedByDate.where( \ invoice -> invoice.Policies.hasMatch( \ p -> p == policyPeriod)).orderBy( \ invoice -> invoice.EventDate)

      assertEquals(2, invoices.Count)
      assertEquals(new MonetaryAmount(600.00,_account.Currency), invoices.first().Amount)
      assertEquals(new MonetaryAmount(600.00,_account.Currency), invoices.last().Amount)
      assertEquals(0, (Date.CurrentDate).compareIgnoreTime(invoices.first().EventDate))
      assertEquals(0, (Date.CurrentDate.addDays(_account.BillingPlan.PaymentDueInterval)).compareIgnoreTime(invoices.first().PaymentDueDate))
      assertEquals(0, (effectiveDate.addMonths(6).addDays(-_account.BillingPlan.PaymentDueInterval).addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment)).compareIgnoreTime(invoices.last().EventDate))
      assertEquals(0, (effectiveDate.addMonths(6).addDays(_semiAnnualPlan.DaysFromReferenceDateToFirstInstallment)).compareIgnoreTime(invoices.last().PaymentDueDate))
    }, "su")
    print("Passed!")
  }

  /**
   * Test if a Semi-Annual policy with effective date ten months before is created correctly. It should have one invoice with billed
   * date equals to the current date.
   */
  function testTenMonthsBackDateSemiAnnualPolicyPeriod() {
    print("Testing backdated Semi-Annual policy with effective date 10 months in the past...")
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var effectiveDate = Date.CurrentDate.addMonths(-10)
      var expirationDate = effectiveDate.addYears(1)

      var policyPeriod = createPolicyPeriodWith1200ChargeAndSemiAnnualPlan(effectiveDate, expirationDate)
      var invoices = policyPeriod.Account.NonBlankInvoicesSortedByDate.where( \ invoice -> invoice.Policies.hasMatch( \ p -> p == policyPeriod)).orderBy( \ invoice -> invoice.EventDate)

      assertEquals(1, invoices.Count)
      assertEquals(new MonetaryAmount(1200.00,_account.Currency), invoices.first().Amount)
      assertEquals(0, (Date.CurrentDate).compareIgnoreTime(invoices.first().EventDate))
      assertEquals(0, (Date.CurrentDate.addDays(_account.BillingPlan.PaymentDueInterval)).compareIgnoreTime(invoices.first().PaymentDueDate))
    }, "su")
    print("Passed!")
  }
}