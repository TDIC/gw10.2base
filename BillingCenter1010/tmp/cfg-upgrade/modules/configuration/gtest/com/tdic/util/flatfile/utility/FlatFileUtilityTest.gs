package com.tdic.util.flatfile.utility

uses java.lang.Exception
uses java.lang.IllegalArgumentException
uses java.io.FileNotFoundException
uses com.tdic.util.flatfile.utility.FlatFileUtility

@gw.testharness.ServerTest
class FlatFileUtilityTest extends tdic.TDICTestBase {
  static final var VENDOR_DEFINITION_XML = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\VendorDef.xml"
  static final var NONEXIST_VENDOR_DEFINITION = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\NoVendorDef.xml"
  static final var VENDOR_DEFINITION_XLSX = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\vendorDef.xlsx"
  static final var NON_EXCEL_FILE = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\EncodedFile.txt"
  var flatFileUtil: FlatFileUtility
  override function beforeMethod() {
    flatFileUtil = new FlatFileUtility()
  }

  /**
   * Tests the readVendorSpec method functions as expected for XML Documents
   */
  function testReadVendorSpecXmlSuccessfulCase() {
    try {
      var returnedFlatFile = flatFileUtil.readVendorSpec(VENDOR_DEFINITION_XML)
      assertNotNull(returnedFlatFile)
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception Thrown")
    }
  }

  /**
   * Tests the readVendorSpec method functions as expected for XLSX Documents
   */
  function testReadVendorSpecXlsxSuccessfulCase() {
    try {
      var returnedFlatFile = flatFileUtil.readVendorSpec(VENDOR_DEFINITION_XLSX)
      assertNotNull(returnedFlatFile)
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception Thrown")
    }
  }

  /**
   * Tests the readVendorSpec method functions as expected
   */
  function testReadVendorSpecUnrecognisedFormat() {
    try {
      var returnedFlatFile = flatFileUtil.readVendorSpec(NON_EXCEL_FILE)
      fail("Failure - Expected Exception not thrown")
    } catch (var e: IllegalArgumentException) {
      //Passed
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception Thrown")
    }
  }

  /**
   * Tests that an IllegalArgumentException is thrown when a null argument is provided to the readVendorSpec method.
   */
  function testReadVendorSpecNullVendorDefinition() {
    try {
      var returnedFlatFile = flatFileUtil.readVendorSpec(null)
      fail("Failure - Expected Exception not thrown")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception Thrown")
    }
  }

  /**
   * Tests that an IllegalArgumentException is thrown when a path to a non existent vendor definition is provided to the readVendorSpec method.
   */
  function testReadVendorSpecNonExistVendorDefinition() {
    try {
      var returnedFlatFile = flatFileUtil.readVendorSpec(NONEXIST_VENDOR_DEFINITION)
      fail("Failure - Expected Exception not thrown")
    } catch (var e: FileNotFoundException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception Thrown")
    }
  }
}