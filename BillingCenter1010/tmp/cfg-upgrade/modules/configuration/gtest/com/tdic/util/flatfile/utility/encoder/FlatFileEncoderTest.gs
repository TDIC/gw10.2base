package com.tdic.util.flatfile.utility.encoder

uses com.tdic.util.flatfile.model.gx.recordmodel.anonymous.elements.Record_Fields_Entry
uses com.tdic.util.flatfile.utility.FlatFileUtility
uses com.tdic.util.flatfile.utility.parser.FlatFileParser
uses com.tdic.util.flatfile.utility.encoder.FlatFileEncoder

uses java.lang.Exception
uses java.lang.IllegalArgumentException
uses java.io.File


@gw.testharness.ServerTest
class FlatFileEncoderTest extends tdic.TDICTestBase {
  static final var VENDOR_DEFINITION = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\VendorDef.xml"
  static final var NONEXIST_VENDOR_DEFINITION =
      ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\NoVendorDef.xml"
  static final var FILE_PATH = ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\EncodedFile.txt"
  static final var ENCODED_OUTPUT_PATH =
      ".\\modules\\configuration\\gtest\\tdic\\util\\flatfile\\files\\EncodedOutputFile.txt"
  var flatFileUtility: FlatFileUtility
  /**
   * Initialise the flatFileUtility variable for each method call
   */
  override function beforeMethod() {
    flatFileUtility = new FlatFileUtility()
  }

  /**
   * US688
   * 09/11/2014 shanem
   *
   * Tests that the encode function behaves appropriately
   */
  function testEncodeSuccessfulCase() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      assertNotNull("Null returned from encode method", encodedFile)
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests file that an Illegal Argument Exception is thrown when null xml provided
   */
  public function testEncodeNullXML() {
    try {
      var encodedFile = flatFileUtility.encode(null, VENDOR_DEFINITION)
      fail("Failure - Expected Exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests file that an Illegal Argument Exception is thrown when null vendor definition provided
   */
  public function testNullVendorDefinition() {
    try {
      var parser = flatFileUtility.parse(FILE_PATH, VENDOR_DEFINITION)
      var xml = (parser as FlatFileParser).Xml
      var encodedFile = flatFileUtility.encode(xml, null)
      fail("Failure - Expected Exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that the fillData function behaves appropriately for right justify
   */
  public function testFillFieldDataJustifyRightSuccessfulCase() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "None",
          : Justify = "Right",
          : Length = 4,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Left"
      }

      var filledData = (encodedFile as FlatFileEncoder).fillFieldData("21", fieldDef)
      assertEquals("0021", filledData.toString())
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that the fillData function behaves appropriately for left justify
   */
  public function testFillFieldDataJustifyLeftSuccessfulCase() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "None",
          : Justify = "Left",
          : Length = 4,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var filledData = (encodedFile as FlatFileEncoder).fillFieldData("21", fieldDef)
      assertEquals("2100", filledData.toString())
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that an IllegalArgumentException is thrown when Fill is empty
   */
  public function testFillFieldDataEmptyFill() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "",
          : Format = "None",
          : Justify = "Left",
          : Length = 10,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var filledData = (encodedFile as FlatFileEncoder).fillFieldData("Exception", fieldDef)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexcpected exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that an IllegalArgumentException is thrown when Fill is Null
   */
  public function testFillFieldDataNullFill() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = null,
          : Format = "None",
          : Justify = "Right",
          : Length = 10,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Left"
      }
      var filledData = (encodedFile as FlatFileEncoder).fillFieldData("Exception", fieldDef)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexcpected exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that an IllegalArgumentException is thrown when Length is Null
   */
  public function testFillFieldDataJustifyRightNullLength() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "",
          : Format = "None",
          : Justify = "Right",
          : Length = null,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Left"
      }
      var filledData = (encodedFile as FlatFileEncoder).fillFieldData("Exception", fieldDef)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexcpected exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that an IllegalArgumentException is thrown when Length is 0
   */
  public function testFillFieldDataJustifyRightLengthZero() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "",
          : Format = "None",
          : Justify = "Right",
          : Length = 0,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Left"
      }
      var filledData = (encodedFile as FlatFileEncoder).fillFieldData("Exception", fieldDef)
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexcpected exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that the same string is returned if no changes are required
   */
  public function testFillFieldDataNoChange() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "None",
          : Justify = "Left",
          : Length = 3,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var filledData = (encodedFile as FlatFileEncoder).fillFieldData("321", fieldDef)
      assertEquals("321", filledData)
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that truncate functions correctly when truncating the left
   */
  public function testTruncateFieldDataLeftSuccessfulCase() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "None",
          : Justify = "Right",
          : Length = 4,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Left"
      }
      var filledData = (encodedFile as FlatFileEncoder).truncateFieldData("123456", fieldDef)
      assertEquals("3456", filledData.toString())
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that truncate functions correctly when truncating the left
   */
  public function testTruncateFieldDataRightSuccessfulCase() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "None",
          : Justify = "Left",
          : Length = 4,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var filledData = (encodedFile as FlatFileEncoder).truncateFieldData("123456", fieldDef)
      assertEquals("1234", filledData.toString())
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that IllegalArgumentException is thrown when Truncate field is empty
   */
  public function testTruncateFieldDataJustifyLeftEmptyTruncate() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "",
          : Format = "None",
          : Justify = "Left",
          : Length = 10,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = ""
      }
      var filledData = (encodedFile as FlatFileEncoder).truncateFieldData("Exception", fieldDef)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexcpected exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that IllegalArgumentException is thrown when Truncate field is null
   */
  public function testTruncateFieldDataJustifyRightNullTruncate() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "",
          : Format = "None",
          : Justify = "Right",
          : Length = 10,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = null
      }
      var filledData = (encodedFile as FlatFileEncoder).truncateFieldData("Exception", fieldDef)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexcpected exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that IllegalArgumentException is thrown when Truncate field is empty
   */
  public function testTruncateFieldDataJustifyRightLengthZero() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "",
          : Format = "None",
          : Justify = "Right",
          : Length = 0,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Left"
      }
      var filledData = (encodedFile as FlatFileEncoder).truncateFieldData("Exception", fieldDef)
      fail("Failure - Expected exception not thrown.")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexcpected exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that the same input string is returned when no change is required
   */
  public function testTruncateFieldDataNoChange() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "None",
          : Justify = "Left",
          : Length = 3,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var filledData = (encodedFile as FlatFileEncoder).truncateFieldData("321", fieldDef)
      assertEquals("321", filledData)
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that the checkFieldDataFormat method behaves as expected, having no affect when the field conforms to the defined format
   */
  public function testCheckFieldDataFormatSuccessfulDate() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "yyMMdd",
          : Justify = "Left",
          : Length = 6,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var testString = "890910"
      (encodedFile as FlatFileEncoder).checkFieldDataCorrectlyFormatted(testString, fieldDef)
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that a ParseException is thrown if data doesn't match expected format
   */
  public function testCheckFieldDataFormatFailCase() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "yyMMdd",
          : Justify = "Left",
          : Length = 6,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var testString = "8909102"
      (encodedFile as FlatFileEncoder).checkFieldDataCorrectlyFormatted(testString, fieldDef)
      fail("Failure - Expected exception not thrown")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown: ${e.Message}")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that a IllegalArgumentException is thrown if data doesn't match expected format
   */
  public function testCheckFieldDataFormatInvalidFormat() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "yyMMddq",
          : Justify = "Left",
          : Length = 6,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var testString = "890910"
      (encodedFile as FlatFileEncoder).checkFieldDataCorrectlyFormatted(testString, fieldDef)
      fail("Failure - Expected exception not thrown")
    } catch (var e: IllegalArgumentException) {
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown: ${e.Message}")
    }
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Tests that a IllegalArgumentException is thrown if data doesn't match expected format
   */
  public function testCheckFieldDataFormatSuccessfulHoursMinutes() {
    try {
      var file = new File(FILE_PATH)
      var xml = com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile.parse(file)
      var encodedFile = flatFileUtility.encode(xml, VENDOR_DEFINITION)
      var fieldDef = new Record_Fields_Entry(){
          : Data = "1",
          : Fill = "0",
          : Format = "HHmm",
          : Justify = "Left",
          : Length = 4,
          : Name = "Constant1",
          : Parameter1 = "",
          : Parameter2 = "",
          : StartPosition = 7,
          : StripType = "None",
          : Truncate = "Right"
      }
      var testString = "1252"
      (encodedFile as FlatFileEncoder).checkFieldDataCorrectlyFormatted(testString, fieldDef)
      //Pass
    } catch (var e: Exception) {
      fail("Failure - Unexpected Exception thrown")
    }
  }
}